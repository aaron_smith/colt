#! /usr/bin/env python
import sys

trues = ['true', 'yes', 'on', '1']
falses = ['false', 'no', 'off', '0']
trues_and_falses = trues + falses
case_sensitive = ['GEOMETRY', 'HAVE_CGAL', 'CGAL_LINKED_WITH_TBB', 'FORTRAN_ORDERING', 'graphite_scaled_PAH',
                  'output_M_F_r', 'output_M_F_r_scat', 'output_M_F_src', 'output_M_F_src_scat', 'output_P_u_r']

def read_defs():
    """ Read file content to a definitions dictionary. """
    defs = {}
    with open('defines.log') as f:          # Written by Makefile
        for line in f:                      # Read each line
            line = line.strip()             # Remove new line characters
            if line:                        # Check for empty lines
                key, val = line.split('=')  # File format: "key=val"
                if key not in case_sensitive: # Check for case sensitivity
                    key = key.casefold()    # Ignore case
                if key in defs:
                    raise ValueError(f'{key} cannot be defined multiple times.')
                defs[key] = val
    return defs

def register_bool(defs, key, default=False):
    """ Require the provided field to be true or false. """
    if key not in defs:
        if default is not None:
            defs[key] = 'true' if default else 'false'
        else:
            defs[key] = None
            return  # Registered as None
    if defs[key].casefold() in trues:
        defs[key] = True  # All true values
    elif defs[key] in falses:
        defs[key] = False  # All false values
    else:
        raise ValueError(f'{key} = {defs[key]} is not a valid option. {trues_and_falses}')

def register_bools(defs, keys, default=False):
    """ Require the provided fields to be true or false. """
    for key in keys:
        register_bool(defs, key, default=default)

def register_float(defs, key, default=0.):
    """ Require the provided field to be a float. """
    if key not in defs:
        defs[key] = float(default) if default is not None else None
    else:
        defs[key] = float(defs[key])  # Convert to float if present

def write_bool(defs, key, default=False, no_val=False, register=True):
    """ Register bool field and write #define statement. """
    if register: register_bool(defs, key, default=default)
    if no_val: return f'#define {key}\n' if defs[key] else ''
    val = 'TRUE' if defs[key] else 'FALSE'
    return f'#define {key} {val}\n'

def write_float(defs, key, default=0., register=True):
    """ Register float field and write constexpr statement. """
    if register: register_float(defs, key, default=default)
    return f'constexpr double {key} = {defs[key]};\n'

def write_geometry(defs, default='SLAB'):
    """ Write geometry section. """
    if 'GEOMETRY' not in defs:
        defs['GEOMETRY'] = default  # Default geometry
    geometry = defs['GEOMETRY'].upper()
    geometries = {key: 'FALSE' for key in ['SLAB', 'SPHERICAL', 'CARTESIAN', 'OCTREE', 'VORONOI']}
    if geometry not in geometries:
        raise ValueError(f'GEOMETRY = {geometry} is not a valid option. {geometries.keys()}')
    geometries[geometry] = 'TRUE'  # Flag selected geometry
    geometry_str = ''
    for key, val in geometries.items():
        geometry_str += f'#define {key} {val}\n'
    geometry_str += write_bool(defs, 'HAVE_CGAL')
    geometry_str += write_bool(defs, 'CGAL_LINKED_WITH_TBB', no_val=True) + '\n'
    geometry_str += write_bool(defs, 'FORTRAN_ORDERING')
    geometry_str += write_bool(defs, 'check_bounds')
    register_bool(defs, 'long_neighbors')  # Default to int
    if defs['long_neighbors']:
        defs['long_circulators'] = True  # Circulators must be long if neighbors are
    else:
        register_bool(defs, 'long_circulators', default=False)  # Default to int
    neib_t = 'long long' if defs['long_neighbors'] else 'int'  # Neighbor type
    circ_t = 'long long' if defs['long_circulators'] else 'int'  # Circulator type
    geometry_str += f'\nusing neib_t = {neib_t};\nusing circ_t = {circ_t};\n'
    return geometry_str

def write_special(defs):
    """ Write special options section. """
    register_bools(defs, ['electron_scattering', 'coherent_electron_scattering'])
    if defs['coherent_electron_scattering']:
        defs['electron_scattering'] = True
    register_bools(defs, ['velocity_gradients', 'voigt_velocity_gradients'])
    if defs['voigt_velocity_gradients']:
        defs['velocity_gradients'] = True
    register_bool(defs, 'sobolev')
    defs['cell_velocity_gradients'] = defs['velocity_gradients'] or defs['sobolev']
    dust_species = ['graphite_dust', 'silicate_dust', 'graphite_scaled_PAH']
    register_bools(defs, dust_species)
    if defs['graphite_scaled_PAH'] and not defs['graphite_dust']:
        raise ValueError('graphite_scaled_PAH requires graphite_dust.')
    defs['multiple_dust_species'] = sum(defs[species] for species in dust_species) > 1
    return (write_bool(defs, 'age_escape') +  # Limit the distance by stellar age
            write_bool(defs, 'box_escape') +  # Restrict ray-tracing to a box region
            write_bool(defs, 'spherical_escape') +  # Restrict ray-tracing a spherical region
            write_bool(defs, 'streaming_escape') +  # Limit the distance by free-streaming
            write_bool(defs, 'multiple_dust_species', register=False) +  # Multiple dust species
            write_bool(defs, 'graphite_dust', register=False) +  # Graphite dust
            write_bool(defs, 'silicate_dust', register=False) +  # Silicate dust
            write_bool(defs, 'graphite_scaled_PAH', register=False) +  # Graphite scaled PAH
            write_bool(defs, 'coherent_line_scattering') +  # Coherent line scattering
            write_bool(defs, 'complete_redistribution') +  # Complete redistribution
            write_bool(defs, 'electron_scattering', register=False) +  # Electron scattering
            write_bool(defs, 'coherent_electron_scattering', register=False) +  # Coherent electron scattering
            write_bool(defs, 'special_relativity') +  # Special relativity
            write_bool(defs, 'velocity_gradients', register=False) +  # Velocity gradients
            write_bool(defs, 'voigt_velocity_gradients', register=False) +  # Voigt velocity gradients
            write_bool(defs, 'sobolev', register=False) +  # Sobolev approximation
            write_bool(defs, 'absorb_lines') +  # Absorb lines (not recommended)
            write_bool(defs, 'cell_velocity_gradients', register=False) +  # Cell velocity gradients
            write_bool(defs, 'dynamical_core_skipping') +  # Dynamical core skipping
            write_float(defs, 'x_crit', default=0.) +  # Constant core skipping value
            write_float(defs, 'x_core', default=0.7) +  # Parallel velocity core method for x < x_core
            write_float(defs, 'x_wing', default=8.) +  # Parallel velocity wing method for x > x_wing
            write_float(defs, 'tau_discard', default=32.) +  # Discard optical depth (absorption)
            write_float(defs, 'weight_discard', default=1e-14) +  # Discard weight (absorption)
            write_float(defs, 'n_tau_dust_sims', default=100) +  # Number of tau_dust simulations
            write_float(defs, 'tau_dust_max', default=1.) +  # Maximum tau_dust value
            write_float(defs, 'deuterium_fraction', default=0.))  # Deuterium fraction (3e-5)

def write_output(defs):
    """ Write output flags. """
    out_str = '\n// Output compile-time values\n'
    # Options for ionization statistics
    opts = ['cell_age_freq', 'radial_age_freq', 'distance_age_freq']
    for opt in opts:
        out_str += write_bool(defs, f'output_ion_{opt}')
    # Options for flow statistics
    out_str += write_bool(defs, 'output_radial_flow')
    # Options for group flow statistics
    out_str += write_bool(defs, 'output_groups')
    out_str += write_bool(defs, 'output_group_flows')
    # Options for subhalo flow statistics
    out_str += write_bool(defs, 'output_subhalos')
    out_str += write_bool(defs, 'output_subhalo_flows')
    # Options for radiation pressure statistics
    out_str += write_bool(defs, 'output_energy_density')
    out_str += write_bool(defs, 'output_acceleration')
    out_str += write_bool(defs, 'output_acceleration_scat')
    out_str += write_bool(defs, 'output_pressure')
    register_bools(defs, ['output_tau_dust_f_esc', 'output_f_esc_r', 'output_rescaled_f_esc_r', 'output_trap_r',
                          'output_M_F_r', 'output_M_F_r_scat', 'output_M_F_src', 'output_M_F_src_scat', 'output_P_u_r'])
    if defs['output_P_u_r']:
        defs['output_trap_r'] = True  # Ratio requires denominator
    defs['output_f_src_r'] = (defs['output_f_esc_r'] or defs['output_rescaled_f_esc_r'] or defs['output_trap_r'] or
                              defs['output_M_F_r'] or defs['output_M_F_r_scat'] or defs['output_M_F_src'] or defs['output_M_F_src_scat'] or defs['output_P_u_r'])
    defs['use_tau'] = not (defs['output_radial_flow'] or defs['output_group_flows'] or defs['output_subhalo_flows'] or
                           defs['output_energy_density'] or defs['output_acceleration'] or defs['output_acceleration_scat'] or defs['output_pressure'] or defs['output_f_src_r'])
    out_str += write_bool(defs, 'output_f_src_r', register=False)
    out_str += write_bool(defs, 'output_tau_dust_f_esc', register=False)
    out_str += write_bool(defs, 'output_f_esc_r', register=False)
    out_str += write_bool(defs, 'output_rescaled_f_esc_r', register=False)
    out_str += write_bool(defs, 'output_trap_r', register=False)
    out_str += write_bool(defs, 'output_M_F_r', register=False)
    out_str += write_bool(defs, 'output_M_F_r_scat', register=False)
    out_str += write_bool(defs, 'output_M_F_src', register=False)
    out_str += write_bool(defs, 'output_M_F_src_scat', register=False)
    out_str += write_bool(defs, 'output_P_u_r', register=False)
    out_str += write_bool(defs, 'use_tau', register=False)
    return out_str

def write_defs(filename):
    """ Write the compile time options file. """
    defs = read_defs()  # Makefile aware definitions
    defs_h = ('#ifndef COMPILE_TIME_INCLUDED\n' +
              '#define COMPILE_TIME_INCLUDED\n\n' +
              '#define TRUE 1\n#define FALSE 0\n\n' +
              write_geometry(defs) +
              write_special(defs) +
              write_output(defs) +
              '\n#endif // COMPILE_TIME_INCLUDED\n')
    # Consistency checks and dependencies
    if defs['GEOMETRY'].upper() != 'SPHERICAL':
        for key in ['velocity_gradients', 'output_tau_dust_f_esc', 'output_f_esc_r', 'output_rescaled_f_esc_r', 'output_trap_r',
                    'output_M_F_r', 'output_M_F_r_scat', 'output_M_F_src', 'output_M_F_src_scat', 'output_P_u_r']:
            if defs[key]:
                raise ValueError(f'{key} requires spherical geometry.')
    if defs['velocity_gradients'] and defs['sobolev']:
        raise ValueError(f'velocity_gradients and sobolev are incompatible.')
    if defs['coherent_line_scattering'] and defs['complete_redistribution']:
        raise ValueError(f'complete_redistribution is incompatible with coherent_line_scattering.')
    with open(filename, "w") as f:
        f.write(defs_h)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        filename = 'source/compile_time.h'
    elif len(sys.argv) == 2:
        filename = sys.argv[1]
    else:
        raise ValueError('Expecting only one argument.')
    write_defs(filename)
