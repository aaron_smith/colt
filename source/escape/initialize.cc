/************************
 * escape/initialize.cc *
 ************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "Escape.h"
#include "../reduction.h" // Reduction operations

#define EVAL_ZA(tab) mass * pow(10., (spec_Z_age.log_##tab(i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                                   + (spec_Z_age.log_##tab(i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

bool avoid_cell(const int cell); // Avoid calculations for certain cells
int find_cell(const Vec3 point, int cell); // Cell index of a point
bool avoid_star_host_halo(const int star); // Check if a star is not in a valid group

// Avoid stars that are outside the emission region
static inline bool invalid_star_sphere(const int i) {
  #if spherical_escape
    return (r_star[i] - escape_center).dot() >= emission_radius2;
  #else
    return false;
  #endif
};
static inline bool invalid_star_box(const int i) {
  #if box_escape
    return (r_star[i].x < emission_box[0].x || r_star[i].x > emission_box[1].x ||
            r_star[i].y < emission_box[0].y || r_star[i].y > emission_box[1].y ||
            r_star[i].z < emission_box[0].z || r_star[i].z > emission_box[1].z);
  #else
    return false;
  #endif
};
static inline bool invalid_star(const int i) {
  return invalid_star_sphere(i) || invalid_star_box(i);
};

/* Setup everything for the escape module. */
void Escape::setup() {
  // Setup spherical escape
  if constexpr (check_escape)
    setup_escape();                          // General escape setup

  // Setup cameras
  if (have_cameras) {
    setup_cameras();                         // General camera setup

    if (root && output_cubes)
      cout << "\nSpectral Bin Images: "
           << double(n_cameras) * double(nx_cube_pixels) * double(ny_pixels) * double(n_bins) * 8. / 1024. / 1024. / 1024.
           << " GB  (" << n_cameras << ", " << nx_cube_pixels << ", " << ny_pixels << ", " << n_bins << ")" << endl;

    // Reset camera data and initialize with zeros
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_bin_escape_fractions)
      bin_f_escs = vectors(n_cameras, vector<double>(n_bins)); // Bin escape fractions
    if (output_dust_absorptions)
      f_abss = vector<double>(n_cameras);    // Dust absorptions
    if (output_bin_dust_absorptions)
      bin_f_abss = vectors(n_cameras, vector<double>(n_bins)); // Bin dust absorptions
    if (output_ion_absorptions)
      f_ions = vectors(n_active_ions, vector<double>(n_cameras)); // Ion absorptions
    if (output_bin_ion_absorptions)
      bin_f_ions = Images(n_active_ions, Image(n_cameras, n_bins)); // Bin ion absorptions
    if (output_absorption_distances)
      mean_dists = vector<double>(n_cameras); // Average absorption distances [cm]
    if (output_bin_absorption_distances)
      bin_mean_dists = Image(n_cameras, n_bins); // Average bin absorption distances [cm]
    if (output_images)
      images = Images(n_cameras, Image(nx_pixels, ny_pixels)); // Images
    if (output_cubes)
      bin_images = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_bins)); // Bin images
    if (output_radial_images)
      radial_images = vectors(n_cameras, vector<double>(n_radial_pixels)); // Radial images
    if (output_radial_cubes)
      bin_radial_images = Images(n_cameras, Image(n_radial_cube_pixels, n_bins)); // Bin radial images
    if (output_gas_columns_int)
      gas_columns_int = vector<double>(n_cameras); // Gas column densities [g/cm^2]
    if (output_gas_columns_esc)
      gas_columns_esc = vector<double>(n_cameras); // Gas column densities [g/cm^2]
    if (output_dust_columns_int) {
      #if multiple_dust_species
        #if graphite_dust
          dust_G_columns_int = vector<double>(n_cameras); // Graphite column densities [g/cm^2]
        #endif
        #if silicate_dust
          dust_S_columns_int = vector<double>(n_cameras); // Silicate column densities [g/cm^2]
        #endif
      #else
        dust_columns_int = vector<double>(n_cameras); // Dust column densities [g/cm^2]
      #endif
    }
    if (output_dust_columns_esc) {
      #if multiple_dust_species
        #if graphite_dust
          dust_G_columns_esc = vector<double>(n_cameras); // Graphite column densities [g/cm^2]
        #endif
        #if silicate_dust
          dust_S_columns_esc = vector<double>(n_cameras); // Silicate column densities [g/cm^2]
        #endif
      #else
        dust_columns_esc = vector<double>(n_cameras); // Dust column densities [g/cm^2]
      #endif
    }
    if (output_metal_columns_int)
      metal_columns_int = vector<double>(n_cameras); // Metal column densities [g/cm^2]
    if (output_metal_columns_esc)
      metal_columns_esc = vector<double>(n_cameras); // Metal column densities [g/cm^2]
    if (output_HI_columns_int)
      HI_columns_int = vector<double>(n_cameras); // HI column densities [g/cm^2]
    if (output_HI_columns_esc)
      HI_columns_esc = vector<double>(n_cameras); // HI column densities [g/cm^2]

    // Initialize intrinsic cameras
    if (output_emission) {
      if (output_images)
        images_int = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_cubes)
        bin_images_int = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_bins));
      if (output_radial_images)
        radial_images_int = vectors(n_cameras, vector<double>(n_radial_pixels));
      if (output_radial_cubes)
        bin_radial_images_int = Images(n_cameras, Image(n_radial_cube_pixels, n_bins));
    }
  }

  // Setup star-based emission sources
  if (star_based_emission) {
    vector<int> active_flags(n_stars);       // Set active star flags
    cell_of_star.resize(n_stars);            // Cell index of each active star
    if (source_model == "GMC") {
      root_error("GMC source model is depricated!");
      // Calculate the star luminosities
      double L_HI = 0.;                      // Total ionizing luminosity [erg/s]
      double L_HeI = 0.;
      double L_HeII = 0.;
      #pragma omp parallel for reduction(+:L_HI, L_HeI, L_HeII)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are outside the mass range
        if (m_massive_star[i] <= 0.)
          continue;
        if (m_massive_star[i] < 12. || m_massive_star[i] > 100.)
          error("GMC model expects stars within the range 12-100 Msun. ["
                + to_string(m_massive_star[i]) + " Msun]");
        if (invalid_star(i))
          continue;                          // Avoid stars outside the emission region
        cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
        if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
          continue;                          // Ignore certain cells or groups
        active_flags[i] = 1;                 // Keep track of active stars
        const double logM = log10(m_massive_star[i]);
        L_HI += pow(10., 29.1169841759 + 8.09061*logM - 1.82165*logM*logM); // [erg/s]
        L_HeI += pow(10., 28.3419526386 + 9.45*logM - 2.14563*logM*logM); // [erg/s]
        L_HeII += pow(10., 24.9894768034 + 13.0864*logM - 3.00213*logM*logM); // [erg/s]
      }
      bin_L_tot = {L_HI, L_HeI, L_HeII};     // Total bin luminosities [erg/s]
      bin_Ndot_tot = {bin_L_tot[0] / global_mean_energy[0], // Total rate [photons/s]
                      bin_L_tot[1] / global_mean_energy[1],
                      bin_L_tot[2] / global_mean_energy[2]};
      L_tot = bin_L_tot[0] + bin_L_tot[1] + bin_L_tot[2]; // Total luminosity [erg/s]
      Ndot_tot = bin_Ndot_tot[0] + bin_Ndot_tot[1] + bin_Ndot_tot[2]; // Total rate [photons/s]
    } else if (source_model == "MRT") {
      root_error("MRT source model is not implemented.");
    } else if (source_file_Z_age != "") {
      bin_Ndot_tot = vector<double>(n_bins); // Bin rates [photons/s]
      bin_L_tot = vector<double>(n_bins);    // Bin luminosities [erg/s]
      global_sigma_ions = vectors(n_active_ions, vector<double>(n_bins)); // Ionization cross-sections [cm^2]
      global_mean_energy = vector<double>(n_bins); // Mean energy of each frequency bin [erg]
      for (int i = 0; i < n_dust_species; ++i)
        global_kappas[i] = vector<double>(n_bins); // Dust opacity [cm^2/g dust]
      Image sigma_Ndot_ions(n_active_ions, n_bins); // Ionization cross-sections rates [cm^2 photons/s]
      dust_vectors kappas_Ndot;              // Dust opacity rates [cm^2/g dust photons/s]
      for (int j = 0; j < n_dust_species; ++j)
        kappas_Ndot[j] = vector<double>(n_bins);
      #pragma omp parallel for reduction(vec_plus:bin_L_tot, bin_Ndot_tot) \
                               reduction(image_plus:sigma_Ndot_ions) \
                               reduction(dust_plus:kappas_Ndot)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are in special cells or outside the emission region
        if (invalid_star(i))
          continue;
        active_flags[i] = 1;                 // Keep track of active stars
        const double mass = m_init_star[i];  // Initial mass of the star [Msun]
        const double Z = Z_star[i];          // Metallicity of the star
        const double age = age_star[i];      // Age of the star [Gyr]

        int i_L_Z = 0, i_L_age = 0;          // Lower interpolation index
        double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
        if (Z >= spec_Z_age.max_Z) {         // Metallicity maximum
          i_L_Z = spec_Z_age.n_Zs - 2;       // Metallicity bins - 2
          frac_R_Z = 1.;
        } else if (Z > spec_Z_age.min_Z) {   // Metallicity minimum
          const double logZ = log10(Z);      // Interpolate in log space
          while (logZ > spec_Z_age.log_Z[i_L_Z+1])
            i_L_Z++;                         // Search metallicity indices
          frac_R_Z = (logZ - spec_Z_age.log_Z[i_L_Z]) / (spec_Z_age.log_Z[i_L_Z+1] - spec_Z_age.log_Z[i_L_Z]);
        }
        if (age >= spec_Z_age.max_age) {     // Age maximum
          i_L_age = spec_Z_age.n_ages - 2;   // Age bins - 2
          frac_R_age = 1.;
        } else if (age > spec_Z_age.min_age) { // Age minimum
          const double logA = log10(age);    // Interpolate in log space
          while (logA > spec_Z_age.log_age[i_L_age+1])
            i_L_age++;                       // Search age indices
          frac_R_age = (logA - spec_Z_age.log_age[i_L_age]) / (spec_Z_age.log_age[i_L_age+1] - spec_Z_age.log_age[i_L_age]);
        }

        // Bilinear interpolation (based on left and right fractions)
        const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
        const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
        for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
          const double local_bin_Ndot = EVAL_ZA(Ndot); // Photon rates [photons/s]
          const double local_bin_L_stars = EVAL_ZA(L); // Luminosities [erg/s]
          bin_Ndot_tot[i_bin] += local_bin_Ndot;
          bin_L_tot[i_bin] += local_bin_L_stars; // Luminosities [erg/s]
          for (int ai = 0; ai < n_active_ions; ++ai) {
            // Cross-section rates [cm^2 photons/s]
            auto& log_sigma_Ndot_ion = spec_Z_age.log_sigma_Ndot_ions[ai];
            double log_sigma_Ndot_val = (log_sigma_Ndot_ion(i_L_Z,i_L_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age
                                      + (log_sigma_Ndot_ion(i_L_Z,i_R_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age;
            sigma_Ndot_ions(ai, i_bin) += mass * pow(10., log_sigma_Ndot_val);
          }
          for (int j = 0; j < n_dust_species; ++j)
            kappas_Ndot[j][i_bin] += EVAL_ZA(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
        }
      }

      // Weighted cross-sections [cm^2] and mean energies [erg]
      #pragma omp parallel for
      for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
        if (bin_Ndot_tot[i_bin] <= 0.)
          continue;                            // Avoid division by zero
        const double bin_Ndot = bin_Ndot_tot[i_bin]; // Bin Ndot
        const double inv_Ndot = safe_division(1., bin_Ndot); // 1 / Ndot
        for (int ai = 0; ai < n_active_ions; ++ai)
          global_sigma_ions[ai][i_bin] = chop(inv_Ndot * sigma_Ndot_ions(ai, i_bin), sigma_chop); // Cross-section [cm^2]
        global_mean_energy[i_bin] = inv_Ndot * bin_L_tot[i_bin]; // Mean energy of each bin [erg]
        for (int j = 0; j < n_dust_species; ++j)
          global_kappas[j][i_bin] = inv_Ndot * kappas_Ndot[j][i_bin]; // Dust opacity [cm^2/g dust]
      }
    } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR" || source_model == "BC03-IMF" ||
               source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
      // Calculate the star luminosities
      double L_HI = 0.;                      // Total ionizing luminosity [erg/s]
      double L_HeI = 0.;
      double L_HeII = 0.;
      double Ndot_HI = 0.;                   // Total ionizing rate [photons/s]
      double Ndot_HeI = 0.;
      double Ndot_HeII = 0.;
      double sigma_Ndot_HI_1 = 0.;           // Cross-section rates [cm^2 photons/s]
      double sigma_Ndot_HI_2 = 0.;
      double sigma_Ndot_HI_3 = 0.;
      double sigma_Ndot_HeI_2 = 0.;
      double sigma_Ndot_HeI_3 = 0.;
      double sigma_Ndot_HeII_3 = 0.;
      double kappa_Ndot_HI = 0.;             // Dust opacity rates [photons/s cm^2/g dust]
      double kappa_Ndot_HeI = 0.;
      double kappa_Ndot_HeII = 0.;
      if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR") {
        #pragma omp parallel for reduction(+:L_HI, L_HeI, L_HeII, Ndot_HI, Ndot_HeI, Ndot_HeII) \
                                 reduction(+:sigma_Ndot_HI_1, sigma_Ndot_HI_2, sigma_Ndot_HI_3) \
                                 reduction(+:sigma_Ndot_HeI_2, sigma_Ndot_HeI_3, sigma_Ndot_HeII_3) \
                                 reduction(+:kappa_Ndot_HI, kappa_Ndot_HeI, kappa_Ndot_HeII)
        for (int i = 0; i < n_stars; ++i) {
          if (invalid_star(i))
            continue;                        // Avoid stars outside the emission region
          cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
          if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
            continue;                        // Ignore certain cells or groups
          active_flags[i] = 1;               // Keep track of active stars
          const double mass = m_init_star[i]; // Initial mass of the star [Msun]
          const double age = age_star[i];    // Age of the star [Gyr]
          int edge = -1;
          if (age <= 1.00000001e-3)          // Age minimum = 1 Myr
            edge = 0;
          else if (age >= 99.999999)         // Age maximum = 100 Gyr
            edge = 50;
          else {
            const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
            const double f_age = floor(d_age); // Floor coordinate
            const double frac_R = d_age - f_age; // Interpolation fraction (right)
            const double frac_L = 1. - frac_R; // Interpolation fraction (left)
            const int i_L = f_age;           // Table age index (left)
            const int i_R = i_L + 1;         // Table age index (right)
            const double local_Ndot_HI = mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R);
            const double local_Ndot_HeI = mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R);
            const double local_Ndot_HeII = mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R);
            Ndot_HI += local_Ndot_HI;        // Rates [photons/s]
            Ndot_HeI += local_Ndot_HeI;
            Ndot_HeII += local_Ndot_HeII;
            L_HI += mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // Luminosities [erg/s]
            L_HeI += mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R);
            L_HeII += mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R);
            // Cross-section rates [cm^2 photons/s]
            sigma_Ndot_HI_1 += mass * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                     + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
            sigma_Ndot_HI_2 += mass * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                     + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            sigma_Ndot_HI_3 += mass * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                     + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            sigma_Ndot_HeI_2 += mass * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                      + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            sigma_Ndot_HeI_3 += mass * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                      + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            sigma_Ndot_HeII_3 += mass * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                       + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
            kappa_Ndot_HI += mass * (ion_age.kappa_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.kappa_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Opacity rates [photons/s cm^2/g dust]
            kappa_Ndot_HeI += mass * (ion_age.kappa_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.kappa_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
            kappa_Ndot_HeII += mass * (ion_age.kappa_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.kappa_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          }

          // Age was out of range so use the bounding value
          if (edge != -1) {
            const double local_Ndot_HI = mass * ion_age.Ndot_HI[edge];
            const double local_Ndot_HeI = mass * ion_age.Ndot_HeI[edge];
            const double local_Ndot_HeII = mass * ion_age.Ndot_HeII[edge];
            Ndot_HI += local_Ndot_HI;        // Rates [photons/s]
            Ndot_HeI += local_Ndot_HeI;
            Ndot_HeII += local_Ndot_HeII;
            L_HI += mass * ion_age.L_HI[edge]; // Luminosities [erg/s]
            L_HeI += mass * ion_age.L_HeI[edge];
            L_HeII += mass * ion_age.L_HeII[edge];
            sigma_Ndot_HI_1 += local_Ndot_HI * ion_age.sigma_HI_1[edge]; // Cross-section rates [cm^2 photons/s]
            sigma_Ndot_HI_2 += local_Ndot_HeI * ion_age.sigma_HI_2[edge];
            sigma_Ndot_HI_3 += local_Ndot_HeII * ion_age.sigma_HI_3[edge];
            sigma_Ndot_HeI_2 += local_Ndot_HeI * ion_age.sigma_HeI_2[edge];
            sigma_Ndot_HeI_3 += local_Ndot_HeII * ion_age.sigma_HeI_3[edge];
            sigma_Ndot_HeII_3 += local_Ndot_HeII * ion_age.sigma_HeII_3[edge];
            kappa_Ndot_HI += local_Ndot_HI * ion_age.kappa_HI[edge]; // Opacity rates [photons/s cm^2/g dust]
            kappa_Ndot_HeI += local_Ndot_HeI * ion_age.kappa_HeI[edge];
            kappa_Ndot_HeII += local_Ndot_HeII * ion_age.kappa_HeII[edge];
          }
        }
      } else if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
        #pragma omp parallel for reduction(+:L_HI, L_HeI, L_HeII, Ndot_HI, Ndot_HeI, Ndot_HeII) \
                                 reduction(+:sigma_Ndot_HI_1, sigma_Ndot_HI_2, sigma_Ndot_HI_3) \
                                 reduction(+:sigma_Ndot_HeI_2, sigma_Ndot_HeI_3, sigma_Ndot_HeII_3) \
                                 reduction(+:kappa_Ndot_HI, kappa_Ndot_HeI, kappa_Ndot_HeII)
        for (int i = 0; i < n_stars; ++i) {
          if (invalid_star(i))
            continue;                        // Avoid stars outside the emission region
          cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
          if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
            continue;                        // Ignore certain cells or groups
          active_flags[i] = 1;               // Keep track of active stars
          const double mass = m_init_star[i]; // Initial mass of the star [Msun]
          const double Z = Z_star[i];        // Metallicity of the star
          const double age = age_star[i];    // Age of the star [Gyr]

          int i_L_Z = 0, i_L_age = 0;        // Lower interpolation index
          double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
          if (Z >= 4e-2) {                   // Metallicity maximum = 0.04
            i_L_Z = 11;                      // 13 metallicity bins
            frac_R_Z = 1.;
          } else if (Z > 1e-5) {             // Metallicity minimum = 10^-5
            const double logZ = log10(Z);    // Interpolate in log space
            while (logZ > logZ_BP[i_L_Z+1])
              i_L_Z++;                       // Search metallicity indices
            frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
          }
          if (age >= 100.) {                 // Age maximum = 100 Gyr
            i_L_age = 49;                    // 51 age bins
            frac_R_age = 1.;
          } else if (age > 1e-3) {           // Age minimum = 1 Myr
            const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
            const double f_age = floor(d_age); // Floor coordinate
            frac_R_age = d_age - f_age;      // Interpolation fraction (right)
            i_L_age = int(f_age);            // Table age index (left)
          }

          // Bilinear interpolation (based on left and right fractions)
          const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
          const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
          // Photon rates [photons/s]
          const double local_Ndot_HI = mass * pow(10., (ion_Z_age.log_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                     + (ion_Z_age.log_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          const double local_Ndot_HeI = mass * pow(10., (ion_Z_age.log_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                      + (ion_Z_age.log_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          const double local_Ndot_HeII = mass * pow(10., (ion_Z_age.log_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                       + (ion_Z_age.log_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          Ndot_HI += local_Ndot_HI;
          Ndot_HeI += local_Ndot_HeI;
          Ndot_HeII += local_Ndot_HeII;
          // Luminosities [erg/s]
          L_HI += mass * pow(10., (ion_Z_age.log_L_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                + (ion_Z_age.log_L_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          L_HeI += mass * pow(10., (ion_Z_age.log_L_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                 + (ion_Z_age.log_L_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          L_HeII += mass * pow(10., (ion_Z_age.log_L_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Cross-section rates [cm^2 photons/s]
          sigma_Ndot_HI_1 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HI_2 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HI_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HeI_2 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                            + (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HeI_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                            + (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          sigma_Ndot_HeII_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                             + (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          // Opacity rates [photons/s cm^2/g dust]
          kappa_Ndot_HI += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          kappa_Ndot_HeI += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          kappa_Ndot_HeII += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        }
      }

      bin_L_tot = {L_HI, L_HeI, L_HeII};     // Total bin luminosities [erg/s]
      bin_Ndot_tot = {Ndot_HI, Ndot_HeI, Ndot_HeII}; // Total bin rates [photons/s]
      // Global weighted cross-sections [cm^2], mean energies [erg], and opacities [cm^2/g dust]
      global_sigma_ions.resize(n_active_ions); // Ionization cross-sections [cm^2]
      global_sigma_ions[HI_ION] = {sigma_Ndot_HI_1 / Ndot_HI, sigma_Ndot_HI_2 / Ndot_HeI, sigma_Ndot_HI_3 / Ndot_HeII};
      global_sigma_ions[HeI_ION] = {0., sigma_Ndot_HeI_2 / Ndot_HeI, sigma_Ndot_HeI_3 / Ndot_HeII};
      global_sigma_ions[HeII_ION] = {0., 0., sigma_Ndot_HeII_3 / Ndot_HeII};
      global_mean_energy = {L_HI / Ndot_HI, L_HeI / Ndot_HeI, L_HeII / Ndot_HeII};
      global_kappas[0] = {kappa_Ndot_HI / Ndot_HI, kappa_Ndot_HeI / Ndot_HeI, kappa_Ndot_HeII / Ndot_HeII};
      L_tot = bin_L_tot[0] + bin_L_tot[1] + bin_L_tot[2]; // Total luminosity [erg/s]
      Ndot_tot = bin_Ndot_tot[0] + bin_Ndot_tot[1] + bin_Ndot_tot[2]; // Total rate [photons/s]
    }

    // Filter star data
    int n_active = 0;                        // Number of actively emitting stars
    const bool read_group_id_star = !group_id_star.empty(); // Group ID
    const bool read_subhalo_id_star = !subhalo_id_star.empty(); // Subhalo ID
    for (int i = 0; i < n_stars; ++i)
      if (active_flags[i]) {
        if (i != n_active) {
          r_star[n_active] = r_star[i];      // Move active stars to the front
          if (read_v_star)                   // Stellar velocities [cm/s]
            v_star[n_active] = v_star[i];
          if (read_m_massive_star)           // Massive stellar masses [Msun]
            m_massive_star[n_active] = m_massive_star[i];
          if (read_m_init_star)              // Initial stellar masses [Msun]
            m_init_star[n_active] = m_init_star[i];
          if (read_Z_star)                   // Stellar metallicities
            Z_star[n_active] = Z_star[i];
          if (read_age_star)                 // Stellar ages [Gyr]
            age_star[n_active] = age_star[i];
          if (read_group_id_star)            // Group ID
            group_id_star[n_active] = group_id_star[i];
          if (read_subhalo_id_star)          // Subhalo ID
            subhalo_id_star[n_active] = subhalo_id_star[i];
          cell_of_star[n_active] = cell_of_star[i]; // Cell index of each star
        }
        n_active++;
      }
    if (n_active <= 0)
      root_error("There are no active stars in this simulation");
    n_stars = n_active;                      // Restrict access to active stars
    r_star.resize(n_active);
    if (read_v_star)                         // Stellar velocities [cm/s]
      v_star.resize(n_active);
    if (read_m_massive_star)                 // Massive stellar masses [Msun]
      m_massive_star.resize(n_active);
    if (read_m_init_star)                    // Initial stellar masses [Msun]
      m_init_star.resize(n_active);
    if (read_Z_star)                         // Stellar metallicities
      Z_star.resize(n_active);
    if (read_age_star)                       // Stellar ages [Gyr]
      age_star.resize(n_active);
    if (read_group_id_star)                  // Group ID
      group_id_star.resize(n_active);
    if (read_subhalo_id_star)                // Subhalo ID
      subhalo_id_star.resize(n_active);
    cell_of_star.resize(n_active);           // Cell index of each active star
  }
}
