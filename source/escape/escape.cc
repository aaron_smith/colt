/********************
 * escape/escape.cc *
 ********************

 * Driver: Assign rays for escape, etc.

*/

#include "../proto.h"
#include "Escape.h"
#include "../timing.h" // Timing functionality

static vectors all_Ndot_int;                 // Intrinsic bin photon rates [photons/s]
static vectors all_Ndot_esc;                 // Escaped bin photon rates [photons/s]
static vectors all_Ndot_abs;                 // Dust absorbed bin photon rates [photons/s]
static vector<Image> all_Ndot_ions;          // Absorbed bin photon rates for all ions [photons/s]
static vectors all_Ndot_weight_ions;         // Total photon rate (ionization absorbed) [photons/s]
static vectors all_n_ions_abs;               // Number densities for all ions [cm^-3]
static vector<Image> all_sigma_ions;         // Bin cross-sections for all ions [cm^2]
static vector<dust_vectors> all_kappas;      // Bin dust opacity [cm^2/g dust]
static vectors all_k_abs;                    // Bin absorption coefficients [1/cm]
static vector<dust_vectors> all_k_dusts_abs; // Bin dust absorption coefficients [1/cm]
static vector<Image> all_k_ions_abs;         // Bin absorption coefficients for all ions [1/cm]
static vectors all_dist_abs;                 // Absorption distances [cm]

static void (*setup_spectra)(const int);     // Set the bin spectral properties

extern Timer escape_timer; // Clock timing
double spherical_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding sphere
double box_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding box
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

#define EVAL_ZA(tab) mass * pow(10., (spec_Z_age.log_##tab(i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                                   + (spec_Z_age.log_##tab(i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)
#define MEAN_ZA(tab) mass_Ndot * pow(10., (spec_Z_age.log_##tab##s_Ndot[i](i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab##s_Ndot[i](i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                                        + (spec_Z_age.log_##tab##s_Ndot[i](i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab##s_Ndot[i](i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)

/* Set the spectral properties of each bin based on the GMC model. */
static void setup_spectra_GMC(const int star) {
  // Note: This needs to be updated!
  auto& Ndot_int = all_Ndot_int[thread];     // Reference to the local Ndot_int
  const double logM = log10(m_massive_star[star]);
  Ndot_int[HI_ION] = pow(10., 29.1169841759 + 8.09061*logM - 1.82165*logM*logM); // HI [erg/s]
  Ndot_int[HeI_ION] = pow(10., 28.3419526386 + 9.45*logM - 2.14563*logM*logM); // HeI [erg/s]
  Ndot_int[HeII_ION] = pow(10., 24.9894768034 + 13.0864*logM - 3.00213*logM*logM); // HeII [erg/s]
}

/* Set the spectral properties based on general (Z,age,bin) tables. */
static void setup_spectra_Z_age(const int star) {
  auto& Ndot_int = all_Ndot_int[thread];     // Reference to the local Ndot_int
  auto& sigma_ions = all_sigma_ions[thread]; // Reference to the local sigma_ions
  auto& kappas = all_kappas[thread];         // Reference to the local dust opacity
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double Z = Z_star[star];             // Metallicity of the star
  const double age = age_star[star];         // Age of the star [Gyr]

  int i_L_Z = 0, i_L_age = 0;                // Lower interpolation index
  double frac_R_Z = 0., frac_R_age = 0.;     // Upper interpolation fraction
  if (Z >= spec_Z_age.max_Z) {               // Metallicity maximum
    i_L_Z = spec_Z_age.n_Zs - 2;             // Metallicity bins - 2
    frac_R_Z = 1.;
  } else if (Z > spec_Z_age.min_Z) {         // Metallicity minimum
    const double logZ = log10(Z);            // Interpolate in log space
    while (logZ > spec_Z_age.log_Z[i_L_Z+1])
      i_L_Z++;                               // Search metallicity indices
    frac_R_Z = (logZ - spec_Z_age.log_Z[i_L_Z]) / (spec_Z_age.log_Z[i_L_Z+1] - spec_Z_age.log_Z[i_L_Z]);
  }
  if (age >= spec_Z_age.max_age) {           // Age maximum
    i_L_age = spec_Z_age.n_ages - 2;         // Age bins - 2
    frac_R_age = 1.;
  } else if (age > spec_Z_age.min_age) {     // Age minimum
    const double logA = log10(age);          // Interpolate in log space
    while (logA > spec_Z_age.log_age[i_L_age+1])
      i_L_age++;                             // Search age indices
    frac_R_age = (logA - spec_Z_age.log_age[i_L_age]) / (spec_Z_age.log_age[i_L_age+1] - spec_Z_age.log_age[i_L_age]);
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
  const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
  for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
    const double Ndot = Ndot_int[i_bin] = EVAL_ZA(Ndot); // Photon rate [photons/s]
    const double mass_Ndot = mass / Ndot;    // Interpolation normalization
    // Cross-sections [cm^2]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      auto& log_sigma_Ndot_ion = spec_Z_age.log_sigma_Ndot_ions[ai];
      double log_sigma_Ndot_val = (log_sigma_Ndot_ion(i_L_Z,i_L_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age
                                + (log_sigma_Ndot_ion(i_L_Z,i_R_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age;
      sigma_ions[ai] = chop(mass_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
    }
    for (int i = 0; i < n_dust_species; ++i)
      kappas[i][i_bin] = MEAN_ZA(kappa);     // Dust opacity [cm^2/g dust]
  }
}

/* Set the spectral properties of each bin based on the BC03-IMF-SOLAR and BC03-IMF-HALF-SOLAR models. */
static void setup_spectra_imf_solar(const int star) {
  if constexpr (multiple_dust_species)
    root_error("Pre-tabulated source models only support one dust species!");
  auto& Ndot_int = all_Ndot_int[thread];     // Reference to the local Ndot_int
  auto& sigma_ions = all_sigma_ions[thread]; // Reference to the local sigma_ions
  auto& kappa = all_kappas[thread][0];       // Reference to the local dust opacity
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double age = age_star[star];         // Age of the star [Gyr]
  int edge = -1;
  if (age <= 1.00000001e-3)                  // Age minimum = 1 Myr
    edge = 0;
  else if (age >= 99.999999)                 // Age maximum = 100 Gyr
    edge = 50;
  else {
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    const double frac_R = d_age - f_age;     // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_age;                   // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    Ndot_int[0] = mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R); // HI [photons/s]
    Ndot_int[1] = mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R); // HeI [photons/s]
    Ndot_int[2] = mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R); // HeII [photons/s]
    const double mass_Ndot[3] = {mass/Ndot_int[0], mass/Ndot_int[1], mass/Ndot_int[2]}; // Interpolation normalization
    sigma_ions(HI_ION,0) = mass_Ndot[0] * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Cross-sections [cm^2]
    sigma_ions(HI_ION,1) = mass_Ndot[1] * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
    sigma_ions(HI_ION,2) = mass_Ndot[2] * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    sigma_ions(HeI_ION,1) = mass_Ndot[1] * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
    sigma_ions(HeI_ION,2) = mass_Ndot[2] * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    sigma_ions(HeII_ION,2) = mass_Ndot[2] * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    kappa[0] = mass_Ndot[0] * (ion_age.kappa_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.kappa_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Dust opacity [cm^2/g dust]
    kappa[1] = mass_Ndot[1] * (ion_age.kappa_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.kappa_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
    kappa[2] = mass_Ndot[2] * (ion_age.kappa_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.kappa_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
    return;                                  // Finished setup
  }

  // Age was out of range so use the bounding value
  Ndot_int[0] = mass * ion_age.Ndot_HI[edge]; // HI [photons/s]
  Ndot_int[1] = mass * ion_age.Ndot_HeI[edge]; // HeI [photons/s]
  Ndot_int[2] = mass * ion_age.Ndot_HeII[edge]; // HeII [photons/s]
  sigma_ions(HI_ION,0) = ion_age.sigma_HI_1[edge]; // Cross-sections [cm^2]
  sigma_ions(HI_ION,1) = ion_age.sigma_HI_2[edge];
  sigma_ions(HI_ION,2) = ion_age.sigma_HI_3[edge];
  sigma_ions(HeI_ION,1) = ion_age.sigma_HeI_2[edge];
  sigma_ions(HeI_ION,2) = ion_age.sigma_HeI_3[edge];
  sigma_ions(HeII_ION,2) = ion_age.sigma_HeII_3[edge];
  kappa[0] = ion_age.kappa_HI[edge];         // Dust opacity [cm^2/g dust]
  kappa[1] = ion_age.kappa_HeI[edge];
  kappa[2] = ion_age.kappa_HeII[edge];
}

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};
/* Set the spectral properties of each bin based on the BC03-IMF, BPASS-IMF-135-100, or BPASS-CHAB-100 models.*/
static void setup_spectra_imf(const int star) {
  if constexpr (multiple_dust_species)
    root_error("Pre-tabulated source models only support one dust species!");
  auto& Ndot_int = all_Ndot_int[thread];     // Reference to the local Ndot_int
  auto& sigma_ions = all_sigma_ions[thread]; // Reference to the local sigma_ions
  auto& kappa = all_kappas[thread][0];       // Reference to the local dust opacity
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double Z = Z_star[star];             // Metallicity of the star
  const double age = age_star[star];         // Age of the star [Gyr]
  int i_L_Z = 0, i_L_age = 0;                // Lower interpolation index
  double frac_R_Z = 0., frac_R_age = 0.;     // Upper interpolation fraction
  if (Z >= 4e-2) {                           // Metallicity maximum = 0.04
    i_L_Z = 11;                              // 13 metallicity bins
    frac_R_Z = 1.;
  } else if (Z > 1e-5) {                     // Metallicity minimum = 10^-5
    const double logZ = log10(Z);            // Interpolate in log space
    while (logZ > logZ_BP[i_L_Z+1])
      i_L_Z++;                               // Search metallicity indices
    frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
  }

  if (age >= 100.) {                         // Age maximum = 100 Gyr
    i_L_age = 49;                            // 51 age bins
    frac_R_age = 1.;
  } else if (age > 1e-3) {                   // Age minimum = 1 Myr
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    frac_R_age = d_age - f_age;              // Interpolation fraction (right)
    i_L_age = int(f_age);                    // Table age index (left)
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
  const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
  // Photon rates [photons/s]
  Ndot_int[0] = mass * pow(10., (ion_Z_age.log_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                              + (ion_Z_age.log_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  Ndot_int[1] = mass * pow(10., (ion_Z_age.log_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                              + (ion_Z_age.log_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  Ndot_int[2] = mass * pow(10., (ion_Z_age.log_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                              + (ion_Z_age.log_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  const double mass_Ndot[3] = {mass/Ndot_int[0], mass/Ndot_int[1], mass/Ndot_int[2]}; // Interpolation normalization
  sigma_ions(HI_ION,0) = mass_Ndot[0] * pow(10., (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age // Cross sections [cm^2]
                                               + (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  sigma_ions(HI_ION,1) = mass_Ndot[1] * pow(10., (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  sigma_ions(HI_ION,2) = mass_Ndot[2] * pow(10., (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  sigma_ions(HeI_ION,1) = mass_Ndot[1] * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                + (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  sigma_ions(HeI_ION,2) = mass_Ndot[2] * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                + (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  sigma_ions(HeII_ION,2) = mass_Ndot[2] * pow(10., (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                 + (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  kappa[0] = mass_Ndot[0] * pow(10., (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
  kappa[1] = mass_Ndot[1] * pow(10., (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  kappa[2] = mass_Ndot[2] * pow(10., (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
  return;                                    // Finished setup
}

/* Calculate escape for a single line of sight. */
void Escape::ray_trace(const int star, const int camera) {
  auto& n_ions_abs = all_n_ions_abs[thread]; // Reference to the local n_ions_abs [cm^-3]
  const auto& sigma_ions = all_sigma_ions[thread]; // Reference to the local sigma_ions [cm^2]
  const auto& kappas = all_kappas[thread];   // Reference to the local dust opacity [cm^2/g dust]
  const auto& Ndot_int = all_Ndot_int[thread]; // Reference to the local Ndot_int [photons/s]
  auto& Ndot_esc = all_Ndot_esc[thread];     // Reference to the local Ndot_esc [photons/s]
  auto& Ndot_abs = all_Ndot_abs[thread];     // Reference to the local Ndot_abs [photons/s]
  auto& Ndot_ions = all_Ndot_ions[thread];   // Reference to the local Ndot_ions [photons/s]
  auto& Ndot_weight_ions = all_Ndot_weight_ions[thread]; // Reference to the local Ndot_weight_ions [photons/s]
  auto& k_abs = all_k_abs[thread];           // Reference to the local k_abs [1/cm]
  auto& k_dusts_abs = all_k_dusts_abs[thread]; // Reference to the local k_dusts_abs [1/cm]
  auto& k_ions_abs = all_k_ions_abs[thread]; // Reference to the local all_k_ions_abs [1/cm]
  auto& dist_abs = all_dist_abs[thread];     // Reference to the local dist_abs [cm]
  int cell = cell_of_star[star];             // Cell index
  int next_cell;                             // Next cell index
  double l;                                  // Path length [cm]
  Vec3 point = r_star[star];                 // Emission position [cm]
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  #if check_escape
    double l_exit = positive_infinity;       // Remaining distance to escape [cm]
  #endif
  #if spherical_escape                       // Spherical escape distance
    inplace_min(l_exit, spherical_escape_distance(point, k_cam));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(l_exit, box_escape_distance(point, k_cam));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(l_exit, max_streaming);
  #endif
  double rho_dl = 0.;                        // Gas column density [g/cm^2]
  #if multiple_dust_species
    #if graphite_dust
      double rho_dust_G_dl = 0.;             // Graphite column density [g/cm^2]
    #endif
    #if silicate_dust
      double rho_dust_S_dl = 0.;             // Silicate column density [g/cm^2]
    #endif
  #else
    double rho_dust_dl = 0.;                 // Dust column density [g/cm^2]
  #endif
  double rho_Z_dl = 0.;                      // Metal column density [g/cm^2]
  double rho_HI_dl = 0.;                     // HI column density [g/cm^2]
  double Ndot_weight_int = 0.;               // Total photon rate (intrinsic) [photons/s]
  double Ndot_weight_esc = 0.;               // Total photon rate (escaped) [photons/s]
  double Ndot_weight_abs = 0.;               // Total photon rate (dust absorbed) [photons/s]
  double Ndot_weight_dist = 0.;              // Total photon rate (mean distance) [photons/s]
  setup_spectra(star);                       // Set the bin spectral properties

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(point, camera);   // Position in camera coordinates
  const double r_pos = have_radial_cameras ? norm(pos) : 0.; // Radial position

  // Set the initial escaped and dust absorbed photon rates
  for (int bin = 0; bin < n_bins; ++bin) {
    Ndot_weight_int += Ndot_int[bin];        // Sum of all bins (intrinsic)
    Ndot_esc[bin] = Ndot_int[bin];           // Same as intrinsic value
    Ndot_abs[bin] = 0.;                      // No dust absorption yet
    dist_abs[bin] = 0.;                      // Absorption distance starts from zero
  }
  double l_tot = 0.;                         // Total path length starts from zero
  for (int ai = 0; ai < n_active_ions; ++ai) {
    Ndot_weight_ions[ai] = 0.;               // Total photon rate (ionization absorbed) [photons/s]
    for (int bin = 0; bin < n_bins; ++bin)
      Ndot_ions(ai,bin) = 0.;                // No ionization absorption yet
  }

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    // Set the absorption coefficients of each bin
    for (int bin = 0; bin < n_bins; ++bin) {
      k_abs[bin] = 0.;                       // Reset the absorption coefficient
      #if multiple_dust_species
        #if graphite_dust
          #if graphite_scaled_PAH
            k_abs[bin] += k_dusts_abs[GRAPHITE][bin] = fm1_PAH * kappas[GRAPHITE][bin] * rho_dust_G[cell];
            k_abs[bin] += k_dusts_abs[PAH][bin] = f_PAH * kappas[PAH][bin] * rho_dust_G[cell];
          #else
            k_abs[bin] += k_dusts_abs[GRAPHITE][bin] = kappas[GRAPHITE][bin] * rho_dust_G[cell]; // Graphite
          #endif
        #endif
        #if silicate_dust
          k_abs[bin] += k_dusts_abs[SILICATE][bin] = kappas[SILICATE][bin] * rho_dust_S[cell]; // Silicate
        #endif
      #else
        k_abs[bin] += k_dusts_abs[0][bin] = kappas[0][bin] * rho_dust[cell]; // Single-species dust
      #endif
    }
    for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
      const double n_atom = fields[n_indices[aa]].data[cell]; // Atom number density [cm^-3]
      const int ion_count = ion_counts[aa];  // Number of active ions
      for (int j = 0; j < ion_count; ++j, ++ai) {
        const double x_ion = fields[x_indices[ai]].data[cell]; // Ionization fraction
        n_ions_abs[ai] = x_ion * n_atom;     // Save species number densities
        for (int bin = 0; bin < n_bins; ++bin)
          k_abs[bin] += k_ions_abs(ai,bin) = n_ions_abs[ai] * sigma_ions(ai,bin); // Ionization contributions
      }
    }

    // Check for spherical or box escape
    #if check_escape
      if (l_exit <= l) {                     // Exit region before cell
        for (int bin = 0; bin < n_bins; ++bin) {
          const double dtau_abs = k_abs[bin] * l_exit; // Absorption optical depth
          const double exp_dtau_abs = exp(-dtau_abs);
          const double Ndot_dl = Ndot_esc[bin] * ((dtau_abs < 1e-5) ? // Numerical stability
            (1. - 0.5 * dtau_abs) * l_exit : // Series approx (dtau_abs << 1)
            (1. - exp_dtau_abs) / k_abs[bin]); // Radiative transfer solution
          const double tau_abs = k_abs[bin] * l_tot; // Absorption optical depth
          const double abs_dl = Ndot_esc[bin] * ((dtau_abs < 1e-5) ? // Numerical stability
            l_exit * (l_tot + 0.5 * l_exit - dtau_abs * (0.5 * l_tot + l_exit / 3.)) : // Series approx (dtau_abs << 1)
            (1. + tau_abs - (1. + tau_abs + dtau_abs) * exp_dtau_abs) / (k_abs[bin] * k_abs[bin])); // Analytic solution
          Ndot_esc[bin] *= exp_dtau_abs;     // Reduce weight by exp(-dtau_abs)
          for (int i = 0; i < n_dust_species; ++i)
            Ndot_abs[bin] += k_dusts_abs[i][bin] * Ndot_dl; // Photon rate removed by dust absorption
          for (int ai = 0; ai < n_active_ions; ++ai)
            Ndot_ions(ai,bin) += k_ions_abs(ai,bin) * Ndot_dl; // Photon rate removed by ionization absorption
          dist_abs[bin] += abs_dl;           // Absorption distance integral
        }
        rho_dl += rho[cell] * l_exit;        // Gas column density [g/cm^2]
        #if multiple_dust_species
          #if graphite_dust
            rho_dust_G_dl += rho_dust_G[cell] * l_exit; // Graphite column density [g/cm^2]
          #endif
          #if silicate_dust
            rho_dust_S_dl += rho_dust_S[cell] * l_exit; // Silicate column density [g/cm^2]
          #endif
        #else
          rho_dust_dl += rho_dust[cell] * l_exit; // Dust column density [g/cm^2]
        #endif
        rho_Z_dl += rho[cell] * Z[cell] * l_exit; // Metal column density [g/cm^2]
        rho_HI_dl += rho[cell] * x_HI[cell] * l_exit; // HI column density [g/cm^2]
        l_tot += l_exit;                     // Update the total path length [cm]
        break;                               // Valid ray completion
      }
      l_exit -= l;                           // Remaining distance to exit [cm]
    #endif

    // Cumulative LOS optical depth
    for (int bin = 0; bin < n_bins; ++bin) {
      const double dtau_abs = k_abs[bin] * l; // Absorption optical depth
      const double exp_dtau_abs = exp(-dtau_abs);
      const double Ndot_dl = Ndot_esc[bin] * ((dtau_abs < 1e-5) ? // Numerical stability
        (1. - 0.5 * dtau_abs) * l :          // Series approx (dtau_abs << 1)
        (1. - exp_dtau_abs) / k_abs[bin]);   // Radiative transfer solution
      const double tau_abs = k_abs[bin] * l_tot; // Absorption optical depth
      const double abs_dl = Ndot_esc[bin] * ((dtau_abs < 1e-5) ? // Numerical stability
        l * (l_tot + 0.5 * l - dtau_abs * (0.5 * l_tot + l / 3.)) : // Series approx (dtau_abs << 1)
        (1. + tau_abs - (1. + tau_abs + dtau_abs) * exp_dtau_abs) / (k_abs[bin] * k_abs[bin])); // Analytic solution
      Ndot_esc[bin] *= exp_dtau_abs;         // Reduce weight by exp(-dtau_abs)
      for (int i = 0; i < n_dust_species; ++i)
        Ndot_abs[bin] += k_dusts_abs[i][bin] * Ndot_dl; // Photon rate removed by dust absorption
      for (int ai = 0; ai < n_active_ions; ++ai)
        Ndot_ions(ai,bin) += k_ions_abs(ai,bin) * Ndot_dl; // Photon rate removed by ionization absorption
      dist_abs[bin] += abs_dl;               // Absorption distance integral
    }
    rho_dl += rho[cell] * l;                 // Gas column density [g/cm^2]
    #if multiple_dust_species
      #if graphite_dust
        rho_dust_G_dl += rho_dust_G[cell] * l; // Graphite column density [g/cm^2]
      #endif
      #if silicate_dust
        rho_dust_S_dl += rho_dust_S[cell] * l; // Silicate column density [g/cm^2]
      #endif
    #else
      rho_dust_dl += rho_dust[cell] * l;     // Dust column density [g/cm^2]
    #endif
    rho_Z_dl += rho[cell] * Z[cell] * l;     // Metal column density [g/cm^2]
    rho_HI_dl += rho[cell] * x_HI[cell] * l; // HI column density [g/cm^2]
    l_tot += l;                              // Update the total path length [cm]
    point += k_cam * l;                      // Update the position of the photon

    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the photon is trapped
        break;
    }
    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Add escaped photon rate contributions
  for (int bin = 0; bin < n_bins; ++bin) {
    // Note: Ndot_weight_int is already calculated above
    Ndot_weight_esc += Ndot_esc[bin];        // Sum of all bins (escaped)
    Ndot_weight_abs += Ndot_abs[bin];        // Sum of all bins (dust absorbed)
    dist_abs[bin] /= l_tot;                  // Absorption distance normalization
    Ndot_weight_dist += dist_abs[bin];       // Sum of all bins (mean distance)
  }
  for (int ai = 0; ai < n_active_ions; ++ai) {
    for (int bin = 0; bin < n_bins; ++bin)
      Ndot_weight_ions[ai] += Ndot_ions(ai,bin); // Sum of all bins (ionized)
  }
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs[camera] += Ndot_weight_esc;       // Escaped photon rate
  if (output_bin_escape_fractions) {
    for (int bin = 0; bin < n_bins; ++bin) {
      #pragma omp atomic
      bin_f_escs[camera][bin] += Ndot_esc[bin]; // Escaped bin photon rates
    }
  }
  if (output_dust_absorptions)
    #pragma omp atomic
    f_abss[camera] += Ndot_weight_abs;       // Dust absorbed photon rates
  if (output_bin_dust_absorptions) {
    for (int bin = 0; bin < n_bins; ++bin) {
      #pragma omp atomic
      bin_f_abss[camera][bin] += Ndot_abs[bin]; // Dust absorbed bin photon rates
    }
  }
  if (output_ion_absorptions)
    for (int ai = 0; ai < n_active_ions; ++ai) {
      #pragma omp atomic
      f_ions[ai][camera] += Ndot_weight_ions[ai]; // Ionization absorbed photon rates
    }
  if (output_bin_ion_absorptions) {
    for (int ai = 0; ai < n_active_ions; ++ai) {
      for (int bin = 0; bin < n_bins; ++bin) {
        #pragma omp atomic
        bin_f_ions[ai](camera,bin) += Ndot_ions(ai,bin); // Ionization absorbed bin photon rates
      }
    }
  }
  if (output_absorption_distances)
    #pragma omp atomic
    mean_dists[camera] += Ndot_weight_dist;  // Average absorption distances [cm]
  if (output_bin_absorption_distances) {
    for (int bin = 0; bin < n_bins; ++bin) {
      #pragma omp atomic
      bin_mean_dists(camera,bin) += dist_abs[bin]; // Average bin absorption distances [cm]
    }
  }

  // Add image contributions
  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images[camera](ix, iy) += Ndot_weight_esc; // Escaped SB images
      if (output_emission)
        #pragma omp atomic
        images_int[camera](ix, iy) += Ndot_weight_int; // Intrinsic SB images
    }
  }

  // Add bin image contributions
  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_cube_pixels) {  // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      for (int bin = 0; bin < n_bins; ++bin) {
        #pragma omp atomic
        bin_images[camera](ix, iy, bin) += Ndot_esc[bin]; // Escaped bin SB images
      }
      if (output_emission) {
        for (int bin = 0; bin < n_bins; ++bin) {
          #pragma omp atomic
          bin_images_int[camera](ix, iy, bin) += Ndot_int[bin]; // Intrinsic bin SB images
        }
      }
    }
  }

  // Add radial image contributions
  if (output_radial_images) {
    const double r_pix = inverse_radial_pixel_width * r_pos;
    if (r_pix < double(n_radial_pixels)) {   // Range check
      const int ir = floor(r_pix);
      #pragma omp atomic
      radial_images[camera][ir] += Ndot_weight_esc; // Escaped radial SB images
      if (output_emission)
        #pragma omp atomic
        radial_images_int[camera][ir] += Ndot_weight_int; // Intrinsic radial SB images
    }
  }

  // Add bin radial image contributions
  if (output_radial_cubes) {
    const double r_pix = inverse_radial_cube_pixel_width * r_pos;
    if (r_pix < double(n_radial_cube_pixels)) { // Range check
      const int ir = floor(r_pix);
      for (int bin = 0; bin < n_bins; ++bin) {
        #pragma omp atomic
        bin_radial_images[camera](ir, bin) += Ndot_esc[bin]; // Escaped bin radial SB images
      }
    }
  }

  // Add column density contributions
  if (output_gas_columns_int)
    #pragma omp atomic
    gas_columns_int[camera] += Ndot_weight_int * rho_dl; // Gas column density [g/cm^2]
  if (output_gas_columns_esc)
    #pragma omp atomic
    gas_columns_esc[camera] += Ndot_weight_esc * rho_dl; // Gas column density [g/cm^2]
  if (output_dust_columns_int) {
    #if multiple_dust_species
      #if graphite_dust
        #pragma omp atomic
        dust_G_columns_int[camera] += Ndot_weight_int * rho_dust_G_dl; // Graphite column density [g/cm^2]
      #endif
      #if silicate_dust
        #pragma omp atomic
        dust_S_columns_int[camera] += Ndot_weight_int * rho_dust_S_dl; // Silicate column density [g/cm^2]
      #endif
    #else
      #pragma omp atomic
      dust_columns_int[camera] += Ndot_weight_int * rho_dust_dl; // Dust column density [g/cm^2]
    #endif
  }
  if (output_dust_columns_esc) {
    #if multiple_dust_species
      #if graphite_dust
        #pragma omp atomic
        dust_G_columns_esc[camera] += Ndot_weight_esc * rho_dust_G_dl; // Graphite column density [g/cm^2]
      #endif
      #if silicate_dust
        #pragma omp atomic
        dust_S_columns_esc[camera] += Ndot_weight_esc * rho_dust_S_dl; // Silicate column density [g/cm^2]
      #endif
    #else
      #pragma omp atomic
      dust_columns_esc[camera] += Ndot_weight_esc * rho_dust_dl; // Dust column density [g/cm^2]
    #endif
  }
  if (output_metal_columns_int)
    #pragma omp atomic
    metal_columns_int[camera] += Ndot_weight_int * rho_Z_dl; // Metal column density [g/cm^2]
  if (output_metal_columns_esc)
    #pragma omp atomic
    metal_columns_esc[camera] += Ndot_weight_esc * rho_Z_dl; // Metal column density [g/cm^2]
  if (output_HI_columns_int)
    #pragma omp atomic
    HI_columns_int[camera] += Ndot_weight_int * rho_HI_dl; // HI column density [g/cm^2]
  if (output_HI_columns_esc)
    #pragma omp atomic
    HI_columns_esc[camera] += Ndot_weight_esc * rho_HI_dl; // HI column density [g/cm^2]
}

/* Ray trace for escape calculations. */
void Escape::calculate_escape() {
  // Allocate local arrays (n_threads, n_bins)
  all_Ndot_int.resize(n_threads);            // Each thread has its own Ndot_int [photons/s]
  all_Ndot_esc.resize(n_threads);            // Each thread has its own Ndot_esc [photons/s]
  all_Ndot_abs.resize(n_threads);            // Each thread has its own Ndot_abs [photons/s]
  all_Ndot_ions.resize(n_threads);           // Each thread has its own Ndot_ions [photons/s]
  all_Ndot_weight_ions.resize(n_threads);    // Each thread has its own Ndot_weight_ions [photons/s]
  all_n_ions_abs.resize(n_threads);          // Each thread has its own n_ions_abs [cm^-3]
  all_sigma_ions.resize(n_threads);          // Each thread has its own sigma_ions [cm^2]
  all_kappas.resize(n_threads);              // Each thread has its own dust opacity [cm^2/g dust]
  all_k_abs.resize(n_threads);               // Each thread has its own k_abs [1/cm]
  all_k_dusts_abs.resize(n_threads);         // Each thread has its own k_dusts_abs [1/cm]
  all_k_ions_abs.resize(n_threads);          // Each thread has its own k_ions_abs [1/cm]
  all_dist_abs.resize(n_threads);            // Each thread has its own dist_abs [cm]
  #pragma omp parallel
  {
    all_Ndot_int[thread].resize(n_bins);     // Intrinsic bin photon rates [photons/s]
    all_Ndot_esc[thread].resize(n_bins);     // Escaped bin photon rates [photons/s]
    all_Ndot_abs[thread].resize(n_bins);     // Dust absorbed photon rates [photons/s]
    all_Ndot_ions[thread] = Image(n_active_ions, n_bins); // Ionization absorbed photon rates [photons/s]
    all_Ndot_weight_ions[thread].resize(n_active_ions); // Total ionization absorbed photon rates [photons/s]
    all_n_ions_abs[thread].resize(n_active_ions); // Ionization number densities [cm^-3]
    all_sigma_ions[thread] = Image(n_active_ions, n_bins); // Ionization cross sections [cm^2]
    for (int i = 0; i < n_dust_species; ++i)
      all_kappas[thread][i].resize(n_bins);  // Bin dust opacity [cm^2/g dust]
    all_k_abs[thread].resize(n_bins);        // Bin absorption coefficients [1/cm]
    for (int i = 0; i < n_dust_species; ++i)
      all_k_dusts_abs[i][thread].resize(n_bins); // Bin dust absorption coefficients [1/cm]
    all_k_ions_abs[thread] = Image(n_active_ions, n_bins); // Bin ionization absorption coefficients [1/cm]
    all_dist_abs[thread].resize(n_bins);     // Absorption distances [cm]
  }

  // Assign luminosity and opacity functions
  if (source_file_Z_age != "") {             // General (Z,age,bin) tables
    setup_spectra = setup_spectra_Z_age;
  } else if (source_model == "GMC") {        // RT is based on the GMC model
    setup_spectra = setup_spectra_GMC;
  } else if (source_model == "MRT") {        // RT is based on the MRT model
    root_error("MRT not implemented.");
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR") {
    setup_spectra = setup_spectra_imf_solar;
  } else if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    setup_spectra = setup_spectra_imf;
  }

  // Models with global cross-sections can be initialized once
  if (source_model == "GMC" || source_model == "MRT") {
    #pragma omp parallel for collapse(3)
    for (int i = 0; i < n_threads; ++i) {
      for (int ai = 0; ai < n_active_ions; ++ai) {
        for (int bin = 0; bin < n_bins; ++bin)
          all_sigma_ions[i](ai,bin) = global_sigma_ions[ai][bin]; // Global cross-sections
      }
    }
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR" || source_model == "BC03-IMF" ||
             source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    #pragma omp parallel
    {
      all_sigma_ions[thread](HeI_ION,0) = 0.; // These indices are always zero
      all_sigma_ions[thread](HeII_ION,0) = 0.;
      all_sigma_ions[thread](HeII_ION,1) = 0.;
    }
  }

  const size_t n_rays = n_stars * n_cameras; // Number of ray calculations
  const size_t interval = (n_rays < 200) ? 1 : n_rays / 100; // Interval between updates
  size_t n_finished = 0;                     // Progress for printing
  if (root)
    cout << "\n RT progress:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (size_t i = 0; i < n_rays; ++i) {
    // Ray tracing for each star and camera
    const int star = i / n_cameras;          // (star, camera) indices
    const int camera = i % n_cameras;
    ray_trace(star, camera);                 // Ray tracing calculations

    // Print completed progress
    if (root) {
      size_t i_finished;                     // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % interval == 0)
        cout << std::setw(3) << (100 * i_finished) / n_rays << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Convert observables to the correct units. */
void Escape::correct_units() {
  cout << "\n RT: Converting to physical units ..." << endl;

  // Escape fractions [fraction]
  if (output_escape_fractions) {
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      f_escs[camera] /= Ndot_tot;            // Normalize to total photon rate
  }
  if (output_bin_escape_fractions) {
    #pragma omp parallel for collapse(2)
    for (int camera = 0; camera < n_cameras; ++camera) {
      for (int bin = 0; bin < n_bins; ++bin)
        bin_f_escs[camera][bin] /= bin_Ndot_tot[bin]; // Normalize each bin
    }
  }

  // Dust absorptions [fraction]
  if (output_dust_absorptions) {
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      f_abss[camera] /= Ndot_tot;            // Normalize to total photon rate
  }
  if (output_bin_dust_absorptions) {
    #pragma omp parallel for collapse(2)
    for (int camera = 0; camera < n_cameras; ++camera) {
      for (int bin = 0; bin < n_bins; ++bin)
        bin_f_abss[camera][bin] /= bin_Ndot_tot[bin]; // Normalize each bin
    }
  }

  // Ion absorptions [fraction]
  if (output_ion_absorptions) {
    #pragma omp parallel for collapse(2)
    for (int ai = 0; ai < n_active_ions; ++ai) {
      for (int camera = 0; camera < n_cameras; ++camera)
        f_ions[ai][camera] /= Ndot_tot;      // Normalize to total photon rate
    }
  }
  if (output_bin_ion_absorptions) {
    #pragma omp parallel for collapse(3)
    for (int ai = 0; ai < n_active_ions; ++ai) {
      for (int camera = 0; camera < n_cameras; ++camera) {
        for (int bin = 0; bin < n_bins; ++bin)
          bin_f_ions[ai](camera,bin) /= bin_Ndot_tot[bin]; // Normalize each bin
      }
    }
  }

  // Average absorption distances [cm]
  if (output_absorption_distances) {
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      mean_dists[camera] /= Ndot_tot;        // Normalize to total photon rate
  }
  if (output_bin_absorption_distances) {
    #pragma omp parallel for collapse(2)
    for (int camera = 0; camera < n_cameras; ++camera) {
      for (int bin = 0; bin < n_bins; ++bin)
        bin_mean_dists(camera,bin) /= bin_Ndot_tot[bin]; // Normalize each bin
    }
  }

  // Gas column density [g/cm^2]
  if (output_gas_columns_int)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      gas_columns_int[camera] /= Ndot_tot;   // Normalize to total photon rate (intrinsic)
  if (output_gas_columns_esc)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      if (f_escs[camera] > 0.)
        gas_columns_esc[camera] /= Ndot_tot * f_escs[camera]; // Normalize to total photon rate (escaped)

  // Dust column density [g/cm^2]
  if (output_dust_columns_int)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera) {
      #if multiple_dust_species
        #if graphite_dust
          dust_G_columns_int[camera] /= Ndot_tot; // Normalize to total photon rate (intrinsic)
        #endif
        #if silicate_dust
          dust_S_columns_int[camera] /= Ndot_tot; // Normalize to total photon rate (intrinsic)
        #endif
      #else
        dust_columns_int[camera] /= Ndot_tot;  // Normalize to total photon rate (intrinsic)
      #endif
    }
  if (output_dust_columns_esc)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      if (f_escs[camera] > 0.) {
        #if multiple_dust_species
          #if graphite_dust
            dust_G_columns_esc[camera] /= Ndot_tot * f_escs[camera]; // Normalize to total photon rate (escaped)
          #endif
          #if silicate_dust
            dust_S_columns_esc[camera] /= Ndot_tot * f_escs[camera]; // Normalize to total photon rate (escaped)
          #endif
        #else
          dust_columns_esc[camera] /= Ndot_tot * f_escs[camera]; // Normalize to total photon rate (escaped)
        #endif
      }

  // Metal column density [g/cm^2]
  if (output_metal_columns_int)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      metal_columns_int[camera] /= Ndot_tot; // Normalize to total photon rate (intrinsic)
  if (output_metal_columns_esc)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      if (f_escs[camera] > 0.)
        metal_columns_esc[camera] /= Ndot_tot * f_escs[camera]; // Normalize to total photon rate (escaped)

  // HI column density [g/cm^2]
  if (output_HI_columns_int)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      HI_columns_int[camera] /= Ndot_tot; // Normalize to total photon rate (intrinsic)
  if (output_HI_columns_esc)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      if (f_escs[camera] > 0.)
        HI_columns_esc[camera] /= Ndot_tot * f_escs[camera]; // Normalize to total photon rate (escaped)

  // Surface brightness images [photons/s/cm^2]
  const double to_image_units = 1. / pixel_area; // L / dA
  const double to_cube_units = 1. / cube_pixel_area; // L / dA
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Bin SB images [photons/s/cm^2]
  if (output_cubes)
    rescale(bin_images, to_cube_units);      // Apply bin image correction

  // Set up radial image units
  vector<double> to_radial_image_units, to_radial_cube_units; // L / dA
  if (output_radial_images) {
    to_radial_image_units = vector<double>(n_radial_pixels);
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_pixels; ++ir)
      to_radial_image_units[ir] = 1. / radial_pixel_areas[ir];
  }
  if (output_radial_cubes) {
    to_radial_cube_units = vector<double>(n_radial_cube_pixels);
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_cube_pixels; ++ir)
      to_radial_cube_units[ir] = 1. / radial_cube_pixel_areas[ir];
  }

  // Radial surface brightness images [photons/s/cm^2]
  if (output_radial_images) {
    for (auto& radial_image : radial_images) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_pixels; ++ir)
        radial_image[ir] *= to_radial_image_units[ir];
    }
  }

  // Bin radial SB images [photons/s/cm^2]
  if (output_radial_cubes) {
    for (auto& bin_radial_image : bin_radial_images) {
      #pragma omp parallel for collapse(2)
      for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
        for (int bin = 0; bin < n_bins; ++bin)
          bin_radial_image(ir, bin) *= to_radial_cube_units[ir];
      }
    }
  }

  // Intrinsic cameras
  if (output_emission) {
    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Bin SB images [photons/s/cm^2]
    if (output_cubes)
      rescale(bin_images_int, to_image_units); // Apply bin image correction

    // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_images) {
      for (auto& radial_image_int : radial_images_int) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_pixels; ++ir)
          radial_image_int[ir] *= to_radial_image_units[ir];
      }
    }

    // Bin radial SB images [photons/s/cm^2]
    if (output_radial_cubes) {
      for (auto& bin_radial_image_int : bin_radial_images_int) {
        #pragma omp parallel for collapse(2)
        for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
          for (int bin = 0; bin < n_bins; ++bin)
            bin_radial_image_int(ir, bin) *= to_radial_cube_units[ir];
        }
      }
    }
  }
}

/* Driver for escape calculations. */
void Escape::run() {
  escape_timer.start();

  // Perform ray tracing escape calculations
  calculate_escape();

  // Collect the results from different processors
  // if (n_ranks > 1)
  //   reduce_escape();

  if (root) {
    // Convert observables to the correct units
    correct_units();
    print("f_esc (LOS)", f_escs);
    if (output_dust_absorptions)
      print("f_abs (LOS)", f_abss);
    if (output_ion_absorptions)
      for (int ai = 0; ai < n_active_ions; ++ai)
        print("f_" + ion_names[active_ions[ai]], f_ions[ai]);
    if (output_absorption_distances)
      print("dists (int)", mean_dists, "kpc", kpc);
    if (output_gas_columns_int)
      print("N_gas (int)", gas_columns_int, "g/cm^2");
    if (output_gas_columns_esc)
      print("N_gas (esc)", gas_columns_esc, "g/cm^2");
    if (output_dust_columns_int) {
      #if multiple_dust_species
        #if graphite_dust
          print("N_graphite (int)", dust_G_columns_int, "g/cm^2");
        #endif
        #if silicate_dust
          print("N_silicate (int)", dust_S_columns_int, "g/cm^2");
        #endif
      #else
        print("N_dust (int)", dust_columns_int, "g/cm^2");
      #endif
    }
    if (output_dust_columns_esc) {
      #if multiple_dust_species
        #if graphite_dust
          print("N_graphite (esc)", dust_G_columns_esc, "g/cm^2");
        #endif
        #if silicate_dust
          print("N_silicate (esc)", dust_S_columns_esc, "g/cm^2");
        #endif
      #else
        print("N_dust (esc)", dust_columns_esc, "g/cm^2");
      #endif
    }
    if (output_metal_columns_int)
      print("N_metal (int)", metal_columns_int, "g/cm^2");
    if (output_metal_columns_esc)
      print("N_metal (esc)", metal_columns_esc, "g/cm^2");
    if (output_HI_columns_int)
      print("N_HI (int)", HI_columns_int, "g/cm^2");
    if (output_HI_columns_esc)
      print("N_HI (esc)", HI_columns_esc, "g/cm^2");
  }
  escape_timer.stop();
}
