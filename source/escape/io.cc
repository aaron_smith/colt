/****************
 * escape/io.cc *
 ****************

 * All I/O operations related to the escape module.

*/

#include "../proto.h"
#include "Escape.h"
#include "../io_hdf5.h" // HDF5 read/write functions

dust_strings get_dust_tags(); // Dust species trailing tags

/* Function to print additional escape information. */
void Escape::print_info() {
  // Print field list
  cout << "\nEscaping RT information: [" << source_model << "]"
       << "\n  n_bins     = " << n_bins << endl;
  #if spherical_escape
    cout << "  r_escape   = " << escape_radius / kpc << " kpc\n"
         << "  r_emission = " << emission_radius / kpc << " kpc\n"
         << "  esc center = " << escape_center / kpc << " kpc" << endl;
  #endif
  #if box_escape
    cout << "  box emission = [" << emission_box[0] / kpc << ", " << emission_box[1] / kpc << "] kpc\n"
         << "  box escape = [" << escape_box[0] / kpc << ", " << escape_box[1] / kpc << "] kpc" << endl;
  #endif
  #if streaming_escape
    cout << "  streaming  = " << max_streaming / kpc << " kpc" << endl;
  #endif

  // Ionizing photon models
  for (int ai = 0; ai < n_active_ions; ++ai) {
    const int ion = active_ions[ai];
    print("sigma_" + ion_names[ion], global_sigma_ions[ion], "cm^2"); // Ionizing cross-sections
  }
  print("avg energy", global_mean_energy, "eV", eV);

  cout << "\nDust properties:" << (multiple_dust_species ? " [" : " ");
  for (int i = 0; i < n_dust_species; ++i) {
    if (i > 0) cout << ", ";
    if (dust_models[i] != "")
      cout << get_filename(dust_models[i]);
  }
  cout << (multiple_dust_species ? "] models" : " model") << endl;
  if (f_ion < 1.) cout << "  f_ion      = " << f_ion << endl;
  if (f_dust > 0.) cout << "  f_dust     = " << f_dust << endl;
  #if graphite_scaled_PAH
    cout << "  f_PAH      = " << f_PAH << endl;
  #endif
  if (T_sputter < 1e6) cout << "  T_sputter  = " << T_sputter << " K" << endl;
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    if (!tags[i].empty())
      cout << "\nMulti-species dust: " << tags[i].substr(1) << endl;
    print("kappas", global_kappas[i], "cm^2/g dust");
  }

  // Source information
  cout << "\nStrength of the source: [n_active_stars = " << n_stars << "]"
       << "\n  Luminosity = " << L_tot << " erg/s"
       << "\n             = " << L_tot / Lsun << " Lsun"
       << "\n  Prod. Rate = " << Ndot_tot << " photons/s" << endl;
  if (n_bins > 1) {
    print("Lum (bins)", bin_L_tot, "erg/s");
    print("          ", bin_L_tot, "Lsun", Lsun);
    print("Prod rates", bin_Ndot_tot, "photons/s");
  }

  print_cameras();                           // General camera parameters
}

/* Writes escape data and info to the specified hdf5 file. */
void Escape::write_module(const H5File& f) {
  // Simulation information
  write(f, "Ndot_tot", Ndot_tot);            // Total emission rate [photons/s]
  write(f, "L_tot", L_tot);                  // Total luminosity [erg/s]
  write(f, "n_bins", n_bins);                // Number of frequency bins
  write(f, "bin_Ndot_tot", bin_Ndot_tot, "photons/s"); // Bin rates [photons/s]
  write(f, "bin_L_tot", bin_L_tot, "erg/s"); // Bin luminosities [erg/s]
  {
    for (int ai = 0; ai < n_active_ions; ++ai) {
      const int ion = active_ions[ai];
      write(f, "sigma_" + ion_names[ion], global_sigma_ions[ion], "cm^2"); // Ionizing cross-sections
    }
    write(f, "mean_energy", global_mean_energy, "erg");
    Group g = exists(f, "/dust") ? f.openGroup("/dust") : f.createGroup("/dust");
    dust_strings tags = get_dust_tags();     // Dust species trailing tags
    for (int i = 0; i < n_dust_species; ++i)
      write(g, "kappa" + tags[i], global_kappas[i], "cm^2/g dust"); // Dust opacity [cm^2/g dust]
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    if (star_based_emission)
      write(g, "n_stars", n_stars);          // Number of stars

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    #if spherical_escape
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    #endif
    #if box_escape
      write(g, "escape_box_min", escape_box[0], "cm"); // Escape bounding box [cm]
      write(g, "escape_box_max", escape_box[1], "cm");
      write(g, "emission_box_min", emission_box[0], "cm"); // Emission bounding box [cm]
      write(g, "emission_box_max", emission_box[1], "cm");
    #endif
    #if streaming_escape
      write(g, "max_streaming", max_streaming); // Maximum streaming distance [cm]
    #endif
  }

  // Additional camera info
  if (have_cameras) {
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_bin_escape_fractions)
      write(f, "bin_f_escs", bin_f_escs);    // Bin escape fractions [fraction]
    if (output_dust_absorptions)
      write(f, "f_abss", f_abss);            // Dust absorptions [fraction]
    if (output_bin_dust_absorptions)
      write(f, "bin_f_abss", bin_f_abss);    // Bin dust absorptions  [fraction]
    if (output_ion_absorptions)
      for (int ai = 0; ai < n_active_ions; ++ai)
        write(f, "f_" + ion_names[active_ions[ai]], f_ions[ai]); // Ion absorptions [fraction]
    if (output_bin_ion_absorptions)
      for (int ai = 0; ai < n_active_ions; ++ai)
        write(f, "bin_f_" + ion_names[active_ions[ai]], bin_f_ions[ai]); // Bin ion absorptions [fraction]
    if (output_absorption_distances)
      write(f, "mean_dists", mean_dists, "cm"); // Average absorption distances [cm]
    if (output_bin_absorption_distances)
      write(f, "bin_mean_dists", bin_mean_dists, "cm"); // Average bin absorption distances [cm]
    if (output_images)
      write(f, "images", images, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
    if (output_cubes)
      write(f, "bin_images", bin_images, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
    if (output_radial_images)
      write(f, "radial_images", radial_images, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_cubes)
      write(f, "bin_radial_images", bin_radial_images, "photons/s/cm^2"); // Bin radial SB images [photons/s/cm^2]
    if (output_gas_columns_int)
      write(f, "gas_columns_int", gas_columns_int, "g/cm^2"); // Gas column densities [g/cm^2]
    if (output_gas_columns_esc)
      write(f, "gas_columns_esc", gas_columns_esc, "g/cm^2"); // Gas column densities [g/cm^2]
    if (output_dust_columns_int) {
      #if multiple_dust_species
        #if graphite_dust
          write(f, "dust_graphite_columns_int", dust_G_columns_int, "g/cm^2"); // Graphite column densities [g/cm^2]
        #endif
        #if silicate_dust
          write(f, "dust_silicate_columns_int", dust_S_columns_int, "g/cm^2"); // Silicate column densities [g/cm^2]
        #endif
      #else
        write(f, "dust_columns_int", dust_columns_int, "g/cm^2"); // Dust column densities [g/cm^2]
      #endif
    }
    if (output_dust_columns_esc) {
      #if multiple_dust_species
        #if graphite_dust
          write(f, "dust_graphite_columns_esc", dust_G_columns_esc, "g/cm^2"); // Graphite column densities [g/cm^2]
        #endif
        #if silicate_dust
          write(f, "dust_silicate_columns_esc", dust_S_columns_esc, "g/cm^2"); // Silicate column densities [g/cm^2]
        #endif
      #else
        write(f, "dust_columns_esc", dust_columns_esc, "g/cm^2"); // Dust column densities [g/cm^2]
      #endif
    }
    if (output_metal_columns_int)
      write(f, "metal_columns_int", metal_columns_int, "g/cm^2"); // Metal column densities [g/cm^2]
    if (output_metal_columns_esc)
      write(f, "metal_columns_esc", metal_columns_esc, "g/cm^2"); // Metal column densities [g/cm^2]
    if (output_HI_columns_int)
      write(f, "HI_columns_int", HI_columns_int, "g/cm^2"); // HI column densities [g/cm^2]
    if (output_HI_columns_esc)
      write(f, "HI_columns_esc", HI_columns_esc, "g/cm^2"); // HI column densities [g/cm^2]

    // Intrinsic emission
    if (output_emission) {
      if (output_images)
        write(f, "images_int", images_int, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
      if (output_cubes)
        write(f, "bin_images_int", bin_images_int, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
      if (output_radial_images)
        write(f, "radial_images_int", radial_images_int, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
      if (output_radial_cubes)
        write(f, "bin_radial_images_int", bin_radial_images_int, "photons/s/cm^2"); // Bin radial SB images [photons/s/cm^2]
    }
  }
}
