/********************
 * escape/config.cc *
 ********************

 * Post-processing escape module configuration.

*/

#include "../proto.h"
#include "Escape.h"
#include "../config.h" // Configuration functions

dust_strings get_dust_tags(); // Dust species trailing tags
void setup_Z_age_table(const string& source_file_Z_age, vector<double>& bin_edges_eV, const dust_strings& dust_models);
void setup_bc03_imf_solar_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bc03_imf_half_solar_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bc03_imf_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bpass_imf135_100_table(const string& dust_model); // Tabulated BPASS (135_100) spectral properties
void setup_bpass_chab_100_table(const string& dust_model); // Tabulated BPASS (chab_100) spectral properties
void ions_config(YAML::Node& file);          // Determine which ions to include
void setup_active_ions(const bool require_contiguous = true); // Setup active ionization states
void setup_bin_edges(YAML::Node& file);      // Determine bin energy edges
void setup_x_indices();                      // Setup ionization fraction indices

/* Escape module configuration. */
void Escape::module_config(YAML::Node& file) {
  // Set the dust properties: Opacity
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    const string dust_str = "dust_model" + tags[i];
    if (file[dust_str])
      load(dust_str, dust_models[i]);        // Dust model: SMC, MW, etc.
  }
  if constexpr (!multiple_dust_species) if (dust_models[0] == "") {
    load("kappa_HI", kappa_HI);              // Dust opacities [cm^2/g dust]
    load("kappa_HeI", kappa_HeI);
    load("kappa_HeII", kappa_HeII);
  }
  gas_config(file);                          // General gas configuration

  if (n_ranks > 1)
    root_error("Escape with n_ranks > 1 is not implemented yet.");

  // Internal simulation flags
  if (file["source_file_Z_age"]) {
    load("source_file_Z_age", source_file_Z_age); // File for the sources (Z,age,wavelength)
  } else {
    load("source_model", source_model);      // Model for the sources
  }
  read_electron_fraction = true;             // Generally requires electrons
  read_HI = read_HII = read_HeI = read_HeII = true; // Default H/He states
  read_helium = true;                        // Generally requires helium states
  bool variable_bins = (source_file_Z_age != "");
  if (variable_bins) {
    ions_config(file);                       // Determine which ions to include
    setup_active_ions();                     // Setup active ionization state information
    setup_bin_edges(file);                   // Determine bin energy edges
  } else {
    active_atoms = {H_ATOM, He_ATOM};        // Otherwise assume H/He atoms
    beg_ions = {HI_ION, HeI_ION};            // Begin with HI, HeI ions
    end_ions = {HI_ION + 1, HeI_ION + 2};    // End with HII, HeIII ions
    ion_counts = {1, 2};                     // Number of active ions
    active_ions = {HI_ION, HeI_ION, HeII_ION}; // Tracking HI, HeI, HeII ions
  }
  setup_x_indices();                         // Setup ionization fraction indices

  if (source_file_Z_age != "") {
    source_model = "file";                   // Source model comes from a file
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    read_Z_star = true;                      // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    setup_Z_age_table(source_file_Z_age, bin_edges_eV, dust_models); // Tabulated spectral properties
  } else if (source_model == "GMC") {
    star_based_emission = true;              // Star-based emission
    read_m_massive_star = true;              // Requires (massive) stellar masses
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    bin_names = {"HI", "HeI", "HeII", "max"}; // Bin energy edge names
    global_sigma_ions = vectors(n_active_ions, vector<double>(n_bins));
    global_sigma_ions[HI_ION] = {2.99466e-18, 5.68179e-19, 7.97294e-20}; // HI ionizing cross-sections
    global_sigma_ions[HeI_ION] = {0., 3.71495e-18, 5.70978e-19}; // HeI ionizing cross-sections
    global_sigma_ions[HeII_ION] = {0., 0., 1.06525e-18}; // HeII ionizing cross-sections
    global_mean_energy = {18.8567*eV, 35.0815*eV, 64.9768*eV}; // Mean energy of each bin [erg]
    if constexpr (multiple_dust_species)
      root_error("GMC source model only supports one dust species!");
  } else if (source_model == "MRT") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    read_Z_star = true;                      // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    bin_names = {"HI", "HeI", "HeII", "max"}; // Bin energy edge names
    global_sigma_ions = vectors(n_active_ions, vector<double>(n_bins));
    global_sigma_ions[HI_ION] = {3.3718e-18, 7.89957e-19, 1.09584e-19}; // HI ionizing cross-sections
    global_sigma_ions[HeI_ION] = {0., 5.10279e-18, 7.76916e-19}; // HeI ionizing cross-sections
    global_sigma_ions[HeII_ION] = {0., 0., 1.42413e-18}; // HeII ionizing cross-sections
    global_mean_energy = {18.0058*eV, 29.8868*eV, 56.8456*eV}; // Mean energy of each bin [erg]
    if constexpr (multiple_dust_species)
      root_error("MRT source model only supports one dust species!");
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR" || source_model == "BC03-IMF" ||
             source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100")
      read_Z_star = true;                    // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    bin_names = {"HI", "HeI", "HeII", "max"}; // Bin energy edge names
    if (source_model == "BC03-IMF-SOLAR")
      setup_bc03_imf_solar_table(dust_models[0]); // Tabulated BC03 spectral properties
    else if (source_model == "BC03-IMF-HALF-SOLAR")
      setup_bc03_imf_half_solar_table(dust_models[0]); // Tabulated BC03 spectral properties
    else if (source_model == "BC03-IMF")
      setup_bc03_imf_table(dust_models[0]);  // Tabulated BC03 spectral properties
    else if (source_model == "BPASS-IMF-135-100")
      setup_bpass_imf135_100_table(dust_models[0]); // Tabulated BPASS (135_100) spectral properties
    else // (source_model == "BPASS-CHAB-100")
      setup_bpass_chab_100_table(dust_models[0]); // Tabulated BPASS (chab_100) spectral properties
    if constexpr (multiple_dust_species)
      root_error("Pre-tabulated source models only supports one dust species!");
  } else if (source_model == "custom") {
    load("n_bins", n_bins, ">0");            // Number of frequency bins
    root_error("The custom source_model is not implemented yet!");
    // load("kappa_dust", kappa_dust, ">=0");   // Dust opacity [cm^2/g dust]
  } else if (source_model != "")
    root_error("Requested stellar source model is not implemented: " + source_model);

  // Setup the escape parameters
  escape_config(file);                       // General escape setup

  // Information about the cameras
  camera_config(file);                       // General camera setup
  if (!have_cameras)
    root_error("The escape module requires at least one camera "
               "but none were specified in " + config_file);
  if (n_bins == 1) output_bin_escape_fractions = false; // Reset default
  load("output_escape_fractions", output_escape_fractions); // Output camera escape fractions
  load("output_bin_escape_fractions", output_bin_escape_fractions); // Output bin escape fractions
  load("output_dust_absorptions", output_dust_absorptions); // Output camera dust absorptions
  load("output_bin_dust_absorptions", output_bin_dust_absorptions); // Output bin dust absorptions
  load("output_absorption_distances", output_absorption_distances); // Output absorption distances
  load("output_bin_absorption_distances", output_bin_absorption_distances); // Output bin absorption distances
  load("output_ion_absorptions", output_ion_absorptions); // Output all ion absorptions
  load("output_bin_ion_absorptions", output_bin_ion_absorptions); // Output bin absorption for all species
  output_images = false;                     // Reset default to false
  load("output_images", output_images);      // Output surface brightness images
  load("output_bin_images", output_cubes);   // Output bin SB images
  load("output_radial_images", output_radial_images); // Output radial surface brightness images
  load("output_bin_radial_images", output_radial_cubes); // Output bin radial SB images
  load("output_emission", output_emission);  // Output intrinsic emission without transport
  load("output_gas_columns_int", output_gas_columns_int); // Output camera gas column densities (intrinsic)
  load("output_gas_columns_esc", output_gas_columns_esc); // Output camera gas column densities (escaped)
  load("output_dust_columns_int", output_dust_columns_int); // Output camera dust column densities (intrinsic)
  load("output_dust_columns_esc", output_dust_columns_esc); // Output camera dust column densities (escaped)
  load("output_metal_columns_int", output_metal_columns_int); // Output camera metal column densities (intrinsic)
  load("output_metal_columns_esc", output_metal_columns_esc); // Output camera metal column densities (escaped)
  load("output_HI_columns_int", output_HI_columns_int); // Output camera HI column densities (intrinsic)
  load("output_HI_columns_esc", output_HI_columns_esc); // Output camera HI column densities (escaped)
  have_radial_cameras = (output_radial_images || output_radial_cubes);
  if (!(output_escape_fractions || output_bin_escape_fractions || output_dust_absorptions ||
        output_bin_dust_absorptions || output_ion_absorptions || output_bin_ion_absorptions ||
        output_absorption_distances || output_bin_absorption_distances || output_images || output_cubes ||
        have_radial_cameras || output_gas_columns_int || output_gas_columns_esc || output_dust_columns_int ||
        output_dust_columns_esc || output_metal_columns_int || output_metal_columns_esc || output_HI_columns_int || output_HI_columns_esc))
    root_error("The escape module requires at least one camera output type.");
  after_camera_config(file);                 // After camera setup
}
