/*******************
 * escape/Escape.h *
 *******************

 * Module declarations.

*/

#ifndef ESCAPE_INCLUDED
#define ESCAPE_INCLUDED

#include "../Simulation.h" // Base simulation class

/* Ray-based escape module. */
class Escape : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  // Ionizing photon models
  string source_model = "";                  // Escape source model
  vectors global_sigma_ions;                 // Ionization cross-sections [cm^2]
  vector<double> global_mean_energy;         // Mean energy of each frequency bin [erg]
  double L_tot = 0.;                         // Total luminosity [erg/s]
  double Ndot_tot = 0.;                      // Total emission rate [photons/s]
  vector<double> bin_L_tot;                  // Total bin luminosities [erg/s]
  vector<double> bin_Ndot_tot;               // Total bin rates [photons/s]
  vector<int> cell_of_star;                  // Cell index of each active star

  // Imformation about the dust model
  dust_vectors global_kappas;                // Dust opacity [cm^2/g dust]

  // Information about secondary cameras
  bool output_emission = false;              // Output intrinsic emission without transport
  bool output_bin_escape_fractions = true;   // Output bin escape fractions
  vectors bin_f_escs;                        // Bin escape fractions [fraction]
  bool output_dust_absorptions = true;       // Output camera dust absorptions
  vector<double> f_abss;                     // Dust absorptions [fraction]
  bool output_bin_dust_absorptions = false;  // Output bin dust absorptions
  vectors bin_f_abss;                        // Bin dust absorptions [fraction]
  bool output_ion_absorptions = true;        // Output camera absorption for all ionization species
  vectors f_ions;                            // Ion absorptions [fraction]
  bool output_bin_ion_absorptions = false;   // Output bin absorption for all ionization species
  Images bin_f_ions;                         // Bin ion absorptions [fraction]
  bool output_absorption_distances = true;   // Output absorption distances [cm]
  vector<double> mean_dists;                 // Average absorption distances [cm]
  bool output_bin_absorption_distances = false; // Output bin absorption distances [cm]
  Image bin_mean_dists;                      // Average bin absorption distances [cm]
  Images images_int;                         // Intrinsic surface brightness images [photons/s/cm^2]
  Cubes bin_images;                          // Escaped bin SB images [photons/s/cm^2]
  Cubes bin_images_int;                      // Intrinsic bin SB images [photons/s/cm^2]
  vectors radial_images_int;                 // Intrinsic radial surface brightness images [photons/s/cm^2]
  Images bin_radial_images;                  // Escaped bin radial SB images [photons/s/cm^2]
  Images bin_radial_images_int;              // Intrinsic bin radial SB images [photons/s/cm^2]
  bool output_gas_columns_int = true;        // Output gas column densities (intrinsic)
  vector<double> gas_columns_int;            // Gas column densities [g/cm^2]
  bool output_gas_columns_esc = true;        // Output gas column densities (escaped)
  vector<double> gas_columns_esc;            // Gas column densities [g/cm^2]
  bool output_dust_columns_int = true;       // Output dust column densities (intrinsic)
  bool output_dust_columns_esc = true;       // Output dust column densities (escaped)
  #if multiple_dust_species
    #if graphite_dust
      vector<double> dust_G_columns_int;     // Graphite column densities [g/cm^2]
      vector<double> dust_G_columns_esc;     // Graphite column densities [g/cm^2]
    #endif
    #if silicate_dust
      vector<double> dust_S_columns_int;     // Silicate column densities [g/cm^2]
      vector<double> dust_S_columns_esc;     // Silicate column densities [g/cm^2]
    #endif
  #else
    vector<double> dust_columns_int;         // Dust column densities [g/cm^2]
    vector<double> dust_columns_esc;         // Dust column densities [g/cm^2]
  #endif
  bool output_metal_columns_int = true;      // Output metal column densities (intrinsic)
  vector<double> metal_columns_int;          // Metal column densities [g/cm^2]
  bool output_metal_columns_esc = true;      // Output metal column densities (escaped)
  vector<double> metal_columns_esc;          // Metal column densities [g/cm^2]
  bool output_HI_columns_int = true;         // Output HI column densities (intrinsic)
  vector<double> HI_columns_int;             // HI column densities [g/cm^2]
  bool output_HI_columns_esc = true;         // Output HI column densities (escaped)
  vector<double> HI_columns_esc;             // HI column densities [g/cm^2]

  void ray_trace(const int star, const int camera); // Single ray
  void calculate_escape();                   // All stars and camera directions
  void correct_units();                      // Convert to observed units
};

#endif // ESCAPE_INCLUDED
