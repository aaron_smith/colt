/*****************
 * mpi_helpers.h *
 *****************

 * General MPI helper functions.

*/

#ifndef MPI_HELPERS_INCLUDED
#define MPI_HELPERS_INCLUDED

#include "proto.h"

/* Convenience function to MPI_Reduce double to root. */
static inline void mpi_reduce(double& val, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, &val, 1, MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(&val, &val, 1, MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce NDArray<N, double> to root. */
template <int N>
static inline void mpi_reduce(NDArray<N, double>& vec, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, vec.data(), vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(vec.data(), vec.data(), vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce NDArray<N, Vec3> to root. */
template <int N>
static inline void mpi_reduce(NDArray<N, Vec3>& vec, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, vec.data(), 3 * vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(vec.data(), vec.data(), 3 * vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce vector<double> to root. */
static inline void mpi_reduce(vector<double>& vec, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, vec.data(), vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(vec.data(), vec.data(), vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce vector<Vec3> to root. */
static inline void mpi_reduce(vector<Vec3>& vec, MPI_Op op = MPI_SUM) {
  if (root)
    MPI_Reduce(MPI_IN_PLACE, vec.data(), 3 * vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
  else
    MPI_Reduce(vec.data(), vec.data(), 3 * vec.size(), MPI_DOUBLE, op, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Reduce vector<class> to root. */
template <class T>
static inline void mpi_reduce(vector<T>& vecs, MPI_Op op = MPI_SUM) {
  for (auto& vec : vecs)
    mpi_reduce(vec, op);
}

/* Convenience function to MPI_Bcast vector from root. */
static inline void mpi_bcast(vector<double>& vec) {
  MPI_Bcast(vec.data(), vec.size(), MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
}

/* Convenience function to MPI_Bcast scalar from root. */
static inline void mpi_bcast(double& val) {
  MPI_Bcast(&val, 1, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
}

#endif // MPI_HELPERS_INCLUDED
