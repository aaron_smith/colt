/*************************
 * escape_cont/config.cc *
 *************************

 * Post-processing escape (continuum) module configuration.

*/

#include "../proto.h"
#include "EscapeCont.h"
#include "../config.h" // Configuration functions

dust_strings get_dust_tags(); // Dust species trailing tags
void set_line_and_dust_parameters_default(); // Line and dust properties (default)
void set_line_and_dust_parameters_derived(); // Line and dust properties (derived)
void setup_bc03_imf_table_Lcont(const string& line); // Tabulated BC03 spectral properties
void setup_bpass_imf135_100_table_Lcont(const string& line); // Tabulated BPASS (135-100) spectral properties
void setup_bpass_chab_100_table_Lcont(const string& line); // Tabulated BPASS (chab_100) spectral properties

/* Escape (continuum) module configuration. */
void EscapeCont::module_config(YAML::Node& file) {
  // Set the line and dust properties: Opacity, albedo, scattering anisotropy
  load("line", line);                        // Name of the line (default "Lyman-alpha")
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    const string dust_str = "dust_model" + tags[i];
    if (file[dust_str])
      load(dust_str, dust_models[i]);        // Dust model: SMC, MW, etc.
  }
  set_line_and_dust_parameters_default();    // Set default line/dust properties

  // Information about the dust model (allow overriding default model)
  incompatible("kappa_dust", "sigma_dust");  // Cannot specify both kappa_dust and sigma_dust
  if (dust_models[0] == "LAURSEN_SMC") {     // LAURSEN_SMC is a special case
    if (file["kappa_dust"])
      root_error("Cannot specify kappa_dust and dust_model = LAURSEN_SMC");
    if (file["sigma_dust"])
      root_error("Cannot specify sigma_dust and dust_model = LAURSEN_SMC");
  }
  load("kappa_dust", kappa_dust, ">=0");     // Dust opacity [cm^2/g dust]
  load("sigma_dust", sigma_dust, ">=0");     // Dust cross section [cm^2/Z/hydrogen atom]
  load("albedo", albedo, "[0,1]");           // Dust albedo for scattering vs. absorption
  load("g_dust", g_dust, "[-1,1]");          // Anisotropy parameter: <μ> for dust scattering
  if (fabs(g_dust) < 1e-6)
    g_dust = (g_dust < 0.) ? -1e-6 : 1e-6;   // Avoid division by zero
  gas_config(file);                          // General gas configuration
  set_line_and_dust_parameters_derived();    // Set derived properties after override

  // Setup stellar source and escape parameters
  escape_config(file);                       // General escape setup
  star_based_emission = read_HI = read_HII = true; // Include star-based sources and HI/HII states
  load("source_model", source_model);        // Model for the sources
  if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BC03-IMF" || source_model == "BPASS-CHAB-100") {
    read_m_init_star = true;                 // Requires initial stellar masses
    read_Z_star = true;                      // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
  } else if (source_model != "")
    root_error("Unrecognized value for source_model: " + source_model);
  if (source_model == "")
    read_continuum = true;                   // User provided continuum fluxes
  if (source_model == "BC03-IMF")
    setup_bc03_imf_table_Lcont(line);        // Tabulated BC03 spectral properties
  else if (source_model == "BPASS-IMF-135-100")
    setup_bpass_imf135_100_table_Lcont(line); // Tabulated BPASS (135-100) spectral properties
  else if (source_model == "BPASS-CHAB-100")
    setup_bpass_chab_100_table_Lcont(line);  // Tabulated BPASS (chab_100) spectral properties

  if (n_ranks > 1)
    root_error("Escape with n_ranks > 1 is not implemented yet.");

  // Information about the cameras
  camera_config(file);                       // General camera setup
  load("use_stellar_frame", use_stellar_frame); // Use stellar velocities to define the emission rest frame
  if (use_stellar_frame || shift_cameras_on_emission || align_cameras_on_emission)
    read_v_star = true;                      // Requires stellar velocities
  if (!have_cameras)
    root_error("The escape_cont module requires at least one camera "
               "but none were specified in " + config_file);
  load("output_absorption_distances", output_absorption_distances); // Output absorption distances
  output_images = false;                     // Reset default to false
  load("output_images", output_images);      // Output surface brightness images
  load("output_radial_images", output_radial_images); // Output radial surface brightness images
  load("output_emission", output_emission);  // Output intrinsic emission without transport
  after_camera_config(file);                 // After camera setup

  // Column densities
  load("output_gas_columns_int", output_gas_columns_int); // Output camera gas column densities (intrinsic)
  load("output_gas_columns_esc", output_gas_columns_esc); // Output camera gas column densities (escaped)
  load("output_metal_columns_int", output_metal_columns_int); // Output camera metal column densities (intrinsic)
  load("output_metal_columns_esc", output_metal_columns_esc); // Output camera metal column densities (escaped)
  load("output_H_columns_int", output_H_columns_int); // Output camera hydrogen column densities (intrinsic)
  load("output_H_columns_esc", output_H_columns_esc); // Output camera hydrogen column densities (escaped)
  load("output_HI_columns_int", output_HI_columns_int); // Output camera HI column densities (intrinsic)
  load("output_HI_columns_esc", output_HI_columns_esc); // Output camera HI column densities (escaped)
  load("output_carrier_columns_int", output_carrier_columns_int); // Output camera carrier column densities (intrinsic)
  load("output_carrier_columns_esc", output_carrier_columns_esc); // Output camera carrier column densities (escaped)
  load("output_lower_columns_int", output_lower_columns_int); // Output camera lower state column densities (intrinsic)
  load("output_lower_columns_esc", output_lower_columns_esc); // Output camera lower state column densities (escaped)

  // Velocities
  bool output_velocities = false;            // Default to false
  load("output_velocities", output_velocities); // Output camera velocities
  load("apply_red_filter", apply_red_filter); // Apply a red filter (v > 0)
  if (output_velocities) {                   // Reset all velocity defaults
    output_gas_velocities_int = output_gas_velocities_esc = true;
    output_metal_velocities_int = output_metal_velocities_esc = true;
    output_H_velocities_int = output_H_velocities_esc = true;
    output_HI_velocities_int = output_HI_velocities_esc = true;
    output_carrier_velocities_int = output_carrier_velocities_esc = true;
    output_lower_velocities_int = output_lower_velocities_esc = true;
  }
  load("output_gas_velocities_int", output_gas_velocities_int); // Output camera gas velocities (intrinsic)
  load("output_gas_velocities_esc", output_gas_velocities_esc); // Output camera gas velocities (escaped)
  load("output_metal_velocities_int", output_metal_velocities_int); // Output camera metal velocities (intrinsic)
  load("output_metal_velocities_esc", output_metal_velocities_esc); // Output camera metal velocities (escaped)
  load("output_H_velocities_int", output_H_velocities_int); // Output camera hydrogen velocities (intrinsic)
  load("output_H_velocities_esc", output_H_velocities_esc); // Output camera hydrogen velocities (escaped)
  load("output_HI_velocities_int", output_HI_velocities_int); // Output camera HI velocities (intrinsic)
  load("output_HI_velocities_esc", output_HI_velocities_esc); // Output camera HI velocities (escaped)
  load("output_carrier_velocities_int", output_carrier_velocities_int); // Output camera carrier velocities (intrinsic)
  load("output_carrier_velocities_esc", output_carrier_velocities_esc); // Output camera carrier velocities (escaped)
  load("output_lower_velocities_int", output_lower_velocities_int); // Output camera lower state velocities (intrinsic)
  load("output_lower_velocities_esc", output_lower_velocities_esc); // Output camera lower state velocities (escaped)

  // Velocities^2
  bool output_velocities2 = false;           // Default to false
  load("output_velocities2", output_velocities2); // Output camera velocities^2
  if (output_velocities2) {                  // Reset all velocity^2 defaults
    output_gas_velocities2_int = output_gas_velocities2_esc = true;
    output_metal_velocities2_int = output_metal_velocities2_esc = true;
    output_H_velocities2_int = output_H_velocities2_esc = true;
    output_HI_velocities2_int = output_HI_velocities2_esc = true;
    output_carrier_velocities2_int = output_carrier_velocities2_esc = true;
    output_lower_velocities2_int = output_lower_velocities2_esc = true;
  }
  load("output_gas_velocities2_int", output_gas_velocities2_int); // Output camera gas velocities^2 (intrinsic)
  load("output_gas_velocities2_esc", output_gas_velocities2_esc); // Output camera gas velocities^2 (escaped)
  load("output_metal_velocities2_int", output_metal_velocities2_int); // Output camera metal velocities^2 (intrinsic)
  load("output_metal_velocities2_esc", output_metal_velocities2_esc); // Output camera metal velocities^2 (escaped)
  load("output_H_velocities2_int", output_H_velocities2_int); // Output camera hydrogen velocities^2 (intrinsic)
  load("output_H_velocities2_esc", output_H_velocities2_esc); // Output camera hydrogen velocities^2 (escaped)
  load("output_HI_velocities2_int", output_HI_velocities2_int); // Output camera HI velocities^2 (intrinsic)
  load("output_HI_velocities2_esc", output_HI_velocities2_esc); // Output camera HI velocities^2 (escaped)
  load("output_carrier_velocities2_int", output_carrier_velocities2_int); // Output camera carrier velocities^2 (intrinsic)
  load("output_carrier_velocities2_esc", output_carrier_velocities2_esc); // Output camera carrier velocities^2 (escaped)
  load("output_lower_velocities2_int", output_lower_velocities2_int); // Output camera lower state velocities^2 (intrinsic)
  load("output_lower_velocities2_esc", output_lower_velocities2_esc); // Output camera lower state velocities^2 (escaped)

  // Output data dependencies
  const int carrier_index = line_carrier - Hydrogen_Line; // Relative index
  if (carrier_index == 0) {                  // Already output HI columns
    output_carrier_columns_int = output_carrier_columns_esc = false;
    output_lower_columns_int = output_lower_columns_esc = false;
    output_carrier_velocities_int = output_carrier_velocities_esc = false;
    output_lower_velocities_int = output_lower_velocities_esc = false;
    output_carrier_velocities2_int = output_carrier_velocities2_esc = false;
    output_lower_velocities2_int = output_lower_velocities2_esc = false;
  }
  // Velocities^2 require velocities
  if (output_gas_velocities2_int) output_gas_velocities_int = true;
  if (output_gas_velocities2_esc) output_gas_velocities_esc = true;
  if (output_metal_velocities2_int) output_metal_velocities_int = true;
  if (output_metal_velocities2_esc) output_metal_velocities_esc = true;
  if (output_H_velocities2_int) output_H_velocities_int = true;
  if (output_H_velocities2_esc) output_H_velocities_esc = true;
  if (output_HI_velocities2_int) output_HI_velocities_int = true;
  if (output_HI_velocities2_esc) output_HI_velocities_esc = true;
  if (output_carrier_velocities2_int) output_carrier_velocities_int = true;
  if (output_carrier_velocities2_esc) output_carrier_velocities_esc = true;
  if (output_lower_velocities2_int) output_lower_velocities_int = true;
  if (output_lower_velocities2_esc) output_lower_velocities_esc = true;
  // Velocities require column densities
  if (output_gas_velocities_int) output_gas_columns_int = true;
  if (output_gas_velocities_esc) output_gas_columns_esc = true;
  if (output_metal_velocities_int) output_metal_columns_int = true;
  if (output_metal_velocities_esc) output_metal_columns_esc = true;
  if (output_H_velocities_int) output_H_columns_int = true;
  if (output_H_velocities_esc) output_H_columns_esc = true;
  if (output_HI_velocities_int) output_HI_columns_int = true;
  if (output_HI_velocities_esc) output_HI_columns_esc = true;
  if (output_carrier_velocities_int) output_carrier_columns_int = true;
  if (output_carrier_velocities_esc) output_carrier_columns_esc = true;
  if (output_lower_velocities_int) output_lower_columns_int = true;
  if (output_lower_velocities_esc) output_lower_columns_esc = true;
  have_radial_cameras = output_radial_images; // Radial cameras are a subset of all cameras
}
