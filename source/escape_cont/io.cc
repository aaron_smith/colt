/*********************
 * escape_cont/io.cc *
 *********************

 * All I/O operations related to the escape (continuum) module.

*/

#include "../proto.h"
#include "EscapeCont.h"
#include "../io_hdf5.h" // HDF5 read/write functions

/* Function to print additional escape information. */
void EscapeCont::print_info() {
  // Print field list
  cout << "\nEscaping line continuum RT information: [" << source_model << "]" << endl;
  if (r_light != 0) {
    const Vec3 sigma_r = sqrt(r2_light - r_light * r_light); // Emission position standard deviation
    cout << "  r_light    = " << r_light / kpc << " kpc\n"
         << "  sigma(r)   = " << sigma_r / kpc << " kpc"
         << ", |sigma(r)| = " << norm(sigma_r) / kpc << " kpc" << endl;
  }
  if (v_light != 0) {
    const Vec3 sigma_v = sqrt(v2_light - v_light * v_light); // Emission velocity standard deviation
    cout << "  v_light    = " << v_light / km << " km/s\n"
         << "  sigma(v)   = " << sigma_v / km << " km/s"
         << ", |sigma(v)| = " << norm(sigma_v) / km << " km/s" << endl;
  }
  if (j_light != 0)
    cout << "  j_light    = " << j_light / (kpc * km) << " kpc km/s"
         << ", |j_light| = " << norm(j_light) / (kpc * km) << " kpc km/s\n"
         << "  j_hat      = " << unit_vector(j_light) << endl;
  #if spherical_escape
    cout << "  r_escape   = " << escape_radius / kpc << " kpc\n"
         << "  r_emission = " << emission_radius / kpc << " kpc\n"
         << "  esc center = " << escape_center / kpc << " kpc" << endl;
  #endif
  #if box_escape
    cout << "  box emission = [" << emission_box[0] / kpc << ", " << emission_box[1] / kpc << "] kpc\n"
         << "  box escape = [" << escape_box[0] / kpc << ", " << escape_box[1] / kpc << "] kpc" << endl;
  #endif
  #if streaming_escape
    cout << "  streaming  = " << max_streaming / kpc << " kpc" << endl;
  #endif
  if (apply_red_filter)
    cout << "  red filter = " << (apply_red_filter ? "yes" : "no") << endl;

  // Print line constants and parameters
  print_line_info();

  cout << "\nDust properties:" << (multiple_dust_species ? " [" : " ");
  for (int i = 0; i < n_dust_species; ++i) {
    if (i > 0) cout << ", ";
    if (dust_models[i] != "")
      cout << get_filename(dust_models[i]);
  }
  cout << (multiple_dust_species ? "] models" : " model") << endl;
  if (sigma_dust > 0.)
    cout << "  sigma_dust = " << sigma_dust << " cm^2/H (Z)" << endl;
  else
    cout << "  kappa_dust = " << kappa_dust << " cm^2/g (D)" << endl;
  if (kappa_dust > 0. || sigma_dust > 0.) {
    cout << "  albedo     = " << albedo
         << "\n  g_dust     = " << g_dust << endl;
    if (f_ion < 1.) cout << "  f_ion      = " << f_ion << endl;
    if (f_dust > 0.) cout << "  f_dust     = " << f_dust << endl;
    #if graphite_scaled_PAH
      cout << "  f_PAH      = " << f_PAH << endl;
    #endif
    if (T_sputter < 1e6) cout << "  T_sputter  = " << T_sputter << " K" << endl;
  }

  // Source information
  cout << "\nStrength of the source: [n_active_stars = " << n_stars << "]"
       << "\n  <n_stars>  = " << n_stars_eff
       << "\n  M_cont     = " << M_cont
       << "\n  Luminosity = " << L_tot << " erg/s/angstrom"
       << "\n             = " << L_tot / Lsun << " Lsun/angstrom"
       << "\n  Prod. Rate = " << Ndot_tot << " photons/s/angstrom" << endl;
  if (cosmological) {
    double flux_tot = L_tot / (4. * M_PI * d_L * d_L * (1. + z));
    cout << "  Flux (Int) = " << flux_tot << " erg/s/cm^2/angstrom" << endl;
  }

  print_cameras();                           // General camera parameters
}

/* Writes escape data and info to the specified hdf5 file. */
void EscapeCont::write_module(const H5File& f) {
  // Simulation information
  write(f, "Ndot_tot", Ndot_tot);            // Total emission rate [photons/s/angstrom]
  write(f, "L_tot", L_tot);                  // Total spactral luminosity [erg/s/angstrom]
  write(f, "apply_red_filter", apply_red_filter); // Apply redshift filter

  // Line data
  {
    Group g = f.createGroup("/line");
    write(g, "name", line);                  // Line name, e.g. Lyman-alpha
    write(g, "E0", E0);                      // Line energy [erg]
    write(g, "A21", A21);                    // Einstein A coefficient [Hz]
    write(g, "f12", f12);                    // Oscillator strength
    write(g, "g12", g12);                    // Degeneracy ratio: g_lower / g_upper
    write(g, "T0", T0);                      // Line temperature [K]
    write(g, "nu0", nu0);                    // Line frequency [Hz]
    write(g, "lambda0", lambda0);            // Line wavelength [angstrom]
    write(g, "B21", B21);                    // Einstein B21 = A21 c^2 / (2 h ν0^3)
    write(g, "DnuL", DnuL);                  // Natural frequency broadening [Hz]
    write(g, "m_carrier", m_carrier);        // Mass of carrier [g]
    // Doublet data
    if (E0p > 0.) {
      write(g, "E0p", E0p);                  // Line energy [erg]
      write(g, "A21p", A21p);                // Einstein A coefficient [Hz]
      write(g, "f12p", f12p);                // Oscillator strength
      write(g, "nu0p", nu0p);                // Line frequency [Hz]
      write(g, "lambda0p", lambda0p);        // Line wavelength [angstrom]
      write(g, "DnuLp", DnuLp);              // Natural frequency broadening [Hz]
    }
  }

  // Information about dust
  {
    Group g = exists(f, "/dust") ? f.openGroup("/dust") : f.createGroup("/dust");
    write(g, "kappa_dust", kappa_dust);      // Dust opacity [cm^2/g dust]
    write(g, "sigma_dust", sigma_dust);      // Dust cross section [cm^2/Z/hydrogen atom]
    write(g, "albedo", albedo);              // Dust albedo for scattering vs. absorption
    write(g, "g_dust", g_dust);              // Anisotropy parameter: <μ> for dust scattering
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    write(g, "n_stars", n_stars);            // Number of stars
    write(g, "n_stars_eff", n_stars_eff);    // Effective number of stars: 1/<w>
    write(g, "r_light", r_light, "cm");      // Center of luminosity position [cm]
    if (v_light != 0)
      write(g, "v_light", v_light, "cm/s");  // Center of luminosity velocity [cm/s]
    if (j_light != 0)
      write(g, "j_light", j_light, "cm^2/s"); // Center of luminosity angular momentum [cm^2/s]
    write(g, "L_stars", L_stars);            // Total L_stars [erg/s/angstrom]
    write(g, "L_cont", L_stars);             // Total L_stars [erg/s/angstrom]
    write(g, "M_cont", M_cont);              // Total M_cont [absolute magnitude]

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    #if spherical_escape
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    #endif
    #if box_escape
      write(g, "escape_box_min", escape_box[0], "cm"); // Escape bounding box [cm]
      write(g, "escape_box_max", escape_box[1], "cm");
      write(g, "emission_box_min", emission_box[0], "cm"); // Emission bounding box [cm]
      write(g, "emission_box_max", emission_box[1], "cm");
    #endif
    #if streaming_escape
      write(g, "max_streaming", max_streaming); // Maximum streaming distance [cm]
    #endif
  }

  // Additional camera info
  if (have_cameras) {
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_absorption_distances)
      write(f, "mean_dists", mean_dists, "cm"); // Average absorption distances [cm]
    if (output_images)
      write(f, "images", images, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
    if (output_radial_images)
      write(f, "radial_images", radial_images, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
    if (output_gas_columns_int)
      write(f, "gas_columns_int", gas_columns_int, "g/cm^2"); // Gas column densities [g/cm^2]

    // Column densities
    if (output_gas_columns_esc)
      write(f, "gas_columns_esc", gas_columns_esc, "g/cm^2"); // Gas column densities [g/cm^2]
    if (output_metal_columns_int)
      write(f, "metal_columns_int", metal_columns_int, "g/cm^2"); // Metal column densities [g/cm^2]
    if (output_metal_columns_esc)
      write(f, "metal_columns_esc", metal_columns_esc, "g/cm^2"); // Metal column densities [g/cm^2]
    if (output_H_columns_int)
      write(f, "H_columns_int", H_columns_int, "cm^-2"); // Hydrogen column densities [cm^-2]
    if (output_H_columns_esc)
      write(f, "H_columns_esc", H_columns_esc, "cm^-2"); // Hydrogen column densities [cm^-2]
    if (output_HI_columns_int)
      write(f, "HI_columns_int", HI_columns_int, "cm^-2"); // HI column densities [cm^-2]
    if (output_HI_columns_esc)
      write(f, "HI_columns_esc", HI_columns_esc, "cm^-2"); // HI column densities [cm^-2]
    string atom = atom_symbols[line_carrier - Hydrogen_Line]; // Atom symbol (e.g. H)
    if (output_carrier_columns_int)
      write(f, atom+"_columns_int", carrier_columns_int, "cm^-2"); // Carrier column densities [cm^-2]
    if (output_carrier_columns_esc)
      write(f, atom+"_columns_esc", carrier_columns_esc, "cm^-2"); // Carrier column densities [cm^-2]
    string ion = fields[line_type].name.substr(2); // Line ionization state symbol (e.g. HI)
    if (output_lower_columns_int)
      write(f, ion+"_columns_int", lower_columns_int, "cm^-2"); // Lower state column densities [cm^-2]
    if (output_lower_columns_esc)
      write(f, ion+"_columns_esc", lower_columns_esc, "cm^-2"); // Lower state column densities [cm^-2]

    // Velocities
    if (output_gas_velocities_int)
      write(f, "gas_velocities_int", gas_velocities_int, "cm/s"); // Gas velocities [cm/s]
    if (output_gas_velocities_esc)
      write(f, "gas_velocities_esc", gas_velocities_esc, "cm/s"); // Gas velocities [cm/s]
    if (output_metal_velocities_int)
      write(f, "metal_velocities_int", metal_velocities_int, "cm/s"); // Metal velocities [cm/s]
    if (output_metal_velocities_esc)
      write(f, "metal_velocities_esc", metal_velocities_esc, "cm/s"); // Metal velocities [cm/s]
    if (output_H_velocities_int)
      write(f, "H_velocities_int", H_velocities_int, "cm/s"); // Hydrogen velocities [cm/s]
    if (output_H_velocities_esc)
      write(f, "H_velocities_esc", H_velocities_esc, "cm/s"); // Hydrogen velocities [cm/s]
    if (output_HI_velocities_int)
      write(f, "HI_velocities_int", HI_velocities_int, "cm/s"); // HI velocities [cm/s]
    if (output_HI_velocities_esc)
      write(f, "HI_velocities_esc", HI_velocities_esc, "cm/s"); // HI velocities [cm/s]
    if (output_carrier_velocities_int)
      write(f, atom+"_velocities_int", carrier_velocities_int, "cm/s"); // Carrier velocities [cm/s]
    if (output_carrier_velocities_esc)
      write(f, atom+"_velocities_esc", carrier_velocities_esc, "cm/s"); // Carrier velocities [cm/s]
    if (output_lower_velocities_int)
      write(f, ion+"_velocities_int", lower_velocities_int, "cm/s"); // Lower state velocities [cm/s]
    if (output_lower_velocities_esc)
      write(f, ion+"_velocities_esc", lower_velocities_esc, "cm/s"); // Lower state velocities [cm/s]

    // Velocities^2
    if (output_gas_velocities2_int)
      write(f, "gas_velocities2_int", gas_velocities2_int, "cm^2/s^2"); // Gas velocities^2 [cm^2/s^2]
    if (output_gas_velocities2_esc)
      write(f, "gas_velocities2_esc", gas_velocities2_esc, "cm^2/s^2"); // Gas velocities^2 [cm^2/s^2]
    if (output_metal_velocities2_int)
      write(f, "metal_velocities2_int", metal_velocities2_int, "cm^2/s^2"); // Metal velocities^2 [cm^2/s^2]
    if (output_metal_velocities2_esc)
      write(f, "metal_velocities2_esc", metal_velocities2_esc, "cm^2/s^2"); // Metal velocities^2 [cm^2/s^2]
    if (output_H_velocities2_int)
      write(f, "H_velocities2_int", H_velocities2_int, "cm^2/s^2"); // Hydrogen velocities^2 [cm^2/s^2]
    if (output_H_velocities2_esc)
      write(f, "H_velocities2_esc", H_velocities2_esc, "cm^2/s^2"); // Hydrogen velocities^2 [cm^2/s^2]
    if (output_HI_velocities2_int)
      write(f, "HI_velocities2_int", HI_velocities2_int, "cm^2/s^2"); // HI velocities^2 [cm^2/s^2]
    if (output_HI_velocities2_esc)
      write(f, "HI_velocities2_esc", HI_velocities2_esc, "cm^2/s^2"); // HI velocities^2 [cm^2/s^2]
    if (output_carrier_velocities2_int)
      write(f, atom+"_velocities2_int", carrier_velocities2_int, "cm^2/s^2"); // Carrier velocities^2 [cm^2/s^2]
    if (output_carrier_velocities2_esc)
      write(f, atom+"_velocities2_esc", carrier_velocities2_esc, "cm^2/s^2"); // Carrier velocities^2 [cm^2/s^2]
    if (output_lower_velocities2_int)
      write(f, ion+"_velocities2_int", lower_velocities2_int, "cm^2/s^2"); // Lower state velocities^2 [cm^2/s^2]
    if (output_lower_velocities2_esc)
      write(f, ion+"_velocities2_esc", lower_velocities2_esc, "cm^2/s^2"); // Lower state velocities^2 [cm^2/s^2]

    // Intrinsic emission
    if (output_emission) {
      if (output_images)
        write(f, "images_int", images_int, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
      if (output_radial_images)
        write(f, "radial_images_int", radial_images_int, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
    }
  }
}
