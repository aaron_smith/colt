/*****************************
 * escape_cont/initialize.cc *
 *****************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "EscapeCont.h"


// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

bool avoid_cell(const int cell); // Avoid calculations for certain cells
int find_cell(const Vec3 point, int cell); // Cell index of a point
bool avoid_star_host_halo(const int star); // Check if a star is not in a valid group

// Avoid stars that are outside the emission region
static inline bool invalid_star_sphere(const int i) {
  #if spherical_escape
    return (r_star[i] - escape_center).dot() >= emission_radius2;
  #else
    return false;
  #endif
};
static inline bool invalid_star_box(const int i) {
  #if box_escape
    return (r_star[i].x < emission_box[0].x || r_star[i].x > emission_box[1].x ||
            r_star[i].y < emission_box[0].y || r_star[i].y > emission_box[1].y ||
            r_star[i].z < emission_box[0].z || r_star[i].z > emission_box[1].z);
  #else
    return false;
  #endif
};
static inline bool invalid_star(const int i) {
  return invalid_star_sphere(i) || invalid_star_box(i);
};

/* Setup everything for the escape (continuum) module. */
void EscapeCont::setup() {
  // Setup general escape
  if constexpr (check_escape)
    setup_escape();                          // General escape setup

  // Setup cameras
  if (have_cameras) {
    setup_cameras();                         // General camera setup

    // Reset camera data and initialize with zeros
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_absorption_distances)
      mean_dists = vector<double>(n_cameras); // Average absorption distances [cm]
    if (output_images)
      images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_radial_images)
      radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));

    // Column densities
    if (output_gas_columns_int)
      gas_columns_int = vector<double>(n_cameras); // Gas column densities [g/cm^2]
    if (output_gas_columns_esc)
      gas_columns_esc = vector<double>(n_cameras); // Gas column densities [g/cm^2]
    if (output_metal_columns_int)
      metal_columns_int = vector<double>(n_cameras); // Metal column densities [g/cm^2]
    if (output_metal_columns_esc)
      metal_columns_esc = vector<double>(n_cameras); // Metal column densities [g/cm^2]
    if (output_H_columns_int)
      H_columns_int = vector<double>(n_cameras); // Hydrogen column densities [cm^-2]
    if (output_H_columns_esc)
      H_columns_esc = vector<double>(n_cameras); // Hydrogen column densities [cm^-2]
    if (output_HI_columns_int)
      HI_columns_int = vector<double>(n_cameras); // HI column densities [cm^-2]
    if (output_HI_columns_esc)
      HI_columns_esc = vector<double>(n_cameras); // HI column densities [cm^-2]
    if (output_carrier_columns_int)
      carrier_columns_int = vector<double>(n_cameras); // Carrier column densities [cm^-2]
    if (output_carrier_columns_esc)
      carrier_columns_esc = vector<double>(n_cameras); // Carrier column densities [cm^-2]
    if (output_lower_columns_int)
      lower_columns_int = vector<double>(n_cameras); // Lower state column densities [cm^-2]
    if (output_lower_columns_esc)
      lower_columns_esc = vector<double>(n_cameras); // Lower state column densities [cm^-2]

    // Velocities
    if (output_gas_velocities_int)
      gas_velocities_int = vector<double>(n_cameras); // Gas velocities [cm/s]
    if (output_gas_velocities_esc)
      gas_velocities_esc = vector<double>(n_cameras); // Gas velocities [cm/s]
    if (output_metal_velocities_int)
      metal_velocities_int = vector<double>(n_cameras); // Metal velocities [cm/s]
    if (output_metal_velocities_esc)
      metal_velocities_esc = vector<double>(n_cameras); // Metal velocities [cm/s]
    if (output_H_velocities_int)
      H_velocities_int = vector<double>(n_cameras); // Hydrogen velocities [cm/s]
    if (output_H_velocities_esc)
      H_velocities_esc = vector<double>(n_cameras); // Hydrogen velocities [cm/s]
    if (output_HI_velocities_int)
      HI_velocities_int = vector<double>(n_cameras); // HI velocities [cm/s]
    if (output_HI_velocities_esc)
      HI_velocities_esc = vector<double>(n_cameras); // HI velocities [cm/s]
    if (output_carrier_velocities_int)
      carrier_velocities_int = vector<double>(n_cameras); // Carrier velocities [cm/s]
    if (output_carrier_velocities_esc)
      carrier_velocities_esc = vector<double>(n_cameras); // Carrier velocities [cm/s]
    if (output_lower_velocities_int)
      lower_velocities_int = vector<double>(n_cameras); // Lower state velocities [cm/s]
    if (output_lower_velocities_esc)
      lower_velocities_esc = vector<double>(n_cameras); // Lower state velocities [cm/s]

    // Velocities^2
    if (output_gas_velocities2_int)
      gas_velocities2_int = vector<double>(n_cameras); // Gas velocities^2 [cm^2/s^2]
    if (output_gas_velocities2_esc)
      gas_velocities2_esc = vector<double>(n_cameras); // Gas velocities^2 [cm^2/s^2]
    if (output_metal_velocities2_int)
      metal_velocities2_int = vector<double>(n_cameras); // Metal velocities^2 [cm^2/s^2]
    if (output_metal_velocities2_esc)
      metal_velocities2_esc = vector<double>(n_cameras); // Metal velocities^2 [cm^2/s^2]
    if (output_H_velocities2_int)
      H_velocities2_int = vector<double>(n_cameras); // Hydrogen velocities^2 [cm^2/s^2]
    if (output_H_velocities2_esc)
      H_velocities2_esc = vector<double>(n_cameras); // Hydrogen velocities^2 [cm^2/s^2]
    if (output_HI_velocities2_int)
      HI_velocities2_int = vector<double>(n_cameras); // HI velocities^2 [cm^2/s^2]
    if (output_HI_velocities2_esc)
      HI_velocities2_esc = vector<double>(n_cameras); // HI velocities^2 [cm^2/s^2]
    if (output_carrier_velocities2_int)
      carrier_velocities2_int = vector<double>(n_cameras); // Carrier velocities^2 [cm^2/s^2]
    if (output_carrier_velocities2_esc)
      carrier_velocities2_esc = vector<double>(n_cameras); // Carrier velocities^2 [cm^2/s^2]
    if (output_lower_velocities2_int)
      lower_velocities2_int = vector<double>(n_cameras); // Lower state velocities^2 [cm^2/s^2]
    if (output_lower_velocities2_esc)
      lower_velocities2_esc = vector<double>(n_cameras); // Lower state velocities^2 [cm^2/s^2]

    // Initialize intrinsic cameras
    if (output_emission) {
      if (output_images)
        images_int = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_radial_images)
        radial_images_int = vectors(n_cameras, vector<double>(n_radial_pixels));
    }
  }

  // Setup star-based emission sources
  vector<int> active_flags(n_stars);         // Set active star flags
  cell_of_star.resize(n_stars);              // Cell index of each active star
  Vec3 Lr = 0, Lv = 0, Lrxv = 0;             // Center of light position, velocity, and angular momentum
  Vec3 Lr2 = 0, Lv2 = 0;                     // Center of light position^2 and velocity^2
  double L2_stars = 0.;                      // Effective number of stars statistic
  if (source_model == "") {
    #pragma omp parallel for reduction(+:L_stars, L2_stars) reduction(+:Lr, Lr2, Lv, Lv2)
    for (int i = 0; i < n_stars; ++i) {
      if (invalid_star(i))                   // Avoid stars that are outside the emission region
        continue;                            // Check before accessing cell_of_star
      cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
      if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
        continue;                            // Ignore certain cells or groups
      active_flags[i] = 1;                   // Keep track of active stars
      const double L_star = L_cont_star[i];  // Line continuum spectral luminosity [erg/s/anstrom]
      L_stars += L_star;                     // Add to total spectral luminosity
      L2_stars += L_star * L_star;           // Effective number of stars statistic
      Lr += L_star * r_star[i];              // Center of spectral luminosity position [cm erg/s/anstrom]
      Lr2 += L_star * r_star[i] * r_star[i]; // Center of spectral luminosity position^2 [cm^2 erg/s/anstrom]
      if (read_v_star) {
        Lv += L_star * v_star[i];            // Center of spectral luminosity velocity [cm/s erg/s/anstrom]
        Lv2 += L_star * v_star[i] * v_star[i]; // Center of spectral luminosity velocity^2 [cm^2/s^2 erg/s/anstrom]
      }
    }
    if (align_cameras_on_emission) {
      r_light = Lr / L_stars;                // Center of light position [cm]
      v_light = Lv / L_stars;                // Center of light velocity [cm/s]
      #pragma omp parallel for reduction(+:Lrxv)
      for (int i = 0; i < n_stars; ++i) {
        if (invalid_star(i))                 // Avoid stars that are outside the emission region
          continue;                          // Check before accessing cell_of_star
        if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
          continue;                          // Ignore certain cells or groups
        const double L_star = L_cont_star[i]; // Line continuum spectral luminosity [erg/s/anstrom]
        const Vec3 dr = r_star[i] - r_light; // Position relative to center of light [cm]
        const Vec3 dv = v_star[i] - v_light; // Velocity relative to center of light [cm/s]
        Lrxv += L_star * cross(dr, dv);      // Center of spectral luminosity angular momentum [cm^2/s erg/s/anstrom]
      }
    }
  } else {                                   // Tabulated by age and metallicity
    read_continuum = true;                   // Save line continuum spectral luminosities
    L_cont_star.resize(n_stars);             // Line continuum spectral luminosities [erg/s/anstrom]
    #pragma omp parallel for reduction(+:L_stars, L2_stars) reduction(+:Lr, Lr2, Lv, Lv2)
    for (int i = 0; i < n_stars; ++i) {
      if (invalid_star(i))                   // Avoid stars that are outside the emission region
        continue;                            // Check before accessing cell_of_star
      cell_of_star[i] = find_cell(r_star[i], 0); // Search for the cell
      if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
        continue;                            // Ignore certain cells or groups
      active_flags[i] = 1;                   // Keep track of active stars
      const double mass = m_init_star[i];    // Initial mass of the star [Msun]
      const double Z = Z_star[i];            // Metallicity of the star
      const double age = age_star[i];        // Age of the star [Gyr]

      int i_L_Z = 0, i_L_age = 0;            // Lower interpolation index
      double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
      if (Z >= 4e-2) {                       // Metallicity maximum = 0.04
        i_L_Z = 11;                          // 13 metallicity bins
        frac_R_Z = 1.;
      } else if (Z > 1e-5) {                 // Metallicity minimum = 10^-5
        const double logZ = log10(Z);        // Interpolate in log space
        while (logZ > logZ_BP[i_L_Z+1])
          i_L_Z++;                           // Search metallicity indices
        frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
      }
      if (age >= 100.) {                     // Age maximum = 100 Gyr
        i_L_age = 49;                        // 51 age bins
        frac_R_age = 1.;
      } else if (age > 1e-3) {               // Age minimum = 1 Myr
        const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
        const double f_age = floor(d_age);   // Floor coordinate
        frac_R_age = d_age - f_age;          // Interpolation fraction (right)
        i_L_age = int(f_age);                // Table age index (left)
      }

      // Bilinear interpolation (based on left and right fractions)
      const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
      const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
      const double L_star = mass             // Mass conversion [erg/s/anstrom]
        * pow(10., (log_Lcont_Z_age[i_L_Z][i_L_age]*frac_L_Z + log_Lcont_Z_age[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                 + (log_Lcont_Z_age[i_L_Z][i_R_age]*frac_L_Z + log_Lcont_Z_age[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      L_cont_star[i] = L_star;               // Save for later use
      L_stars += L_star;                     // Add to total spectral luminosity
      L2_stars += L_star * L_star;           // Effective number of stars statistic
      Lr += L_star * r_star[i];              // Center of spectral luminosity position [cm erg/s/anstrom]
      Lr2 += L_star * r_star[i] * r_star[i]; // Center of spectral luminosity position^2 [cm^2 erg/s/anstrom]
      if (read_v_star) {
        Lv += L_star * v_star[i];            // Center of spectral luminosity velocity [cm/s erg/s/anstrom]
        Lv2 += L_star * v_star[i] * v_star[i]; // Center of spectral luminosity velocity^2 [cm^2/s^2 erg/s/anstrom]
      }
    }
    if (align_cameras_on_emission) {
      r_light = Lr / L_stars;                // Center of light position [cm]
      v_light = Lv / L_stars;                // Center of light velocity [cm/s]
      #pragma omp parallel for reduction(+:Lrxv)
      for (int i = 0; i < n_stars; ++i) {
        if (invalid_star(i))                 // Avoid stars that are outside the emission region
          continue;                          // Check before accessing cell_of_star
        if (avoid_cell(cell_of_star[i]) || avoid_star_host_halo(i))
          continue;                          // Ignore certain cells or groups
        const double mass = m_init_star[i];  // Initial mass of the star [Msun]
        const double Z = Z_star[i];          // Metallicity of the star
        const double age = age_star[i];      // Age of the star [Gyr]

        int i_L_Z = 0, i_L_age = 0;          // Lower interpolation index
        double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
        if (Z >= 4e-2) {                     // Metallicity maximum = 0.04
          i_L_Z = 11;                        // 13 metallicity bins
          frac_R_Z = 1.;
        } else if (Z > 1e-5) {               // Metallicity minimum = 10^-5
          const double logZ = log10(Z);      // Interpolate in log space
          while (logZ > logZ_BP[i_L_Z+1])
            i_L_Z++;                         // Search metallicity indices
          frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
        }
        if (age >= 100.) {                   // Age maximum = 100 Gyr
          i_L_age = 49;                      // 51 age bins
          frac_R_age = 1.;
        } else if (age > 1e-3) {             // Age minimum = 1 Myr
          const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
          const double f_age = floor(d_age); // Floor coordinate
          frac_R_age = d_age - f_age;        // Interpolation fraction (right)
          i_L_age = int(f_age);              // Table age index (left)
        }

        // Bilinear interpolation (based on left and right fractions)
        const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
        const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
        const double L_star = mass           // Mass conversion [erg/s/anstrom]
          * pow(10., (log_Lcont_Z_age[i_L_Z][i_L_age]*frac_L_Z + log_Lcont_Z_age[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (log_Lcont_Z_age[i_L_Z][i_R_age]*frac_L_Z + log_Lcont_Z_age[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        const Vec3 dr = r_star[i] - r_light; // Position relative to center of light [cm]
        const Vec3 dv = v_star[i] - v_light; // Velocity relative to center of light [cm/s]
        Lrxv += L_star * cross(dr, dv);      // Center of spectral luminosity angular momentum [cm^2/s erg/s/anstrom]
      }
    }
  }

  if (L_stars <= 0.)
    root_error("Total stellar luminosity must be positive");
  const double lambda_cont = lambda0 * angstrom; // Continuum wavelength [cm]
  constexpr double R_10pc = 10. * pc;        // Reference distance for continuum [cm]
  const double fnu_cont_fac = lambda_cont * lambda_cont / (4. * M_PI * c * R_10pc * R_10pc * angstrom);
  M_cont = -2.5 * log10(fnu_cont_fac * L_stars) - 48.6; // Continuum absolute magnitude
  L_tot = L_stars;                           // Total spectral luminosity [erg/s/anstrom]
  Ndot_tot = L_tot / E0;                     // Total emission rate [photons/s/angstrom]
  n_stars_eff = L_stars * L_stars / L2_stars; // Effective number of stars: <L>^2/<L^2>
  r_light = Lr / L_tot;                      // Center of light position [cm]
  r2_light = Lr2 / L_tot;                    // Center of light position^2 [cm^2]
  if (read_v_star) {
    v_light = Lv / L_tot;                    // Center of light velocity [cm/s]
    v2_light = Lv2 / L_tot;                  // Center of light velocity^2 [cm^2/s^2]
  }
  if (focus_cameras_on_emission)
    camera_center = r_light;                 // Focus cameras on emission (position)
  if (shift_cameras_on_emission)
    camera_motion = v_light;                 // Shift cameras on emission (velocity)
  if (align_cameras_on_emission) {
    // Rotate the camera axes to align with the angular momentum vector
    // See: https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
    // First we define v = cross(a,b) and c = dot(a,b) then we have
    // Rodrigues rotation formula: p' = p + cross(v,p) + cross(v,cross(v,p)) / (1 + c)
    // Here a = j_hat, b = camera_north, and p = camera_directions, camera_xaxes, camera_yaxes
    j_light = Lrxv / L_tot;                  // Center of light angular momentum [cm^2/s]
    const Vec3 j_hat = unit_vector(j_light); // Angular momentum (unit vector)
    const Vec3 jv = cross(j_hat, camera_north); // Perpendicular to j_hat and camera_north
    const double jc = dot(j_hat, camera_north); // cos(theta) = a * b
    const double factor = 1. / (1. + jc);    // Convenience: 1 / (1 + cos(theta))
    camera_north = j_hat;                    // Align camera north with angular momentum
    #pragma omp parallel for
    for (int i = 0; i < n_cameras; ++i) {
      const Vec3 vpx = cross(jv, camera_xaxes[i]); // vp = cross(v,p)
      const Vec3 vpy = cross(jv, camera_yaxes[i]);
      const Vec3 vpz = cross(jv, camera_directions[i]);
      camera_xaxes[i] += vpx + factor * cross(jv, vpx); // p' = p + vp + vvp / (1 + c)
      camera_yaxes[i] += vpy + factor * cross(jv, vpy);
      camera_directions[i] += vpz + factor * cross(jv, vpz);
    }
  }

  // Filter star data
  int n_active = 0;                          // Number of actively emitting stars
  const bool read_group_id_star = !group_id_star.empty(); // Group ID
  const bool read_subhalo_id_star = !subhalo_id_star.empty(); // Subhalo ID
  for (int i = 0; i < n_stars; ++i)
    if (active_flags[i]) {
      if (i != n_active) {
        r_star[n_active] = r_star[i];        // Move active stars to the front
        if (read_v_star)                     // Stellar velocities [cm/s]
          v_star[n_active] = v_star[i];
        if (read_m_massive_star)             // Massive stellar masses
          m_massive_star[n_active] = m_massive_star[i];
        if (read_m_init_star)                // Initial stellar masses
          m_init_star[n_active] = m_init_star[i];
        if (read_Z_star)                     // Stellar metallicities
          Z_star[n_active] = Z_star[i];
        if (read_age_star)                   // Stellar ages [Gyr]
          age_star[n_active] = age_star[i];
        if (read_group_id_star)              // Group ID
          group_id_star[n_active] = group_id_star[i];
        if (read_subhalo_id_star)            // Subhalo ID
          subhalo_id_star[n_active] = subhalo_id_star[i];
        cell_of_star[n_active] = cell_of_star[i]; // Cell index of each star
        L_cont_star[n_active] = L_cont_star[i]; // Line continuum spectral luminosity [erg/s/anstrom]
      }
      n_active++;
    }
  if (n_active <= 0)
    root_error("There are no active stars in this simulation");
  n_stars = n_active;                        // Restrict access to active stars
  r_star.resize(n_active);                   // Stellar positions [cm]
  if (read_v_star)                           // Stellar velocities [cm/s]
    v_star.resize(n_active);
  if (read_m_massive_star)                   // Massive stellar masses [Msun]
    m_massive_star.resize(n_active);
  if (read_m_init_star)                      // Initial stellar masses [Msun]
    m_init_star.resize(n_active);
  if (read_Z_star)                           // Stellar metallicities
    Z_star.resize(n_active);
  if (read_age_star)                         // Stellar ages [Gyr]
    age_star.resize(n_active);
  if (read_group_id_star)                    // Group ID
    group_id_star.resize(n_active);
  if (read_subhalo_id_star)                  // Subhalo ID
    subhalo_id_star.resize(n_active);
  cell_of_star.resize(n_active);             // Cell index of each active star
  L_cont_star.resize(n_active);              // Line continuum spectral luminosity [erg/s/anstrom]
}
