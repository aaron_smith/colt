/******************************
 * escape_cont/escape_cont.cc *
 ******************************

 * Driver: Assign rays for escape, etc.

*/

#include "../proto.h"
#include "EscapeCont.h"
#include "../timing.h" // Timing functionality

extern Timer escape_timer; // Clock timing
double spherical_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding sphere
double box_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding box
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

/* Helper macro for updating statistics during ray tracing. */
#define move_photon(distance) {\
  const double dl = distance;                /* Copy traversal distance */\
  const double dv = v[cell].dot(k_cam) - v_ref; /* LOS velocity [cm/s] */\
  const double v_dl = dv * dl;               /* LOS velocity * dl [cm^2/s] */\
  const double v2_dl = dv * v_dl;            /* LOS velocity^2 * dl [cm^3/s^2] */\
  const double k_abs = (apply_red_filter && dv <= 0.) ? 0. : k_dust[cell]; /* Dust absorption coefficient [1/cm] */\
  const double dtau_abs = k_abs * dl;        /* Absorption optical depth */\
  const double exp_dtau_abs = exp(-dtau_abs), tau_abs = k_abs * l_tot; \
  const double L_ldl = L_esc * ((dtau_abs < 1e-5) ? /* Numerical stability */\
    dl * (l_tot + 0.5 * dl - dtau_abs * (0.5 * l_tot + dl / 3.)) : /* Series approx (dtau_abs << 1) */\
    (1. + tau_abs - (1. + tau_abs + dtau_abs) * exp_dtau_abs) / (k_abs * k_abs)); /* Analytic solution */\
  L_esc *= exp_dtau_abs;                     /* Reduce weight by exp(-dtau_abs) */\
  L_dist_dl += L_ldl;                        /* Absorption distance integral */\
  rho_dl += rho[cell] * dl;                  /* Gas column density [g/cm^2] */\
  rho_Z_dl += Z[cell] * rho[cell] * dl;      /* Metal column density [g/cm^2] */\
  n_H_dl += n_H[cell] * dl;                  /* Hydrogen column density [cm^-2] */\
  n_HI_dl += x_HI[cell] * n_H[cell] * dl;    /* HI column density [cm^-2] */\
  n_atom_dl += n_carrier[cell] * dl;         /* Carrier column density [cm^-2] */\
  n_ion_dl += x_lower[cell] * n_carrier[cell] * dl; /* Lower state column density [cm^-2] */\
  v_rho_dl += rho[cell] * v_dl;              /* Gas column velocity [g/cm/s] */\
  v2_rho_dl += rho[cell] * v2_dl;            /* Gas column velocity^2 [g/s^2] */\
  v_rho_Z_dl += Z[cell] * rho[cell] * v_dl;  /* Metal column velocity [g/cm/s] */\
  v2_rho_Z_dl += Z[cell] * rho[cell] * v2_dl; /* Metal column velocity^2 [g/s^2] */\
  v_n_H_dl += n_H[cell] * v_dl;              /* Hydrogen column velocity [1/cm/s] */\
  v2_n_H_dl += n_H[cell] * v2_dl;            /* Hydrogen column velocity^2 [1/s^2] */\
  v_n_HI_dl += x_HI[cell] * n_H[cell] * v_dl; /* HI column velocity [1/cm/s] */\
  v2_n_HI_dl += x_HI[cell] * n_H[cell] * v2_dl; /* HI column velocity^2 [1/s^2] */\
  v_n_atom_dl += n_carrier[cell] * v_dl;     /* Carrier column velocity [1/cm/s] */\
  v2_n_atom_dl += n_carrier[cell] * v2_dl;   /* Carrier column velocity^2 [1/s^2] */\
  v_n_ion_dl += x_lower[cell] * n_carrier[cell] * v_dl; /* Lower state column velocity [1/cm/s] */\
  v2_n_ion_dl += x_lower[cell] * n_carrier[cell] * v2_dl; /* Lower state column velocity^2 [1/s^2] */\
  l_tot += dl;                               /* Update the total path length */\
  point += k_cam * dl;                       /* Update the position of the photon */\
}

/* Calculate escape for a single line of sight. */
void EscapeCont::ray_trace(const int star, const int camera) {
  const vector<double>& n_carrier = fields[line_carrier].data; // Carrier number density [cm^-3]
  const vector<double>& x_lower = fields[line_type].data; // Lower state fraction
  int cell = cell_of_star[star];             // Cell index
  int next_cell;                             // Next cell index
  double l, l_tot = 0.;                      // Path lengths
  const double L_int = L_cont_star[star];    // Intrinsic spectral luminosity [erg/s/angstrom]
  double L_esc = L_int;                      // Escaped spectral luminosity [erg/s/angstrom]
  double L_dist_dl = 0.;                     // Absorption distance integral [cm^2 erg/s/angstrom]
  // Column densities, velocities, and velocities^2
  double rho_dl = 0., v_rho_dl = 0., v2_rho_dl = 0.; // Gas
  double rho_Z_dl = 0., v_rho_Z_dl = 0., v2_rho_Z_dl = 0.; // Metal
  double n_H_dl = 0., v_n_H_dl = 0., v2_n_H_dl = 0.; // Hydrogen
  double n_HI_dl = 0., v_n_HI_dl = 0., v2_n_HI_dl = 0.; // HI
  double n_atom_dl = 0., v_n_atom_dl = 0., v2_n_atom_dl = 0.; // Carrier
  double n_ion_dl = 0., v_n_ion_dl = 0., v2_n_ion_dl = 0.; // Lower state
  Vec3 point = r_star[star];                 // Emission position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  const Vec2 pos = project(point, camera);   // Position in camera coordinates
  const Vec3& v_cam = use_stellar_frame ? v_star[star] : camera_motion; // Camera velocity
  const double v_ref = dot(k_cam, v_cam);    // Emission velocity [cm/s]
  #if check_escape
    double l_exit = positive_infinity;       // Escape distance
  #endif
  #if spherical_escape                       // Sphere escape distance
    inplace_min(l_exit, spherical_escape_distance(point, k_cam));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(l_exit, box_escape_distance(point, k_cam));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(l_exit, max_streaming);
  #endif
  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    // Check for spherical or box escape
    #if check_escape
      if (l_exit <= l) {                     // Exit region before cell
        move_photon(l_exit);                 // Move to the exit surface
        break;                               // Valid ray completion
      }
      l_exit -= l;                           // Remaining distance to exit
    #endif

    // Cumulative LOS optical depth
    move_photon(l);                          // Move to the cell boundary

    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the photon is trapped
        break;
    }
    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Add escaped emission contributions
  const double L_dist = L_dist_dl / l_tot;   // Absorption distance normalization [cm erg/s/angstrom]
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs[camera] += L_esc;                 // Escape fractions [fraction]
  if (output_absorption_distances)
    #pragma omp atomic
    mean_dists[camera] += L_dist;            // Average absorption distances [cm]

  // Surface brightness images
  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images[camera](ix, iy) += L_esc;       // Escaped SB images

      if (output_emission)
        #pragma omp atomic
        images_int[camera](ix, iy) += L_int; // Intrinsic SB images
    }
  }

  // Radial surface brightness images
  if (output_radial_images) {
    const double r_pix = inverse_radial_pixel_width * norm(pos); // Projected radius
    if (r_pix < double(n_radial_pixels)) {   // Range check
      const int ir = floor(r_pix);             // Radial pixel index
      #pragma omp atomic
      radial_images[camera][ir] += L_esc;      // Escaped radial SB images

      if (output_emission)
      #pragma omp atomic
      radial_images_int[camera][ir] += L_int;  // Intrinsic radial SB images
    }
  }

  // Column densities
  if (output_gas_columns_int)
    #pragma omp atomic
    gas_columns_int[camera] += L_int * rho_dl; // Gas column density [g/cm^2]
  if (output_gas_columns_esc)
    #pragma omp atomic
    gas_columns_esc[camera] += L_esc * rho_dl; // Gas column density [g/cm^2]
  if (output_metal_columns_int)
    #pragma omp atomic
    metal_columns_int[camera] += L_int * rho_Z_dl; // Metal column density [g/cm^2]
  if (output_metal_columns_esc)
    #pragma omp atomic
    metal_columns_esc[camera] += L_esc * rho_Z_dl; // Metal column density [g/cm^2]
  if (output_H_columns_int)
    #pragma omp atomic
    H_columns_int[camera] += L_int * n_H_dl; // Hydrogen column density [cm^-2]
  if (output_H_columns_esc)
    #pragma omp atomic
    H_columns_esc[camera] += L_esc * n_H_dl; // Hydrogen column density [cm^-2]
  if (output_HI_columns_int)
    #pragma omp atomic
    HI_columns_int[camera] += L_int * n_HI_dl; // HI column density [g/cm^2]
  if (output_HI_columns_esc)
    #pragma omp atomic
    HI_columns_esc[camera] += L_esc * n_HI_dl; // HI column density [g/cm^2]
  if (output_carrier_columns_int)
    #pragma omp atomic
    carrier_columns_int[camera] += L_int * n_atom_dl; // Carrier column density [cm^-2]
  if (output_carrier_columns_esc)
    #pragma omp atomic
    carrier_columns_esc[camera] += L_esc * n_atom_dl; // Carrier column density [cm^-2]
  if (output_lower_columns_int)
    #pragma omp atomic
    lower_columns_int[camera] += L_int * n_ion_dl; // Lower state column density [cm^-2]
  if (output_lower_columns_esc)
    #pragma omp atomic
    lower_columns_esc[camera] += L_esc * n_ion_dl; // Lower state column density [cm^-2]

  // Velocities
  if (output_gas_velocities_int)
    #pragma omp atomic
    gas_velocities_int[camera] += L_int * v_rho_dl; // Gas velocity [cm/s]
  if (output_gas_velocities_esc)
    #pragma omp atomic
    gas_velocities_esc[camera] += L_esc * v_rho_dl; // Gas velocity [cm/s]
  if (output_metal_velocities_int)
    #pragma omp atomic
    metal_velocities_int[camera] += L_int * v_rho_Z_dl; // Metal velocity [cm/s]
  if (output_metal_velocities_esc)
    #pragma omp atomic
    metal_velocities_esc[camera] += L_esc * v_rho_Z_dl; // Metal velocity [cm/s]
  if (output_H_velocities_int)
    #pragma omp atomic
    H_velocities_int[camera] += L_int * v_n_H_dl; // Hydrogen velocity [cm/s]
  if (output_H_velocities_esc)
    #pragma omp atomic
    H_velocities_esc[camera] += L_esc * v_n_H_dl; // Hydrogen velocity [cm/s]
  if (output_HI_velocities_int)
    #pragma omp atomic
    HI_velocities_int[camera] += L_int * v_n_HI_dl; // HI velocity [cm/s]
  if (output_HI_velocities_esc)
    #pragma omp atomic
    HI_velocities_esc[camera] += L_esc * v_n_HI_dl; // HI velocity [cm/s]
  if (output_carrier_velocities_int)
    #pragma omp atomic
    carrier_velocities_int[camera] += L_int * v_n_atom_dl; // Carrier velocity [cm/s]
  if (output_carrier_velocities_esc)
    #pragma omp atomic
    carrier_velocities_esc[camera] += L_esc * v_n_atom_dl; // Carrier velocity [cm/s]
  if (output_lower_velocities_int)
    #pragma omp atomic
    lower_velocities_int[camera] += L_int * v_n_ion_dl; // Lower state velocity [cm/s]
  if (output_lower_velocities_esc)
    #pragma omp atomic
    lower_velocities_esc[camera] += L_esc * v_n_ion_dl; // Lower state velocity [cm/s]

  // Velocities^2
  if (output_gas_velocities2_int)
    #pragma omp atomic
    gas_velocities2_int[camera] += L_int * v2_rho_dl; // Gas velocity^2 [cm^2/s^2]
  if (output_gas_velocities2_esc)
    #pragma omp atomic
    gas_velocities2_esc[camera] += L_esc * v2_rho_dl; // Gas velocity^2 [cm^2/s^2]
  if (output_metal_velocities2_int)
    #pragma omp atomic
    metal_velocities2_int[camera] += L_int * v2_rho_Z_dl; // Metal velocity^2 [cm^2/s^2]
  if (output_metal_velocities2_esc)
    #pragma omp atomic
    metal_velocities2_esc[camera] += L_esc * v2_rho_Z_dl; // Metal velocity^2 [cm^2/s^2]
  if (output_H_velocities2_int)
    #pragma omp atomic
    H_velocities2_int[camera] += L_int * v2_n_H_dl; // Hydrogen velocity^2 [cm^2/s^2]
  if (output_H_velocities2_esc)
    #pragma omp atomic
    H_velocities2_esc[camera] += L_esc * v2_n_H_dl; // Hydrogen velocity^2 [cm^2/s^2]
  if (output_HI_velocities2_int)
    #pragma omp atomic
    HI_velocities2_int[camera] += L_int * v2_n_HI_dl; // HI velocity^2 [cm^2/s^2]
  if (output_HI_velocities2_esc)
    #pragma omp atomic
    HI_velocities2_esc[camera] += L_esc * v2_n_HI_dl; // HI velocity^2 [cm^2/s^2]
  if (output_carrier_velocities2_int)
    #pragma omp atomic
    carrier_velocities2_int[camera] += L_int * v2_n_atom_dl; // Carrier velocity^2 [cm^2/s^2]
  if (output_carrier_velocities2_esc)
    #pragma omp atomic
    carrier_velocities2_esc[camera] += L_esc * v2_n_atom_dl; // Carrier velocity^2 [cm^2/s^2]
  if (output_lower_velocities2_int)
    #pragma omp atomic
    lower_velocities2_int[camera] += L_int * v2_n_ion_dl; // Lower state velocity^2 [cm^2/s^2]
  if (output_lower_velocities2_esc)
    #pragma omp atomic
    lower_velocities2_esc[camera] += L_esc * v2_n_ion_dl; // Lower state velocity^2 [cm^2/s^2]
}

/* Ray trace for escape calculations. */
void EscapeCont::calculate_escape_cont() {
  const size_t n_rays = n_stars * n_cameras; // Number of ray calculations
  const size_t interval = (n_rays < 200) ? 1 : n_rays / 100; // Interval between updates
  size_t n_finished = 0;                     // Progress for printing
  if (root)
    cout << "\n RT progress:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (size_t i = 0; i < n_rays; ++i) {
    // Ray tracing for each star and camera
    const int star = i / n_cameras;          // (star, camera) indices
    const int camera = i % n_cameras;
    ray_trace(star, camera);                 // Ray tracing calculations

    // Print completed progress
    if (root) {
      size_t i_finished;                     // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % interval == 0)
        cout << std::setw(3) << (100 * i_finished) / n_rays << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Helper function for converting to observed units. */
static inline void normalize(const bool flag, vector<double>& vec, const vector<double>& denom) {
  if (flag)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      if (denom[camera] > 0.)
        vec[camera] /= denom[camera];        // Normalization
}

/* Helper function for converting to observed units. */
static inline void normalize(const bool flag, vector<double>& vec, const double denom) {
  if (flag && denom > 0.)
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      vec[camera] /= denom;                  // Normalization
}

/* Convert observables to the correct units. */
void EscapeCont::correct_units() {
  cout << "\n RT: Converting to physical units ..." << endl;

  // Average absorption distances [cm]
  normalize(output_absorption_distances, mean_dists, L_tot); // Normalize to total emission

  // Gas column density [g/cm^2], velocity [cm/s], and velocity^2 [cm^2/s^2]
  normalize(output_gas_velocities_int, gas_velocities_int, gas_columns_int); // Normalize to column density
  normalize(output_gas_velocities_esc, gas_velocities_esc, gas_columns_esc);
  normalize(output_gas_velocities2_int, gas_velocities2_int, gas_columns_int);
  normalize(output_gas_velocities2_esc, gas_velocities2_esc, gas_columns_esc);
  normalize(output_gas_columns_int, gas_columns_int, L_tot); // Normalize to emission (intrinsic)
  normalize(output_gas_columns_esc, gas_columns_esc, f_escs); // Normalize to emission (escaped)

  // Metal column density [g/cm^2], velocity [cm/s], and velocity^2 [cm^2/s^2]
  normalize(output_metal_velocities_int, metal_velocities_int, metal_columns_int); // Normalize to column density
  normalize(output_metal_velocities_esc, metal_velocities_esc, metal_columns_esc);
  normalize(output_metal_velocities2_int, metal_velocities2_int, metal_columns_int);
  normalize(output_metal_velocities2_esc, metal_velocities2_esc, metal_columns_esc);
  normalize(output_metal_columns_int, metal_columns_int, L_tot); // Normalize to emission (intrinsic)
  normalize(output_metal_columns_esc, metal_columns_esc, f_escs); // Normalize to emission (escaped)

  // Hydrogen column density [cm^-2], velocity [cm/s], and velocity^2 [cm^2/s^2]
  normalize(output_H_velocities_int, H_velocities_int, H_columns_int); // Normalize to column density
  normalize(output_H_velocities_esc, H_velocities_esc, H_columns_esc);
  normalize(output_H_velocities2_int, H_velocities2_int, H_columns_int);
  normalize(output_H_velocities2_esc, H_velocities2_esc, H_columns_esc);
  normalize(output_H_columns_int, H_columns_int, L_tot); // Normalize to emission (intrinsic)
  normalize(output_H_columns_esc, H_columns_esc, f_escs); // Normalize to emission (escaped)

  // HI column density [g/cm^2], velocity [cm/s], and velocity^2 [cm^2/s^2]
  normalize(output_HI_velocities_int, HI_velocities_int, HI_columns_int); // Normalize to column density
  normalize(output_HI_velocities_esc, HI_velocities_esc, HI_columns_esc);
  normalize(output_HI_velocities2_int, HI_velocities2_int, HI_columns_int);
  normalize(output_HI_velocities2_esc, HI_velocities2_esc, HI_columns_esc);
  normalize(output_HI_columns_int, HI_columns_int, L_tot); // Normalize to emission (intrinsic)
  normalize(output_HI_columns_esc, HI_columns_esc, f_escs); // Normalize to emission (escaped)

  // Carrier column density [cm^-2], velocity [cm/s], and velocity^2 [cm^2/s^2]
  normalize(output_carrier_velocities_int, carrier_velocities_int, carrier_columns_int); // Normalize to column density
  normalize(output_carrier_velocities_esc, carrier_velocities_esc, carrier_columns_esc);
  normalize(output_carrier_velocities2_int, carrier_velocities2_int, carrier_columns_int);
  normalize(output_carrier_velocities2_esc, carrier_velocities2_esc, carrier_columns_esc);
  normalize(output_carrier_columns_int, carrier_columns_int, L_tot); // Normalize to emission (intrinsic)
  normalize(output_carrier_columns_esc, carrier_columns_esc, f_escs); // Normalize to emission (escaped)

  // Lower state column density [cm^-2], velocity [cm/s], and velocity^2 [cm^2/s^2]
  normalize(output_lower_velocities_int, lower_velocities_int, lower_columns_int); // Normalize to column density
  normalize(output_lower_velocities_esc, lower_velocities_esc, lower_columns_esc);
  normalize(output_lower_velocities2_int, lower_velocities2_int, lower_columns_int);
  normalize(output_lower_velocities2_esc, lower_velocities2_esc, lower_columns_esc);
  normalize(output_lower_columns_int, lower_columns_int, L_tot); // Normalize to emission (intrinsic)
  normalize(output_lower_columns_esc, lower_columns_esc, f_escs); // Normalize to emission (escaped)

  // Surface brightness images [photons/s/cm^2]
  const double to_image_units = 1. / pixel_area; // L / dA
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Radial surface brightness images [photons/s/cm^2]
  vector<double> to_radial_image_units;      // L / dA
  if (output_radial_images) {
    to_radial_image_units = vector<double>(n_radial_pixels);
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_pixels; ++ir)
      to_radial_image_units[ir] = 1. / radial_pixel_areas[ir];

    for (auto& radial_image : radial_images) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_pixels; ++ir)
        radial_image[ir] *= to_radial_image_units[ir];
    }
  }

  // Intrinsic cameras
  if (output_emission) {
    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_images) {
      for (auto& radial_image_int : radial_images_int) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_pixels; ++ir)
          radial_image_int[ir] *= to_radial_image_units[ir];
      }
    }
  }
}

/* Driver for escape calculations. */
void EscapeCont::run() {
  escape_timer.start();

  // Perform ray tracing escape calculations
  calculate_escape_cont();

  // Collect the results from different processors
  // if (n_ranks > 1)
  //   reduce_escape();

  if (root) {
    // Convert observables to the correct units
    correct_units();
    print("f_esc (LOS)", f_escs);
    if (output_absorption_distances)
      print("dists (int)", mean_dists, "kpc", kpc);

    // Column densities
    if (output_gas_columns_int)
      print("N_gas (int)", gas_columns_int, "g/cm^2");
    if (output_gas_columns_esc)
      print("N_gas (esc)", gas_columns_esc, "g/cm^2");
    if (output_metal_columns_int)
      print("N_metal (int)", metal_columns_int, "g/cm^2");
    if (output_metal_columns_esc)
      print("N_metal (esc)", metal_columns_esc, "g/cm^2");
    if (output_H_columns_int)
      print("N_H (int)", H_columns_int, "cm^-2");
    if (output_H_columns_esc)
      print("N_H (esc)", H_columns_esc, "cm^-2");
    if (output_HI_columns_int)
      print("N_HI (int)", HI_columns_int, "cm^-2");
    if (output_HI_columns_esc)
      print("N_HI (esc)", HI_columns_esc, "cm^-2");
    string atom = atom_symbols[line_carrier - Hydrogen_Line]; // Atom symbol (e.g. H)
    if (output_carrier_columns_int)
      print("N_"+atom+" (int)", carrier_columns_int, "cm^-2");
    if (output_carrier_columns_esc)
      print("N_"+atom+" (esc)", carrier_columns_esc, "cm^-2");
    string ion = fields[line_type].name.substr(2); // Line ionization state symbol (e.g. HI)
    if (output_lower_columns_int)
      print("N_"+ion+" (int)", lower_columns_int, "cm^-2");
    if (output_lower_columns_esc)
      print("N_"+ion+" (esc)", lower_columns_esc, "cm^-2");

    // Velocities
    if (output_gas_velocities_int)
      print("v_gas (int)", gas_velocities_int, "km/s", km);
    if (output_gas_velocities_esc)
      print("v_gas (esc)", gas_velocities_esc, "km/s", km);
    if (output_metal_velocities_int)
      print("v_metal (int)", metal_velocities_int, "km/s", km);
    if (output_metal_velocities_esc)
      print("v_metal (esc)", metal_velocities_esc, "km/s", km);
    if (output_H_velocities_int)
      print("v_H (int)", H_velocities_int, "km/s", km);
    if (output_H_velocities_esc)
      print("v_H (esc)", H_velocities_esc, "km/s", km);
    if (output_HI_velocities_int)
      print("v_HI (int)", HI_velocities_int, "km/s", km);
    if (output_HI_velocities_esc)
      print("v_HI (esc)", HI_velocities_esc, "km/s", km);
    if (output_carrier_velocities_int)
      print("v_"+atom+" (int)", carrier_velocities_int, "km/s", km);
    if (output_carrier_velocities_esc)
      print("v_"+atom+" (esc)", carrier_velocities_esc, "km/s", km);
    if (output_lower_velocities_int)
      print("v_"+ion+" (int)", lower_velocities_int, "km/s", km);
    if (output_lower_velocities_esc)
      print("v_"+ion+" (esc)", lower_velocities_esc, "km/s", km);

    // Velocities^2
    constexpr double km2 = km * km;
    if (output_gas_velocities2_int)
      print("v2_gas (int)", gas_velocities2_int, "km^2/s^2", km2);
    if (output_gas_velocities2_esc)
      print("v2_gas (esc)", gas_velocities2_esc, "km^2/s^2", km2);
    if (output_metal_velocities2_int)
      print("v2_metal (int)", metal_velocities2_int, "km^2/s^2", km2);
    if (output_metal_velocities2_esc)
      print("v2_metal (esc)", metal_velocities2_esc, "km^2/s^2", km2);
    if (output_H_velocities2_int)
      print("v2_H (int)", H_velocities2_int, "km^2/s^2", km2);
    if (output_H_velocities2_esc)
      print("v2_H (esc)", H_velocities2_esc, "km^2/s^2", km2);
    if (output_HI_velocities2_int)
      print("v2_HI (int)", HI_velocities2_int, "km^2/s^2", km2);
    if (output_HI_velocities2_esc)
      print("v2_HI (esc)", HI_velocities2_esc, "km^2/s^2", km2);
    if (output_carrier_velocities2_int)
      print("v2_"+atom+" (int)", carrier_velocities2_int, "km^2/s^2", km2);
    if (output_carrier_velocities2_esc)
      print("v2_"+atom+" (esc)", carrier_velocities2_esc, "km^2/s^2", km2);
    if (output_lower_velocities2_int)
      print("v2_"+ion+" (int)", lower_velocities2_int, "km^2/s^2", km2);
    if (output_lower_velocities2_esc)
      print("v2_"+ion+" (esc)", lower_velocities2_esc, "km^2/s^2", km2);
  }
  escape_timer.stop();
}
