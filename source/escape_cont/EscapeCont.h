/****************************
 * escape_cont/EscapeCont.h *
 ****************************

 * Module declarations.

*/

#ifndef ESCAPE_CONT_INCLUDED
#define ESCAPE_CONT_INCLUDED

#include "../Simulation.h" // Base simulation class

// Line parameters
extern string line;                          // Name of the line
extern double E0;                            // Line energy [erg]
extern double A21;                           // Einstein A coefficient [Hz]
extern double f12;                           // Oscillator strength
extern double g12;                           // Degeneracy ratio: g_lower / g_upper
extern double T0;                            // Line temperature [K]
extern double nu0;                           // Line frequency [Hz]
extern double lambda0;                       // Line wavelength [angstrom]
extern double B21;                           // Einstein B21 = A21 c^2 / (2 h ν0^3)
extern double DnuL;                          // Natural frequency broadening [Hz]
extern double m_carrier;                     // Mass of carrier [g]

// Doublet line parameters
extern double E0p;                           // Line energy [erg]
extern double A21p;                          // Einstein A coefficient [Hz]
extern double f12p;                          // Oscillator strength
extern double nu0p;                          // Line frequency [Hz]
extern double lambda0p;                      // Line wavelength [angstrom]
extern double DnuLp;                         // Natural frequency broadening [Hz]

// Information about the dust model
extern double kappa_dust;                    // Dust opacity [cm^2/g dust]
extern double sigma_dust;                    // Dust cross section [cm^2/Z/hydrogen atom]
extern double albedo;                        // Dust albedo for scattering vs. absorption
extern double g_dust;                        // Anisotropy parameter: <μ> for dust scattering

/* Ray-based escape (continuum) module. */
class EscapeCont : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  // Ionizing photon models
  string source_model = "";                  // Escape source model
  double L_tot = 0.;                         // Total spectral luminosity [erg/s/angstrom]
  double Ndot_tot = 0.;                      // Total spectral emission rate [photons/s/angstrom]
  double L_stars = 0.;                       // Total stellar luminosity [erg/s/angstrom]
  double M_cont = 0.;                        // Total stellar absolute magnitude
  vector<int> cell_of_star;                  // Cell index of each active star

  // Information about secondary cameras
  bool apply_red_filter = false;             // Apply a red filter (v > 0)
  bool use_stellar_frame = false;            // Use stellar velocities to define the emission rest frame
  bool output_emission = false;              // Output intrinsic emission without transport
  bool output_absorption_distances = true;   // Output absorption distances [cm]
  vector<double> mean_dists;                 // Average absorption distances [cm]
  Images images_int;                         // Intrinsic surface brightness images [photons/s/cm^2/angstrom]
  vectors radial_images_int;                 // Intrinsic radial surface brightness images [photons/s/cm^2/angstrom]

  // Column densities
  bool output_gas_columns_int = true;        // Output gas column densities (intrinsic)
  vector<double> gas_columns_int;            // Gas column densities [g/cm^2]
  bool output_gas_columns_esc = true;        // Output gas column densities (escaped)
  vector<double> gas_columns_esc;            // Gas column densities [g/cm^2]
  bool output_metal_columns_int = true;      // Output metal column densities (intrinsic)
  vector<double> metal_columns_int;          // Metal column densities [g/cm^2]
  bool output_metal_columns_esc = true;      // Output metal column densities (escaped)
  vector<double> metal_columns_esc;          // Metal column densities [g/cm^2]
  bool output_H_columns_int = true;          // Output hydrogen column densities (intrinsic)
  vector<double> H_columns_int;              // Hydrogen column densities [cm^-2]
  bool output_H_columns_esc = true;          // Output hydrogen column densities (escaped)
  vector<double> H_columns_esc;              // Hydrogen column densities [cm^-2]
  bool output_HI_columns_int = true;         // Output HI column densities (intrinsic)
  vector<double> HI_columns_int;             // HI column densities [cm^-2]
  bool output_HI_columns_esc = true;         // Output HI column densities (escaped)
  vector<double> HI_columns_esc;             // HI column densities [cm^-2]
  bool output_carrier_columns_int = true;    // Output carrier column densities (intrinsic)
  vector<double> carrier_columns_int;        // Carrier column densities [cm^-2]
  bool output_carrier_columns_esc = true;    // Output carrier column densities (escaped)
  vector<double> carrier_columns_esc;        // Carrier column densities [cm^-2]
  bool output_lower_columns_int = true;      // Output lower state column densities (intrinsic)
  vector<double> lower_columns_int;          // Lower state column densities [cm^-2]
  bool output_lower_columns_esc = true;      // Output lower state column densities (escaped)
  vector<double> lower_columns_esc;          // Lower state column densities [cm^-2]

  // Velocities
  bool output_gas_velocities_int = false;    // Output gas velocities (intrinsic)
  vector<double> gas_velocities_int;         // Gas velocities [cm/s]
  bool output_gas_velocities_esc = false;    // Output gas velocities (escaped)
  vector<double> gas_velocities_esc;         // Gas velocities [cm/s]
  bool output_metal_velocities_int = false;  // Output metal velocities (intrinsic)
  vector<double> metal_velocities_int;       // Metal velocities [cm/s]
  bool output_metal_velocities_esc = false;  // Output metal velocities (escaped)
  vector<double> metal_velocities_esc;       // Metal velocities [cm/s]
  bool output_H_velocities_int = false;      // Output hydrogen velocities (intrinsic)
  vector<double> H_velocities_int;           // Hydrogen velocities [cm/s]
  bool output_H_velocities_esc = false;      // Output hydrogen velocities (escaped)
  vector<double> H_velocities_esc;           // Hydrogen velocities [cm/s]
  bool output_HI_velocities_int = false;     // Output HI velocities (intrinsic)
  vector<double> HI_velocities_int;          // HI velocities [cm/s]
  bool output_HI_velocities_esc = false;     // Output HI velocities (escaped)
  vector<double> HI_velocities_esc;          // HI velocities [cm/s]
  bool output_carrier_velocities_int = false;// Output carrier velocities (intrinsic)
  vector<double> carrier_velocities_int;     // Carrier velocities [cm/s]
  bool output_carrier_velocities_esc = false;// Output carrier velocities (escaped)
  vector<double> carrier_velocities_esc;     // Carrier velocities [cm/s]
  bool output_lower_velocities_int = false;  // Output lower state velocities (intrinsic)
  vector<double> lower_velocities_int;       // Lower state velocities [cm/s]
  bool output_lower_velocities_esc = false;  // Output lower state velocities (escaped)
  vector<double> lower_velocities_esc;       // Lower state velocities [cm/s]

  // Velocities^2
  bool output_gas_velocities2_int = false;   // Output gas velocities^2 (intrinsic)
  vector<double> gas_velocities2_int;        // Gas velocities^2 [cm^2/s^2]
  bool output_gas_velocities2_esc = false;   // Output gas velocities^2 (escaped)
  vector<double> gas_velocities2_esc;        // Gas velocities^2 [cm^2/s^2]
  bool output_metal_velocities2_int = false; // Output metal velocities^2 (intrinsic)
  vector<double> metal_velocities2_int;      // Metal velocities^2 [cm^2/s^2]
  bool output_metal_velocities2_esc = false; // Output metal velocities^2 (escaped)
  vector<double> metal_velocities2_esc;      // Metal velocities^2 [cm^2/s^2]
  bool output_H_velocities2_int = false;     // Output hydrogen velocities^2 (intrinsic)
  vector<double> H_velocities2_int;          // Hydrogen velocities^2 [cm^2/s^2]
  bool output_H_velocities2_esc = false;     // Output hydrogen velocities^2 (escaped)
  vector<double> H_velocities2_esc;          // Hydrogen velocities^2 [cm^2/s^2]
  bool output_HI_velocities2_int = false;    // Output HI velocities^2 (intrinsic)
  vector<double> HI_velocities2_int;         // HI velocities^2 [cm^2/s^2]
  bool output_HI_velocities2_esc = false;    // Output HI velocities^2 (escaped)
  vector<double> HI_velocities2_esc;         // HI velocities^2 [cm^2/s^2]
  bool output_carrier_velocities2_int = false; // Output carrier velocities^2 (intrinsic)
  vector<double> carrier_velocities2_int;    // Carrier velocities^2 [cm^2/s^2]
  bool output_carrier_velocities2_esc = false; // Output carrier velocities^2 (escaped)
  vector<double> carrier_velocities2_esc;    // Carrier velocities^2 [cm^2/s^2]
  bool output_lower_velocities2_int = false; // Output lower state velocities^2 (intrinsic)
  vector<double> lower_velocities2_int;      // Lower state velocities^2 [cm^2/s^2]
  bool output_lower_velocities2_esc = false; // Output lower state velocities^2 (escaped)
  vector<double> lower_velocities2_esc;      // Lower state velocities^2 [cm^2/s^2]

  void ray_trace(const int star, const int camera); // Single ray
  void calculate_escape_cont();              // All stars and camera directions
  void correct_units();                      // Convert to observed units
};

#endif // ESCAPE_CONT_INCLUDED
