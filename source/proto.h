/***********
 * proto.h *
 ***********

 * General level function declarations.

*/

#ifndef PROTO_INCLUDED
#define PROTO_INCLUDED

// Standard headers
#include <iomanip>   // int -> string
#include <iostream>  // input / output
#include <string>    // Standard strings
#include <vector>    // Standard vectors
#include <array>     // Standard arrays
#include <tuple>     // Standard tuples
#include <limits>    // Numeric limits
#include <algorithm> // Standard algorithms

#include "mpi.h"     // MPI library
#include <omp.h>     // OpenMP library

// Standard library usage
using std::cout;
using std::endl;
using std::string;
using std::to_string;
using std::vector;
using std::array;
using std::tuple;
using std::make_tuple;
using std::tie;
using strings = vector<string>;
using vectors = vector<vector<double>>;

// Simple function to print an error message and exit
#define error(message) { error_info(message, __FILE__, __LINE__, __func__); }
#define root_error(message) { if (root) error(message); MPI_Barrier(MPI_COMM_WORLD); }
template <typename T>
void error_info(const T message, const string file, const int line, const string func) {
  std::cerr << "\nError: " << message
            << "\nCheck: " << file << ":" << line << " in " << func << "()" << endl;
  MPI_Abort(MPI_COMM_WORLD, 1);
  exit(EXIT_FAILURE);
}

// Include general data structures
#include "compile_time.h" // Compile time options
#include "Vec3.h" // 3D vectors in (x,y,z) order
#include "NDArray.h" // N-dimensional array class

using Image = NDArray<2, double>;
using Images = std::vector<Image>;
using Image3 = NDArray<2, Vec3>;
using Image3s = std::vector<Image3>;
using Cube = NDArray<3, double>;
using Cubes = std::vector<Cube>;

// Include general template funtions
#include "utils.h"

// Universal constants
constexpr double c = 2.99792458e10;          // Speed of light [cm/s]
constexpr double kB = 1.380648813e-16;       // Boltzmann's constant [g cm^2/s^2/K]
constexpr double h = 6.626069573e-27;        // Planck's constant [erg s]
constexpr double amu = 1.6605402e-24;        // Atomic mass unit [g]
constexpr double mH = 1.6735327e-24;         // Mass of hydrogen atom [g]
constexpr double mHe = 4.002602 * amu;       // Mass of helium atom [g]
constexpr double mC = 12.011 * amu;          // Mass of carbon atom [g]
constexpr double mN = 14.0067 * amu;         // Mass of nitrogen atom [g]
constexpr double mO = 15.999 * amu;          // Mass of oxygen atom [g]
constexpr double mNe = 20.1797 * amu;        // Mass of neon atom [g]
constexpr double mMg = 24.305 * amu;         // Mass of magnesium atom [g]
constexpr double mSi = 28.0855 * amu;        // Mass of silicon atom [g]
constexpr double mS = 32.065 * amu;          // Mass of sulfer atom [g]
constexpr double mFe = 55.845 * amu;         // Mass of iron atom [g]
constexpr double me = 9.109382917e-28;       // Electron mass [g]
constexpr double ee = 4.80320451e-10;        // Electron charge [g^(1/2) cm^(3/2) / s]

// Emperical unit definitions
constexpr double Msun = 1.988435e33;         // Solar mass [g]
constexpr double Lsun = 3.839e33;            // Solar luminosity [erg/s]
constexpr double Zsun = 0.0134;              // Solar metallicity [mass fraction]
// Convert Grevesse et al. (2010) abundances to mass fractions:
// Zsun_X = Xsun * mX / mH * 10^(A_X - 12)   (where Xsun = 0.738)
constexpr double Zsun_C = 0.00236729;        // Solar carbon metallicity [mass fraction]
constexpr double Zsun_N = 0.000693438;       // Solar nitrogen metallicity [mass fraction]
constexpr double Zsun_O = 0.00573805;        // Solar oxygen metallicity [mass fraction]
constexpr double Zsun_Ne = 0.00125773;       // Solar neon metallicity [mass fraction]
constexpr double Zsun_Mg = 0.000708545;      // Solar magnesium metallicity [mass fraction]
constexpr double Zsun_Si = 0.000665509;      // Solar silicon metallicity [mass fraction]
constexpr double Zsun_S = 0.00030953;        // Solar sulfer metallicity [mass fraction]
constexpr double Zsun_Fe = 0.00129317;       // Solar iron metallicity [mass fraction]
constexpr double eV = 1.60217725e-12;        // Electron volt: 1 eV = 1.6e-12 erg
constexpr double arcsec = 648000. / M_PI;    // arseconds per radian
constexpr double pc = 3.085677581467192e18;  // Units: 1 pc  = 3e18 cm
constexpr double kpc = 1e3 * pc;             // Units: 1 kpc = 3e21 cm
constexpr double Mpc = 1e6 * pc;             // Units: 1 Mpc = 3e24 cm
constexpr double Gpc = 1e9 * pc;             // Units: 1 Gpc = 3e27 cm
constexpr double km = 1e5;                   // Units: 1 km  = 1e5  cm
constexpr double angstrom = 1e-8;            // Units: 1 angstrom = 1e-8 cm
constexpr double day = 86400.;               // Units: 1 day = 24 * 3600 seconds
constexpr double yr = 365.24 * day;          // Units: 1 year = 365.24 days
constexpr double kyr = 1e3 * yr;             // Units: 1 kyr = 10^3 yr
constexpr double Myr = 1e6 * yr;             // Units: 1 Myr = 10^6 yr
constexpr double Gyr = 1e9 * yr;             // Units: 1 Gyr = 10^9 yr
constexpr double cGyr = c * Gyr;             // Units: 1 Giga-lightyear [cm]

// Ionization threshold energies [eV]
constexpr double HI_eV = 13.6, HeI_eV = 24.59, HeII_eV = 54.42;
constexpr double CI_eV = 11.26, CII_eV = 24.38, CIII_eV = 47.89, CIV_eV = 64.49, CV_eV = 392.1, CVI_eV = 490.;
constexpr double NI_eV = 14.53, NII_eV = 29.6, NIII_eV = 47.45, NIV_eV = 77.47, NV_eV = 97.89, NVI_eV = 552.1, NVII_eV = 667.1;
constexpr double OI_eV = 13.62, OII_eV = 35.12, OIII_eV = 54.94, OIV_eV = 77.41, OV_eV = 113.9, OVI_eV = 138.1, OVII_eV = 739.2, OVIII_eV = 871.4;
constexpr double NeI_eV = 21.56, NeII_eV = 40.96, NeIII_eV = 63.46, NeIV_eV = 97.12, NeV_eV = 126.2, NeVI_eV = 157.9, NeVII_eV = 207.3, NeVIII_eV = 239.1;
constexpr double MgI_eV = 7.646, MgII_eV = 15.04, MgIII_eV = 80.14, MgIV_eV = 109.3, MgV_eV = 141.3, MgVI_eV = 186.5, MgVII_eV = 224.9, MgVIII_eV = 266., MgIX_eV = 328.2, MgX_eV = 367.5;
constexpr double SiI_eV = 8.152, SiII_eV = 16.35, SiIII_eV = 33.49, SiIV_eV = 45.14, SiV_eV = 166.8, SiVI_eV = 205.1, SiVII_eV = 246.5, SiVIII_eV = 303.2, SiIX_eV = 351.1, SiX_eV = 401.4, SiXI_eV = 476.1, SiXII_eV = 523.5;
constexpr double SI_eV = 10.36, SII_eV = 23.33, SIII_eV = 34.83, SIV_eV = 47.31, SV_eV = 72.68, SVI_eV = 88.05, SVII_eV = 280.9, SVIII_eV = 328.2, SIX_eV = 379.1, SX_eV = 447.1, SXI_eV = 504.8, SXII_eV = 564.7, SXIII_eV = 651.7, SXIV_eV = 707.2;
constexpr double FeI_eV = 7.902, FeII_eV = 16.19, FeIII_eV = 30.65, FeIV_eV = 54.8, FeV_eV = 75.01, FeVI_eV = 99.06, FeVII_eV = 125., FeVIII_eV = 151.1, FeIX_eV = 233.6, FeX_eV = 262.1, FeXI_eV = 290.2, FeXII_eV = 330.8, FeXIII_eV = 361., FeXIV_eV = 392.2, FeXV_eV = 457., FeXVI_eV = 489.3;
// Ionization threshold energies [erg]
constexpr double HI_erg = HI_eV * eV, HeI_erg = HeI_eV * eV, HeII_erg = HeII_eV * eV;
constexpr double CI_erg = CI_eV * eV, CII_erg = CII_eV * eV, CIII_erg = CIII_eV * eV, CIV_erg = CIV_eV * eV, CV_erg = CV_eV * eV, CVI_erg = CVI_eV * eV;
constexpr double NI_erg = NI_eV * eV, NII_erg = NII_eV * eV, NIII_erg = NIII_eV * eV, NIV_erg = NIV_eV * eV, NV_erg = NV_eV * eV, NVI_erg = NVI_eV * eV, NVII_erg = NVII_eV * eV;
constexpr double OI_erg = OI_eV * eV, OII_erg = OII_eV * eV, OIII_erg = OIII_eV * eV, OIV_erg = OIV_eV * eV, OV_erg = OV_eV * eV, OVI_erg = OVI_eV * eV, OVII_erg = OVII_eV * eV, OVIII_erg = OVIII_eV * eV;
constexpr double NeI_erg = NeI_eV * eV, NeII_erg = NeII_eV * eV, NeIII_erg = NeIII_eV * eV, NeIV_erg = NeIV_eV * eV, NeV_erg = NeV_eV * eV, NeVI_erg = NeVI_eV * eV, NeVII_erg = NeVII_eV * eV, NeVIII_erg = NeVIII_eV * eV;
constexpr double MgI_erg = MgI_eV * eV, MgII_erg = MgII_eV * eV, MgIII_erg = MgIII_eV * eV, MgIV_erg = MgIV_eV * eV, MgV_erg = MgV_eV * eV, MgVI_erg = MgVI_eV * eV, MgVII_erg = MgVII_eV * eV, MgVIII_erg = MgVIII_eV * eV, MgIX_erg = MgIX_eV * eV, MgX_erg = MgX_eV * eV;
constexpr double SiI_erg = SiI_eV * eV, SiII_erg = SiII_eV * eV, SiIII_erg = SiIII_eV * eV, SiIV_erg = SiIV_eV * eV, SiV_erg = SiV_eV * eV, SiVI_erg = SiVI_eV * eV, SiVII_erg = SiVII_eV * eV, SiVIII_erg = SiVIII_eV * eV, SiIX_erg = SiIX_eV * eV, SiX_erg = SiX_eV * eV, SiXI_erg = SiXI_eV * eV, SiXII_erg = SiXII_eV * eV;
constexpr double SI_erg = SI_eV * eV, SII_erg = SII_eV * eV, SIII_erg = SIII_eV * eV, SIV_erg = SIV_eV * eV, SV_erg = SV_eV * eV, SVI_erg = SVI_eV * eV, SVII_erg = SVII_eV * eV, SVIII_erg = SVIII_eV * eV, SIX_erg = SIX_eV * eV, SX_erg = SX_eV * eV, SXI_erg = SXI_eV * eV, SXII_erg = SXII_eV * eV, SXIII_erg = SXIII_eV * eV, SXIV_erg = SXIV_eV * eV;
constexpr double FeI_erg = FeI_eV * eV, FeII_erg = FeII_eV * eV, FeIII_erg = FeIII_eV * eV, FeIV_erg = FeIV_eV * eV, FeV_erg = FeV_eV * eV, FeVI_erg = FeVI_eV * eV, FeVII_erg = FeVII_eV * eV, FeVIII_erg = FeVIII_eV * eV, FeIX_erg = FeIX_eV * eV, FeX_erg = FeX_eV * eV, FeXI_erg = FeXI_eV * eV, FeXII_erg = FeXII_eV * eV, FeXIII_erg = FeXIII_eV * eV, FeXIV_erg = FeXIV_eV * eV, FeXV_erg = FeXV_eV * eV, FeXVI_erg = FeXVI_eV * eV;
constexpr double sigma_zero = 1e-40;         // Zero cross-section [cm^2]
constexpr double sigma_chop = 1.01e-40;      // Chop cross-section [cm^2]

// Utility constants
constexpr double negative_infinity = std::numeric_limits<double>::lowest(); // Lowest double
constexpr double positive_infinity = std::numeric_limits<double>::max(); // Highest double
constexpr int OUTSIDE = -1;                  // Flag for outside points
constexpr int INSIDE = -2;                   // Flag for inside points
constexpr int SUM = -3;                      // Alias for sum weight index
constexpr int AVG = -4;                      // Alias for volume average index
constexpr int MASS = -5;                     // Alias for mass average index
constexpr int OUTSIDE_DUAL = -6;             // Flag for dual grid outside points
enum GridFlags { GRID, RADIAL, DISTANCE, RADIAL_FLOW, GROUP_FLOW, SUBHALO_FLOW,
                 GROUP_VIR_ESCAPE, GROUP_GAL_ESCAPE, SUBHALO_VIR_ESCAPE, SUBHALO_GAL_ESCAPE }; // Multiple grid flags

// Main control variables
extern string executable;                    // Executable name (command line)
extern string config_file;                   // Config file name (command line)
extern string snap_str;                      // Snapshot number (option command line)
extern int snap_num;                         // Snapshot number (converted from string)
extern string halo_str;                      // Group or subhalo number (option command line)
extern int halo_num;                         // Group or subhalo number (converted from string)
extern string init_dir;                      // Initial conditions directory (optional)
extern string init_subdir;                   // Initial conditions subdirectory name ("snap")
extern string init_base;                     // Initial conditions file base (optional)
extern string init_ext;                      // Initial conditions file extension
extern string init_file;                     // Initial conditions file (required)
extern string output_dir;                    // Output directory name ("output")
extern string output_subdir;                 // Output subdirectory name ("snap")
extern string output_base;                   // Output file base name ("colt")
extern string output_ext;                    // Output file extension ("hdf5")
extern string module;                        // Module type: mcrt, projections
extern bool verbose;                         // Verbose output for tests and debugging (false)
extern bool select_subhalo;                  // Specified a subhalo (true)
extern unsigned long long seed;              // Seed for random number generator
#pragma omp threadprivate(seed)              // Unique to each thread

// Supplementary files
extern bool save_connections;                // Save voronoi connections to a file
extern bool save_circulators;                // Flag to save circulators
extern string cgal_file;                     // CGAL connectivity file (optional)
extern string group_file;                    // Group catalog file (optional)
extern string subhalo_file;                  // Subhalo catalog file (optional)
extern string abundances_file;               // Abundances initial conditions file (optional)
extern string abundances_output_file;        // Abundances output file (optional)

// MPI and OpenMP variables
extern int rank;                             // MPI local rank
extern int n_ranks;                          // Number of MPI processes
extern int thread;                           // OpenMP local thread
#pragma omp threadprivate(thread)            // Unique to each thread
extern int n_threads;                        // Number of OpenMP threads
constexpr int ROOT = 0;                      // Root process
extern bool root;                            // True if rank is root
extern bool load_balancing;                  // Use MPI load balancing algorithms

// Keep track of unit scalings
struct Units {
  double a_scaling;                          // Exponent of the cosmological factor
  double h_scaling;                          // Exponent of the hubble parameter
  double length_scaling;                     // Length unit scaling
  double mass_scaling;                       // Mass unit scaling
  double velocity_scaling;                   // Velocity unit scaling
  double to_cgs;                             // Conversion factor to cgs units

  Units() = default;
  Units(double a_scaling, double h_scaling, double length_scaling,
        double mass_scaling, double velocity_scaling, double to_cgs) :
        a_scaling(a_scaling), h_scaling(h_scaling), length_scaling(length_scaling),
        mass_scaling(mass_scaling), velocity_scaling(velocity_scaling), to_cgs(to_cgs) {};
};

// Group data and units
template<typename T>
struct GenericField {
  string name;                               // Field name
  string cgs;                                // Field units [cgs]
  Units units;                               // Field units [code]
  double constant = -1.;                     // Constant value
  bool read = false;                         // Read from file
  vector<T> data;                            // Field data
};

using Field = GenericField<double>;

// List of possible fields
enum FieldIndex {
  Density = 0,                               // Density index
  Volume,                                    // Volume index
  Temperature,                               // Temperature index
  DustTemperature,                           // Dust temperature index
  InternalEnergy,                            // Internal energy index
  HydrogenDensity,                           // Hydrogen number density index
  HeliumDensity,                             // Helium number density index
  CarbonDensity,                             // Carbon number density index
  NitrogenDensity,                           // Nitrogen number density index
  OxygenDensity,                             // Oxygen number density index
  NeonDensity,                               // Neon number density index
  MagnesiumDensity,                          // Magnesium number density index
  SiliconDensity,                            // Silicon number density index
  SulferDensity,                             // Sulfer number density index
  IronDensity,                               // Iron number density index
  LowerDensity,                              // Number density of lower transition index
  UpperDensity,                              // Number density of upper transition index
  HI_Fraction, HII_Fraction, H2_Fraction,    // Ionization fraction indices
  HeI_Fraction, HeII_Fraction, HeIII_Fraction,
  CI_Fraction, CII_Fraction, CIII_Fraction, CIV_Fraction, CV_Fraction, CVI_Fraction, CVII_Fraction,
  NI_Fraction, NII_Fraction, NIII_Fraction, NIV_Fraction, NV_Fraction, NVI_Fraction, NVII_Fraction, NVIII_Fraction,
  OI_Fraction, OII_Fraction, OIII_Fraction, OIV_Fraction, OV_Fraction, OVI_Fraction, OVII_Fraction, OVIII_Fraction, OIX_Fraction,
  NeI_Fraction, NeII_Fraction, NeIII_Fraction, NeIV_Fraction, NeV_Fraction, NeVI_Fraction, NeVII_Fraction, NeVIII_Fraction, NeIX_Fraction,
  MgI_Fraction, MgII_Fraction, MgIII_Fraction, MgIV_Fraction, MgV_Fraction, MgVI_Fraction, MgVII_Fraction, MgVIII_Fraction, MgIX_Fraction, MgX_Fraction, MgXI_Fraction,
  SiI_Fraction, SiII_Fraction, SiIII_Fraction, SiIV_Fraction, SiV_Fraction, SiVI_Fraction, SiVII_Fraction, SiVIII_Fraction, SiIX_Fraction, SiX_Fraction, SiXI_Fraction, SiXII_Fraction, SiXIII_Fraction,
  SI_Fraction, SII_Fraction, SIII_Fraction, SIV_Fraction, SV_Fraction, SVI_Fraction, SVII_Fraction, SVIII_Fraction, SIX_Fraction, SX_Fraction, SXI_Fraction, SXII_Fraction, SXIII_Fraction, SXIV_Fraction, SXV_Fraction,
  FeI_Fraction, FeII_Fraction, FeIII_Fraction, FeIV_Fraction, FeV_Fraction, FeVI_Fraction, FeVII_Fraction, FeVIII_Fraction, FeIX_Fraction, FeX_Fraction, FeXI_Fraction, FeXII_Fraction, FeXIII_Fraction, FeXIV_Fraction, FeXV_Fraction, FeXVI_Fraction, FeXVII_Fraction,
  ElectronAbundance,                         // Electron fraction index
  PhotoheatingRate,                          // Photoheating rate index
  StarFormationRate,                         // Star formation rate index
  VelocitySquared,                           // Velocity^2 index
  VelocitySquaredLOS,                        // Velocity_LOS^2 index
  VelocityLOS,                               // Velocity_LOS index
  VelocityX,                                 // Velocity_X index
  VelocityY,                                 // Velocity_Y index
  MagneticFieldSquared,                      // Magnetic field^2 index
  MagneticFieldLOS,                          // Magnetic field LOS index
  MagneticFieldX,                            // Magnetic field X index
  MagneticFieldY,                            // Magnetic field Y index
  HydrogenFraction,                          // Mass fraction of hydrogen index
  HeliumFraction,                            // Mass fraction of helium index
  Metallicity,                               // Gas metallicity index
  CarbonMetallicity,                         // Carbon metallicity index
  NitrogenMetallicity,                       // Nitrogen metallicity index
  OxygenMetallicity,                         // Oxygen metallicity index
  NeonMetallicity,                           // Neon metallicity index
  MagnesiumMetallicity,                      // Magnesium metallicity index
  SiliconMetallicity,                        // Silicon metallicity index
  SulferMetallicity,                         // Sulfer metallicity index
  IronMetallicity,                           // Iron metallicity index
  DustMetallicity,                           // Dust-to-gas ratio index
  SilicateMetallicity,                       // Silicate dust-to-gas ratio index
  DustCoefficient,                           // Absorption coefficient of dust index
  DustDensity,                               // Dust density index
  GraphiteDensity,                           // Graphite dust density index
  SilicateDensity,                           // Silicate dust density index
  ElectronCoefficient,                       // Absorption coefficient of electrons index
  LineCoefficient,                           // Absorption coefficient of primary line index
  DoubletCoefficient,                        // Absorption coefficient of doublet line index
  DampingParameter,                          // Line damping parameter index
  NonLocalATau0,                             // Nonlocal estimate of a * tau0 index
  LineEmissivity,                            // Line emissivity [erg/s/cm^3]
  DoubletLineEmissivity,                     // Doublet line emissivity [erg/s/cm^3]
  EnergyDensity,                             // Radiation energy density [erg/cm^3]
  RadialAcceleration,                        // Radial radiation acceleration [cm/s^2]
  RadialAccelerationScat,                    // Radial radiation acceleration (scattering-based) [cm/s^2]
  RadialPressure,                            // Radial radiation pressure [erg/cm^3]
  SourceFraction,                            // Cumulative source fraction
  DustEscapeFraction,                        // Dust escape fraction
  EscapeFraction,                            // Cumulative escape fraction
  RescaledEscapeFraction,                    // Cumulative rescaled escape fraction
  TrappingRatio,                             // Cumulative t_trap / t_light
  ForceMultiplier,                           // Cumulative force multiplier
  ForceMultiplierScat,                       // Cumulative force multiplier (scattering-based)
  ForceMultiplierSource,                     // Cumulative force multiplier (by source)
  ForceMultiplierSourceScat,                 // Cumulative force multiplier (by source, scattering-based)
  PressureRatio,                             // Cumulative pressure-to-energy density ratio (P/u)
  DensitySquared,                            // Density^2 index
  DensitySquaredHI,                          // x_HI * Density^2 index
  StellarDensity,                            // Stellar density index
  IonizationFront,                           // Ionization front index
  n_fields                                   // Number of fields
};

extern int n_active_fields;                  // Number of active fields
extern vector<int> active_fields;            // List of active field indices
extern array<Field, n_fields> fields;        // File dataset fields

// Enumerated list of atoms
enum AtomIndex {
  H_ATOM = 0, He_ATOM, C_ATOM, N_ATOM, O_ATOM, Ne_ATOM, Mg_ATOM, Si_ATOM, S_ATOM, Fe_ATOM, n_atoms
};

// Enumerated list of ions
enum IonIndex {
  HI_ION = 0, HeI_ION, HeII_ION,             // Species indices
  CI_ION, CII_ION, CIII_ION, CIV_ION, CV_ION, CVI_ION,
  NI_ION, NII_ION, NIII_ION, NIV_ION, NV_ION, NVI_ION, NVII_ION,
  OI_ION, OII_ION, OIII_ION, OIV_ION, OV_ION, OVI_ION, OVII_ION, OVIII_ION,
  NeI_ION, NeII_ION, NeIII_ION, NeIV_ION, NeV_ION, NeVI_ION, NeVII_ION, NeVIII_ION,
  MgI_ION, MgII_ION, MgIII_ION, MgIV_ION, MgV_ION, MgVI_ION, MgVII_ION, MgVIII_ION, MgIX_ION, MgX_ION,
  SiI_ION, SiII_ION, SiIII_ION, SiIV_ION, SiV_ION, SiVI_ION, SiVII_ION, SiVIII_ION, SiIX_ION, SiX_ION, SiXI_ION, SiXII_ION,
  SI_ION, SII_ION, SIII_ION, SIV_ION, SV_ION, SVI_ION, SVII_ION, SVIII_ION, SIX_ION, SX_ION, SXI_ION, SXII_ION, SXIII_ION, SXIV_ION,
  FeI_ION, FeII_ION, FeIII_ION, FeIV_ION, FeV_ION, FeVI_ION, FeVII_ION, FeVIII_ION, FeIX_ION, FeX_ION, FeXI_ION, FeXII_ION, FeXIII_ION, FeXIV_ION, FeXV_ION, FeXVI_ION,
  n_ions,                                    // Number of absorbing ions
  H_SUM = n_ions, He_SUM, C_SUM, N_SUM, O_SUM, Ne_SUM, Mg_SUM, Si_SUM, S_SUM, Fe_SUM, // Sum indices
  n_ions_atoms,                              // Number of absorbing ions and atoms
  STATS_tot = n_ions_atoms, STATS_gas, STATS_dust, // Total, gas, and dust indices
  n_ion_stats                                // Max number of ionization statistics
};

constexpr array<int, n_atoms+1> atom_ranges = {
  HI_ION, HeI_ION, CI_ION, NI_ION, OI_ION, NeI_ION, MgI_ION, SiI_ION, SI_ION, FeI_ION, n_ions
};

constexpr array<int, n_atoms> atom_offsets = {
  HI_Fraction - HI_ION, HeI_Fraction - HeI_ION, CI_Fraction - CI_ION, NI_Fraction - NI_ION, OI_Fraction - OI_ION,
  NeI_Fraction - NeI_ION, MgI_Fraction - MgI_ION, SiI_Fraction - SiI_ION, SI_Fraction - SI_ION, FeI_Fraction - FeI_ION
};

extern const strings atom_names;             // Atom names
extern const strings atom_symbols;           // Atom symbols
extern const strings ion_names;              // Ionization state names
extern int n_active_atoms;                   // Number of active atoms
extern vector<int> active_atoms;             // Active atom indices
extern vector<int> ion_counts;               // Active atom number of active ions
extern int n_active_ions;                    // Number of active ions
extern int n_active_ions_atoms;              // Number of active ions and atoms
extern vector<int> beg_ions;                 // Active atom beginning ion indices
extern vector<int> end_ions;                 // Active atom ending ion indices
extern vector<int> active_ions;              // Active ionization state indices
extern vector<int> n_indices;                // Atom number density data indices
extern vector<int> x_indices;                // Ionization fraction data indices
extern vector<double> ions_eV;               // Ionization threshold energies [eV]
extern vector<double> ions_erg;              // Ionization threshold energies [erg]
extern int n_active_ion_stats;               // Number of active ionization statistics
extern vector<int> active_ion_stats;         // Active ionization statistics indices
extern vector<double> bin_edges_eV;          // Bin energy edges [eV]
extern strings bin_names;                    // Bin energy edge names

// List of possible lines (carrier, state, type)
enum LineCarrier {
  Hydrogen_Line = HydrogenDensity,           // Hydrogen atom
  Helium_Line = HeliumDensity,               // Helium atom
  Carbon_Line = CarbonDensity,               // Carbon atom
  Nitrogen_Line = NitrogenDensity,           // Nitrogen atom
  Oxygen_Line = OxygenDensity,               // Oxygen atom
  Neon_Line = NeonDensity,                   // Neon atom
  Magnesium_Line = MagnesiumDensity,         // Magnesium atom
  Silicon_Line = SiliconDensity,             // Silicon atom
  Sulfer_Line = SulferDensity,               // Sulfer atom
  Iron_Line = IronDensity,                   // Iron atom
  n_carriers                                 // Number of carriers
};
enum LineState {
  State_I = 0,                               // Fully neutral state (I)
  State_II,                                  // Singly ionized state (II)
  State_III,                                 // Doubly ionized state (III)
  State_IV,                                  // Triply ionized state (IV)
  State_V,                                   // Quadruply ionized state (V)
  State_VI,                                  // Quintuply ionized state (VI)
  State_VII,                                 // Sextuply ionized state (VII)
  State_VIII,                                // Septuply ionized state (VIII)
  State_IX,                                  // Octuply ionized state (IX)
  State_X,                                   // Nonuply ionized state (X)
  State_XI,                                  // Decuply ionized state (XI)
  State_XII,                                 // Undecuply ionized state (XII)
  State_XIII,                                // Duodecuply ionized state (XIII)
  State_XIV,                                 // Tredecuply ionized state (XIV)
  State_XV,                                  // Quattuordecuply ionized state (XV)
  State_XVI,                                 // Quindecuply ionized state (XVI)
  State_XVII,                                // Sexdecuply ionized state (XVII)
  // Number of included ionization states
  N_Hydrogen = State_II + 1,                 // HII
  N_Helium = State_III + 1,                  // HeIII
  N_Carbon = State_VII + 1,                  // CVII
  N_Nitrogen = State_VIII + 1,               // NVIII
  N_Oxygen = State_IX + 1,                   // OIX
  N_Neon = State_IX + 1,                     // NeIX
  N_Magnesium = State_XI + 1,                // MgXI
  N_Silicon = State_XIII + 1,                // SiXIII
  N_Sulfer = State_XV + 1,                   // SXV
  N_Iron = State_XVII + 1                    // FeXVII
};
enum LineType {
  TwoLevelAtom = 0,                          // Two level atom line
  HI_Line = HI_Fraction,                     // Alias lines by state fractions
  HeI_Line = HeI_Fraction, HeII_Line,        // Higher states are sequential
  CI_Line = CI_Fraction, CII_Line, CIII_Line, CIV_Line, CV_Line, CVI_Line,
  NI_Line = NI_Fraction, NII_Line, NIII_Line, NIV_Line, NV_Line, NVI_Line, NVII_Line,
  OI_Line = OI_Fraction, OII_Line, OIII_Line, OIV_Line, OV_Line, OVI_Line, OVII_Line, OVIII_Line,
  NeI_Line = NeI_Fraction, NeII_Line, NeIII_Line, NeIV_Line, NeV_Line, NeVI_Line, NeVII_Line, NeVIII_Line,
  MgI_Line = MgI_Fraction, MgII_Line, MgIII_Line, MgIV_Line, MgV_Line, MgVI_Line, MgVII_Line, MgVIII_Line, MgIX_Line, MgX_Line,
  SiI_Line = SiI_Fraction, SiII_Line, SiIII_Line, SiIV_Line, SiV_Line, SiVI_Line, SiVII_Line, SiVIII_Line, SiIX_Line, SiX_Line, SiXI_Line, SiXII_Line,
  SI_Line = SI_Fraction, SII_Line, SIII_Line, SIV_Line, SV_Line, SVI_Line, SVII_Line, SVIII_Line, SIX_Line, SX_Line, SXI_Line, SXII_Line, SXIII_Line, SXIV_Line,
  FeI_Line = FeI_Fraction, FeII_Line, FeIII_Line, FeIV_Line, FeV_Line, FeVI_Line, FeVII_Line, FeVIII_Line, FeIX_Line, FeX_Line, FeXI_Line, FeXII_Line, FeXIII_Line, FeXIV_Line, FeXV_Line, FeXVI_Line
};
extern int line_carrier;                     // Carrier of line
extern int line_state;                       // State of line
extern int line_type;                        // Type of line

// Enumerated list of dust species
enum DustSpecies {
  NO_DUST = -1,
#if graphite_dust
  GRAPHITE,                                  // Graphite dust
#endif
#if silicate_dust
  SILICATE,                                  // Silicate dust
#endif
#if graphite_scaled_PAH
  PAH,                                       // Polycyclic aromatic hydrocarbons
#endif
#if !graphite_dust && !silicate_dust && !graphite_scaled_PAH
  DUST,                                      // Dust species
#endif
  n_dust_species                             // Number of dust species
};
using dust_strings = array<string, n_dust_species>; // Dust names
using dust_vectors = array<vector<double>, n_dust_species>; // Dust properties

// Basic grid data
extern int n_cells;                          // Number of cells
extern int n_stars;                          // Number of stars
extern double n_cells_eff;                   // Effective number of cells: 1/<w>
extern double n_cells_effp;                  // Effective number of doublet cells: 1/<w>
extern double n_stars_eff;                   // Effective number of stars: 1/<w>
extern vector<Vec3> r;                       // Cell position [cm]
extern vector<double>& V;                    // Cell volume [cm^3]
extern array<Vec3, 2> bbox;                  // Bounding box [cm]
extern double max_bbox_width;                // Max bbox width [cm]

// Halo catalog data
extern bool use_all_halos;                   // Use all halos in the catalog
extern int halo_index;                       // Specific halo index
extern Vec3 halo_position;                   // Specific halo position [cm]
extern double halo_radius;                   // Specific halo radius [cm]

// Group catalog data
extern bool use_all_groups;                  // Use all groups in the catalog
extern bool filter_group_catalog;            // Filter the group catalog
extern int n_groups, n_ugroups;              // Number of groups (filtered and unfiltered)
extern vector<int> group_id, ugroup_id;      // Group ID (filtered and unfiltered)
extern vector<Vec3> v_grp;                   // Group velocity [km/s]
extern vector<Vec3> r_grp_vir, r_grp_gal;    // Group position [cm] (virial and galaxy)
extern vector<double> R_grp_vir, R_grp_gal;  // Group radius [cm]
extern vector<double> R2_grp_vir, R2_grp_gal; // Group radius squared [cm^2]
extern vector<int> group_id_cell;            // Gas group IDs
extern vector<int> group_id_star;            // Star group IDs

// Subhalo catalog data
extern bool use_all_subhalos;                // Use all subhalos in the catalog
extern bool filter_subhalo_catalog;          // Filter the subhalo catalog
extern int n_subhalos, n_usubhalos;          // Number of subhalos (filtered and unfiltered)
extern vector<int> subhalo_id, usubhalo_id;  // Subhalo ID (filtered and unfiltered)
extern vector<Vec3> v_sub;                   // Subhalo velocity [km/s]
extern vector<Vec3> r_sub_vir, r_sub_gal;    // Subhalo position [cm] (virial and galaxy)
extern vector<double> R_sub_vir, R_sub_gal;  // Subhalo radius [cm]
extern vector<double> R2_sub_vir, R2_sub_gal; // Subhalo radius squared [cm^2]
extern vector<int> subhalo_id_cell;          // Gas subhalo IDs
extern vector<int> subhalo_id_star;          // Star subhalo IDs

// Cartesian grid data
extern int nx;                               // Number of x cells
extern int ny;                               // Number of y cells
extern int nz;                               // Number of z cells
extern int n2;                               // 2D hyperslab size
extern double wx;                            // Cell x width [cm]
extern double wy;                            // Cell y width [cm]
extern double wz;                            // Cell z width [cm]
extern double dV;                            // Cell volume [cm^3]
#if CARTESIAN
#define VOLUME(x) dV
#else
#define VOLUME(x) V[x]
#endif

// Octree grid data (size = n_cells)
extern int n_leafs;                          // Number of leaf cells
extern vector<int> parent;                   // Cell parent
extern vector<array<int, 8>> child;          // Child cell indices
extern vector<bool> child_check;             // True if cell has children
extern vector<double> w;                     // Cell width [cm]

// Voronoi tessellation data
struct Face {
  int neighbor;                              // Neighbor index
  double area;                               // Face area [cm^2]
  vector<int> circulator;                    // Circulator indices
};
extern bool avoid_edges;                     // Avoid ray-tracing through edge cells
extern bool inner_edges;                     // Flag to also include inner edge cells
extern bool output_inner_edges;              // Flag to output inner edge cells
extern int n_edges;                          // Number of edge Voronoi cells
extern int n_inner_edges;                    // Number of inner edge Voronoi cells
extern neib_t n_neighbors_tot;               // Total number of neighbors for all cells
extern circ_t n_circulators_tot;             // Total number of circulators for all faces
extern vector<bool> edge_flag;               // Flag for edge cells
extern vector<vector<Face>> faces;           // Face connection data

// Additional data (size = n_cells)
extern bool continuous_velocity;             // Use a continuous velocity field
extern vector<Vec3> v;                       // Bulk velocity [vth]
extern vector<double> dvdr;                  // Bulk velocity gradient [vth/cm]
extern vector<double> v0;                    // Velocity extrapolation [vth/cm]
extern vector<double> r_c;                   // Cell center position [cm]
extern vector<Vec3> B;                       // Magnetic field [G]
extern vector<Vec3> a_rad, a_rad_scat;       // Radiation acceleration [cm/s^2]
extern vector<double>& rho;                  // Density [g/cm^3]
extern vector<double>& T;                    // Temperature [K]
extern vector<double>& e_int;                // Internal energy [cm^2/s^2]
extern vector<double>& n_H;                  // Hydrogen number density [cm^-3]
extern vector<double>& n_He;                 // Helium number density [cm^-3]
extern vector<double>& n_C;                  // Carbon number density [cm^-3]
extern vector<double>& n_N;                  // Nitrogen number density [cm^-3]
extern vector<double>& n_O;                  // Oxygen number density [cm^-3]
extern vector<double>& n_Ne;                 // Neon number density [cm^-3]
extern vector<double>& n_Mg;                 // Magnesium number density [cm^-3]
extern vector<double>& n_Si;                 // Silicon number density [cm^-3]
extern vector<double>& n_S;                  // Sulfer number density [cm^-3]
extern vector<double>& n_Fe;                 // Iron number density [cm^-3]
extern vector<double>& n_lower;              // Number density of lower transition [cm^-3]
extern vector<double>& n_upper;              // Number density of upper transition [cm^-3]
extern vector<double>& x_HI;                 // x_HI = n_HI / n_H
extern vector<double>& x_HII;                // x_HII = n_HII / n_H
extern vector<double>& x_H2;                 // x_H2 = n_H2 / n_H
extern vector<double> &x_HeI, &x_HeII, &x_HeIII; // x_ion = n_ion / n_atom
extern vector<double>& x_e;                  // Electron fraction: x_e = n_e / n_H
extern vector<double>& G_ion;                // Photoheating rate [erg/s]
extern vector<double>& SFR;                  // Star formation rate [Msun/yr]
extern vector<double>& T_dust;               // Dust temperature [K]
extern vector<double>& v2;                   // Velocity^2 [cm^2/s^2]
extern vector<double>& v2_LOS;               // Velocity_LOS^2 [cm^2/s^2]
extern vector<double>& v_LOS;                // Velocity_LOS [cm/s]
extern vector<double>& v_x;                  // Velocity_X [cm/s]
extern vector<double>& v_y;                  // Velocity_Y [cm/s]
extern vector<double>& B2;                   // Magnetic field^2 [G^2]
extern vector<double>& B_LOS;                // Magnetic field LOS [G]
extern vector<double>& B_x;                  // Magnetic field X [G]
extern vector<double>& B_y;                  // Magnetic field Y [G]
extern vector<double>& X;                    // Mass fraction of hydrogen
extern vector<double>& Y;                    // Mass fraction of helium
extern vector<double>& Z;                    // Gas metallicity [mass fraction]
extern vector<double>& Z_C;                  // Carbon metallicity [mass fraction]
extern vector<double>& Z_N;                  // Nitrogen metallicity [mass fraction]
extern vector<double>& Z_O;                  // Oxygen metallicity [mass fraction]
extern vector<double>& Z_Ne;                 // Neon metallicity [mass fraction]
extern vector<double>& Z_Mg;                 // Magnesium metallicity [mass fraction]
extern vector<double>& Z_Si;                 // Silicon metallicity [mass fraction]
extern vector<double>& Z_S;                  // Sulfer metallicity [mass fraction]
extern vector<double>& Z_Fe;                 // Iron metallicity [mass fraction]
extern vector<double>& D;                    // Dust-to-gas ratio [mass fraction]
extern vector<double>& D_S;                  // Silicate dust-to-gas ratio [mass fraction]
extern vector<double>& k_e;                  // Absorption coefficient of electrons [1/cm]
extern vector<double>& k_0;                  // Absorption coefficient of primary line [1/cm]
extern vector<double>& kp_0;                 // Absorption coefficient of doublet line [1/cm]
extern vector<double>& k_dust;               // Absorption coefficient of dust [1/cm]
extern vector<double>& rho_dust;             // Dust density [g/cm^3]
extern vector<double>& rho_dust_G;           // Graphite dust density [g/cm^3]
extern vector<double>& rho_dust_S;           // Silicate dust density [g/cm^3]
extern vector<double>& a;                    // "Damping parameter"
extern vector<double>& atau;                 // Nonlocal estimate of a * tau for each cell
extern vector<double>& j_line;               // Line emissivity [erg/s/cm^3]
extern vector<double>& jp_line;              // Doublet line emissivity [erg/s/cm^3]
extern vector<double>& u_rad;                // Radiation energy density [erg/cm^3]
extern vector<double>& a_rad_r;              // Radial radiation acceleration [cm/s^2]
extern vector<double>& a_rad_r_scat;         // Radial radiation acceleration (scattering-based) [cm/s^2]
extern vector<double>& P_rad_r;              // Radial radiation pressure [erg/cm^3]
extern vector<double>& f_src_r;              // Cumulative source fraction
extern vector<double>& tau_dust_f_esc;       // escape fraction varrying with tau_dust in gridless simulations
extern vector<double>& f_esc_r;              // Cumulative escape fraction
extern vector<double>& rescaled_f_esc_r;     // Rescaled cumulative escape fraction
extern vector<double>& trap_r;               // Cumulative t_trap / t_light
extern vector<double>& M_F_r;                // Cumulative force multiplier
extern vector<double>& M_F_r_scat;           // Cumulative force multiplier (scattering-based)
extern vector<double>& M_F_src;              // Cumulative force multiplier (by source)
extern vector<double>& M_F_src_scat;         // Cumulative force multiplier (by source, scattering-based)
extern vector<double>& P_u_r;                // Cumulative pressure-to-energy density ratio (P/u)
extern vector<double>& rho2;                 // Density^2 [g^2/cm^6]
extern vector<double>& rho2HI;               // x_HI * Density^2 [g^2/cm^6]
extern vector<double>& rho_star;             // Stellar density [g/cm^3]
extern vector<double>& ion_front;            // Ionization front
extern dust_strings dust_models;             // Dust model: SMC, MW, etc.
extern double T_sputter;                     // Thermal sputtering cutoff [K]
extern double f_ion;                         // HII region survival fraction
extern double f_dust;                        // Fraction of metals locked in dust
#if graphite_scaled_PAH
extern double f_PAH, fm1_PAH;                // Fraction of carbonaceous dust in PAHs
#endif
extern double& hydrogen_fraction;            // Constant hydrogen mass fraction
extern double& helium_fraction;              // Constant helium mass fraction
extern double& metallicity;                  // Constant metallicity
extern bool mass_weighted_metallicities;     // Adopt mass-weighted metallicities
extern double dust_to_metal;                 // Constant dust-to-metal ratio
extern double& dust_to_gas;                  // Constant dust-to-gas ratio
extern double dust_boost;                    // Optional dust boost factor
extern double& constant_temperature;         // Constant temperature: T [K]
extern double T_floor;                       // Apply a temperature floor: T [K]
extern double T_rtol;                        // Relative tolerance for temperature changes
extern double& electron_fraction;            // Constant electron fraction: x_e
extern bool use_internal_energy;             // Set temperature from the internal energy
extern bool set_density_from_mass;           // Calculate the density as mass / volume
extern bool read_density_as_mass;            // Read the density as a mass field
extern bool &read_helium;                    // Read n_He from the input file
extern bool &read_HI, &read_HII, &read_H2, &read_HeI, &read_HeII, &read_HeIII;
extern bool &read_electron_fraction;         // Read x_e from the input file
extern bool &read_hydrogen_fraction;         // Read mass fraction of hydrogen
extern bool &read_helium_fraction;           // Read mass fraction of helium
extern bool read_B;                          // Read magnetic field
extern bool save_line_emissivity;            // Save the line emissivity

// Star data for continuum radiative transfer  (size = n_stars)
extern int n_photons;                        // Actual number of photon packets used
extern int n_photons_per_star;               // Scale the number of photons with stars
extern int n_photons_max;                    // Maximum number of photons
extern vector<Vec3> r_star;                  // Star positions [cm]
extern vector<Vec3> v_star;                  // Star velocities [cm/s]
extern vector<double> L_cont_star;           // Star cont band [erg/s/angstrom]
extern vector<double> m_massive_star;        // Massive star masses [Msun]
extern vector<double> m_init_star;           // Initial star masses [Msun]
extern vector<double> Z_star;                // Star metallicities [mass fraction]
extern vector<double> age_star;              // Star ages [Gyr]
extern Vec3 r_AGN;                           // AGN position [cm]
extern double Lbol_AGN;                      // AGN bolometric luminosity [erg/s]
extern bool cell_based_emission;             // Include cell-based sources
extern bool star_based_emission;             // Include star-based sources
extern bool AGN_emission;                    // Include an AGN source
extern bool read_continuum;                  // Read continuum fluxes [erg/s/angstrom]
extern bool read_m_massive_star;             // Read massive star masses [Msun]
extern bool read_m_init_star;                // Read initial star masses [Msun]
extern bool read_Z_star;                     // Read star metallicities [mass fraction]
extern bool read_age_star;                   // Read star ages [Gyr]
extern bool read_v_star;                     // Read star velocities [cm/s]

// Information about escape criteria
#define check_escape (spherical_escape || box_escape || streaming_escape)
#if spherical_escape
extern Vec3 escape_center;                   // Center of the escape region [cm]
extern double escape_radius;                 // Radius for spherical escape [cm]
extern double escape_radius_bbox;            // Radius for spherical escape [min distance to bbox edge]
extern double escape_radius_Rvir;            // Radius for spherical escape [selected halo virial radius]
extern double escape_epsilon;                // Escape uncertainty [cm]
extern double escape_radius2;                // Escape radius^2 [cm^2]
extern double emission_radius;               // Radius for spherical emission [cm]
extern double emission_radius_bbox;          // Radius for spherical emission [min distance to bbox edge]
extern double emission_radius_Rvir;          // Radius for spherical emission [selected halo virial radius]
extern double emission_radius2;              // Emission radius^2 [cm^2]
#endif
#if box_escape
extern array<Vec3, 2> escape_box;            // Box for escape [cm]
extern array<Vec3, 2> emission_box;          // Box for emission [cm]
#endif
#if streaming_escape
extern double max_streaming;                 // Maximum path length without scattering [cm]
#endif

// Information about group photons (observed, virial, galaxy)
extern vector<double> Ndot_grp, Ndot_grp_vir, Ndot_grp_gal, Ndot_HI_grp, Ndot_HI_grp_vir, Ndot_HI_grp_gal; // Group photon rate [photons/s]
extern vector<double> Ndot_stars_grp, Ndot_stars_grp_vir, Ndot_stars_grp_gal, Ndot_stars_HI_grp, Ndot_stars_HI_grp_vir, Ndot_stars_HI_grp_gal; // Group stellar photon rate [photons/s]
extern vector<double> Ndot2_stars_grp, Ndot2_stars_grp_vir, Ndot2_stars_grp_gal, Ndot2_stars_HI_grp, Ndot2_stars_HI_grp_vir, Ndot2_stars_HI_grp_gal; // Group stellar rates^2 [photons^2/s^2]
extern vector<double> n_stars_eff_grp, n_stars_eff_grp_vir, n_stars_eff_grp_gal, n_stars_eff_HI_grp, n_stars_eff_HI_grp_vir, n_stars_eff_HI_grp_gal; // Effective number of stars: 1/<w>
extern vector<double> Ndot_gas_grp, Ndot_gas_grp_vir, Ndot_gas_grp_gal, Ndot_gas_HI_grp, Ndot_gas_HI_grp_vir, Ndot_gas_HI_grp_gal; // Group gas photon rate [photons/s]
extern vector<double> Ndot2_gas_grp, Ndot2_gas_grp_vir, Ndot2_gas_grp_gal, Ndot2_gas_HI_grp, Ndot2_gas_HI_grp_vir, Ndot2_gas_HI_grp_gal; // Group gas rates^2 [photons^2/s^2]
extern vector<double> n_cells_eff_grp, n_cells_eff_grp_vir, n_cells_eff_grp_gal, n_cells_eff_HI_grp, n_cells_eff_HI_grp_vir, n_cells_eff_HI_grp_gal; // Effective number of cells: 1/<w>
extern vector<double> f_src_grp, f_src_grp_vir, f_src_grp_gal, f_src_HI_grp, f_src_HI_grp_vir, f_src_HI_grp_gal; // Group source fraction: sum(w0)
extern vector<double> f2_src_grp, f2_src_grp_vir, f2_src_grp_gal, f2_src_HI_grp, f2_src_HI_grp_vir, f2_src_HI_grp_gal; // Group squared f_src: sum(w0^2)
extern vector<double> n_photons_src_grp, n_photons_src_grp_vir, n_photons_src_grp_gal, n_photons_src_HI_grp, n_photons_src_HI_grp_vir, n_photons_src_HI_grp_gal; // Effective number of emitted photons: 1/<w0>
extern vector<double> f_esc_grp, f_esc_grp_vir, f_esc_grp_gal, f_esc_HI_grp, f_esc_HI_grp_vir, f_esc_HI_grp_gal; // Group escape fraction: sum(w)
extern vector<double> f2_esc_grp, f2_esc_grp_vir, f2_esc_grp_gal, f2_esc_HI_grp, f2_esc_HI_grp_vir, f2_esc_HI_grp_gal; // Group squared f_esc: sum(w^2)
extern vector<double> n_photons_esc_grp, n_photons_esc_grp_vir, n_photons_esc_grp_gal, n_photons_esc_HI_grp, n_photons_esc_HI_grp_vir, n_photons_esc_HI_grp_gal; // Effective number of escaped photons: 1/<w>
extern vector<double> f_abs_grp, f_abs_grp_vir, f_abs_grp_gal, f_abs_HI_grp, f_abs_HI_grp_vir, f_abs_HI_grp_gal; // Group dust absorption fraction
extern vector<double> f2_abs_grp, f2_abs_grp_vir, f2_abs_grp_gal, f2_abs_HI_grp, f2_abs_HI_grp_vir, f2_abs_HI_grp_gal; // Group squared f_abs: sum(w^2)
extern vector<double> n_photons_abs_grp, n_photons_abs_grp_vir, n_photons_abs_grp_gal, n_photons_abs_HI_grp, n_photons_abs_HI_grp_vir, n_photons_abs_HI_grp_gal; // Effective number of absorbed photons: 1/<w>
extern bool focus_groups_on_emission;        // Center groups on emission (position and velocity)
extern vector<Vec3> r_light_grp, v_light_grp; // Group center of luminosity position [cm] and velocity [cm/s]
extern Image bin_Ndot_grp, bin_Ndot_grp_vir, bin_Ndot_grp_gal; // Group bin photon rate [photons/s]
extern Image bin_Ndot_stars_grp, bin_Ndot_stars_grp_vir, bin_Ndot_stars_grp_gal; // Group stellar bin photon rate [photons/s]
extern Image bin_Ndot2_stars_grp, bin_Ndot2_stars_grp_vir, bin_Ndot2_stars_grp_gal; // Group stellar bin rates^2 [photons^2/s^2]
extern Image bin_n_stars_eff_grp, bin_n_stars_eff_grp_vir, bin_n_stars_eff_grp_gal; // Bin effective number of stars: 1/<w>
extern Image bin_Ndot_gas_grp, bin_Ndot_gas_grp_vir, bin_Ndot_gas_grp_gal; // Group gas bin photon rate [photons/s]
extern Image bin_Ndot2_gas_grp, bin_Ndot2_gas_grp_vir, bin_Ndot2_gas_grp_gal; // Group gas bin rates^2 [photons^2/s^2]
extern Image bin_n_cells_eff_grp, bin_n_cells_eff_grp_vir, bin_n_cells_eff_grp_gal; // Bin effective number of cells: 1/<w>
extern Image bin_f_src_grp, bin_f_src_grp_vir, bin_f_src_grp_gal; // Group bin source fraction: sum(w0)
extern Image bin_f2_src_grp, bin_f2_src_grp_vir, bin_f2_src_grp_gal; // Group bin squared f_src: sum(w0^2)
extern Image bin_n_photons_src_grp, bin_n_photons_src_grp_vir, bin_n_photons_src_grp_gal; // Effective number of emitted photons: 1/<w0>
extern Image bin_f_esc_grp, bin_f_esc_grp_vir, bin_f_esc_grp_gal; // Group bin escape fraction: sum(w)
extern Image bin_f2_esc_grp, bin_f2_esc_grp_vir, bin_f2_esc_grp_gal; // Group bin squared f_esc: sum(w^2)
extern Image bin_n_photons_esc_grp, bin_n_photons_esc_grp_vir, bin_n_photons_esc_grp_gal; // Effective number of escaped photons: 1/<w>
extern Image bin_f_abs_grp, bin_f_abs_grp_vir, bin_f_abs_grp_gal; // Group bin dust absorption fraction
extern Image bin_f2_abs_grp, bin_f2_abs_grp_vir, bin_f2_abs_grp_gal; // Group bin squared f_abs: sum(w^2)
extern Image bin_n_photons_abs_grp, bin_n_photons_abs_grp_vir, bin_n_photons_abs_grp_gal; // Effective number of absorbed photons: 1/<w>
extern vector<double> Ndot_ugrp, Ndot_HI_ugrp; // Unfiltered group photon rate [photons/s]
extern vector<double> Ndot_stars_ugrp, Ndot_stars_HI_ugrp; // Unfiltered group stellar photon rate [photons/s]
extern vector<double> Ndot2_stars_ugrp, Ndot2_stars_HI_ugrp; // Unfiltered group stellar rates^2 [photons^2/s^2]
extern vector<double> n_stars_eff_ugrp, n_stars_eff_HI_ugrp; // Effective number of stars: 1/<w>
extern vector<double> Ndot_gas_ugrp, Ndot_gas_HI_ugrp; // Unfiltered group gas photon rate [photons/s]
extern vector<double> Ndot2_gas_ugrp, Ndot2_gas_HI_ugrp; // Unfiltered group gas rates^2 [photons^2/s^2]
extern vector<double> n_cells_eff_ugrp, n_cells_eff_HI_ugrp; // Effective number of cells: 1/<w>
extern vector<double> f_src_ugrp, f_src_HI_ugrp; // Unfiltered group source fraction: sum(w0)
extern vector<double> f2_src_ugrp, f2_src_HI_ugrp; // Unfiltered group squared f_src: sum(w0^2)
extern vector<double> n_photons_src_ugrp, n_photons_src_HI_ugrp; // Effective number of emitted photons: 1/<w0>
extern Image bin_Ndot_ugrp;                  // Unfiltered group bin photon rate [photons/s]
extern Image bin_Ndot_stars_ugrp;            // Unfiltered group stellar bin photon rate [photons/s]
extern Image bin_Ndot2_stars_ugrp;           // Unfiltered group stellar bin rates^2 [photons^2/s^2]
extern Image bin_n_stars_eff_ugrp;           // Bin effective number of stars: 1/<w>
extern Image bin_Ndot_gas_ugrp;              // Unfiltered group gas bin photon rate [photons/s]
extern Image bin_Ndot2_gas_ugrp;             // Unfiltered group gas bin rates^2 [photons^2/s^2]
extern Image bin_n_cells_eff_ugrp;           // Bin effective number of cells: 1/<w>
extern Image bin_f_src_ugrp;                 // Unfiltered group bin source fraction: sum(w0)
extern Image bin_f2_src_ugrp;                // Unfiltered group bin squared f_src: sum(w0^2)
extern Image bin_n_photons_src_ugrp;         // Effective number of emitted photons: 1/<w0>

// Information about subhalo photons (observed, virial, galaxy)
extern vector<double> Ndot_sub, Ndot_sub_vir, Ndot_sub_gal, Ndot_HI_sub, Ndot_HI_sub_vir, Ndot_HI_sub_gal; // Subhalo photon rate [photons/s]
extern vector<double> Ndot_stars_sub, Ndot_stars_sub_vir, Ndot_stars_sub_gal, Ndot_stars_HI_sub, Ndot_stars_HI_sub_vir, Ndot_stars_HI_sub_gal; // Subhalo stellar photon rate [photons/s]
extern vector<double> Ndot2_stars_sub, Ndot2_stars_sub_vir, Ndot2_stars_sub_gal, Ndot2_stars_HI_sub, Ndot2_stars_HI_sub_vir, Ndot2_stars_HI_sub_gal; // Subhalo stellar rates^2 [photons^2/s^2]
extern vector<double> n_stars_eff_sub, n_stars_eff_sub_vir, n_stars_eff_sub_gal, n_stars_eff_HI_sub, n_stars_eff_HI_sub_vir, n_stars_eff_HI_sub_gal; // Effective number of stars: 1/<w>
extern vector<double> Ndot_gas_sub, Ndot_gas_sub_vir, Ndot_gas_sub_gal, Ndot_gas_HI_sub, Ndot_gas_HI_sub_vir, Ndot_gas_HI_sub_gal; // Subhalo gas photon rate [photons/s]
extern vector<double> Ndot2_gas_sub, Ndot2_gas_sub_vir, Ndot2_gas_sub_gal, Ndot2_gas_HI_sub, Ndot2_gas_HI_sub_vir, Ndot2_gas_HI_sub_gal; // Subhalo gas rates^2 [photons^2/s^2]
extern vector<double> n_cells_eff_sub, n_cells_eff_sub_vir, n_cells_eff_sub_gal, n_cells_eff_HI_sub, n_cells_eff_HI_sub_vir, n_cells_eff_HI_sub_gal; // Effective number of cells: 1/<w>
extern vector<double> f_src_sub, f_src_sub_vir, f_src_sub_gal, f_src_HI_sub, f_src_HI_sub_vir, f_src_HI_sub_gal; // Subhalo source fraction: sum(w0)
extern vector<double> f2_src_sub, f2_src_sub_vir, f2_src_sub_gal, f2_src_HI_sub, f2_src_HI_sub_vir, f2_src_HI_sub_gal; // Subhalo squared f_src: sum(w0^2)
extern vector<double> n_photons_src_sub, n_photons_src_sub_vir, n_photons_src_sub_gal, n_photons_src_HI_sub, n_photons_src_HI_sub_vir, n_photons_src_HI_sub_gal; // Effective number of emitted photons: 1/<w0>
extern vector<double> f_esc_sub, f_esc_sub_vir, f_esc_sub_gal, f_esc_HI_sub, f_esc_HI_sub_vir, f_esc_HI_sub_gal; // Subhalo escape fraction: sum(w)
extern vector<double> f2_esc_sub, f2_esc_sub_vir, f2_esc_sub_gal, f2_esc_HI_sub, f2_esc_HI_sub_vir, f2_esc_HI_sub_gal; // Subhalo squared f_esc: sum(w^2)
extern vector<double> n_photons_esc_sub, n_photons_esc_sub_vir, n_photons_esc_sub_gal, n_photons_esc_HI_sub, n_photons_esc_HI_sub_vir, n_photons_esc_HI_sub_gal; // Effective number of escaped photons: 1/<w>
extern vector<double> f_abs_sub, f_abs_sub_vir, f_abs_sub_gal, f_abs_HI_sub, f_abs_HI_sub_vir, f_abs_HI_sub_gal; // Subhalo dust absorption fraction
extern vector<double> f2_abs_sub, f2_abs_sub_vir, f2_abs_sub_gal, f2_abs_HI_sub, f2_abs_HI_sub_vir, f2_abs_HI_sub_gal; // Subhalo squared f_abs: sum(w^2)
extern vector<double> n_photons_abs_sub, n_photons_abs_sub_vir, n_photons_abs_sub_gal, n_photons_abs_HI_sub, n_photons_abs_HI_sub_vir, n_photons_abs_HI_sub_gal; // Effective number of absorbed photons: 1/<w>
extern bool focus_subhalos_on_emission;      // Center subhalos on emission (position and velocity)
extern vector<Vec3> r_light_sub, v_light_sub; // Subhalo center of luminosity position [cm] and velocity [cm/s]
extern Image bin_Ndot_sub, bin_Ndot_sub_vir, bin_Ndot_sub_gal; // Subhalo bin photon rate [photons/s]
extern Image bin_Ndot_stars_sub, bin_Ndot_stars_sub_vir, bin_Ndot_stars_sub_gal; // Subhalo stellar bin photon rate [photons/s]
extern Image bin_Ndot2_stars_sub, bin_Ndot2_stars_sub_vir, bin_Ndot2_stars_sub_gal; // Subhalo stellar bin rates^2 [photons^2/s^2]
extern Image bin_n_stars_eff_sub, bin_n_stars_eff_sub_vir, bin_n_stars_eff_sub_gal; // Bin effective number of stars: 1/<w>
extern Image bin_Ndot_gas_sub, bin_Ndot_gas_sub_vir, bin_Ndot_gas_sub_gal; // Subhalo gas bin photon rate [photons/s]
extern Image bin_Ndot2_gas_sub, bin_Ndot2_gas_sub_vir, bin_Ndot2_gas_sub_gal; // Subhalo gas bin rates^2 [photons^2/s^2]
extern Image bin_n_cells_eff_sub, bin_n_cells_eff_sub_vir, bin_n_cells_eff_sub_gal; // Bin effective number of cells: 1/<w>
extern Image bin_f_src_sub, bin_f_src_sub_vir, bin_f_src_sub_gal; // Subhalo bin source fraction: sum(w0)
extern Image bin_f2_src_sub, bin_f2_src_sub_vir, bin_f2_src_sub_gal; // Subhalo bin squared f_src: sum(w0^2)
extern Image bin_n_photons_src_sub, bin_n_photons_src_sub_vir, bin_n_photons_src_sub_gal; // Effective number of emitted photons: 1/<w0>
extern Image bin_f_esc_sub, bin_f_esc_sub_vir, bin_f_esc_sub_gal; // Subhalo bin escape fraction: sum(w)
extern Image bin_f2_esc_sub, bin_f2_esc_sub_vir, bin_f2_esc_sub_gal; // Subhalo bin squared f_esc: sum(w^2)
extern Image bin_n_photons_esc_sub, bin_n_photons_esc_sub_vir, bin_n_photons_esc_sub_gal; // Effective number of escaped photons: 1/<w>
extern Image bin_f_abs_sub, bin_f_abs_sub_vir, bin_f_abs_sub_gal; // Subhalo bin dust absorption fraction
extern Image bin_f2_abs_sub, bin_f2_abs_sub_vir, bin_f2_abs_sub_gal; // Subhalo bin squared f_abs: sum(w^2)
extern Image bin_n_photons_abs_sub, bin_n_photons_abs_sub_vir, bin_n_photons_abs_sub_gal; // Effective number of absorbed photons: 1/<w>
extern vector<double> Ndot_usub, Ndot_HI_usub; // Unfiltered subhalo photon rate [photons/s]
extern vector<double> Ndot_stars_usub, Ndot_stars_HI_usub; // Unfiltered subhalo stellar photon rate [photons/s]
extern vector<double> Ndot2_stars_usub, Ndot2_stars_HI_usub; // Unfiltered subhalo stellar rates^2 [photons^2/s^2]
extern vector<double> n_stars_eff_usub, n_stars_eff_HI_usub; // Effective number of stars: 1/<w>
extern vector<double> Ndot_gas_usub, Ndot_gas_HI_usub; // Unfiltered subhalo gas photon rate [photons/s]
extern vector<double> Ndot2_gas_usub, Ndot2_gas_HI_usub; // Unfiltered subhalo gas rates^2 [photons^2/s^2]
extern vector<double> n_cells_eff_usub, n_cells_eff_HI_usub; // Effective number of cells: 1/<w>
extern vector<double> f_src_usub, f_src_HI_usub; // Unfiltered subhalo source fraction: sum(w0)
extern vector<double> f2_src_usub, f2_src_HI_usub; // Unfiltered subhalo squared f_src: sum(w0^2)
extern vector<double> n_photons_src_usub, n_photons_src_HI_usub; // Effective number of emitted photons: 1/<w0>
extern Image bin_Ndot_usub;                  // Unfiltered subhalo bin photon rate [photons/s]
extern Image bin_Ndot_stars_usub;            // Unfiltered subhalo stellar bin photon rate [photons/s]
extern Image bin_Ndot2_stars_usub;           // Unfiltered subhalo stellar bin rates^2 [photons^2/s^2]
extern Image bin_n_stars_eff_usub;           // Bin effective number of stars: 1/<w>
extern Image bin_Ndot_gas_usub;              // Unfiltered subhalo gas bin photon rate [photons/s]
extern Image bin_Ndot2_gas_usub;             // Unfiltered subhalo gas bin rates^2 [photons^2/s^2]
extern Image bin_n_cells_eff_usub;           // Bin effective number of cells: 1/<w>
extern Image bin_f_src_usub;                 // Unfiltered subhalo bin source fraction: sum(w0)
extern Image bin_f2_src_usub;                // Unfiltered subhalo bin squared f_src: sum(w0^2)
extern Image bin_n_photons_src_usub;         // Effective number of emitted photons: 1/<w0>

// Information about group line photons (observed, virial, galaxy)
extern vector<double> L_grp, L_grp_vir, L_grp_gal; // Group photon rate [photons/s]
extern vector<double> L_stars_grp, L_stars_grp_vir, L_stars_grp_gal; // Group stellar photon rate [photons/s]
extern vector<double> L2_stars_grp, L2_stars_grp_vir, L2_stars_grp_gal; // Group stellar rates^2 [photons^2/s^2]
extern vector<double> L_gas_grp, L_gas_grp_vir, L_gas_grp_gal; // Group gas photon rate [photons/s]
extern vector<double> L2_gas_grp, L2_gas_grp_vir, L2_gas_grp_gal; // Group gas rates^2 [photons^2/s^2]
extern vector<double> L_ugrp;                // Unfiltered group photon rate [photons/s]
extern vector<double> L_stars_ugrp;          // Unfiltered group stellar photon rate [photons/s]
extern vector<double> L2_stars_ugrp;         // Unfiltered group stellar rates^2 [photons^2/s^2]
extern vector<double> L_gas_ugrp;            // Unfiltered group gas photon rate [photons/s]
extern vector<double> L2_gas_ugrp;           // Unfiltered group gas rates^2 [photons^2/s^2]

// Information about subhalo line photons (observed, virial, galaxy)
extern vector<double> L_sub, L_sub_vir, L_sub_gal; // Subhalo photon rate [photons/s]
extern vector<double> L_stars_sub, L_stars_sub_vir, L_stars_sub_gal; // Subhalo stellar photon rate [photons/s]
extern vector<double> L2_stars_sub, L2_stars_sub_vir, L2_stars_sub_gal; // Subhalo stellar rates^2 [photons^2/s^2]
extern vector<double> L_gas_sub, L_gas_sub_vir, L_gas_sub_gal; // Subhalo gas photon rate [photons/s]
extern vector<double> L2_gas_sub, L2_gas_sub_vir, L2_gas_sub_gal; // Subhalo gas rates^2 [photons^2/s^2]
extern vector<double> L_usub;                // Unfiltered subhalo photon rate [photons/s]
extern vector<double> L_stars_usub;          // Unfiltered subhalo stellar photon rate [photons/s]
extern vector<double> L2_stars_usub;         // Unfiltered subhalo stellar rates^2 [photons^2/s^2]
extern vector<double> L_gas_usub;            // Unfiltered subhalo gas photon rate [photons/s]
extern vector<double> L2_gas_usub;           // Unfiltered subhalo gas rates^2 [photons^2/s^2]

// Information about dual grids
extern Vec3 radial_center;                   // Center of the radial grid [cm]
extern Vec3 radial_motion;                   // Motion of the radial grid [cm/s]
extern bool focus_radial_on_emission;        // Focus radial grid on emission (position)
extern bool shift_radial_on_emission;        // Shift radial grid on emission (velocity)

// Information about the cameras
extern bool have_cameras;                    // Camera-based output
extern int n_cameras;                        // Number of cameras
extern int n_side;                           // Healpix parameter for cameras
extern int n_rot;                            // Number of rotation directions
extern Vec3 rotation_axis;                   // Rotation axis, e.g. (0,0,1)
extern int n_side_atau;                      // Healpix parameter for a*tau0
extern vector<Vec3> camera_directions;       // Normalized camera directions
extern vector<Vec3> camera_xaxes;            // Camera "x-axis" direction vectors (x,y,z)
extern vector<Vec3> camera_yaxes;            // Camera "y-axis" direction vectors (x,y,z)
extern Vec3 camera_center;                   // Camera target position [cm]
extern Vec3 camera_motion;                   // Camera target velocity [cm/s]
extern Vec3 camera_north;                    // Camera north orientation
extern Vec3 r_light;                         // Center of luminosity position [cm]
extern Vec3 r2_light;                        // Center of luminosity position^2 [cm^2]
extern Vec3 v_light;                         // Center of luminosity velocity [cm/s]
extern Vec3 v2_light;                        // Center of luminosity velocity^2 [cm^2/s^2]
extern Vec3 j_light;                         // Center of luminosity angular momentum [cm^2/s]
extern bool focus_cameras_on_emission;       // Focus cameras on emission (position)
extern bool shift_cameras_on_emission;       // Shift cameras on emission (velocity)
extern bool align_cameras_on_emission;       // Align cameras on emission (angular momentum)

// Information about the images
extern Vec2 n_pixels;                        // Number of (x,y) image pixels
extern int nx_pixels;                        // Number of x image pixels
extern int ny_pixels;                        // Number of y image pixels
extern vector<double> image_xedges;          // Image relative x edge positions [cm]
extern vector<double> image_yedges;          // Image relative y edge positions [cm]
extern vector<double> image_zedges;          // Image relative z edge positions [cm]
extern Vec2 image_widths;                    // Image widths (x,y) [cm] (defaults to entire domain)
extern Vec2 image_widths_cMpc;               // Image widths (x,y) [cMpc]
extern Vec2 image_radii;                     // Image radii (x,y) [cm] (image_widths / 2)
extern Vec2 image_radii_bbox;                // Image radii (x,y) [min distance to bbox edge]
extern Vec2 image_radii_Rvir;                // Image radii (x,y) [selected halo virial radius]
extern Vec2 pixel_widths;                    // Image pixel widths (x,y) [cm]
extern double pixel_area;                    // Pixel area [cm^2]
extern Vec2 inverse_pixel_widths;            // Inverse image pixel widths (x,y) [1/cm]
extern Vec2 pixel_offsets;                   // Image radii / pixel widths
extern Vec2 pixel_arcsecs;                   // Image pixel widths [arcsec] (optional)
extern double pixel_arcsec2;                 // Image pixel area [arcsec^2] (optional)
extern double aperture_radius;               // Aperture radius [cm]
extern double aperture_radius2;              // Aperture radius^2 [cm^2]
extern double aperture_radius_bbox;          // Aperture radius [min distance to bbox edge]
extern double aperture_radius_Rvir;          // Aperture radius [selected halo virial radius]
extern int n_bins;                           // Number of frequency bins
extern int n_slices;                         // Number of LOS projection slices
extern double freq_min;                      // Generic frequency extrema [freq units]
extern double freq_max;                      // Calculated when freq is initialized
extern double freq_bin_width;                // Frequency bin width [freq units]
extern double observed_bin_width;            // Observed wavelength resolution [angstrom]
extern double inverse_freq_bin_width;        // Frequency bin width^-1 [freq units]
extern double rgb_freq_min;                  // RGB frequency lower limit [freq units]
extern double rgb_freq_max;                  // RGB frequency upper limit [freq units]
extern double inverse_rgb_freq_bin_width;    // RGB frequency bin width^-1 [freq units]
extern double Dv_cont_min;                   // Continuum velocity offset lower limit [km/s]
extern double Dv_cont_max;                   // Continuum velocity offset upper limit [km/s]
extern double Dv_cont_res;                   // Continuum velocity offset resolution [km/s]
extern double l_cont_res;                    // Continuum wavelength resolution [angstrom]
extern double R_cont_res;                    // Spectral resolution: R = lambda / Delta_lambda
extern double proj_depth;                    // Projection depth [cm] (defaults to image_width)
extern double proj_depth_cMpc;               // Projection depth [cMpc]
extern double proj_radius;                   // Projection radius [cm] (proj_depth / 2)
extern double proj_radius_bbox;              // Projection radius [min distance to bbox edge]
extern double proj_radius_Rvir;              // Projection radius [selected halo virial radius]
extern bool adaptive;                        // Adaptive convergence projections
extern double pixel_rtol;                    // Relative tolerance per pixel
extern bool perspective;                     // Perspective camera projections
extern bool monoscopic;                      // 360 degree view
extern double stepback_factor;               // Perspective stepback factor (relative to radius)
extern double shape_factor;                  // Extraction shape factor (relative to radius)
extern bool override_volume;                 // Override projection volume normalization
extern bool proj_sphere;                     // Project through a spherical volume
extern bool proj_cylinder;                   // Project through a cylindrical volume
extern bool output_flux_avg;                 // Output the angle-averaged flux
extern vector<double> flux_avg;              // Angle-averaged flux [erg/s/cm^2/angstrom]
extern bool output_radial_avg;               // Output the angle-averaged radial surface brightness
extern vector<double> radial_avg;            // Angle-averaged radial profile [erg/s/cm^2/arcsec^2]
extern bool output_radial_cube_avg;          // Output the angle-averaged radial spectral data cube
extern Image radial_cube_avg;                // Angle-averaged radial spectral profile [erg/s/cm^2/arcsec^2/angstrom]
extern bool output_escape_fractions;         // Output camera escape fractions
extern vector<double> f_escs;                // Escape fractions [fraction]
extern bool output_freq_avgs;                // Output frequency averages
extern vector<double> freq_avgs;             // Frequency averages [freq units]
extern bool output_freq_stds;                // Output frequency standard deviations
extern vector<double> freq_stds;             // Standard deviations [freq units]
extern bool output_freq_skews;               // Output frequency skewnesses
extern vector<double> freq_skews;            // Frequency skewnesses [normalized]
extern bool output_freq_kurts;               // Output frequency kurtoses
extern vector<double> freq_kurts;            // Frequency kurtoses [normalized]
extern bool output_fluxes;                   // Output spectral fluxes
extern vectors fluxes;                       // Spectral fluxes [erg/s/cm^2/angstrom]
extern bool output_images;                   // Output surface brightness images
extern Images images;                        // Surface brightness images [erg/s/cm^2/arcsec^2]
extern bool output_images2;                  // Output statistical moment images
extern Images images2;                       // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
extern bool output_freq_images;              // Output average frequency images
extern Images freq_images;                   // Average frequency images [freq units]
extern bool output_freq2_images;             // Output frequency^2 images
extern Images freq2_images;                  // Frequency^2 images [freq^2 units]
extern bool output_freq3_images;             // Output frequency^3 images
extern Images freq3_images;                  // Frequency^3 images [freq^3 units]
extern bool output_freq4_images;             // Output frequency^4 images
extern Images freq4_images;                  // Frequency^4 images [freq^4 units]
extern bool output_rgb_images;               // Output flux-weighted frequency RGB images
extern Image3s rgb_images;                   // Flux-weighted frequency RGB images

// Information about the spectral slits
extern int n_slit_pixels;                    // Number of slit pixels
// extern vector<double> slit_edges;            // Slit relative edge positions [cm]
extern double slit_width;                    // Slit width [cm]
extern double slit_width_cMpc;               // Slit width [cMpc]
extern double slit_radius;                   // Slit radius [cm] (slit_width / 2)
extern double slit_radius_bbox;              // Slit radius [min distance to bbox edge]
extern double slit_radius_Rvir;              // Slit radius [selected halo virial radius]
extern double slit_pixel_width;              // Slit pixel width [cm]
extern double slit_pixel_area;               // Slit pixel area [cm^2]
extern double inverse_slit_pixel_width;      // Inverse slit pixel width [1/cm]
extern double slit_pixel_offset;             // Slit radius / pixel width
extern double slit_pixel_arcsec;             // Slit pixel width [arcsec] (optional)
extern double slit_pixel_arcsec2;            // Slit pixel area [arcsec^2] (optional)
extern double slit_aperture;                 // Slit aperture [cm]
extern double slit_aperture_arcsec;          // Slit aperture [arcsec]
extern double slit_half_aperture;            // Slit half aperture [cm]
extern int n_slit_bins;                      // Number of slit frequency bins
extern double slit_freq_min;                 // Slit frequency extrema [freq units]
extern double slit_freq_max;                 // Calculated when freq is initialized
extern double slit_freq_bin_width;           // Slit frequency bin width [freq units]
extern double observed_slit_bin_width;       // Observed slit wavelength resolution [angstrom]
extern double inverse_slit_freq_bin_width;   // Slit frequency bin width^-1 [freq units]
extern bool output_slits;                    // Output spectral slits
extern Images slits_x, slits_y;              // Spectral slits [erg/s/cm^2/arcsec^2/angstrom]

// Information about the spectral data cubes
extern Vec2 n_cube_pixels;                   // Number of (x,y) cube pixels
extern int nx_cube_pixels;                   // Number of x cube pixels
extern int ny_cube_pixels;                   // Number of y cube pixels
extern vector<double> cube_xedges;           // Cube relative x edge positions [cm]
extern vector<double> cube_yedges;           // Cube relative y edge positions [cm]
extern Vec2 cube_widths;                     // Cube widths (x,y) [cm] (defaults to entire domain)
extern Vec2 cube_widths_cMpc;                // Cube widths (x,y) [cMpc]
extern Vec2 cube_radii;                      // Cube radii (x,y) [cm] (cube_widths / 2)
extern Vec2 cube_radii_bbox;                 // Cube radii (x,y) [min distance to bbox edge]
extern Vec2 cube_radii_Rvir;                 // Cube radii (x,y) [selected halo virial radius]
extern Vec2 cube_pixel_widths;               // Cube pixel widths (x,y) [cm]
extern double cube_pixel_area;               // Cube pixel area [cm^2]
extern Vec2 inverse_cube_pixel_widths;       // Inverse cube pixel widths (x,y) [1/cm]
extern Vec2 cube_pixel_offsets;              // Cube radii / pixel widths
extern Vec2 cube_pixel_arcsecs;              // Cube pixel widths [arcsec] (optional)
extern double cube_pixel_arcsec2;            // Cube pixel area [arcsec^2] (optional)
extern int n_cube_bins;                      // Number of cube frequency bins
extern double cube_freq_min;                 // Cube frequency extrema [freq units]
extern double cube_freq_max;                 // Calculated when freq is initialized
extern double cube_freq_bin_width;           // Cube frequency bin width [freq units]
extern double observed_cube_bin_width;       // Observed cube wavelength resolution [angstrom]
extern double inverse_cube_freq_bin_width;   // Cube frequency bin width^-1 [freq units]
extern bool output_cubes;                    // Output spectral data cubes
extern Cubes cubes;                          // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

//! Project a point onto a camera
inline Vec2 project(const Vec3& point, const int camera) {
  const Vec3 r_cam = point - camera_center;  // Position relative to camera center
  return {dot(r_cam, camera_xaxes[camera]), dot(r_cam, camera_yaxes[camera])};
}

// Information about the radial cameras
extern bool have_radial_cameras;             // Radial-camera-based output
extern int n_radial_pixels;                  // Number of radial image pixels
extern double radial_image_radius;           // Radial image radius [cm] (defaults to half domain)
extern double radial_image_radius_bbox;      // Radial image radius [min distance to bbox edge]
extern double radial_image_radius_Rvir;      // Radial image radius [selected halo virial radius]
extern double radial_pixel_width;            // Radial pixel width [cm]
extern vector<double> radial_pixel_areas;    // Radial pixel areas [cm^2]
extern double inverse_radial_pixel_width;    // Inverse radial pixel width [1/cm]
extern double radial_pixel_arcsec;           // Radial pixel width [arcsec] (optional)
extern vector<double> radial_pixel_arcsec2;  // Radial pixel areas [arcsec^2] (optional)
extern bool output_radial_images;            // Output radial surface brightness images
extern vectors radial_images;                // Radial surface brightness images [erg/s/cm^2/arcsec^2]
extern bool output_radial_images2;           // Output statistical moment radial images
extern vectors radial_images2;               // Statistical moment radial images [erg^2/s^2/cm^4/arcsec^4]
extern bool output_freq_radial_images;       // Output average frequency radial images
extern vectors freq_radial_images;           // Average frequency radial images [freq units]
extern bool output_freq2_radial_images;      // Output frequency^2 radial images
extern vectors freq2_radial_images;          // Frequency^2 radial images [freq^2 units]
extern bool output_freq3_radial_images;      // Output frequency^3 radial images
extern vectors freq3_radial_images;          // Frequency^3 radial images [freq^3 units]
extern bool output_freq4_radial_images;      // Output frequency^4 radial images
extern vectors freq4_radial_images;          // Frequency^4 radial images [freq^4 units]
extern bool output_rgb_radial_images;        // Output flux-weighted frequency RGB radial images
extern vector<vector<Vec3>> rgb_radial_images; // Flux-weighted frequency RGB radial images

// Information about the radial spectral data cubes
extern int n_radial_cube_pixels;             // Number of radial cube pixels
extern double radial_cube_radius;            // Radial cube radius [cm] (defaults to half domain)
extern double radial_cube_radius_bbox;       // Radial cube radius [min distance to bbox edge]
extern double radial_cube_radius_Rvir;       // Radial cube radius [selected halo virial radius]
extern double radial_cube_pixel_width;       // Radial cube pixel width [cm]
extern vector<double> radial_cube_pixel_areas; // Radial cube pixel areas [cm^2]
extern double inverse_radial_cube_pixel_width; // Inverse radial cube pixel width [1/cm]
extern double radial_cube_pixel_arcsec;      // Radial cube pixel width [arcsec] (optional)
extern vector<double> radial_cube_pixel_arcsec2; // Radial cube pixel areas [arcsec^2] (optional)
extern int n_radial_cube_bins;               // Number of radial cube frequency bins
extern double radial_cube_freq_min;          // Radial cube frequency extrema [freq units]
extern double radial_cube_freq_max;          // Calculated when freq is initialized
extern double radial_cube_freq_bin_width;    // Radial cube frequency bin width [freq units]
extern double observed_radial_cube_bin_width; // Observed radial cube wavelength resolution [angstrom]
extern double inverse_radial_cube_freq_bin_width; // Radial cube frequency bin width^-1 [freq units]
extern bool output_radial_cubes;             // Output radial spectral data cubes
extern Images radial_cubes;                  // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about the line-of-sight healpix maps
extern int n_side_map;                       // Healpix parameter for maps
extern bool output_map;                      // Output line-of-sight healpix map
extern vector<double> map;                   // Escape fractions [fraction]
extern bool output_map2;                     // Output healpix map^2
extern vector<double> map2;                  // Escape fractions^2 [statistic]
extern bool output_freq_map;                 // Output average frequency map
extern vector<double> freq_map;              // Average frequency map [freq units]
extern bool output_freq2_map;                // Output frequency^2 map
extern vector<double> freq2_map;             // Frequency^2 map [freq^2 units]
extern bool output_freq3_map;                // Output frequency^3 map
extern vector<double> freq3_map;             // Frequency^3 map [freq^3 units]
extern bool output_freq4_map;                // Output frequency^4 map
extern vector<double> freq4_map;             // Frequency^4 map [freq^4 units]
extern bool output_rgb_map;                  // Output flux-weighted frequency RGB map
extern vector<Vec3> rgb_map;                 // Flux-weighted frequency RGB map

// Information about the line-of-sight healpix flux map
extern int n_side_flux;                      // Healpix parameter for the flux map
extern int n_map_bins;                       // Number of flux map frequency bins
extern double map_freq_min;                  // Flux map frequency extrema [freq units]
extern double map_freq_max;                  // Calculated when freq is initialized
extern double map_freq_bin_width;            // Flux map frequency bin width [freq units]
extern double observed_map_bin_width;        // Observed flux map wavelength resolution [angstrom]
extern double inverse_map_freq_bin_width;    // Flux map frequency bin width^-1 [freq units]
extern bool output_flux_map;                 // Output line-of-sight healpix flux map
extern Image flux_map;                       // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight healpix radial map
extern int n_side_radial;                    // Healpix parameter for the radial map
extern int n_map_pixels;                     // Number of radial map radial bins
extern double map_radius;                    // Radial map radius [cm] (defaults to half domain)
extern double map_radius_bbox;               // Radial map radius [min distance to bbox edge]
extern double map_radius_Rvir;               // Radial map radius [selected halo virial radius]
extern double map_pixel_width;               // Radial map pixel width [cm]
extern vector<double> map_pixel_areas;       // Radial map pixel areas [cm^2]
extern double inverse_map_pixel_width;       // Inverse radial map pixel width [1/cm]
extern double map_pixel_arcsec;              // Radial map pixel width [arcsec] (optional)
extern vector<double> map_pixel_arcsec2;     // Radial map pixel areas [arcsec^2] (optional)
extern bool output_radial_map;               // Output line-of-sight healpix radial profiles
extern Image radial_map;                     // Line-of-sight healpix radial profiles [erg/s/cm^2/arcsec^2]

// Information about the line-of-sight healpix radial spectral map
extern int n_side_cube;                      // Healpix parameter for the radial spectral map
extern int n_exp_cube;                       // Healpix exponent for the radial spectral map
extern int n_cube_map_pixels;                // Number of radial spectral map radial bins
extern double cube_map_radius;               // Radial spectral map radius [cm] (defaults to half domain)
extern double cube_map_radius_bbox;          // Radial spectral map radius [min distance to bbox edge]
extern double cube_map_radius_Rvir;          // Radial spectral map radius [selected halo virial radius]
extern double cube_map_pixel_width;          // Radial spectral map pixel width [cm]
extern vector<double> cube_map_pixel_areas;  // Radial spectral map pixel areas [cm^2]
extern double inverse_cube_map_pixel_width;  // Inverse radial spectral map pixel width [1/cm]
extern double cube_map_pixel_arcsec;         // Radial spectral map pixel width [arcsec] (optional)
extern vector<double> cube_map_pixel_arcsec2; // Radial spectral map pixel areas [arcsec^2] (optional)
extern int n_cube_map_bins;                  // Number of radial spectral map frequency bins
extern double cube_map_freq_min;             // Radial spectral map frequency extrema [freq units]
extern double cube_map_freq_max;             // Calculated when freq is initialized
extern double cube_map_freq_bin_width;       // Radial spectral map frequency bin width [freq units]
extern double observed_cube_map_bin_width;   // Observed radial spectral map wavelength resolution [angstrom]
extern double inverse_cube_map_freq_bin_width; // Flux map frequency bin width^-1 [freq units]
extern bool output_cube_map;                 // Output line-of-sight healpix radial spectral map
extern Cube cube_map;                        // Line-of-sight healpix radial spectral map [erg/s/cm^2/arcsec^2/angstrom]

// Information about the line-of-sight angular cosine (μ=cosθ) map
extern Vec3 mu_north;                        // Angular north orientation
extern int n_mu;                             // Number of mu bins
extern double inverse_dmu;                   // Cosine bin width^-1
extern bool output_mu;                       // Output line-of-sight mu map
extern vector<double> mu1;                   // Escape fractions [fraction]
extern bool output_mu2;                      // Output mu map^2
extern vector<double> mu2;                   // Escape fractions^2 [statistic]
extern bool output_freq_mu;                  // Output average frequency map
extern vector<double> freq_mu;               // Average frequency map [freq units]
extern bool output_freq2_mu;                 // Output frequency^2 map
extern vector<double> freq2_mu;              // Frequency^2 map [freq^2 units]
extern bool output_freq3_mu;                 // Output frequency^3 map
extern vector<double> freq3_mu;              // Frequency^3 map [freq^3 units]
extern bool output_freq4_mu;                 // Output frequency^4 map
extern vector<double> freq4_mu;              // Frequency^4 map [freq^4 units]

// Information about the line-of-sight flux angular cosine (μ=cosθ) map
extern int n_mu_flux;                        // Number of flux map mu bins
extern int n_mu_bins;                        // Number of flux map frequency bins
extern double inverse_dmu_flux;              // Flux cosine bin width^-1
extern double mu_freq_min;                   // Flux map frequency extrema [freq units]
extern double mu_freq_max;                   // Calculated when freq is initialized
extern double mu_freq_bin_width;             // Flux map frequency bin width [freq units]
extern double observed_mu_bin_width;         // Observed flux map wavelength resolution [angstrom]
extern double inverse_mu_freq_bin_width;     // Flux map frequency bin width^-1 [freq units]
extern bool output_flux_mu;                  // Output line-of-sight flux mu map
extern Image flux_mu;                        // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight group healpix maps
extern bool output_grp_obs;                  // Output group observed data
extern bool output_grp_vir;                  // Output group virial radius data
extern bool output_grp_gal;                  // Output group galaxy data
extern int n_side_grp;                       // Healpix parameter for group maps
extern bool output_map_grp;                  // Output line-of-sight group healpix map
extern Image map_grp;                        // Escape fractions [fraction]
extern bool output_map2_grp;                 // Output healpix group maps^2
extern Image map2_grp;                       // Escape fractions^2 [statistic]
extern bool output_freq_map_grp;             // Output average frequency group maps
extern Image freq_map_grp;                   // Average frequency group maps [freq units]
extern bool output_freq2_map_grp;            // Output frequency^2 group maps
extern Image freq2_map_grp;                  // Frequency^2 group maps [freq^2 units]
extern bool output_freq3_map_grp;            // Output frequency^3 group maps
extern Image freq3_map_grp;                  // Frequency^3 group maps [freq^3 units]
extern bool output_freq4_map_grp;            // Output frequency^4 group maps
extern Image freq4_map_grp;                  // Frequency^4 group maps [freq^4 units]
extern Image map_grp_vir;                    // Group map at the halo virial radius
extern Image map_grp_gal;                    // Group map at the galaxy radius
extern Image map2_grp_vir;                   // Group map^2 at the halo virial radius
extern Image map2_grp_gal;                   // Group map^2 at the galaxy radius

// Information about the line-of-sight subhalo healpix maps
extern bool output_sub_obs;                  // Output subhalo observed data
extern bool output_sub_vir;                  // Output subhalo virial radius data
extern bool output_sub_gal;                  // Output subhalo galaxy data
extern int n_side_sub;                       // Healpix parameter for subhalo maps
extern bool output_map_sub;                  // Output line-of-sight subhalo healpix map
extern Image map_sub;                        // Escape fractions [fraction]
extern bool output_map2_sub;                 // Output healpix subhalo maps^2
extern Image map2_sub;                       // Escape fractions^2 [statistic]
extern bool output_freq_map_sub;             // Output average frequency subhalo maps
extern Image freq_map_sub;                   // Average frequency subhalo maps [freq units]
extern bool output_freq2_map_sub;            // Output frequency^2 subhalo maps
extern Image freq2_map_sub;                  // Frequency^2 subhalo maps [freq^2 units]
extern bool output_freq3_map_sub;            // Output frequency^3 subhalo maps
extern Image freq3_map_sub;                  // Frequency^3 subhalo maps [freq^3 units]
extern bool output_freq4_map_sub;            // Output frequency^4 subhalo maps
extern Image freq4_map_sub;                  // Frequency^4 subhalo maps [freq^4 units]
extern Image map_sub_vir;                    // Subhalo maps at the halo virial radius
extern Image map_sub_gal;                    // Subhalo maps at the galaxy radius
extern Image map2_sub_vir;                   // Subhalo maps^2 at the halo virial radius
extern Image map2_sub_gal;                   // Subhalo maps^2 at the galaxy radius

// Information about the line-of-sight healpix flux group maps
extern int n_side_flux_grp;                  // Healpix parameter for the flux map
extern int n_map_bins_grp;                   // Number of flux map frequency bins
extern double map_freq_min_grp;              // Flux map frequency extrema [freq units]
extern double map_freq_max_grp;              // Calculated when freq is initialized
extern double map_freq_bin_width_grp;        // Flux map frequency bin width [freq units]
extern double observed_map_bin_width_grp;    // Observed flux map wavelength resolution [angstrom]
extern double inverse_map_freq_bin_width_grp;// Flux map frequency bin width^-1 [freq units]
extern bool output_flux_map_grp;             // Output line-of-sight healpix flux map
extern Cube flux_map_grp;                    // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight healpix flux subhalo maps
extern int n_side_flux_sub;                  // Healpix parameter for the flux map
extern int n_map_bins_sub;                   // Number of flux map frequency bins
extern double map_freq_min_sub;              // Flux map frequency extrema [freq units]
extern double map_freq_max_sub;              // Calculated when freq is initialized
extern double map_freq_bin_width_sub;        // Flux map frequency bin width [freq units]
extern double observed_map_bin_width_sub;    // Observed flux map wavelength resolution [angstrom]
extern double inverse_map_freq_bin_width_sub;// Flux map frequency bin width^-1 [freq units]
extern bool output_flux_map_sub;             // Output line-of-sight healpix flux map
extern Cube flux_map_sub;                    // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the angle-averaged spectral flux for groups
extern int n_bins_grp;                       // Number of flux avg frequency bins
extern double freq_min_grp;                  // Flux avg frequency extrema [freq units]
extern double freq_max_grp;                  // Calculated when freq is initialized
extern double freq_bin_width_grp;            // Flux avg frequency bin width [freq units]
extern double observed_bin_width_grp;        // Observed flux avg wavelength resolution [angstrom]
extern double inverse_freq_bin_width_grp;    // Flux avg frequency bin width^-1 [freq units]
extern bool output_flux_avg_grp;             // Output line-of-sight healpix flux avg
extern Image flux_avg_grp;                   // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the angle-averaged spectral flux for subhalos
extern int n_bins_sub;                       // Number of flux avg frequency bins
extern double freq_min_sub;                  // Flux avg frequency extrema [freq units]
extern double freq_max_sub;                  // Calculated when freq is initialized
extern double freq_bin_width_sub;            // Flux avg frequency bin width [freq units]
extern double observed_bin_width_sub;        // Observed flux avg wavelength resolution [angstrom]
extern double inverse_freq_bin_width_sub;    // Flux avg frequency bin width^-1 [freq units]
extern bool output_flux_avg_sub;             // Output line-of-sight healpix flux avg
extern Image flux_avg_sub;                   // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight healpix group radial maps
extern int n_side_radial_grp;                // Healpix parameter for the radial map
extern int n_map_pixels_grp;                 // Number of radial map radial bins
extern double map_radius_grp;                // Radial map radius [cm] (defaults to half domain)
extern double map_radius_bbox_grp;           // Radial map radius [min distance to bbox edge]
extern double map_radius_Rvir_grp;           // Radial map radius [selected halo virial radius]
extern double map_pixel_width_grp;           // Radial map pixel width [cm]
extern vector<double> map_pixel_areas_grp;   // Radial map pixel areas [cm^2]
extern double inverse_map_pixel_width_grp;   // Inverse radial map pixel width [1/cm]
extern double min_map_radius_grp;            // Minimum radial map radius [cm]
extern double log_min_map_radius_grp;        // Logarithm of the minimum radial map radius [log10(cm)]
extern double map_pixel_arcsec_grp;          // Radial map pixel width [arcsec] (optional)
extern vector<double> map_pixel_arcsec2_grp; // Radial map pixel areas [arcsec^2] (optional)
extern bool output_radial_map_grp;           // Output line-of-sight healpix radial profiles
extern Cube radial_map_grp;                  // Line-of-sight healpix radial profiles [erg/s/cm^2/arcsec^2]

// Information about the line-of-sight healpix subhalo radial maps
extern int n_side_radial_sub;                // Healpix parameter for the radial map
extern int n_map_pixels_sub;                 // Number of radial map radial bins
extern double map_radius_sub;                // Radial map radius [cm] (defaults to half domain)
extern double map_radius_bbox_sub;           // Radial map radius [min distance to bbox edge]
extern double map_radius_Rvir_sub;           // Radial map radius [selected halo virial radius]
extern double map_pixel_width_sub;           // Radial map pixel width [cm]
extern vector<double> map_pixel_areas_sub;   // Radial map pixel areas [cm^2]
extern double inverse_map_pixel_width_sub;   // Inverse radial map pixel width [1/cm]
extern double min_map_radius_sub;            // Minimum radial map radius [cm]
extern double log_min_map_radius_sub;        // Logarithm of the minimum radial map radius [log10(cm)]
extern double map_pixel_arcsec_sub;          // Radial map pixel width [arcsec] (optional)
extern vector<double> map_pixel_arcsec2_sub; // Radial map pixel areas [arcsec^2] (optional)
extern bool output_radial_map_sub;           // Output line-of-sight healpix radial profiles
extern Cube radial_map_sub;                  // Line-of-sight healpix radial profiles [erg/s/cm^2/arcsec^2]

// Information about the angle-averaged radial surface brightness (groups)
extern int n_pixels_grp;                     // Number of radial avg radial bins
extern double radius_grp;                    // Radial avg radius [cm] (defaults to half domain)
extern double radius_bbox_grp;               // Radial avg radius [min distance to bbox edge]
extern double radius_Rvir_grp;               // Radial avg radius [selected halo virial radius]
extern double pixel_width_grp;               // Radial avg pixel width [cm]
extern vector<double> pixel_areas_grp;       // Radial avg pixel areas [cm^2]
extern double inverse_pixel_width_grp;       // Inverse radial avg pixel width [1/cm]
extern double min_radius_grp;                // Minimum radial avg radius [cm]
extern double log_min_radius_grp;            // Logarithm of the minimum radial avg radius [log10(cm)]
extern double pixel_arcsec_grp;              // Radial avg pixel width [arcsec] (optional)
extern vector<double> pixel_arcsec2_grp;     // Radial avg pixel areas [arcsec^2] (optional)
extern bool output_radial_avg_grp;           // Output line-of-sight healpix radial profiles
extern Image radial_avg_grp;                 // Angle-averaged radial profile [erg/s/cm^2/arcsec^2]

// Information about the angle-averaged radial surface brightness (subhalos)
extern int n_pixels_sub;                     // Number of radial avg radial bins
extern double radius_sub;                    // Radial avg radius [cm] (defaults to half domain)
extern double radius_bbox_sub;               // Radial avg radius [min distance to bbox edge]
extern double radius_Rvir_sub;               // Radial avg radius [selected halo virial radius]
extern double pixel_width_sub;               // Radial avg pixel width [cm]
extern vector<double> pixel_areas_sub;       // Radial avg pixel areas [cm^2]
extern double inverse_pixel_width_sub;       // Inverse radial avg pixel width [1/cm]
extern double min_radius_sub;                // Minimum radial avg radius [cm]
extern double log_min_radius_sub;            // Logarithm of the minimum radial avg radius [log10(cm)]
extern double pixel_arcsec_sub;              // Radial avg pixel width [arcsec] (optional)
extern vector<double> pixel_arcsec2_sub;     // Radial avg pixel areas [arcsec^2] (optional)
extern bool output_radial_avg_sub;           // Output line-of-sight healpix radial profiles
extern Image radial_avg_sub;                 // Angle-averaged radial profile [erg/s/cm^2/arcsec^2]

// Tabulated ionizing spectral energy distributions
struct IonAgeTables {
  vector<double> L_HI;                       // Luminosities [erg/s/Msun]
  vector<double> L_HeI;
  vector<double> L_HeII;
  vector<double> Ndot_HI;                    // Photon rates [photons/s/Msun]
  vector<double> Ndot_HeI;
  vector<double> Ndot_HeII;
  vector<double> sigma_HI_1;                 // Cross-sections [cm^2]
  vector<double> sigma_HI_2;
  vector<double> sigma_HI_3;
  vector<double> sigma_HeI_2;
  vector<double> sigma_HeI_3;
  vector<double> sigma_HeII_3;
  vector<double> epsilon_HI_1;               // Photoheating [erg]
  vector<double> epsilon_HI_2;
  vector<double> epsilon_HI_3;
  vector<double> epsilon_HeI_2;
  vector<double> epsilon_HeI_3;
  vector<double> epsilon_HeII_3;
  vector<double> albedo_HI;                  // Dust scattering albedos
  vector<double> albedo_HeI;
  vector<double> albedo_HeII;
  vector<double> cosine_HI;                  // Dust scattering anisotropy parameters
  vector<double> cosine_HeI;
  vector<double> cosine_HeII;
  vector<double> kappa_HI;                   // Dust opacities [cm^2/g dust]
  vector<double> kappa_HeI;
  vector<double> kappa_HeII;
};
extern IonAgeTables ion_age;                 // Tabulated ionizing SEDs

// Tabulated ionizing spectral energy distributions
struct IonMetallicityAgeTables {
  vectors log_L_HI, log_L_HeI, log_L_HeII;   // Luminosities [erg/s/Msun]
  vectors log_Ndot_HI, log_Ndot_HeI, log_Ndot_HeII; // Photon rates [photons/s/Msun]
  vectors log_sigma_Ndot_HI_1, log_sigma_Ndot_HI_2, log_sigma_Ndot_HI_3; // Cross-sections [cm^2]
  vectors log_sigma_Ndot_HeI_2, log_sigma_Ndot_HeI_3, log_sigma_Ndot_HeII_3;
  vectors log_sigma_L_HI_1, log_sigma_L_HI_2, log_sigma_L_HI_3; // Photoheating [erg]
  vectors log_sigma_L_HeI_2, log_sigma_L_HeI_3, log_sigma_L_HeII_3;
  vectors log_albedo_Ndot_HI, log_albedo_Ndot_HeI, log_albedo_Ndot_HeII; // Dust scattering albedos
  vectors log_cosine_Ndot_HI, log_cosine_Ndot_HeI, log_cosine_Ndot_HeII; // Dust scattering anisotropy parameters
  vectors log_kappa_Ndot_HI, log_kappa_Ndot_HeI, log_kappa_Ndot_HeII; // Dust opacities [cm^2/g dust]
};
extern string source_file_Z_age;             // Spectral source file (Z,age,wavelength)
extern IonMetallicityAgeTables ion_Z_age;    // Tabulated ionizing SEDs
extern vectors log_Qion_Z_age;               // Tabulated ionizing photon rate [photons/s/Msun]
extern vectors log_Lcont_Z_age;              // Tabulated line continuum [erg/s/angstrom/Msun]

// Tabulated generalized ionizing spectral energy distributions
struct CubeMetallicityAgeTables {
  int n_Zs = 0;                              // Number of metallicities
  int n_ages = 0;                            // Number of ages
  double min_Z = 0., max_Z = 0.;             // Metallicity boundaries
  double min_age = 0., max_age = 0.;         // Age boundaries [Gyr]
  vector<double> log_Z;                      // Log metallicities [mass fraction]
  vector<double> log_age;                    // Log ages [Gyr]
  Cube log_L;                                // Luminosities [erg/s/Msun]
  Cube log_Ndot;                             // Photon rates [photons/s/Msun]
  Cubes log_sigma_Ndot_ions;                 // Cross-sections [cm^2]
  Cubes log_sigma_L_ions;                    // Photoheating [erg]
  using dust_cubes = array<Cube, n_dust_species>;
  dust_cubes log_albedos_Ndot;               // Dust scattering albedos
  dust_cubes cosines_Ndot;                   // Dust scattering anisotropy parameters
  dust_cubes log_kappas_Ndot;                // Dust opacities [cm^2/g dust]
};
extern CubeMetallicityAgeTables spec_Z_age;  // Tabulated generalized SEDs

// Tabulated free-free emission spectral energy distributions
struct FreeFreeTemperatureTables {           // Note: Units are per (n_e n_ions V)
  static constexpr int n_Ts = 101;           // Number of temperatures
  static constexpr double min_T = 1., max_T = 1e10; // Temperature boundaries [K]
  static constexpr double min_logT = 0., max_logT = 10.; // Log temperature boundaries
  static constexpr double dlogT = 0.1, inv_dlogT = 10.; // Point separations in dex
  vector<array<double,n_Ts>> log_L;          // Luminosities [erg/s]
  vector<array<double,n_Ts>> log_Ndot;       // Photon rates [photons/s]
  vector<vector<array<double,n_Ts>>> log_sigma_Ndot_ions; // Cross-sections [cm^2]
  vector<vector<array<double,n_Ts>>> log_sigma_L_ions; // Photoheating [erg]
  using dust_ffs = array<vector<array<double,n_Ts>>, n_dust_species>;
  dust_ffs log_albedos_Ndot;                 // Dust scattering albedos
  dust_ffs cosines_Ndot;                     // Dust scattering anisotropy parameters
  dust_ffs log_kappas_Ndot;                  // Dust opacities [cm^2/g dust]
};
extern FreeFreeTemperatureTables spec_ff_Z1; // Tabulated free-free SEDs (Z = 1)
extern FreeFreeTemperatureTables spec_ff_Z2; // Tabulated free-free SEDs (Z = 2)

// Tabulated free-bound emission spectral energy distributions
struct FreeBoundTemperatureTables {          // Note: Units are per (n_e n_ions V)
  static constexpr int n_Ts = 151;           // Number of temperatures
  static constexpr double min_T = 1e2, max_T = 1e5; // Temperature boundaries [K]
  static constexpr double min_logT = 2., max_logT = 5.; // Log temperature boundaries
  static constexpr double dlogT = 0.02, inv_dlogT = 50.; // Point separations in dex
  vector<array<double,n_Ts>> log_L;          // Luminosities [erg/s]
  vector<array<double,n_Ts>> log_Ndot;       // Photon rates [photons/s]
  vector<vector<array<double,n_Ts>>> log_sigma_Ndot_ions; // Cross-sections [cm^2]
  vector<vector<array<double,n_Ts>>> log_sigma_L_ions; // Photoheating [erg]
  using dust_fbs = array<vector<array<double,n_Ts>>, n_dust_species>;
  dust_fbs log_albedos_Ndot;                 // Dust scattering albedos
  dust_fbs cosines_Ndot;                     // Dust scattering anisotropy parameters
  dust_fbs log_kappas_Ndot;                  // Dust opacities [cm^2/g dust]
};
extern FreeBoundTemperatureTables spec_fb_HI; // Tabulated free-bound SEDs (HII->HI)
extern FreeBoundTemperatureTables spec_fb_HeI; // Tabulated free-bound SEDs (HeII->HeI)
extern FreeBoundTemperatureTables spec_fb_HeII; // Tabulated free-bound SEDs (HeIII->HeII)

// Tabulated two-photon emission spectral energy distributions
struct TwoPhotonWvlTables {                  // Note: Units are per (n_e n_ions V) * rate_2p(T,n_e)
  vector<double> L;                          // Luminosities [erg/s/Msun]
  vector<double> Ndot;                       // Photon rates [photons/s/Msun]
  vectors sigma_Ndot_ions;                   // Cross-sections [cm^2]
  vectors sigma_L_ions;                      // Photoheating [erg]
  dust_vectors albedos_Ndot;                 // Dust scattering albedos
  dust_vectors cosines_Ndot;                 // Dust scattering anisotropy parameters
  dust_vectors kappas_Ndot;                  // Dust opacities [cm^2/g dust]
};
extern TwoPhotonWvlTables spec_2p_HI;        // Pre-calculated two-photon SEDs (HII->HI)
// extern TwoPhotonWvlTables spec_2p_HeI;       // Tabulated free-bound SEDs (HeII->HeI)
extern TwoPhotonWvlTables spec_2p_HeII;      // Tabulated free-bound SEDs (HeIII->HeII)

extern double albedo_HI;                     // Dust scattering albedos
extern double albedo_HeI;
extern double albedo_HeII;
extern double cosine_HI;                     // Dust scattering anisotropy parameters
extern double cosine_HeI;
extern double cosine_HeII;
extern double kappa_HI;                      // Dust opacities [cm^2/g dust]
extern double kappa_HeI;
extern double kappa_HeII;

#endif // PROTO_INCLUDED
