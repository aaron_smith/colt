/************************
 * geometry/geometry.cc *
 ************************

 * Geometry: Calculations of distances etc.

*/

#include "../proto.h"

static constexpr double _2_3 = 2. / 3.;
static constexpr double _2pi = 2. * M_PI;

bool point_is_in_cell(const Vec3 point, const int cell); // Cell validation
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

/* Returns the integer n, which fulfills n^2 <= arg < (n+1)^2. */
static inline int isqrt(const int arg) {
  return sqrt(arg + 0.5);
}

/* Returns healpix directions in RING ordering from a given number of base pixel subdivisions. */
vector<Vec3> healpix_vectors(const int nside) {
  const int npface = nside * nside;          // npface = nside^2
  const int ncap = (npface - nside) << 1;    // ncap = 2 nside (nside - 1)
  const int npix = 12 * npface;              // npix = 12 nside^2 = 12*4^order
  const double fact2 = 4. / npix;            // Useful constants
  const double fact1 = (nside << 1) * fact2; // 8 nside / npix

  vector<Vec3> vecs;                         // Resulting healpix vectors
  vecs.resize(npix);                         // Allocate space

  #pragma omp parallel for
  for (int pix = 0; pix < npix; ++pix) {
    double z, phi, sth;                      // pix -> (z, phi, sqrt(theta))
    if (pix < ncap) {                        // North polar cap
      int iring = (1 + isqrt(1 + 2*pix)) >> 1; // Counted from north pole
      int iphi = (pix+1) - 2*iring*(iring-1);

      double tmp = (iring * iring) * fact2;
      z = 1. - tmp;
      phi = (iphi - 0.5) * M_PI_2 / iring;
      sth = (z > 0.99) ? sqrt(tmp * (2. - tmp)) : sqrt(1. - z * z);
    } else if (pix < npix - ncap) {          // Equatorial region
      int nl4 = 4 * nside;
      int ip  = pix - ncap;
      int tmp = ip / nl4;
      int iring = tmp + nside;
      int iphi = ip - nl4*tmp + 1;
      // 1 if iring + nside is odd, 1/2 otherwise
      double fodd = ((iring + nside) & 1) ? 1. : 0.5;

      z = (2*nside - iring) * fact1;
      phi = (iphi - fodd) * M_PI * 0.75 * fact1;
      sth = sqrt(1. - z * z);
    } else {                                 // South polar cap
      int ip = npix - pix;
      int iring = (1 + isqrt(2*ip - 1)) >> 1; // Counted from south pole
      int iphi  = 4*iring + 1 - (ip - 2*iring*(iring-1));

      double tmp = (iring * iring) * fact2;
      z = tmp - 1.;
      phi = (iphi - 0.5) * M_PI_2 / iring;
      sth = (z < -0.99) ? sqrt(tmp * (2. - tmp)) : sqrt(1. - z * z);
    }
    vecs[pix] = {sth * cos(phi), sth * sin(phi), z};
  }
  return vecs;
}

/* Returns the remainder of the division v1/v2. The result is non-negative.
   v1 can be positive or negative but v2 must be positive. */
static double fmodulo(const double v1, const double v2) {
  if (v1 >= 0.)
    return (v1 < v2) ? v1 : fmod(v1, v2);
  double tmp = fmod(v1, v2) + v2;
  return (tmp == v2) ? 0. : tmp;
}

static int imodulo(const int v1, const int v2) {
  int v = v1 % v2;
  return (v >= 0) ? v : v + v2;
}

static int ang2pix_ring_z_phi(const int nside, const double z, const double s, const double phi) {
  const double za = fabs(z);
  const double tt = fmodulo(phi, _2pi) * M_2_PI; // in [0,4)

  if (za <= _2_3) {                      // Equatorial region
    const double temp1 = nside * (0.5 + tt);
    const double temp2 = nside * z * 0.75;
    const int jp = (int)(temp1 - temp2);     // Index of ascending edge line
    const int jm = (int)(temp1 + temp2);     // Index of descending edge line

    // Ring number counted from z = 2/3
    const int ir = nside + 1 + jp - jm;      // in {1,2n+1}
    const int kshift = 1 - (ir & 1);         // kshift=1 if ir even, 0 otherwise
    int ip = (jp + jm - nside + kshift + 1) / 2; // in {0,4n-1}
    ip = imodulo(ip, 4 * nside);

    return nside * (nside - 1) * 2 + (ir - 1) * 4 * nside + ip;
  } else {                                   // North & South polar caps
    const double tp = tt - (int)(tt);
    const double tmp = (s > -2.) ? nside * s / sqrt((1. + za)/3.) : nside * sqrt(3*(1 - za));
    const int jp = (int)(tp * tmp);          // Increasing edge line index
    const int jm = (int)((1. - tp) * tmp);   // Decreasing edge line index

    const int ir = jp + jm + 1;              // Ring number counted from the closest pole
    int ip = (int)(tt * ir);                 // in {0,4*ir-1}
    ip = imodulo(ip, 4 * ir);

    if (z > 0.)
      return 2 * ir * (ir - 1) + ip;
    else
      return 12 * nside * nside - 2 * ir * (ir + 1) + ip;
  }
}

int vec2pix_ring(const int nside, const Vec3& vec) {
  double sth = (fabs(vec.z) > 0.99) ? sqrt(vec.x*vec.x + vec.y*vec.y) : -5.;
  return ang2pix_ring_z_phi(nside, vec.z, sth, atan2(vec.y, vec.x));
}

/* Calculate the distance to escape the bounding sphere. */
double escape_distance(const Vec3& dr, const Vec3& direction, const double radius2) {
  const double k_dot_r = dr.dot(direction);  // Angular cosine
  const double r_dot_r = dr.dot();           // Radius squared
  return sqrt(k_dot_r*k_dot_r - r_dot_r + radius2) - k_dot_r; // Distance to the sphere
}

#if spherical_escape
/* Calculate the distance to escape the emission sphere. */
double spherical_emission_distance(const Vec3& point, const Vec3& direction) {
  return escape_distance(point - escape_center, direction, emission_radius2); // Position relative to the center
}

/* Calculate the distance to escape the bounding sphere. */
double spherical_escape_distance(const Vec3& point, const Vec3& direction) {
  return escape_distance(point - escape_center, direction, escape_radius2); // Position relative to the center
}
#endif

#if box_escape
/* Calculate the distance to the escape/emission box. */
static inline double box_distance_template(const Vec3& point, const Vec3& direction, const array<Vec3, 2>& box) {
  double l = positive_infinity, l_comp;      // Comparison distances
  // x direction
  if (direction.x != 0.) {
    if (direction.x > 0.) {
      l_comp = (box[1].x - point.x) / direction.x; // Right x face
      if (l_comp < l)
        l = l_comp;
    } else {
      l_comp = (box[0].x - point.x) / direction.x; // Left x face
      if (l_comp < l)
        l = l_comp;
    }
  }
  // y direction
  if (direction.y != 0.) {
    if (direction.y > 0.) {
      l_comp = (box[1].y - point.y) / direction.y; // Right y face
      if (l_comp < l)
        l = l_comp;
    } else {
      l_comp = (box[0].y - point.y) / direction.y; // Left y face
      if (l_comp < l)
        l = l_comp;
    }
  }
  // z direction
  if (direction.z != 0.) {
    if (direction.z > 0.) {
      l_comp = (box[1].z - point.z) / direction.z; // Right z face
      if (l_comp < l)
        l = l_comp;
    } else {
      l_comp = (box[0].z - point.z) / direction.z; // Left z face
      if (l_comp < l)
        l = l_comp;
    }
  }
  return l;                                  // Distance to escape/emission
}

/* Calculate the distance to escape the emission box. */
double box_emission_distance(const Vec3& point, const Vec3& direction) {
  return box_distance_template(point, direction, emission_box);
}

/* Calculate the distance to escape the bounding box. */
double box_escape_distance(const Vec3& point, const Vec3& direction) {
  return box_distance_template(point, direction, escape_box);
}
#endif

/* Integrate a scalar field along a ray until escape. */
double ray_integral(vector<double>& field, Vec3 point, Vec3 direction, int cell) {
  if (!point_is_in_cell(point, cell))
    error("Ray integration point is not in the starting cell.");
  int next_cell;                             // Next cell index
  double l, result = 0.;                     // Path length and result
  #if check_escape
    double l_exit = positive_infinity;       // Remaining distance to escape
  #endif
  #if spherical_escape                       // Spherical escape distance
    if ((point - escape_center).dot() >= escape_radius2)
      error("Attempted ray integration outside the spherical escape region");
    inplace_min(l_exit, spherical_escape_distance(point, direction));
  #endif
  #if box_escape                             // Box escape distance
    if (point.x < escape_box[0].x || point.x > escape_box[1].x ||
        point.y < escape_box[0].y || point.y > escape_box[1].y ||
        point.z < escape_box[0].z || point.z > escape_box[1].z)
      error("Attempted ray integration outside the box escape region");
    inplace_min(l_exit, box_escape_distance(point, direction));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(l_exit, max_streaming);
  #endif
  while (true) {                             // Ray trace until escape
    tie(l, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    #if check_escape                         // Check for spherical escape
      if (l_exit <= l)                       // Exit sphere or box before cell
        return result + field[cell] * l_exit; // += field integral
      l_exit -= l;                           // Remaining distance to exit
    #endif
    result += field[cell] * l;               // Path integral of the field
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        return result;
    }
    if (next_cell == OUTSIDE)                // Check if the ray escapes
      return result;
    point += direction * l;                  // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }
}
