/**********************
 * geometry/octree.cc *
 **********************

 * Octree specific functions.

*/

#include "../compile_time.h" // Compile time options

#if OCTREE

#include "../proto.h"
#include "../io_hdf5.h" // HDF5 read/write functions

static double ray_eps;                       // Ray tracing uncertainty [cm]

double ran(); // Random number (from global seed)

/* Read octree specific data. */
void read_geometry(H5File& f) {
  n_cells = read(f, "r", r, "cm");           // Cell left corner position [cm]
  read(f, "w", w, n_cells, "cm");            // Cell width [cm]
  read(f, "parent", parent, n_cells);        // Cell parent
  read(f, "child", child, n_cells);          // Child cell indices
  read(f, strings{"child_check", "sub_cell_check"}, child_check, n_cells); // True if cell has children

  // Additional geometry info
  bbox[0] = r[0];                            // Bounding box [cm]
  bbox[1] = r[0] + w[0];                     // Min/Max edges
  max_bbox_width = (bbox[1]-bbox[0]).max();  // Max bbox width [cm]
  ray_eps = 1e-12 * w[0];                    // Ray tracing uncertainty [cm]
  V.resize(n_cells);                         // Allocate space for volume
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    V[i] = w[i] * w[i] * w[i];               // Volume [cm^3] (V = w^3)

  // Count the number of leaf cells
  n_leafs = 0;                               // Leaf counter
  #pragma omp parallel for reduction(+:n_leafs)
  for (int i = 0; i < n_cells; ++i)
    if (!child_check[i])                     // Leaf cells have no children
      ++n_leafs;
}

/* Avoid calculations for certain cells. */
bool avoid_cell(const int cell) {
  return child_check[cell];                  // Non-leaf cells
}

/* Avoid calculations for certain cells without overrides. */
bool avoid_cell_strict(const int cell) {
  return child_check[cell];                  // Non-leaf cells
}

/* Return the center position of the cell. */
Vec3 cell_center(const int cell) {
  return r[cell] + 0.5 * w[cell];            // Left corner plus half width
}

/* Draw position uniformly within the cell volume. */
Vec3 random_point_in_cell(const int cell) {
  return {r[cell].x + w[cell] * ran(),       // [x,x+w]
          r[cell].y + w[cell] * ran(),       // [y,y+w]
          r[cell].z + w[cell] * ran()};      // [z,z+w]
}

/* Check whether a point is in the specified cell. */
bool point_is_in_cell(const Vec3 point, const int cell) {
  for (int i = 0; i < 3; ++i)
    if (point[i] < r[cell][i] || point[i] >= r[cell][i] + w[cell])
      return false;                          // Point is not in the cell
  return true;                               // Point is in the cell
}

/* Find the cell index of the specified point. */
int find_cell(const Vec3 point, int cell) {
  // Check if outside the domain
  if (!point_is_in_cell(point, 0))
    return OUTSIDE;

  // Invalid cell indices start from the top level
  if (cell < 0 || cell >= n_cells)
    cell = 0;

  // Find the parent cell it belongs to
  while (!point_is_in_cell(point, cell))
    cell = parent[cell];

  // Find the finest-level cell
  int n_oct[3], n_child;
  while (child_check[cell]) {
    for (int i = 0; i < 3; ++i) {
      n_oct[i] = int(2. * (point[i] - r[cell][i]) / w[cell]);
      if (n_oct[i] < 0)
        n_oct[i] = 0;
      if (n_oct[i] > 1)
        n_oct[i] = 1;
    }
    n_child = 4 * n_oct[0] + 2 * n_oct[1] + n_oct[2];
    cell = child[cell][n_child];
  }
  return cell;
}

/* Find the minimum distance to a face from a given point in the cell. */
double min_face_distance(const Vec3& point, const int cell) {
  double l = positive_infinity, l_comp;      // Comparison distances
  for (int i = 0; i < 3; ++i) {
    l_comp = fabs(r[cell][i] + w[cell] - point[i]); // Right face
    if (l_comp < l)
      l = l_comp;
    l_comp = fabs(point[i] - r[cell][i]);    // Left face
    if (l_comp < l)
      l = l_comp;
  }
  return l;
}

/* Compute the maximum distance the photon can travel in the cell. */
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
  double l = positive_infinity, l_comp;      // Comparison distances
  for (int i = 0; i < 3; ++i) {
    if (direction[i] == 0.)
      continue;
    if (direction[i] > 0.)
      l_comp = (r[cell][i] + w[cell] - point[i]) / direction[i]; // Right face
    else
      l_comp = (r[cell][i] - point[i]) / direction[i]; // Left face
    if (l_comp >= 0. && l_comp < l)
      l = l_comp;
  }
  l += ray_eps;                              // Ensure escape from the cell
  return make_tuple(l, find_cell(point + direction * l, cell));
}

#endif // OCTREE
