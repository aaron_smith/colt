/********************
 * geometry/cgal.cc *
 ********************

 * CGAL: Computational Goemoetry Algorithms Library (optional)

*/

#include "../compile_time.h" // Compile time options

#if HAVE_CGAL

#include "cgal.h"

typedef CGAL::Exact_predicates_inexact_constructions_kernel K;
typedef CGAL::Triangulation_vertex_base_with_info_3<int, K> Vb;
typedef CGAL::Delaunay_triangulation_cell_base_3<K>         Cb;
#ifdef CGAL_LINKED_WITH_TBB
typedef CGAL::Parallel_tag                                  Ptag;
typedef CGAL::Triangulation_data_structure_3<Vb, Cb, Ptag>  Tds;
#else
typedef CGAL::Triangulation_data_structure_3<Vb, Cb>        Tds;
#endif
typedef CGAL::Delaunay_triangulation_3<K, Tds>              Delaunay;
typedef Delaunay::Cell_handle                               Cell_handle;
typedef Delaunay::Point                                     Point;

// Note: Include COLT specific items after CGAL headers
using std::cout;
using std::endl;
using std::string;
using std::pair;
using std::make_pair;
using std::set;

#include <omp.h>
#include "../Vec3.h"  // 3D vectors in (x,y,z) order

// Voronoi tessellation data
struct Face {
  int neighbor;                              // Neighbor index
  double area;                               // Face area [cm^2]
  std::vector<int> circulator;               // Circulator indices
};
extern int n_cells;                          // Number of cells
extern std::vector<Vec3> r;                  // Cell position [cm]
extern std::vector<double>& V;               // Cell volume [cm^3]
extern neib_t n_neighbors_tot;               // Total number of neighbors for all cells
extern circ_t n_circulators_tot;             // Total number of circulators for all faces
extern bool save_circulators;                // Flag to save circulators
extern std::vector<bool> edge_flag;          // Flag for edge cells
extern std::vector<std::vector<Face>> faces; // Face connection data

/* Set a vector equal to the difference of two vectors. */
static inline void set_vector_to_difference(double A[3], const double B[3], const double C[3]) {
  A[0] = B[0] - C[0];
  A[1] = B[1] - C[1];
  A[2] = B[2] - C[2];
}

/* Set a vector equal to the cross product of two others. */
static inline void set_vector_to_cross(double AxB[3], const double A[3], const double B[3]) {
  AxB[0] = A[1]*B[2] - A[2]*B[1];
  AxB[1] = A[2]*B[0] - A[0]*B[2];
  AxB[2] = A[0]*B[1] - A[1]*B[0];
}

/* Computes the two-norm of a vector. */
static inline double norm (double A[3]) {
  return sqrt(A[0]*A[0] + A[1]*A[1] + A[2]*A[2]);
}

/* Computes the area of the triangle formed by three points. */
static inline double calculate_area(const double A[3], const double B[3], const double C[3]) {
  // area(ABC) = |AB x AC| / 2
  double AB[3], AC[3], ABxAC[3];
  set_vector_to_difference(AB, A, B);
  set_vector_to_difference(AC, A, C);
  set_vector_to_cross(ABxAC, AB, AC);
  return 0.5 * norm(ABxAC);
}

/* Check whether a given triangulation has a vertex corresponding to a given Voronoi cell. */
static inline bool tetra_has_cell(Cell_handle ch, const int cell) {
  for (int i = 0; i < 4; ++i)
    if (ch->vertex(i)->info() == cell)
      return true;
  return false;
}

/* Use CGAL to calculate Delaunay connections. */
void calculate_connectivity(const bool verbose_cgal) {
  // Setup global data
  n_neighbors_tot = 0;                       // Total number of neighbors for all cells
  n_circulators_tot = 0;                     // Total number of circulators for all faces
  edge_flag.resize(n_cells);                 // Allocate space for edge flags
  faces.resize(n_cells);                     // Allocate space for faces
  V.resize(n_cells);                         // Allocate space for cell volumes

  // Setup function scope data
  std::vector<pair<Point,int>> points;       // Points to construct mesh
  std::vector<set<Cell_handle>> tetras;      // Delaunay tetra in cells
  points.resize(n_cells);                    // Allocate space for points
  tetras.resize(n_cells);                    // Allocate space for tetras

  // Insert each point in (position, index) pairs
  if (verbose_cgal)
#ifdef CGAL_LINKED_WITH_TBB
      cout << "\nCGAL: Calculating the Delaunay Triangulation ... [parallel]" << endl;
#else
      cout << "\nCGAL: Calculating the Delaunay Triangulation ... [serial]" << endl;
#endif
  for (int i = 0; i < n_cells; ++i) {
    points[i] = make_pair(Point(r[i].x, r[i].y, r[i].z), i);
    edge_flag[i] = false;                    // Initialize flags to false
  }

#ifdef CGAL_LINKED_WITH_TBB
  // Construct the locking data-structure, using the bounding-box of the points
  const double negative_infinity = std::numeric_limits<double>::lowest(); // Lowest double
  const double positive_infinity = std::numeric_limits<double>::max(); // Highest double
  double x_min = positive_infinity, y_min = positive_infinity, z_min = positive_infinity;
  double x_max = negative_infinity, y_max = negative_infinity, z_max = negative_infinity;
  for (int i = 0; i < n_cells; ++i) {
    if (r[i].x < x_min)
      x_min = r[i].x;
    if (r[i].x > x_max)
      x_max = r[i].x;
    if (r[i].y < y_min)
      y_min = r[i].y;
    if (r[i].y > y_max)
      y_max = r[i].y;
    if (r[i].z < z_min)
      z_min = r[i].z;
    if (r[i].z > z_max)
      z_max = r[i].z;
  }

  const double x_eps = 1e-6 * (x_max - x_min);
  const double y_eps = 1e-6 * (y_max - y_min);
  const double z_eps = 1e-6 * (z_max - z_min);

  if (verbose_cgal) {
    const double kpc = 3.085677581467192e21; // Units: 1 kpc = 3e21 cm
    cout << "\n  Bbox = [(" << x_min / kpc << ", " << y_min / kpc << ", " << z_min / kpc << "),"
         << "\n          (" << x_max / kpc << ", " << y_max / kpc << ", " << z_max / kpc << ")] kpc" << endl;
  }

  x_min -= x_eps;
  y_min -= y_eps;
  z_min -= z_eps;
  x_max += x_eps;
  y_max += y_eps;
  z_max += z_eps;

  Delaunay::Lock_data_structure locking_ds(
    CGAL::Bbox_3(x_min, y_min, z_min, x_max, y_max, z_max), 50);

  Delaunay T(points.begin(), points.end(), &locking_ds);
#else // Serial point insertion
  Delaunay T(points.begin(), points.end());
#endif // CGAL_LINKED_WITH_TBB
  points = std::vector<pair<Point,int>>();   // Free points

  CGAL_assertion(T.number_of_vertices() == unsigned(n_cells));

  if (verbose_cgal)
    cout << "\n  number_of_vertices      = " << T.number_of_vertices()
         << "\n  number_of_finite_cells  = " << T.number_of_finite_cells()
         << "\n  number_of_finite_facets = " << T.number_of_finite_facets()
         << "\n  number_of_finite_edges  = " << T.number_of_finite_edges() << endl;

  // Find which Voronoi cells are edges and find all Voronoi neighbors
  if (verbose_cgal)
    cout << "\nCGAL: Finding edges and neighbors from the triangulation ..." << endl;
  {
    // Create a countable list of finite Delaunay cells
    const size_t n_finite_cells = T.number_of_finite_cells();
    if constexpr (std::is_same<neib_t, int>::value) {
      if (n_finite_cells > static_cast<size_t>(std::numeric_limits<int>::max())) {
        std::cerr << "\nError: More neighbors than can be stored in an integer. Compile with long_neighbors: true" << endl;
        exit(EXIT_FAILURE);
      }
    }
    const neib_t n_tetras = n_finite_cells;  // Number of finite tetrahedra
    std::vector<Cell_handle> finite_tetras;
    finite_tetras.resize(n_tetras);
    neib_t i_tetra =  0;
    for (auto cit = T.finite_cells_begin(); cit != T.finite_cells_end(); ++cit)
      finite_tetras[i_tetra++] = cit;        // Ordered list of tetras

    // Create locks to restrict access to cells
    std::vector<omp_lock_t> locks;           // Locks for cell tetra sets
    locks.resize(n_cells);
    for (int cell = 0; cell < n_cells; ++cell)
      omp_init_lock(&locks[cell]);           // Initialize locks
    #pragma omp parallel for
    for (neib_t ict = 0; ict < n_tetras; ++ict) {
      const Cell_handle ct = finite_tetras[ict];
      for (int i = 0; i < 4; ++i) {
        // If the neighbor is a boundary then flag the vertices
        if (T.is_infinite(ct->neighbor(i))) { // Neighbor is incident to the infinite vertex
          for (int j = 0; j < 4; ++j)
            if (i != j) {
              const int edge = ct->vertex(j)->info();
              omp_set_lock(&locks[edge]);    // Restrict access to edge_flag[edge]
              edge_flag[edge] = true;        // Flag as an edge cell
              omp_unset_lock(&locks[edge]);  // Allow access again
            }
        }
        const int cell = ct->vertex(i)->info();
        omp_set_lock(&locks[cell]);          // Restrict access to tetras[cell]
        tetras[cell].insert(ct);             // Add to cell list of tetras
        omp_unset_lock(&locks[cell]);        // Allow cell access again
      } // end for all tetrahedron vertices
    } // end for all Delaunay cells to find edges and neighbors
    for (int cell = 0; cell < n_cells; ++cell)
      omp_destroy_lock(&locks[cell]);        // Free locks
  }

  // Extract vertex neighbors from tretra containing that vertex
  if (verbose_cgal)
    cout << "\nCGAL: Extracting vertex neighbors from the triangulation ..." << endl;
  #pragma omp parallel for reduction(+:n_neighbors_tot)
  for (int cell = 0; cell < n_cells; ++cell) {
    // Find unique Voronoi cell neighbor indices
    set<int> neibs;
    for (const auto& ct : tetras[cell])      // Tetra associated with the cell
      for (int i = 0; i < 4; ++i)
        neibs.insert(ct->vertex(i)->info());
    neibs.erase(cell);

    // Build the neighbor lists
    const int n_neibs = neibs.size();
    n_neighbors_tot += n_neibs;
    faces[cell].resize(n_neibs);             // Allocate space for cell faces
    int face = 0;                            // Face index
    for (const auto neib : neibs)
      faces[cell][face++].neighbor = neib;
  } // end for all Voronoi cells get neighbor indices

  // Calculate the volumes of the Voronoi cells
  if (verbose_cgal)
    cout << "\nCGAL: Computing the Voronoi cell volumes ..." << endl;
  #pragma omp parallel for reduction(+:n_circulators_tot)
  for (int cell = 0; cell < n_cells; ++cell) {
    V[cell] = 0.;                            // Set to zero and add tetrahedron
    // Make sure the cell is an interior, otherwise it will have infinite volume
    if (edge_flag[cell])
      continue;
    Cell_handle tetra;                       // Tetra cell handle
    for (auto& face : faces[cell]) {
      const int neib = face.neighbor;
      // Find a cell with both cell and neib
      for (const auto& ct : tetras[cell])    // Tetra associated with the cell
        if (tetra_has_cell(ct, neib)) {
          tetra = ct;
          break;                             // Cell has been found
        }

      // Find the local index of that cell that is not cell or neib
      // Start with the end to avoid redundant check in do loop
      int start;
      for (int i = 0; i < 4; ++i) {
        start = tetra->vertex(i)->info();
        if (start != cell && start != neib)
          break;
      } // end check 4 vertices

      int prev = start, next;                // Previous and next circulators
      std::vector<int> vcirc;                // Temporary circulator indices
      std::vector<Point> vface;              // Face edge points
      do {
        vcirc.push_back(prev);               // Track circulator cell order
        int prev_vertex;                     // Setup prev_vertex
        for (prev_vertex = 0; prev_vertex < 4; ++prev_vertex) {
          next = tetra->vertex(prev_vertex)->info();
          if (next != cell && next != neib && next != prev)
            break;
        } // end check 4 vertices
        vface.push_back(T.dual(tetra));
        const int next_vertex = T.mirror_index(tetra, prev_vertex);
        tetra = tetra->neighbor(prev_vertex);
        prev = tetra->vertex(next_vertex)->info();
      } while (prev != start);

      // Populate the circulator indices
      const int n_circs = vcirc.size();
      if (save_circulators) {
        n_circulators_tot += n_circs;
        face.circulator.resize(n_circs);       // Allocate circulator space
        for (int circ = 0; circ < n_circs; ++circ)
          face.circulator[circ] = vcirc[circ];
      }

      // Get the area of the Voronoi faces
      double face_area = 0.;
      for (int circ = 2; circ < n_circs; ++circ)
        face_area += calculate_area(&vface[0][0], &vface[circ-1][0], &vface[circ][0]);
      V[cell] += face_area * r[cell].dist(r[neib]) / 6.;
      vface.clear();                         // Remove all points
    } // end for all neighboring faces
  } // end for all Voronoi cells

  // Set face areas of edge cells based on their mirror neighbors
  #pragma omp parallel for
  for (int cell = 0; cell < n_cells; ++cell) {
    if (edge_flag[cell]) {
      for (auto& face : faces[cell]) {
        const int neib = face.neighbor;      // Neighbor cell index
        for (const auto& mirror : faces[neib]) {
          if (mirror.neighbor == cell) {     // Find reflection index
            face.area = mirror.area;         // Same face area
            break;                           // Increment face
          }
        }
      }
    }
  }
}

#endif // HAVE_CGAL
