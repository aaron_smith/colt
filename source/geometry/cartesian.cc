/*************************
 * geometry/cartesian.cc *
 *************************

 * Cartesian specific functions.

*/

#include "../compile_time.h" // Compile time options

#if CARTESIAN

#include "../proto.h"
#include "../io_hdf5.h" // HDF5 read/write functions

double ran(); // Random number (from global seed)

/* Read cartesian specific data. */
void read_geometry(H5File& f) {
  read(f, "nx", nx);                         // Number of x cells
  read(f, "ny", ny);                         // Number of y cells
  read(f, "nz", nz);                         // Number of z cells
#if FORTRAN_ORDERING
  n2 = nx * ny;                              // Save hyperslab size
  n_cells = n2 * nz;                         // Total number of cells
#else // C-style ordering
  n2 = ny * nz;                              // Save hyperslab size
  n_cells = nx * n2;                         // Total number of cells
#endif

  // Additional geometry info
  double r_box = 0.;                         // Box radius [cm]
  read_if_exists(f, "r_box", r_box);
  if (r_box > 0.) {                          // File contains bbox info
    bbox[0] = {-r_box, -r_box, -r_box};
    bbox[1] = {r_box, r_box, r_box};
  } else {                                   // Read bbox vector info
    vector<Vec3> bbox_vec;                   // Bounding box [cm]
    int n_bbox = read(f, "bbox", bbox_vec, "cm");
    if (n_bbox != 2)
      error("Did not recognize bbox as [[xmin,ymin,zmin],[xmax,ymax,zmax]]");
    bbox[0] = bbox_vec[0];                   // Transfer to bbox array
    bbox[1] = bbox_vec[1];
  }
  max_bbox_width = (bbox[1]-bbox[0]).max();  // Max bbox width [cm]
  wx = (bbox[1].x - bbox[0].x) / double(nx); // Cell x width [cm]
  wy = (bbox[1].y - bbox[0].y) / double(ny); // Cell y width [cm]
  wz = (bbox[1].z - bbox[0].z) / double(nz); // Cell z width [cm]
  dV = wx * wy * wz;                         // Cell volume [cm^3]
}

/* Avoid calculations for certain cells. */
bool avoid_cell(const int) {
  return false;                              // Every cell is valid
}

/* Avoid calculations for certain cells without overrides. */
bool avoid_cell_strict(const int) {
  return false;                              // Every cell is valid
}

/* Return the leftmost corner position [cm] from the cell index. */
inline Vec3 cell_corner(int cell) {
#if FORTRAN_ORDERING
  const int iz = cell / n2;                  // z index
  cell -= iz * n2;                           // Hyperslab remainder
  const int iy = cell / nx;                  // y index
  const int ix = cell - iy * nx;             // Column remainder
#else // C-style ordering
  const int ix = cell / n2;                  // x index
  cell -= ix * n2;                           // Hyperslab remainder
  const int iy = cell / nz;                  // y index
  const int iz = cell - iy * nz;             // Column remainder
#endif
  return {bbox[0].x + wx * double(ix),       // Corner position [cm]
          bbox[0].y + wy * double(iy),
          bbox[0].z + wz * double(iz)};
}

/* Return the center position of the cell. */
Vec3 cell_center(const int cell) {
  return cell_corner(cell) + Vec3(0.5 * wx, 0.5 * wy, 0.5 * wz);
}

/* Draw position uniformly within the cell volume. */
Vec3 random_point_in_cell(const int cell) {
  return cell_corner(cell) + Vec3(ran() * wx, ran() * wy, ran() * wz);
}

/* Check whether a point is in the specified cell. */
bool point_is_in_cell(const Vec3 point, const int cell) {
  const Vec3 r_cell = cell_corner(cell);     // Left corner position
  if (point.x < r_cell.x || point.x >= r_cell.x + wx ||
      point.y < r_cell.y || point.y >= r_cell.y + wy ||
      point.z < r_cell.z || point.z >= r_cell.z + wz)
    return false;                            // Point is not in the cell
  return true;                               // Point is in the cell
}

/* Find the cell index of the specified point. */
int find_cell(const Vec3 point, int) {
  const double x_box = (point.x - bbox[0].x) / wx;
  if (x_box < 0. || x_box >= double(nx))
    return OUTSIDE;                          // Point is outside x range
  const double y_box = (point.y - bbox[0].y) / wy;
  if (y_box < 0. || y_box >= double(ny))
    return OUTSIDE;                          // Point is outside y range
  const double z_box = (point.z - bbox[0].z) / wz;
  if (z_box < 0. || z_box >= double(nz))
    return OUTSIDE;                          // Point is outside z range
  const int ix = floor(x_box);
  const int iy = floor(y_box);
  const int iz = floor(z_box);
#if FORTRAN_ORDERING
  return ix + nx * (iy + ny * iz);           // Found a valid cell
#else // C-style ordering
  return (ix * ny + iy) * nz + iz;           // Found a valid cell
#endif
}

/* Find the minimum distance to a face from a given point in the cell. */
double min_face_distance(const Vec3& point, const int cell) {
  const Vec3 dr = cell_corner(cell) - point; // Relative position
  // x direction
  double l = fabs(dr.x + wx);                // Right x face
  double l_comp = fabs(dr.x);                // Left x face
  if (l_comp < l)
    l = l_comp;
  // y direction
  l_comp = fabs(dr.y + wy);                  // Right y face
  if (l_comp < l)
    l = l_comp;
  l_comp = fabs(dr.y);                       // Left y face
  if (l_comp < l)
    l = l_comp;
  // z direction
  l_comp = fabs(dr.z + wz);                  // Right z face
  if (l_comp < l)
    l = l_comp;
  l_comp = fabs(dr.z);                       // Left z face
  if (l_comp < l)
    l = l_comp;
  return l;                                  // Return min
}

/* Compute the maximum distance the photon can travel in the cell. */
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
  int ixn, iyn, izn, new_cell = cell;        // New cell indices
#if FORTRAN_ORDERING
  const int iz = new_cell / n2;              // z index
  new_cell -= iz * n2;                       // Hyperslab remainder
  const int iy = new_cell / nx;              // y index
  const int ix = new_cell - iy * nx;         // Column remainder
#else // C-style ordering
  const int ix = new_cell / n2;              // x index
  new_cell -= ix * n2;                       // Hyperslab remainder
  const int iy = new_cell / nz;              // y index
  const int iz = new_cell - iy * nz;         // Column remainder
#endif
  const Vec3 r_cell = {bbox[0].x + wx * double(ix), // Corner position [cm]
                       bbox[0].y + wy * double(iy),
                       bbox[0].z + wz * double(iz)};
  double l = positive_infinity, l_comp;      // Comparison distances
  // x direction
  if (direction.x != 0.) {
    if (direction.x > 0.) {
      l_comp = (r_cell.x + wx - point.x) / direction.x; // Right x face
      if (l_comp < l) {
        l = l_comp;
        ixn = ix + 1;
        iyn = iy;
        izn = iz;
      }
    } else {
      l_comp = (r_cell.x - point.x) / direction.x; // Left x face
      if (l_comp < l) {
        l = l_comp;
        ixn = ix - 1;
        iyn = iy;
        izn = iz;
      }
    }
  }
  // y direction
  if (direction.y != 0.) {
    if (direction.y > 0.) {
      l_comp = (r_cell.y + wy - point.y) / direction.y; // Right y face
      if (l_comp < l) {
        l = l_comp;
        ixn = ix;
        iyn = iy + 1;
        izn = iz;
      }
    } else {
      l_comp = (r_cell.y - point.y) / direction.y; // Left y face
      if (l_comp < l) {
        l = l_comp;
        ixn = ix;
        iyn = iy - 1;
        izn = iz;
      }
    }
  }
  // z direction
  if (direction.z != 0.) {
    if (direction.z > 0.) {
      l_comp = (r_cell.z + wz - point.z) / direction.z; // Right z face
      if (l_comp < l) {
        l = l_comp;
        ixn = ix;
        iyn = iy;
        izn = iz + 1;
      }
    } else {
      l_comp = (r_cell.z - point.z) / direction.z; // Left z face
      if (l_comp < l) {
        l = l_comp;
        ixn = ix;
        iyn = iy;
        izn = iz - 1;
      }
    }
  }
  // Check boundaries and reconstruct the new cell index
  if (ixn < 0 || ixn >= nx || iyn < 0 || iyn >= ny || izn < 0 || izn >= nz)
    new_cell = OUTSIDE;
  else
#if FORTRAN_ORDERING
    new_cell = ixn + nx * (iyn + ny * izn);  // Found a valid cell
#else // C-style ordering
    new_cell = (ixn * ny + iyn) * nz + izn;  // Found a valid cell
#endif
  if (l < 0.)
    l = 0.;                                  // Avoid negative distances
  return make_tuple(l, new_cell);
}

#endif // CARTESIAN
