/*******************
 * geometry/cgal.h *
 *******************

 * CGAL: Computational Goemoetry Algorithms Library (optional)

*/

#include "../compile_time.h" // Compile time options

#if HAVE_CGAL

#pragma GCC system_header
#pragma clang system_header

#include <CGAL/Exact_predicates_inexact_constructions_kernel.h>
#include <CGAL/Triangulation_vertex_base_with_info_3.h>
#include <CGAL/Delaunay_triangulation_3.h>

#endif // HAVE_CGAL
