/*!
 * \file    NDArray.h
 *
 * \brief   Data structures for N-dimensional arrays.
 *
 * \details This file contains convenient data structures and the associated
 *          operations needed for N-dimensional arrays. The primary use case
 *          is for handling camera images (2D), data cubes (3D), or similar.
 *          We note that the underlying layout is consistent with HDF5 and
 *          other tools that assume row-major indexing for flattened arrays.
 */

#ifndef NDARRAY_INCLUDED
#define NDARRAY_INCLUDED

#include <vector>
#include <stddef.h>

//! Row-major N-dimensional array which takes arguments in (\f$i_x, i_y, i_z, \ldots\f$) order.
template <int N, typename T>
class NDArray {
  static_assert(N >= 2 && N <= 4, "N must be 2, 3, or 4");

public:
  //! Default constructor that initializes all dimensions to zero.
  NDArray() : dims{} {}  // Empty braces to value-initialize all elements

  /*! \brief Constructor for \f$N=2\f$ based on (\f$n_x, n_y\f$) dimensions.
   *
   *  \param[in] nx Number of horizontal elements (\f$n_x\f$).
   *  \param[in] ny Number of vertical elements (\f$n_y\f$).
   */
  NDArray(const size_t nx, const size_t ny) :
    inner(std::vector<T>(nx * ny)), dims{nx, ny} {
    static_assert(N == 2, "This constructor is only for N == 2");
  }

  /*! \brief Constructor for \f$N=3\f$ based on (\f$n_x, n_y, n_z\f$) dimensions.
   *
   *  \param[in] nx Number of horizontal elements (\f$n_x\f$).
   *  \param[in] ny Number of vertical elements (\f$n_y\f$).
   *  \param[in] nz Number of depth elements (\f$n_z\f$).
   */
  NDArray(const size_t nx, const size_t ny, const size_t nz) :
    inner(std::vector<T>(nx * ny * nz)), dims{nx, ny, nz} {
    static_assert(N == 3, "This constructor is only for N == 3");
  }


  /*! \brief Constructor for \f$N=4\f$ based on (\f$n_x, n_y, n_z, n_t\f$) dimensions.
   *
   *  \param[in] nx Number of horizontal elements (\f$n_x\f$).
   *  \param[in] ny Number of vertical elements (\f$n_y\f$).
   *  \param[in] nz Number of depth elements (\f$n_z\f$).
   *  \param[in] nt Number of time elements (\f$n_t\f$).
   */
  NDArray(const size_t nx, const size_t ny, const size_t nz, const size_t nt) :
    inner(std::vector<T>(nx * ny * nz * nt)), dims{nx, ny, nz, nt} {
    static_assert(N == 4, "This constructor is only for N == 4");
  }

  /*! \brief Access the number of horizontal elements (\f$n_x\f$). Requires \f$N \geq 1\f$.
   *
   *  \return Number of horizontal elements (\f$n_x\f$).
   */
  size_t nx() const {
    return dims[0];
  }

  /*! \brief Access the number of vertical elements (\f$n_y\f$). Requires \f$N \geq 2\f$.
   *
   *  \return Number of vertical elements (\f$n_y\f$).
   */
  size_t ny() const {
    return dims[1];
  }

  /*! \brief Access the number of depth elements (\f$n_z\f$). Requires \f$N \geq 3\f$.
   *
   *  \return Number of depth elements (\f$n_z\f$).
   */
  size_t nz() const {
    static_assert(N >= 3, "This dimension is only for N >= 3");
    return dims[2];
  }

  /*! \brief Access the number of time elements (\f$n_t\f$). Requires \f$N \geq 4\f$.
   *
   *  \return Number of time elements (\f$n_t\f$).
   */
  size_t nt() const {
    static_assert(N == 4, "This dimension is only for N >= 4");
    return dims[3];
  }

  /*! \brief Access the total number of elements in the N-dimensional array (\f$n_x \times n_y \times \ldots\f$).
   *
   *  \return Total number of elements (\f$n_x \times n_y \times \ldots\f$).
   */
  size_t size() const {
    return inner.size();
  }

  /*! \brief Access a pointer to the internal data.
   *
   *  \return Pointer to the internal data.
   */
  T* data() {
    return inner.data();
  }

  /*! \brief Access a pointer to the \f$i_x^\text{th}\f$ row of data.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *
   *  \return Pointer to the \f$i_x^\text{th}\f$ row of data.
   */
  T* data(const size_t ix) {
    return inner.data() + ix * subarray_size();
  }

  /*! \brief Access a pointer to the (\f$i_x^\text{th}, i_y^\text{th}\f$) row and column of data. Requires \f$N \geq 3\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *
   *  \return Pointer to the (\f$i_x^\text{th}, i_y^\text{th}\f$) row and column of data.
   */
  T* data(const size_t ix, const size_t iy) {
    static_assert(N >= 3, "This data accessor is only for N >= 3");
    return inner.data() + (ix * dims[1] + iy) * subsubarray_size();
  }

  /*! \brief Access a pointer to the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}\f$) row, column, and depth of data. Requires \f$N \geq 4\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *
   *  \return Pointer to the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}\f$) row, column, and depth of data.
   */
  T* data(const size_t ix, const size_t iy, const size_t iz) {
    static_assert(N >= 4, "This data accessor is only for N >= 4");
    return inner.data() + ((ix * dims[1] + iy) * dims[2] + iz) * dims[3];
  }

  /*! \brief Read access for the (\f$i_x^\text{th}, i_y^\text{th}\f$) element. Requires \f$N = 2\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *
   *  \return Value of the data at (\f$i_x, i_y\f$).
   */
  T operator()(const size_t ix, const size_t iy) const {
    static_assert(N == 2, "This access operator is only for N == 2");
    #if check_bounds
      if (ix >= dims[0] || iy >= dims[1]) {
        error("Index out of bounds: [ix, iy] = [" + std::to_string(ix) + ", " + std::to_string(iy) +
        "], [nx, ny] = [" + std::to_string(dims[0]) + ", " + std::to_string(dims[1]) + "]");
      }
    #endif
    return inner[ix * dims[1] + iy];
  }

  /*! \brief Write access for the (\f$i_x^\text{th}, i_y^\text{th}\f$) element. Requires \f$N = 2\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *
   *  \return Reference to the data at (\f$i_x, i_y\f$).
   */
  T& operator()(const size_t ix, const size_t iy) {
    static_assert(N == 2, "This access operator is only for N == 2");
    #if check_bounds
      if (ix >= dims[0] || iy >= dims[1]) {
        error("Index out of bounds: [ix, iy] = [" + std::to_string(ix) + ", " + std::to_string(iy) +
        "], [nx, ny] = [" + std::to_string(dims[0]) + ", " + std::to_string(dims[1]) + "]");
      }
    #endif
    return inner[ix * dims[1] + iy];
  }

  /*! \brief Read access for the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}\f$) element. Requires \f$N = 3\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *
   *  \return Value of the data at (\f$i_x, i_y, i_z\f$).
   */
  T operator()(const size_t ix, const size_t iy, const size_t iz) const {
    static_assert(N == 3, "This access operator is only for N == 3");
    #if check_bounds
      if (ix >= dims[0] || iy >= dims[1] || iz >= dims[2]) {
        error("Index out of bounds: [ix, iy, iz] = [" + std::to_string(ix) + ", " + std::to_string(iy) + ", " + std::to_string(iz) +
        "], [nx, ny, nz] = [" + std::to_string(dims[0]) + ", " + std::to_string(dims[1]) + ", " + std::to_string(dims[2]) + "]");
      }
    #endif
    return inner[(ix * dims[1] + iy) * dims[2] + iz];
  }

  /*! \brief Write access for the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}\f$) element.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *
   *  \return Reference to the data at (\f$i_x, i_y, i_z\f$).
   */
  T& operator()(const size_t ix, const size_t iy, const size_t iz) {
    static_assert(N == 3, "This access operator is only for N == 3");
    #if check_bounds
      if (ix >= dims[0] || iy >= dims[1] || iz >= dims[2]) {
        error("Index out of bounds: [ix, iy, iz] = [" + std::to_string(ix) + ", " + std::to_string(iy) + ", " + std::to_string(iz) +
        "], [nx, ny, nz] = [" + std::to_string(dims[0]) + ", " + std::to_string(dims[1]) + ", " + std::to_string(dims[2]) + "]");
      }
    #endif
    return inner[(ix * dims[1] + iy) * dims[2] + iz];
  }

  /*! \brief Read access for the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}, i_t^\text{th}\f$) element. Requires \f$N = 4\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *  \param[in] it Time index (\f$i_t\f$).
   *
   *  \return Value of the data at (\f$i_x, i_y, i_z, i_t\f$).
   */
  T operator()(const size_t ix, const size_t iy, const size_t iz, const size_t it) const {
    static_assert(N == 4, "This access operator is only for N == 4");
    #if check_bounds
      if (ix >= dims[0] || iy >= dims[1] || iz >= dims[2] || it >= dims[3]) {
        error("Index out of bounds: [ix, iy, iz, it] = [" + std::to_string(ix) + ", " + std::to_string(iy) + ", " + std::to_string(iz) + ", " + std::to_string(it) +
        "], [nx, ny, nz, nt] = [" + std::to_string(dims[0]) + ", " + std::to_string(dims[1]) + ", " + std::to_string(dims[2]) + ", " + std::to_string(dims[3]) + "]");
      }
    #endif
    return inner[((ix * dims[1] + iy) * dims[2] + iz) * dims[3] + it];
  }

  /*! \brief Write access for the (\f$i_x^\text{th}, i_y^\text{th}, i_z^\text{th}, i_t^\text{th}\f$) element. Requires \f$N = 4\f$.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] iy Column index (\f$i_y\f$).
   *  \param[in] iz Depth index (\f$i_z\f$).
   *  \param[in] it Time index (\f$i_t\f$).
   *
   *  \return Reference to the data at (\f$i_x, i_y, i_z, i_t\f$).
   */
  T& operator()(const size_t ix, const size_t iy, const size_t iz, const size_t it) {
    static_assert(N == 4, "This access operator is only for N == 4");
    #if check_bounds
      if (ix >= dims[0] || iy >= dims[1] || iz >= dims[2] || it >= dims[3]) {
        error("Index out of bounds: [ix, iy, iz, it] = [" + std::to_string(ix) + ", " + std::to_string(iy) + ", " + std::to_string(iz) + ", " + std::to_string(it) +
        "], [nx, ny, nz, nt] = [" + std::to_string(dims[0]) + ", " + std::to_string(dims[1]) + ", " + std::to_string(dims[2]) + ", " + std::to_string(dims[3]) + "]");
      }
    #endif
    return inner[((ix * dims[1] + iy) * dims[2] + iz) * dims[3] + it];
  }

  /*! \brief Read access for the \f$i^\text{th}\f$ flat element.
   *
   *  \param[in] i Flat index (\f$i\f$).
   *
   *  \return Value of the flat data at (\f$i\f$).
   */
  T operator[](const size_t i) const {
    return inner[i];
  }

  /*! \brief Write access for the \f$i^\text{th}\f$ flat element.
   *
   *  \param[in] i Flat index (\f$i\f$).
   *
   *  \return Reference to the flat data at (\f$i\f$).
   */
  T& operator[](const size_t i) {
    return inner[i];
  }

  typedef typename std::vector<T>::iterator iterator;
  typedef typename std::vector<T>::const_iterator const_iterator;

  /*! \brief Access an iterator for the beginning of the internal data.
   *
   *  \return Iterator for the beginning of the internal data.
   */
  iterator begin() {
    return inner.begin();
  }

  /*! \brief Access an iterator for the end of the internal data.
   *
   *  \return Iterator for the end of the internal data.
   */
  iterator end() {
    return inner.end();
  }

  /*! \brief Access a const iterator for the beginning of the internal data.
   *
   *  \return Const iterator for the beginning of the internal data.
   */
  const_iterator cbegin() const {
    return inner.cbegin();
  }

  /*! \brief Access a const iterator for the end of the internal data.
   *
   *  \return Const iterator for the end of the internal data.
   */
  const_iterator cend() const {
    return inner.cend();
  }

  /*! \brief Calculate the sum of all data elements using OpenMP parallelization.
   *
   *  \return Sum of all data.
   */
  T omp_sum() const {
    T result = static_cast<T>(0);
    const size_t i_end = inner.size();
    #pragma omp parallel for reduction(+:result)
    for (size_t i = 0; i < i_end; ++i)
      result += inner[i];
    return result;
  }

  /*! \brief Calculate the sum of the \f$i_x^\text{th}\f$ row of data elements.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *
   *  \return Sum of the \f$i_x^\text{th}\f$ row of data.
   */
  T sum(const size_t ix) const {
    T result = static_cast<T>(0);
    const size_t i_start = ix * subarray_size();
    const size_t i_end = i_start + subarray_size();
    for (size_t i = i_start; i < i_end; ++i)
      result += inner[i];
    return result;
  }

  /*! \brief Calculate the average of the \f$i_x^\text{th}\f$ row of data elements.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *
   *  \return Average of the \f$i_x^\text{th}\f$ row of data.
   */
  T mean(const size_t ix) const {
    return sum(ix) / static_cast<T>(subarray_size());
  }

  /*! \brief Calculate the weighted average by \f$w\f$ of the \f$i_x^\text{th}\f$ row of data elements.
   *
   *  \param[in] ix Row index (\f$i_x\f$).
   *  \param[in] weight Weight field (\f$w\f$).
   *
   *  \return Weighted average by \f$w\f$ of the \f$i_x^\text{th}\f$ row of data.
   */
  T weighted_mean(const size_t ix, const NDArray<N, T>& weight) const {
    const T zero = static_cast<T>(0);
    T result = zero;
    T result_norm = zero;
    const size_t i_start = ix * subarray_size();
    const size_t i_end = i_start + subarray_size();
    for (size_t i = i_start; i < i_end; ++i) {
      result += weight[i] * inner[i];
      result_norm += weight[i];
    }
    return (result_norm > zero) ? result / result_norm : zero;
  }

private:
  std::vector<T> inner;                      // Internal data
  size_t dims[N];                            // Dimensions

  /*! \brief Calculate the product of dimensions for the subarray size.
   *
   *  \return Product of dimensions for the subarray size.
   */
  constexpr size_t subarray_size() const {
    if constexpr (N == 2) {
      return dims[1];
    } else if constexpr (N == 3) {
      return dims[1] * dims[2];
    } else if constexpr (N == 4) {
      return dims[1] * dims[2] * dims[3];
    }
  }

  /*! \brief Calculate the product of dimensions for the subsubarray size. Requires \f$N \geq 3\f$.
   *
   *  \return Product of dimensions for the subsubarray size.
   */
  constexpr size_t subsubarray_size() const {
    if constexpr (N == 3) {
      return dims[2];
    } else if constexpr (N == 4) {
      return dims[2] * dims[3];
    }
  }
};

#endif // NDARRAY_INCLUDED
