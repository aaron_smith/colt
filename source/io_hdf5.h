/*!
 * \file    io_hdf5.h
 *
 * \brief   HDF5 helper function definitions.
 *
 * \details This file contains convenience functions and wrappers to help with
 *          the general management of HDF5 files, mostly read/write operations.
 */

#ifndef IO_HDF5_INCLUDED
#define IO_HDF5_INCLUDED

#include "proto.h"
#include <typeinfo>
#include "H5Cpp.h"
using namespace H5;

//! Macro to distinguish between HDF5 PredTypes with template types.
#define H5PT ((typeid(T) == typeid(int))    ? PredType::NATIVE_INT :\
             ((typeid(T) == typeid(double)) ? PredType::NATIVE_DOUBLE :\
             ((typeid(T) == typeid(float))  ? PredType::NATIVE_FLOAT :\
             ((typeid(T) == typeid(long long)) ? PredType::NATIVE_LLONG : PredType::NATIVE_HERR))))

static_assert(sizeof(Vec3) == sizeof(double[3]),
  "Vec3 must be the same size as double[3] for the read/write code to work");
static_assert(sizeof(Vec3F) == sizeof(float[3]),
  "Vec3F must be the same size as float[3] for the read/write code to work");

/*! \brief Determine the name length of an HDF5 DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *
 *  \return Name length of the HDF5 DataSet.
 */
template <typename File_or_Group>
inline int h5_len(const File_or_Group& f) {
  return H5Iget_name(f.getId(), NULL, 0);
}

/*! \brief Determine the name of an HDF5 DataSet (of known name length).
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] len Name length of the HDF5 DataSet.
 *
 *  \return Name of the HDF5 DataSet.
 */
template <typename File_or_Group>
inline string h5_str(const File_or_Group& f, int len) {
  char name[len];
  H5Iget_name(f.getId(),name,len+1);
  return name;
}

/*! \brief Determine the name of an HDF5 DataSet (of unknown name length).
 *
 *  \param[in] f HDF5 File or Group object.
 *
 *  \return Name of the HDF5 DataSet.
 */
template <typename File_or_Group>
inline string h5_str(const File_or_Group& f) {
  return h5_str(f, h5_len(f));
}

//! Extends error messages to include information about the HDF5 file.
#define h5_error(message) {\
  int len = h5_len(f);\
  root_error(message + (" for '" + name + "' from " + h5_str(f, len) + " in " + init_file));\
}

//! Common error handling for HDF5 files in case of unmet expectations.
#define h5_expect(what, want, have) {\
  if ((want) != (have))\
    h5_error("Expected array " + string(what) + " to be " + to_string(want) +\
             " but got " + to_string(have));\
}

/*! \brief Returns whether the link exists or not.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 link.
 */
template <typename File_or_Group>
inline bool exists(const File_or_Group& f, const string& name) {
  return (H5Lexists(f.getId(), name.c_str(), H5P_DEFAULT) > 0);
}

/*! \brief Returns whether the attribute exists or not.
 *
 *  \param[in] f HDF5 File, Group, or DataSet object.
 *  \param[in] name Name of the HDF5 attribute.
 */
template <typename File_or_Group_or_DataSet>
inline bool attribute_exists(const File_or_Group_or_DataSet& f, const string& name) {
  return (H5Aexists(f.getId(), name.c_str()) > 0);
}

/*! \brief Verify that an HDF5 Attribute exists before attempting to read.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 */
template <typename File_or_Group>
inline void verify_attribute(const File_or_Group& f, const string& name) {
  if (!attribute_exists(f, name))
    h5_error("Missing expected attribute");
}

/*! \brief Verify that an HDF5 DataSet exists before attempting to read.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 */
template <typename File_or_Group>
inline void verify_field(const File_or_Group& f, const string& name) {
  if (!exists(f, name))
    h5_error("Missing expected field");
}

/*! \brief Verify that an HDF5 DataSet exists before attempting to read.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] names List of possible names for the HDF5 DataSet.
 */
template <typename File_or_Group>
inline string verify_field(const File_or_Group& f, strings names) {
  for (const auto& name : names) {
    if (exists(f, name))
      return name;
  }
  auto name = names[0]; // Still allow an error if unsuccessful
  h5_error("Missing expected field");
  return name;
}

/*! \brief Verify that an HDF5 DataSet has the expected shape.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] d HDF5 DataSet object.
 *  \param[in] shape Expected data sizes for each dimension.
 */
template <typename File_or_Group>
void verify_shape(const File_or_Group& f, const string& name, const DataSet& d, const vector<hsize_t>& shape) {
  int dims = shape.size();
  hsize_t s_shape[dims];
  DataSpace s = DataSpace(d.getSpace());
  h5_expect("dimensions", dims, s.getSimpleExtentNdims());
  h5_expect("dimensions", dims, s.getSimpleExtentDims(s_shape, NULL));
  for (int i = 0; i < dims; ++i)
    h5_expect("shape[" + to_string(i) + "]", shape[i], s_shape[i]);
}

/*! \brief Infer the shape of an HDF5 DataSet (first dimension only).
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *
 *  \return Data size for the first shape dimension.
 */
template <typename File_or_Group>
hsize_t get_size(const File_or_Group& f, const string& name) {
  DataSet d = f.openDataSet(name);           // Access the dataset
  DataSpace s = DataSpace(d.getSpace());     // Access the dataspace
  hsize_t shape[s.getSimpleExtentNdims()];   // Buffer for shape
  s.getSimpleExtentDims(shape, NULL);        // Fill shape
  return shape[0];                           // Return unknown vector length
}

/*! \brief Verify the units of an HDF5 DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] d HDF5 DataSet object.
 *  \param[in] units Expected DataSet units name.
 */
template <typename File_or_Group>
void verify_units(const File_or_Group& f, const string& name, const DataSet& d, const string& units) {
  if (attribute_exists(d, "units")) {
    StrType PT(PredType::C_S1, H5T_VARIABLE);
    H5std_string buffer("");
    Attribute a = d.openAttribute("units");
    a.read(PT, buffer);
    h5_expect("units", units, buffer);
  } // If units are expected but none are present throw an error
  else if (!units.empty())
    h5_error("Missing expected units of " + units);
}

/*! \brief Read an HDF5 scalar Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] attr Reference to store the HDF5 Attribute.
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, T& attr) {
  verify_attribute(f, name);                 // Ensure the attribute exists
  Attribute a = f.openAttribute(name);       // Access the attribute
  a.read(H5PT, &attr);                       // Read attribute from file
}

/*! \brief Read an HDF5 scalar Attribute if it exists.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] attr Reference to store the HDF5 Attribute.
 */
template <typename File_or_Group, typename T>
void read_if_exists(const File_or_Group& f, const string& name, T& attr) {
  if (attribute_exists(f, name)) {
    Attribute a = f.openAttribute(name);     // Access the attribute
    a.read(H5PT, &attr);                     // Read attribute from file
  }
}

/*! \brief Read a single element from an HDF5 vector<T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] attr Reference to store the HDF5 DataSet element.
 *  \param[in] index Index of the element in the vector.
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, T& attr, const hsize_t index) {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  DataSpace s = DataSpace(d.getSpace());     // Access the dataspace
  int ndims = s.getSimpleExtentNdims();      // Number of dimensions
  if (ndims != 1)
    h5_error("Expected a 1D DataSet but ndims = " + to_string(ndims));
  hsize_t size;                              // Number of elements
  s.getSimpleExtentDims(&size, NULL);        // Fill the value
  if (index >= size)
    h5_error("Requested out of range index");
  const hsize_t count = 1;                   // Extract a single element
  DataSpace hs(1, &count);                   // Specify hyperslab data space
  s.selectHyperslab(H5S_SELECT_SET, &count, &index);
  d.read(&attr, H5PT, hs, s);                // Read element from dataset
}

/*! \brief Read a single element from an HDF5 vector<GenericVec3<T>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet element.
 *  \param[in] index Index of the element in the vector.
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, GenericVec3<T>& vec, const hsize_t index) {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  DataSpace s = DataSpace(d.getSpace());     // Access the dataspace
  int ndims = s.getSimpleExtentNdims();      // Number of dimensions
  if (ndims != 2)
    h5_error("Expected a 2D DataSet but ndims = " + to_string(ndims));
  hsize_t dims[2];                           // Dimension sizes
  s.getSimpleExtentDims(dims, NULL);         // Fill the values
  if (dims[1] != 3)
    h5_error("Expected a DataSet of GenericVec3<T> objects but dims[1] = " + to_string(dims[1]));
  if (index >= dims[0])
    h5_error("Requested out of range index");
  const hsize_t hdims[] = {1, 3};            // Extract a single GenericVec3<T> element
  const hsize_t hoffs[] = {index, 0};        // Specify hyperslab offsets
  DataSpace hs(2, hdims);                    // Specify hyperslab data space
  s.selectHyperslab(H5S_SELECT_SET, hdims, hoffs);
  d.read(&vec[0], H5PT, hs, s);              // Read element from dataset
}

/*! \brief Read a single element from an HDF5 vector<Array<T,N>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet element.
 *  \param[in] index Index of the element in the vector.
 */
template <typename File_or_Group, typename T, size_t N>
void read(const File_or_Group& f, const string& name, array<T, N>& vec, const hsize_t index) {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  DataSpace s = DataSpace(d.getSpace());     // Access the dataspace
  int ndims = s.getSimpleExtentNdims();      // Number of dimensions
  if (ndims != 2)
    h5_error("Expected a 2D DataSet but ndims = " + to_string(ndims));
  hsize_t dims[2];                           // Dimension sizes
  s.getSimpleExtentDims(dims, NULL);         // Fill the values
  if (dims[1] != N)
    h5_error("Expected a DataSet of array<" + to_string(N) + "> objects but dims[1] = " + to_string(dims[1]));
  if (index >= dims[0])
    h5_error("Requested out of range index");
  const hsize_t hdims[] = {1, N};            // Extract a single Array<T,N> element
  const hsize_t hoffs[] = {index, 0};        // Specify hyperslab offsets
  DataSpace hs(2, hdims);                    // Specify hyperslab data space
  s.selectHyperslab(H5S_SELECT_SET, hdims, hoffs);
  d.read(&vec[0], H5PT, hs, s);              // Read element from dataset
}

/*! \brief Read an HDF5 vector<T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] size Expected size of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, vector<T>& vec,
          const hsize_t size, const string& units = "") {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size});          // Ensure it has the expected shape
  verify_units(f, name, d, units);           // Ensure the units are as expected
  vec.resize(size);                          // Allocate space for the vector
  d.read(vec.data(), H5PT);                  // Read dataset from file
}

/*! \brief Read an HDF5 NDArray<2, T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] nx Expected size (x) of the HDF5 DataSet for verification.
 *  \param[in] ny Expected size (y) of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, NDArray<2, T>& vec,
          const hsize_t nx, const hsize_t ny, const string& units = "") {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {nx,ny});         // Ensure it has the expected shape
  verify_units(f, name, d, units);           // Ensure the units are as expected
  vec = NDArray<2, T>(nx,ny,nz);             // Allocate space for the vector
  d.read(vec.data(), H5PT);                  // Read dataset from file
}

/*! \brief Read an HDF5 NDArray<3, T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] nx Expected size (x) of the HDF5 DataSet for verification.
 *  \param[in] ny Expected size (y) of the HDF5 DataSet for verification.
 *  \param[in] nz Expected size (z) of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, NDArray<3, T>& vec,
          const hsize_t nx, const hsize_t ny, const hsize_t nz, const string& units = "") {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {nx,ny,nz});      // Ensure it has the expected shape
  verify_units(f, name, d, units);           // Ensure the units are as expected
  vec = NDArray<3, T>(nx,ny,nz);             // Allocate space for the vector
  d.read(vec.data(), H5PT);                  // Read dataset from file
}

/*! \brief Read an HDF5 vector<int> DataSet and convert to vector<bool>.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] size Expected size of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group>
void read(const File_or_Group& f, const string& name, vector<bool>& vec,
          const hsize_t size, const string& units = "") {
  vector<int> vec_int;                       // Read as integer flags
  read(f, name, vec_int, size, units);
  vec.resize(size);                          // Allocate space for the vector
  for (size_t i = 0; i < size; ++i)
    vec[i] = (bool)vec_int[i];               // Convert to bool vector
}

/*! \brief Read an HDF5 vector<GenericVec3<T>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] size Expected size of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, const string& name, vector<GenericVec3<T>>& vec,
          const hsize_t size, const string units = "") {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size, 3});       // Ensure it has the expected shape
  verify_units(f, name, d, units);           // Ensure the units are as expected
  vec.resize(size);                          // Allocate space for the vector
  d.read(vec.data(), H5PT);                  // Read dataset from file
}

/*! \brief Read an HDF5 vector<Array<T,N>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] size Expected size of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T, size_t N>
void read(const File_or_Group& f, const string& name, vector<array<T, N>>& vec,
          const hsize_t size, const string units = "") {
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size, N});       // Ensure it has the expected shape
  verify_units(f, name, d, units);           // Ensure the units are as expected
  vec.resize(size);                          // Allocate space for the vector
  d.read(vec.data(), H5PT);                  // Read dataset from file
}

/*! \brief Read an HDF5 vector<T> DataSet without first verifying the size.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] units Expected DataSet units name (optional).
 *
 *  \return Size of the HDF5 DataSet.
 */
template <typename File_or_Group, typename T>
size_t read(const File_or_Group& f, const string& name, vector<T>& vec, const string units = "") {
  verify_field(f, name);                     // Ensure the dataset exists
  read(f, name, vec, get_size(f, name), units); // Read the vector
  return vec.size();
}

/*! \brief Read an HDF5 vector<T> DataSet allowing multiple names.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] names List of possible names for the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] size Expected size of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read(const File_or_Group& f, strings names, vector<T>& vec,
          const hsize_t size, const string& units = "") {
  auto name = verify_field(f, names);        // Ensure the dataset exists
  read(f, name, vec, size, units);           // Read first valid name
}

/*! \brief Read an HDF5 vector<T> DataSet without first verifying the size
 *         but allowing multiple names.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] names List of possible names for the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] units Expected DataSet units name (optional).
 *
 *  \return Size of the HDF5 DataSet.
 */
template <typename File_or_Group, typename T>
size_t read(const File_or_Group& f, strings names, vector<T>& vec, const string& units = "") {
  auto name = verify_field(f, names);        // Ensure the dataset exists
  return read(f, name, vec, units);          // Read first valid name
}

/*! \brief Read an HDF5 vector<T> DataSet if it exists.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] val Default value in case the HDF5 DataSet does not exist.
 *  \param[in] size Expected size of the HDF5 DataSet for verification.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read_if_exists(const File_or_Group& f, const string& name, vector<T>& vec,
                    const T val, const hsize_t size, const string& units = "") {
  if (exists(f, name)) {
    read(f, name, vec, size, units);         // Read dataset from file
  } else {
    vec.resize(size);                        // Allocate space
    #pragma omp parallel for
    for (hsize_t i = 0; i < size; ++i)
      vec[i] = val;                          // Set to constant value
  }
}

/*! \brief Read an HDF5 vector<T> DataSet if it exists, else scale to vector.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] ref Reference vector to scale to if the HDF5 DataSet does not exist.
 *  \param[in] scale Scaling factor to apply to the reference vector.
 *  \param[in] units Expected DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void read_or_scale(const File_or_Group& f, const string& name, vector<T>& vec,
                   const vector<T>& ref, const T scale, const string& units = "") {
  const hsize_t size = ref.size();           // Inherit size from reference
  if (exists(f, name)) {
    read(f, name, vec, size, units);         // Read dataset from file
  } else {
    vec.resize(size);                        // Allocate space
    #pragma omp parallel for
    for (hsize_t i = 0; i < size; ++i)
      vec[i] = scale * ref[i];               // Rescale reference vector
  }
}

#define MIN_FRAC 1e-10

/*! \brief Fill a vector<T> with the remainder of a total value.
 *
 *  \param[out] vec Reference to store the HDF5 DataSet.
 *  \param[in] refs Reference to subtraction vectors.
 *  \param[in] tot Default total value to subtract from.
 */
template <typename T>
void fill_remaining(vector<T>& vec, const vector<vector<T>*>& refs, const T tot = static_cast<T>(1)) {
  const int n_refs = refs.size();            // Number of references
  const hsize_t size = refs[0]->size();      // Inherit size from first ref
  for (int j = 1; j < n_refs; ++j)
    if (refs[j]->size() != size)
      error("Mismatched sizes in fill_remaining: "+to_string(j+1)+" / "+to_string(n_refs));
  vec.resize(size);                          // Allocate space
  #pragma omp parallel for
  for (hsize_t i = 0; i < size; ++i) {
    T sum = T{};                             // Initialize sum to zero
    for (const auto* ref : refs)
      sum += (*ref)[i];                      // Add element from each ref
    vec[i] = tot - sum;                      // Set to remaining value
    if (vec[i] < static_cast<T>(MIN_FRAC))
      vec[i] = static_cast<T>(MIN_FRAC);     // Do not allow negative values
  }
}

/*! \brief If the Attribute already exists then raise an error.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 */
template <typename T>
void check_attribute(const T& f, const string name) {
  if (attribute_exists(f, name))
    error("Attribute '" + name + "' already exists in " + h5_str(f));
}

/*! \brief If the DataSet already exists then raise an error.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 */
template <typename File_or_Group>
void check_dataset(const File_or_Group& f, const string name) {
  if (exists(f, name))
    error("DataSet '" + name + "' already exists in " + h5_str(f));
}

/*! \brief Write an HDF5 string Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] attr Value for the HDF5 Attribute.
 */
template <typename File_or_Group>
void write(const File_or_Group& f, const string name, const string attr) {
  check_attribute(f, name);                  // Ensure attribute uniqueness
  StrType PT(PredType::C_S1, H5T_VARIABLE);
  Attribute a = f.createAttribute(name, PT, H5S_SCALAR);
  a.write(PT, attr);
}

/*! \brief Write the units for an HDF5 DataSet.
 *
 *  \param[in] d HDF5 DataSet object.
 *  \param[in] units DataSet units name.
 */
template <typename T>
void write_units(const T& d, const string units) {
  check_attribute(d, "units");               // Ensure attribute uniqueness
  StrType PT(PredType::C_S1, H5T_VARIABLE);
  Attribute a = d.createAttribute("units", PT, H5S_SCALAR);
  a.write(PT, units);
}

/*! \brief Write an HDF5 scalar Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] attr Value for the HDF5 Attribute.
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string name, const T attr) {
  check_attribute(f, name);                  // Ensure attribute uniqueness
  Attribute a = f.createAttribute(name, H5PT, H5S_SCALAR); // Access
  a.write(H5PT, &attr);                      // Write attribute to file
}

/*! \brief Write an HDF5 bool Attribute (as int).
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] attr Value for the HDF5 Attribute.
 */
template <typename File_or_Group>
void write(const File_or_Group& f, const string name, const bool attr) {
  write(f, name, int(attr));                 // Write as int attribute
}

/*! \brief Write an HDF5 vector<T> Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] vec Reference to the vector data.
 */
template <typename File_or_Group, typename T>
void write_attr(const File_or_Group& f, const string& name, vector<T>& vec) {
  check_attribute(f, name);                  // Ensure attribute uniqueness
  const hsize_t size = vec.size();           // Size of the dataset
  DataSpace s(1, &size);                     // Specify data space
  Attribute a = f.createAttribute(name, H5PT, s); // Access
  a.write(H5PT, vec.data());                 // Write attribute to file
}

/*! \brief Write an HDF5 array<T,N> Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] vec Reference to the array data.
 */
template <typename File_or_Group, typename T, size_t N>
void write_attr(const File_or_Group& f, const string& name, array<T, N>& vec) {
  check_attribute(f, name);                  // Ensure attribute uniqueness
  const hsize_t size = N;                    // Size of the dataset
  DataSpace s(1, &size);                     // Specify data space
  Attribute a = f.createAttribute(name, H5PT, s); // Access
  a.write(H5PT, vec.data());                 // Write attribute to file
}

/*! \brief Write an HDF5 GenericVec3<T> Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] vec Reference to the GenericVec3<T> data.
 */
template <typename File_or_Group, typename T>
void write_attr(const File_or_Group& f, const string& name, GenericVec3<T>& vec) {
  check_attribute(f, name);                  // Ensure attribute uniqueness
  const hsize_t size = 3;                    // Size of GenericVec3<T>
  DataSpace s(1, &size);                     // Specify data space
  Attribute a = f.createAttribute(name, H5PT, s); // Access
  a.write(H5PT, &vec[0]);                    // Write attribute to file
}

/*! \brief Write an HDF5 vector<string> Attribute.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 Attribute.
 *  \param[in] vec Reference to the vector<string> data.
 */
template <typename File_or_Group>
void write_attr(const File_or_Group& f, const string& name, strings& vec) {
  check_attribute(f, name);                  // Ensure attribute uniqueness
  const hsize_t size = vec.size();           // Size of the dataset
  DataSpace s(1, &size);                     // Specify data space
  StrType PT(PredType::C_S1, H5T_VARIABLE);
  Attribute a = f.createAttribute(name, PT, s); // Access
  vector<const char *> cvec;                 // Convert to a C string array
  for (size_t i = 0; i < size; ++i)
    cvec.push_back(vec[i].c_str());
  a.write(PT, cvec.data());                  // Write attribute to file
}

/*! \brief Write an HDF5 vector<T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, vector<T>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t size = vec.size();           // Size of the dataset
  DataSpace s(1, &size);                     // Specify data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  d.write(vec.data(), H5PT);                 // Write dataset to file
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Overwrite an HDF5 DataSet (delete link if it exists).
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] data Reference to the data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void overwrite(const File_or_Group& f, const string& name, T& data, const string& units = "") {
  if (exists(f, name)) {
    H5Ldelete(f.getId(), name.c_str(), H5P_DEFAULT);
    if (verbose)
      cout << "\nWarning: Replacing " << name << " dataset." << endl;
  }
  write(f, name, data, units);               // Write dataset
}

/*! \brief Write an HDF5 NDArray<2, T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the Image data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, NDArray<2, T>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t dims[] = {vec.nx(), vec.ny()}; // Dimension sizes
  DataSpace s(2, dims);                      // Specify data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  d.write(vec.data(), H5PT);                 // Write dataset to file
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 NDArray<3, T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the Cube data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, NDArray<3, T>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t dims[] = {vec.nx(), vec.ny(), vec.nz()}; // Dimension sizes
  DataSpace s(3, dims);                      // Specify data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  d.write(vec.data(), H5PT);                 // Write dataset to file
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 GenericVec3<T> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the GenericVec3<T> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, GenericVec3<T> vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t size = 3;                    // Size of the dataset
  DataSpace s(1, &size);                     // Specify data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  d.write(&vec[0], H5PT);                    // Write dataset to file
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 vector<GenericVec3<T>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector<GenericVec3<T>> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, vector<GenericVec3<T>>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t dims[] = {vec.size(), 3};    // Dimension sizes
  DataSpace s(2, dims);                      // Specify data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  d.write(vec.data(), H5PT);                 // Write dataset to file
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 vector<array<T,N>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector<array<T,N>> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T, size_t N>
void write(const File_or_Group& f, const string& name, vector<array<T, N>>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t dims[] = {vec.size(), N};    // Dimension sizes
  DataSpace s(2, dims);                      // Specify data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  d.write(vec.data(), H5PT);                 // Write dataset to file
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 vector<vector<T>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector<vector<T>> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, vector<vector<T>>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t fdims[] = {vec.size(), vec[0].size()}; // Dimension sizes
  DataSpace s(2, fdims);                     // Specify file data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  const hsize_t hdims[] = {1, fdims[1]};     // Hyperslab dimensions
  DataSpace hs(2, hdims);                    // Specify hyperslab data space
  for (hsize_t i = 0; i < vec.size(); ++i) {
    hsize_t offsets[] = {i, 0};              // Specify hyperslab offsets
    s.selectHyperslab(H5S_SELECT_SET, hdims, offsets);
    d.write(vec[i].data(), H5PT, hs, s);     // Write hyperslab to file
  }
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 vector<NDArray<2, T>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector<NDArray<2, T>> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, vector<NDArray<2, T>>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t fdims[] = {vec.size(), vec[0].nx(), vec[0].ny()}; // Dimension sizes
  DataSpace s(3, fdims);                     // Specify file data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  const hsize_t hdims[] = {1, fdims[1], fdims[2]}; // Hyperslab dimensions
  DataSpace hs(3, hdims);                    // Specify hyperslab data space
  for (hsize_t i = 0; i < vec.size(); ++i) {
    hsize_t offsets[] = {i, 0, 0};           // Specify hyperslab offsets
    s.selectHyperslab(H5S_SELECT_SET, hdims, offsets);
    d.write(vec[i].data(), H5PT, hs, s);     // Write hyperslab to file
  }
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 vector<NDArray<2, GenericVec3<T>>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector<NDArray<2, GenericVec3<T>>> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, vector<NDArray<2, GenericVec3<T>>>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t fdims[] = {vec.size(), vec[0].nx(), vec[0].ny(), 3}; // Dimension sizes
  DataSpace s(4, fdims);                     // Specify file data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  const hsize_t hdims[] = {1, fdims[1], fdims[2], 3}; // Hyperslab dimensions
  DataSpace hs(4, hdims);                    // Specify hyperslab data space
  for (hsize_t i = 0; i < vec.size(); ++i) {
    hsize_t offsets[] = {i, 0, 0, 0};        // Specify hyperslab offsets
    s.selectHyperslab(H5S_SELECT_SET, hdims, offsets);
    d.write(vec[i].data(), H5PT, hs, s);     // Write hyperslab to file
  }
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

/*! \brief Write an HDF5 vector<NDArray<3, T>> DataSet.
 *
 *  \param[in] f HDF5 File or Group object.
 *  \param[in] name Name of the HDF5 DataSet.
 *  \param[in] vec Reference to the vector<Cube> data.
 *  \param[in] units DataSet units name (optional).
 */
template <typename File_or_Group, typename T>
void write(const File_or_Group& f, const string& name, vector<NDArray<3, T>>& vec, const string& units = "") {
  check_dataset(f, name);                    // Ensure dataset uniqueness
  const hsize_t fdims[] = {vec.size(), vec[0].nx(), vec[0].ny(), vec[0].nz()}; // Dimension sizes
  DataSpace s(4, fdims);                     // Specify file data space
  DataSet d = f.createDataSet(name, H5PT, s); // Access dataset
  const hsize_t hdims[] = {1, fdims[1], fdims[2], fdims[3]}; // Hyperslab dimensions
  DataSpace hs(4, hdims);                    // Specify hyperslab data space
  for (hsize_t i = 0; i < vec.size(); ++i) {
    hsize_t offsets[] = {i, 0, 0, 0};        // Specify hyperslab offsets
    s.selectHyperslab(H5S_SELECT_SET, hdims, offsets);
    d.write(vec[i].data(), H5PT, hs, s);     // Write hyperslab to file
  }
  if (!units.empty())
    write_units(d, units);                   // Write dataset units (optional)
}

#endif // IO_HDF5_INCLUDED
