/*!
 * \file    cosmology.cc
 *
 * \brief   General cosmology functions.
 *
 * \details This file contains code related to general cosmology calculations.
 */

#include "proto.h"
#include "Simulation.h"
#include <gsl/gsl_sf.h>

/*! \brief Calculate the luminosity distance [cm].
 *
 *  The luminosity distance is \f$d_\text{L} = (1+z) \int_0^z c\,\text{d}z' / H(z')\f$
 *  and is caluclated by trapezoid integration with 1000 redshift bins.
 *
 *  \param[in] z Cosmological redshift.
 *
 *  \return Luminosity distance [cm].
 */
double Simulation::calculate_d_L(const double z) {
  // Recall: d_L = (1+z) Integrate[c/H(zp), {zp, 0, z}]
  const double OmegaLambda = 1. - Omega0;    // Dark energy density
  const double H0 = 100. * h100 * km / Mpc;  // Hubble constant [1/s]
  const double R_H0 = c / H0, zp1 = 1. + z;  // Hubble radius [cm]
  const double a_beta = 1. / 6., b_beta = 1. / 3.;
  const double b_0 = gsl_sf_beta_inc(a_beta, b_beta, OmegaLambda); // Comoving distance integral chi(z=0)
  const double ul = OmegaLambda / (OmegaLambda + Omega0 * zp1*zp1*zp1); // Upper limit of integration for chi(z)
  const double b_z = gsl_sf_beta_inc(a_beta, b_beta, ul); // Comoving distance integral chi(z)
  const double betanorm = gsl_sf_beta(a_beta, b_beta); // Account for normalization of beta function
  return zp1 * R_H0 * (b_0 - b_z) * betanorm / (3. * pow(Omega0, b_beta) * pow(OmegaLambda, a_beta));
}
