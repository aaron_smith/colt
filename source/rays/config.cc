/******************
 * rays/config.cc *
 ******************

 * Rays module configuration.

*/

#include "../proto.h"
#include "Rays.h"
#include "../config.h" // Configuration functions

/* Rays module configuration. */
void Rays::module_config(YAML::Node& file) {
  if constexpr (!VORONOI)
    root_error("Rays module requires voronoi geometry");
  if constexpr (!HAVE_CGAL)
    root_error("Rays module requires CGAL for mesh connectivity");
#ifdef CGAL_LINKED_WITH_TBB
    root_error("Rays module should not be run with CGAL_LINKED_WITH_TBB");
#endif

  if (!cosmological)
    cosmological = true;                     // Require cosmological flag

  // Information about the cameras
  int n_grid = 0;                            // Uninitialized grid size
  if (file["n_xy"]) {
    load("n_xy", n_grid, ">0");              // Number of grid points
    n_cameras = n_grid * n_grid;             // Total number of cameras
    const double dx = 1. / double(n_grid);   // Grid spacing
    camera_directions.resize(n_cameras);     // Allocate camera directions
    ray_origins.resize(n_cameras);           // Allocate camera origins
    #pragma omp parallel for
    for (int i = 0; i < n_cameras; ++i) {
      const int ix = i / n_grid;             // Grid indices
      const int iy = i - n_grid * ix;
      camera_directions[i] = Vec3(0, 0, 1);  // Camera direction (z-axis)
      ray_origins[i] = Vec3(double(ix) * dx, double(iy) * dx, 0.); // Camera origin (x-y plane)
    }
    have_cameras = true;                     // Camera-based output
    multiple_origins = true;                 // Flag for camera controls
  }
  camera_config(file);                       // General camera setup
  if (n_cameras <= 0)
    root_error("The rays module requires at least one camera "
               "but none were specified in " + config_file);
  if (file["origins_bbox"]) {
    load("origins_bbox", ray_origins);       // List of camera origins
    multiple_origins = true;                 // Flag for camera controls
  } else if (file["origin_bbox"]) {
    load("origin_bbox", ray_origin);         // Camera origin
  } else if (n_grid <= 0) {
    fof_origin = true;                       // Read origin from fof file
  }
  if (multiple_origins) {                    // Check that the sizes match
    if ((int)ray_origins.size() != n_cameras)
      root_error("ray_origins.size() != n_cameras");
  } else {
    load("spherical_extraction", spherical_extraction); // Flag for spherical extraction
    if (spherical_extraction) {
      load("extraction_only", extraction_only); // Only perform the extraction step
      load("ray_trace_only", ray_trace_only); // Only perform the ray tracing step
    }
  }
  if (!fof_origin && (file["fof_base"] || file["select_subhalo"] || file["start_fvir"]))
    root_error("Not expecting fof_base, select_subhalo, or start_fvir when ray origins are specified.");

  // Information about the rays
  if (fof_origin) {
    load("fof_base", fof_base);              // Friends of friends base name
    if (halo_str.empty())
      halo_str = "0";                        // Select the first group/subhalo
    incompatible("black_hole_origin", "lyman_alpha_origin"); // Can only use one of these
    load("black_hole_origin", black_hole_origin); // Most massive black hole
    load("lyman_alpha_origin", lyman_alpha_origin); // Lyman-alpha catalog
  }
  if (fof_origin && file["start_fvir"]) {
    load("start_fvir", start_fvir, ">=0");   // Ray starting radius [f_vir]
  } else {
    load("start_cMpc", start_cMpc, ">=0");   // Ray starting radius [cMpc]
  }
  if (file["length_kms"]) {
    load("length_kms", length_kms, ">0");    // Ray length proxy [km/s]
  } else if (file["length_bbox"]) {
    load("length_bbox", length_bbox, ">0");  // Ray length [bbox]
  } else if (file["length_Mpc"]) {
    load("length_Mpc", length_Mpc, ">0");    // Ray length [Mpc]
  } else {
    load("length_cMpc", length_cMpc, ">0");  // Ray length [cMpc]
  }
  if (file["impact_kpc"]) {
    load("impact_kpc", r_max_kpc, ">0");     // Cylinder impact radius [kpc]
  } else if (file["cell_radius_factor"]) {
    load("cell_radius_factor", cell_radius_factor, ">=1"); // Cell radius factor
  }

  // Information about the fields
  load("output_dust", output_dust);          // Output dust metallicity
  load("output_metallicity", output_metallicity); // Output metallicity
  load("output_HI", output_HI);              // Output HI fraction
  load("output_HeI", output_HeI);            // Output HeI fraction
  load("output_HeII", output_HeII);          // Output HeII fraction
  load("output_neutral", output_neutral);    // Output neutral hydrogen abundance
  load("output_electrons", output_electrons); // Output electron abundance
  load("output_internal_energy", output_internal_energy); // Output internal energy
  load("output_SFR", output_SFR);            // Output star formation rate
  load("output_parallel_velocity", output_parallel_velocity); // Output parallel velocity
  load("output_velocities", output_velocities); // Output velocities
  load("output_acceleration", output_acceleration_rays); // Output acceleration
  load("output_radiation", output_radiation); // Output photon density
  load("output_magnetic_field", output_magnetic_field); // Output magnetic field
  load("output_metals", output_metals);      // Read metal species fractions
  if (output_metals)
    load("output_metals_as_densities", output_metals_as_densities); // Output metal species as densities
  ray_flags[Density] = true;                 // Activate density
  if (output_dust)
    ray_flags[DustMetallicity] = true;       // Activate dust metallicity
  if (output_metallicity)
    ray_flags[Metallicity] = true;           // Activate metallicity
  if (output_HI)
    ray_flags[HI_Fraction] = true;           // Activate HI fraction
  if (output_HeI)
    ray_flags[HeI_Fraction] = true;          // Activate HeI fraction
  if (output_HeII)
    ray_flags[HeII_Fraction] = true;         // Activate HeII fraction
  if (output_neutral)
    ray_flags[NeutralHydrogenAbundance] = true; // Activate neutral hydrogen abundance
  if (output_electrons)
    ray_flags[ElectronAbundance] = true;     // Activate electron abundance
  if (output_internal_energy)
    ray_flags[InternalEnergy] = true;        // Activate internal energy
  if (output_SFR)
    ray_flags[StarFormationRate] = true;     // Activate star formation rate
  if (output_parallel_velocity & !spherical_extraction)
    ray_flags[Velocity] = true;              // Activate parallel velocity
  if (output_parallel_velocity || output_velocities)
    vec_flags[Velocities] = true;            // Activate velocities
  if (output_acceleration_rays)
    vec_flags[Acceleration] = true;          // Activate acceleration
  if (output_radiation)
    vec_flags[PhotonDensity] = true;         // Activate photon density
  if (output_magnetic_field)
    vec_flags[MagneticField] = true;         // Activate magnetic field
}
