/****************
 * rays/rays.cc *
 ****************

 * Driver: Assign rays for extraction, etc.

*/

#include "../proto.h"
#include "Rays.h"
#include "../io_hdf5.h" // Module HDF5 read/write functions
#include "../timing.h" // Timing functionality

extern Timer rays_timer, extract_cylinders_timer, combine_cylinders_timer;
extern Timer cylinders_to_rays_timer, combine_rays_timer;
static int max_n_cells = 0;

void calculate_connectivity(const bool verbose_cgal = false); // Calculate Delaunay connections
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

/* Box centering routine to maintain positions within [-box_half, box_half]. */
void Rays::recenter_box(const Vec3 displacement, const int size) {
  // Recenter based on the displacement vector
  #pragma omp parallel for
  for (int i = 0; i < size; ++i)
    r[i] -= displacement;

  // Check the particle tiling within [-box_half, box_half]
  for (int j = 0; j < 3; ++j) {
    if (displacement[j] > 0.) {              // Wrap to the right if needed
      #pragma omp parallel for
      for (int i = 0; i < size; ++i)
        if (r[i][j] < -box_half)
          r[i][j] += box_size;
    } else {                                 // Wrap to the left if needed
      #pragma omp parallel for
      for (int i = 0; i < size; ++i)
        if (r[i][j] >= box_half)
          r[i][j] -= box_size;
    }
  }
}

/* Minimum tiling index required for a ray with a given angular cosine. */
inline int Rays::min_tile(const double k_i) {
  return (k_i > 0.) ? floor( (k_i * r_start - r_max + box_half) / box_size )
                    : floor( (k_i * r_end - r_max + box_half) / box_size );
}

/* Maximum tiling index required for a ray with a given angular cosine. */
inline int Rays::max_tile(const double k_i) {
  return (k_i > 0.) ? ceil( (k_i * r_end + r_max - box_half) / box_size )
                    : ceil( (k_i * r_start + r_max - box_half) / box_size );
}

/* Compute the maximum distance in the +z direction from (0,0,lz) include edge cells. */
static tuple<double, int> z_distance(const double lz, const int cell) {
  int next = -1;                             // Next cell index
  double kz;                                 // Face direction (+z)
  double l = positive_infinity, l_comp;      // Set path length to large value
  const double r2 = r[cell].dot();           // r^2 = x^2 + y^2 + z^2 for the cell
  double kdotp; // k * p = (r_2 - r_1) * (r_1 + r_2) / 2 = (|r_2|^2 - |r_1|^2) / 2

  // Loop through neighbors to find the closest face distance
  for (const auto& face : faces[cell]) {
    // Calculate the distance from the point to the adjacent face
    const auto neib = face.neighbor;         // Neighbor cell index
    kz = r[neib].z - r[cell].z;              // k = r_2 - r_1
    if (kz <= 0.)                            // Do not consider incoming rays
      continue;
    kdotp = 0.5 * (r[neib].dot() - r2);      // k * p = (|r_2|^2 - |r_1|^2) / 2
    l_comp = kdotp / kz - lz;                // l = (k*p - k*point) / (k*direction)

    // Check candidate cell  =>  minimal distance to face
    if (l_comp < l) {
      l = l_comp;                            // Set the new length
      next = neib;                           // Remember the cell index
    }
  }
  if (l < 0.)                                // Corner case (numerical errors)
    error("Ray segment resulted in a negative face distance");
  return make_tuple(l, next);                // Minimum face distance and neighbor
}

/* Extract 1D path segments along a ray in the +z direction. */
static void ray_segments(vector<int>& inds, vector<double>& segs, double l_path) {
  int next, cell = find_cell({0.}, 0);       // Next and current cell indices
  double l = 0., dl = 0.;                    // Path position and segment length
  while (true) {                             // Ray trace over the path length
    inds.push_back(cell);                    // Track the cell indices
    tie(dl, next) = z_distance(l, cell);     // Maximum propagation distance
    if (l_path <= dl) {                      // Stop within the current cell
      segs.push_back(l_path);                // Track final segment length
      return;                                // Finished ray tracing
    }
    segs.push_back(dl);                      // Track the segment lengths
    l += dl;                                 // Move to the new position
    l_path -= dl;                            // Update the remaining length
    cell = next;                             // Update the next cell index
  }
}

/* Extract 1D path segments along a ray in an arbitrary direction. */
static void ray_segments(vector<int>& inds, vector<double>& segs, double l_path, Vec3& direction, double l_start, int& start_cell) {
  Vec3 point = direction * l_start;          // Starting position
  int next, cell = find_cell(point, start_cell); // Next and current cell indices
  start_cell = cell;                         // Update starting cell index
  double dl = 0.;                            // Path position and segment length
  while (true) {                             // Ray trace over the path length
    inds.push_back(cell);                    // Track the cell indices
    tie(dl, next) = face_distance(point, direction, cell); // Maximum propagation distance
    if (l_path <= dl) {                      // Stop within the current cell
      segs.push_back(l_path);                // Track final segment length
      return;                                // Finished ray tracing
    }
    segs.push_back(dl);                      // Track the segment lengths
    point += direction * dl;                 // Move to the new position
    l_path -= dl;                            // Update the remaining length
    cell = next;                             // Update the next cell index
  }
}

/* Extract spherical region. */
void Rays::extract_sphere() {
  extract_cylinders_timer.start();

  if (root)
    cout << "\nExtracting spherical region:\n  Progress:   0%\b\b\b\b" << std::flush;

  // Set up particle selection mask (Note: bool masks are not thread safe)
  vector<int> mask;                          // Spherical selection mask
  mask.reserve(max_file_size);               // Allocate max space
  vector<int> i_buf;                         // Spherical selection indices
  vector<Vec3> r_buf;                        // Position buffer
  vector<float> f_buf;                       // Scalar field buffer
  vector<Vec3F> v_buf;                       // Vector field buffer
  vector<array<float,n_metals>> m_buf;       // Metal species buffer
  const double r_sph = r_end + r_max;        // Selection radius
  const double r2_sph = r_sph * r_sph;       // Selection radius^2
  int n_sphere_rank = 0, n_sphere_tot = 0, n_max_rank = 0, n_max_tot = 0;

  // Check all subfiles
  const int n_root = n_files * n_cameras;    // Total number of tasks on root
  int n_interval = (n_root < 200) ? 1 : n_root / 100; // Interval between updates
  int n_finished = 0;                        // Reset progress counter
  const string prename = output_dir + "/sphere" + snap_str + halo_str + ".";
  for (int file = 0; file < n_files; ++file) {
    const int size = file_sizes[file];
    read_data(file);                         // Read subfile data
    mask.resize(size);                       // Ensure mask is the proper size

    // Recenter the box so particles are in [-box_half, box_half]
    Vec3 prev_origin = {box_half, box_half, box_half};
    #pragma omp parallel for
    for (int i = 0; i < size; ++i)
      r[i] -= prev_origin;

    // Center the box on the selected halo (accounting for periodic values)
    recenter_box(ray_origin - prev_origin, size);

    // Select spherical region
    if (max_tile(1.) > 0)
      error("Spherical region should not be larger than the box!");
    #pragma omp parallel for
    for (int i = 0; i < size; ++i)
      mask[i] = 0;                           // Initialize mask as false

    #pragma omp parallel for
    for (int i = 0; i < size; ++i) {
      // Distance from radial center
      const double r2_i = r[i].dot();        // dot(r,r)
      if (r2_i > r2_sph)
        continue;                            // Not in spherical range
      for (int ray = 0; ray < n_cameras; ++ray) {
        // Distance from a line and a point  = || (r-p) - ((r-p) * k) k ||
        // Line = r + t*k, p = (0,0,0)  =>   = || r - (r*k) k ||
        const Vec3 k = camera_directions[ray]; // Ray direction
        const double r_dot_k = r[i].dot(k);  // dot(r,k)
        const double r2_impact = (r[i] - k * r_dot_k).dot(); // Impact distance^2
        if (r2_impact > r2_max)
          continue;                          // Not in cylinder radial range
        double r2_dist = r2_i - r2_impact;   // Parallel ray distance^2
        if (r_dot_k < 0.)
          r2_dist *= -1.;                    // Remember +/- ray distances
        if (r2_dist < r2_start_buffer || r2_dist > r2_end_buffer)
          continue;                          // Not in clyinder length range
        mask[i] = 1;                         // Remember selected particles
        break;                               // Only needs to be in one cylinder
      }
    }

    const int n_mask = omp_sum(mask);        // Number of selected particles
    const string sphere_filename = prename + to_string(first_file + file) + "." + output_ext;
    H5File f(sphere_filename, H5F_ACC_TRUNC);
    write(f, "NumMask", n_mask);             // Save n_mask

    if (n_mask > 0) {
      if (n_mask > n_max_rank)
        n_max_rank = n_mask;                 // Update the max mask size
      n_sphere_rank += n_mask;               // Update the total mask count
      i_buf.resize(n_mask);                  // Final selected indices
      r_buf.resize(n_mask);                  // Final selected positions
      int i_mask = 0;                        // Index for the buffers
      for (int i = 0; i < size; ++i)
        if (mask[i])
          i_buf[i_mask++] = i;               // Save list of mask indices
      if (i_mask != n_mask)
        error("Accounting error with the spherical mask buffers");
      #pragma omp parallel for
      for (int i = 0; i < n_mask; ++i)
        r_buf[i] = r[i_buf[i]];              // Fill position buffer


      // Copy remaining data
      f_buf.resize(n_mask);                  // Final selected scalar fields
      v_buf.resize(n_mask);                  // Final selected vector fields
      if (output_metals)
        m_buf.resize(n_mask);                // Final selected metal species
    }

    // Write datasets to the file
    write(f, "Coordinates", r_buf);
    for (int field = 0; field < n_read_fields; ++field) {
      if (ray_flags[field]) {
        const auto& fld = fields[field];     // RayField struct
        const auto& dat = fld.data;          // Scalar field data
        #pragma omp parallel for
        for (int i = 0; i < n_mask; ++i)
          f_buf[i] = dat[i_buf[i]];          // Fill scalar field buffer
        write(f, fld.name, f_buf);           // Save selected data
      }
    }
    if (output_parallel_velocity || output_velocities) {
      const auto& fld = vec_fields[Velocities]; // VecField struct
      const auto& dat = fld.data;            // Vector field data
      #pragma omp parallel for
      for (int i = 0; i < n_mask; ++i)
        v_buf[i] = dat[i_buf[i]];            // Fill vector field buffer
      write(f, fld.name, v_buf);             // Save selected data
    }
    for (int field = 1; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        const auto& fld = vec_fields[field]; // VecField struct
        const auto& dat = fld.data;          // Vector field data
        #pragma omp parallel for
        for (int i = 0; i < n_mask; ++i)
          v_buf[i] = dat[i_buf[i]];          // Fill vector field buffer
        write(f, fld.name, v_buf);           // Save selected data
      }
    }
    if (output_metals) {                     // Individual metal species
      const auto& dat = metals.data;         // Metal species data
      #pragma omp parallel for
      for (int i = 0; i < n_mask; ++i)
        for (int s = 0; s < n_metals; ++s)
          m_buf[i][s] = dat[i_buf[i]][s];    // Fill metal species buffer
      write(f, metals.name, m_buf);          // Save selected data
    }

    // Print completed progress
    if (root && (++n_finished % n_interval == 0))
      cout << std::setw(3) << (100 * n_finished) / n_root << "%\b\b\b\b" << std::flush;
  }
  if (root)
    cout << "100%" << endl;

  // Free spherical extraction memory
  r = vector<Vec3>();                        // Positions memory
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data = vector<float>();  // Scalar fields memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data = vector<Vec3F>(); // Vector fields memory
  if (output_metals)
    metals.data = vector<array<float,n_metals>>(); // Metal species memory

  extract_cylinders_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
  MPI_Allreduce(&n_sphere_rank, &n_sphere_tot, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
  MPI_Allreduce(&n_max_rank, &n_max_tot, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
  if (extraction_only) {
    if (root) {
      const string summary_filename = prename + "n." + output_ext;
      H5File sf(summary_filename, H5F_ACC_TRUNC);
      write(sf, "NumFiles", n_files_tot);    // Save n_files_tot
      write(sf, "NumPartTot", n_sphere_tot); // Save n_sphere_tot
      write(sf, "NumPartMax", n_max_tot);    // Save n_max_tot
    }
    return;                                  // Early exit
  }

  combine_cylinders_timer.start();

  double mem_size = 1.;                      // Positions memory
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      mem_size += 0.5;                       // Scalar fields memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      mem_size += 1.5;                       // Vector fields memory
  if (output_metals)
    mem_size += 0.5 * double(n_metals);      // Metal species memory
  mem_size *= double (n_sphere_tot) * 8. / 1024. / 1024. / 1024.; // GB of memory

  if (root)
    cout << "\nCombining sphere files ... [" << n_sphere_tot << " particles requires " << mem_size << " GB]" << endl;

  MPI_Barrier(MPI_COMM_WORLD);               // Sync before resizing buffers

  // Collect all masked data and set up data buffers
  if (n_sphere_tot > max_n_cells)
    max_n_cells = n_sphere_tot;              // Update the allocation size
  r.resize(n_sphere_tot);                    // Allow sufficient space
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data.resize(n_sphere_tot); // For all scalar fields too
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data.resize(n_sphere_tot); // For all vector fields too
  if (output_metals)
    metals.data.resize(n_sphere_tot);        // For metal species too
  r_buf.reserve(n_max_tot);                  // Allocate position buffer space
  f_buf.reserve(n_max_tot);                  // Allocate scalar field buffer space
  v_buf.reserve(n_max_tot);                  // Allocate vector field buffer space
  if (output_metals)
    m_buf.reserve(n_max_tot);                // Allocate metal species buffer space

  // Populate data from subfiles
  for (int file = 0, prog = 0; file < n_files_tot; ++file) {
    H5File f(prename + to_string(file) + "." + output_ext, H5F_ACC_RDONLY);
    int n_mask;                              // Current number of particles
    read(f, "NumMask", n_mask);              // Read local number of particles
    if (n_mask == 0)
      continue;                              // No particles to copy
    r_buf.resize(n_mask);                    // Resize to reflect file size
    f_buf.resize(n_mask);                    // Scalar field buffer too
    v_buf.resize(n_mask);                    // Vector field buffer too
    if (output_metals)
      m_buf.resize(n_mask);                  // Metal species buffer too
    read(f, "Coordinates", r_buf);           // Read position data
    #pragma omp parallel for
    for (int i = 0; i < n_mask; ++i)
      r[prog+i] = r_buf[i];                  // Fill position data from buffer
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        auto& fld = fields[field];           // RayField struct
        auto& dat = fld.data;                // Scalar field data
        read(f, fld.name, f_buf);            // Read scalar field data
        #pragma omp parallel for
        for (int i = 0; i < n_mask; ++i)
          dat[prog+i] = f_buf[i];            // Fill scalar field data from buffer
      }
    }
    for (int field = 0; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        auto& fld = vec_fields[field];       // VecField struct
        auto& dat = fld.data;                // Vector field data
        read(f, fld.name, v_buf);            // Read vector field data
        #pragma omp parallel for
        for (int i = 0; i < n_mask; ++i)
          dat[prog+i] = v_buf[i];            // Fill vector field data from buffer
      }
    }
    if (output_metals) {
      auto& dat = metals.data;               // Metal species data
      read(f, metals.name, m_buf);           // Read metal species data
      #pragma omp parallel for
      for (int i = 0; i < n_mask; ++i)
        for (int s = 0; s < n_metals; ++s)
          dat[prog+i][s] = m_buf[i][s];      // Fill metal species data from buffer
    }
    prog += n_mask;                          // Track cumulative progress
  }

  combine_cylinders_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition

  // Remove temporary subfiles
  for (int file = 0; file < n_files; ++file) {
    const string sphere_filename = prename + to_string(first_file + file) + "." + output_ext;
    remove(sphere_filename.c_str());
  }

  cylinders_to_rays_timer.start();

  if (root)
    cout << "\nRay-tracing calculations:\n  Progress:   0%\b\b\b\b" << std::flush;

  // Voronoi grid construction (sphere)
  n_cells = n_sphere_tot;                    // Imporant to define n_cells
  if constexpr (HAVE_CGAL)
    calculate_connectivity();                // Calculate Delaunay connections
  int start_cell = find_cell({0.}, 0);       // Common start guess for all rays

  // MPI ray assignments
  int n_rays = n_cameras / n_ranks;          // Equal assignment
  int first_ray = rank * n_rays;             // File start range
  const int remainder = n_cameras - n_rays * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_rays;                                // Assign remaining work
    first_ray += remainder - n_ranks + rank; // Correct ray range
  }
  const int last_ray = first_ray + n_rays;   // End of ray range

  // Perform ray-tracing
  const string ray_prename = output_dir + "/ray" + snap_str + halo_str + "_";
  n_interval = (n_rays < 200) ? 1 : n_rays / 100; // Interval between updates
  n_finished = 0;                            // Reset progress counter
  for (int ray = first_ray; ray < last_ray; ++ray) {
    // Ray trace for 1D path segments
    vector<int> indices;                     // Extracted path indices
    vector<double> segments;                 // Extracted path segments
    ray_segments(indices, segments, r_len, camera_directions[ray], r_start, start_cell); // Populate the indices and segments
    const int n_segs = indices.size();       // Number of path segments
    vector<float> f_buf;                     // Scalar field buffer
    vector<Vec3F> v_buf;                     // Vector field buffer
    vector<array<float,n_metals>> m_buf;     // Metal species buffer
    f_buf.resize(n_segs);                    // Allow scalar field data access
    v_buf.resize(n_segs);                    // Allow vector field data access
    if (output_metals)
      m_buf.resize(n_segs);                  // Allow metal species data access

    // Write ray files
    const string rfile = ray_prename + to_string(ray) + "." + output_ext;
    H5File rf(rfile, H5F_ACC_TRUNC);         // Open file
    write(rf, "NumSegments", n_segs);        // Write number of path segments
    write(rf, "RaySegments", segments);      // Write path lengths
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        const auto& fld = fields[field];     // RayField struct
        const auto& dat = fld.data;          // Scalar field data
        #pragma omp parallel for
        for (int i = 0; i < n_segs; ++i)
          f_buf[i] = dat[indices[i]];        // Fill scalar field buffer
        write(rf, fld.name, f_buf);          // Save selected data
      }
    }
    for (int field = 0; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        const auto& fld = vec_fields[field]; // VecField struct
        const auto& dat = fld.data;          // Vector field data
        #pragma omp parallel for
        for (int i = 0; i < n_segs; ++i)
          v_buf[i] = dat[indices[i]];        // Fill vector field buffer
        if (field == Velocities) {
          if (output_velocities)
            write(rf, fld.name, v_buf);      // Save selected data
          if (output_parallel_velocity) {
            const Vec3 k = camera_directions[ray]; // Ray direction
            #pragma omp parallel for
            for (int i = 0; i < n_segs; ++i) {
              const auto& v_i = v_buf[i];    // Velocity vector reference
              f_buf[i] = k.x*double(v_i.x) + k.y*double(v_i.y) + k.z*double(v_i.z);
            }
            write(rf, fields[Velocity].name, f_buf); // Save parellel velocity data
          }
        } else
          write(rf, fld.name, v_buf);        // Save selected data
      }
    }
    if (output_metals) {
      const auto& dat = metals.data;         // Metal species data
      #pragma omp parallel for
      for (int i = 0; i < n_segs; ++i)
        for (int s = 0; s < n_metals; ++s)
          m_buf[i][s] = dat[indices[i]][s];  // Fill metal species buffer
      write(rf, metals.name, m_buf);         // Save selected data
    }
    rf.close();                              // Close the hdf5 file

    // Print completed progress
    if (root && (++n_finished % n_interval == 0))
      cout << std::setw(3) << (100 * n_finished) / n_rays << "%\b\b\b\b" << std::flush;
  }
  if (root)
    cout << "100%" << endl;

  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data = vector<float>();  // Free scalar field memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data = vector<Vec3F>(); // Free vector field memory
  if (output_metals)
    metals.data = vector<array<float,n_metals>>(); // Free metal species memory
  if (output_parallel_velocity)
    ray_flags[Velocity] = true;              // Activate parallel velocity
  if (output_parallel_velocity && !output_velocities)
    vec_flags[Velocities] = false;           // Velocities no longer need copying

  cylinders_to_rays_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
}

/* Only perform the ray tracing step for the spherical region. */
void Rays::ray_trace_sphere() {
  combine_cylinders_timer.start();

  // Populate data from sphere file
  if (root)
    cout << "\nRay-tracing setup:" << endl;
  {
    const string sfile = output_dir + "/sphere" + snap_str + halo_str + "." + output_ext;
    H5File f(sfile, H5F_ACC_RDONLY);
    read(f, "NumPartTot", n_cells);          // Read number of particles
    read(f, "Coordinates", r);               // Read position data
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        auto& fld = fields[field];           // RayField struct
        auto& dat = fld.data;                // Scalar field data
        read(f, fld.name, dat);              // Read scalar field data
      }
    }
    for (int field = 0; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        auto& fld = vec_fields[field];       // VecField struct
        auto& dat = fld.data;                // Vector field data
        read(f, fld.name, dat);              // Read vector field data
      }
    }
    if (output_metals) {
      auto& dat = metals.data;               // Metal species data
      read(f, metals.name, dat);             // Read metal species data
    }
    MPI_Barrier(MPI_COMM_WORLD);             // Avoid read/write race condition
    if (root)
      remove(sfile.c_str());                 // Sphere file is no longer needed
  }

  // Voronoi grid construction (sphere)
  if constexpr (HAVE_CGAL)
    calculate_connectivity();                // Calculate Delaunay connections
  int start_cell = find_cell({0.}, 0);       // Common start guess for all rays

  // MPI ray assignments
  int n_rays = n_cameras / n_ranks;          // Equal assignment
  int first_ray = rank * n_rays;             // File start range
  const int remainder = n_cameras - n_rays * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_rays;                                // Assign remaining work
    first_ray += remainder - n_ranks + rank; // Correct ray range
  }
  const int last_ray = first_ray + n_rays;   // End of ray range

  combine_cylinders_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
  cylinders_to_rays_timer.start();

  // Perform ray-tracing
  if (root)
    cout << "\nRay-tracing calculations:\n  Progress:   0%\b\b\b\b" << std::flush;
  const string ray_prename = output_dir + "/ray" + snap_str + halo_str + "_";
  int n_interval = (n_rays < 200) ? 1 : n_rays / 100; // Interval between updates
  int n_finished = 0;                        // Reset progress counter
  for (int ray = first_ray; ray < last_ray; ++ray) {
    // Ray trace for 1D path segments
    vector<int> indices;                     // Extracted path indices
    vector<double> segments;                 // Extracted path segments
    ray_segments(indices, segments, r_len, camera_directions[ray], r_start, start_cell); // Populate the indices and segments
    const int n_segs = indices.size();       // Number of path segments
    vector<float> f_buf;                     // Scalar field buffer
    vector<Vec3F> v_buf;                     // Vector field buffer
    vector<array<float,n_metals>> m_buf;     // Metal species buffer
    f_buf.resize(n_segs);                    // Allow scalar field data access
    v_buf.resize(n_segs);                    // Allow vector field data access
    if (output_metals)
      m_buf.resize(n_segs);                  // Allow metal species data access

    // Write ray files
    const string rfile = ray_prename + to_string(ray) + "." + output_ext;
    H5File rf(rfile, H5F_ACC_TRUNC);         // Open file
    write(rf, "NumSegments", n_segs);        // Write number of path segments
    write(rf, "RaySegments", segments);      // Write path lengths
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        const auto& fld = fields[field];     // RayField struct
        const auto& dat = fld.data;          // Scalar field data
        #pragma omp parallel for
        for (int i = 0; i < n_segs; ++i)
          f_buf[i] = dat[indices[i]];        // Fill scalar field buffer
        write(rf, fld.name, f_buf);          // Save selected data
      }
    }
    for (int field = 0; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        const auto& fld = vec_fields[field]; // VecField struct
        const auto& dat = fld.data;          // Vector field data
        #pragma omp parallel for
        for (int i = 0; i < n_segs; ++i)
          v_buf[i] = dat[indices[i]];        // Fill vector field buffer
        if (field == Velocities) {
          if (output_velocities)
            write(rf, fld.name, v_buf);      // Save selected data
          if (output_parallel_velocity) {
            const Vec3 k = camera_directions[ray]; // Ray direction
            #pragma omp parallel for
            for (int i = 0; i < n_segs; ++i) {
              const auto& v_i = v_buf[i];    // Velocity vector reference
              f_buf[i] = k.x*double(v_i.x) + k.y*double(v_i.y) + k.z*double(v_i.z);
            }
            write(rf, fields[Velocity].name, f_buf); // Save parellel velocity data
          }
        } else
          write(rf, fld.name, v_buf);        // Save selected data
      }
    }
    if (output_metals) {
      const auto& dat = metals.data;         // Metal species data
      #pragma omp parallel for
      for (int i = 0; i < n_segs; ++i)
        for (int s = 0; s < n_metals; ++s)
          m_buf[i][s] = dat[indices[i]][s];  // Fill metal species buffer
      write(rf, metals.name, m_buf);         // Save selected data
    }
    rf.close();                              // Close the hdf5 file

    // Print completed progress
    if (root && (++n_finished % n_interval == 0))
      cout << std::setw(3) << (100 * n_finished) / n_rays << "%\b\b\b\b" << std::flush;
  }
  if (root)
    cout << "100%" << endl;

  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data = vector<float>();  // Free scalar field memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data = vector<Vec3F>(); // Free vector field memory
  if (output_metals)
    metals.data = vector<array<float,n_metals>>(); // Free metal species memory
  if (output_parallel_velocity)
    ray_flags[Velocity] = true;              // Activate parallel velocity
  if (output_parallel_velocity && !output_velocities)
    vec_flags[Velocities] = false;           // Velocities no longer need copying

  cylinders_to_rays_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
}

/* Extract all cylinder regions. */
void Rays::extract_cylinders() {
  extract_cylinders_timer.start();

  if (root)
    cout << "\nExtracting cylinder regions:\n  Progress:   0%\b\b\b\b" << std::flush;

  // Set up particle selection mask (Note: bool masks are not thread safe)
  vector<int> mask;                          // Cylinder selection mask
  vector<Vec3> r_mask;                       // Selected/rotated positions
  mask.reserve(max_file_size);               // Allocate max space
  r_mask.reserve(max_file_size);
  vector<int> i_buf;                         // Cylinder selection indices
  vector<Vec3> r_buf;                        // Position buffer
  vector<float> f_buf;                       // Scalar field buffer
  vector<Vec3F> v_buf;                       // Vector field buffer
  vector<array<float,n_metals>> m_buf;       // Metal species buffer
  vector<int> n_masks;                       // Save number for each ray
  n_masks.resize(n_cameras);                 // Always the same size

  // Check all subfiles
  const int n_root = n_files * n_cameras;    // Total number of tasks on root
  const int n_interval = (n_root < 200) ? 1 : n_root / 100; // Interval between updates
  int n_finished = 0;                        // Reset progress counter
  for (int file = 0; file < n_files; ++file) {
    const int size = file_sizes[file];
    const string cylinder_filename = output_dir + "/cylinder" + snap_str + halo_str + "."
                                   + to_string(first_file + file) + "." + output_ext;
    create_data_groups("cylinder", file);    // Set up cylinder file headers
    read_data(file);                         // Read subfile data
    mask.resize(size);                       // Ensure mask is the proper size
    r_mask.resize(size);

    // Recenter the box so particles are in [-box_half, box_half]
    Vec3 prev_origin = {box_half, box_half, box_half};
    #pragma omp parallel for
    for (int i = 0; i < size; ++i)
      r[i] -= prev_origin;

    // Center the box on the selected halo (accounting for periodic values)
    if (!multiple_origins)
      recenter_box(ray_origin - prev_origin, size);

    // Select cylindrical regions for each ray
    for (int ray = 0; ray < n_cameras; ++ray) {
      const Vec3 k = camera_directions[ray]; // Ray direction

      // Determine which tilings to check for this ray
      const int ix_min = min_tile(k.x), ix_max = max_tile(k.x);
      const int iy_min = min_tile(k.y), iy_max = max_tile(k.y);
      const int iz_min = min_tile(k.z), iz_max = max_tile(k.z);
      const int nx_tiles = ix_max - ix_min + 1;
      const int ny_tiles = iy_max - iy_min + 1;
      const int nz_tiles = iz_max - iz_min + 1;
      const int n2_tiles = ny_tiles * nz_tiles; // Save hyperslab size
      const int n_tiles = nx_tiles * n2_tiles; // Total number of tiles
      Vec3 tile_offset = 0.;                 // Tile displacement vector
      int n_mask_tot = 0;                    // Total mask count (over tiles)

      // Center the box on the selected halo (accounting for periodic values)
      if (multiple_origins) {
        recenter_box(ray_origins[ray] - prev_origin, size);
        prev_origin = ray_origins[ray];      // Update the previous centering
      }

      // Loop through all tiles with potential intersections
      for (int tile = 0; tile < n_tiles; ++tile) {
        const int ix0 = tile / n2_tiles;     // x index
        const int sub_tile = tile - ix0 * n2_tiles; // Hyperslab remainder
        const int iy0 = sub_tile / nz_tiles; // y index
        const int iz0 = sub_tile - iy0 * nz_tiles; // z index
        const int ix = ix_min + ix0, iy = iy_min + iy0, iz = iz_min + iz0;
        // TODO: Skip this tile if the cylinder does not intersect this offset box
        const bool nonzero_tile = ((ix != 0) || (iy != 0) || (iz != 0));
        if (nonzero_tile) {
          tile_offset = {double(ix)*box_size, double(iy)*box_size, double(iz)*box_size};
          #pragma omp parallel for
          for (int i = 0; i < size; ++i)
            r[i] += tile_offset;
        }

        #pragma omp parallel for
        for (int i = 0; i < size; ++i)
          mask[i] = 0;                       // Initialize mask as false

        #pragma omp parallel for
        for (int i = 0; i < size; ++i) {
          // Distance from a line and a point  = || (r-p) - ((r-p) * k) k ||
          // Line = r + t*k, p = (0,0,0)  =>   = || r - (r*k) k ||
          const double r_dot_k = r[i].dot(k); // dot(r,k)
          const double r2_impact = (r[i] - k * r_dot_k).dot(); // Impact distance^2
          if (r2_impact > r2_max)
            continue;                        // Not in cylinder radial range
          double r2_dist = r[i].dot() - r2_impact; // Parallel ray distance^2
          if (r_dot_k < 0.)
            r2_dist *= -1.;                  // Remember +/- ray distances
          if (r2_dist < r2_start_buffer || r2_dist > r2_end_buffer)
            continue;                        // Not in clyinder length range

          // Rotate the points to align with the z-axis (simplifies the bbox)
          // See: https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
          // The rotation matrix to get (kx,ky,kz) -> (0,0,1) leads to
          // (x',y',z') = (x - kx * CZ, y - ky * CZ, r_dot_k) where CZ = (r_dot_k + z) / (1 + kz)
          // Note: This is singular only when k = (0,0,-1)
          // Note: The points are shifted so the start of the the ray is at the origin
          const double CZ = (r_dot_k + r[i].z) / (1. + k.z);
          r_mask[i] = {r[i].x - k.x * CZ, r[i].y - k.y * CZ, r_dot_k - r_start};
          mask[i] = 1;                       // Remember selected particles
        }

        const int n_mask = omp_sum(mask);    // Number of selected particles
        if (n_mask > 0) {
          const int n_prev = n_mask_tot;     // Track the previous mask count
          n_mask_tot += n_mask;              // Update the total mask count
          i_buf.resize(n_mask_tot);          // Final selected indices
          r_buf.resize(n_mask_tot);          // Final selected positions
          int i_mask = n_prev;               // Index for the buffers
          for (int i = 0; i < size; ++i)
            if (mask[i])
              i_buf[i_mask++] = i;           // Save list of mask indices
          if (i_mask != n_mask_tot)
            error("Accounting error with the cylindrical mask buffers");
          #pragma omp parallel for
          for (int i = n_prev; i < n_mask_tot; ++i)
            r_buf[i] = r_mask[i_buf[i]];     // Fill position buffer
        }

        // Undo the tile position displacements
        if (nonzero_tile) {
          #pragma omp parallel for
          for (int i = 0; i < size; ++i)
            r[i] -= tile_offset;
        }
      }

      n_masks[ray] = n_mask_tot;             // Save the number of particles
      if (n_mask_tot > 0) {
        // Copy remaining data
        const auto& v_dat = vec_fields[Velocities].data; // Velocities data reference
        f_buf.resize(n_mask_tot);            // Final selected scalar fields
        v_buf.resize(n_mask_tot);            // Final selected vector fields
        if (output_metals)
          m_buf.resize(n_mask_tot);          // Final selected metal species
        if (output_parallel_velocity) {
          #pragma omp parallel for
          for (int i = 0; i < n_mask_tot; ++i) {
            const auto& v_i = v_dat[i_buf[i]]; // Velocity vector reference
            f_buf[i] = k.x*double(v_i.x) + k.y*double(v_i.y) + k.z*double(v_i.z);
          }
        }
        // Write datasets to the file
        const string ray_str = "/" + to_string(ray);
        H5File f(cylinder_filename, H5F_ACC_RDWR);
        write(f, "Coordinates"+ray_str, r_buf);
        if (output_parallel_velocity)
          write(f, "Velocity"+ray_str, f_buf); // Parallel velocity derived field
        for (int field = 0; field < n_read_fields; ++field) {
          if (ray_flags[field]) {
            const auto& fld = fields[field]; // RayField struct
            const auto& dat = fld.data;      // Scalar field data
            #pragma omp parallel for
            for (int i = 0; i < n_mask_tot; ++i)
              f_buf[i] = dat[i_buf[i]];      // Fill scalar field buffer
            write(f, fld.name+ray_str, f_buf); // Save selected data
          }
        }
        if (output_velocities) {
          const auto& fld = vec_fields[Velocities]; // VecField struct
          const auto& dat = fld.data;        // Vector field data
          #pragma omp parallel for
          for (int i = 0; i < n_mask_tot; ++i)
            v_buf[i] = dat[i_buf[i]];        // Fill vector field buffer
          write(f, fld.name+ray_str, v_buf); // Save selected data
        }
        for (int field = 1; field < n_vec_fields; ++field) {
          if (vec_flags[field]) {
            const auto& fld = vec_fields[field]; // VecField struct
            const auto& dat = fld.data;      // Vector field data
            #pragma omp parallel for
            for (int i = 0; i < n_mask_tot; ++i)
              v_buf[i] = dat[i_buf[i]];      // Fill vector field buffer
            write(f, fld.name+ray_str, v_buf); // Save selected data
          }
        }
        if (output_metals) {                 // Individual metal species
          const auto& dat = metals.data;     // Metal species data
          #pragma omp parallel for
          for (int i = 0; i < n_mask_tot; ++i)
            for (int s = 0; s < n_metals; ++s)
              m_buf[i][s] = dat[i_buf[i]][s]; // Fill metal species buffer
          write(f, metals.name+ray_str, m_buf); // Save selected data
        }
      }

      // Print completed progress
      if (root && (++n_finished % n_interval == 0))
        cout << std::setw(3) << (100 * n_finished) / n_root << "%\b\b\b\b" << std::flush;
    }

    // Save the number of particles selected for each ray
    H5File f(cylinder_filename, H5F_ACC_RDWR);
    write(f, "NumMask", n_masks);            // Save n_masks
  }
  if (root)
    cout << "100%" << endl;

  // Free cylinder extraction memory
  r = vector<Vec3>();                        // Positions memory
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data = vector<float>();  // Scalar fields memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data = vector<Vec3F>(); // Vector fields memory
  if (output_metals)
    metals.data = vector<array<float,n_metals>>(); // Metal species memory
  if (output_parallel_velocity && !output_velocities)
    vec_flags[Velocities] = false;           // Velocities no longer need copying

  extract_cylinders_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
}

/* Combine subfiles into cylinder files. */
void Rays::combine_cylinders() {
  combine_cylinders_timer.start();

  if (root)
    cout << "\nCombining cylinder files ..." << endl;

  // MPI ray assignments
  int n_rays = n_cameras / n_ranks;          // Equal assignment
  int first_ray = rank * n_rays;             // File start range
  const int remainder = n_cameras - n_rays * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_rays;                                // Assign remaining work
    first_ray += remainder - n_ranks + rank; // Correct ray range
  }
  const int last_ray = first_ray + n_rays;   // End of ray range

  // Reorganize the data into individual cylinder files
  vector<Vec3> r_buf;                        // Position buffer
  vector<float> f_buf;                       // Scalar field buffer
  vector<Vec3F> v_buf;                       // Vector field buffer
  vector<array<float,n_metals>> m_buf;       // Metal species buffer
  vector<int> n_masks;                       // Save number for each file
  n_masks.resize(n_files_tot);               // Always the same size
  const string prename = output_dir + "/cylinder" + snap_str + halo_str + ".";
  for (int ray = first_ray; ray < last_ray; ++ray) {
    const string ray_str = "/" + to_string(ray);
    // Count the total number of particles for each ray
    for (int file = 0; file < n_files_tot; ++file) {
      H5File f(prename + to_string(file) + "." + output_ext, H5F_ACC_RDONLY);
      read(f, "NumMask", n_masks[file], ray); // Read the number from each file
    }

    // Set up data buffers
    const int n_tot = omp_sum(n_masks);      // Total number of selected particles
    if (n_tot > max_n_cells)
      max_n_cells = n_tot;                   // Update the allocation size
    const int n_max = omp_max(n_masks);      // Maximum buffer size
    r.resize(n_tot);                         // Allow sufficient space
    for (int field = 0; field < n_copy_fields; ++field)
      if (ray_flags[field])
        fields[field].data.resize(n_tot);    // For all scalar fields too
    for (int field = 0; field < n_vec_fields; ++field)
      if (vec_flags[field])
        vec_fields[field].data.resize(n_tot); // For all vector fields too
    if (output_metals)
      metals.data.resize(n_tot);             // For metal species too
    r_buf.reserve(n_max);                    // Allocate position buffer space
    f_buf.reserve(n_max);                    // Allocate scalar field buffer space
    v_buf.reserve(n_max);                    // Allocate vector field buffer space
    if (output_metals)
      m_buf.reserve(n_max);                  // Allocate metal species buffer space

    // Populate data from subfiles
    for (int file = 0, prog = 0; file < n_files_tot; ++file) {
      const int n_mask = n_masks[file];      // Local number of particles
      if (n_mask == 0)
        continue;                            // No particles to copy
      r_buf.resize(n_mask);                  // Resize to reflect file size
      f_buf.resize(n_mask);                  // Scalar field buffer too
      v_buf.resize(n_mask);                  // Vector field buffer too
      if (output_metals)
        m_buf.resize(n_mask);                // Metal species buffer too
      H5File f(prename + to_string(file) + "." + output_ext, H5F_ACC_RDONLY);
      read(f, "Coordinates"+ray_str, r_buf); // Read position data
      #pragma omp parallel for
      for (int i = 0; i < n_mask; ++i)
        r[prog+i] = r_buf[i];                // Fill position data from buffer
      for (int field = 0; field < n_copy_fields; ++field) {
        if (ray_flags[field]) {
          auto& fld = fields[field];         // RayField struct
          auto& dat = fld.data;              // Scalar field data
          read(f, fld.name+ray_str, f_buf);  // Read scalar field data
          #pragma omp parallel for
          for (int i = 0; i < n_mask; ++i)
            dat[prog+i] = f_buf[i];          // Fill scalar field data from buffer
        }
      }
      for (int field = 0; field < n_vec_fields; ++field) {
        if (vec_flags[field]) {
          auto& fld = vec_fields[field];     // VecField struct
          auto& dat = fld.data;              // Vector field data
          read(f, fld.name+ray_str, v_buf);  // Read vector field data
          #pragma omp parallel for
          for (int i = 0; i < n_mask; ++i)
            dat[prog+i] = v_buf[i];          // Fill vector field data from buffer
        }
      }
      if (output_metals) {
        auto& dat = metals.data;             // Metal species data
        read(f, metals.name+ray_str, m_buf); // Read metal species data
        #pragma omp parallel for
        for (int i = 0; i < n_mask; ++i)
          for (int s = 0; s < n_metals; ++s)
            dat[prog+i][s] = m_buf[i][s];    // Fill metal species data from buffer
      }
      prog += n_mask;                        // Track cumulative progress
    }

    // Write the consolidated cylinder file
    write_cylinder(ray);                     // Header and datasets
  }
  r = vector<Vec3>();                        // Free memory
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data = vector<float>();  // Free memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data = vector<Vec3F>(); // Free memory
  if (output_metals)
    metals.data = vector<array<float,n_metals>>(); // Free memory

  combine_cylinders_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition

  // Remove temporary cylinder subfiles
  for (int file = 0; file < n_files; ++file) {
    const string cylinder_filename = output_dir + "/cylinder" + snap_str + halo_str + "."
                                   + to_string(first_file + file) + "." + output_ext;
    remove(cylinder_filename.c_str());
  }
}

/* Convert cylinders to rays. */
void Rays::cylinders_to_rays() {
  cylinders_to_rays_timer.start();

  if (root)
    cout << "\nConverting cylinders to rays:\n  Progress:   0%\b\b\b\b" << std::flush;

  // MPI ray assignments
  int n_rays = n_cameras / n_ranks;          // Equal assignment
  int first_ray = rank * n_rays;             // File start range
  const int remainder = n_cameras - n_rays * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_rays;                                // Assign remaining work
    first_ray += remainder - n_ranks + rank; // Correct ray range
  }
  const int last_ray = first_ray + n_rays;   // End of ray range

  // Generate the connections for each cylinder and perform ray tracing
  r.reserve(max_n_cells);                    // Allocate max space
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data.reserve(max_n_cells); // Copy scalar fields
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data.reserve(max_n_cells); // Copy vector fields
  if (output_metals)
    metals.data.reserve(max_n_cells);        // Copy metal species
  const string cylinder_prename = output_dir + "/cylinder" + snap_str + halo_str + "_";
  const string ray_prename = output_dir + "/ray" + snap_str + halo_str + "_";
  const int n_interval = (n_rays < 200) ? 1 : n_rays / 100; // Interval between updates
  int n_finished = 0;                        // Reset progress counter
  for (int ray = first_ray; ray < last_ray; ++ray) {
    // Read cylinder files
    const string cfile = cylinder_prename + to_string(ray) + "." + output_ext;
    H5File cf(cfile, H5F_ACC_RDONLY);        // Open file
    n_cells = read(cf, "Coordinates", r);    // Read mesh generating points
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        auto& fld = fields[field];           // RayField struct
        read(cf, fld.name, fld.data, n_cells); // Read scalar field data
      }
    }
    for (int field = 0; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        auto& fld = vec_fields[field];       // VecField struct
        read(cf, fld.name, fld.data, n_cells); // Read vector field data
      }
    }
    if (output_metals)
      read(cf, metals.name, metals.data, n_cells); // Read metal species data
    cf.close();                              // Close the hdf5 file

    // Voronoi grid construction
    if constexpr (HAVE_CGAL)
      calculate_connectivity();              // Calculate Delaunay connections

    // Ray trace for 1D path segments
    vector<int> indices;                     // Extracted path indices
    vector<double> segments;                 // Extracted path segments
    ray_segments(indices, segments, r_len);  // Populate the indices and segments
    const int n_segs = indices.size();       // Number of path segments
    vector<float> f_buf;                     // Scalar field buffer
    vector<Vec3F> v_buf;                     // Vector field buffer
    vector<array<float,n_metals>> m_buf;     // Metal species buffer
    f_buf.resize(n_segs);                    // Allow scalar field data access
    v_buf.resize(n_segs);                    // Allow vector field data access
    if (output_metals)
      m_buf.resize(n_segs);                  // Allow metal species data access

    // Write ray files
    const string rfile = ray_prename + to_string(ray) + "." + output_ext;
    H5File rf(rfile, H5F_ACC_TRUNC);         // Open file
    write(rf, "NumSegments", n_segs);        // Write number of path segments
    write(rf, "RaySegments", segments);      // Write path lengths
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        const auto& fld = fields[field];     // RayField struct
        const auto& dat = fld.data;          // Scalar field data
        #pragma omp parallel for
        for (int i = 0; i < n_segs; ++i)
          f_buf[i] = dat[indices[i]];        // Fill scalar field buffer
        write(rf, fld.name, f_buf);          // Save selected data
      }
    }
    for (int field = 0; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        const auto& fld = vec_fields[field]; // VecField struct
        const auto& dat = fld.data;          // Vector field data
        #pragma omp parallel for
        for (int i = 0; i < n_segs; ++i)
          v_buf[i] = dat[indices[i]];        // Fill vector field buffer
        write(rf, fld.name, v_buf);          // Save selected data
      }
    }
    if (output_metals) {
      const auto& dat = metals.data;         // Metal species data
      #pragma omp parallel for
      for (int i = 0; i < n_segs; ++i)
        for (int s = 0; s < n_metals; ++s)
          m_buf[i][s] = dat[indices[i]][s];  // Fill metal species buffer
      write(rf, metals.name, m_buf);         // Save selected data
    }
    rf.close();                              // Close the hdf5 file
    remove(cfile.c_str());                   // Remove the old cylinder file

    // Print completed progress
    if (root && (++n_finished % n_interval == 0))
      cout << std::setw(3) << (100 * n_finished) / n_rays << "%\b\b\b\b" << std::flush;
  }
  if (root)
    cout << "100%" << endl;

  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data = vector<float>();  // Free scalar field memory
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data = vector<Vec3F>(); // Free vector field memory
  if (output_metals)
    metals.data = vector<array<float,n_metals>>(); // Free metal species memory

  cylinders_to_rays_timer.stop();
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
}

/* Combine all ray files into a single ray file. */
void Rays::combine_rays() {
  combine_rays_timer.start();

  if (root) {
    cout << "\nCombining ray files ..." << endl;

    // Create data groups for the final file
    const string filename = output_dir + "/" + output_base + snap_str + halo_str + "." + output_ext;
    create_rays_file(filename);

    // Reorganize the data into a single ray file
    vector<int> n_segs;                      // Save size for each file
    n_segs.resize(n_cameras);                // Always the same size
    const string ray_prename = output_dir + "/ray" + snap_str + halo_str + "_";
    for (int ray = 0; ray < n_cameras; ++ray) {
      const string rfile = ray_prename + to_string(ray) + "." + output_ext;
      H5File f(rfile, H5F_ACC_RDONLY);       // Open file
      read(f, "NumSegments", n_segs[ray]);   // Read the number from each file
    }

    // Set up data buffers
    const int n_max = omp_max(n_segs);       // Maximum buffer size
    vector<double> segments;                 // Segments buffer
    segments.reserve(n_max);                 // Allow sufficient space
    for (int field = 0; field < n_copy_fields; ++field)
      if (ray_flags[field])
        fields[field].data.reserve(n_max);   // For all scalar fields too
    for (int field = 0; field < n_vec_fields; ++field)
      if (vec_flags[field])
        vec_fields[field].data.reserve(n_max); // For all vector fields too
    if (output_metals)
      metals.data.reserve(n_max);            // For metal species too

    // Copy the data into the consolidated file
    for (int ray = 0; ray < n_cameras; ++ray) {
      const int n_seg = n_segs[ray];         // Local number of ray segments
      const string rfile = ray_prename + to_string(ray) + "." + output_ext;
      H5File rf(rfile, H5F_ACC_RDONLY);      // Open file
      read(rf, "RaySegments", segments, n_seg); // Read segments data
      for (int field = 0; field < n_copy_fields; ++field) {
        if (ray_flags[field]) {
          auto& fld = fields[field];         // RayField struct
          read(rf, fld.name, fld.data, n_seg); // For all scalar fields too
        }
      }
      for (int field = 0; field < n_vec_fields; ++field) {
        if (vec_flags[field]) {
          auto& fld = vec_fields[field];     // VecField struct
          read(rf, fld.name, fld.data, n_seg); // For all vector fields too
        }
      }
      if (output_metals)
        read(rf, metals.name, metals.data, n_seg); // For metal species too
      rf.close();                            // Close the hdf5 file

      const string ray_str = "/" + to_string(ray);
      H5File f(filename, H5F_ACC_RDWR);      // Open file
      write(f, "RaySegments"+ray_str, segments); // Write segments data
      for (int field = 0; field < n_copy_fields; ++field) {
        if (ray_flags[field]) {
          auto& fld = fields[field];         // RayField struct
          write(f, fld.name+ray_str, fld.data); // For all scalar fields too
        }
      }
      for (int field = 0; field < n_vec_fields; ++field) {
        if (vec_flags[field]) {
          auto& fld = vec_fields[field];     // VecField struct
          write(f, fld.name+ray_str, fld.data); // For all vector fields too
        }
      }
      if (output_metals) {
        if (output_metals_as_densities) {    // Convert fractions to densities
          auto& mets = metals.data;          // Local references
          const auto& dens_fld = fields[Density];
          const auto& dens = dens_fld.data;
          #pragma omp parallel for
          for (int i = 0; i < n_seg; ++i) {
            const auto dens_i = dens[i];
            for (int s = 0; s < n_metals; ++s)
              mets[i][s] *= dens_i;
          }
        }
        write(f, metals.name+ray_str, metals.data); // For metal species too
      }
    }

    for (int field = 0; field < n_copy_fields; ++field)
      if (ray_flags[field])
        fields[field].data = vector<float>(); // Free scalar field memory
    for (int field = 0; field < n_vec_fields; ++field)
      if (vec_flags[field])
        vec_fields[field].data = vector<Vec3F>(); // Free vector field memory
    if (output_metals)
      metals.data = vector<array<float,n_metals>>(); // Free metal species memory

    // Remove temporary ray files
    for (int ray = 0; ray < n_cameras; ++ray) {
      const string rfile = ray_prename + to_string(ray) + "." + output_ext;
      remove(rfile.c_str());
    }
  }

  combine_rays_timer.stop();
}

/* Driver for rays module. */
void Rays::run() {
  rays_timer.start();

  if (spherical_extraction) {
    if (ray_trace_only)
      ray_trace_sphere();                    // Only perform the ray tracing step
    else
      extract_sphere();                      // Extract rays from spherical region
  } else {
    extract_cylinders();                     // Extract all cylinder regions
    combine_cylinders();                     // Combine cylinder files
    cylinders_to_rays();                     // Convert cylinders to rays
  }
  if (!extraction_only)
    combine_rays();                          // Combine ray files

  rays_timer.stop();
}
