/**********************
 * rays/initialize.cc *
 **********************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "Rays.h"

/* Setup everything for the rays module. */
void Rays::setup() {
  if (have_cameras)
    setup_cameras();                         // General camera setup

  // Convert from normalized to code positions
  if (multiple_origins) {
    #pragma omp parallel for
    for (int ray = 0; ray < n_cameras; ++ray) {
      Vec3& origin = ray_origins[ray];
      if (origin.x < 0. || origin.x > 1. || origin.y < 0. || origin.y > 1. || origin.z < 0. || origin.z > 1.)
        error("ray_origin[" + to_string(ray) + "] is out of the normalized range [0,1]: " + to_string(origin));
      origin *= box_size;                    // Origin in code units [a*length/h]
    }
  } else if (!fof_origin) {
    ray_origin *= box_size;                  // Origin in code units [a*length/h]
  }
}
