/**************
 * rays/io.cc *
 **************

 * All I/O operations related to the rays module.

*/

#include "../proto.h"
#include "Rays.h"
#include "../io_hdf5.h" // Module HDF5 read/write functions
#include "../timing.h" // Timing functionality

extern Timer read_timer;
extern Timer write_timer;

double calculate_d_L(const double z); // Luminosity distance [cm]
void write_sim_info(const H5File& f); // Write general simulation information

/* Function to print additional rays information. */
void Rays::print_info() {
  cout << "\nCamera parameters:"
       << "\n  n_cameras  = " << n_cameras;
  if (n_side > 0)
    cout << "\n  n_side     = " << n_side << " (Note: Healpix in RING ordering)";
  cout << endl;
  if (verbose)
    print("directions", camera_directions);
}

/* Read top level information. */
void Rays::read_info() {
  // Setup file naming scheme
  pre_filename = init_dir + (init_dir.empty() ? "" : "/") + "snapdir" +
                 snap_str + "/" + init_base + snap_str + ".";
  const string filename = sub_file(0);       // Read from the first file

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize cosmology information
  Group g = f.openGroup("Header");           // Info is in the Header group
  Group pg = f.openGroup("Parameters");      // Info is in the Parameters group
  read(g, "Redshift", z);                    // Current simulation redshift
  read(pg, "HubbleParam", h100);             // Hubble constant [100 km/s/Mpc]
  read(pg, "Omega0", Omega0);                // Matter density [rho_crit_0]
  read(pg, "OmegaBaryon", OmegaB);           // Baryon density [rho_crit_0]
  d_L = calculate_d_L(z);                    // Luminosity distance [cm]

  // Units: length, mass, and velocity
  read(pg, "UnitLength_in_cm", unit_length); // Unit length [cm]
  read(pg, "UnitMass_in_g", unit_mass);      // Unit mass [g]
  read(pg, "UnitVelocity_in_cm_per_s", unit_velocity); // Unit velocity [cm/s]

  // Initialize ray information
  read(g, "NumFilesPerSnapshot", n_files_tot); // Number of files per snapshot
  if (n_files_tot < n_ranks)
    root_error("Cannot have more MPI ranks than subfiles: " + to_string(n_files_tot));
  read(g, "BoxSize", box_size);              // Simulation box size [a*length/h]
  box_half = box_size / 2.;                  // Half box size [a*length/h]

  // File assignments
  n_files = n_files_tot / n_ranks;           // Equal assignment
  first_file = rank * n_files;               // File start range
  const int remainder = n_files_tot - n_files * n_ranks;
  if (n_ranks - rank <= remainder) {
    ++n_files;                               // Assign remaining work
    first_file += remainder - n_ranks + rank; // Correct file range
  }
}

/* Read file offsets information. */
void Rays::read_file_offsets_info() {
  // Treat halo 0 as a special case
  if (halo_num == 0) {
    FileOffsetsGroup = vector<long long>(n_files_tot, 1);
    FileOffsetsSubhalo = vector<long long>(n_files_tot, 1);
    FileOffsetsGroup[0] = 0;                 // Read from the first file
    FileOffsetsSubhalo[0] = 0;               // Treat remaining files as empty
    return;
  }

  // Setup offsets file naming scheme
  const string filename = init_dir + (init_dir.empty() ? "" : "/")
    + "../postprocessing/offsets/offsets" + snap_str + ".hdf5";

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize halo information
  Group g = f.openGroup("FileOffsets");      // File offsets
  read(g, "Group", FileOffsetsGroup);        // File group offsets
  read(g, "Subhalo", FileOffsetsSubhalo);    // File subhalo offsets
}

/* Read particle offsets information. */
void Rays::read_offsets_info(const int s0) {
  // Treat halo 0 as a special case
  if (halo_num == 0) {
    SubhaloOffsetsType.fill(0);
    FileOffsetsType.resize(n_files_tot);
    FileOffsetsType[0] = array<long long,6>{0}; // Read from the first file
    for (int i = 1; i < n_files_tot; ++i)
      FileOffsetsType[i] = array<long long,6>{1,1,1,1,1,1};
    return;
  }

  // Setup offsets file naming scheme
  const string filename = init_dir + (init_dir.empty() ? "" : "/")
    + "../postprocessing/offsets/offsets" + snap_str + ".hdf5";

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize halo information
  Group s = f.openGroup("Subhalo");          // Subhalo
  read(s, "SnapByType", SubhaloOffsetsType, s0); // Subhalo particle offsets by type

  Group g = f.openGroup("FileOffsets");      // File offsets
  read(g, "SnapByType", FileOffsetsType);    // File particle offsets by type
}

/* Read information about the most massive black hole of the subhalo. */
void Rays::read_max_black_hole_info() {
  // Set the black hole offset and count
  read_offsets_info(SubhaloID);
  const long long n_bh = (halo_num == 0) ? 1 : SubhaloLenType[5];
  const long long bh_offset = SubhaloOffsetsType[5];
  const long long bh_last = bh_offset + n_bh - 1;
  if (n_bh <= 0)
    root_error("There are no black holes in the selected subhalo!");

  // Check the min/max file range
  int bh_file;
  for (bh_file = 0; bh_file < n_files_tot; ++bh_file) {
    if (FileOffsetsType[bh_file][5] > bh_offset) {
      --bh_file;                             // Undo last check
      break;                                 // Completed search
    }
  }
  int bh_last_file;
  for (bh_last_file = bh_file; bh_last_file < n_files_tot; ++bh_last_file) {
    if (FileOffsetsType[bh_last_file][5] > bh_last) {
      --bh_last_file;                        // Undo last check
      break;                                 // Completed search
    }
  }
  if (bh_last_file > bh_file)
    error("Subhalo black holes spread over multiple files is not allowed");
  const long long bh_local = bh_offset - FileOffsetsType[bh_file][5];
  FileOffsetsType = vector<array<long long,6>>(); // Free memory

  // Setup fof file naming scheme
  pre_fof_name = init_dir + (init_dir.empty() ? "" : "/") + "snapdir" +
                 snap_str + "/" + init_base + snap_str + ".";
  const string filename = fof_sub_file(bh_file); // Read from the first file

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Initialize halo information
  Group p5 = f.openGroup("PartType5");       // Black hole particle data
  float max_BH_Mass = 0.;                    // Most massive black hole mass
  long long i_bh_max = 0;                    // Index of most massive black hole
  for (long long i_bh = 0; i_bh < n_bh; ++i_bh) {
    float BH_Mass;
    read(p5, "BH_Mass", BH_Mass, bh_local+i_bh); // Black hole masses
    if (BH_Mass > max_BH_Mass) {
      i_bh_max = i_bh;
      max_BH_Mass = BH_Mass;                 // Update the mass and index
    }
  }

  // Set the ray origin to the black hole position
  read(p5, "Coordinates", ray_origin, bh_local+i_bh_max); // [a*length/h]
  read(p5, "Velocities", ray_systemic, bh_local+i_bh_max); // [sqrt(a)*velocity]
  ray_systemic /= sqrt(1. + z);              // Convert to km/s
}

/* Read information from the Lyman-alpha subhalo catalog. */
void Rays::read_lyman_alpha_info() {
  // Setup Lya file naming scheme
  const string filename = init_dir + (init_dir.empty() ? "" : "/")
    + "../postprocessing/Lya/Lya" + snap_str + ".hdf5";

  // Open file and read info
  H5File f(filename, H5F_ACC_RDONLY);

  // Set the ray origin to the Lyman-alpha center of luminosity
  Group s = f.openGroup("Subhalo");          // Subhalo
  read(s, "LyaPos", ray_origin, SubhaloID);  // [a*length/h]
  read(s, "LyaVel", ray_systemic, SubhaloID); // [km/s]
}

/* Read top level halo information. */
void Rays::read_fof_info() {
  // Setup fof file naming scheme
  pre_fof_name = init_dir + (init_dir.empty() ? "" : "/") + "groups" +
                 snap_str + "/" + fof_base + snap_str + ".";
  // Determine which files to read
  int s_file, g_file;
  read_file_offsets_info();                  // Populate file offsets
  if (select_subhalo) {
    SubhaloID = halo_num;                    // Specified by Subhalo ID
    for (s_file = 0; s_file < n_files_tot; ++s_file) {
      if (int(FileOffsetsSubhalo[s_file]) > SubhaloID) {
        --s_file;                            // Undo last check
        break;                               // Completed search
      }
    }
    const string filename = fof_sub_file(s_file); // Read from the file
    H5File f(filename, H5F_ACC_RDONLY);      // Open file and read info
    // Group header = f.openGroup("Header"); // Counts are in the Header group
    // long long Nsubgroups_Total;           // Note: Arepo changed to Nsubhalos_Total
    // read(header, "Nsubgroups_Total", Nsubgroups_Total); // Total number of subhalos
    // if (int(Nsubgroups_Total) <= SubhaloID)
    //   error("Requested an out of range subhalo ID");
    const int s0 = SubhaloID - int(FileOffsetsSubhalo[s_file]); // Subhalo ID (local)
    Group s = f.openGroup("Subhalo");        // FoF subhalo
    if (exists(f, "SubhaloGrNr"))
      read(s, "SubhaloGrNr", GroupID, s0);   // Read the Group ID
    else
      read(s, "SubhaloGroupNr", GroupID, s0); // Read the Group ID
    for (g_file = 0; g_file < n_files_tot; ++g_file) {
      if (int(FileOffsetsGroup[g_file]) > GroupID) {
        --g_file;                            // Undo last check
        break;                               // Completed search
      }
    }
  } else {
    GroupID = halo_num;                      // Specified by Group ID
    for (g_file = 0; g_file < n_files_tot; ++g_file) {
      if (int(FileOffsetsGroup[g_file]) > GroupID) {
        --g_file;                            // Undo last check
        break;                               // Completed search
      }
    }
    const string filename = fof_sub_file(g_file); // Read from the file
    H5File f(filename, H5F_ACC_RDONLY);      // Open file and read info
    // Group header = f.openGroup("Header"); // Counts are in the Header group
    // long long Ngroups_Total;
    // read(header, "Ngroups_Total", Ngroups_Total); // Total number of groups
    // if (int(Ngroups_Total) <= GroupID)
    //   error("Requested an out of range group ID");
    const int g0 = GroupID - int(FileOffsetsGroup[g_file]); // Group ID (local)
    Group g = f.openGroup("Group");          // FoF group
    read(g, "GroupFirstSub", SubhaloID, g0); // Read the Subhalo ID
    for (s_file = 0; s_file < n_files_tot; ++s_file) {
      if (int(FileOffsetsSubhalo[s_file]) > SubhaloID) {
        --s_file;                            // Undo last check
        break;                               // Completed search
      }
    }
  }
  const int s0 = SubhaloID - int(FileOffsetsSubhalo[s_file]); // Subhalo ID (local)
  const int g0 = GroupID - int(FileOffsetsGroup[g_file]); // Group ID (local)
  FileOffsetsSubhalo = vector<long long>();  // Free memory
  FileOffsetsGroup = vector<long long>();

  // Open group file and read info
  {
    const string filename = fof_sub_file(g_file); // Read from the group file
    H5File f(filename, H5F_ACC_RDONLY);
    Group g = f.openGroup("Group");          // FoF group
    read(g, "Group_R_Crit200", R_Crit200, g0); // Halo virial radius [a*length/h]
  }

  // Open subhalo file and read info
  {
    const string filename = fof_sub_file(s_file); // Read from the subhalo file
    H5File f(filename, H5F_ACC_RDONLY);
    Group s = f.openGroup("Subhalo");        // FoF subhalo
    read(s, "SubhaloMass", M_halo, s0);      // Halo virial mass [mass/h]
    read(s, "SubhaloPos", r_halo, s0);       // Halo position [a*length/h]
    read(s, "SubhaloCM", r_com, s0);         // Halo center of mass [a*length/h]
    read(s, "SubhaloVel", v_halo, s0);       // Halo velocity [km/s]
    read(s, "SubhaloMassInRadType", SubhaloMassInRadType, s0); // Subhalo mass in radius [mass/h]
    read(s, "SubhaloMassType", SubhaloMassType, s0); // Subhalo mass in halo [mass/h]
    read(s, "SubhaloLenType", SubhaloLenType, s0); // Subhalo particle counts by type
  }

  // Determine ray origin based on halo config options
  if (black_hole_origin)
    read_max_black_hole_info();              // Set by most massive black hole
  else if (lyman_alpha_origin)
    read_lyman_alpha_info();                 // Set by Lyman-alpha catalog
  else {
    ray_origin = r_halo;                     // Default to halo center [a*length/h]
    ray_systemic = v_halo;                   // Default to halo velocity [km/s]
  }
}

/* Read subfile dataset sizes. */
static inline int get_file_sizes(const string filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  return get_size(f, "PartType0/Density");   // Infer dataset size
}

/* Allocate space for each file. */
void Rays::setup_dataspace() {
  file_sizes.resize(n_files);                // Allocate space for each file
  max_file_size = 0;                         // Largest assigned file size
  for (int i = 0; i < n_files; ++i) {
    file_sizes[i] = get_file_sizes(sub_file(i)); // Infer the dataset sizes
    if (file_sizes[i] > max_file_size)
      max_file_size = file_sizes[i];         // Update the largest file size
  }
}

/* Initialize active fields. Units(a, h, length, mass, velocity, to_cgs) */
void Rays::setup_fields() {
  r.reserve(max_file_size);                  // Allocate max space
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      fields[field].data.reserve(max_file_size); // Scalar fields
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      vec_fields[field].data.reserve(max_file_size); // Vector fields
  if (output_metals)
    metals.data.reserve(max_file_size);      // Metals

  const double dimensionless = 0.;
  const double unit_volume = unit_length * unit_length * unit_length; // L^3
  const double unit_density = unit_mass / unit_volume; // M / L^3
  const double unit_velocity2 = unit_velocity * unit_velocity; // V^2
  const double unit_time = unit_length / unit_velocity; // L / V
  const double unit_SFR = unit_mass / unit_time; // M V / L
  const double unit_acceleration = unit_velocity2 / unit_length; // V^2 / L
  const double unit_magnetic = sqrt(unit_mass / unit_length) / unit_time; // M^1/2 V / L^3/2

  // Scalar fields
  fields[Density].name = "Density";          // Gas density [h^2/a^3 * M/L^3]
  fields[Density].units = Units(-3., 2., -3., 1., 0., unit_density);

  fields[DustMetallicity].name = "GFM_DustMetallicity"; // Dust-to-gas ratio
  fields[DustMetallicity].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[Metallicity].name = "GFM_Metallicity";  // Gas metallicity
  fields[Metallicity].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[HI_Fraction].name = "HI_Fraction";  // HI fraction
  fields[HI_Fraction].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[HeI_Fraction].name = "HeI_Fraction"; // HeI fraction
  fields[HeI_Fraction].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[HeII_Fraction].name = "HeII_Fraction"; // HeII fraction
  fields[HeII_Fraction].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[NeutralHydrogenAbundance].name = "NeutralHydrogenAbundance"; // Neutral hydrogen abundance
  fields[NeutralHydrogenAbundance].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[ElectronAbundance].name = "ElectronAbundance"; // Electron abundance
  fields[ElectronAbundance].units = Units(0., 0., 0., 0., 0., dimensionless);

  fields[InternalEnergy].name = "InternalEnergy"; // Internal energy [V^2]
  fields[InternalEnergy].units = Units(0., 0., 0., 0., 2., unit_velocity2);

  fields[StarFormationRate].name = "StarFormationRate"; // Star formation rate [Msun/yr]
  fields[StarFormationRate].units = Units(0., 0., -1., 1., 1., unit_SFR);

  fields[Velocity].name = "Velocity";        // Parallel velocity [sqrt(a) * V]
  fields[Velocity].units = Units(0.5, 0., 0., 0., 1., unit_velocity);

  fields[Masses].name = "Masses";            // Gas mass [M/h]
  fields[Masses].units = Units(0., -1., 0., 1., 0., unit_mass);

  // Vector fields
  vec_fields[Velocities].name = "Velocities"; // Velocities [sqrt(a) * V]
  vec_fields[Velocities].units = Units(0.5, 0., 0., 0., 1., unit_velocity);

  vec_fields[Acceleration].name = "Acceleration"; // Acceleration [h*V^2/a^2/L]
  vec_fields[Acceleration].units = Units(-2., 1., -1., 0., 2., unit_acceleration);

  vec_fields[PhotonDensity].name = "PhotonDensity"; // Photon density [1e63*h^3/a^3/L^3]
  vec_fields[PhotonDensity].units = Units(-3., 3., -3., 0., 0., 1./unit_volume);

  vec_fields[MagneticField].name = "MagneticField"; // Magnetic field [h/a^2 * M^1/2 V / L^3/2]
  vec_fields[MagneticField].units = Units(-2., 1., -1.5, 0.5, 1., unit_magnetic);

  // Metal species
  metals.name = "GFM_Metals";                // Metal species
  metals.units = Units(0., 0., 0., 0., 0., dimensionless);
}

static inline void verify_units(DataSet& d, const string& name, const double value) {
  if (attribute_exists(d, name)) {
    double attr;                             // Attribute value
    Attribute a = d.openAttribute(name);     // Access the attribute
    a.read(PredType::NATIVE_DOUBLE, &attr);  // Read attribute from file
    if (attr != value)
      error("Unexpected units for " + name);
  }
}

/* Verify the units of a specified dataset. */
static inline void verify_units(DataSet& d, Units& units) {
  verify_units(d, "a_scaling", units.a_scaling);
  verify_units(d, "h_scaling", units.h_scaling);
  verify_units(d, "length_scaling", units.length_scaling);
  verify_units(d, "mass_scaling", units.mass_scaling);
  verify_units(d, "velocity_scaling", units.velocity_scaling);
}

/* Read a file dataset. */
static inline void read(const string& name, vector<float>& data, Units& units,
                        const hsize_t size, const string& filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size});          // Ensure it has the expected shape
  d.read(data.data(), PredType::NATIVE_FLOAT); // Read dataset from file
  verify_units(d, units);                    // Verify it has the expected units
}

/* Read a file GenericVec3<T> dataset. */
template <typename T>
static inline void read(const string& name, vector<GenericVec3<T>>& data, Units& units,
                        const hsize_t size, const string& filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size, 3});       // Ensure it has the expected shape
  d.read(data.data(), H5PT);                 // Read dataset from file
  verify_units(d, units);                    // Verify it has the expected units
}

/* Read a file array<T,N> dataset. */
template <typename T, size_t N>
static inline void read(const string& name, vector<array<T, N>>& data, Units& units,
                        const hsize_t size, const string& filename) {
  H5File f(filename, H5F_ACC_RDONLY);        // Open file
  verify_field(f, name);                     // Ensure the dataset exists
  DataSet d = f.openDataSet(name);           // Access the dataset
  verify_shape(f, name, d, {size, N});       // Ensure it has the expected shape
  d.read(data.data(), H5PT);                 // Read dataset from file
  verify_units(d, units);                    // Verify it has the expected units
}

/* Read full data after initial setup. */
void Rays::read_data(const int file) {
  const int size = file_sizes[file];         // Array sizes
  const string sfile = sub_file(file);       // Subfile name

  // Read particle positions
  r.resize(size);                            // Ensure data space availability
  auto r_units = Units(1., -1., 1., 0., 0., unit_length);
  read("PartType0/Coordinates", r, r_units, size, sfile);

  // Read all scalar datasets
  for (int field = 0; field < n_read_fields; ++field) {
    if (ray_flags[field]) {
      RayField& fld = fields[field];         // RayField struct
      fld.data.resize(size);                 // Adjust the data size
      read("PartType0/" + fld.name, fld.data, fld.units, size, sfile);
    }
  }

  // Read all vector datasets
  for (int field = 0; field < n_vec_fields; ++field) {
    if (vec_flags[field]) {
      VecField& fld = vec_fields[field];     // VecField struct
      fld.data.resize(size);                 // Adjust the data size
      read("PartType0/" + fld.name, fld.data, fld.units, size, sfile);
    }
  }

  if (output_metals) {                       // Read individual metal species
    metals.data.resize(size);                // Adjust the data size
    read("PartType0/" + metals.name, metals.data, metals.units, size, sfile);
  }
}

/* Calculate the maximum cell radius. */
double Rays::calculate_max_cell_radius() {
  auto& dens_fld = fields[Density];          // Density field
  auto& mass_fld = fields[Masses];           // Masses field
  auto& dens = dens_fld.data;                // Local references
  auto& mass = mass_fld.data;
  mass.reserve(max_file_size);               // Allocate max space
  float V_max = 0.;
  for (int file = 0; file < n_files; ++file) {
    const int size = file_sizes[file];
    dens.resize(size);                       // Ensure data space availability
    mass.resize(size);
    read("PartType0/" + dens_fld.name, dens, dens_fld.units, size, sub_file(file));
    read("PartType0/" + mass_fld.name, mass, mass_fld.units, size, sub_file(file));
    #pragma omp parallel for reduction(max:V_max)
    for (int i = 0; i < size; ++i) {
      if (mass[i] > V_max * dens[i])
        V_max = mass[i] / dens[i];
    }
  }
  mass = vector<float>();                    // Free memory

  // Distribute the global max
  if (n_ranks > 1)
    MPI_Allreduce(MPI_IN_PLACE, &V_max, 1, MPI_FLOAT, MPI_MAX, MPI_COMM_WORLD);

  return cell_radius_factor * 0.75 * pow(V_max, 1./3.);  // Allow distorted Voronoi cells
  // Note that a sphere has: (3 V_max / 4 pi)^1/3 = 0.62 V_max^1/3
}

/* Write dataset unit scalings. */
template <typename T>
static inline void write(T& f, Units& units) {
  write(f, "a_scaling", units.a_scaling);
  write(f, "h_scaling", units.h_scaling);
  write(f, "length_scaling", units.length_scaling);
  write(f, "mass_scaling", units.mass_scaling);
  write(f, "velocity_scaling", units.velocity_scaling);
  write(f, "to_cgs", units.to_cgs);
}

/* Reads rays data and info from the hdf5 files. */
void Rays::read_hdf5() {
  read_timer.start();

  read_info();                               // Read top level info
  setup_dataspace();                         // Allocate space for each file
  setup_fields();                            // Initialize active fields
  const double zp1 = 1. + z;                 // 1/a factor
  const double OmegaLambda = 1. - Omega0;    // Dark energy density (flat cosmology)
  const double Hz = 100. * h100 * sqrt(Omega0 * zp1*zp1*zp1 + OmegaLambda); // Hubble parameter [km/s/Mpc]
  const double cMpc_to_code = h100 * Mpc / unit_length; // cMpc -> code units
  if (r_max_kpc > 0.)
    r_max = 1e-3 * zp1 * r_max_kpc * cMpc_to_code; // Pre-specified cylinder radius [a*length/h]
  else
    r_max = calculate_max_cell_radius();     // Cylinder radius [a*length/h]
  if (fof_origin)
    read_fof_info();                         // Read top level halo info

  // Calculate derived quantities
  r2_max = r_max * r_max;                    // Cylinder radius^2 [a*length/h]^2
  if (start_fvir > 0.)
    r_start = start_fvir * R_Crit200;        // Specify starting radius from f_vir
  else
    r_start = start_cMpc * cMpc_to_code;     // Specify starting radius from cMpc
  if (length_kms > 0.)
    length_Mpc = length_kms / Hz;
  if (length_bbox > 0.)
    length_cMpc = length_bbox * box_size / cMpc_to_code;
  if (length_Mpc > 0.)
    length_cMpc = zp1 * length_Mpc;          // Ray length [cMpc]
  length_Mpc = length_cMpc / zp1;            // Ray length [Mpc]
  length_bbox = length_cMpc * cMpc_to_code / box_size; // Ray length [bbox]
  length_kms = Hz * length_Mpc;              // Ray length proxy [km/s]
  r_len = length_cMpc * cMpc_to_code;        // Specify ray lengths from cMpc
  r_end = r_start + r_len;                   // Add start offset
  r_start_buffer = r_start - r_max;          // Add padding
  r_end_buffer = r_end + r_max;
  r2_start_buffer = r_start_buffer * r_start_buffer;
  r2_end_buffer = r_end_buffer * r_end_buffer;
  if (r_start_buffer < 0.)                   // Ensure consistency
    r2_start_buffer *= -1.;
  if (r_end_buffer < 0.)
    r2_end_buffer *= -1.;
  bbox[0] = {-r_max, -r_max, -r_max};        // Bounding box min [a*length/h]
  bbox[1] = {r_max, r_max, r_max + r_len};   // Bounding box max [a*length/h]
  max_bbox_width = 2.*r_max + r_len;         // Max bbox width [a*length/h]

  // Print the initial conditions data
  if (root) {
    const double length_to_cMpc = 1. / cMpc_to_code;
    const double length_to_cMpch = h100 * length_to_cMpc;
    const double length_to_Mpc = length_to_cMpc / zp1;
    const double length_to_kpc = 1e3 * length_to_Mpc;
    const double length_to_ckpch = 1e3 * length_to_cMpch;
    const double length_to_kms = Hz * length_to_Mpc;
    cout << "\nSimulation info: [Rays]"
         << "\n  redshift   = " << z;
    if (verbose)
      cout << "\n  Omega0     = " << Omega0 << " rho_crit_0"
           << "\n  OmegaB     = " << OmegaB << " rho_crit_0"
           << "\n  H_0        = " << 100. * h100 << " km/s/Mpc"
           << "\n  H(z)       = " << Hz << " km/s/Mpc"
           << "\n  d_L        = " << d_L / Gpc << " Gpc"
           << "\n  n_files    = " << n_files_tot;
    cout << "\n  box_size   = " << box_size * length_to_cMpch << " cMpc/h = "
                                << box_size * length_to_cMpc << " cMpc = "
                                << box_size * length_to_Mpc << " Mpc = "
                                << box_size * length_to_kms << " km/s"
         << "\n  r_length   = " << r_len * length_to_cMpch << " cMpc/h = "
                                << r_len * length_to_cMpc << " cMpc = "
                                << r_len * length_to_Mpc << " Mpc = "
                                << r_len * length_to_kms << " km/s"
         << "\n  r_cylinder = " << r_max * length_to_kpc << " kpc" << endl;
    if (fof_origin) {
      const double dr_com = r_halo.dist(r_com);
      cout << "\nSelected halo " << SubhaloID << " from group " << GroupID
           << "\n  R_Crit200  = " << R_Crit200 * length_to_ckpch << " ckpc/h = "
                                  << R_Crit200 * length_to_kpc << " kpc"
           << "\n  halo mass  = " << M_halo * unit_mass / (h100 * Msun) << " Msun"
           << "\n  position   = " << r_halo * length_to_Mpc << " Mpc"
           << "\n  centroid   = " << r_com * length_to_Mpc << " Mpc"
           << "\n  offset     = " << dr_com * length_to_kpc << " kpc"
           << "\n  velocity   = " << v_halo << " km/s" << endl;
      print("mass (rad)", SubhaloMassInRadType, "Msun", Msun*h100/unit_mass);
      print("mass (halo)", SubhaloMassType, "Msun", Msun*h100/unit_mass);
      print("n_particles", SubhaloLenType);
      if (black_hole_origin) {
        print("part offsets", SubhaloOffsetsType);
        cout << "  black hole = " << ray_origin * length_to_Mpc << " Mpc" << endl;
      }
      if (lyman_alpha_origin)
        cout << "  Lya center = " << ray_origin * length_to_Mpc << " Mpc" << endl;
      if (black_hole_origin || lyman_alpha_origin) {
        cout << "  v_systemic = " << ray_systemic << " km/s\n"
             << "  | v_sys |  = " << ray_systemic.norm() << " km/s" << endl;
      }
    } else if (multiple_origins) {
      cout << "\nUsing distinct ray origins" << endl;
      if (verbose)
        print("origins", ray_origins, "box size");
    } else {
      cout << "\nUser specified ray origin: " << ray_origin << " box size" << endl;
    }
    cout << "\nOutput fields:" << endl;
    for (int field = 0; field < n_copy_fields; ++field) {
      if (ray_flags[field]) {
        RayField& fld = fields[field];       // RayField struct
        cout << "  " << fld.name << endl;
      }
    }
    if (output_velocities)
      cout << "  Velocities" << endl;
    for (int field = 1; field < n_vec_fields; ++field) {
      if (vec_flags[field]) {
        VecField& fld = vec_fields[field];   // VecField struct
        cout << "  " << fld.name << endl;
      }
    }
    if (output_metals)                       // Metal species
      cout << "  GFM_Metals" << endl;
  }
  read_timer.stop();
}

/* Create cylinder data groups. */
void Rays::create_data_groups(const string& prefix, const int file) {
  // Open file and write header
  H5File f(output_dir + "/" + prefix + snap_str + halo_str + "." +
           to_string(first_file + file) + "." + output_ext, H5F_ACC_TRUNC);

  // Set up the file group structure
  Group g = f.createGroup("Coordinates");    // Coordinates group
  for (int field = 0; field < n_copy_fields; ++field)
    if (ray_flags[field])
      Group g = f.createGroup(fields[field].name); // Scalar field groups
  for (int field = 0; field < n_vec_fields; ++field)
    if (vec_flags[field])
      Group g = f.createGroup(vec_fields[field].name); // Vector field groups
  if (output_metals)
    Group g = f.createGroup(metals.name);    // Metal species groups
}

/* Writes consolidated cylinder file. */
void Rays::write_cylinder(const int ray) {
  // Open file and write header
  H5File f(output_dir + "/cylinder" + snap_str + halo_str + "_" + to_string(ray) + "." + output_ext, H5F_ACC_TRUNC);

  // Write position and field datasets
  write(f, "Coordinates", r);                // Write positions data
  for (int field = 0; field < n_copy_fields; ++field) {
    if (ray_flags[field]) {
      auto& fld = fields[field];             // RayField struct
      write(f, fld.name, fld.data);          // Write scalar field data
    }
  }
  for (int field = 0; field < n_vec_fields; ++field) {
    if (vec_flags[field]) {
      auto& fld = vec_fields[field];         // VecField struct
      write(f, fld.name, fld.data);          // Write vector field data
    }
  }
  if (output_metals)
    write(f, metals.name, metals.data);      // Write vector field data
}

/* Create final rays data file. */
void Rays::create_rays_file(const string& filename) {
  // Open file and write header
  H5File f(filename, H5F_ACC_TRUNC);

  // Write general simulation information
  write(f, "Redshift", z);                   // Current simulation redshift
  write(f, "HubbleParam", h100);             // Hubble constant [100 km/s/Mpc]
  write(f, "Omega0", Omega0);                // Matter density [rho_crit_0]
  write(f, "OmegaBaryon", OmegaB);           // Baryon density [rho_crit_0]
  write(f, "UnitLength_in_cm", unit_length); // Unit length [cm]
  write(f, "UnitMass_in_g", unit_mass);      // Unit mass [g]
  write(f, "UnitVelocity_in_cm_per_s", unit_velocity); // Unit velocity [cm/s]
  write(f, "BoxSize", box_size);             // Simulation box size [a*length/h]

  write(f, "MultipleOrigins", multiple_origins);
  if (fof_origin) {
    // Subhalo information
    write(f, "GroupID", GroupID);            // Group ID
    write(f, "SubhaloID", SubhaloID);        // Subhalo ID
    write(f, "R_Crit200", R_Crit200);        // Halo virial radius [a*length/h]
    write(f, "SubhaloMass", M_halo);         // Halo virial mass [mass/h]
    write_attr(f, "SubhaloPos", r_halo);     // Halo position [a*length/h]
    write_attr(f, "SubhaloCM", r_com);       // Halo center of mass [a*length/h]
    write_attr(f, "SubhaloVel", v_halo);     // Halo velocity [km/s]
    write_attr(f, "SubhaloMassInRadType", SubhaloMassInRadType); // Subhalo mass in radius [mass/h]
    write_attr(f, "SubhaloMassType", SubhaloMassType); // Subhalo mass in halo [mass/h]
    write_attr(f, "SubhaloLenType", SubhaloLenType); // Subhalo particle counts by type
    if (black_hole_origin)
      write_attr(f, "SubhaloOffsetsType", SubhaloOffsetsType); // Subhalo particle offsets by type
    write_attr(f, "SystemicVelocity", ray_systemic); // Systemic velocity [km/s]
  }

  // Rays information
  vector<Vec3> origins;                      // Ray origin points [a*length/h]
  vector<Vec3> endings;                      // Ray ending points [a*length/h]
  origins.resize(n_cameras);
  endings.resize(n_cameras);

  if (multiple_origins) {
    #pragma omp parallel for
    for (int ray = 0; ray < n_cameras; ++ray) {
      origins[ray] = ray_origins[ray] + camera_directions[ray] * r_start;
      endings[ray] = ray_origins[ray] + camera_directions[ray] * (r_start + r_len);
    }
  } else {
    #pragma omp parallel for
    for (int ray = 0; ray < n_cameras; ++ray) {
      origins[ray] = ray_origin + camera_directions[ray] * r_start;
      endings[ray] = ray_origin + camera_directions[ray] * (r_start + r_len);
    }
  }
  write(f, "RayImpact", r_max);              // Cylinder radius [a*length/h]
  write(f, "RaySphere", r_start);            // Ray starting radius [a*length/h]
  write(f, "RayLength", r_len);              // Ray length [a*length/h]
  if (fof_origin && start_fvir > 0.)
    write(f, "RaySphere_fvir", start_fvir);  // Ray starting radius [fvir]
  else if (start_cMpc > 0.)
    write(f, "RaySphere_cMpc", start_cMpc);  // Ray starting radius [cMpc]
  if (length_cMpc > 0.)
    write(f, "RayLength_cMpc", length_cMpc); // Ray length [cMpc]
  auto r_units = Units(1., -1., 1., 0., 0., unit_length);
  if (multiple_origins) {
    write(f, "RayOrigins", origins);         // Ray origin points [a*length/h]
    DataSet d = f.openDataSet("RayOrigins"); // Access the dataset
    write(d, r_units);                       // Length units
  } else {
    write(f, "RayOrigin", ray_origin);       // Ray origin point [a*length/h]
    DataSet d = f.openDataSet("RayOrigin");  // Access the dataset
    write(d, r_units);                       // Length units
  }
  if (multiple_origins) {
    write(f, "RayEndings", endings);         // Ray origin points [a*length/h]
    DataSet d = f.openDataSet("RayEndings"); // Access the dataset
    write(d, r_units);                       // Length units
  }
  write(f, "NumRays", n_cameras);            // Number of rays
  if (n_side > 0)
    write(f, "n_side", n_side);              // Healpix parameter for cameras
  write(f, "RayDirections", camera_directions); // Normalized ray directions

  // Set up the file group structure
  for (int field = 0; field < n_copy_fields; ++field) {
    if (ray_flags[field]) {
      auto& fld = fields[field];             // RayField struct
      Group g = f.createGroup(fld.name);     // Scalar field groups
      write(g, fld.units);                   // Scalar field units
    }
  }
  for (int field = 0; field < n_vec_fields; ++field) {
    if (vec_flags[field]) {
      auto& fld = vec_fields[field];         // VecField struct
      Group g = f.createGroup(fld.name);     // Vector field groups
      write(g, fld.units);                   // Vector field units
    }
  }
  if (output_metals) {
    Group g = f.createGroup(metals.name);    // Metal species group
    auto m_units = output_metals_as_densities ? fields[Density].units : metals.units;
    write(g, m_units);                       // Metal species units
  }
  Group g = f.createGroup("RaySegments");    // Ray segments group
  write(g, r_units);                         // Length units
}

/* Writes data and info to the specified hdf5 filename. */
void Rays::write_hdf5() {
  write_timer.start();
  // Dummy function for this module
  write_timer.stop();
}
