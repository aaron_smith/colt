/***************
 * rays/Rays.h *
 ***************

 * Module declarations.

*/

#ifndef RAYS_INCLUDED
#define RAYS_INCLUDED

#include "../Simulation.h" // Base simulation class

using RayField = GenericField<float>;
using VecField = GenericField<Vec3F>;

/* Reionization box data analysis. */
class Rays : public Simulation {
public:
  void run() override;                       // Module calculations

  // List of possible scalar fields
  enum RayFieldIndex {
    // Required fields
    Density = 0,                             // Density index
    DustMetallicity,                         // Dust metallicity index
    Metallicity,                             // Metallicity index
    HI_Fraction,                             // HI fraction index
    HeI_Fraction,                            // HeI fraction index
    HeII_Fraction,                           // HeII fraction index
    NeutralHydrogenAbundance,                // Neutral hydrogen abundance index
    ElectronAbundance,                       // Electron abundance index
    InternalEnergy,                          // Internal energy index
    StarFormationRate,                       // Star formation rate index
    n_read_fields,                           // Number of fields to read
    Velocity = n_read_fields,                // Parallel velocity index
    n_copy_fields,                           // Number of fields to copy
    Masses = n_copy_fields,                  // Masses index
    n_ray_fields                             // Number of scalar fields
  };

  // List of possible vector fields
  enum VecFieldIndex {
    // Required fields
    Velocities = 0,                          // Velocities index
    Acceleration,                            // Acceleration index
    PhotonDensity,                           // Photon density index
    MagneticField,                           // Magnetic field index
    n_vec_fields                             // Number of vector fields
  };

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void read_hdf5() override;                 // Reads data and info from files
  void write_hdf5() override;                // Writes data and info to a file

private:
  double unit_length;                        // Unit length [cm]
  double unit_mass;                          // Unit mass [g]
  double unit_velocity;                      // Unit velocity [cm/s]

  double box_size;                           // Box size [a*length/h]
  double box_half;                           // Half box size [a*length/h]
  double r_max_kpc = 0.;                     // Cylinder radius [kpc]
  double r_max = 0.;                         // Cylinder radius [a*length/h]
  double r2_max = 0.;                        // Cylinder radius^2 [a*length/h]^2
  double cell_radius_factor = 1.;            // Cell radius factor
  double r_start = 0.;                       // Ray starting radius [a*length/h]
  double r_len = 1.;                         // Ray length [a*length/h]
  double r_end = 1.;                         // Ray ending radius [a*length/h]
  double r_start_buffer = 0.;                // Ray starting radius with padding
  double r_end_buffer = 1.;                  // Ray ending radius with padding
  double r2_start_buffer = 0.;               // Starting radius^2
  double r2_end_buffer = 0.;                 // Ending radius^2

  bool multiple_origins = false;             // Each ray has its own origin
  bool spherical_extraction = false;         // Flag for spherical extraction
  bool extraction_only = false;              // Only perform the extraction step
  bool ray_trace_only = false;               // Only perform the ray tracing step
  vector<Vec3> ray_origins;                  // Ray origins [a*length/h]
  Vec3 ray_origin;                           // Ray origin [a*length/h]
  Vec3 ray_systemic = 0.;                    // Ray systemic velocity [km/s]
  double start_fvir = 0.;                    // Starting radius [f_vir]
  double start_cMpc = 0.;                    // Starting radius [cMpc]
  double length_cMpc = 1.;                   // Ray length [cMpc]
  double length_Mpc = 0.;                    // Ray length [Mpc]
  double length_bbox = 0.;                   // Ray length [bbox]
  double length_kms = 0.;                    // Ray length proxy [km/s]

  bool fof_origin = false;                   // Read origin from fof file
  bool black_hole_origin = false;            // Use most massive black hole of subhalo
  bool lyman_alpha_origin = false;           // Use Lyman-alpha subhalo catalog
  int SubhaloID = 0, GroupID = 0;            // Subhalo ID, Group ID
  double R_Crit200 = 0.;                     // Halo virial radius [a*length/h]
  double M_halo = 0.;                        // Halo virial mass [mass/h]
  Vec3 r_halo;                               // Halo position [a*length/h]
  Vec3 r_com;                                // Halo center of mass [a*length/h]
  Vec3 v_halo;                               // Halo velocity [km/s]
  array<float, 6> SubhaloMassInRadType;      // Subhalo mass in radius [mass/h]
  array<float, 6> SubhaloMassType;           // Subhalo mass in halo [mass/h]
  array<int, 6> SubhaloLenType;              // Subhalo particle counts by type
  array<long long, 6> SubhaloOffsetsType;    // Subhalo particle offsets by type
  vector<long long> FileOffsetsGroup;        // File offsets for groups
  vector<long long> FileOffsetsSubhalo;      // File offsets for subhalos
  vector<array<long long,6>> FileOffsetsType; // File offsets for snapshots

  int n_files_tot = 1;                       // Number of initial conditions files
  int n_files = 1;                           // Number of local files
  int first_file = 0;                        // File ownership range
  vector<int> file_sizes;                    // Dataset sizes on each file
  int max_file_size = 0;                     // Largest assigned file size

  array<RayField, n_ray_fields> fields;      // File scalar dataset fields
  array<VecField, n_vec_fields> vec_fields;  // File vector dataset fields
  array<bool, n_ray_fields> ray_flags;       // Flags for scalar fields
  array<bool, n_vec_fields> vec_flags;       // Flags for vector fields
  bool output_dust = true;                   // Output dust metallicity
  bool output_metallicity = true;            // Output metallicity
  bool output_HI = true;                     // Output HI fraction
  bool output_HeI = false;                   // Output HeI fraction
  bool output_HeII = false;                  // Output HeII fraction
  bool output_neutral = false;               // Output neutral hydrogen abundance
  bool output_electrons = true;              // Output electron abundance
  bool output_internal_energy = true;        // Output internal energy
  bool output_SFR = true;                    // Output star formation rate
  bool output_parallel_velocity = true;      // Output parallel velocity
  bool output_velocities = false;            // Output velocities
  bool output_acceleration_rays = false;     // Output acceleration
  bool output_radiation = false;             // Output photon density
  bool output_magnetic_field = false;        // Output magnetic field

  static const int n_metals = 9;             // Number of metal species
  GenericField<array<float,n_metals>> metals; // Metal species dataset [mass fraction]
  bool output_metals = false;                // Output metal species fractions
  bool output_metals_as_densities = false;   // Output metal species as densities

  // Generate a rendering subfile name
  string pre_filename;                       // Common part of the file names
  string fof_base = "fof_subhalo_tab";       // Friends of friends base name
  string pre_fof_name;                       // Common part of the file names
  string sub_file(const int file) {
    string str = to_string(first_file + file); // Setup leading zeros
    return pre_filename + str + "." + init_ext;
  }
  string fof_sub_file(const int file) {
    return pre_fof_name + to_string(file) + "." + init_ext;
  }

  void read_info();                          // Read top level information
  void read_fof_info();                      // Read top level halo information
  void read_file_offsets_info();             // Read file offsets information
  void read_offsets_info(const int s0);      // Read subhalo offsets information
  void read_max_black_hole_info();           // Read subhalo black hole info
  void read_lyman_alpha_info();              // Read subhalo Lyman-alpha info
  void setup_dataspace();                    // Allocate space for each file
  void setup_fields();                       // Initialize active fields
  void read_data(const int file = 0);        // Read full datasets
  double calculate_max_cell_radius();        // Default cylinder radius
  void recenter_box(const Vec3 displacement, const int size); // Box centering routine
  int min_tile(const double k_i);            // Min tiling index for an axis
  int max_tile(const double k_i);            // Max tiling index for an axis
  void ray_trace_sphere();                   // Only perform the ray tracing step
  void extract_sphere();                     // Extract spherical region
  void extract_cylinders();                  // Extract all cylinder regions
  void create_data_groups(const string& prefix, const int file = 0); // Create data groups
  void combine_cylinders();                  // Combine cylinder files
  void cylinders_to_rays();                  // Convert cylinder file to ray file
  void combine_rays();                       // Combine ray files
  void write_cylinder(const int ray = 0);    // Consolidated cylinder file
  void create_rays_file(const string& filename); // Create final rays data file
};

#endif // RAYS_INCLUDED
