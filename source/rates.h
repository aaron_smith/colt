/***********
 * rates.h *
 ***********

 * Radiative and diaelectric recombination, collisional ionization, and
   charge exchange rates for H, He, C, N, O, Ne, Mg, Si, S, and Fe.

*/

// Ionization threshold temperatures [K]
constexpr double HI_K = HI_erg / kB;         // Hydrogen energy [K]
constexpr double HeI_K = HeI_erg / kB;       // Helium energies [K]
constexpr double HeII_K = HeII_erg / kB;
constexpr double CI_K = CI_erg / kB;         // Carbon energies [K]
constexpr double CII_K = CII_erg / kB;
constexpr double CIII_K = CIII_erg / kB;
constexpr double CIV_K = CIV_erg / kB;
constexpr double CV_K = CV_erg / kB;
constexpr double CVI_K = CVI_erg / kB;
constexpr double NI_K = NI_erg / kB;         // Nitrogen energies [K]
constexpr double NII_K = NII_erg / kB;
constexpr double NIII_K = NIII_erg / kB;
constexpr double NIV_K = NIV_erg / kB;
constexpr double NV_K = NV_erg / kB;
constexpr double NVI_K = NVI_erg / kB;
constexpr double NVII_K = NVII_erg / kB;
constexpr double OI_K = OI_erg / kB;         // Oxygen energies [K]
constexpr double OII_K = OII_erg / kB;
constexpr double OIII_K = OIII_erg / kB;
constexpr double OIV_K = OIV_erg / kB;
constexpr double OV_K = OV_erg / kB;
constexpr double OVI_K = OVI_erg / kB;
constexpr double OVII_K = OVII_erg / kB;
constexpr double OVIII_K = OVIII_erg / kB;
constexpr double NeI_K = NeI_erg / kB;       // Neon energies [K]
constexpr double NeII_K = NeII_erg / kB;
constexpr double NeIII_K = NeIII_erg / kB;
constexpr double NeIV_K = NeIV_erg / kB;
constexpr double NeV_K = NeV_erg / kB;
constexpr double NeVI_K = NeVI_erg / kB;
constexpr double NeVII_K = NeVII_erg / kB;
constexpr double NeVIII_K = NeVIII_erg / kB;
constexpr double MgI_K = MgI_erg / kB;       // Magnesium energies [K]
constexpr double MgII_K = MgII_erg / kB;
constexpr double MgIII_K = MgIII_erg / kB;
constexpr double MgIV_K = MgIV_erg / kB;
constexpr double MgV_K = MgV_erg / kB;
constexpr double MgVI_K = MgVI_erg / kB;
constexpr double MgVII_K = MgVII_erg / kB;
constexpr double MgVIII_K = MgVIII_erg / kB;
constexpr double MgIX_K = MgIX_erg / kB;
constexpr double MgX_K = MgX_erg / kB;
constexpr double SiI_K = SiI_erg / kB;       // Silicon energies [K]
constexpr double SiII_K = SiII_erg / kB;
constexpr double SiIII_K = SiIII_erg / kB;
constexpr double SiIV_K = SiIV_erg / kB;
constexpr double SiV_K = SiV_erg / kB;
constexpr double SiVI_K = SiVI_erg / kB;
constexpr double SiVII_K = SiVII_erg / kB;
constexpr double SiVIII_K = SiVIII_erg / kB;
constexpr double SiIX_K = SiIX_erg / kB;
constexpr double SiX_K = SiX_erg / kB;
constexpr double SiXI_K = SiXI_erg / kB;
constexpr double SiXII_K = SiXII_erg / kB;
constexpr double SI_K = SI_erg / kB;         // Sulfer energies [K]
constexpr double SII_K = SII_erg / kB;
constexpr double SIII_K = SIII_erg / kB;
constexpr double SIV_K = SIV_erg / kB;
constexpr double SV_K = SV_erg / kB;
constexpr double SVI_K = SVI_erg / kB;
constexpr double SVII_K = SVII_erg / kB;
constexpr double SVIII_K = SVIII_erg / kB;
constexpr double SIX_K = SIX_erg / kB;
constexpr double SX_K = SX_erg / kB;
constexpr double SXI_K = SXI_erg / kB;
constexpr double SXII_K = SXII_erg / kB;
constexpr double SXIII_K = SXIII_erg / kB;
constexpr double SXIV_K = SXIV_erg / kB;
constexpr double FeI_K = FeI_erg / kB;       // Iron energies [K]
constexpr double FeII_K = FeII_erg / kB;
constexpr double FeIII_K = FeIII_erg / kB;
constexpr double FeIV_K = FeIV_erg / kB;
constexpr double FeV_K = FeV_erg / kB;
constexpr double FeVI_K = FeVI_erg / kB;
constexpr double FeVII_K = FeVII_erg / kB;
constexpr double FeVIII_K = FeVIII_erg / kB;
constexpr double FeIX_K = FeIX_erg / kB;
constexpr double FeX_K = FeX_erg / kB;
constexpr double FeXI_K = FeXI_erg / kB;
constexpr double FeXII_K = FeXII_erg / kB;
constexpr double FeXIII_K = FeXIII_erg / kB;
constexpr double FeXIV_K = FeXIV_erg / kB;
constexpr double FeXV_K = FeXV_erg / kB;
constexpr double FeXVI_K = FeXVI_erg / kB;

/************/
/* Hydrogen */
/************/

/* Recombination coefficient (HII Case B) - Units: cm^3/s */
static inline double alpha_B_HII(const double T) {
  // Case B recombination coefficient - Units: cm^3/s
  // α_B(T) is from Hui & Gnedin (1997) good to 0.7% accuracy from 1 K < T < 10^9 K
  const double lambda = 315614. / T;         // 2 T_i/T where T_i = 157,807 K
  return 2.753e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
}

/* HI collisional ionization rates (Cen 1992) */
static inline double beta_HI(const double T) {
  return 5.85e-11 * sqrt(T) * exp(-157809.1/T) / (1. + sqrt(T * 1e-5)); // H + e-  ->  H+ + 2e-
}

/**********/
/* Helium */
/**********/

/* Recombination coefficient (HeII Case B) - Units: cm^3/s */
static inline double alpha_B_HeII(const double T) {
  const double lambda = 570670. / T;         // 2 T_i/T where T_i = 285,335 K
  return 1.26e-14 * pow(lambda, 0.75);
}

/* Diaelectric recombination coefficient (HeII Case B) - Units: cm^3/s */
static inline double alpha_DR_HeII(const double T) {
  return 1.9e-3 * pow(T, -1.5) * exp(-470000./T) * (1. + 0.3 * exp(-94000./T));
}

/* Recombination coefficient (HeIII Case B) - Units: cm^3/s */
static inline double alpha_B_HeIII(const double T) {
  const double lambda = 1263030. / T;        // 2 T_i/T where T_i = 631,515 K
  return 5.506e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
}

/* HeI collisional ionization rates (Cen 1992) */
static inline double beta_HeI(const double T) {
  return 2.38e-11 * sqrt(T) * exp(-285335.4/T) / (1. + sqrt(T * 1e-5)); // He + e-  ->  He+ + 2e-
}

/* HeII collisional ionization rates (Cen 1992) */
static inline double beta_HeII(const double T) {
  return 5.68e-12 * sqrt(T) * exp(-631515./T) / (1. + sqrt(T * 1e-5)); // He+ + e-  ->  He++ + 2e-
}

/* HeIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_HeIII([[maybe_unused]] const double T) {
  return 1e-14;
}

/* HeII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_HeII(const double T) {
  const double T4 = (T < 6e3) ? 0.6 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 7.47e-15 * pow(T4, 2.06) * (1. + 9.93 * exp(-3.89 * T4));
}

/**********/
/* Carbon */
/**********/

/* CVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_CVII(const double T) {
  const double ST0 = sqrt(T / 95.02);
  const double ST1 = sqrt(T / 2.517e7);
  return 5.337e-10 / (ST0 * pow(1. + ST0, 0.2515) * pow(1. + ST1, 1.7485));
}

/* CVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_CVI(const double T) {
  const double ST0 = sqrt(T / 264.7);
  const double ST1 = sqrt(T / 2.773e7);
  return 2.044e-10 / (ST0 * pow(1. + ST0, 0.3258) * pow(1. + ST1, 1.6742));
}

/* CV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_CV(const double T) {
  const double ST0 = sqrt(T / 1355.);
  const double ST1 = sqrt(T / 1.872e7);
  return 4.798e-11 / (ST0 * pow(1. + ST0, 0.5166) * pow(1. + ST1, 1.4834));
}

/* CIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_CIV(const double T) {
  const double ST0 = sqrt(T / 111.5);
  const double ST1 = sqrt(T / 5.938e6);
  return 1.12e-10 / (ST0 * pow(1. + ST0, 0.3263) * pow(1. + ST1, 1.6737));
}

/* CIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_CIII(const double T) {
  const double ST0 = sqrt(T / 0.1643);
  const double ST1 = sqrt(T / 2.172e6);
  const double Bp = 0.8012 + 0.0427 * exp(-63410./T);
  return 2.067e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* CII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_CII(const double T) {
  const double ST0 = sqrt(T / 0.00667);
  const double ST1 = sqrt(T / 1.943e6);
  const double Bp = 0.7849 + 0.1597 * exp(-49550./T);
  return 2.995e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* CVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_CVI(const double T) {
  return pow(T, -1.5) * (0.001426 * exp(-3.116e6/T) + 0.03046 * exp(-4.075e6/T) + 0.0008373 * exp(-5.749e6/T));
}

/* CV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_CV(const double T) {
  return pow(T, -1.5) * (0.002646 * exp(-2.804e6/T) + 0.01762 * exp(-3.485e6/T) - 0.0007843 * exp(-4.324e6/T));
}

/* CIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_CIV(const double T) {
  return pow(T, -1.5) * (4.673e-7 * exp(-723.3/T) + 1.887e-5 * exp(-2847./T) + 1.305e-5 * exp(-10540./T) + 0.003099 * exp(-89150./T) + 0.0003001 * exp(-281200./T) + 0.002553 * exp(-3.254e6/T));
}

/* CIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_CIII(const double T) {
  return pow(T, -1.5) * (3.489e-6 * exp(-2660./T) + 2.222e-7 * exp(-3756./T) + 1.954e-5 * exp(-25660./T) + 0.004212 * exp(-140000./T) + 0.0002037 * exp(-1.801e6/T) + 0.0002936 * exp(-4.307e6/T));
}

/* CII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_CII(const double T) {
  return pow(T, -1.5) * (6.346e-9 * exp(-12.17/T) + 9.793e-9 * exp(-73.8/T) + 1.634e-6 * exp(-15230./T) + 0.0008369 * exp(-120700./T) + 0.0003355 * exp(-214400./T));
}

/* CII total radiative and dialectric recombination rate coefficient [cm^3/s]. */
static inline double alpha_CII(const double T) {
  return alpha_RR_CII(T) + alpha_DR_CII(T);
}

/* CI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_CI(const double T) {
  const double U = CI_K / T;
  return 6.85e-8 * pow(U, 0.25) * exp(-U) / (0.193 + U);
}

/* CII collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_CII(const double T) {
  const double U = CII_K / T;
  return 1.86e-8 * (1. + sqrt(U)) * pow(U, 0.24) * exp(-U) / (0.286 + U);
}

/* CIII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_CIII(const double T) {
  const double U = CIII_K / T;
  return 6.35e-9 * (1. + sqrt(U)) * pow(U, 0.21) * exp(-U) / (0.427 + U);
}

/* CIV collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_CIV(const double T) {
  const double U = CIV_K / T;
  return 1.5e-9 * (1. + sqrt(U)) * pow(U, 0.13) * exp(-U) / (0.416 + U);
}

/* CV collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_CV(const double T) {
  const double U = CV_K / T;
  return 2.99e-10 * (1. + sqrt(U)) * pow(U, 0.02) * exp(-U) / (0.666 + U);
}

/* CVI collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_CVI(const double T) {
  const double U = CVI_K / T;
  return 1.23e-10 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.62 + U);
}

/* CV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_CV(const double T) {
  const double T4 = (T < 10.) ? 0.001 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 3.3246e-7 * pow(T4, -0.11) * (1. - 0.995 * exp(-0.00158 * T4));
}

/* CIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_CIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 3.25e-9 * pow(T4, 0.21) * (1. + 0.19 * exp(-3.29 * T4));
}

/* CIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_CIII(const double T) {
  const double T4 = (T < 5e3) ? 0.5 : ((T > 5e4) ? 5. : 1e-4 * T);
  return 1.67e-13 * pow(T4, 2.79) * (1. + 304.72 * exp(-4.07 * T4));
}

/* CII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_CII(const double T) {
  const double T4 = (T < 5500.) ? 0.55 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 4.88e-16 * pow(T4, 3.25) * (1. - 1.12 * exp(-0.21 * T4));
}

/* CV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_CV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 1.)
    return 3.12e-16 * pow(T4, -0.0737) * (1. + 35 * exp(2.4 * T4));
  if (T4 < 35.)
    return 1.49e-14 * pow(T4, 2.73) * (1. + 5.93 * exp(-0.0874 * T4));
  return 5.8e-11 * pow(T4, 0.73) * (1. - 0.86 * exp(-0.0096 * T4));
}

/* CIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_CIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  return 1.12e-9 * pow(T4, 0.42) * (1. - 0.69 * exp(-0.34 * T4));
}

/* CI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_CI(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 1.07e-15 * pow(T4, 3.15) * (1. + 176.43 * exp(-4.29 * T4));
}

/* CII + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_CII(const double T) {
  return 5e-20 * T * T * exp(-7e-6 * T) * exp(-6.29*eV/(kB*T));
}

/* CI + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_CI(const double T) {
  return 6.3e-15 * pow(T/300., 0.75);
}

/************/
/* Nitrogen */
/************/

/* NVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NVIII(const double T) {
  const double ST0 = sqrt(T / 131.6);
  const double ST1 = sqrt(T / 3.427e7);
  return 6.17e-10 / (ST0 * pow(1. + ST0, 0.2519) * pow(1. + ST1, 1.7481));
}

/* NVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NVII(const double T) {
  const double ST0 = sqrt(T / 396.);
  const double ST1 = sqrt(T / 3.583e7);
  return 2.388e-10 / (ST0 * pow(1. + ST0, 0.3268) * pow(1. + ST1, 1.6732));
}

/* NVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NVI(const double T) {
  const double ST0 = sqrt(T / 1957.);
  const double ST1 = sqrt(T / 2.177e7);
  return 6.245e-11 / (ST0 * pow(1. + ST0, 0.5015) * pow(1. + ST1, 1.4985));
}

/* NV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NV(const double T) {
  const double ST0 = sqrt(T / 182.3);
  const double ST1 = sqrt(T / 7.751e6);
  return 1.533e-10 / (ST0 * pow(1. + ST0, 0.3318) * pow(1. + ST1, 1.6682));
}

/* NIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NIV(const double T) {
  const double ST0 = sqrt(T / 3.75);
  const double ST1 = sqrt(T / 3.468e6);
  const double Bp = 0.7768 + 0.0223 * exp(-72060/T);
  return 7.923e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NIII(const double T) {
  const double ST0 = sqrt(T / 0.1231);
  const double ST1 = sqrt(T / 3.016e6);
  const double Bp = 0.7948 + 0.0774 * exp(-101600./T);
  return 2.41e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NII(const double T) {
  const double ST0 = sqrt(T / 0.09467);
  const double ST1 = sqrt(T / 2.954e6);
  const double Bp = 0.7308 + 0.244 * exp(-67390./T);
  return 6.387e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NVII(const double T) {
  return pow(T, -1.5) * (0.002801 * exp(-4.198e6/T) + 0.04362 * exp(-5.516e6/T) + 0.001117 * exp(-8.05e6/T));
}

/* NVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NVI(const double T) {
  return pow(T, -1.5) * (0.005761 * exp(-3.86e6/T) + 0.03434 * exp(-4.883e6/T) - 0.00166 * exp(-6.259e6/T));
}

/* NV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NV(const double T) {
  return pow(T, -1.5) * (2.04e-6 * exp(-3084/T) + 6.986e-5 * exp(-13320/T) + 0.0003168 * exp(-64750/T) + 0.004353 * exp(-118100/T) + 0.0007765 * exp(-668700/T) + 0.005101 * exp(-4.778e6/T));
}

/* NIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NIV(const double T) {
  return pow(T, -1.5) * (3.386e-6 * exp(-1406./T) + 3.036e-5 * exp(-6965./T) + 5.945e-5 * exp(-26040./T) + 0.001195 * exp(-130400./T) + 0.006462 * exp(-196500./T) + 0.001358 * exp(-4.466e6/T));
}

/* NIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NIII(const double T) {
  return pow(T, -1.5) * (7.712e-8 * exp(-71.13/T) + 4.839e-8 * exp(-276.5/T) + 2.218e-6 * exp(-14390./T) + 0.001536 * exp(-134700./T) + 0.003647 * exp(-249600./T) + 4.234e-5 * exp(-2.204e6/T));
}

/* NII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NII(const double T) {
  return pow(T, -1.5) * (1.658e-8 * exp(-12.65/T) + 2.76e-8 * exp(-84.25/T) + 2.391e-9 * exp(-296.4/T) + 7.585e-7 * exp(-5923./T) + 0.0003012 * exp(-127800./T) + 0.0007132 * exp(-218400./T));
}

/* NI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_NI(const double T) {
  const double U = NI_K / T;
  return 4.82e-8 * pow(U, 0.42) * exp(-U) / (0.0652 + U);
}

/* NII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_NII(const double T) {
  const double U = NII_K / T;
  return 2.98e-8 * pow(U, 0.3) * exp(-U) / (0.31 + U);
}

/* NIII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_NIII(const double T) {
  const double U = NIII_K / T;
  return 8.1e-9 * (1. + sqrt(U)) * pow(U, 0.24) * exp(-U) / (0.35 + U);
}

/* NIV collisional ionization rates over [4 eV, 20 keV]. */
static inline double beta_NIV(const double T) {
  const double U = NIV_K / T;
  return 3.71e-9 * (1. + sqrt(U)) * pow(U, 0.18) * exp(-U) / (0.549 + U);
}

/* NV collisional ionization rates over [4 eV, 20 keV]. */
static inline double beta_NV(const double T) {
  const double U = NV_K / T;
  return 1.51e-9 * pow(U, 0.74) * exp(-U) / (0.0167 + U);
}

/* NVI collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_NVI(const double T) {
  const double U = NVI_K / T;
  return 3.71e-10 * pow(U, 0.29) * exp(-U) / (0.546 + U);
}

/* NVII collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_NVII(const double T) {
  const double U = NVII_K / T;
  return 7.77e-11 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.624 + U);
}

/* NV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e6) ? 100. : 1e-4 * T);
  return 2.95e-09 * pow(T4, 0.55) * (1. - 0.39 * exp(-1.07 * T4));
}

/* NIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NIV(const double T) {
  const double T4 = (T < 10.) ? 0.001 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 4.54e-9 * pow(T4, 0.57) * (1. - 0.65 * exp(-0.89 * T4));
}

/* NIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NIII(const double T) {
  if (T < 1500.)
    return 0.8692e-9 * pow(T/1500., 0.17);
  if (T < 20000.)
    return 0.9703e-9 * pow(T/10000., 0.058);
  return 1.0101e-9 + 1.4589e-9 * pow(log10(T/20000.), 2.06);
}

/* NII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NII(const double T) {
  const double lnT = log(T);
  return ((1.56e-15 - 1.79e-16*lnT + 1.15e-20*T)*T + 1.08e-13*lnT)
    + ((6.83e-16 - 7.40e-17*lnT + 3.73e-21*T)*T + 1.75e-15*lnT) * exp(-16680./T);
}

/* NV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_NV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 9.)
    return 1.26e-11 * pow(T4, 1.55) * (1. + 11.2 * exp(-7.82 * T4));
  return 3.75e-10 * pow(T4, 0.54) * (1. - 0.82 * exp(-0.0207 * T4));
}

/* NIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_NIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  return 2.05e-9 * pow(T4, 0.23) * (1. - 0.72 * exp(-0.19 * T4));
}

/* NIII + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_NIII(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 4.)
    return 4.84e-10 * pow(T4, 0.92) * (1. + 2.37 * exp(-10.2 * T4));
  return 3.17e-9 * pow(T4, 0.2) * (1. - 0.72 * exp(-0.0481 * T4));
}

/* NI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_NI(const double T) {
  const double lnT = log(T);
  return ((1.64e-16 - 8.76e-17*lnT + 2.41e-20*T)*T + 9.83e-13*lnT) * exp(-10985./T);
}

/* NII + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_NII(const double T) {
  return 3.7e-20 * T * T * exp(-6.3e-6 * T) * exp(-1.44*eV/(kB*T));
}

/**********/
/* Oxygen */
/**********/

/* OIX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OIX(const double T) {
  const double ST0 = sqrt(T / 195.1);
  const double ST1 = sqrt(T / 4.483e7);
  return 6.552e-10 / (ST0 * pow(1. + ST0, 0.253) * pow(1. + ST1, 1.747));
}

/* OVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OVIII(const double T) {
  const double ST0 = sqrt(T / 584.2);
  const double ST1 = sqrt(T / 4.559e7);
  return 2.652e-10 / (ST0 * pow(1. + ST0, 0.3295) * pow(1. + ST1, 1.6705));
}

/* OVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OVII(const double T) {
  const double ST0 = sqrt(T / 2392.);
  const double ST1 = sqrt(T / 2.487e7);
  return 8.193e-11 / (ST0 * pow(1. + ST0, 0.4835) * pow(1. + ST1, 1.5165));
}

/* OVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OVI(const double T) {
  const double ST0 = sqrt(T / 337.2);
  const double ST1 = sqrt(T / 1.03e7);
  return 1.724e-10 / (ST0 * pow(1. + ST0, 0.3444) * pow(1. + ST1, 1.6556));
}

/* OV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OV(const double T) {
  const double ST0 = sqrt(T / 0.6821);
  const double ST1 = sqrt(T / 5.076e6);
  return 3.955e-9 / (ST0 * pow(1. + ST0, 0.2187) * pow(1. + ST1, 1.7813));
}

/* OIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OIV(const double T) {
  const double ST0 = sqrt(T / 0.5235);
  const double ST1 = sqrt(T / 4.47e6);
  const double Bp = 0.7844 + 0.0447 * exp(-164200./T);
  return 2.501e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* OIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OIII(const double T) {
  const double ST0 = sqrt(T / 0.1602);
  const double ST1 = sqrt(T / 4.377e6);
  const double Bp = 0.7668 + 0.107 * exp(-139200./T);
  return 2.096e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* OII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_OII(const double T) {
  const double ST0 = sqrt(T / 4.136);
  const double ST1 = sqrt(T / 4.214e6);
  const double Bp = 0.6109 + 0.4093 * exp(-87700./T);
  return 6.622e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* OVIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OVIII(const double T) {
  return pow(T, -1.5) * (0.004925 * exp(-5.44e6/T) + 0.05837 * exp(-7.17e6/T) + 0.001359 * exp(-1.152e7/T));
}

/* OVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OVII(const double T) {
  return pow(T, -1.5) * (0.06135 * exp(-6.113e6/T) + 0.0001968 * exp(-3.656e7/T));
}

/* OVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OVI(const double T) {
  return pow(T, -1.5) * (2.389e-5 * exp(-23260/T) + 0.0001355 * exp(-32090./T) + 0.005885 * exp(-131600./T) + 0.002163 * exp(-673100./T) + 0.0006341 * exp(-1.892e6/T) + 0.01348 * exp(-6.15e6/T));
}

/* OV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OV(const double T) {
  return pow(T, -1.5) * (1.615e-5 * exp(-756.9/T) + 9.299e-6 * exp(-3659./T) + 0.000153 * exp(-19840./T) + 0.0006616 * exp(-84290./T) + 0.0108 * exp(-229400./T) + 0.0007503 * exp(-1.161e6/T) + 0.002892 * exp(-6.137e6/T));
}

/* OIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OIV(const double T) {
  return pow(T, -1.5) * (3.932e-7 * exp(-150.9/T) + 2.523e-7 * exp(-621.1/T) + 3.447e-5 * exp(-15620./T) + 0.005776 * exp(-193600./T) + 0.005101 * exp(-470000./T));
}

/* OIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OIII(const double T) {
  return pow(T, -1.5) * (1.627e-7 * exp(-45.35/T) + 1.262e-7 * exp(-284.7/T) + 6.663e-7 * exp(-4166./T) + 3.925e-6 * exp(-28770./T) + 0.002406 * exp(-195300./T) + 0.001146 * exp(-364600./T));
}

/* OII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_OII(const double T) {
  return pow(T, -1.5) * (5.629e-8 * exp(-5395./T) + 2.55e-7 * exp(-17700./T) + 0.0006173 * exp(-167100./T) + 0.0001627 * exp(-268700./T));
}

/* OI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_OI(const double T) {
  const double U = OI_K / T;
  return 3.59e-8 * pow(U, 0.34) * exp(-U) / (0.073 + U);
}

/* OII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_OII(const double T) {
  const double U = OII_K / T;
  return 1.39e-8 * (1. + sqrt(U)) * pow(U, 0.22) * exp(-U) / (0.212 + U);
}

/* OIII collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_OIII(const double T) {
  const double U = OIII_K / T;
  return 9.31e-9 * (1. + sqrt(U)) * pow(U, 0.27) * exp(-U) / (0.27 + U);
}

/* OIV collisional ionization rates over [4 eV, 20 keV]. */
static inline double beta_OIV(const double T) {
  const double U = OIV_K / T;
  return 1.02e-8 * pow(U, 0.27) * exp(-U) / (0.614 + U);
}

/* OV collisional ionization rates over [5 eV, 20 keV]. */
static inline double beta_OV(const double T) {
  const double U = OV_K / T;
  return 2.19e-9 * (1. + sqrt(U)) * pow(U, 0.17) * exp(-U) / (0.63 + U);
}

/* OVI collisional ionization rates over [7 eV, 20 keV]. */
static inline double beta_OVI(const double T) {
  const double U = OVI_K / T;
  return 1.95e-9 * pow(U, 0.54) * exp(-U) / (0.36 + U);
}

/* OVII collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_OVII(const double T) {
  const double U = OVII_K / T;
  return 2.12e-10 * pow(U, 0.35) * exp(-U) / (0.396 + U);
}

/* OVIII collisional ionization rates over [40 eV, 20 keV]. */
static inline double beta_OVIII(const double T) {
  const double U = OVIII_K / T;
  return 5.21e-11 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.629 + U);
}

/* OV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_OV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 2.52e-10 * pow(T4, 0.63) * (1. + 2.08 * exp(-4.16 * T4));
}

/* OIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_OIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 5e4) ? 5. : 1e-4 * T);
  return 3.98e-9 * pow(T4, 0.26) * (1. + 0.56 * exp(-2.62 * T4));
}

/* OIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_OIII(const double T) {
  if (T < 1500.)
    return 0.5337e-9 * pow(T/100., -0.076);
  return 0.4344e-9 + 0.6340e-9 * pow(log10(T/1500.), 2.06);
}

/* OII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_OII(const double T) {
  if (T < 10.)                               // Low T limit
    return 3.744e-10;
  const double lnT = log(T);
  return ((((1.1963502e-13*lnT - 2.8577012e-12)*lnT + 2.9979994e-11)*lnT
          - 1.3146803e-10)*lnT + 2.3651505e-10)*lnT + 2.3344302e-10;
}

/* OV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_OV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  return 9.97e-10 * pow(T4, 0.4) * (1. - 0.46 * exp(-0.35 * T4));
}

/* OIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_OIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  return 1.12e-9 * pow(T4, 0.42) * (1. - 0.71 * exp(-0.0198 * T4));
}

/* OIII + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_OIII(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 5.)
    return 7.1e-12 * pow(T4, 2.6) * (1. + 8.99 * exp(-0.78 * T4));
  return 6.21e-10 * pow(T4, 0.53) * (1. - 0.66 * exp(-0.0222 * T4));
}

/* OI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_OI(const double T) {
  if (T < 10.)                               // Low T limit
    return 4.749e-20;
  if (T < 190.)
    return exp(-21.134531 - 242.06831/T + 84.761441/(T*T));
  if (T < 200.)
    return 2.18733e-12 * (T - 190.) + 1.85823e-10;
  const double lnT = log(T);
  return (((((1.1580844e-14*lnT - 2.6139493e-13)*lnT + 2.0699463e-12)*lnT
    - 3.6606214e-12)*lnT - 1.488594e-12)*lnT - 3.7282001e-13)*lnT - 7.6767404e-14;
}

/* OI + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_OI(const double T) {
  const double T4 = 1e-4 * T;
  const double T5 = (T < 1e7) ? 1e-5 * T : 100.;
  return 4.991e-15 * pow(T4, 0.3794) * exp(-T4/112.1) +
         2.780e-15 * pow(T4, -0.2163) * exp(T5/8.158);
}

/********/
/* Neon */
/********/

/* NeIX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeIX(const double T) {
  const double ST0 = sqrt(T / 3647.);
  const double ST1 = sqrt(T / 3.365e7);
  return 1.186e-10 / (ST0 * pow(1. + ST0, 0.4646) * pow(1. + ST1, 1.5354));
}

/* NeVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeVIII(const double T) {
  const double ST0 = sqrt(T / 510.2);
  const double ST1 = sqrt(T / 1.535e7);
  return 2.755e-10 / (ST0 * pow(1. + ST0, 0.3414) * pow(1. + ST1, 1.6586));
}

/* NeVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeVII(const double T) {
  const double ST0 = sqrt(T / 6.293);
  const double ST1 = sqrt(T / 8.091e6);
  return 2.557e-9 / (ST0 * pow(1. + ST0, 0.2399) * pow(1. + ST1, 1.7601));
}

/* NeVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeVI(const double T) {
  const double ST0 = sqrt(T / 13.11);
  const double ST1 = sqrt(T / 8.047e6);
  const double Bp = 0.7556 + 0.025 * exp(-277100./T);
  return 1.127e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NeV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeV(const double T) {
  const double ST0 = sqrt(T / 2.504);
  const double ST1 = sqrt(T / 8.037e6);
  const double Bp = 0.7593 + 0.0406 * exp(-325500./T);
  return 1.861e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NeIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeIV(const double T) {
  const double ST0 = sqrt(T / 3.332);
  const double ST1 = sqrt(T / 8.696e6);
  const double Bp = 0.7254 + 0.0921 * exp(-304400./T);
  return 8.321e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NeIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeIII(const double T) {
  const double ST0 = sqrt(T / 9.924);
  const double ST1 = sqrt(T / 8.878e6);
  const double Bp = 0.6434 + 0.2205 * exp(-229200./T);
  return 1.773e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NeII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_NeII(const double T) {
  const double ST0 = sqrt(T / 67.39);
  const double ST1 = sqrt(T / 7.563e6);
  const double Bp = 0.3556 + 0.6472 * exp(-159800./T);
  return 1.295e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* NeIX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeIX(const double T) {
  return pow(T, -1.5) * (0.01552 * exp(-7.845e6/T) + 0.09008 * exp(-9.803e6/T) + 0.01182 * exp(-1.209e7/T));
}

/* NeVIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeVIII(const double T) {
  return pow(T, -1.5) * (0.0002399 * exp(-25360./T) + 0.0003532 * exp(-48000./T) + 0.008928 * exp(-177000./T) + 0.005427 * exp(-1.03e6/T) + 0.005342 * exp(-1.859e6/T) + 0.03981 * exp(-9.743e6/T));
}

/* NeVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeVII(const double T) {
  return pow(T, -1.5) * (8.46e-5 * exp(-1049./T) + 0.0001817 * exp(-3829./T) + 0.001176 * exp(-61330./T) + 0.01397 * exp(-256800./T) + 0.005566 * exp(-460000./T) + 0.006229 * exp(-1.324e6/T) + 0.01031 * exp(-9.353e6/T));
}

/* NeVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeVI(const double T) {
  return pow(T, -1.5) * (5.653e-6 * exp(-628./T) + 4.344e-5 * exp(-2812./T) + 0.0001086 * exp(-13240./T) + 0.000598 * exp(-80640./T) + 0.01457 * exp(-305200./T) + 0.01601 * exp(-1.032e6/T) + 0.0005365 * exp(-2.388e6/T));
}

/* NeV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeV(const double T) {
  return pow(T, -1.5) * (2.922e-6 * exp(-205./T) + 7.144e-6 * exp(-2205./T) + 2.836e-5 * exp(-9271./T) + 9.82e-5 * exp(-49880./T) + 0.008379 * exp(-290400./T) + 0.01009 * exp(-878200./T));
}

/* NeIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeIV(const double T) {
  return pow(T, -1.5) * (2.763e-6 * exp(-639.3/T) + 1.053e-5 * exp(-1499./T) + 4.453e-5 * exp(-32270./T) + 0.006244 * exp(-256100./T) + 0.0003146 * exp(-450500./T) + 0.004465 * exp(-793400./T));
}

/* NeIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeIII(const double T) {
  return pow(T, -1.5) * (2.98e-8 * exp(-45.79/T) + 1.257e-7 * exp(-475.3/T) + 1.122e-6 * exp(-14810./T) + 0.002626 * exp(-281000./T) + 0.0008802 * exp(-476300./T) + 1.231 * exp(-4.677e8/T));
}

/* NeII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_NeII(const double T) {
  return pow(T, -1.5) * (4.152e-9 * exp(-26.89/T) + 4.656e-9 * exp(-202.1/T) + 1.31e-8 * exp(-720./T) + 1.417e-9 * exp(-48920./T) + 0.0007968 * exp(-314400./T) + 1.271e-5 * exp(-673800./T));
}

/* NeI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_NeI(const double T) {
  const double U = NeI_K / T;
  return 1.5e-8 * (1. + sqrt(U)) * pow(U, 0.43) * exp(-U) / (0.0329 + U);
}

/* NeII collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_NeII(const double T) {
  const double U = NeII_K / T;
  return 1.98e-8 * pow(U, 0.2) * exp(-U) / (0.295 + U);
}

/* NeIII collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_NeIII(const double T) {
  const double U = NeIII_K / T;
  return 7.03e-9 * (1. + sqrt(U)) * pow(U, 0.39) * exp(-U) / (0.0677 + U);
}

/* NeIV collisional ionization rates over [5 eV, 20 keV]. */
static inline double beta_NeIV(const double T) {
  const double U = NeIV_K / T;
  return 4.24e-9 * (1. + sqrt(U)) * pow(U, 0.58) * exp(-U) / (0.0482 + U);
}

/* NeV collisional ionization rates over [7 eV, 20 keV]. */
static inline double beta_NeV(const double T) {
  const double U = NeV_K / T;
  return 2.79e-9 * (1. + sqrt(U)) * pow(U, 0.25) * exp(-U) / (0.305 + U);
}

/* NeVI collisional ionization rates over [7 eV, 20 keV]. */
static inline double beta_NeVI(const double T) {
  const double U = NeVI_K / T;
  return 3.45e-9 * pow(U, 0.28) * exp(-U) / (0.581 + U);
}

/* NeVII collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_NeVII(const double T) {
  const double U = NeVII_K / T;
  return 9.56e-10 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (0.749 + U);
}

/* NeVIII collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_NeVIII(const double T) {
  const double U = NeVIII_K / T;
  return 4.73e-10 * (1. + sqrt(U)) * pow(U, 0.04) * exp(-U) / (0.992 + U);
}

/* NeV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NeV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 6.47e-09 * pow(T4, 0.54) * (1. + 3.59 * exp(-5.22 * T4));
}

/* NeIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NeIV(const double T) {
  const double T4 = (T < 5e3) ? 0.5 : ((T > 5e4) ? 5. : 1e-4 * T);
  return 1.473e-8 * pow(T4, 0.0452) * (1. - 0.84 * exp(-0.31 * T4));
}

/* NeIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_NeIII([[maybe_unused]] const double T) {
  return 1e-14;
}

/* NeV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_NeV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 5.)
    return 1.77e-09 * pow(T4, 0.14) * (1. + 0.0488 * exp(-3.35 * T4));
  return 2.67e-10 * pow(T4, 0.54) * (1. + 0.91 * exp(-0.0188 * T4));
}

/* NeIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_NeIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 2.5)
    return 1e-14;
  if (T4 < 9.5)
    return 1.34e-13 * pow(T4, 2.33) * (1. - 2.55 * exp(-0.37 * T4));
  return 1e-10 * pow(T4, 0.24) * (1. - 1.09 * exp(-0.0247 * T4));
}

/* NeIII + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_NeIII(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 0.5)
    return 1e-14;
  if (T4 < 0.8)
    return 8.48e-12 * pow(T4, 3.35) * (1. - 1.92 * exp(-1.5 * T4));
  return 2.52e-11 * pow(T4, 0.14) * (1. - 1.99 * exp(-0.91 * T4));
}

/*************/
/* Magnesium */
/*************/

/* MgXI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgXI(const double T) {
  const double ST0 = sqrt(T / 4944.);
  const double ST1 = sqrt(T / 4.434e7);
  return 1.602e-10 / (ST0 * pow(1. + ST0, 0.4508) * pow(1. + ST1, 1.5492));
}

/* MgX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgX(const double T) {
  const double ST0 = sqrt(T / 869.3);
  const double ST1 = sqrt(T / 2.196e7);
  return 3.445e-10 / (ST0 * pow(1. + ST0, 0.3447) * pow(1. + ST1, 1.6553));
}

/* MgIX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgIX(const double T) {
  const double ST0 = sqrt(T / 38.06);
  const double ST1 = sqrt(T / 1.205e7);
  return 1.634e-9 / (ST0 * pow(1. + ST0, 0.2565) * pow(1. + ST1, 1.7435));
}

/* MgVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgVIII(const double T) {
  const double ST0 = sqrt(T / 5.587);
  const double ST1 = sqrt(T / 1.235e7);
  return 3.859e-9 / (ST0 * pow(1. + ST0, 0.2421) * pow(1. + ST1, 1.7579));
}

/* MgVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgVII(const double T) {
  const double ST0 = sqrt(T / 36.74);
  const double ST1 = sqrt(T / 1.263e7);
  const double Bp = 0.7353 + 0.0211 * exp(-404900./T);
  return 9.133e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* MgVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgVI(const double T) {
  const double ST0 = sqrt(T / 25.82);
  const double ST1 = sqrt(T / 1.355e7);
  const double Bp = 0.7203 + 0.0436 * exp(-569100./T);
  return 7.515e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* MgV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgV(const double T) {
  const double ST0 = sqrt(T / 32.05);
  const double ST1 = sqrt(T / 1.626e7);
  const double Bp = 0.6803 + 0.0764 * exp(-539900./T);
  return 4.031e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* MgIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgIV(const double T) {
  const double ST0 = sqrt(T / 77.48);
  const double ST1 = sqrt(T / 2.015e7);
  const double Bp = 0.56 + 0.1917 * exp(-513900./T);
  return 1.249e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* MgIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgIII(const double T) {
  const double ST0 = sqrt(T / 787.7);
  const double ST1 = sqrt(T / 7.925e7);
  const double Bp = 0.1074 + 0.4631 * exp(-502700./T);
  return 1.345e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* MgII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_MgII(const double T) {
  const double ST0 = sqrt(T / 5.637);
  const double ST1 = sqrt(T / 1.551e6);
  const double Bp = 0.6845 + 0.3945 * exp(-836000./T);
  return 5.452e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* MgXI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgXI(const double T) {
  return pow(T, -1.5) * (0.03067 * exp(-1.136e7/T) + 0.1375 * exp(-1.431e7/T) + 0.01347 * exp(-1.762e7/T));
}

/* MgX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgX(const double T) {
  return pow(T, -1.5) * (0.0002582 * exp(-28610./T) + 0.0005033 * exp(-40870./T) + 0.004561 * exp(-152800./T) + 0.008754 * exp(-267700./T) + 0.02734 * exp(-2.055e6/T) + 0.07509 * exp(-1.298e7/T) + 0.01385 * exp(-1.852e7/T));
}

/* MgIX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgIX(const double T) {
  return pow(T, -1.5) * (3.565e-5 * exp(-1759./T) + 2.017e-5 * exp(-12370./T) + 0.001165 * exp(-56510./T) + 0.005016 * exp(-168000./T) + 0.02434 * exp(-400700./T) + 0.02508 * exp(-1.991e6/T) + 0.02475 * exp(-1.33e7/T) + 0.0007087 * exp(-4.06e7/T));
}

/* MgVIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgVIII(const double T) {
  return pow(T, -1.5) * (5.451e-5 * exp(-219.6/T) + 6.999e-5 * exp(-4524./T) + 0.0003928 * exp(-24910./T) + 0.0007483 * exp(-91110./T) + 0.01851 * exp(-336700./T) + 0.0119 * exp(-831600./T) + 0.04764 * exp(-1.817e6/T));
}

/* MgVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgVII(const double T) {
  return pow(T, -1.5) * (2.931e-5 * exp(-766.7/T) + 5.192e-5 * exp(-3444./T) + 0.0001769 * exp(-24090./T) + 0.0007988 * exp(-119000./T) + 0.01488 * exp(-399900./T) + 0.04546 * exp(-1.502e6/T) + 0.0001505 * exp(-2.464e7/T));
}

/* MgVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgVI(const double T) {
  return pow(T, -1.5) * (2.407e-5 * exp(-8061./T) + 0.0001187 * exp(-21540./T) + 0.0003845 * exp(-66400./T) + 0.01333 * exp(-340600./T) + 0.02804 * exp(-1.327e6/T) + 0.0008746 * exp(-3.347e6/T));
}

/* MgV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgV(const double T) {
  return pow(T, -1.5) * (2.574e-6 * exp(-112.9/T) + 2.001e-6 * exp(-1585./T) + 1.829e-5 * exp(-11930./T) + 2.362e-5 * exp(-41510./T) + 0.006413 * exp(-368400./T) + 0.004118 * exp(-692500./T) + 0.007224 * exp(-1.15e6/T));
}

/* MgIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgIV(const double T) {
  return pow(T, -1.5) * (9.4e-8 * exp(-266./T) + 3.818e-7 * exp(-1141./T) + 2.064e-7 * exp(-3210./T) + 2.71e-5 * exp(-172700./T) + 0.003802 * exp(-452300./T) + 0.003086 * exp(-910500./T));
}

/* MgIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgIII(const double T) {
  return pow(T, -1.5) * (6.269e-6 * exp(-410400./T) + 0.0009181 * exp(-576600./T) + 0.0003082 * exp(-731000./T));
}

/* MgII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_MgII(const double T) {
  return pow(T, -1.5) * (3.871e-8 * exp(-8415./T) + 4.732e-7 * exp(-16820./T) + 0.001599 * exp(-50000./T) + 2.628e-5 * exp(-275900./T));
}

/* MgI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_MgI(const double T) {
  const double U = MgI_K / T;
  return 6.21e-7 * pow(U, 0.39) * exp(-U) / (0.592 + U);
}

/* MgII collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_MgII(const double T) {
  const double U = MgII_K / T;
  return 1.92e-8 * pow(U, 0.85) * exp(-U) / (0.0027 + U);
}

/* MgIII collisional ionization rates over [4 eV, 20 keV]. */
static inline double beta_MgIII(const double T) {
  const double U = MgIII_K / T;
  return 5.56e-9 * (1. + sqrt(U)) * pow(U, 0.3) * exp(-U) / (0.107 + U);
}

/* MgIV collisional ionization rates over [5 eV, 20 keV]. */
static inline double beta_MgIV(const double T) {
  const double U = MgIV_K / T;
  return 4.35e-9 * (1. + sqrt(U)) * pow(U, 0.31) * exp(-U) / (0.159 + U);
}

/* MgV collisional ionization rates over [7 eV, 20 keV]. */
static inline double beta_MgV(const double T) {
  const double U = MgV_K / T;
  return 7.1e-9 * pow(U, 0.25) * exp(-U) / (0.658 + U);
}

/* MgVI collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_MgVI(const double T) {
  const double U = MgVI_K / T;
  return 1.7e-9 * (1. + sqrt(U)) * pow(U, 0.28) * exp(-U) / (0.242 + U);
}

/* MgVII collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_MgVII(const double T) {
  const double U = MgVII_K / T;
  return 1.22e-9 * (1. + sqrt(U)) * pow(U, 0.23) * exp(-U) / (0.343 + U);
}

/* MgVIII collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_MgVIII(const double T) {
  const double U = MgVIII_K / T;
  return 2.2e-9 * pow(U, 0.22) * exp(-U) / (0.897 + U);
}

/* MgIX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_MgIX(const double T) {
  const double U = MgIX_K / T;
  return 4.86e-10 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (0.751 + U);
}

/* MgX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_MgX(const double T) {
  const double U = MgX_K / T;
  return 2.35e-10 * (1. + sqrt(U)) * pow(U, 0.1) * exp(-U) / (1.03 + U);
}

/* MgV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_MgV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 6.36e-09 * pow(T4, 0.55) * (1. + 3.86 * exp(-5.19 * T4));
}

/* MgIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_MgIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 6.49e-09 * pow(T4, 0.53) * (1. + 2.82 * exp(-7.63 * T4));
}

/* MgIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_MgIII(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 8.58e-14 * pow(T4, 0.00249) * (1. + 0.0293 * exp(-4.33 * T4));
}

/* MgV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_MgV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  return 1.37e-09 * pow(T4, 0.21) * (1. - 0.59 * exp(-0.0594 * T4));
}

/* MgIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_MgIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  if (T4 < 0.7)
    return 1.88e-11 * pow(T4, 0.13) * (1. + 0.83 * exp(-4.94 * T4));
  return 3.41e-11 * pow(T4, 0.15) * (1. - 0.45 * exp(-0.0483 * T4));
}

/* MgII + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_MgII(const double T) {
  const double T4 = (T < 1e4) ? 1. : ((T > 3e5) ? 30. : 1e-4 * T);
  return 7.6e-14 * (1. - 1.97 * exp(-4.32 * T4)) * exp(-1.67/T4);
}

/* MgI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_MgI(const double T) {
  const double T4 = (T < 5e3) ? 0.5 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 9.76e-12 * pow(T4, 3.14) * (1. + 55.54 * exp(-1.12 * T4));
}

/***********/
/* Silicon */
/***********/

/* SiXIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiXIII(const double T) {
  const double ST0 = sqrt(T / 6494.);
  const double ST1 = sqrt(T / 5.693e7);
  return 2.017e-10 / (ST0 * pow(1. + ST0, 0.4412) * pow(1. + ST1, 1.5588));
}

/* SiXII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiXII(const double T) {
  const double ST0 = sqrt(T / 1088.);
  const double ST1 = sqrt(T / 2.896e7);
  return 4.633e-10 / (ST0 * pow(1. + ST0, 0.3398) * pow(1. + ST1, 1.6602));
}

/* SiXI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiXI(const double T) {
  const double ST0 = sqrt(T / 69.06);
  const double ST1 = sqrt(T / 1.644e7);
  return 1.851e-9 / (ST0 * pow(1. + ST0, 0.2616) * pow(1. + ST1, 1.7384));
}

/* SiX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiX(const double T) {
  const double ST0 = sqrt(T / 55.49);
  const double ST1 = sqrt(T / 1.716e7);
  return 1.688e-9 / (ST0 * pow(1. + ST0, 0.261) * pow(1. + ST1, 1.739));
}

/* SiIX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiIX(const double T) {
  const double ST0 = sqrt(T / 25.23);
  const double ST1 = sqrt(T / 1.842e7);
  return 2.1e-9 / (ST0 * pow(1. + ST0, 0.2599) * pow(1. + ST1, 1.7401));
}

/* SiVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiVIII(const double T) {
  const double ST0 = sqrt(T / 88.6);
  const double ST1 = sqrt(T / 1.997e7);
  const double Bp = 0.7072 + 0.0185 * exp(-694900./T);
  return 7.532e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiVII(const double T) {
  const double ST0 = sqrt(T / 114.3);
  const double ST1 = sqrt(T / 2.377e7);
  const double Bp = 0.6753 + 0.0356 * exp(-859500./T);
  return 4.615e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiVI(const double T) {
  const double ST0 = sqrt(T / 164.9);
  const double ST1 = sqrt(T / 3.231e7);
  const double Bp = 0.6113 + 0.0636 * exp(-983700./T);
  return 2.468e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiV(const double T) {
  const double ST0 = sqrt(T / 1009);
  const double ST1 = sqrt(T / 8.514e7);
  const double Bp = 0.3678 + 0.1646 * exp(-1.084e6/T);
  return 5.134e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiIV(const double T) {
  const double ST0 = sqrt(T / 216.6);
  const double ST1 = sqrt(T / 4.491e7);
  const double Bp = 0.4931 + 0.1667 * exp(-904600./T);
  return 6.739e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiIII(const double T) {
  const double ST0 = sqrt(T / 7.712);
  const double ST1 = sqrt(T / 2.951e7);
  const double Bp = 0.6287 + 0.1523 * exp(-480400./T);
  return 1.964e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SiII(const double T) {
  const double ST0 = sqrt(T / 15.9);
  const double ST1 = sqrt(T / 4.237e7);
  const double Bp = 0.627 + 0.2333 * exp(-58280./T);
  return 3.262e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SiXIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiXIII(const double T) {
  return pow(T, -1.5) * (0.05318 * exp(-1.552e7/T) + 0.1874 * exp(-1.969e7/T) + 0.01227 * exp(-2.532e7/T) + 0.0007173 * exp(-2.696e8/T));
}

/* SiXII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiXII(const double T) {
  return pow(T, -1.5) * (0.001205 * exp(-48240./T) + 0.01309 * exp(-213700./T) + 0.005333 * exp(-549200./T) + 0.02858 * exp(-2.46e6/T) + 0.03195 * exp(-3.647e6/T) + 0.1433 * exp(-1.889e7/T));
}

/* SiXI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiXI(const double T) {
  return pow(T, -1.5) * (0.0002214 * exp(-3586./T) + 0.00126 * exp(-13240./T) + 0.003832 * exp(-56360./T) + 0.01158 * exp(-249800./T) + 0.02871 * exp(-536300./T) + 0.05456 * exp(-2.809e6/T) + 0.05035 * exp(-1.854e7/T));
}

/* SiX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiX(const double T) {
  return pow(T, -1.5) * (0.0001246 * exp(-1167./T) + 0.0006649 * exp(-9088./T) + 0.002912 * exp(-53320./T) + 0.02912 * exp(-421900./T) + 0.03049 * exp(-1.571e6/T) + 0.1056 * exp(-2.721e6/T));
}

/* SiIX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiIX(const double T) {
  return pow(T, -1.5) * (0.0005845 * exp(-985.6/T) + 0.00066 * exp(-6577./T) + 0.000718 * exp(-42810./T) + 0.01714 * exp(-394400./T) + 0.02958 * exp(-1.296e6/T) + 0.1075 * exp(-2.441e6/T));
}

/* SiVIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiVIII(const double T) {
  return pow(T, -1.5) * (5.272e-5 * exp(-2829./T) + 0.0002282 * exp(-26170./T) + 0.001345 * exp(-137400./T) + 0.02246 * exp(-452000./T) + 0.09606 * exp(-2.072e6/T) + 0.0008366 * exp(-7.808e6/T));
}

/* SiVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiVII(const double T) {
  return pow(T, -1.5) * (2.086e-6 * exp(-370.8/T) + 9.423e-6 * exp(-3870./T) + 3.423e-5 * exp(-22260./T) + 0.000395 * exp(-131800./T) + 0.01535 * exp(-528500./T) + 0.04986 * exp(-1.742e6/T) + 0.0004067 * exp(-8.392e6/T));
}

/* SiVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiVI(const double T) {
  return pow(T, -1.5) * (7.163e-7 * exp(-562.5/T) + 2.656e-6 * exp(-2952./T) + 1.119e-6 * exp(-9682./T) + 4.796e-5 * exp(-147300./T) + 0.004052 * exp(-506400./T) + 0.006101 * exp(-804700./T) + 0.02366 * exp(-1.623e6/T));
}

/* SiV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiV(const double T) {
  return pow(T, -1.5) * (0.0001422 * exp(-768500./T) + 0.009474 * exp(-1.208e6/T) + 0.00165 * exp(-1.839e6/T));
}

/* SiIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiIV(const double T) {
  return pow(T, -1.5) * (3.819e-6 * exp(-3802./T) + 2.421e-5 * exp(-12800./T) + 0.0002283 * exp(-59530./T) + 0.008604 * exp(-102600./T) + 0.002617 * exp(-1.154e6/T));
}

/* SiIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiIII(const double T) {
  return pow(T, -1.5) * (2.93e-6 * exp(-116.2/T) + 2.803e-6 * exp(-5721./T) + 9.023e-5 * exp(-34770./T) + 0.006909 * exp(-117600./T) + 2.582e-5 * exp(-3.505e6/T));
}

/* SiII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SiII(const double T) {
  return pow(T, -1.5) * (3.408e-8 * exp(-24.31/T) + 1.913e-7 * exp(-129.3/T) + 1.679e-7 * exp(-427.2/T) + 7.523e-7 * exp(-3729./T) + 8.386e-5 * exp(-55140./T) + 0.004083 * exp(-129500./T));
}

/* SiI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_SiI(const double T) {
  const double U = SiI_K / T;
  return 1.88e-7 * (1. + sqrt(U)) * pow(U, 0.25) * exp(-U) / (0.376 + U);
}

/* SiII collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_SiII(const double T) {
  const double U = SiII_K / T;
  return 6.43e-8 * (1. + sqrt(U)) * pow(U, 0.2) * exp(-U) / (0.632 + U);
}

/* SiIII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_SiIII(const double T) {
  const double U = SiIII_K / T;
  return 2.01e-8 * (1. + sqrt(U)) * pow(U, 0.22) * exp(-U) / (0.473 + U);
}

/* SiIV collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_SiIV(const double T) {
  const double U = SiIV_K / T;
  return 4.94e-9 * (1. + sqrt(U)) * pow(U, 0.23) * exp(-U) / (0.172 + U);
}

/* SiV collisional ionization rates over [7 eV, 20 keV]. */
static inline double beta_SiV(const double T) {
  const double U = SiV_K / T;
  return 1.76e-9 * (1. + sqrt(U)) * pow(U, 0.31) * exp(-U) / (0.102 + U);
}

/* SiVI collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_SiVI(const double T) {
  const double U = SiVI_K / T;
  return 1.74e-9 * (1. + sqrt(U)) * pow(U, 0.29) * exp(-U) / (0.18 + U);
}

/* SiVII collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_SiVII(const double T) {
  const double U = SiVII_K / T;
  return 1.23e-9 * (1. + sqrt(U)) * pow(U, 0.07) * exp(-U) / (0.518 + U);
}

/* SiVIII collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SiVIII(const double T) {
  const double U = SiVIII_K / T;
  return 8.27e-10 * (1. + sqrt(U)) * pow(U, 0.28) * exp(-U) / (0.239 + U);
}

/* SiIX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SiIX(const double T) {
  const double U = SiIX_K / T;
  return 6.01e-10 * (1. + sqrt(U)) * pow(U, 0.25) * exp(-U) / (0.305 + U);
}

/* SiX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SiX(const double T) {
  const double U = SiX_K / T;
  return 4.65e-10 * (1. + sqrt(U)) * pow(U, 0.04) * exp(-U) / (0.666 + U);
}

/* SiXI collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SiXI(const double T) {
  const double U = SiXI_K / T;
  return 2.63e-10 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.666 + U);
}

/* SiXII collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_SiXII(const double T) {
  const double U = SiXII_K / T;
  return 1.18e-10 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.734 + U);
}

/* SiV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SiV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 5e4) ? 5. : 1e-4 * T);
  return 7.58e-9 * pow(T4, 0.37) * (1. + 1.06 * exp(-4.09 * T4));
}

/* SiIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SiIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 4.9e-10 * pow(T4, -0.0874) * (1. - 0.36 * exp(-0.79 * T4));
}

/* SiIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SiIII(const double T) {
  const double T4 = (T < 500.) ? 0.05 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 6.77e-9 * pow(T4, 0.0736) * (1. - 0.43 * exp(-0.11 * T4));
}

/* SiV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_SiV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 5e5) ? 50. : 1e-4 * T);
  return 5.75e-10 * pow(T4, 0.93) * (1. + 1.33 * exp(-0.29 * T4));
}

/* SiIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_SiIV(const double T) {
  const double T4 = (T < 100) ? 0.01 : ((T > 1e6) ? 100. : 1e-4 * T);
  return 1.03e-9 * pow(T4, 0.6) * (1. - 0.61 * exp(-1.42 * T4));
}

/* SiII + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_SiII(const double T) {
  const double T4 = (T < 2e3) ? 0.2 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 2.26e-9 * pow(T4, 0.0736) * (1. - 0.43 * exp(-0.11 * T4)) * exp(-3.031/T4);
}

/* SiI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_SiI(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 2e5) ? 20. : 1e-4 * T);
  return 9.2e-10 * pow(T4, 1.15) * (1. + 0.8 * exp(-0.24 * T4));
}

/* SiIII + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_SiIII(const double T) {
  return 1.15e-11 * sqrt(T) * exp(-8.88*eV/(kB*T));
}

/* SiII + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_SiII(const double T) {
  return 1.5e-11 * pow(T, 0.25) * exp(-6.91*eV/(kB*T));
}

/* SiI + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_SiI([[maybe_unused]] const double T) {
  return 1.3e-9;
}

/**********/
/* Sulfer */
/**********/

/* SXV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SXV(const double T) {
  const double ST0 = sqrt(T / 8776.);
  const double ST1 = sqrt(T / 7.208e7);
  return 2.362e-10 / (ST0 * pow(1. + ST0, 0.4385) * pow(1. + ST1, 1.5615));
}

/* SXIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SXIV(const double T) {
  const double ST0 = sqrt(T / 1492.);
  const double ST1 = sqrt(T / 3.755e7);
  return 5.511e-10 / (ST0 * pow(1. + ST0, 0.3402) * pow(1. + ST1, 1.6598));
}

/* SXIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SXIII(const double T) {
  const double ST0 = sqrt(T / 149.4);
  const double ST1 = sqrt(T / 2.193e7);
  return 1.74e-9 / (ST0 * pow(1. + ST0, 0.2697) * pow(1. + ST1, 1.7303));
}

/* SXII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SXII(const double T) {
  const double ST0 = sqrt(T / 113.8);
  const double ST1 = sqrt(T / 2.253e7);
  return 1.702e-9 / (ST0 * pow(1. + ST0, 0.2699) * pow(1. + ST1, 1.7301));
}

/* SXI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SXI(const double T) {
  const double ST0 = sqrt(T / 98.45);
  const double ST1 = sqrt(T / 2.39e7);
  return 1.518e-9 / (ST0 * pow(1. + ST0, 0.2754) * pow(1. + ST1, 1.7246));
}

/* SX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SX(const double T) {
  const double ST0 = sqrt(T / 109.9);
  const double ST1 = sqrt(T / 2.745e7);
  return 1.137e-9 / (ST0 * pow(1. + ST0, 0.292) * pow(1. + ST1, 1.708));
}

/* SIX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SIX(const double T) {
  const double ST0 = sqrt(T / 111.5);
  const double ST1 = sqrt(T / 3.386e7);
  return 8.773e-10 / (ST0 * pow(1. + ST0, 0.3147) * pow(1. + ST1, 1.6853));
}

/* SVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SVIII(const double T) {
  const double ST0 = sqrt(T / 338.);
  const double ST1 = sqrt(T / 4.347e7);
  const double Bp = 0.6175 + 0.0225 * exp(-1.672e6/T);
  return 3.384e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SVII(const double T) {
  const double ST0 = sqrt(T / 1599.);
  const double ST1 = sqrt(T / 9.252e7);
  const double Bp = 0.4517 + 0.0612 * exp(-1.986e6/T);
  return 9.565e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SVI(const double T) {
  const double ST0 = sqrt(T / 335.);
  const double ST1 = sqrt(T / 5.188e7);
  const double Bp = 0.5584 + 0.0591 * exp(-1.656e6/T);
  return 1.588e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SV(const double T) {
  const double ST0 = sqrt(T / 62.38);
  const double ST1 = sqrt(T / 2.803e7);
  const double Bp = 0.6343 + 0.0773 * exp(-1.059e6/T);
  return 2.615e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SIV(const double T) {
  const double ST0 = sqrt(T / 16.78);
  const double ST1 = sqrt(T / 2.05e7);
  const double Bp = 0.6947 + 0.0795 * exp(-68680./T);
  return 3.043e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SIII(const double T) {
  const double ST0 = sqrt(T / 329.4);
  const double ST1 = sqrt(T / 2.166e7);
  const double Bp = 0.4642 + 0.3351 * exp(-763000./T);
  return 2.478e-11 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* SII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_SII(const double T) {
  return 4.1e-13 * pow(1e-4 * T, -0.63);
}

/* SXV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SXV(const double T) {
  return pow(T, -1.5) * (0.0841 * exp(-2.032e7/T) + 0.2381 * exp(-2.592e7/T) + 0.01065 * exp(-3.206e7/T) + 0.001049 * exp(-2.016e8/T));
}

/* SXIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SXIV(const double T) {
  return pow(T, -1.5) * (0.001173 * exp(-13660./T) + 0.001718 * exp(-63710./T) + 0.01657 * exp(-254000./T) + 0.007474 * exp(-649300./T) + 0.09698 * exp(-3.868e6/T) + 0.1399 * exp(-2.186e7/T) + 0.07029 * exp(-2.983e7/T));
}

/* SXIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SXIII(const double T) {
  return pow(T, -1.5) * (0.000261 * exp(-5114./T) + 0.0009442 * exp(-17500./T) + 0.00819 * exp(-106100./T) + 0.04271 * exp(-469100./T) + 0.01997 * exp(-1.821e6/T) + 0.0959 * exp(-4.033e6/T) + 0.08444 * exp(-2.476e7/T));
}

/* SXII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SXII(const double T) {
  return pow(T, -1.5) * (9.126e-5 * exp(-3575./T) + 0.0001691 * exp(-14910./T) + 0.00305 * exp(-93880./T) + 0.02604 * exp(-385600./T) + 0.03245 * exp(-963000./T) + 0.2511 * exp(-3.49e6/T) + 0.0004459 * exp(-1.743e8/T));
}

/* SXI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SXI(const double T) {
  return pow(T, -1.5) * (0.0001371 * exp(-1505./T) + 0.0003096 * exp(-11000./T) + 0.001782 * exp(-67200./T) + 0.01278 * exp(-336400./T) + 0.03323 * exp(-968600./T) + 0.2593 * exp(-3.195e6/T));
}

/* SX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SX(const double T) {
  return pow(T, -1.5) * (0.000183 * exp(-11220./T) + 0.001551 * exp(-31210./T) + 0.001717 * exp(-93480./T) + 0.02798 * exp(-479200./T) + 0.06933 * exp(-2.052e6/T) + 0.1727 * exp(-3.291e6/T));
}

/* SIX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SIX(const double T) {
  return pow(T, -1.5) * (2.924e-5 * exp(-719.2/T) + 0.0003366 * exp(-7490./T) + 0.0002104 * exp(-45100./T) + 0.0191 * exp(-558600./T) - 0.0004017 * exp(-383000./T) + 0.06541 * exp(-1.976e6/T) + 0.09546 * exp(-2.894e6/T));
}

/* SVIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SVIII(const double T) {
  return pow(T, -1.5) * (2.585e-6 * exp(-1277./T) + 9.517e-6 * exp(-5978./T) + 5.194e-6 * exp(-20970./T) + 0.0003715 * exp(-229200./T) + 0.01553 * exp(-797400./T) + 0.1013 * exp(-2.376e6/T));
}

/* SVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SVII(const double T) {
  return pow(T, -1.5) * (3.931e-5 * exp(-945500./T) + 0.004431 * exp(-1.365e6/T) + 0.05156 * exp(-2.169e6/T));
}

/* SVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SVI(const double T) {
  return pow(T, -1.5) * (2.816e-6 * exp(-7590./T) + 3.172e-5 * exp(-15580./T) + 0.0001832 * exp(-40130./T) + 0.00436 * exp(-115600./T) + 0.01618 * exp(-160100./T) + 0.007707 * exp(-1.839e6/T));
}

/* SV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SV(const double T) {
  return pow(T, -1.5) * (9.571e-6 * exp(-1180./T) + 6.268e-5 * exp(-6443./T) + 0.0003807 * exp(-22640./T) + 0.01874 * exp(-153000./T) + 0.005526 * exp(-356400./T));
}

/* SIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SIV(const double T) {
  return pow(T, -1.5) * (5.817e-7 * exp(-362.8/T) + 1.391e-6 * exp(-1058./T) + 1.123e-5 * exp(-7160./T) + 0.0001521 * exp(-32600./T) + 0.001875 * exp(-123500./T) + 0.02097 * exp(-207000./T));
}

/* SIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SIII(const double T) {
  return pow(T, -1.5) * (3.04e-7 * exp(-50.16/T) + 4.393e-7 * exp(-326.6/T) + 1.609e-6 * exp(-3102./T) + 4.98e-6 * exp(-12100./T) + 3.457e-5 * exp(-49690./T) + 0.008617 * exp(-201000./T) + 0.0009284 * exp(-257500./T));
}

/* SII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_SII(const double T) {
  return 0.0017126 * pow(T, -1.5) * exp(-173490./T);
}

/* SI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_SI(const double T) {
  const double U = SI_K / T;
  return 5.49e-8 * (1. + sqrt(U)) * pow(U, 0.25) * exp(-U) / (0.1 + U);
}

/* SII collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_SII(const double T) {
  const double U = SII_K / T;
  return 6.81e-8 * (1. + sqrt(U)) * pow(U, 0.21) * exp(-U) / (0.693 + U);
}

/* SIII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_SIII(const double T) {
  const double U = SIII_K / T;
  return 2.14e-8 * (1. + sqrt(U)) * pow(U, 0.24) * exp(-U) / (0.353 + U);
}

/* SIV collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_SIV(const double T) {
  const double U = SIV_K / T;
  return 1.66e-8 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (1.03 + U);
}

/* SV collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_SV(const double T) {
  const double U = SV_K / T;
  return 6.12e-9 * (1. + sqrt(U)) * pow(U, 0.19) * exp(-U) / (0.58 + U);
}

/* SVI collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_SVI(const double T) {
  const double U = SVI_K / T;
  return 1.33e-9 * (1. + sqrt(U)) * pow(U, 0.35) * exp(-U) / (0.0688 + U);
}

/* SVII collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SVII(const double T) {
  const double U = SVII_K / T;
  return 4.93e-9 * pow(U, 0.16) * exp(-U) / (1.13 + U);
}

/* SVIII collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SVIII(const double T) {
  const double U = SVIII_K / T;
  return 8.73e-10 * (1. + sqrt(U)) * pow(U, 0.28) * exp(-U) / (0.193 + U);
}

/* SIX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SIX(const double T) {
  const double U = SIX_K / T;
  return 1.35e-9 * pow(U, 0.32) * exp(-U) / (0.431 + U);
}

/* SX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_SX(const double T) {
  const double U = SX_K / T;
  return 4.59e-10 * (1. + sqrt(U)) * pow(U, 0.28) * exp(-U) / (0.242 + U);
}

/* SXI collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_SXI(const double T) {
  const double U = SXI_K / T;
  return 3.49e-10 * (1. + sqrt(U)) * pow(U, 0.25) * exp(-U) / (0.305 + U);
}

/* SXII collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_SXII(const double T) {
  const double U = SXII_K / T;
  return 5.23e-10 * pow(U, 0.35) * exp(-U) / (0.428 + U);
}

/* SXIII collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_SXIII(const double T) {
  const double U = SXIII_K / T;
  return 2.59e-10 * pow(U, 0.12) * exp(-U) / (0.854 + U);
}

/* SXIV collisional ionization rates over [30 eV, 20 keV]. */
static inline double beta_SXIV(const double T) {
  const double U = SXIV_K / T;
  return 7.5e-11 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.734 + U);
}

/* SV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 6.44e-9 * pow(T4, 0.13) * (1. + 2.69 * exp(-5.69 * T4));
}

/* SIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 2.29e-9 * pow(T4, 0.0402) * (1. + 1.59 * exp(-6.06 * T4));
}

/* SIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SIII([[maybe_unused]] const double T) {
  return 1e-14;
}

/* SII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_SII(const double T) {
  const double T4 = (T < 3e3) ? 0.3 : ((T > 1e4) ? 1. : 1e-4 * T);
  return 3.82e-16 * pow(T4, 11.1) * (1. + 25700 * exp(-8.22 * T4));
}

/* SV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_SV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3.1e4) ? 3.1 : 1e-4 * T);
  return 7.44e-13 * pow(T4, 0.34) * (1. + 3.74 * exp(-5.18 * T4));
}

/* SIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_SIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3.1e4) ? 3.1 : 1e-4 * T);
  return 3.58e-9 * pow(T4, 0.00777) * (1. - 0.94 * exp(-0.3 * T4));
}

/* SI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_SI([[maybe_unused]] const double T) {
  return 1e-14;
}

/* SIII + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_SIII(const double T) {
  return 5.5e-18 * pow(T, 1.6) * exp(-4.6e-6*T) * exp(-10.5*eV/(kB*T));
}

/* SII + HeII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HeII_SII(const double T) {
  return 4.4e-16 * pow(T, 1.2) * exp(-3.6e-6*T) * exp(-9.2*eV/(kB*T));
}

/********/
/* Iron */
/********/

/* FeXVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXVII(const double T) {
  const double ST0 = sqrt(T / 17510.);
  const double ST1 = sqrt(T / 1.579e8);
  return 2.034e-10 / (ST0 * pow(1. + ST0, 0.5452) * pow(1. + ST1, 1.4548));
}

/* FeXVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXVI(const double T) {
  const double ST0 = sqrt(T / 6295.);
  const double ST1 = sqrt(T / 9.035e7);
  return 3.133e-10 / (ST0 * pow(1. + ST0, 0.4493) * pow(1. + ST1, 1.5507));
}

/* FeXV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXV(const double T) {
  const double ST0 = sqrt(T / 1881.);
  const double ST1 = sqrt(T / 5.429e7);
  return 5.398e-10 / (ST0 * pow(1. + ST0, 0.3705) * pow(1. + ST1, 1.6295));
}

/* FeXIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXIV(const double T) {
  const double ST0 = sqrt(T / 1531.);
  const double ST1 = sqrt(T / 4.632e7);
  const double Bp = 0.6338 + 0.0278 * exp(-90700./T);
  return 5.37e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* FeXIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXIII(const double T) {
  const double ST0 = sqrt(T / 115.8);
  const double ST1 = sqrt(T / 4.4e7);
  return 1.984e-9 / (ST0 * pow(1. + ST0, 0.2899) * pow(1. + ST1, 1.7101));
}

/* FeXII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXII(const double T) {
  const double ST0 = sqrt(T / 353.1);
  const double ST1 = sqrt(T / 3.554e7);
  const double Bp = 0.7156 + 0.0132 * exp(-295100./T);
  return 8.303e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* FeXI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeXI(const double T) {
  const double ST0 = sqrt(T / 163.9);
  const double ST1 = sqrt(T / 2.924e7);
  const double Bp = 0.737 + 0.0224 * exp(-429100./T);
  return 1.052e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* FeX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeX(const double T) {
  const double ST0 = sqrt(T / 72.42);
  const double ST1 = sqrt(T / 2.453e7);
  const double Bp = 0.7495 + 0.0404 * exp(-419900./T);
  return 1.338e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* FeIX total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeIX(const double T) {
  const double ST0 = sqrt(T / 1891.);
  const double ST1 = sqrt(T / 5.181e7);
  const double Bp = 0.4865 + 0.0575 * exp(-27340./T);
  return 3.341e-10 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* FeVIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeVIII(const double T) {
  const double ST0 = sqrt(T / 37.53);
  const double ST1 = sqrt(T / 2.418e7);
  const double Bp = 0.7407 + 0.072 * exp(-328000./T);
  return 1.121e-9 / (ST0 * pow(1. + ST0, 1.-Bp) * pow(1. + ST1, 1.+Bp));
}

/* FeVII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeVII(const double T) {
  return 2.62e-11 * pow(1e-4 * T, -0.728);
}

/* FeVI total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeVI(const double T) {
  return 1.51e-11 * pow(1e-4 * T, -0.699);
}

/* FeV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeV(const double T) {
  return 7.8e-12 * pow(1e-4 * T, -0.682);
}

/* FeIV total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeIV(const double T) {
  return 3.32e-12 * pow(1e-4 * T, -0.746);
}

/* FeIII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeIII(const double T) {
  return 1.02e-12 * pow(1e-4 * T, -0.843);
}

/* FeII total radiative recombination rate coefficient [cm^3/s]. */
static inline double alpha_RR_FeII(const double T) {
  return 1.42e-13 * pow(1e-4 * T, -0.891);
}

/* FeXVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXVII(const double T) {
  return pow(T, -1.5) * (0.0006342 * exp(-2.77e6/T) + 0.0835 * exp(-3.978e6/T) + 1.045 * exp(-7.052e6/T) + 0.3663 * exp(-1.3e7/T) - 0.03955 * exp(-3.579e7/T));
}

/* FeXVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXVI(const double T) {
  return pow(T, -1.5) * (0.0007676 * exp(-29350./T) + 0.005587 * exp(-81580./T) + 0.1152 * exp(-359100./T) + 0.04929 * exp(-1.735e6/T) + 0.7274 * exp(-7.545e6/T) + 0.007347 * exp(-4.634e7/T));
}

/* FeXV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXV(const double T) {
  return pow(T, -1.5) * (0.0005636 * exp(-3628./T) + 0.00786 * exp(-24890./T) + 0.05063 * exp(-140500./T) + 0.1753 * exp(-513300./T) + 0.1209 * exp(-5.018e6/T) + 0.1934 * exp(-8.689e6/T));
}

/* FeXIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXIV(const double T) {
  return pow(T, -1.5) * (0.002305 * exp(-4747./T) + 0.01072 * exp(-18770./T) + 0.03512 * exp(-119000./T) + 0.2105 * exp(-509000./T) + 0.03622 * exp(-1.595e6/T));
}

/* FeXIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXIII(const double T) {
  return pow(T, -1.5) * (0.004469 * exp(-2462./T) + 0.008538 * exp(-12610./T) + 0.01741 * exp(-93300./T) + 0.163 * exp(-488700./T) + 0.0868 * exp(-1.312e6/T));
}

/* FeXII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXII(const double T) {
  return pow(T, -1.5) * (0.001074 * exp(-1387./T) + 0.00608 * exp(-10480./T) + 0.01887 * exp(-39550./T) + 0.0254 * exp(-146100./T) + 0.0758 * exp(-401000./T) + 0.2773 * exp(-720800./T));
}

/* FeXI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeXI(const double T) {
  return pow(T, -1.5) * (6.487e-5 * exp(-110.1/T) + 8.793e-5 * exp(-565.4/T) + 0.0004939 * exp(-1842./T) + 0.003787 * exp(-7134./T) + 0.008878 * exp(-30850./T) + 0.05325 * exp(-187800./T) + 0.2104 * exp(-670600./T));
}

/* FeX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeX(const double T) {
  return pow(T, -1.5) * (6.485e-5 * exp(-39.94/T) + 6.36e-5 * exp(-562.1/T) + 0.000372 * exp(-1992./T) + 0.001607 * exp(-8325./T) + 0.003516 * exp(-27570./T) + 0.007326 * exp(-74090./T) + 0.0256 * exp(-155200./T) + 0.1005 * exp(-438800./T) + 0.1942 * exp(-735500./T));
}

/* FeIX total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeIX(const double T) {
  return pow(T, -1.5) * (4.777e-7 * exp(-9.034/T) + 1.231e-6 * exp(-112.8/T) + 5.055e-5 * exp(-662.4/T) + 0.0003413 * exp(-1143./T) + 0.001625 * exp(-3926./T) + 0.003873 * exp(-13000./T) + 0.006438 * exp(-46840./T) + 0.0697 * exp(-267000./T) + 0.2925 * exp(-735800./T));
}

/* FeVIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeVIII(const double T) {
  return pow(T, -1.5) * (5.978e-7 * exp(-8.385/T) + 8.939e-7 * exp(-99.22/T) + 1.64e-5 * exp(-523.4/T) + 9.598e-5 * exp(-1579./T) + 0.0001105 * exp(-4489./T) + 0.0007299 * exp(-21020./T) + 0.003858 * exp(-97780./T) + 0.02476 * exp(-335300./T) + 0.1789 * exp(-808100./T));
}

/* FeVII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeVII(const double T) {
  return pow(T, -1.5) * (0.092054 * exp(-528000./T) + 0.041024 * exp(-4.1776e6/T));
}

/* FeVI total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeVI(const double T) {
  return pow(T, -1.5) * (0.080047 * exp(-628960./T) + 0.024014 * exp(-1.1605e6/T));
}

/* FeV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeV(const double T) {
  return pow(T, -1.5) * (0.038023 * exp(-432850./T) + 0.01601 * exp(-782140./T));
}

/* FeIV total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeIV(const double T) {
  return pow(T, -1.5) * (0.015009 * exp(-331890./T) + 0.0047027 * exp(-604590./T));
}

/* FeIII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeIII(const double T) {
  return pow(T, -1.5) * (0.0023013 * exp(-193800./T) + 0.0027016 * exp(-364380./T));
}

/* FeII total dielectronic recombination rate coefficient [cm^3/s]. */
static inline double alpha_DR_FeII(const double T) {
  return pow(T, -1.5) * (0.00022013 * exp(-59415./T) + 0.00010006 * exp(-149700./T));
}

/* FeI collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_FeI(const double T) {
  const double U = FeI_K / T;
  return 2.52e-7 * pow(U, 0.25) * exp(-U) / (0.701 + U);
}

/* FeII collisional ionization rates over [1 eV, 20 keV]. */
static inline double beta_FeII(const double T) {
  const double U = FeII_K / T;
  return 2.21e-8 * (1. + sqrt(U)) * pow(U, 0.45) * exp(-U) / (0.033 + U);
}

/* FeIII collisional ionization rates over [2 eV, 20 keV]. */
static inline double beta_FeIII(const double T) {
  const double U = FeIII_K / T;
  return 4.1e-8 * pow(U, 0.17) * exp(-U) / (0.366 + U);
}

/* FeIV collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_FeIV(const double T) {
  const double U = FeIV_K / T;
  return 3.53e-8 * pow(U, 0.39) * exp(-U) / (0.243 + U);
}

/* FeV collisional ionization rates over [3 eV, 20 keV]. */
static inline double beta_FeV(const double T) {
  const double U = FeV_K / T;
  return 1.04e-8 * (1. + sqrt(U)) * pow(U, 0.17) * exp(-U) / (0.285 + U);
}

/* FeVI collisional ionization rates over [4 eV, 20 keV]. */
static inline double beta_FeVI(const double T) {
  const double U = FeVI_K / T;
  return 1.23e-8 * (1. + sqrt(U)) * pow(U, 0.21) * exp(-U) / (0.411 + U);
}

/* FeVII collisional ionization rates over [5 eV, 20 keV]. */
static inline double beta_FeVII(const double T) {
  const double U = FeVII_K / T;
  return 9.47e-9 * (1. + sqrt(U)) * pow(U, 0.21) * exp(-U) / (0.458 + U);
}

/* FeVIII collisional ionization rates over [7 eV, 20 keV]. */
static inline double beta_FeVIII(const double T) {
  const double U = FeVIII_K / T;
  return 4.71e-9 * (1. + sqrt(U)) * pow(U, 0.28) * exp(-U) / (0.28 + U);
}

/* FeIX collisional ionization rates over [10 eV, 20 keV]. */
static inline double beta_FeIX(const double T) {
  const double U = FeIX_K / T;
  return 3.02e-9 * (1. + sqrt(U)) * pow(U, 0.15) * exp(-U) / (0.697 + U);
}

/* FeX collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_FeX(const double T) {
  const double U = FeX_K / T;
  return 2.34e-9 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (0.764 + U);
}

/* FeXI collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_FeXI(const double T) {
  const double U = FeXI_K / T;
  return 1.76e-9 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (0.805 + U);
}

/* FeXII collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_FeXII(const double T) {
  const double U = FeXII_K / T;
  return 1.14e-9 * (1. + sqrt(U)) * pow(U, 0.15) * exp(-U) / (0.773 + U);
}

/* FeXIII collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_FeXIII(const double T) {
  const double U = FeXIII_K / T;
  return 8.66e-10 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (0.805 + U);
}

/* FeXIV collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_FeXIV(const double T) {
  const double U = FeXIV_K / T;
  return 6.61e-10 * (1. + sqrt(U)) * pow(U, 0.14) * exp(-U) / (0.762 + U);
}

/* FeXV collisional ionization rates over [20 eV, 20 keV]. */
static inline double beta_FeXV(const double T) {
  const double U = FeXV_K / T;
  return 4.41e-10 * (1. + sqrt(U)) * pow(U, 0.16) * exp(-U) / (0.698 + U);
}

/* FeXVI collisional ionization rates over [70 eV, 20 keV]. */
static inline double beta_FeXVI(const double T) {
  const double U = FeXVI_K / T;
  return 1.18e-10 * (1. + sqrt(U)) * pow(U, 0.15) * exp(-U) / (0.211 + U);
}

/* FeV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_FeV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 3e4) ? 3. : 1e-4 * T);
  return 1.46e-8 * pow(T4, 0.0357) * (1. - 0.92 * exp(-0.37 * T4));
}

/* FeIV + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_FeIV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 3.42e-9 * pow(T4, 0.51) * (1. - 2.06 * exp(-8.99 * T4));
}

/* FeIII + HI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HI_FeIII(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e5) ? 10. : 1e-4 * T);
  return 1.26e-9 * pow(T4, 0.0772) * (1. - 0.41 * exp(-7.31 * T4));
}

/* FeV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_FeV(const double T) {
  const double T4 = (T < 1e3) ? 0.1 : ((T > 1e7) ? 1000. : 1e-4 * T);
  return 1.38e-9 * pow(T4, 0.43) * (1. + 1.12 * exp(-0.16 * T4));
}

/* FeIV + HeI charge exchange recombination rate coefficient [cm^3/s]. */
static inline double xi_HeI_FeIV([[maybe_unused]] const double T) {
  return 3.05e-11;
}

/* FeII + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_FeII(const double T) {
  const double T4 = (T < 1e4) ? 1. : ((T > 1e5) ? 10. : 1e-4 * T);
  return 2.1e-9 * pow(T4, 0.0772) * (1. - 0.41 * exp(-7.31 * T4)) * exp(-3.005/T4);
}

/* FeI + HII charge exchange ionization rate coefficient [cm^3/s]. */
static inline double chi_HII_FeI([[maybe_unused]] const double T) {
  return 5.4e-9;
}
