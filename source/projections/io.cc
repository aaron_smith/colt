/*********************
 * projections/io.cc *
 *********************

 * All I/O operations related to projections.

*/

#include "../proto.h"
#include "Projections.h"
#include "../io_hdf5.h" // HDF5 read/write functions

/* Function to print additional projection information. */
void Projections::print_info() {
  // Print field list
  cout << "\nProjection information:"
       << "\n  n_fields   = " << n_projections
       << "\n  depth      = " << proj_depth / kpc << " kpc"
       << "\n  radius     = " << proj_radius / kpc << " kpc";
  if (adaptive)
    cout << "\n  pixel_rtol = " << 100. * pixel_rtol << "%  (adaptive convergence)";
  if (perspective)
    cout << "\nNote: Using a perspective camera.";
  if (monoscopic)
    cout << "\nNote: Using a monoscopic camera.";
  if (stepback_factor != 0.)
    cout << "  [stepback_factor = " << stepback_factor << "]";
  if (override_volume)
    cout << "\nNote: Using a constant volume override.";
  if (proj_sphere)
    cout << "\nNote: Projecting through a spherical volume.";
  if (proj_cylinder)
    cout << "\nNote: Projecting through a cylindrical volume.";
  if (fade) {
    if (fade_in_length > negative_infinity)
      cout << "\nNote: Using fade distance weights with a characteristic length of "
           << fade_in_length / kpc << " kpc and width of " << fade_in_width / kpc << " kpc";
    if (fade_out_length < positive_infinity)
      cout << "\nNote: Using fade-out distance weights with a characteristic length of "
           << fade_out_length / kpc << " kpc and width of " << fade_out_width / kpc << " kpc";
  }

  cout << "\n\nProjection fields:";
  for (auto& fw : proj_list) {
    string proj_str = "\n  " + fields[fw.field].name;
    if (fw.weight == SUM)
      proj_str += " [sum]";
    else if (fw.weight == AVG)
      proj_str += " [avg]";
    else if (fw.weight == MASS)
      proj_str += " [mass]";
    else
      proj_str += " [" + fields[fw.weight].name + "]";
    if (fw.hist >= 0)
      proj_str += " [" + fields[fw.hist].name + "]";
    cout << proj_str;
  }
  cout << endl;
  if (kappa_flag)
    cout << "\nAbsorption opacity field: " << kappa_field_name << endl;

  print_cameras();                           // General camera parameters
}

/* Copy of a string with underscores removed. */
static inline string no_underscores(const string str) {
  string result;
  result.reserve(str.size());                // Avoids buffer reallocations
  for (size_t i = 0; i < str.size(); ++i)
    if (str[i] != '_')
      result += str[i];
  return result;
}

/* Writes projection data and info to the specified HDF5 File or Group. */
void Projections::write_projections(const H5Object& f) {
  write(f, "proj_depth", proj_depth);        // Projection depth [cm]
  if (use_all_halos)
    write(f, "halo_radius", halo_radius);    // Halo radius [cm]
  else
    write(f, "proj_radius", proj_radius);    // Projection radius [cm]
  if (fade) {
    write(f,"fade_in_length", fade_in_length); // Characteristic length for the weights
    write(f,"fade_in_width" , fade_in_width); // Characteristic width for the weights
    write(f,"fade_out_length", fade_out_length); // Characteristic length for the weights
    write(f,"fade_out_width" , fade_out_width); // Characteristic width for the weights
  }
  if (n_hists > 0) {
    strings hist_names(n_hists);             // Histogram field names
    for (int i = 0; i < n_hists; ++i)
      hist_names[i] = fields[hist_list[i]].name;
    write(f, "n_hists", n_hists);            // Number of histogram fields
    write(f, "hist_keys", hist_keys);        // Histogram field keys
    write(f, "hist_names", hist_names);      // Histogram field names
    vector<int> hist_logs_int(n_hists);      // Logarithmic histogram flag (int)
    for (int i = 0; i < n_hists; ++i)
      hist_logs_int[i] = hist_logs[i];       // Convert to int
    write(f, "hist_logs", hist_logs_int);    // Logarithmic histogram flag
    write(f, "hist_mins", hist_mins);        // Histogram minimum values
    write(f, "hist_maxs", hist_maxs);        // Histogram maximum values
    write(f, "hist_n_bins", hist_n_bins);    // Number of histogram bins
  } else if (n_slices > 1)
    write(f, "n_slices", n_slices);          // Number of slices
  const bool is_3d = n_hists > 0 || n_slices > 1; // 3D projection flag
  for (int i = 0; i < n_projections; ++i) {
    const int field = proj_list[i].field;
    const int weight = proj_list[i].weight;
    const int hist = proj_list[i].hist;
    string name = "proj_" + no_underscores(fields[field].name);
    if (weight == SUM)                       // Conserved integral projection
      name += "_sum";
    else if (weight == AVG)                  // Volume-weighted average
      name += "_avg";
    else if (weight == MASS)                 // Mass-weighted average
      name += "_mass";
    else                                     // Field-weighted average
      name += "_" + no_underscores(fields[weight].name);
    if (hist >= 0)
      name += "_" + no_underscores(fields[hist].name);
    if (is_3d)
      write(f, name, proj_3D[i], projection_units[i]); // 3D projection data
    else
      write(f, name, projections[i], projection_units[i]); // Projection data
  }
}

/* Writes projection data and info to the specified hdf5 halo_num group. */
void Projections::write_projections(const int num) {
  H5File f(output_dir + "/" + output_base + snap_str + halo_str + "." + output_ext, H5F_ACC_RDWR);
  if (num == 0) {
    Group g_proj = f.createGroup("/proj");
    if (use_all_halos) {
      const int n_halos = select_subhalo ? n_subhalos : n_groups; // Number of halos
      const vector<int>& halo_id = select_subhalo ? subhalo_id : group_id; // Halo IDs
      write(f, "n_halos", n_halos);          // Number of halos
      write(f, "halo_id", halo_id);          // Halo IDs
    }
  }
  Group g = f.createGroup("/proj/" + to_string(use_all_halos ? halo_num : num));
  write_camera_info(g, false, true);         // Camera information
  write_projections(g);                      // Projection data and info
}

/* Writes projection data and info to the specified hdf5 file. */
void Projections::write_module(const H5File& f) {
  write(f, "n_projections", n_projections);  // Number of projections
  write(f, "adaptive", adaptive);            // Adaptive convergence flag
  if (adaptive)
    write(f, "pixel_rtol", pixel_rtol);      // Relative tolerance per pixel
  write(f, "perspective", perspective);      // Perspective camera flag
  if (perspective || monoscopic)
    write(f, "stepback_factor", stepback_factor); // Stepback factor
  write(f, "override_volume", override_volume); // Volume normalization
  write(f, "proj_sphere", proj_sphere);      // Spherical volume flag
  write(f, "proj_cylinder", proj_cylinder);  // Cylindrical volume flag
  if (proj_sphere || proj_cylinder)
    write(f, "shape_factor", shape_factor);  // Extraction shape factor
  write(f, "fade", fade);                    // Fade distance weights in projections
  if (!use_all_halos)
    write_projections(f);                    // Projection data and info
}
