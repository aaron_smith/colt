/*****************************
 * projections/initialize.cc *
 *****************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "Projections.h"
bool avoid_cell(const int cell); // Avoid calculations for certain cells

/* General setup for projections. */
void Simulation::setup_projections() {
  // Consistency checks for image width and number of pixels
  if (proj_radius_bbox > 0.)
    proj_radius = min_bbox_distance(camera_center) * proj_radius_bbox; // Set based on distance to bbox
  if (proj_radius_Rvir > 0.)
    proj_radius = halo_radius * proj_radius_Rvir; // Set based on virial radius
  if (proj_radius >= 0.)
    proj_depth = 2. * proj_radius;           // Set depth from radius
  if (proj_depth_cMpc > 0.)
    proj_depth = proj_depth_cMpc * Mpc / (1. + z); // Set depth from comoving Mpc
  if (proj_depth <= 0.)
    proj_depth = 0.5 * (image_widths.x + image_widths.y); // Default to mean image width
  if (proj_depth > 1e3 * max_bbox_width)
    proj_depth = 1e3 * max_bbox_width;       // Avoid very large depths

  // Set redundant projection parameters
  proj_radius = 0.5 * proj_depth;            // Set radius based on depth
}

/* Setup everything for projections. */
void Projections::setup() {
  // General setup for cameras
  setup_cameras();

  // General setup for projections
  setup_projections();
  image_zedges = linspace(-proj_radius, proj_radius, n_slices+1);

  if (fade) {
    // Fade setup (bbox)
    const double min_bbox_dist = min_bbox_distance(camera_center);
    if (fade_in_length_bbox > negative_infinity)
      fade_in_length = min_bbox_dist * fade_in_length_bbox;
    if (fade_in_width_bbox > 0.)
      fade_in_width = min_bbox_dist * fade_in_width_bbox;
    if (fade_out_length_bbox < positive_infinity)
      fade_out_length = min_bbox_dist * fade_out_length_bbox;
    if (fade_out_width_bbox > 0.)
      fade_out_width = min_bbox_dist * fade_out_width_bbox;

    // Fade setup (Rvir)
    if (fade_in_length_Rvir > 0.)
      fade_in_length = halo_radius * fade_in_length_Rvir;
    if (fade_in_width_Rvir > 0.)
      fade_in_width = halo_radius * fade_in_width_Rvir;
    if (fade_out_length_Rvir > 0.)
      fade_out_length = halo_radius * fade_out_length_Rvir;
    if (fade_out_width_Rvir > 0.)
      fade_out_width = halo_radius * fade_out_width_Rvir;
  }

  // Allocate projection data
  if (n_hists > 0) {
    proj_3D.resize(n_projections);
    #pragma omp parallel for
    for (int i = 0; i < n_projections; ++i)
      proj_3D[i] = Cubes(n_cameras, Cube(nx_pixels, ny_pixels, hist_n_bins[hist_keys[i]]));
  } else if (n_slices > 1) {
    proj_3D.resize(n_projections);
    #pragma omp parallel for
    for (int i = 0; i < n_projections; ++i)
      proj_3D[i] = Cubes(n_cameras, Cube(nx_pixels, ny_pixels, n_slices));
  } else {
    projections.resize(n_projections);
    #pragma omp parallel for
    for (int i = 0; i < n_projections; ++i)
      projections[i] = Images(n_cameras, Image(nx_pixels, ny_pixels));
  }

  // Initialize the min/max values for the histograms
  for (int i = 0; i < n_hists; ++i) {
    // Check if the min/max values are already set
    if (hist_mins[i] == 0. || hist_maxs[i] == 0.) {
      // Some fields require special handling
      const int hist = hist_list[i];         // Histogram index
      if (hist == VelocitySquaredLOS || hist == VelocityLOS || hist == VelocityX || hist == VelocityY ||
          hist == MagneticFieldLOS || hist == MagneticFieldX || hist == MagneticFieldY) {
        #pragma omp parallel
        for (int i = 0; i < n_cells; ++i) {
          if (avoid_cell(i))
            continue;
          fields[hist].data[i] = norm(fields[hist].data[i]); // Magnitude
        }
      }
      vector<double>& hist_data = fields[hist].data;
      hist_mins[i] = omp_min(hist_data);     // Minimum value
      hist_maxs[i] = omp_max(hist_data);     // Maximum value
      hist_mins[i] *= (hist_mins[i] < 0.) ? 1.000001 : 0.999999; // Adjust min/max values
      hist_maxs[i] *= (hist_maxs[i] < 0.) ? 0.999999 : 1.000001; // to avoid edge cases
      if (hist_logs[i]) {                    // Logarithmic field
        if (hist_mins[i] <= 0.)
          root_error("Logarithmic histogram field " + fields[hist].name + " has non-positive minimum value.");
        hist_mins[i] = log10(hist_mins[i]);  // Convert to log scale
        hist_maxs[i] = log10(hist_maxs[i]);
      }
    }
    hist_inv_widths[i] = double(hist_n_bins[i]) / (hist_maxs[i] - hist_mins[i]); // Inverse width
  }
}
