/*****************************
 * projections/Projections.h *
 *****************************

 * Module declarations.

*/

#pragma once
#include "../Simulation.h" // Base simulation class

/* Ray-based projections module. */
class Projections : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  int n_projections = 0, n_hists = 0;        // Number of projections (and histogram fields)
  strings projection_units;                  // List of projection units
  vector<FieldWeightPair> proj_list;         // Projection (field, weight, hist) list
  vector<bool> hist_logs;                    // Logarithmic histogram flag
  vector<int> hist_list, hist_keys, hist_n_bins; // Histogram fields, key index, and number of bins
  vector<double> hist_mins, hist_maxs, hist_inv_widths; // Histogram min/max values and inverse widths
  vector<Images> projections;                // Projection images
  vector<Cubes> proj_3D;                     // 3D projection cubes
  bool kappa_flag = false;                   // Absorption opacity flag
  string kappa_field_name = "";              // Absorption opacity field name
  int kappa_field_key = -1;                  // Absorption opacity field key
  double kappa_constant = 1.;                // Absorption opacity coefficient
  double kappa_exponent = 1.;                // Opacity exponent
  double rho_exponent = 1.;                  // Density exponent
  bool fade = false;                         // Flag for distance weights
  double fade_in_length = negative_infinity; // Characteristic length for the fade-in weights
  double fade_in_width = 1e-10;              // Characteristic width for the fade-in weights
  double fade_out_length = positive_infinity; // Characteristic length for the fade-out weights
  double fade_out_width = 1e-10;             // Characteristic width for the fade-out weights
  double fade_in_length_bbox = negative_infinity; // Characteristic length for the fade-in weights
  double fade_in_width_bbox = 0.;            // Characteristic width for the fade-in weights
  double fade_out_length_bbox = positive_infinity; // Characteristic length for the fade-out weights
  double fade_out_width_bbox = 0.;           // Characteristic width for the fade-out weights
  double fade_in_length_Rvir = negative_infinity; // Characteristic length for the fade-in weights
  double fade_in_width_Rvir = 0.;            // Characteristic width for the fade-in weights
  double fade_out_length_Rvir = positive_infinity; // Characteristic length for the fade-out weights
  double fade_out_width_Rvir = 0.;           // Characteristic width for the fade-out weights

  bool check_perp_fields();                  // Test whether there are any perpendicular fields
  double ray_trace_single_field(const double rx, const double ry, const int iz); // Single ray (single field)
  double pixel_quad_2D(const int ix, const int iy, const int iz); // Adaptive 2D integrator
  void calculate_projections();              // Single image and direction
  void run_adaptive(const int start_cam, const int end_cam); // Adaptive projections
  void ray_trace_all_fields(vector<vector<double>>& results, const double rx, const double ry, const int iz); // Single ray (all fields)
  void run_standard(const int start_cam, const int end_cam); // Non-adaptive projections
  void ray_trace_perspective_all_fields(vector<double>& results, const double rx, const double ry, const bool perp_flag); // Single ray (all fields)
  void run_perspective(const int start_cam, const int end_cam); // Non-adaptive perspective projections
  void ray_trace_monoscopic_all_fields(vector<double>& results, const Vec3& k_LOS, const bool perp_flag);
  void run_monoscopic();                     // Non-adaptive monoscopic projections
  void write_projections(const int num);     // Write projection data
  void write_projections(const H5::H5Object& f); // Write projection data

  // Computes a weight in the range [0,1] due to the distance from the camera
  inline double get_distance_weight(const double distance) {
    double x_in  = 9. * (distance - fade_in_length) / fade_in_width;
    double x_out = 9. * (fade_out_length - distance) / fade_out_width;
    return 1. / ( (1. + exp(-x_in)) * (1. + exp(-x_out)) ); // Double logistic decay
  }
};
