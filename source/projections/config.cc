/*************************
 * projections/config.cc *
 *************************

 * Projections module configuration.

*/

#include "../proto.h"
#include "Projections.h"
#include "../config.h" // Configuration functions
#include <unordered_map>

static inline int key_of_known_field(const string& field) {
  static const std::unordered_map<string, FieldIndex> field_map = {
    {"Density", Density}, {"rho", Density},
    {"Volume", Volume}, {"V", Volume},
    {"Temperature", Temperature}, {"T", Temperature},
    {"InternalEnergy", InternalEnergy}, {"e_int", InternalEnergy},
    {"HydrogenDensity", HydrogenDensity}, {"n_H", HydrogenDensity},
    {"HeliumDensity", HeliumDensity}, {"n_He", HeliumDensity},
    {"CarbonDensity", CarbonDensity}, {"n_C", CarbonDensity},
    {"NitrogenDensity", NitrogenDensity}, {"n_N", NitrogenDensity},
    {"OxygenDensity", OxygenDensity}, {"n_O", OxygenDensity},
    {"NeonDensity", NeonDensity}, {"n_Ne", NeonDensity},
    {"MagnesiumDensity", MagnesiumDensity}, {"n_Mg", MagnesiumDensity},
    {"SiliconDensity", SiliconDensity}, {"n_Si", SiliconDensity},
    {"SulferDensity", SulferDensity}, {"n_S", SulferDensity},
    {"IronDensity", IronDensity}, {"n_Fe", IronDensity},
    {"LowerDensity", LowerDensity}, {"n_lower", LowerDensity},
    {"UpperDensity", UpperDensity}, {"n_upper", UpperDensity},
    // Ionization fractions
    {"HI_Fraction", HI_Fraction}, {"x_HI", HI_Fraction},
    {"HII_Fraction", HII_Fraction}, {"x_HII", HII_Fraction},
    {"H2_Fraction", H2_Fraction}, {"x_H2", H2_Fraction},
    {"HeI_Fraction", HeI_Fraction}, {"x_HeI", HeI_Fraction},
    {"HeII_Fraction", HeII_Fraction}, {"x_HeII", HeII_Fraction},
    {"HeIII_Fraction", HeIII_Fraction}, {"x_HeIII", HeIII_Fraction},
    // Carbon
    {"CI_Fraction", CI_Fraction}, {"x_CI", CI_Fraction},
    {"CII_Fraction", CII_Fraction}, {"x_CII", CII_Fraction},
    {"CIII_Fraction", CIII_Fraction}, {"x_CIII", CIII_Fraction},
    {"CIV_Fraction", CIV_Fraction}, {"x_CIV", CIV_Fraction},
    {"CV_Fraction", CV_Fraction}, {"x_CV", CV_Fraction},
    {"CVI_Fraction", CVI_Fraction}, {"x_CVI", CVI_Fraction},
    {"CVII_Fraction", CVII_Fraction}, {"x_CVII", CVII_Fraction},
    // Nitrogen
    {"NI_Fraction", NI_Fraction}, {"x_NI", NI_Fraction},
    {"NII_Fraction", NII_Fraction}, {"x_NII", NII_Fraction},
    {"NIII_Fraction", NIII_Fraction}, {"x_NIII", NIII_Fraction},
    {"NIV_Fraction", NIV_Fraction}, {"x_NIV", NIV_Fraction},
    {"NV_Fraction", NV_Fraction}, {"x_NV", NV_Fraction},
    {"NVI_Fraction", NVI_Fraction}, {"x_NVI", NVI_Fraction},
    {"NVII_Fraction", NVII_Fraction}, {"x_NVII", NVII_Fraction},
    {"NVIII_Fraction", NVIII_Fraction}, {"x_NVIII", NVIII_Fraction},
    // Oxygen
    {"OI_Fraction", OI_Fraction}, {"x_OI", OI_Fraction},
    {"OII_Fraction", OII_Fraction}, {"x_OII", OII_Fraction},
    {"OIII_Fraction", OIII_Fraction}, {"x_OIII", OIII_Fraction},
    {"OIV_Fraction", OIV_Fraction}, {"x_OIV", OIV_Fraction},
    {"OV_Fraction", OV_Fraction}, {"x_OV", OV_Fraction},
    {"OVI_Fraction", OVI_Fraction}, {"x_OVI", OVI_Fraction},
    {"OVII_Fraction", OVII_Fraction}, {"x_OVII", OVII_Fraction},
    {"OVIII_Fraction", OVIII_Fraction}, {"x_OVIII", OVIII_Fraction},
    {"OIX_Fraction", OIX_Fraction}, {"x_OIX", OIX_Fraction},
    // Neon
    {"NeI_Fraction", NeI_Fraction}, {"x_NeI", NeI_Fraction},
    {"NeII_Fraction", NeII_Fraction}, {"x_NeII", NeII_Fraction},
    {"NeIII_Fraction", NeIII_Fraction}, {"x_NeIII", NeIII_Fraction},
    {"NeIV_Fraction", NeIV_Fraction}, {"x_NeIV", NeIV_Fraction},
    {"NeV_Fraction", NeV_Fraction}, {"x_NeV", NeV_Fraction},
    {"NeVI_Fraction", NeVI_Fraction}, {"x_NeVI", NeVI_Fraction},
    {"NeVII_Fraction", NeVII_Fraction}, {"x_NeVII", NeVII_Fraction},
    {"NeVIII_Fraction", NeVIII_Fraction}, {"x_NeVIII", NeVIII_Fraction},
    {"NeIX_Fraction", NeIX_Fraction}, {"x_NeIX", NeIX_Fraction},
    // Magnesium
    {"MgI_Fraction", MgI_Fraction}, {"x_MgI", MgI_Fraction},
    {"MgII_Fraction", MgII_Fraction}, {"x_MgII", MgII_Fraction},
    {"MgIII_Fraction", MgIII_Fraction}, {"x_MgIII", MgIII_Fraction},
    {"MgIV_Fraction", MgIV_Fraction}, {"x_MgIV", MgIV_Fraction},
    {"MgV_Fraction", MgV_Fraction}, {"x_MgV", MgV_Fraction},
    {"MgVI_Fraction", MgVI_Fraction}, {"x_MgVI", MgVI_Fraction},
    {"MgVII_Fraction", MgVII_Fraction}, {"x_MgVII", MgVII_Fraction},
    {"MgVIII_Fraction", MgVIII_Fraction}, {"x_MgVIII", MgVIII_Fraction},
    {"MgIX_Fraction", MgIX_Fraction}, {"x_MgIX", MgIX_Fraction},
    {"MgX_Fraction", MgX_Fraction}, {"x_MgX", MgX_Fraction},
    {"MgXI_Fraction", MgXI_Fraction}, {"x_MgXI", MgXI_Fraction},
    // Silicon
    {"SiI_Fraction", SiI_Fraction}, {"x_SiI", SiI_Fraction},
    {"SiII_Fraction", SiII_Fraction}, {"x_SiII", SiII_Fraction},
    {"SiIII_Fraction", SiIII_Fraction}, {"x_SiIII", SiIII_Fraction},
    {"SiIV_Fraction", SiIV_Fraction}, {"x_SiIV", SiIV_Fraction},
    {"SiV_Fraction", SiV_Fraction}, {"x_SiV", SiV_Fraction},
    {"SiVI_Fraction", SiVI_Fraction}, {"x_SiVI", SiVI_Fraction},
    {"SiVII_Fraction", SiVII_Fraction}, {"x_SiVII", SiVII_Fraction},
    {"SiVIII_Fraction", SiVIII_Fraction}, {"x_SiVIII", SiVIII_Fraction},
    {"SiIX_Fraction", SiIX_Fraction}, {"x_SiIX", SiIX_Fraction},
    {"SiX_Fraction", SiX_Fraction}, {"x_SiX", SiX_Fraction},
    {"SiXI_Fraction", SiXI_Fraction}, {"x_SiXI", SiXI_Fraction},
    {"SiXII_Fraction", SiXII_Fraction}, {"x_SiXII", SiXII_Fraction},
    {"SiXIII_Fraction", SiXIII_Fraction}, {"x_SiXIII", SiXIII_Fraction},
    // Sulfer
    {"SI_Fraction", SI_Fraction}, {"x_SI", SI_Fraction},
    {"SII_Fraction", SII_Fraction}, {"x_SII", SII_Fraction},
    {"SIII_Fraction", SIII_Fraction}, {"x_SIII", SIII_Fraction},
    {"SIV_Fraction", SIV_Fraction}, {"x_SIV", SIV_Fraction},
    {"SV_Fraction", SV_Fraction}, {"x_SV", SV_Fraction},
    {"SVI_Fraction", SVI_Fraction}, {"x_SVI", SVI_Fraction},
    {"SVII_Fraction", SVII_Fraction}, {"x_SVII", SVII_Fraction},
    {"SVIII_Fraction", SVIII_Fraction}, {"x_SVIII", SVIII_Fraction},
    {"SIX_Fraction", SIX_Fraction}, {"x_SIX", SIX_Fraction},
    {"SX_Fraction", SX_Fraction}, {"x_SX", SX_Fraction},
    {"SXI_Fraction", SXI_Fraction}, {"x_SXI", SXI_Fraction},
    {"SXII_Fraction", SXII_Fraction}, {"x_SXII", SXII_Fraction},
    {"SXIII_Fraction", SXIII_Fraction}, {"x_SXIII", SXIII_Fraction},
    {"SXIV_Fraction", SXIV_Fraction}, {"x_SXIV", SXIV_Fraction},
    {"SXV_Fraction", SXV_Fraction}, {"x_SXV", SXV_Fraction},
    // Iron
    {"FeI_Fraction", FeI_Fraction}, {"x_FeI", FeI_Fraction},
    {"FeII_Fraction", FeII_Fraction}, {"x_FeII", FeII_Fraction},
    {"FeIII_Fraction", FeIII_Fraction}, {"x_FeIII", FeIII_Fraction},
    {"FeIV_Fraction", FeIV_Fraction}, {"x_FeIV", FeIV_Fraction},
    {"FeV_Fraction", FeV_Fraction}, {"x_FeV", FeV_Fraction},
    {"FeVI_Fraction", FeVI_Fraction}, {"x_FeVI", FeVI_Fraction},
    {"FeVII_Fraction", FeVII_Fraction}, {"x_FeVII", FeVII_Fraction},
    {"FeVIII_Fraction", FeVIII_Fraction}, {"x_FeVIII", FeVIII_Fraction},
    {"FeIX_Fraction", FeIX_Fraction}, {"x_FeIX", FeIX_Fraction},
    {"FeX_Fraction", FeX_Fraction}, {"x_FeX", FeX_Fraction},
    {"FeXI_Fraction", FeXI_Fraction}, {"x_FeXI", FeXI_Fraction},
    {"FeXII_Fraction", FeXII_Fraction}, {"x_FeXII", FeXII_Fraction},
    {"FeXIII_Fraction", FeXIII_Fraction}, {"x_FeXIII", FeXIII_Fraction},
    {"FeXIV_Fraction", FeXIV_Fraction}, {"x_FeXIV", FeXIV_Fraction},
    {"FeXV_Fraction", FeXV_Fraction}, {"x_FeXV", FeXV_Fraction},
    {"FeXVI_Fraction", FeXVI_Fraction}, {"x_FeXVI", FeXVI_Fraction},
    {"FeXVII_Fraction", FeXVII_Fraction}, {"x_FeXVII", FeXVII_Fraction},
    {"ElectronAbundance", ElectronAbundance}, {"x_e", ElectronAbundance},
    {"StarFormationRate", StarFormationRate}, {"SFR", StarFormationRate},
    {"DustTemperature", DustTemperature}, {"T_dust", DustTemperature},
    {"VelocitySquared", VelocitySquared}, {"v2", VelocitySquared},
    {"VelocitySquaredLOS", VelocitySquaredLOS}, {"v2_LOS", VelocitySquaredLOS},
    {"VelocityLOS", VelocityLOS}, {"v_LOS", VelocityLOS}, {"vz", VelocityLOS},
    {"VelocityX", VelocityX}, {"v_x", VelocityX}, {"vx", VelocityX},
    {"VelocityY", VelocityY}, {"v_y", VelocityY}, {"vy", VelocityY},
    {"MagneticFieldSquared", MagneticFieldSquared}, {"B2", MagneticFieldSquared},
    {"MagneticFieldLOS", MagneticFieldLOS}, {"B_LOS", MagneticFieldLOS}, {"Bz", MagneticFieldLOS},
    {"MagneticFieldX", MagneticFieldX}, {"B_x", MagneticFieldX}, {"Bx", MagneticFieldX},
    {"MagneticFieldY", MagneticFieldY}, {"B_y", MagneticFieldY}, {"By", MagneticFieldY},
    {"HydrogenFraction", HydrogenFraction}, {"X", HydrogenFraction},
    {"HeliumFraction", HeliumFraction}, {"Y", HeliumFraction},
    {"Metallicity", Metallicity}, {"Z", Metallicity},
    {"CarbonMetallicity", CarbonMetallicity}, {"Z_C", CarbonMetallicity},
    {"NitrogenMetallicity", NitrogenMetallicity}, {"Z_N", NitrogenMetallicity},
    {"OxygenMetallicity", OxygenMetallicity}, {"Z_O", OxygenMetallicity},
    {"NeonMetallicity", NeonMetallicity}, {"Z_Ne", NeonMetallicity},
    {"MagnesiumMetallicity", MagnesiumMetallicity}, {"Z_Mg", MagnesiumMetallicity},
    {"SiliconMetallicity", SiliconMetallicity}, {"Z_Si", SiliconMetallicity},
    {"SulferMetallicity", SulferMetallicity}, {"Z_S", SulferMetallicity},
    {"IronMetallicity", IronMetallicity}, {"Z_Fe", IronMetallicity},
    {"DustMetallicity", DustMetallicity}, {"D", DustMetallicity},
    {"LineEmissivity", LineEmissivity}, {"j_line", LineEmissivity},
    {"DoubletLineEmissivity", DoubletLineEmissivity}, {"jp_line", DoubletLineEmissivity},
    {"EnergyDensity", EnergyDensity}, {"u_rad", EnergyDensity},
    {"RadialAcceleration", RadialAcceleration}, {"a_rad_r", RadialAcceleration},
    {"RadialPressure", RadialPressure}, {"P_rad_r", RadialPressure},
    {"DensitySquared", DensitySquared}, {"rho2", DensitySquared},
    {"DensitySquaredHI", DensitySquaredHI}, {"rho2HI", DensitySquaredHI},
    {"StellarDensity", StellarDensity}, {"rho_star", StellarDensity},
    {"IonizationFront", IonizationFront}, {"ion_front", IonizationFront},
  };

  auto it = field_map.find(field);
  if (it != field_map.end())
    return it->second;
  root_error("Unrecognized field name: " + field);
  return 0;
}

static inline int key_of_known_weight(const string& weight) {
  if (weight == "sum")
    return SUM;                              // Conserved sum weight
  else if (weight == "avg")
    return AVG;                              // Volume-weighted average
  else if (weight == "mass")
    return MASS;                             // Mass-weighted average
  else if (weight == "none")
    return OUTSIDE;                          // Needed for histograms
  return key_of_known_field(weight);
}

static inline string units_of_known_field(int field) {
  if (field < 0 || field >= n_fields)
    root_error("Field units are not known: " + to_string(field));
  return fields[field].cgs;                  // Return units [cgs]
}

/* General projection configuration. */
void Simulation::projection_config(YAML::Node& file) {
  // Setup the projection depth
  incompatible("proj_radius_bbox", "proj_radius_Rvir", "proj_radius", "proj_depth_cMpc", "proj_depth");
  if (file["proj_radius_bbox"]) {
    load("proj_radius_bbox", proj_radius_bbox, ">=0"); // Projection radius [min distance to bbox edge]
    if (file["stepback_factor_bbox"]) {
      double stepback_factor_bbox = 0.;      // Stepback factor in bbox units
      load("stepback_factor_bbox", stepback_factor_bbox); // Perspective stepback factor (bbox)
      stepback_factor = stepback_factor_bbox / proj_radius_bbox;
    }
    if (file["shape_factor_bbox"]) {
      double shape_factor_bbox = 0.;         // Shape factor in bbox units
      load("shape_factor_bbox", shape_factor_bbox); // Extraction shape factor (bbox)
      shape_factor = shape_factor_bbox / proj_radius_bbox;
    }
  } else if (file["proj_radius_Rvir"]) {
    load("proj_radius_Rvir", proj_radius_Rvir, ">=0"); // Projection radius [selected halo virial radius]
    if (file["stepback_factor_Rvir"]) {
      double stepback_factor_Rvir = 0.;      // Stepback factor in Rvir units
      load("stepback_factor_Rvir", stepback_factor_Rvir); // Perspective stepback factor (Rvir)
      stepback_factor = stepback_factor_Rvir / proj_radius_Rvir;
    }
    if (file["shape_factor_Rvir"]) {
      double shape_factor_Rvir = 0.;         // Shape factor in Rvir units
      load("shape_factor_Rvir", shape_factor_Rvir); // Extraction shape factor (Rvir)
      shape_factor = shape_factor_Rvir / proj_radius_Rvir;
    }
  } else if (file["proj_radius"]) {
    load("proj_radius", proj_radius, ">=0"); // Projection radius [cm] (proj_depth / 2)
  } else if (file["proj_depth_cMpc"]) {
    load("proj_depth_cMpc", proj_depth_cMpc, ">=0"); // Projection depth [cMpc]
  } else {
    load("proj_depth", proj_depth, ">=0");   // Projection depth [cm] (defaults to image_width)
  }
  load("n_slices", n_slices, ">0");          // Number of slices for adaptive projections

  // Relative tolerence for adaptive convergence
  load("adaptive", adaptive);                // Adaptive convergence projections
  if (adaptive)
    load("pixel_rtol", pixel_rtol);          // Relative tolerance per pixel
  load("perspective", perspective);          // Perspective camera
  if (perspective || monoscopic)
    load("stepback_factor", stepback_factor); // Stepback factor (relative to radius)
  if (perspective && monoscopic)
    root_error("perspective and monoscopic are not compatible!");
  load("override_volume", override_volume);  // Override projection volume normalization
  load("proj_sphere", proj_sphere);          // Project through a spherical volume
  load("proj_cylinder", proj_cylinder);      // Project through a cylindrical volume
  if (proj_sphere || proj_cylinder) {
    load("shape_factor", shape_factor);      // Extraction shape factor (relative to radius)
    if (adaptive)
      root_error("proj_sphere and proj_cylinder are not (yet) compatible with adaptive!");
  }
}

// Some compilers allow duplicate (field, weight) pairs, so we avoid this
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const FieldWeightPair& fwp) {
  const bool is_in = proj_set.find(fwp) != proj_set.end();
  if (!is_in)
    proj_set.insert(fwp);                    // Only add if not found
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const int field, const int weight, const int hist = OUTSIDE) {
  auto fwp = FieldWeightPair(field, weight, hist); // Object to insert
  safe_insert(proj_set, fwp);                // Wrapper
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const string& f_str, const string& w_str, const string& h_str = "") {
  const int hist_key = h_str.empty() ? OUTSIDE : key_of_known_field(h_str);
  safe_insert(proj_set, key_of_known_field(f_str), key_of_known_weight(w_str), hist_key); // Wrapper
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const std::pair<string,string>& fw) {
  safe_insert(proj_set, fw.first, fw.second); // Wrapper
}
static inline void safe_insert(std::set<FieldWeightPair>& proj_set, const tuple<string,string,string>& fw) {
  const auto& [first, second, third] = fw;   // Unpack tuple
  safe_insert(proj_set, key_of_known_field(first), key_of_known_weight(second), key_of_known_field(third)); // Wrapper
}

/* Projections module configuration. */
void Projections::module_config(YAML::Node& file) {
  // Information about the cameras
  camera_config(file);                       // General camera setup
  if (n_cameras <= 0)
    root_error("The projections module requires at least one camera "
               "but none were specified in " + config_file);
  if (n_cameras < n_ranks)
    root_error("Projections require n_cameras >= n_ranks to avoid idle processors.");
  after_camera_config(file);                 // After camera setup

  // Information about the projections
  projection_config(file);                   // General projection setup
  if (file["fade_in_length"] || file["fade_in_length_bbox"] || file["fade_in_length_Rvir"] ||
      file["fade_out_length"] || file["fade_out_length_bbox"] || file["fade_out_length_Rvir"])
    fade = true;                             // Activate flag for distance weights
  if (fade && adaptive)
    root_error("Distance weights are not (yet) compatible with adaptive");
  if (fade) {
    // Characteristic fade in length for the weights
    if (file["fade_in_length_bbox"]) {
      load("fade_in_length_bbox", fade_in_length_bbox); // [bbox]
    } else if (file["fade_in_length_Rvir"]) {
      load("fade_in_length_Rvir", fade_in_length_Rvir); // [Rvir]
    } else {
      load("fade_in_length", fade_in_length); // [cm]
    }
    // Characteristic fade in width for the weights
    if (file["fade_in_width_bbox"]) {
      load("fade_in_width_bbox", fade_in_width_bbox, ">0"); // [bbox]
    } else if (file["fade_in_width_Rvir"]) {
      load("fade_in_width_Rvir", fade_in_width_Rvir, ">0"); // [Rvir]
    } else {
      load("fade_in_width", fade_in_width, ">0"); // [cm]
    }
    // Characteristic fade out length for the weights
    if (file["fade_out_length_bbox"]) {
      load("fade_out_length_bbox", fade_out_length_bbox); // [bbox]
    } else if (file["fade_out_length_Rvir"]) {
      load("fade_out_length_Rvir", fade_out_length_Rvir); // [Rvir]
    } else {
      load("fade_out_length", fade_out_length); // [cm]
    }
    // Characteristic fade out width for the weights
    if (file["fade_out_width_bbox"]) {
      load("fade_out_width_bbox", fade_out_width_bbox, ">0"); // [bbox]
    } else if (file["fade_out_width_Rvir"]) {
      load("fade_out_width_Rvir", fade_out_width_Rvir, ">0"); // [Rvir]
    } else {
      load("fade_out_width", fade_out_width, ">0"); // [cm]
    }
  }

  // List of fields
  std::set<FieldWeightPair> proj_set;
  if (file["field_weight_pairs"]) {
    vector<std::pair<string,string>> field_weight_pairs;
    load("field_weight_pairs", field_weight_pairs); // Projection fields and weights
    for (const auto& fw : field_weight_pairs)
      safe_insert(proj_set, fw);             // Save unique pairs
  } else if (file["field_weight_hists"]) {
    vector<tuple<string,string,string>> field_weight_hists;
    load("field_weight_hists", field_weight_hists); // Projection fields, weights, and histograms
    for (const auto& fw : field_weight_hists)
      safe_insert(proj_set, fw);             // Save unique triplets
  } else {
    strings projection_fields;               // Projection field names
    if (file["fields"]) {
      load("fields", projection_fields);     // Projection fields
    } else {
      projection_fields = { "Density" };     // Default denisty projection
    }

    strings projection_weights;              // Projection weight names
    if (file["weights"])
      load("weights", projection_weights);   // Projection weights

    strings projection_hists;                // Projection histogram names
    if (file["hists"])
      load("hists", projection_hists);       // Projection histograms

    const size_t n_proj = projection_fields.size();
    while (projection_weights.size() > n_proj)
      projection_weights.pop_back();         // Remove extra weights
    while (projection_weights.size() < n_proj)
      projection_weights.push_back("avg");   // Default to volume averages

    while (projection_hists.size() > n_proj)
      projection_hists.pop_back();           // Remove extra hists
    while (projection_hists.size() < n_proj)
      projection_hists.push_back("none");    // Default to no histograms

    // Remove duplicate fields
    for (size_t i = 0; i < n_proj; ++i)
      safe_insert(proj_set, projection_fields[i], projection_weights[i], projection_hists[i]);
  }

  // Add related weight sums and set up histogram information
  std::set<int> proj_set_mass_hists, hist_set;
  std::set<FieldWeightPair> proj_set_weights;
  for (auto& fw : proj_set) {
    if (fw.weight == MASS)
      proj_set_mass_hists.insert(fw.hist);   // Save histogram field (for mass-weighted fields)
    else if (fw.weight >= 0)
      safe_insert(proj_set_weights, Density, fw.weight, fw.hist); // Retain {weight, hist} labels
    if (fw.hist >= 0)
      hist_set.insert(fw.hist);              // Save histogram field
  }
  if (!proj_set_mass_hists.empty()) {
    for (auto& hist : proj_set_mass_hists)
      safe_insert(proj_set, Density, AVG, hist); // Require: sum(rho dl)
  }
  for (auto& fw : proj_set_weights)
    safe_insert(proj_set, fw.weight, SUM, fw.hist); // Require: sum(w dl/V)

  // Save unique projections list
  n_projections = proj_set.size();           // Number of projections
  proj_list.reserve(n_projections);          // Reserve list size
  for (auto& fw : proj_set) {
    fields[fw.field].read = true;            // Ensure field is active
    if (fw.weight >= 0)                      // Avoid weight alias range
      fields[fw.weight].read = true;         // Ensure weight is active
    if (fw.hist >= 0)                        // Avoid hist alias range
      fields[fw.hist].read = true;           // Ensure hist is active
    else if (fw.hist != OUTSIDE)             // Invalid histogram field
      root_error("Invalid histogram field: " + to_string(fw.hist));
    proj_list.push_back(fw);                 // Copy to projections list
  }

  // Determine opacity properties
  load("kappa_field", kappa_field_name);     // Absorption opacity field name
  if (kappa_field_name != "") {
    kappa_flag = true;                       // Activate absorption flag
    kappa_field_key = key_of_known_field(kappa_field_name);
    fields[kappa_field_key].read = true;     // Mark opacity field as active
    load("kappa_constant", kappa_constant, ">0"); // Absorption opacity coefficient
    load("kappa_exponent", kappa_exponent);  // Opacity exponent
    load("rho_exponent", rho_exponent);      // Density exponent
    if (adaptive)
      root_error("kappa_field is not (yet) compatible with adaptive");
  }

  // Field dependencies
  const int Z_offset = CarbonMetallicity - CarbonDensity;
  for (int field = CarbonDensity; field <= IronDensity; ++field) {
    if (fields[field].read)
      fields[field + Z_offset].read = true;  // Number density requires metallicity
  }
  if (fields[MagneticFieldSquared].read || fields[MagneticFieldLOS].read ||
      fields[MagneticFieldX].read || fields[MagneticFieldY].read)
    read_B = true;
  if (fields[IonizationFront].read)
    fields[HI_Fraction].read = true;
  fields[Density].read = true;               // Density is always active
  fields[Volume].read = true;                // Volume is always active
  gas_config(file);                          // General gas configuration

  // Save active fields
  for (int field = 0; field < n_fields; ++field)
    if (fields[field].read)
      active_fields.push_back(field);        // Save list of active fields
  n_active_fields = active_fields.size();    // Save number of active fields

  // Histogram binning configuration
  n_hists = hist_set.size();                 // Number of histograms
  if (n_hists > 0) {
    if (n_slices > 1)
      root_error("n_slices is not compatible with histograms!");
    // Save unique histograms list
    hist_list.reserve(n_hists);              // Reserve list size
    for (auto& hist : hist_set)
      hist_list.push_back(hist);             // Copy to histograms list

    // Build a lookup table for histogram fields
    auto hist_key_from_field = vector<int>(n_fields, OUTSIDE); // Lookup table
    for (int i = 0; i < n_hists; ++i) {
      const int hist = hist_list[i];         // Histogram field
      if (hist < 0 || hist >= n_fields)
        root_error("Invalid histogram field: " + to_string(hist));
      hist_key_from_field[hist] = i;         // Save index in lookup table
    }

    // Save histogram keys
    hist_keys = vector<int>(n_projections, OUTSIDE); // Histogram keys
    for (int i = 0; i < n_projections; ++i) {
      const int hist = proj_list[i].hist;    // Histogram field
      if (hist < 0 || hist >= n_fields)
        root_error("Invalid histogram field: " + to_string(hist));
      hist_keys[i] = hist_key_from_field[hist]; // Save index
    }

    // Load histogram configuration
    int override_n_bins = -1;                // Override number of bins
    load("n_bins", override_n_bins);
    hist_logs.resize(n_hists);               // Histogram log flags
    hist_n_bins.resize(n_hists);             // Histogram number of bins
    hist_mins.resize(n_hists);               // Histogram min values
    hist_maxs.resize(n_hists);               // Histogram max values
    hist_inv_widths.resize(n_hists);         // Histogram inverse widths
    for (int i = 0; i < n_hists; ++i) {
      const int hist = hist_list[i];         // Histogram field
      const string name = fields[hist].name; // Histogram field name
      if (file["linspace_"+name]) {
        hist_logs[i] = false;                // Linear spacing
        Vec3 vals;                           // Parse as (min, max, n_bins)
        load("linspace_"+name, vals);        // Load values
        hist_mins[i] = vals[0];              // Min value
        hist_maxs[i] = vals[1];              // Max value
        hist_n_bins[i] = vals[2];            // Number of bins
      } else if (file["logspace_"+name]) {
        hist_logs[i] = true;                 // Logarithmic spacing
        Vec3 vals;                           // Parse as (log10(min), log10(max), n_bins)
        load("logspace_"+name, vals);        // Load values
        hist_mins[i] = vals[0];              // Min value
        hist_maxs[i] = vals[1];              // Max value
        hist_n_bins[i] = vals[2];            // Number of bins
      } else {
        bool log_flag = false;               // Default to linear spacing
        int hist_n = 100;                    // Default number of bins
        double hist_min = 0., hist_max = 0.; // Default to linear spacing
        load("log_"+name, log_flag);         // Logarithmic spacing flag
        if (file["log_min_"+name]) {
          load("log_min_"+name, hist_min);   // Logarithmic min value
          if (!log_flag)
            hist_min = pow(10., hist_min);   // Convert to linear
        } else {
          load("min_"+name, hist_min);       // Linear min value
          if (log_flag) {
            if (hist_min <= 0.) {
              root_error("Invalid min_" + name + " value for log scale: " + to_string(hist_min));
            }
            hist_min = log10(hist_min);      // Convert to log10
          }
        }
        if (file["log_max_"+name]) {
          load("log_max_"+name, hist_max);   // Logarithmic max value
          if (!log_flag)
            hist_max = pow(10., hist_max);   // Convert to linear
        } else {
          load("max_"+name, hist_max);       // Linear max value
          if (log_flag) {
            if (hist_max <= 0.) {
              root_error("Invalid max_" + name + " value for log scale: " + to_string(hist_max));
            }
            hist_max = log10(hist_max);      // Convert to log10
          }
        }
        if (override_n_bins > 0)
          hist_n = override_n_bins;          // Override number of bins
        else
          load("n_"+name+"_bins", hist_n, ">0"); // Histogram number of bins
        hist_logs[i] = log_flag;             // Logarithmic spacing flag
        hist_mins[i] = hist_min;             // Min value
        hist_maxs[i] = hist_max;             // Max value
        hist_n_bins[i] = hist_n;             // Number of bins
      }
    }
  }

  // Setup projection units
  projection_units.resize(n_projections);
  for (int i = 0; i < n_projections; ++i)
    projection_units[i] = units_of_known_field(proj_list[i].field);
}
