/******************************
 * projections/projections.cc *
 ******************************

 * Driver: Assign rays for projections, etc.

*/

#include "../proto.h"
#include "Projections.h"
#include "../timing.h" // Timing functionality

extern Timer proj_timer; // Clock timing
bool avoid_cell_strict(const int cell); // Avoid calculations without overrides
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

// Quad tree data structure
struct QuadTreeNode {
  int children;                              // Index of the first child node
  double rtol;                               // Node relative tolerance
  double xL, xR;                             // Node x range [xmin, xmax]
  double yL, yR;                             // Node y range [ymin, ymax]
  double fLL, fLR, fRL, fRR;                 // Pass pre-calculated values to children
  double fc;                                 // Cumulative integral over node region

  QuadTreeNode() = default;
  QuadTreeNode(double rtol, double xL, double xR, double yL, double yR,
               double fLL, double fLR, double fRL, double fRR) :
               children(-1), rtol(rtol), xL(xL), xR(xR), yL(yL), yR(yR),
               fLL(fLL), fLR(fLR), fRL(fRL), fRR(fRR), fc(0.) {};
};

static int start_cell;                       // Starting cell for searches
#pragma omp threadprivate(start_cell)        // Each thread needs start_cell
static Cube corners;                         // Pixel corner values (3D)
static vector<vector<QuadTreeNode>> trees;   // Quad tree arrangement of nodes
static double V_col;                         // Projection column volume [cm^3]
static double slice_depth;                   // Slice depth [cm]
static double inverse_slice_depth;           // Inverse projection slice depth [1/cm]
static double inverse_slice_radius;          // Inverse projection slice radius [1/cm]
static Vec3 bbox_min;                        // Box min + epsilon [cm]
static Vec3 bbox_max;                        // Box max - epsilon [cm]
static vector<double> weighted_field;        // Integration field
static vector<double> k_abs;                 // Absorption coefficient
static int projection;                       // Projection index
static int camera;                           // Camera index
static Vec3 direction;                       // Camera direction
static Vec3 xaxis;                           // Camera x-axis
static Vec3 yaxis;                           // Camera y-axis

/* Calculate projections for a single line of sight. */
double Projections::ray_trace_single_field(const double rx, const double ry, const int iz) {
  // Set the ray starting point based on (rx,ry,image_zedges[iz])
  const double rz_beg = image_zedges[iz];    // Beginning of slice
  const double rz_end = image_zedges[iz+1];  // End of slice
  Vec3 point = {camera_center.x + rx * xaxis.x
                                + ry * yaxis.x
                                + rz_beg * direction.x,
                camera_center.y + rx * xaxis.y
                                + ry * yaxis.y
                                + rz_beg * direction.y,
                camera_center.z + rx * xaxis.z
                                + ry * yaxis.z
                                + rz_beg * direction.z};
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell
  int next_cell;                             // Next cell index
  double dl, l_stop = rz_end - rz_beg;       // Path lengths [cm]
  double result = 0.;                        // Integration result

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }
    result += weighted_field[cell] * dl;     // Volume-weighted integration
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        break;
    }
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += direction * dl;                 // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  return result * inverse_slice_depth;       // Divide by the projected slice depth
}

// Adaptive quadrature 2D integrator
double Projections::pixel_quad_2D(const int ix, const int iy, const int iz) {
  // Additional quadtree variables
  double rtol, xL, xM, xR, yL, yM, yR;
  double fLL, fLR, fRL, fRR, fLM, fML, fMM, fMR, fRM, f_trap, f_simp;
  auto& tree = trees[thread];                // Reference to the local tree

  // Initialize the base node
  int n_nodes = 1;                           // Initialize the node counter
  tree.resize(0);                            // Clear the tree (retain capacity)
  tree.emplace_back(                         // Start with no children
    pixel_rtol,                              // Base tolerance is target tolerance
    image_xedges[ix], image_xedges[ix+1],    // Domain: x in (xL,xR)
    image_yedges[iy], image_yedges[iy+1],    // Domain: y in (yL,yR)
    corners(ix,iy,iz), corners(ix,iy+1,iz),  // LL, LR corners
    corners(ix+1,iy,iz), corners(ix+1,iy+1,iz) // RL, RR corners, cumulative integral
  );

  // Populate the quadtree
  for (int i = 0; i < n_nodes; ++i) {        // Note: n_nodes changes size
    rtol = tree[i].rtol;                     // Relative tolerance
    xL = tree[i].xL;  xR = tree[i].xR;       // Domain x edges
    yL = tree[i].yL;  yR = tree[i].yR;       // Domain y edges
    fLL = tree[i].fLL;  fLR = tree[i].fLR;   // LL, LR corners
    fRL = tree[i].fRL;  fRR = tree[i].fRR;   // RL, RR corners
    xM = 0.5 * (xL + xR);                    // Local node x midpoint
    yM = 0.5 * (yL + yR);                    // Local node y midpoint
    fLM = ray_trace_single_field(xL, yM, iz); // Midpoint evaluations
    fML = ray_trace_single_field(xM, yL, iz);
    fMM = ray_trace_single_field(xM, yM, iz);
    fMR = ray_trace_single_field(xM, yR, iz);
    fRM = ray_trace_single_field(xR, yM, iz);

    // Note: Estimates are multiplied by 36/Area for convenience and corrected later
    f_trap = fLL + fLR + fRL + fRR;          // Trapezoid rule
    f_simp = f_trap + 4.*(fLM + fML + fMR + fRM) + 16.*fMM; // Simpson's rule
    // Check whether the node is converged
    if (fabs(9.*f_trap - f_simp) <= rtol * fabs(f_simp)) {
      tree[i].fc = f_simp;                   // Save converged value
      continue;                              // No need to refine this node
    }

    // Refine the current node
    tree[i].children = n_nodes;              // Append children to the end
    n_nodes += 4;                            // Add four children nodes
    const double child_rtol = 2. * rtol;     // Factor of sqrt(4) per refinement level

    //  Layout of children:
    //
    // (yR) fLR-----fMR-----fRR
    //       |       |       |
    //       |   2   |   4   |
    //       |       |       |
    // (yM) fLM-----fMM-----fRM
    //       |       |       |
    //       |   1   |   3   |
    //       |       |       |
    // (yL) fLL-----fML-----fRL
    //
    //      (xL)    (xM)    (xR)

    // Initialize 1st child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yL, yM,                        // (xL,xR,yL,yR) = (xL,xM,yL,xM)
      fLL, fLM, fML, fMM                     // (LL,LR,RL,RR) = (LL,LM,ML,MM)
    );

    // Initialize 2nd child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yM, yR,                        // (xL,xR,yL,yR) = (xL,xM,yM,xR)
      fLM, fLR, fMM, fMR                     // (LL,LR,RL,RR) = (LM,LR,MM,MR)
    );

    // Initialize 3rd child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yL, yM,                        // (xL,xR,yL,yR) = (xM,xR,yL,yM)
      fML, fMM, fRL, fRM                     // (LL,LR,RL,RR) = (ML,MM,RL,RM)
    );

    // Initialize 4th child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yM, yR,                        // (xL,xR,yL,yR) = (xM,xR,yM,yR)
      fMM, fMR, fRM, fRR                     // (LL,LR,RL,RR) = (MM,MR,RM,RR)
    );
  }

  // Aggregation step for converged sub-domain integrals
  for (int ci, i = n_nodes - 1; i >= 0; --i) {
    ci = tree[i].children;
    if (ci > 0) // The node has children (i.e. not a leaf node)
      tree[i].fc = 0.25 * (tree[ci].fc + tree[ci+1].fc + tree[ci+2].fc + tree[ci+3].fc);
  }

  // Note: Simpson estimate was multiplied by 36/Area for efficiency
  return tree[0].fc / 36.;
}

/* Ray trace for projection calculations. */
void Projections::calculate_projections() {
  // Pre-calculate the pixel corner values
  const int nxp1 = nx_pixels + 1;            // Number of x pixel edges
  const int nyp1 = ny_pixels + 1;            // Number of y pixel edges
  const int n2_corners = nyp1 * n_slices;    // Total number of corners (2D)
  const int n3_corners = nxp1 * n2_corners;  // Total number of corners (3D)
  const int corner_interval = (n3_corners < 200) ? 1 : n3_corners / 100; // Interval between updates
  int n_finished = 0;                        // Progress for printing
  if (root)
    cout << "\n    Pixel corner calculations:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (int i3 = 0; i3 < n3_corners; ++i3) {
    // Ray tracing for each corner
    const int ix = i3 / n2_corners;          // x index
    const int i2 = i3 - ix * n2_corners;     // 2D index
    const int iy = i2 / n_slices;            // y index
    const int iz = i2 - iy * n_slices;       // z index
    const double rx = image_xedges[ix];      // Corner position
    const double ry = image_yedges[iy];
    corners(ix,iy,iz) = ray_trace_single_field(rx,ry,iz);

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % corner_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n3_corners) << "%\b\b\b\b" << std::flush;
    }
  }

  // Perform adaptive convergence for all pixels
  if (root)
    cout << "100%\n    Pixel convergence calculations:   0%\b\b\b\b" << std::flush;
  const int n2_slices = ny_pixels * n_slices; // Total number of pixels (2D)
  const int n3_pixels = nx_pixels * n2_slices; // Total number of pixels (3D)
  const int pixel_interval = (n3_pixels < 200) ? 1 : n3_pixels / 100; // Interval between updates
  n_finished = 0;                            // Reset progress counter
  #pragma omp parallel for schedule(dynamic)
  for (int i3 = 0; i3 < n3_pixels; ++i3) {
    // Ray tracing for each pixel
    const int ix = i3 / n2_slices;           // x index
    const int i2 = i3 - ix * n2_slices;      // 2D index
    const int iy = i2 / n_slices;            // y index
    const int iz = i2 - iy * n_slices;       // z index
    if (n_slices == 1)
      projections[projection][camera](ix,iy) = pixel_quad_2D(ix, iy, iz);
    else
      proj_3D[projection][camera](ix,iy,iz) = pixel_quad_2D(ix, iy, iz);

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % pixel_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n3_pixels) << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Update fields that depend on the line of sight. */
static inline void update_LOS_field(vector<double>& field, const int fw_field) {
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (!avoid_cell_strict(i)) {
      switch (fw_field) {
        case VelocitySquaredLOS: field[i] = v[i].dot2(direction); break; // Velocity_LOS^2 [cm^2/s^2]
        case VelocityLOS: field[i] = v[i].dot(direction); break; // Velocity_LOS [cm/s]
        case VelocityX: field[i] = v[i].dot(xaxis); break; // Velocity_X [cm/s]
        case VelocityY: field[i] = v[i].dot(yaxis); break; // Velocity_Y [cm/s]
        case MagneticFieldLOS: field[i] = B[i].dot(direction); break; // Magnetic field LOS [G]
        case MagneticFieldX: field[i] = B[i].dot(xaxis); break; // Magnetic field X [G]
        case MagneticFieldY: field[i] = B[i].dot(yaxis); break; // Magnetic field Y [G]
        default: break;                      // No camera direction dependence
      }
    } else {
      field[i] = 0.;                         // Transparent for avoided cells
    }
  }
}


/* Test whether a given field is a LOS field. */
static inline bool check_LOS_field(const int field) {
  return (field == VelocitySquaredLOS || field == VelocityLOS
    || field == VelocityX || field == VelocityY || field == MagneticFieldLOS
    || field == MagneticFieldX || field == MagneticFieldY);
}

/* Test whether a given field is a perpendicular field. */
static inline bool check_perp_field(const int field) {
  return (field == VelocityX || field == VelocityY || field == MagneticFieldX || field == MagneticFieldY);
}

/* Test whether there are any perpendicular field. */
inline bool Projections::check_perp_fields() {
  for (auto& fw : proj_list) {               // Field-wieght indices
    if (check_perp_field(fw.field))
      return true;                           // At least one perp field
    if (check_perp_field(fw.weight))
      return true;                           // At least one perp field
    if (check_perp_field(fw.hist))
      return true;                           // At least one perp field
  }
  return false;                              // No perp fields
}

/* Run adaptive projections. */
void Projections::run_adaptive(const int start_cam, const int end_cam) {
  // Allocate pixel corner values and quad trees
  corners = Cube(nx_pixels+1, ny_pixels+1, n_slices); // Pixel corner values (3D)
  trees.resize(n_threads);                   // Each thread has its own tree
  #pragma omp parallel
  trees[thread].reserve(4097);               // Reserve reasonable tree sizes

  // Setup specific projection field and camera
  string field_name, weight_name;            // Field-wieght names
  weighted_field.resize(n_cells);            // Data for weighted fields
  for (projection = 0; projection < n_projections; ++projection) {
    const auto& fw = proj_list[projection];  // Field-wieght indices
    vector<double>& field = fields[fw.field].data;
    field_name = fields[fw.field].name;
    if (fw.weight == SUM)
      weight_name = "sum";
    else if (fw.weight == AVG)
      weight_name = "avg";
    else if (fw.weight == MASS)
      weight_name = "mass";
    else
      weight_name = fields[fw.weight].name;
    if (root)
      cout << "\nProjection: " << field_name << " [" << weight_name << "]";

    const bool LOS_field_flag = check_LOS_field(fw.field);
    const bool LOS_weight_flag = check_LOS_field(fw.weight);
    const bool LOS_flag = (LOS_field_flag || LOS_weight_flag);

    for (camera = start_cam; camera < end_cam; ++camera) {
      direction = camera_directions[camera]; // Camera direction
      xaxis = camera_xaxes[camera];          // Camera x-axis
      yaxis = camera_yaxes[camera];          // Camera y-axis
      if (root) {
        cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
        if (n_ranks > 1)
          cout << " (root)";
      }

      // Some fields depend on the camera direction
      if (LOS_field_flag)
        update_LOS_field(field, fw.field);

      // Calculations are determined by the weight
      if (LOS_flag || camera == start_cam)
        switch (fw.weight) {
          case SUM:                          // Conserved integral projection
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i)
              weighted_field[i] = avoid_cell_strict(i) ? 0. : field[i] * V_col / VOLUME(i); // Transform to density-like field
            break;
          case AVG:                          // Volume-weighted average
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i)
              weighted_field[i] = avoid_cell_strict(i) ? 0. : field[i]; // Already a density-like field
            break;
          case MASS:                         // Mass-weighted average
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i)
              weighted_field[i] = avoid_cell_strict(i) ? 0. : field[i] * rho[i]; // Include mass-weighting
            break;
          default:                           // Field-weighted average
            vector<double>& weight = fields[fw.weight].data;
            if (LOS_weight_flag)
              update_LOS_field(weight, fw.weight); // Ensure updated LOS weight fields
            #pragma omp parallel for
            for (int i = 0; i < n_cells; ++i)
              weighted_field[i] = avoid_cell_strict(i) ? 0. : field[i] * weight[i] * V_col / VOLUME(i); // Transform to weighted field
            break;
        }

      // Perform the actual projection calculations
      calculate_projections();
    }
  }
  weighted_field = vector<double>();         // Free memory
}

/* Run VR monoscopic projections. */
void Projections::run_monoscopic() {
  camera = 0;                                // Monoscopic camera
  const bool perp_flag = check_perp_fields(); // Perpendicular fields need to be updated
  if (root) {
    cout << "\nStarted monoscopic calculations:";
    if (n_ranks > 1)
      cout << " (root)";
  }

  // Perform projection calculations for all pixels
  if (root)
    cout << "\n  Pixel calculations:   0%\b\b\b\b" << std::flush;
  const int n2_pixels = nx_pixels * ny_pixels; // Total number of pixels
  const int pixel_interval = (n2_pixels < 200) ? 1 : n2_pixels / 100; // Interval between updates
  int n_finished = 0;                        // Progress for printing
  #pragma omp parallel
  {
    auto results = vector<double>(n_projections);
    #pragma omp for schedule(dynamic)
    for (int i2 = 0; i2 < n2_pixels; ++i2) {
      // Ray tracing for each pixel
      const int ix = i2 / ny_pixels;         // Pixel index along x-axis
      const int iy = i2 - ix * ny_pixels;    // Pixel index along y-axis
      const double ixh = double(ix) + 0.5;   // Pixel center along x-axis
      const double iyh = double(iy) + 0.5;   // Pixel center along y-axis

      // Calculate spherical coordinates direction for the pixel
      const double theta = 2. * M_PI * ixh / double(nx_pixels) - M_PI; // -π to π
      const double phi = M_PI * iyh / double(ny_pixels) - M_PI / 2.; // -π/2 to π/2

      // Calculate ray direction in Cartesian coordinates (computer graphics convention)
      const double cos_phi = cos(phi);
      const double x = cos_phi * sin(theta);
      const double y = sin(phi);
      const double z = cos_phi * cos(theta);

      // Transform ray direction to camera space
      Vec3 ray_dir = {x, y, z};
      ray_dir.normalize();

      // Perform ray tracing with the new ray direction (all fields)
      ray_trace_monoscopic_all_fields(results, ray_dir, perp_flag);
      for (int i = 0; i < n_projections; ++i)
        projections[i][camera](ix,iy) += results[i]; // Save final results

      // Print completed progress
      if (root) {
        int i_finished;                      // Progress counter
        #pragma omp atomic capture
        i_finished = ++n_finished;           // Update finished counter
        if (i_finished % pixel_interval == 0)
          cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n2_pixels) << "%\b\b\b\b" << std::flush;
      }
    }
  }
  if (root)
    cout << "100%" << endl;
}

static inline bool cannot_move_into_bbox(Vec3& point, const Vec3& direction, double& rz_beg) {
  // First check if the point is inside the bounding box
  if (point.x >= bbox_min.x && point.x <= bbox_max.x &&
      point.y >= bbox_min.y && point.y <= bbox_max.y &&
      point.z >= bbox_min.z && point.z <= bbox_max.z)
    return false;                            // Already inside the box
  // Check the distance to the X face
  if (point.x < bbox_min.x) {                // Outside left boundary
    if (direction.x <= 0.)                   // Heading away
      return true;                           // Ray misses the box
    const double dl = (bbox_min.x - point.x) / direction.x;
    if (dl > slice_depth)                    // Check for slice depth violation
      return true;
    rz_beg += dl;                            // Update starting coordinate
    point += direction * dl;                 // Move to the new position
    if (point.y >= bbox_min.y && point.y <= bbox_max.y &&
        point.z >= bbox_min.z && point.z <= bbox_max.z)
      return false;                          // Successfully entered the box
  } else if (point.x > bbox_max.x) {         // Outside Right boundary
    if (direction.x >= 0.)                   // Heading away
      return true;                           // Ray misses the box
    const double dl = (bbox_max.x - point.x) / direction.x;
    if (dl > slice_depth)                    // Check for slice depth violation
      return true;
    rz_beg += dl;                            // Update starting coordinate
    point += direction * dl;                 // Move to the new position
    if (point.y >= bbox_min.y && point.y <= bbox_max.y &&
        point.z >= bbox_min.z && point.z <= bbox_max.z)
      return false;                          // Successfully entered the box
  }
  // Check the distance to the Y face
  if (point.y < bbox_min.y) {                // Outside left boundary
    if (direction.y <= 0.)                   // Heading away
      return true;                           // Ray misses the box
    const double dl = (bbox_min.y - point.y) / direction.y;
    if (dl > slice_depth)                    // Check for slice depth violation
      return true;
    rz_beg += dl;                            // Update starting coordinate
    point += direction * dl;                 // Move to the new position
    if (point.x >= bbox_min.x && point.x <= bbox_max.x &&
        point.z >= bbox_min.z && point.z <= bbox_max.z)
      return false;                          // Successfully entered the box
  } else if (point.y > bbox_max.y) {         // Outside Right boundary
    if (direction.y >= 0.)                   // Heading away
      return true;                           // Ray misses the box
    const double dl = (bbox_max.y - point.y) / direction.y;
    if (dl > slice_depth)                    // Check for slice depth violation
      return true;
    rz_beg += dl;                            // Update starting coordinate
    point += direction * dl;                 // Move to the new position
    if (point.x >= bbox_min.x && point.x <= bbox_max.x &&
        point.z >= bbox_min.z && point.z <= bbox_max.z)
      return false;                          // Successfully entered the box
  }
  // Check the distance to the Z face
  if (point.z < bbox_min.z) {                // Outside left boundary
    if (direction.z <= 0.)                   // Heading away
      return true;                           // Ray misses the box
    const double dl = (bbox_min.z - point.z) / direction.z;
    if (dl > slice_depth)                    // Check for slice depth violation
      return true;
    rz_beg += dl;                            // Update starting coordinate
    point += direction * dl;                 // Move to the new position
    if (point.x >= bbox_min.x && point.x <= bbox_max.x &&
        point.y >= bbox_min.y && point.y <= bbox_max.y)
      return false;                          // Successfully entered the box
  } else if (point.z > bbox_max.z) {         // Outside Right boundary
    if (direction.z >= 0.)                   // Heading away
      return true;                           // Ray misses the box
    const double dl = (bbox_max.z - point.z) / direction.z;
    if (dl > slice_depth)                    // Check for slice depth violation
      return true;
    rz_beg += dl;                            // Update starting coordinate
    point += direction * dl;                 // Move to the new position
    if (point.x >= bbox_min.x && point.x <= bbox_max.x &&
        point.y >= bbox_min.y && point.y <= bbox_max.y)
      return false;                          // Successfully entered the box
  }
  return true;                               // Corrections were unsuccessful
}

// Calculate the distance to escape the bounding box
static inline double face_distance_bbox(const Vec3& point, const Vec3& direction) {
  double l = positive_infinity, l_comp;      // Comparison distances
  // Check each direction
  for (int i = 0; i < 3; ++i) {
    if (direction[i] != 0.) {
      if (direction[i] > 0.) {               // Right face
        l_comp = (bbox_max[i] - point[i]) / direction[i];
        if (l_comp < l)
          l = l_comp;
      } else {                               // Left face
        l_comp = (bbox_min[i] - point[i]) / direction[i];
        if (l_comp < l)
          l = l_comp;
      }
    }
  }
  return l;                                  // Minimum distance
}

/* Calculate projections for a single line of sight (all fields). */
void Projections::ray_trace_all_fields(vector<vector<double>>& results, const double rx, const double ry, const int iz) {
  for (int i = 0; i < n_projections; ++i) {
    auto& result_i = results[i];             // Integration results
    const int n_results = result_i.size();   // Number of results
    for (int j = 0; j < n_results; ++j)
      result_i[j] = 0.;                      // Integration results
  }
  // Set the ray starting point based on (rx,ry,rz_beg)
  double rz_beg, rz_end;                     // Starting and ending depths
  if (proj_sphere) {                         // Restrict to a spherical volume
    const double rz2 = proj_radius*proj_radius - rx*rx - ry*ry; // Half depth^2
    if (rz2 > 0.) {
      const double rz = sqrt(rz2);           // Projection half depth
      rz_beg = -rz;                          // Account for sphere depth
      rz_end = rz;
    } else {
      return;                                // Ignore rays missing the sphere
    }
  } else if (proj_cylinder) {                // Restrict to a cylindrical volume
    const double rz2 = proj_radius*proj_radius - rx*rx; // Half depth^2
    if (rz2 > 0.) {
      const double rz = sqrt(rz2);           // Projection half depth
      rz_beg = -rz;                          // Account for cylinder depth
      rz_end = rz;
    } else {
      return;                                // Ignore rays missing the cylinder
    }
  } else {
    rz_beg = -proj_radius;                   // Box projection
    rz_end = proj_radius;
  }
  if (rz_beg < image_zedges[iz])
    rz_beg = image_zedges[iz];               // Start at the beginning of slice
  if (rz_end > image_zedges[iz+1])
    rz_end = image_zedges[iz+1];             // End at the end of slice
  Vec3 point = {camera_center.x + rx * xaxis.x
                                + ry * yaxis.x
                                + rz_beg * direction.x,
                camera_center.y + rx * xaxis.y
                                + ry * yaxis.y
                                + rz_beg * direction.y,
                camera_center.z + rx * xaxis.z
                                + ry * yaxis.z
                                + rz_beg * direction.z};
  // Ensure rays start within the bounding box (returns a flag to exclude rays)
  if (cannot_move_into_bbox(point, direction, rz_beg))
    return;                                  // Ignore rays missing the target
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell
  // Calculate the escape distance the bounding box (update rz_end if needed)
  const double rz_end_bbox = rz_beg + face_distance_bbox(point, direction);
  if (rz_end_bbox < rz_end)
    rz_end = rz_end_bbox;                    // Shorter than destination
  int next_cell;                             // Next cell index
  double dl, l_stop = rz_end - rz_beg;       // Path lengths [cm]
  double tau = 0., exp_tau = 1.;             // Absorption optical depth
  double j_local = 0., dl_eff = 0.;          // Local emissivity and length
  const double V_ray = pixel_area * (override_volume ? slice_depth : l_stop); // Column volume

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }

    // Calculate absorption and emission constants
    if (kappa_flag) {
      tau = k_abs[cell] * dl;                // Absorption optical depth
      if (tau < 1e-5) {                      // Numerical stability
        exp_tau = 1. - tau;                  // Survival fraction
        dl_eff = (1. - 0.5 * tau) * dl;      // Emission factor
      } else {                               // Full transfer equation
        exp_tau = exp(-tau);                 // Survival fraction
        dl_eff = (1. - exp_tau) / k_abs[cell]; // Emission factor
      }
    } else {                                 // No absorption
      dl_eff = dl;                           // Note: exp_tau is always 1
    }

    const double dist_weight = fade ? get_distance_weight(rz_end - l_stop + 0.5*dl) : 1.;

    for (int i = 0; i < n_projections; ++i) {
      const auto& fw = proj_list[i];         // Field-wieght indices
      vector<double>& field = fields[fw.field].data;
      switch (fw.weight) {
        case SUM:                            // Conserved integral projection
          j_local = field[cell] * dist_weight * V_ray / VOLUME(cell); // Transform to density-like field
          break;
        case AVG:                            // Volume-weighted average
          j_local = field[cell] * dist_weight; // Already a density-like field
          break;
        case MASS:                           // Mass-weighted average
          j_local = field[cell] * dist_weight * rho[cell]; // Include mass-weighting
          break;
        default:                             // Field-weighted average
          vector<double>& weight = fields[fw.weight].data;
          j_local = field[cell] * dist_weight * weight[cell] * V_ray / VOLUME(cell); // Transform to weighted field
      }
      // Bin the field value
      int ibin = 0;                          // Default bin index
      if (n_hists > 0) {
        const int key = hist_keys[i];        // Histogram key
        const double hist_min = hist_mins[key]; // Histogram minimum
        const double hist_inv_width = hist_inv_widths[key]; // Inverse bin width
        double hist_value = fields[fw.hist].data[cell]; // Histogram field value
        if (hist_logs[key])                  // Logarithmic binning
          hist_value = (hist_value > 0.) ? log10(hist_value) : hist_min;
        ibin = floor(hist_inv_width * (hist_value - hist_min)); // Bin index (rounded down)
        if (ibin < 0)                        // Handle underflow
          ibin = 0;
        else if (ibin >= hist_n_bins[key])   // Handle overflow
          ibin = hist_n_bins[key] - 1;
      }
      results[i][ibin] *= exp_tau;           // Attenuate incoming flux
      results[i][ibin] += j_local * dl_eff;  // Add escaping emission
    }
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        break;
    }
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += direction * dl;                 // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  const double inverse_norm = override_volume ?
    inverse_slice_depth : 1. / (rz_end - rz_beg); // Inverse normalization
  for (int i = 0; i < n_projections; ++i) {
    auto& result_i = results[i];             // Integration results
    const int n_results = result_i.size();   // Number of bins
    for (int ibin = 0; ibin < n_results; ++ibin)
      result_i[ibin] *= inverse_norm;        // Divide by the projected depth
  }
}

/* Run non-adaptive projections. */
void Projections::run_standard(const int start_cam, const int end_cam) {
  // Set up minimal LOS set
  std::set<int> LOS_checks;
  for (int i = 0; i < n_projections; ++i) {
    const auto& fw = proj_list[i];           // Field-wieght indices
    if (check_LOS_field(fw.field))
      LOS_checks.insert(fw.field);
    if (fw.weight >= 0 && check_LOS_field(fw.weight))
      LOS_checks.insert(fw.weight);
    if (fw.hist >= 0 && check_LOS_field(fw.hist))
      LOS_checks.insert(fw.hist);
  }
  auto image_xcenters = vector<double>(nx_pixels); // Image x center positions [cm]
  auto image_ycenters = vector<double>(ny_pixels); // Image y center positions [cm]
  for (int i = 0; i < nx_pixels; ++i)
    image_xcenters[i] = 0.5 * (image_xedges[i] + image_xedges[i+1]);
  for (int i = 0; i < ny_pixels; ++i)
    image_ycenters[i] = 0.5 * (image_yedges[i] + image_yedges[i+1]);
  for (camera = start_cam; camera < end_cam; ++camera) {
    direction = camera_directions[camera];   // Camera direction
    xaxis = camera_xaxes[camera];            // Camera x-axis
    yaxis = camera_yaxes[camera];            // Camera y-axis
    if (root) {
      cout << "\nCamera progress: " << camera+1 << " / " << end_cam;
      if (n_ranks > 1)
        cout << " (root)";
    }

    for (auto LOS_check : LOS_checks)        // Ensure updated LOS fields and weights
      update_LOS_field(fields[LOS_check].data, LOS_check);

    // Perform projection calculations for all pixels
    if (root)
      cout << "\n  Pixel calculations:   0%\b\b\b\b" << std::flush;
    const int n2_slices = ny_pixels * n_slices; // Total number of pixels (2D)
    const int n3_pixels = nx_pixels * n2_slices; // Total number of pixels (3D)
    const int pixel_interval = (n3_pixels < 200) ? 1 : n3_pixels / 100; // Interval between updates
    int n_finished = 0;                      // Progress for printing
    #pragma omp parallel
    {
      auto results = vector<vector<double>>(n_projections); // Integration results
      if (n_hists > 0) {
        for (int i = 0; i < n_projections; ++i)
          results[i] = vector<double>(hist_n_bins[hist_keys[i]]);
      } else {
        for (int i = 0; i < n_projections; ++i)
          results[i] = vector<double>(n_slices);
      }
      #pragma omp for schedule(dynamic)
      for (int i3 = 0; i3 < n3_pixels; ++i3) {
        // Ray tracing for each pixel
        const int ix = i3 / n2_slices;       // x index
        const int i2 = i3 - ix * n2_slices;  // 2D index
        const int iy = i2 / n_slices;        // y index
        const int iz = i2 - iy * n_slices;   // z index
        const double rx = image_xcenters[ix]; // Pixel center position
        const double ry = image_ycenters[iy];
        ray_trace_all_fields(results, rx, ry, iz); // Single ray (all fields);
        if (n_hists > 0 || n_slices > 1) {
          for (int i = 0; i < n_projections; ++i) {
            auto& proj_i = proj_3D[i][camera];
            auto& results_i = results[i];
            const int n_results = results_i.size();
            for (int ibin = 0; ibin < n_results; ++ibin)
              proj_i(ix,iy,ibin) = results_i[ibin]; // Save final results
          }
        } else {
          for (int i = 0; i < n_projections; ++i)
            projections[i][camera](ix,iy) += results[i][0]; // Save final results
        }

        // Print completed progress
        if (root) {
          int i_finished;                    // Progress counter
          #pragma omp atomic capture
          i_finished = ++n_finished;         // Update finished counter
          if (i_finished % pixel_interval == 0)
            cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n3_pixels) << "%\b\b\b\b" << std::flush;
        }
      }
    }
    if (root)
      cout << "100%" << endl;
  }
}

/* Returns the value of a field for a given cell and direction. */
static inline double get_cell_value(const int fw_field, const int cell, const Vec3& k_LOS, const Vec3& k_xa, const Vec3& k_ya) {
  switch (fw_field) {
    case VelocitySquaredLOS: return v[cell].dot2(k_LOS); // Velocity_LOS^2 [cm^2/s^2]
    case VelocityLOS: return v[cell].dot(k_LOS); // Velocity_LOS [cm/s]
    case VelocityX: return v[cell].dot(k_xa); // Velocity_X [cm/s]
    case VelocityY: return v[cell].dot(k_ya); // Velocity_Y [cm/s]
    case MagneticFieldLOS: return B[cell].dot(k_LOS); // Magnetic field LOS [G]
    case MagneticFieldX: return B[cell].dot(k_xa); // Magnetic field X [G]
    case MagneticFieldY: return B[cell].dot(k_ya); // Magnetic field Y [G]
    default: return fields[fw_field].data[cell]; // No camera direction dependence
  }
}

/* Updates the values of the local perpendicular axes. */
static inline void update_axes(const Vec3& dir, Vec3& x_axis, Vec3& y_axis) {
  const double mu_north = dir.dot(camera_north); // North angular cosine
  if (1. - fabs(mu_north) > 1e-9) {          // Use north to build basis
    y_axis = camera_north - dir * mu_north;  // Orthogonal projection
  } else if (mu_north > 0.) {                // Degenerate case (+north)
    // Use the rotated y-axis as the new north vector
    const double CZ = camera_north.y / (1. + camera_north.z);
    Vec3 new_north = {-camera_north.x * CZ, 1. - camera_north.y * CZ, -camera_north.y};
    const double new_mu_north = dir.dot(new_north);
    y_axis = new_north - dir * new_mu_north; // Orthogonal projection (+y)
  } else {                                   // Degenerate case (-north)
    // Use the rotated x-axis as the new north vector
    const double CZ = camera_north.x / (1. + camera_north.z);
    Vec3 new_north = {1. - camera_north.x * CZ, -camera_north.y * CZ, -camera_north.x};
    const double new_mu_north = dir.dot(new_north);
    y_axis = new_north - dir * new_mu_north; // Orthogonal projection (+x)
  }
  y_axis.normalize();                        // Make it a unit vector
  x_axis = y_axis.cross(dir);                // Final orthogonal axis
  x_axis.normalize();                        // Ensure normalization
}

/* Calculate projections for a single line of sight (all fields). */
void Projections::ray_trace_monoscopic_all_fields(vector<double>& results, const Vec3& k_LOS, const bool perp_flag) {
  for (int i = 0; i < n_projections; ++i)
    results[i] = 0.;                         // Integration results
  // Set the ray starting point on a sphere at the camera center
  Vec3 point = camera_center - k_LOS * ((1. + stepback_factor) * proj_radius);
  Vec3 k_xa, k_ya;                           // Local perpendicular axes
  if (perp_flag)
    update_axes(k_LOS, k_xa, k_ya);          // Update the perpendicular axes
  double rz_beg = 0., rz_end = proj_radius;  // Starting and ending depths

  // Ensure rays start within the bounding box (returns a flag to exclude rays)
  if (cannot_move_into_bbox(point, k_LOS, rz_beg))
    return;                                  // Ignore rays missing the target
  // Calculate the escape distance the bounding box (update rz_end if needed)
  const double rz_end_bbox = rz_beg + face_distance_bbox(point, k_LOS);
  if (rz_end_bbox < rz_end)
    rz_end = rz_end_bbox;                    // Shorter than destination
  int next_cell;                             // Next cell index
  double dl, l_stop = rz_end - rz_beg;       // Path lengths [cm]
  double tau = 0., exp_tau = 1.;             // Absorption optical depth
  double j_local = 0., dl_eff = 0.;          // Local emissivity and length
  const double V_ray = pixel_area * (override_volume ? proj_radius : l_stop); // Column volume
  // Flip the ray for forward ray-tracing
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, k_LOS, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }

    // Calculate absorption and emission constants
    if (kappa_flag) {
      tau = k_abs[cell] * dl;                // Absorption optical depth
      if (tau < 1e-5) {                      // Numerical stability
        exp_tau = 1. - tau;                  // Survival fraction
        dl_eff = (1. - 0.5 * tau) * dl;      // Emission factor
      } else {                               // Full transfer equation
        exp_tau = exp(-tau);                 // Survival fraction
        dl_eff = (1. - exp_tau) / k_abs[cell]; // Emission factor
      }
    } else {                                 // No absorption
      dl_eff = dl;                           // Note: exp_tau is always 1
    }

    // Note: Using proj_radius - distance to avoid flipping the direction
    const double dist_weight = fade ? get_distance_weight(proj_radius - rz_beg - l_stop + 0.5*dl) : 1.;

    for (int i = 0; i < n_projections; ++i) {
      const auto& fw = proj_list[i];         // Field-wieght indices
      const double field_cell = get_cell_value(fw.field, cell, k_LOS, k_xa, k_ya);
      switch (fw.weight) {
        case SUM:                            // Conserved integral projection
          j_local = field_cell * dist_weight * V_ray / VOLUME(cell); // Transform to density-like field
          break;
        case AVG:                            // Volume-weighted average
          j_local = field_cell * dist_weight; // Already a density-like field
          break;
        case MASS:                           // Mass-weighted average
          j_local = field_cell * dist_weight * rho[cell]; // Include mass-weighting
          break;
        default:                             // Field-weighted average
          const double weight_cell = get_cell_value(fw.weight, cell, k_LOS, k_xa, k_ya);
          j_local = field_cell * dist_weight * weight_cell * V_ray / VOLUME(cell); // Transform to weighted field
      }
      results[i] *= exp_tau;                 // Attenuate incoming flux
      results[i] += j_local * dl_eff;        // Add escaping emission
    }
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        break;
    }
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += k_LOS * dl;                     // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  const double inverse_norm = override_volume ?
    inverse_slice_radius : 1. / (rz_end - rz_beg); // Inverse normalization
  for (int i = 0; i < n_projections; ++i)
    results[i] *= inverse_norm;              // Divide by the projected depth
}

/* Calculate projections for a single line of sight (all fields). */
void Projections::ray_trace_perspective_all_fields(vector<double>& results, const double rx, const double ry, const bool perp_flag) {
  for (int i = 0; i < n_projections; ++i)
    results[i] = 0.;                         // Integration results
  // Set the ray starting point at the camera center
  Vec3 point = camera_center - direction * (stepback_factor * proj_radius);
  // Set the ray direction based on the image coordinates (-rx,ry,proj_radius)
  Vec3 k_LOS = {-rx * xaxis.x + ry * yaxis.x + proj_radius * direction.x,
                -rx * xaxis.y + ry * yaxis.y + proj_radius * direction.y,
                -rx * xaxis.z + ry * yaxis.z + proj_radius * direction.z};
  k_LOS.normalize();                         // Normalized perspective direction
  Vec3 k_xa, k_ya;                           // Local perpendicular axes
  if (perp_flag)
    update_axes(k_LOS, k_xa, k_ya);          // Update the perpendicular axes
  double rz_beg = 0., rz_end = proj_radius;  // Starting and ending depths
  if (proj_sphere) {                         // Restrict to a spherical volume
    // Spherical propagation: dl = -k*r +/- sqrt(|k*r|^2 - r^2 + r_inner^2)
    const Vec3 p = point - camera_center;    // Recentered point
    const double mu_r = k_LOS.dot(p);        // Unnormalized radial cosine
    const double rp = shape_factor * proj_radius; // Sphere radius [cm]
    const double disc = rp*rp + mu_r*mu_r - p.dot(); // Discriminant
    if (disc <= 0.)
      return;                                // Ignore rays missing the sphere
    const double sqrt_disc = sqrt(disc);
    if (sqrt_disc <= mu_r)
      return;                                // Ignore rays missing the sphere
    const double dl = -sqrt_disc - mu_r;     // First distance
    if (dl > 0.) {
      rz_beg += dl;                          // Update starting coordinate
      point += k_LOS * dl;                   // Move to the new position
    }
    const double rz_end_sphere = rz_beg + sqrt_disc - mu_r;
    if (rz_end_sphere < rz_end)
      rz_end = rz_end_sphere;                // Shorter than destination
  } else if (proj_cylinder) {                // Restrict to a cylindrical volume
    // Cylindrical propagation: dl = [-B +/- sqrt(B^2 - 4*A*C)] / (2*A)
    const double rp = shape_factor * proj_radius; // Cylinder radius [cm]
    const Vec3 p = point - camera_center;    // Recentered point
    const Vec3 k_perp = k_LOS - camera_north * k_LOS.dot(camera_north);
    const Vec3 p_perp = p - camera_north * p.dot(camera_north);
    const double A = k_perp.dot();
    const double B = 2. * k_perp.dot(p_perp);
    const double C = p_perp.dot() - rp*rp;
    const double disc = B*B - 4.*A*C;        // Discriminant
    if (disc <= 0.)
      return;                                // Ignore rays missing the cylinder
    const double sqrt_disc = sqrt(disc);
    if (sqrt_disc <= B)
      return;                                // Ignore rays missing the cylinder
    const double dfac = 0.5 / A;             // Inverse denominator
    const double dl = dfac * (-sqrt_disc - B); // First distance
    if (dl > 0.) {
      rz_beg += dl;                          // Update starting coordinate
      point += k_LOS * dl;                   // Move to the new position
    }
    const double rz_end_cylinder = rz_beg + dfac * (sqrt_disc - B);
    if (rz_end_cylinder < rz_end)
      rz_end = rz_end_cylinder;              // Shorter than destination
  }
  // Ensure rays start within the bounding box (returns a flag to exclude rays)
  if (cannot_move_into_bbox(point, k_LOS, rz_beg))
    return;                                  // Ignore rays missing the target
  // Calculate the escape distance the bounding box (update rz_end if needed)
  const double rz_end_bbox = rz_beg + face_distance_bbox(point, k_LOS);
  if (rz_end_bbox < rz_end)
    rz_end = rz_end_bbox;                    // Shorter than destination
  int next_cell;                             // Next cell index
  double dl, l_stop = rz_end - rz_beg;       // Path lengths [cm]
  double tau = 0., exp_tau = 1.;             // Absorption optical depth
  double j_local = 0., dl_eff = 0.;          // Local emissivity and length
  const double V_ray = pixel_area * (override_volume ? proj_radius : l_stop); // Column volume
  // Flip the ray for forward ray-tracing
  point += k_LOS * l_stop;                   // Move to the end
  k_LOS *= -1.;                              // Reverse direction
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, k_LOS, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }

    // Calculate absorption and emission constants
    if (kappa_flag) {
      tau = k_abs[cell] * dl;                // Absorption optical depth
      if (tau < 1e-5) {                      // Numerical stability
        exp_tau = 1. - tau;                  // Survival fraction
        dl_eff = (1. - 0.5 * tau) * dl;      // Emission factor
      } else {                               // Full transfer equation
        exp_tau = exp(-tau);                 // Survival fraction
        dl_eff = (1. - exp_tau) / k_abs[cell]; // Emission factor
      }
    } else {                                 // No absorption
      dl_eff = dl;                           // Note: exp_tau is always 1
    }

    const double dist_weight = fade ? get_distance_weight(rz_beg + l_stop - 0.5*dl) : 1.;

    for (int i = 0; i < n_projections; ++i) {
      const auto& fw = proj_list[i];         // Field-wieght indices
      const double field_cell = get_cell_value(fw.field, cell, k_LOS, k_xa, k_ya);
      switch (fw.weight) {
        case SUM:                            // Conserved integral projection
          j_local = field_cell * dist_weight * V_ray / VOLUME(cell); // Transform to density-like field
          break;
        case AVG:                            // Volume-weighted average
          j_local = field_cell * dist_weight; // Already a density-like field
          break;
        case MASS:                           // Mass-weighted average
          j_local = field_cell * dist_weight * rho[cell]; // Include mass-weighting
          break;
        default:                             // Field-weighted average
          const double weight_cell = get_cell_value(fw.weight, cell, k_LOS, k_xa, k_ya);
          j_local = field_cell * dist_weight * weight_cell * V_ray / VOLUME(cell); // Transform to weighted field
      }
      results[i] *= exp_tau;                 // Attenuate incoming flux
      results[i] += j_local * dl_eff;        // Add escaping emission
    }
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        break;
    }
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += k_LOS * dl;                     // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Normalize the units to return the average
  const double inverse_norm = override_volume ?
    inverse_slice_radius : 1. / (rz_end - rz_beg); // Inverse normalization
  for (int i = 0; i < n_projections; ++i)
    results[i] *= inverse_norm;              // Divide by the projected depth
}

/* Run non-adaptive perspective projections. */
void Projections::run_perspective(const int start_cam, const int end_cam) {
  const bool perp_flag = check_perp_fields(); // Perpendicular fields need to be updated
  auto image_xcenters = vector<double>(nx_pixels); // Image x center positions [cm]
  auto image_ycenters = vector<double>(ny_pixels); // Image y center positions [cm]
  for (int i = 0; i < nx_pixels; ++i)
    image_xcenters[i] = 0.5 * (image_xedges[i] + image_xedges[i+1]);
  for (int i = 0; i < ny_pixels; ++i)
    image_ycenters[i] = 0.5 * (image_yedges[i] + image_yedges[i+1]);

  for (camera = start_cam; camera < end_cam; ++camera) {
    direction = camera_directions[camera];   // Camera direction
    xaxis = camera_xaxes[camera];            // Camera x-axis
    yaxis = camera_yaxes[camera];            // Camera y-axis
    if (root) {
      cout << "\nCamera progress: " << camera+1 << " / " << end_cam;
      if (n_ranks > 1)
        cout << " (root)";
    }

    // Perform projection calculations for all pixels
    if (root)
      cout << "\n  Pixel calculations:   0%\b\b\b\b" << std::flush;
    const int n2_pixels = nx_pixels * ny_pixels; // Total number of pixels
    const int pixel_interval = (n2_pixels < 200) ? 1 : n2_pixels / 100; // Interval between updates
    int n_finished = 0;                      // Progress for printing
    #pragma omp parallel
    {
      auto results = vector<double>(n_projections); // Integration results
      #pragma omp for schedule(dynamic)
      for (int i2 = 0; i2 < n2_pixels; ++i2) {
        // Ray tracing for each pixel
        const int ix = i2 / ny_pixels;       // Pixel ix,iy indices
        const int iy = i2 - ix * ny_pixels;
        const double rx = image_xcenters[ix]; // Pixel center position
        const double ry = image_ycenters[iy];
        ray_trace_perspective_all_fields(results, rx, ry, perp_flag); // Single ray (all fields)
        for (int i = 0; i < n_projections; ++i)
          projections[i][camera](ix,iy) = results[i]; // Save final results

        // Print completed progress
        if (root) {
          int i_finished;                    // Progress counter
          #pragma omp atomic capture
          i_finished = ++n_finished;         // Update finished counter
          if (i_finished % pixel_interval == 0)
            cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n2_pixels) << "%\b\b\b\b" << std::flush;
        }
      }
    }
    if (root)
      cout << "100%" << endl;
  }
}

/* Driver for projection calculations. */
void Projections::run() {
  proj_timer.start();

  // Set redundant projection parameters
  const int n_cam_rank = n_cameras / n_ranks; // Equal assignment
  const int remainder = n_cameras - n_cam_rank * n_ranks;
  int start_cam = rank * n_cam_rank;         // Camera start range
  int end_cam = (rank + 1) * n_cam_rank;     // Camera end range
  if (rank < remainder) {
    start_cam += rank;                       // Correct start range
    end_cam += (rank + 1);                   // Correct end range
  } else {
    start_cam += remainder;                  // Correct start range
    end_cam += remainder;                    // Correct end range
  }
  slice_depth = proj_depth / double(n_slices); // Slice depth
  V_col = slice_depth * pixel_area;          // Projection slice column volume
  inverse_slice_depth = 1. / slice_depth;    // Inverse projection slice depth
  inverse_slice_radius = 2. / slice_depth;   // Inverse projection slice radius
  bbox_min = bbox[0] + (bbox[1] - bbox[0]) * 1e-10; // Box min + epsilon [cm]
  bbox_max = bbox[1] - (bbox[1] - bbox[0]) * 1e-10; // Box max - epsilon [cm]
  #pragma omp parallel
  start_cell = 0;                            // Reset the starting cell

  // Initialize opacity field data
  if (kappa_flag) {
    k_abs.resize(n_cells);
    const vector<double>& field = fields[kappa_field_key].data;
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {      // const * kappa^alpha * rho^beta
      if (avoid_cell_strict(i))
        k_abs[i] = 0.;                       // Avoid certain cells
      else
        k_abs[i] = kappa_constant * pow(field[i], kappa_exponent) * pow(rho[i], rho_exponent);
    }
  }

  const vector<int>& halo_id = select_subhalo ? subhalo_id : group_id; // Halo ID
  const vector<Vec3>& r_halo = select_subhalo ? r_sub_vir : r_grp_vir; // Halo position
  const vector<double>& R_halo = select_subhalo ? R_sub_vir : R_grp_vir; // Halo radius
  const int n_halos = select_subhalo ? n_subhalos : n_groups; // Number of halos
  const int n_loops = use_all_halos ? n_halos : 1; // Number of loops
  for (int loop = 0; loop < n_loops; ++loop) {
    if (use_all_halos) {
      if (loop > 0) {
        halo_index = loop;                   // Halo index
        halo_num = halo_id[loop];            // Halo number
        halo_position = r_halo[loop];        // Halo center
        halo_radius = R_halo[loop];          // Halo radius
        setup();                             // Reset the projection setup
      }
      if (root) {
        int nx = n_pixels.x, ny = n_pixels.y;
        cout << "\nHalo progress: " << halo_index+1 << " / " << n_halos
             << "\n  Halo " << halo_num << ": r = " << halo_position/kpc
             << " kpc, image_width = " << image_radii/halo_radius << " R_halo"
             << " kpc, R = " << halo_radius/kpc << " kpc, n_pixels = "
             << (nx == ny ? to_string(nx) : to_string(n_pixels)) << endl;
      }
    }

    if (adaptive) {
      if (perspective) {
        root_error("Adaptive perspective camera projections are not implemented.");
      } else
        run_adaptive(start_cam, end_cam);    // Adaptive projections
    } else {
      if (perspective)
        run_perspective(start_cam, end_cam); // Non-adaptive perspective projections
      if (monoscopic)
        run_monoscopic();                    // Non-adaptive monoscopic projections
      else
        run_standard(start_cam, end_cam);    // Non-adaptive projections
    }

    // Correct the units for weighted projections (divide by weight sum)
    for (projection = 0; projection < n_projections; ++projection) {
      FieldWeightPair fw = proj_list[projection]; // Field-wieght indices
      if (fw.weight == SUM || fw.weight == AVG)
        continue;                            // Already in the correct units
      int denominator = -1;                  // Find the denominator index
      int den_field = -1, den_weight = -1;   // Denominator field and weight
      if (fw.weight == MASS) {
        den_field = Density;                 // Mass-weighting is defined as:
        den_weight = AVG;                    // <f>_mass = sum(f rho dl) / sum(rho dl)
      } else {
        den_field = fw.weight;               // Field-weighting is defined as:
        den_weight = SUM;                    // <f>_w = sum(f w dl/V) / sum(w dl/V)
      }
      for (int i = 0; i < n_projections; ++i) {
        const auto& dfw = proj_list[i];      // Denominator field-wieght indices
        if (dfw.field == den_field && dfw.weight == den_weight && dfw.hist == fw.hist)
          denominator = i;
      }
      if (denominator == -1)
        error("Missing the projection weight denominator.");
      const bool is_3d = n_hists > 0 || n_slices > 1; // 3D projection flag
      for (camera = start_cam; camera < end_cam; ++camera) {
        int size_n, size_d;                  // Number of elements
        double *data_n, *data_d;             // Data pointers
        if (is_3d) {
          size_n = proj_3D[projection][camera].size();
          data_n = proj_3D[projection][camera].data();
          size_d = proj_3D[denominator][camera].size();
          data_d = proj_3D[denominator][camera].data();
        } else {
          size_n = projections[projection][camera].size();
          data_n = projections[projection][camera].data();
          size_d = projections[denominator][camera].size();
          data_d = projections[denominator][camera].data();
        }
        if (size_n != size_d)
          error("Projection data size mismatch: " + to_string(size_n) + " != " + to_string(size_d));
        #pragma omp parallel for
        for (int i = 0; i < size_n; ++i) {
          if (data_d[i] != 0.)
            data_n[i] /= data_d[i];          // Weight normalization
        }
      }
    }

    // Reduce all projections data to root
    if (n_ranks > 1) {
      auto mpi_proj = [&](auto& data) {
        for (auto& ps : data) {
          if (root) {
            for (camera = end_cam; camera < n_cameras; ++camera)
              MPI_Recv(ps[camera].data(), ps[camera].size(), MPI_DOUBLE, MPI_ANY_SOURCE, camera, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
          } else {
            for (camera = start_cam; camera < end_cam; ++camera)
              MPI_Send(ps[camera].data(), ps[camera].size(), MPI_DOUBLE, ROOT, camera, MPI_COMM_WORLD);
          }
        }
      };

      mpi_proj(projections);                 // Collect 2D projections
      mpi_proj(proj_3D);                     // Collect 3D projections
    }

    // Save the projections to disk
    if (use_all_halos)
      write_projections(loop);               // Write projection data
  }

  // Clean up opacity field data
  if (kappa_flag)
    k_abs = vector<double>();                // Free memory

  proj_timer.stop();
}
