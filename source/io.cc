/*!
 * \file    io.cc
 *
 * \brief   General input and output functionality.
 *
 * \details This file contains general input and output functionality for the
 *          flow of the program, mostly print/read/write operations.
 */

#include "proto.h"
#include "Simulation.h"
#include "timing.h" // Timing functionality
#include "io_hdf5.h" // HDF5 read/write functions

extern Timer main_timer; // Clock timing
extern Timer read_timer;
extern Timer write_timer;

// Module specific parameters
extern bool continuum;                       // Include stellar continuum emission
extern bool two_level_atom;                  // Two level atom line transfer
extern double g12;                           // Degeneracy ratio: g_lower / g_upper
extern bool scaled_microturb;                // Scaled microturbulence model
extern double T_turb;                        // Microturbulent temperature [K]
extern double a_sqrtT;                       // "damping parameter": a = ΔνL / 2 ΔνD
extern double sigma0_sqrtT;                  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
extern double sigma0p_sqrtT;                 // Doublet cross section [cm^2]
extern double E0p;                           // Doublet line energy [erg]
extern double kappa_dust;                    // Dust opacity [cm^2/g dust]
extern double sigma_dust;                    // Dust cross section [cm^2/Z/hydrogen atom]

void print_min_max(string name, vector<double> vec, string units = "", double conv = 1.);
void print_min_max(string name, vector<Vec3> vec, string units = "", double conv = 1.);
double calculate_d_L(const double z); // Luminosity distance [cm]
void read_geometry(H5File& f); // Read geometry specific data
bool avoid_cell_strict(const int cell); // Avoid calculations without overrides
void setup_active_ions(const bool require_contiguous = false); // Setup active ionization states
void setup_x_indices(); // Setup ionization fraction indices

//! Utility function to print singular or plural words.
static inline string plural(const int size, const string str) {
  return to_string(size) + " " + str + (size > 1 ? "s" : "");
}

//! Print a header for the code.
void print_header() {
  cout << main_timer.date()
       << "\n                  _"
       << "\n                 | |  _"
       << "\n         ___ ___ | |_| |_"
       << "\n        / __) _ \\| (_   _)"
       << "\n       ( (_| |_| | | | |_"
       << "\n        \\___)___/ \\_) \\__)"
       << "\n"
       << "\n Cosmic Lyman-alpha Transfer code"
       << "\n"
       << "\n"
       << "\nWorking with " << plural(n_ranks * n_threads, "core") << " ("
       << plural(n_ranks, "task") << ", " << plural(n_threads, "thread") << ")"
       << "\n"
       << "\nRun as " << executable << " " << config_file
       << ((snap_num >= 0) ? " " + to_string(snap_num) : "")
       << ((halo_num > -3 && !use_all_halos) ? " " + to_string(halo_num) : "")
       << "\n"
       << "\nMain simulation parameters:"
       << "\n  Initial conditions file = " << init_file
       << "\n  Data output directory   = " << output_dir
       << "\n  Output file base name   = " << output_base << endl;
}

//! Print a footer for code setup.
void print_footer() {
  cout << "\n+-------------------------------------------------+"
       << "\n| Initialization complete. Starting calculations. |"
       << "\n+-------------------------------------------------+" << endl;
}

//! Print a success banner.
void print_success() {
  cout << "\n+-------------------------------------------------+"
       << "\n| Completed successfully!  Finalizing simulation. |"
       << "\n+-------------------------------------------------+" << endl;
}

// Set a given vector to a constant value of size n_cells.
static inline void fill_constant(vector<double>& vec, const double val) {
  vec.resize(n_cells);                       // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    vec[i] = val;                            // Set to constant value
}

// Set a given vector to the product of a constant and a vector of size n_cells.
static inline void fill_product(vector<double>& vec, const vector<double>& ref, const double val = 1.) {
  vec.resize(n_cells);                       // Allocate space
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    vec[i] = ref[i] * val;                   // Set to product value
}

// Set a given vector to the corresponding mass-weighted average.
static inline void set_to_mass_weighted_average(vector<double>& vec) {
  double m_tot = 0., vec_m_tot = 0.;         // Total mass [g] and mass weight
  #pragma omp parallel for reduction(+:m_tot, vec_m_tot)
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell_strict(i))
      continue;
    const double m_i = rho[i] * VOLUME(i);   // m = rho V
    m_tot += m_i;                            // sum(m)
    vec_m_tot += m_i * vec[i];               // sum(m * vec)
  }
  const double vec_avg = vec_m_tot / m_tot;  // sum(m * vec) / sum(m)
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i)
    vec[i] = vec_avg;                        // Set to the average
}

//! Find unfiltered items in the catalog.
static void find_unfiltered(vector<int>& hid, vector<int>& uhid, int& n_uhid) {
  #pragma omp parallel
  {
    const int n_hid = hid.size();            // Number of IDs
    vector<int> local_uhid;                  // Thread local IDs
    #pragma omp for
    for (int i = 0; i < n_hid; ++i) {
      if (find(local_uhid.begin(), local_uhid.end(), hid[i]) == local_uhid.end())
        local_uhid.push_back(hid[i]);        // Add IDs not already in the list
    }
    #pragma omp critical                     // Combine thread local lists
    uhid.insert(uhid.end(), local_uhid.begin(), local_uhid.end());
  }
  n_uhid = uhid.size();                      // Number of unfiltered groups
}

//! Sort and remove duplicate IDs.
static void sort_unfiltered(vector<int>& uhid, int& n_uhid, const vector<int>& hid, const int n_hid) {
  for (int i = 0; i < n_uhid - 1; ++i) {
    int i_min = i;                           // Initialize min index
    for (int i_p = i + 1; i_p < n_uhid; ++i_p) {
      if (uhid[i_p] < uhid[i_min])
        i_min = i_p;                         // Update min index
    }
    if (i_min > i)                           // Requires sorting
      std::swap(uhid[i], uhid[i_min]);
  }

  // Remove duplicate IDs
  for (int i = 0; i < n_uhid - 1; ++i) {
    if (uhid[i] == uhid[i + 1]) {
      uhid.erase(uhid.begin() + i);
      --n_uhid; --i;                         // Update bin count and index
    }
  }

  // Remove IDs that are already in the catalog (assumes both are sorted)
  for (int i_hid = 0, i_start = 0; i_hid < n_hid; ++i_hid) {
    for (int i_uhid = i_start; i_uhid < n_uhid; ++i_uhid) {
      if (hid[i_hid] == uhid[i_uhid]) {
        uhid.erase(uhid.begin() + i_uhid);
        i_start = i_uhid;                    // Start from this index next time
        --n_uhid;                            // Update unfiltered count
        break;                               // Move to next halo
      }
    }
  }
}

//! Sort and remove duplicate halos based on the IDs.
static void sort_halos(vector<int>& hid, vector<Vec3>& v_halo,
  vector<Vec3>& r_vir, vector<double>& R_vir, vector<double>& R2_vir, const bool have_vir,
  vector<Vec3>& r_gal, vector<double>& R_gal, vector<double>& R2_gal, const bool have_gal) {
  const int n_hid = hid.size();              // Number of halos
  const bool have_vel = !v_halo.empty();     // Flag for velocities
  for (int i = 0; i < n_hid - 1; ++i) {
    int i_min = i;                           // Initialize min index
    for (int i_p = i + 1; i_p < n_hid; ++i_p) {
      if (hid[i_p] < hid[i_min])
        i_min = i_p;                         // Update min index
    }
    if (i_min > i) {                         // Requires sorting
      std::swap(hid[i], hid[i_min]);         // Swap IDs
      if (have_vel)
        std::swap(v_halo[i], v_halo[i_min]); // Swap velocities
      if (have_vir) {
        std::swap(r_vir[i], r_vir[i_min]);   // Swap positions
        std::swap(R_vir[i], R_vir[i_min]);   // Swap radii
        std::swap(R2_vir[i], R2_vir[i_min]); // Swap squared radii
      }
      if (have_gal) {
        std::swap(r_gal[i], r_gal[i_min]);   // Swap positions
        std::swap(R_gal[i], R_gal[i_min]);   // Swap radii
        std::swap(R2_gal[i], R2_gal[i_min]); // Swap squared radii
      }
    }
  }

  // Check or duplicate IDs
  for (int i = 0; i < n_hid - 1; ++i) {
    if (hid[i] == hid[i + 1])
      error("Duplicate halo ID: " + to_string(hid[i]));
  }
}

//! Read the initial conditions from the specified hdf5 filename.
void Simulation::read_hdf5() {
  read_timer.start();

  if (!init_base.empty())
    init_file += "." + init_ext;             // Add file extension

  // Check existence and open files in read only mode
  if (root) {
    if (FILE *file = fopen(init_file.c_str(), "r"))
      fclose(file);                          // First check if the file exists
    else
      error("Initial conditions file does not exist: "+init_file);
    if (FILE *file = fopen(abundances_file.c_str(), "r"))
      fclose(file);                          // First check if the file exists
    else
      H5File f_x(abundances_file, H5F_ACC_TRUNC);
  }
  MPI_Barrier(MPI_COMM_WORLD);               // Avoid read/write race condition
  const bool same_file = (init_file == abundances_file); // Flag
  H5File f(init_file, H5F_ACC_RDONLY), f_xp; // Main initial conditions file
  if (!same_file) f_xp = H5File(abundances_file, H5F_ACC_RDONLY);
  H5File& f_x = (same_file) ? f : f_xp;      // Ionization states file

  // Initialize cosmology information
  if (cosmological) {
    read(f, "redshift", z);                  // Current simulation redshift
    read_if_exists(f, "Omega0", Omega0);     // Matter density [rho_crit_0]
    read_if_exists(f, "OmegaB", OmegaB);     // Baryon density [rho_crit_0]
    read_if_exists(f, "h100", h100);         // Hubble constant [100 km/s/Mpc]
    d_L = calculate_d_L(z);                  // Luminosity distance [cm]
  } else
    read_if_exists(f, "time", sim_time);     // Simulation time [s]

  // Initialize group information
  if (output_groups || output_group_flows || (halo_num > -3 && !select_subhalo && !use_all_halos)) {
    if (cell_based_emission) {
      read(f, "group_id", group_id_cell);    // Group ID
      if (!filter_group_catalog)             // Find unfiltered groups
        find_unfiltered(group_id_cell, ugroup_id, n_ugroups);
    }
    if (star_based_emission) {
      read(f, "group_id_star", group_id_star); // Group ID (stars)
      if (!filter_group_catalog)             // Find unfiltered groups
        find_unfiltered(group_id_star, ugroup_id, n_ugroups);
    }
    if (cell_based_emission || star_based_emission)
      sort_unfiltered(ugroup_id, n_ugroups, group_id, n_groups); // Sort and remove duplicates
  }

  // Initialize subhalo information
  if (output_subhalos || output_subhalo_flows || (halo_num > -3 && select_subhalo && !use_all_halos)) {
    if (cell_based_emission) {
      read(f, "subhalo_id", subhalo_id_cell); // Subhalo ID
      if (!filter_subhalo_catalog)           // Find unfiltered subhalos
        find_unfiltered(subhalo_id_cell, usubhalo_id, n_usubhalos);
    }
    if (star_based_emission) {
      read(f, "subhalo_id_star", subhalo_id_star); // Subhalo ID (stars)
      if (!filter_subhalo_catalog)           // Find unfiltered subhalos
        find_unfiltered(subhalo_id_star, usubhalo_id, n_usubhalos);
    }
    if (cell_based_emission || star_based_emission)
      sort_unfiltered(usubhalo_id, n_usubhalos, subhalo_id, n_subhalos); // Sort and remove duplicates
  }

  // Consistency check for atoms and ionization states
  fields[HydrogenDensity].read = true;       // Always read n_H
  if (fields[DensitySquaredHI].read || fields[IonizationFront].read)
    fields[HI_Fraction].read = true;         // Always read x_HI
  if (f_ion < 1.)
    fields[HII_Fraction].read = true;        // Read x_HII
  if (active_ions.empty())
    setup_active_ions();                     // Setup active ionization states
  if (x_indices.empty())
    setup_x_indices();                       // Setup ionization fraction indices

  // Read data fields
  read_geometry(f);                          // Read geometry specific data
  if (set_density_from_mass)
    read(f, "m", rho, n_cells, "g");         // Mass [g] (converted below)
  else
    read(f, "rho", rho, n_cells, "g/cm^3");  // Density [g/cm^3]
  if (read_density_as_mass || set_density_from_mass)
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i))
        rho[i] = 0.;                         // Ignore certain cells
      else
        rho[i] /= VOLUME(i);                 // Convert mass to density
    }
  if (exists(f, "v"))
    read(f, "v", v, n_cells, "cm/s");        // Bulk velocity [cm/s]
  else
    v = vector<Vec3>(n_cells);               // Initialize with zeros [cm/s]
  if constexpr (cell_velocity_gradients && SPHERICAL) {
    if (exists(f, "dvdr"))
      read(f, "dvdr", dvdr, n_cells, "1/s"); // Bulk velocity gradient [1/s]
    else if (continuous_velocity) {          // Use a continuous velocity field
      dvdr.resize(n_cells);                  // Allocate space
      vector<double> v_face = vector<double>(n_cells+1); // Face velocity [cm/s]
      #pragma omp parallel for
      for (int i = 1; i < n_cells; ++i) {
        double dr_L = r[i].x - r_c[i-1];     // Distance from face to left cell center
        double dr_R = r_c[i] - r[i].x;       // Distance from face to right cell center
        double dr_tot = dr_L + dr_R;         // Total distance
        double w_L = dr_R / dr_tot;          // Normalized weights
        double w_R = dr_L / dr_tot;
        v_face[i] = v[i-1].x * w_L + v[i].x * w_R; // Face velocity
      }
      v_face[n_cells] = 2. * v[n_cells-1].x - v_face[n_cells-1]; // Extrapolate boundary
      v_face[0] = (r[0].x > 0.) ? 2. * v[0].x - v_face[1] : 0.; // Pin boundary to zero
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        v[i].x = 0.5 * (v_face[i] + v_face[i+1]); // Average of face velocities
        dvdr[i] = (v_face[i+1] - v_face[i]) / (r[i+1].x - r[i].x); // Velocity gradient
      }
    } else {                                 // Centered finite difference
      dvdr.resize(n_cells);                  // Allocate space
      #pragma omp parallel for
      for (int i = 1; i < n_cells-1; ++i)    // Simple gradient estimator
        dvdr[i] = (v[i+1].x - v[i-1].x) / (r_c[i+1] - r_c[i-1]);
      dvdr[0] = (v[1].x - v[0].x) / (r_c[1] - r_c[0]);
      dvdr[n_cells-1] = (v[n_cells-1].x - v[n_cells-2].x) / (r_c[n_cells-1] - r_c[n_cells-2]);
    }
    v0.resize(n_cells);                      // Allocate space
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      v0[i] = v[i].x - dvdr[i] * r_c[i];     // Velocity extrapolation [cm/s]
  }
  if (constant_temperature >= 0.)
    fill_constant(T, constant_temperature);  // User defined temperature
  else if (!exists(f, "T") || use_internal_energy) {
    use_internal_energy = true;              // Read internal energy instead
    read_electron_fraction = true;           // Read electron fraction as well
    read(f, "e_int", e_int, n_cells, "cm^2/s^2"); // Internal energy [erg/g = cm^2/s^2]
    T.resize(n_cells);                       // Allocate space
  } else
    read(f, "T", T, n_cells, "K");           // Temperature [K]
  // Hydrogen
  const int first_metal_atom = read_helium ? 2 : 1; // First metal atom index
  if (read_H2) {                             // Molecular hydrogen
    if (fields[H2_Fraction].constant >= 0.)
      fill_constant(x_H2, fields[H2_Fraction].constant); // User defined H2 fraction
    else
      read_if_exists(f_x, "x_H2", x_H2, 0.5, n_cells); // x_H2 = n_H2 / n_H
  }
  if (read_electron_fraction) {
    if (electron_fraction >= 0.)
      fill_constant(x_e, electron_fraction); // User defined electron fraction
    else
      read_if_exists(f_x, "x_e", x_e, 1., n_cells); // x_e = n_e / n_H ~ x_HII
  }
  // Ionization fractions
  for (int atom = 0, ion = 0; atom < n_atoms; ++atom) { // Loop over atoms
    vector<vector<double>*> vecs;            // Vector of pointers to vectors
    const int offset = atom_offsets[atom];   // Atom offset
    for (ion = atom_ranges[atom]; ion < atom_ranges[atom+1]; ++ion) {
      auto& field = fields[ion + offset];    // Data for this ion
      if (field.read) {                      // Ion read flag
        if (field.constant >= 0.) {
          fill_constant(field.data, field.constant); // User defined fraction
        } else {
          const double val = vecs.empty() ? 1. : MIN_FRAC; // Default value
          vecs.push_back(&field.data);       // Add to vector of pointers
          read_if_exists(f_x, field.name, field.data, val, n_cells); // x_ion = n_ion / n_atom
        }
      }
    }
    auto& field = fields[ion + offset];      // Data for final implicit ion
    if (field.read) {                        // Ion read flag
      if (field.constant >= 0.) {
        fill_constant(field.data, field.constant); // User defined fraction
      } else if (exists(f_x, field.name)) {
        read(f_x, field.name, field.data, n_cells); // x_ion = n_ion / n_atom
      } else if (!vecs.empty()) {
        fill_remaining(field.data, vecs);    // x_ion = 1 - sum(x_ion lower states)
      } else {
        root_error(field.name + " requested but not in file, constant, or derivable from lower states!");
      }
    }
  }
  // Metallicity and dust fields
  const bool hydrogen_unspecified = (hydrogen_fraction < 0.); // Remember if specified
  const bool helium_unspecified = (helium_fraction < 0.); // Remember if specified
  if (hydrogen_unspecified)
    hydrogen_fraction = 0.76;                // Default to primordial value
  if (helium_unspecified)
    helium_fraction = 1. - hydrogen_fraction; // Default to Y = 1 - X
  if (exists(f, "X") && hydrogen_unspecified) {
    read(f, "X", X, n_cells);                // Mass fraction of hydrogen
    read_hydrogen_fraction = true;           // Allocation was made
    hydrogen_fraction = -1.;                 // Reset to flag that it was read
    clip(X, 0., 1.);                         // Clip to the mass fraction range
  } else if (read_hydrogen_fraction) {
    fill_constant(X, hydrogen_fraction);     // Filled with specified value
  }
  if (exists(f, "Y") && helium_unspecified) {
    read(f, "Y", Y, n_cells);                // Mass fraction of helium
    read_helium_fraction = true;             // Allocation was made
    helium_fraction = -1.;                   // Reset to flag that it was read
    clip(Y, 0., 1.);                         // Clip to the mass fraction range
  } else if (read_helium_fraction) {
    fill_constant(Y, helium_fraction);       // Filled with specified value
  }
  if (metallicity >= 0.) {
    fill_constant(Z, metallicity);           // User defined metallicity
  } else {
    read_if_exists(f, "Z", Z, Zsun, n_cells); // Metallicity [mass fraction]
    clip(Z, 0., 1.);                         // Clip to the metallicity range
  }
  const array<double, n_atoms> all_Zsun_atoms = {0., 0., Zsun_C, Zsun_N, Zsun_O, Zsun_Ne, Zsun_Mg, Zsun_Si, Zsun_S, Zsun_Fe};
  vector<double> Z_indices(n_active_atoms);  // Active atom metallicities [mass fraction]
  Z_indices[0] = HydrogenFraction;           // Hydrogen
  if (read_helium)
    Z_indices[1] = HeliumFraction;           // Helium
  const int Z_offset = CarbonMetallicity - C_ATOM; // Offset for metallicity fields
  for (int aa = first_metal_atom; aa < n_active_atoms; ++aa) {
    const int atom = active_atoms[aa];       // Active atom index
    const double Zsun_atom = all_Zsun_atoms[atom]; // Active solar metallicity fraction
    Z_indices[aa] = atom + Z_offset;         // Active atom metallicity index
    auto& field = fields[Z_indices[aa]];     // Data for atom metallicity
    if (field.constant >= 0.)
      fill_constant(field.data, field.constant); // User defined atom metallicity
    else
      read_or_scale(f, field.name, field.data, Z, Zsun_atom / Zsun); // Atom metallicity [mass fraction]
    clip(field.data, 0., Z);                 // Clip to the metallicity range
  }
  if (mass_weighted_metallicities) {         // Adopt mass-weighted metallicities
    set_to_mass_weighted_average(Z);
    for (int Z_index : Z_indices)
      set_to_mass_weighted_average(fields[Z_index].data);
  }
  if (dust_to_gas >= 0.)
    fill_constant(D, dust_to_gas);           // User defined dust-to-gas ratio
  else if (dust_to_metal >= 0.)
    fill_product(D, Z, dust_to_metal);       // User defined dust-to-metal ratio
  else {
    read_if_exists(f, "D", D, 0., n_cells);  // Dust-to-gas ratio [mass fraction]
    clip(D, 0., 1.);                         // Clip to the dust-to-gas ratio range
  }
  if (silicate_dust || fields[SilicateMetallicity].read) {
    read(f, strings{"D_S", "D_Si"}, D_S, n_cells); // Silicate dust-to-gas ratio [mass fraction]
    clip(D_S, 0., D);                        // Clip to the dust-to-gas ratio range
  }
  if (f_dust > 0.) {                         // Convert metals to dust
    // Note: Order is due to dependencies: D_S -> D -> Z
    const double f_Z = 1. - f_dust;          // Fraction remaining in metals
    if (silicate_dust || fields[SilicateMetallicity].read) {
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (D_S[i] > 0.)
          D_S[i] += f_dust * Z[i] * D_S[i] / D[i]; // Add to silicate dust-to-gas ratio
      }
    }
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      D[i] += f_dust * Z[i];                 // Add to dust-to-gas ratio
    rescale(Z, f_Z);                         // Remaining metallicity
    for (int aa = first_metal_atom; aa < n_active_atoms; ++aa)
      rescale(fields[Z_indices[aa]].data, f_Z); // Remaining atom metallicity
  }
  if (dust_boost > 0.)
    rescale(D, dust_boost);                  // Optional dust boost factor
  // Special purpose fields
  if (fields[PhotoheatingRate].read)
    read(f_x, "G_ion", G_ion, n_cells, "erg/s"); // Photoheating rate [erg/s]
  if (two_level_atom) {
    read(f, "n_lower", n_lower, n_cells, "cm^-3"); // Number density of lower transition
    read(f, "n_upper", n_upper, n_cells, "cm^-3"); // Number density of upper transition
  }
  if (fields[StarFormationRate].read)
    read(f, "SFR", SFR, n_cells, "Msun/yr"); // Star formation rate [Msun/yr]
  if (fields[DustTemperature].read)
    read(f, "T_dust", T_dust, n_cells, "K"); // Dust temperature [K]
  if (fields[StellarDensity].read)
    read(f, "rho_star", rho_star, n_cells, "g/cm^3"); // Stellar density [g/cm^3]
  if (fields[VelocitySquared].read) {
    v2.resize(n_cells);                      // Velocity^2 [cm^2/s^2]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      v2[i] = v[i].dot();                    // Velocity^2 [cm^2/s^2]
  }
  if (fields[VelocitySquaredLOS].read)
    v2_LOS.resize(n_cells);                  // Velocity_LOS^2 [cm^2/s^2]
  if (fields[VelocityLOS].read)
    v_LOS.resize(n_cells);                   // Velocity_LOS [cm/s]
  if (fields[VelocityX].read)
    v_x.resize(n_cells);                     // Velocity_X [cm/s]
  if (fields[VelocityY].read)
    v_y.resize(n_cells);                     // Velocity_Y [cm/s]
  if (read_B)
    read(f, "B", B, n_cells, "G");           // Magnetic field [G]
  if (fields[MagneticFieldSquared].read) {
    B2.resize(n_cells);                      // Magnetic field^2 [G^2]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      B2[i] = B[i].dot();                    // Magnetic field^2 [G^2]
  }
  if (fields[MagneticFieldLOS].read)
    B_LOS.resize(n_cells);                   // Magnetic field LOS [G]
  if (fields[MagneticFieldX].read)
    B_x.resize(n_cells);                     // Magnetic field X [G]
  if (fields[MagneticFieldY].read)
    B_y.resize(n_cells);                     // Magnetic field Y [G]
  if (fields[LineEmissivity].read)
    read(f, "j_line", j_line, n_cells, "erg/s/cm^3"); // Line emissivity [erg/s/cm^3]
  if (fields[DoubletLineEmissivity].read)
    read(f, "jp_line", jp_line, n_cells, "erg/s/cm^3"); // Doublet line emissivity [erg/s/cm^3]
  if (fields[EnergyDensity].read)
    read(f, "u_rad", u_rad, n_cells, "erg/cm^3"); // Radiation energy density [erg/cm^3]
  if (fields[RadialAcceleration].read)
    read(f, "a_rad_r", a_rad_r, n_cells, "cm/s^2"); // Radial radiation acceleration [cm/s^2]
  if (fields[RadialPressure].read)
    read(f, "P_rad_r", P_rad_r, n_cells, "erg/cm^3"); // Radial radiation pressure [erg/cm^3]
  if (fields[DensitySquared].read) {
    rho2.resize(n_cells);
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      rho2[i] = rho[i] * rho[i];             // Density^2 [g^2/cm^6]
  }
  if (fields[DensitySquaredHI].read) {
    rho2HI.resize(n_cells);
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      rho2HI[i] = x_HI[i] * rho[i] * rho[i]; // x_HI * Density^2 [g^2/cm^6]
  }
  if (fields[IonizationFront].read) {
    ion_front.resize(n_cells);
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      ion_front[i] = exp(-1000.0 * pow(x_HI[i]-0.01,2)); // Ionization front
  }

  // Read star data
  if (star_based_emission) {
    n_stars = read(f, "r_star", r_star, "cm"); // Star positions [cm]
    if (n_photons_per_star * n_stars > n_photons) {
      n_photons = n_photons_per_star * n_stars; // Update n_photons
      if (n_photons > n_photons_max)
        n_photons = n_photons_max;           // Limit n_photons
    }
    if (read_v_star)
      read(f, "v_star", v_star, n_stars, "cm/s"); // Star velocities [cm/s]
    if (read_continuum)
      read(f, strings{"L_cont", "L_UV"}, L_cont_star, n_stars, "erg/s/angstrom"); // Stellar continuum [erg/s/angstrom]
    if (read_m_massive_star)
      read(f, "m_massive_star", m_massive_star, n_stars, "Msun"); // Massive star mass [Msun]
    if (read_m_init_star)
      read(f, strings{"m_init_star", "m_star"}, m_init_star, n_stars, "Msun"); // Initial star mass [Msun]
    if (read_Z_star)
      read(f, "Z_star", Z_star, n_stars);    // Star metallicity [mass fraction]
    if (read_age_star)
      read(f, "age_star", age_star, n_stars, "Gyr"); // Star age [Gyr]
  }

  // Read AGN data
  if (AGN_emission) {
    vector<Vec3> r_AGNs;
    const int n_AGN = read(f, "r_bh", r_AGNs, "cm"); // AGN position [cm]
    if (n_AGN != 1)
      root_error("Expecting only one black hole but file has " + to_string(n_AGN));
    r_AGN = r_AGNs[0];                       // Save AGN position [cm]
    if (attribute_exists(f, "Lbol"))
      read(f, "Lbol", Lbol_AGN);             // Save AGN bolometric luminosity [erg/s]
  }

  // Calculate derived quantities
  const array<double, n_atoms> all_m_atoms = {mH, mHe, mC, mN, mO, mNe, mMg, mSi, mS, mFe};
  vector<double> m_atoms(n_active_atoms);    // Active atom masses [g]
  n_indices.resize(n_active_atoms);          // Active atom number density indices
  for (int aa = 0; aa < n_active_atoms; ++aa) {
    const int atom = active_atoms[aa];       // Active atom index
    m_atoms[aa] = all_m_atoms[atom];         // Active atom mass [g]
    n_indices[aa] = HydrogenDensity + atom;  // Active atom number density index
  }
  for (int n_index : n_indices)
    fields[n_index].data.resize(n_cells);    // Allocate number densities [cm^-3]
  constexpr double gamma = 5. / 3.;          // Adiabatic index
  constexpr double T_div_emu = (gamma - 1.) * mH / kB; // T / (e * mu)
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (Z[i] < 0.)                           // Avoid negative metallicities
      Z[i] = 0.;
    if (Z[i] > 0.999999999)                  // Avoid negative gas fractions
      Z[i] = 0.999999999;
    if (read_hydrogen_fraction) {
      if (X[i] < 0.)                         // Avoid negative hydrogen mass fractions
        X[i] = 0.;
      if (X[i] > 1.)                         // Avoid unphysical fractions
        X[i] = 1.;
    }
    const double X_cell = read_hydrogen_fraction ? X[i] : hydrogen_fraction * (1. - Z[i]);
    n_H[i] = X_cell * rho[i] / mH;           // n_H = X rho / mH

    if (read_helium) {
      if (read_helium_fraction) {
        if (Y[i] < 0.)                       // Avoid negative helium mass fractions
          Y[i] = 0.;
        if (Y[i] > 1.)                       // Avoid unphysical fractions
          Y[i] = 1.;
      }
      const double Y_cell = read_helium_fraction ? Y[i] : helium_fraction * (1. - Z[i]);
      n_He[i] = Y_cell * rho[i] / mHe;       // n_He = Y rho / mHe
    }

    for (int aa = first_metal_atom; aa < n_active_atoms; ++aa) {
      auto& n_atom = fields[n_indices[aa]].data; // Number density [cm^-3]
      auto& Z_atom = fields[Z_indices[aa]].data; // Metallicity [mass fraction]
      if (Z_atom[i] < 0.)                    // Avoid negative metallicities
        Z_atom[i] = 0.;
      n_atom[i] = Z_atom[i] * rho[i] / m_atoms[aa]; // n_atom = Z_atom rho / m_atom
    }

    if (use_internal_energy) {
      const double mu = 4. / (1. + X_cell * (3. + 4. * x_e[i])); // Mean molecular mass [mH]
      T[i] = T_div_emu * e_int[i] * mu;      // Gas temperature [K]
    }
    if (T_floor >= 0. && T[i] < T_floor)
      T[i] = T_floor;                         // Apply temperature floor [K]
  }

  if (module == "mcrt") {
    constexpr double SQRTPI = 2. / M_2_SQRTPI; // sqrt(pi)
    constexpr double turb_coef = km * km / (gamma * kB); // Density scaling [K cm^3/g]
    const vector<double>& n_carrier = fields[line_carrier].data; // Carrier number density [cm^-3]
    const vector<double>& x_lower = fields[line_type].data; // Lower state fraction

    k_0.resize(n_cells);                     // Absorption coefficient of primary line [1/cm]
    if (E0p > 0.)
      kp_0.resize(n_cells);                  // Absorption coefficient of doublet line [1/cm]
    a.resize(n_cells);                       // "Damping parameter"

    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i)) {
        k_0[i] = a[i] = 0.;                  // Ignore certain cells
        if (E0p > 0.)
          kp_0[i] = 0.;                      // Ignore doublet line
        continue;
      }

      double T_turb_local = T_turb;          // Minimum microturbulence value
      if (scaled_microturb && turb_coef * rho[i] > T_turb)
        T_turb_local = turb_coef * rho[i];   // Update T_turb based on scaling

      // Calculate absorption coefficient: k_0 = n * sigma0
      const double _1_sqrt_T = 1. / sqrt(T[i] + T_turb_local);
      const double sigma0 = sigma0_sqrtT * _1_sqrt_T; // Cross-section [cm^2]
      if (two_level_atom)                    // Two level atoms
        k_0[i] = (n_lower[i] - g12 * n_upper[i]) * sigma0;
      else                                   // All other line carriers
        k_0[i] = x_lower[i] * n_carrier[i] * sigma0;
      if constexpr (sobolev)                 // Sobolev approximation
        k_0[i] *= SQRTPI;
      if (E0p > 0.) {                        // Doublet line
        kp_0[i] = (sigma0_sqrtT > 0.) ? k_0[i] * sigma0p_sqrtT / sigma0_sqrtT : 0.;
        if constexpr (sobolev)               // Sobolev approximation
          kp_0[i] *= SQRTPI;
      }

      // Calculate the "damping parameter" once: a = 0.5 * DnuL / DnuD
      a[i] = a_sqrtT * _1_sqrt_T;
    }
  }

  if constexpr (multiple_dust_species) {
    if (module != "ionization")
      root_error("Multi-species dust is only supported in the ionization module!");
  }

  if (module == "mcrt" || module == "escape_cont") {
    k_dust.resize(n_cells);                  // Absorption coefficient of dust [1/cm]

    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i)) {
        k_dust[i] = 0.;                      // Ignore certain cells
        continue;
      }

      // Dust absorption coefficient using:
      //  - constant albedo (accounted for in ray tracing)
      //  - constant dust-to-gas ratio (or metallicity scaling)
      //  - frequency-independent dust opacity (or cross section)
      //  - constant f_ion survival in HII regions
      //  - thermal sputtering destroys dust above T_sputter
      const double f_surv = (T[i] > T_sputter) ? 0. : (f_ion == 1. ? 1. : 1. + (f_ion - 1.) * x_HII[i]);
      if (sigma_dust > 0.)
        k_dust[i] = f_surv * n_H[i] * sigma_dust * (Z[i] + D[i]); // Cross-section and metallicity
      else
        k_dust[i] = f_surv * kappa_dust * rho[i] * D[i]; // Opacity and dust-to-gas ratio
    }
  }

  if (module == "ionization" || module == "escape") {
    if constexpr (graphite_dust)
      rho_dust_G.resize(n_cells);            // Graphite dust density [g/cm^3]
    if constexpr (silicate_dust)
      rho_dust_S.resize(n_cells);            // Silicate dust density [g/cm^3]
    if constexpr (n_dust_species == 1)
      rho_dust.resize(n_cells);              // Dust density [g/cm^3]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i)) {            // Ignore certain cells
        if constexpr (graphite_dust)
          rho_dust_G[i] = 0.;
        if constexpr (silicate_dust)
          rho_dust_S[i] = 0.;
        if constexpr (n_dust_species == 1)
          rho_dust[i] = 0.;
        continue;
      }
      const double f_surv = (T[i] > T_sputter) ? 0. : (f_ion == 1. ? 1. : 1. + (f_ion - 1.) * x_HII[i]);
      if constexpr (n_dust_species == 1) {
        rho_dust[i] = f_surv * rho[i] * D[i]; // Dust density [g/cm^3]
      } else {                               // Multi-species dust
        if constexpr (graphite_dust) {
          double D_G = D[i];                 // Graphite dust-to-gas ratio
          if constexpr (silicate_dust)
            D_G -= D_S[i];                   // Subtract silicate dust-to-gas ratio
          rho_dust_G[i] = f_surv * rho[i] * D_G; // Graphite dust density [g/cm^3]
        }
        if constexpr (silicate_dust)
          rho_dust_S[i] = f_surv * rho[i] * D_S[i]; // Silicate dust density [g/cm^3]
      }
    }
  }

  if constexpr (electron_scattering) {
    constexpr double r_0 = ee * ee / (me * c * c); // Electron radius = e^2 / m c^2
    constexpr double sigma_T = 8. * M_PI * r_0 * r_0 / 3.; // Thomson cross-section [cm^2]
    k_e.resize(n_cells);                     // Absorption coefficient of electrons [1/cm]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i)) {
        k_e[i] = 0.;                         // Ignore certain cells
        continue;
      }
      k_e[i] = x_e[i] * n_H[i] * sigma_T;    // Optical depth: τ_e = ∫ n_e σ_T dl
    }
  }

  // Print the initial conditions data
  if (root) {
    string geometry_name;
    if constexpr (SLAB)
      geometry_name = "slab";
    else if constexpr (SPHERICAL)
      geometry_name = "spherical";
    else if constexpr (CARTESIAN)
      geometry_name = "cartesian";
    else if constexpr (OCTREE)
      geometry_name = "octree";
    else if constexpr (VORONOI)
      geometry_name = "voronoi";
    cout << "\nSimulation info: [" << geometry_name << " geometry]";
    if (cosmological) {
      cout << "\n  redshift   = " << z;
      if (verbose)
        cout << "\n  Omega0     = " << Omega0 << " rho_crit_0"
             << "\n  OmegaB     = " << OmegaB << " rho_crit_0"
             << "\n  H_0        = " << 100. * h100 << " km/s/Mpc"
             << "\n  d_L        = " << d_L / Gpc << " Gpc";
    } else if (sim_time >= 0.)
      cout << "\n  time       = " << sim_time / Myr << " Myr";
    if constexpr (CARTESIAN)
      cout << "\n  (nx,ny,nz) = (" << nx << ", " << ny << ", " << nz << ")";
    else if constexpr (OCTREE)
      cout << "\n  n_leafs    = " << n_leafs;
    cout << "\n  n_cells    = " << n_cells << endl;
    if (mass_weighted_metallicities) {       // Print mass-weighted metallicities
      cout << "\nMass-weighted metallicities:\n  Z          = " << Z[0] << endl;
      for (int Z_index : Z_indices) {
        if (fields[Z_index].data.empty()) continue;
        string name = fields[Z_index].name; name.resize(10, ' ');
        cout << "  " << name << " = " << fields[Z_index].data[0] << endl;
      }
    }
    if (verbose) {
      cout << "\nGrid data:" << endl;
      if constexpr (CARTESIAN)
        cout << "  (dx,dy,dz) = " << Vec3(wx,wy,wz) / pc << " pc" << endl;
      else {
        print("r", r, "kpc", kpc);
        print("V", V, "pc^3", pc * pc * pc);
      }
      if constexpr (SLAB || SPHERICAL || OCTREE)
        print("w", w, "pc", pc);
      for (int n_index : n_indices)          // Number densities (n_H etc.)
        print(fields[n_index].name, fields[n_index].data, "cm^-3");
      if (two_level_atom) {
        print("n_lower", n_lower, "cm^-3");
        print("n_upper", n_upper, "cm^-3");
      }

      print("rho", rho, "g/cm^3");
      print("T", T, "K");
      print("v", v, "km/s", km);
      if constexpr (cell_velocity_gradients) {
        print("dv/dr", dvdr, "s^-1");
        print("v0", v0, "km/s", km);
      }
      print("Z", Z);
      for (int Z_index : Z_indices)
        print(fields[Z_index].name, fields[Z_index].data);
      print("D", D);
      if (read_H2)
        print("x_H2", x_H2);

      const int n_x_indices = x_indices.size(); // Number of ionization states
      for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
        const int ion_count = ion_counts[aa]; // Number of active ions
        if (ion_count <= 0) continue;        // Skip inactive atoms
        for (int j = 0; j < ion_count; ++j, ++ai) {
          if (ai < 0 || ai >= n_x_indices)
            error("ai is out of bounds. ai = " + to_string(ai));
          const auto& field = fields[x_indices[ai]];
          print(field.name, field.data);
        }
        const int aip = ai - 1;              // Previous ion index
        if (aip < 0 || aip >= n_x_indices)
          error("ai-1 is out of bounds. ai-1 = " + to_string(aip));
        const auto& field = fields[x_indices[aip]+1];
        if (!field.data.empty())
          print(field.name, field.data);
      }

      if (read_electron_fraction)
        print("x_e", x_e);
      if constexpr (electron_scattering)
        print("k_e", k_e, "cm^-1");

      if (n_groups > 0) {
        cout << "\nGroup catalog data:"
             << "\n  n_groups   = " << n_groups << endl;
        if (output_grp_obs || output_grp_vir) {
          print("r_vir", r_grp_vir, "kpc", kpc);
          print("R_vir", R_grp_vir, "kpc", kpc);
          if (verbose)
            print("R2_vir", R2_grp_vir, "kpc^2", kpc*kpc);
        }
        if (output_grp_gal) {
          print("r_gal", r_grp_gal, "kpc", kpc);
          print("R_gal", R_grp_gal, "kpc", kpc);
          if (verbose)
            print("R2_gal", R2_grp_gal, "kpc^2", kpc*kpc);
        }
        if (!v_grp.empty())
          print("v_group", v_grp, "km/s");
        print("group_id", group_id);
        if (n_ugroups > 0)
          print("unfiltered", ugroup_id);
      }
      if (n_subhalos > 0) {
        cout << "\nSubhalo catalog data:"
             << "\n  n_subhalos = " << n_subhalos << endl;
        if (output_sub_obs || output_sub_vir) {
          print("r_vir", r_sub_vir, "kpc", kpc);
          print("R_vir", R_sub_vir, "kpc", kpc);
        }
        if (output_sub_gal) {
          print("r_gal", r_sub_gal, "kpc", kpc);
          print("R_gal", R_sub_gal, "kpc", kpc);
        }
        if (!v_sub.empty())
          print("v_subhalo", v_sub, "km/s");
        print("subhalo_id", subhalo_id);
        if (n_usubhalos > 0)
          print("unfiltered", usubhalo_id);
      }

      cout << "\nmin/max cell data:"
           << "\n  (x,y,z)    = [" << bbox[0] / kpc << ","
           << "\n                " << bbox[1] / kpc << "] kpc" << endl;
      if constexpr (SLAB || SPHERICAL || OCTREE)
        print_min_max("w", w, "pc", pc);
      print_min_max("(vx,vy,xz)", v, "km/s", km);
      if constexpr (!CARTESIAN)
        print_min_max("V", V, "pc^3", pc*pc*pc);
      for (int n_index : n_indices)
        print_min_max(fields[n_index].name, fields[n_index].data, "cm^-3");
      if (two_level_atom) {
        print_min_max("n_lower", n_lower, "cm^-3");
        print_min_max("n_upper", n_upper, "cm^-3");
      }
      print_min_max("rho", rho, "g/cm^3");
      print_min_max("T", T, "K");
      print_min_max("Z", Z);
      for (int Z_index : Z_indices)
        print_min_max(fields[Z_index].name, fields[Z_index].data);
      print_min_max("D", D);
      if (read_H2)
        print_min_max("x_H2", x_H2);

      for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
        const int ion_count = ion_counts[aa]; // Number of active ions
        if (ion_count <= 0) continue;        // Skip inactive atoms
        for (int j = 0; j < ion_count; ++j, ++ai) {
          if (ai < 0 || ai >= n_x_indices)
            error("ai is out of bounds. ai = " + to_string(ai));
          print_min_max(fields[x_indices[ai]].name, fields[x_indices[ai]].data);
        }
        const int aip = ai - 1;              // Previous ion index
        if (aip < 0 || aip >= n_x_indices)
          error("ai-1 is out of bounds. ai-1 = " + to_string(aip));
        print_min_max(fields[x_indices[aip]+1].name, fields[x_indices[aip]+1].data);
      }

      if (read_electron_fraction)
        print_min_max("x_e", x_e);
      if constexpr (electron_scattering)
        print_min_max("k_e", k_e, "1/cm");
    }

    if (star_based_emission) {
      cout << "\nStar data:"
           << "\n  n_stars    = " << n_stars << endl;
      if (verbose) {
        print("r_star", r_star, "kpc", kpc);
        if (read_v_star)
          print("v_star", v_star, "km/s", km);
        if (read_continuum)
          print("L_cont", L_cont_star, "erg/s/angstrom");
        if (read_m_massive_star)
          print("m_star(OB)", m_massive_star, "Msun");
        if (read_m_init_star)
          print("m_star(0)", m_init_star, "Msun");
        if (read_Z_star)
          print("Z_star", Z_star);
        if (read_age_star)
          print("age_star", age_star, "Gyr");
        if (!group_id_star.empty())
          print("group_id", group_id_star);
        if (!subhalo_id_star.empty())
          print("subhalo_id", subhalo_id_star);
      }
    }
    if (cell_based_emission && verbose) {
      if (!group_id_cell.empty())
        print("group_id", group_id_cell);
      if (!subhalo_id_cell.empty())
        print("subhalo_id", subhalo_id_cell);
    }
  }
  read_timer.stop();
}

//! Read the group catalog data from the hdf5 file.
void read_halo_catalog(const bool is_group) {
  string halo_file = is_group ? group_file : subhalo_file; // Halo file
  string hstr = is_group ? "group" : "subhalo"; // Halo string
  int& n_halos = is_group ? n_groups : n_subhalos; // Number of halos
  vector<Vec3>& r_vir = is_group ? r_grp_vir : r_sub_vir; // Halo positions [cm]
  vector<Vec3>& r_gal = is_group ? r_grp_gal : r_sub_gal; // Halo galaxy positions [cm]
  vector<Vec3>& v_halo = is_group ? v_grp : v_sub; // Halo velocities [cm/s]
  vector<double>& R_vir = is_group ? R_grp_vir : R_sub_vir; // Halo radii [cm]
  vector<double>& R_gal = is_group ? R_grp_gal : R_sub_gal; // Halo galaxy radii [cm]
  vector<double>& R2_vir = is_group ? R2_grp_vir : R2_sub_vir; // Halo radii squared [cm^2]
  vector<double>& R2_gal = is_group ? R2_grp_gal : R2_sub_gal; // Halo galaxy radii squared [cm^2]
  vector<int>& halo_id = is_group ? group_id : subhalo_id; // Halo IDs
  bool read_vir = is_group ? (output_grp_obs || output_grp_vir || output_group_flows)
                           : (output_sub_vir || output_sub_obs || output_subhalo_flows); // Read virial data
  bool read_gal = is_group ? output_grp_gal : output_sub_gal; // Read galaxy data
  const bool is_target_type = select_subhalo != is_group; // Target this halo type
  if (FILE *file = fopen(halo_file.c_str(), "r"))
    fclose(file);                            // First check if the file exists
  else
    root_error(halo_file + " is not a valid file. [See " + hstr + "_{dir,base,file}]");
  H5File f(halo_file, H5F_ACC_RDONLY);
  if (read_vir) {
    // Search order (* = group/subhalo): r_*, r_halo
    n_halos = read(f, strings{"r_"+hstr, "r_halo"}, r_vir, "cm"); // Halo positions [cm]
    if (n_halos <= 0)
      root_error("No " + hstr + "s found in " + halo_file);
    read(f, strings{"R_"+hstr, "R_halo"}, R_vir, n_halos, "cm"); // Halo radii [cm]
    R2_vir.resize(n_halos);                  // Halo radii squared [cm^2]
    #pragma omp parallel for
    for (int i = 0; i < n_halos; ++i)
      R2_vir[i] = R_vir[i] * R_vir[i];       // Halo radii squared [cm^2]
  }
  if (read_gal) {
    // Search order (* = group/subhalo): r_*_gal, r_*_light, r_*_mass, r_gal, r_light, r_mass
    string rstr = "r_"+hstr+"_", Rstr = "R_"+hstr+"_"; // Convenience strings
    n_halos = read(f, strings{rstr+"gal", rstr+"light", rstr+"mass", "r_gal", "r_light", "r_mass"}, r_gal, "cm"); // Halo galaxy positions [cm]
    if (n_halos <= 0)
      root_error("No galaxy " + hstr + "s found in " + halo_file);
    read(f, strings{Rstr+"gal", Rstr+"light", Rstr+"mass", "R_gal", "R_light", "R_mass"}, R_gal, n_halos, "cm"); // Halo galaxy radii [cm]
    R2_gal.resize(n_halos);                  // Halo galaxy radii squared [cm^2]
    #pragma omp parallel for
    for (int i = 0; i < n_halos; ++i)
      R2_gal[i] = R_gal[i] * R_gal[i];       // Halo galaxy radii squared [cm^2]
  }
  if (module == "mcrt") {
    read(f, strings{"v_"+hstr, "v_"+hstr+"_light", "v_"+hstr+"_mass", "v_halo"}, v_halo, n_halos, "cm/s"); // Halo velocities [cm/s]
    for (int i = 0; i < n_halos; ++i)
      v_halo[i] /= km;                       // Convert to km/s
  }
  if (exists(f, hstr+"_id") || exists(f, "id")) {
    read(f, strings{hstr+"_id", "id"}, halo_id, n_halos); // Halo IDs
    sort_halos(halo_id, v_halo, r_vir, R_vir, R2_vir, read_vir, r_gal, R_gal, R2_gal, read_gal); // Sort halos by ID
    if (is_target_type && halo_num >= 0) {
      for (halo_index = 0; halo_index < n_halos; ++halo_index)
        if (halo_id[halo_index] == halo_num)
          break;                             // Find the requested halo
      if (halo_index == n_halos)
        root_error("Requested " + hstr + " number " + to_string(halo_num) + " is not found!");
    }
  } else {
    cout << "Warning: '" + hstr + "_id' or 'id' does not exist in the " + hstr + " catalog!" << endl;
    if (halo_num >= n_halos)
      root_error("Requested " + hstr + " number " + to_string(halo_num) + " is out of bounds!");
    halo_id.resize(n_halos);                 // Default to index
    #pragma omp parallel for
    for (int i = 0; i < n_halos; ++i)
      halo_id[i] = i;
    if (is_target_type && halo_num >= 0)
      halo_index = halo_num;                 // Use the requested halo
  }
  if (is_target_type && use_all_halos) {
    halo_index = 0;                          // Use the first halo for printing
    halo_num = halo_id[halo_index];          // Halo number
  }
  if (is_target_type && halo_num >= 0) {     // Target halo data (with updated halo_index and halo_num)
    halo_position = r_vir[halo_index];       // Halo position [cm]
    halo_radius = R_vir[halo_index];         // Halo radius [cm]
  }
}

/*! \brief Write general simulation information.
 *
 *  \param[in] f HDF5 File object.
 */
void Simulation::write_sim_info(const H5File& f) {
  // Information about the cosmology
  write(f, "cosmological", cosmological);    // Flag for cosmological simulations
  if (cosmological) {
    write(f, "Omega0", Omega0);              // Matter density [rho_crit_0]
    write(f, "OmegaB", OmegaB);              // Baryon density [rho_crit_0]
    write(f, "h100", h100);                  // Hubble constant [100 km/s/Mpc]
    write(f, "z", z);                        // Simulation redshift
    write(f, "d_L", d_L);                    // Luminosity distance [cm]
  } else if (sim_time >= 0.)
    write(f, "time", sim_time);              // Simulation time [s]
}

/* Write a blank hdf5 file. */
void Simulation::write_blank() {
  H5File f(output_dir + "/" + output_base + snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
}

//! Writes data and info to the specified hdf5 filename.
void Simulation::write_hdf5() {
  write_timer.start();
  // Open file and write all data
  H5File f(output_dir + "/" + output_base + snap_str + halo_str + "." + output_ext,
           use_all_halos ? H5F_ACC_RDWR : H5F_ACC_TRUNC);

  // Write general simulation information
  write_sim_info(f);

  // Write general camera parameters
  if (have_cameras)
    write_camera_info(f, true, !use_all_halos);

  // Write general dust parameters
  if (module == "mcrt" || module == "ionization" || module == "escape")
    write_dust_info(f);

  // Write module specific data
  write_module(f);

  write_timer.stop();
}
