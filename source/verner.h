/************
 * verner.h *
 ************

 * Cross-sections of H, He, C, N, O, Ne, Mg, Si, S, and Fe from Verner et al. (1996).

*/

/************/
/* Hydrogen */
/************/

/* HI photoionization cross-section over [13.6, 50000] eV. */
static inline double sigma_HI(const double E_eV) {
  if (E_eV < HI_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.4298;
  const double xm1 = x - 1.;
  return 5.475e-14 * xm1 * xm1 * pow(x, -4.0185) / pow(1. + sqrt(x/32.88), 2.963);
}

/**********/
/* Helium */
/**********/

/* HeI photoionization cross-section over [24.59, 50000] eV. */
static inline double sigma_HeI(const double E_eV) {
  if (E_eV < HeI_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 13.61 - 0.4434;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 4.562496);
  return 9.492e-16 * (xm1 * xm1 + 4.157521) * pow(y, -3.906) / pow(1. + sqrt(y/1.469), 3.188);
}

/* HeII photoionization cross-section over [54.42, 50000] eV. */
static inline double sigma_HeII(const double E_eV) {
  if (E_eV < HeII_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 1.72;
  const double xm1 = x - 1.;
  return 1.369e-14 * xm1 * xm1 * pow(x, -4.0185) / pow(1. + sqrt(x/32.88), 2.963);
}

/**********/
/* Carbon */
/**********/

/* CI photoionization cross-section over [11.26, 291] eV. */
static inline double sigma_CI(const double E_eV) {
  if (E_eV < CI_eV || E_eV > 291.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.144 - 1.133;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 2.582449);
  return 5.027e-16 * (xm1 * xm1 + 0.0083850649) * pow(y, -2.9495) / pow(1. + sqrt(y/62.16), 5.101);
}

/* CII photoionization cross-section over [24.38, 307.6] eV. */
static inline double sigma_CII(const double E_eV) {
  if (E_eV < CII_eV || E_eV > 307.6)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.4058 - 49.29;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 10.458756);
  return 8.709e-18 * (xm1 * xm1 + 4.380649) * pow(y, -1.211) / pow(1. + sqrt(y/126.1), 8.578);
}

/* CIII photoionization cross-section over [47.89, 328.9] eV. */
static inline double sigma_CIII(const double E_eV) {
  if (E_eV < CIII_eV || E_eV > 328.9)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 4.614 - 0.004378;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0006390784);
  return 1.539e-14 * (xm1 * xm1 + 35.070084) * pow(y, 2.465) / pow(1. + sqrt(y/1.737), 15.93);
}

/* CIV photoionization cross-section over [64.49, 352.2] eV. */
static inline double sigma_CIV(const double E_eV) {
  if (E_eV < CIV_eV || E_eV > 352.2)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 3.506;
  const double xm1 = x - 1.;
  return 1.068e-16 * xm1 * xm1 * pow(x, -1.7715) / pow(1. + sqrt(x/14.36), 7.457);
}

/* CV photoionization cross-section over [392.1, 50000] eV. */
static inline double sigma_CV(const double E_eV) {
  if (E_eV < CV_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 46.24;
  const double xm1 = x - 1.;
  return 2.344e-16 * xm1 * xm1 * pow(x, -4.2095) / pow(1. + sqrt(x/21.83), 2.581);
}

/* CVI photoionization cross-section over [490, 50000] eV. */
static inline double sigma_CVI(const double E_eV) {
  if (E_eV < CVI_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 15.48;
  const double xm1 = x - 1.;
  return 1.521e-15 * xm1 * xm1 * pow(x, -4.0185) / pow(1. + sqrt(x/32.88), 2.963);
}

/************/
/* Nitrogen */
/************/

/* NI photoionization cross-section over [14.53, 404.8] eV. */
static inline double sigma_NI(const double E_eV) {
  if (E_eV < NI_eV || E_eV > 404.8)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 4.034 - 0.8598;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 5.405625);
  return 8.235e-16 * (xm1 * xm1 + 0.0082755409) * pow(y, -3.536) / pow(1. + sqrt(y/80.33), 3.928);
}

/* NII photoionization cross-section over [29.6, 423.6] eV. */
static inline double sigma_NII(const double E_eV) {
  if (E_eV < NII_eV || E_eV > 423.6)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.06128 - 428.;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 412.09);
  return 1.944e-18 * (xm1 * xm1 + 108.7849) * pow(y, -1.1135) / pow(1. + sqrt(y/816.3), 8.773);
}

/* NIII photoionization cross-section over [47.45, 447.3] eV. */
static inline double sigma_NIII(const double E_eV) {
  if (E_eV < NIII_eV || E_eV > 447.3)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.242 - 187.7;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 15.992001);
  return 9.375e-19 * (xm1 * xm1 + 3.4225) * pow(y, -0.922) / pow(1. + sqrt(y/278.8), 9.156);
}

/* NIV photoionization cross-section over [77.47, 475.3] eV. */
static inline double sigma_NIV(const double E_eV) {
  if (E_eV < NIV_eV || E_eV > 475.3)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 5.494 - 0.006415;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0003751969);
  return 1.69e-14 * (xm1 * xm1 + 62.473216) * pow(y, 3.03) / pow(1. + sqrt(y/1.714), 17.06);
}

/* NV photoionization cross-section over [97.89, 504.3] eV. */
static inline double sigma_NV(const double E_eV) {
  if (E_eV < NV_eV || E_eV > 504.3)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 4.471;
  const double xm1 = x - 1.;
  return 8.376e-17 * xm1 * xm1 * pow(x, -2.4985) / pow(1. + sqrt(x/32.97), 6.003);
}

/* NVI photoionization cross-section over [552.1, 50000] eV. */
static inline double sigma_NVI(const double E_eV) {
  if (E_eV < NVI_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 69.43;
  const double xm1 = x - 1.;
  return 1.519e-16 * xm1 * xm1 * pow(x, -4.3425) / pow(1. + sqrt(x/26.27), 2.315);
}

/* NVII photoionization cross-section over [667.1, 50000] eV. */
static inline double sigma_NVII(const double E_eV) {
  if (E_eV < NVII_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 21.08;
  const double xm1 = x - 1.;
  return 1.117e-15 * xm1 * xm1 * pow(x, -4.0185) / pow(1. + sqrt(x/32.88), 2.963);
}

/**********/
/* Oxygen */
/**********/

/* OI photoionization cross-section over [13.62, 538] eV. */
static inline double sigma_OI(const double E_eV) {
  if (E_eV < OI_eV || E_eV > 538.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 1.24 - 8.698;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.01615441);
  return 1.745e-15 * (xm1 * xm1 + 0.0057592921) * pow(y, 3.32) / pow(1. + sqrt(y/3.784), 17.64);
}

/* OII photoionization cross-section over [35.12, 558.1] eV. */
static inline double sigma_OII(const double E_eV) {
  if (E_eV < OII_eV || E_eV > 558.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 1.386 - 21.31;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0002259009);
  return 5.967e-17 * (xm1 * xm1 + 0.0003740356) * pow(y, -1.0285) / pow(1. + sqrt(y/31.75), 8.943);
}

/* OIII photoionization cross-section over [54.94, 584] eV. */
static inline double sigma_OIII(const double E_eV) {
  if (E_eV < OIII_eV || E_eV > 584.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1723 - 0.003839;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.20875761);
  return 6.753e-16 * (xm1 * xm1 + 0.01418481) * pow(y, -2.089) / pow(1. + sqrt(y/385.2), 6.822);
}

/* OIV photoionization cross-section over [77.41, 614.4] eV. */
static inline double sigma_OIV(const double E_eV) {
  if (E_eV < OIV_eV || E_eV > 614.4)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.2044 - 332.8;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 1836.1225);
  return 8.659e-19 * (xm1 * xm1 + 9.878449) * pow(y, -1.1075) / pow(1. + sqrt(y/493.1), 8.785);
}

/* OV photoionization cross-section over [113.9, 649.1] eV. */
static inline double sigma_OV(const double E_eV) {
  if (E_eV < OV_eV || E_eV > 649.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.854 - 0.03036;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0030846916);
  return 1.642e-14 * (xm1 * xm1 + 804.2896) * pow(y, 7.735) / pow(1. + sqrt(y/1.792), 26.47);
}

/* OVI photoionization cross-section over [138.1, 683.7] eV. */
static inline double sigma_OVI(const double E_eV) {
  if (E_eV < OVI_eV || E_eV > 683.7)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 7.824;
  const double xm1 = x - 1.;
  return 6.864e-17 * xm1 * xm1 * pow(x, -2.7525) / pow(1. + sqrt(x/32.1), 5.495);
}

/* OVII photoionization cross-section over [739.3, 50000] eV. */
static inline double sigma_OVII(const double E_eV) {
  if (E_eV < OVII_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 87.09;
  const double xm1 = x - 1.;
  return 1.329e-16 * xm1 * xm1 * pow(x, -4.332) / pow(1. + sqrt(x/25.35), 2.336);
}

/* OVIII photoionization cross-section over [871.4, 50000] eV. */
static inline double sigma_OVIII(const double E_eV) {
  if (E_eV < OVIII_eV || E_eV > 50000.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 27.54;
  const double xm1 = x - 1.;
  return 8.554e-16 * xm1 * xm1 * pow(x, -4.0185) / pow(1. + sqrt(x/32.88), 2.963);
}

/********/
/* Neon */
/********/

/* NeI photoionization cross-section over [21.56, 870.1] eV. */
static inline double sigma_NeI(const double E_eV) {
  if (E_eV < NeI_eV || E_eV > 870.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 4.87 - 0.04236;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 34.492129);
  return 4.287e-15 * (xm1 * xm1 + 0.05924356) * pow(y, -1.3225) / pow(1. + sqrt(y/5.798), 8.355);
}

/* NeII photoionization cross-section over [40.96, 883.1] eV. */
static inline double sigma_NeII(const double E_eV) {
  if (E_eV < NeII_eV || E_eV > 883.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 12.47 - 1.52;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.01175056);
  return 1.583e-15 * (xm1 * xm1 + 0.0043007364) * pow(y, -1.595) / pow(1. + sqrt(y/3.935), 7.81);
}

/* NeIII photoionization cross-section over [63.46, 913.1] eV. */
static inline double sigma_NeIII(const double E_eV) {
  if (E_eV < NeIII_eV || E_eV > 913.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.7753 - 76.54;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 4.092529);
  return 5.708e-18 * (xm1 * xm1 + 0.21464689) * pow(y, -0.475) / pow(1. + sqrt(y/67.25), 10.05);
}

/* NeIV photoionization cross-section over [97.12, 948] eV. */
static inline double sigma_NeIV(const double E_eV) {
  if (E_eV < NeIV_eV || E_eV > 948.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 5.566 - 5.149;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 44.715969);
  return 1.685e-15 * (xm1 * xm1 + 6.87241e-05) * pow(y, -3.972) / pow(1. + sqrt(y/640.9), 3.056);
}

/* NeV photoionization cross-section over [126.2, 987.3] eV. */
static inline double sigma_NeV(const double E_eV) {
  if (E_eV < NeV_eV || E_eV > 987.3)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 1.248 - 91.69;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.13704804);
  return 2.43e-18 * (xm1 * xm1 + 0.46991025) * pow(y, -1.0005) / pow(1. + sqrt(y/106.6), 8.999);
}

/* NeVI photoionization cross-section over [157.9, 1031] eV. */
static inline double sigma_NeVI(const double E_eV) {
  if (E_eV < NeVI_eV || E_eV > 1031.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 1.499 - 104.2;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 2.059225);
  return 9.854e-19 * (xm1 * xm1 + 2.742336) * pow(y, -1.082) / pow(1. + sqrt(y/135.), 8.836);
}

/* NeVII photoionization cross-section over [207.3, 1078] eV. */
static inline double sigma_NeVII(const double E_eV) {
  if (E_eV < NeVII_eV || E_eV > 1078.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 4.888 - 0.02536;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0019509889);
  return 1.198e-14 * (xm1 * xm1 + 790.1721) * pow(y, 7.25) / pow(1. + sqrt(y/1.788), 25.5);
}

/* NeVIII photoionization cross-section over [239.1, 1125] eV. */
static inline double sigma_NeVIII(const double E_eV) {
  if (E_eV < NeVIII_eV || E_eV > 1125.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 10.03;
  const double xm1 = x - 1.;
  return 5.631e-17 * xm1 * xm1 * pow(x, -2.7075) / pow(1. + sqrt(x/36.28), 5.585);
}

/*************/
/* Magnesium */
/*************/

/* MgI photoionization cross-section over [7.646, 54.9] eV. */
static inline double sigma_MgI(const double E_eV) {
  if (E_eV < MgI_eV || E_eV > 54.9)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 11.97;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0);
  return 1.372e-10 * (xm1 * xm1 + 0.07868025) * pow(y, 2.37) / pow(1. + sqrt(y/0.2228), 15.74);
}

/* MgII photoionization cross-section over [15.04, 65.69] eV. */
static inline double sigma_MgII(const double E_eV) {
  if (E_eV < MgII_eV || E_eV > 65.69)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 8.139;
  const double xm1 = x - 1.;
  return 3.278e-18 * xm1 * xm1 * pow(x, -3.695) / pow(1. + sqrt(x/4.341e7), 3.61);
}

/* MgIII photoionization cross-section over [80.14, 1317] eV. */
static inline double sigma_MgIII(const double E_eV) {
  if (E_eV < MgIII_eV || E_eV > 1317.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 10.86 - 4.86;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 13.853284);
  return 5.377e-16 * (xm1 * xm1 + 6.780816) * pow(y, -1.9415) / pow(1. + sqrt(y/9.779), 7.117);
}

/* MgIV photoionization cross-section over [109.3, 1356] eV. */
static inline double sigma_MgIV(const double E_eV) {
  if (E_eV < MgIV_eV || E_eV > 1356.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 29.12 - 0.9402;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.01288225);
  return 1.394e-15 * (xm1 * xm1 + 0.0018714276) * pow(y, -2.2565) / pow(1. + sqrt(y/2.895), 6.487);
}

/* MgV photoionization cross-section over [141.3, 1400] eV. */
static inline double sigma_MgV(const double E_eV) {
  if (E_eV < MgV_eV || E_eV > 1400.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.9762 - 127.6;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 15.832441);
  return 1.728e-18 * (xm1 * xm1 + 0.654481) * pow(y, -0.47) / pow(1. + sqrt(y/91.84), 10.06);
}

/* MgVI photoionization cross-section over [186.5, 1449] eV. */
static inline double sigma_MgVI(const double E_eV) {
  if (E_eV < MgVI_eV || E_eV > 1449.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 1.711 - 100.7;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 2.989441);
  return 2.185e-18 * (xm1 * xm1 + 0.40005625) * pow(y, -0.899) / pow(1. + sqrt(y/93.5), 9.202);
}

/* MgVII photoionization cross-section over [224.9, 1501] eV. */
static inline double sigma_MgVII(const double E_eV) {
  if (E_eV < MgVII_eV || E_eV > 1501.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 3.57 - 54.52;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 4.318084);
  return 3.104e-18 * (xm1 * xm1 + 2.022084) * pow(y, -1.0715) / pow(1. + sqrt(y/60.6), 8.857);
}

/* MgVIII photoionization cross-section over [266, 1558] eV. */
static inline double sigma_MgVIII(const double E_eV) {
  if (E_eV < MgVIII_eV || E_eV > 1558.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.4884 - 534.8;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 1.5976009e-05);
  return 6.344e-20 * (xm1 * xm1 + 0.44435556) * pow(y, -0.8075) / pow(1. + sqrt(y/508.5), 9.385);
}

/* MgIX photoionization cross-section over [328.2, 1618] eV. */
static inline double sigma_MgIX(const double E_eV) {
  if (E_eV < MgIX_eV || E_eV > 1618.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 34.82 - 5.444;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0062694724);
  return 9.008e-16 * (xm1 * xm1 + 7.568001) * pow(y, 1.72) / pow(1. + sqrt(y/1.823), 14.44);
}

/* MgX photoionization cross-section over [367.5, 1675] eV. */
static inline double sigma_MgX(const double E_eV) {
  if (E_eV < MgX_eV || E_eV > 1675.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 14.52;
  const double xm1 = x - 1.;
  return 4.427e-17 * xm1 * xm1 * pow(x, -2.77) / pow(1. + sqrt(x/38.26), 5.46);
}

/***********/
/* Silicon */
/***********/

/* SiI photoionization cross-section over [8.152, 106] eV. */
static inline double sigma_SiI(const double E_eV) {
  if (E_eV < SiI_eV || E_eV > 106.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 23.17 - 1.672e-05;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.17698849);
  return 2.506e-17 * (xm1 * xm1 + 0.08048569) * pow(y, -3.727) / pow(1. + sqrt(y/20.57), 3.546);
}

/* SiII photoionization cross-section over [16.35, 118.6] eV. */
static inline double sigma_SiII(const double E_eV) {
  if (E_eV < SiII_eV || E_eV > 118.6)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.556 - 6.634;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.01617984);
  return 4.14e-18 * (xm1 * xm1 + 2.4649) * pow(y, 0.455) / pow(1. + sqrt(y/13.37), 11.91);
}

/* SiIII photoionization cross-section over [33.49, 131.1] eV. */
static inline double sigma_SiIII(const double E_eV) {
  if (E_eV < SiIII_eV || E_eV > 131.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1659 - 96.13;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.41499364);
  return 5.79e-22 * (xm1 * xm1 + 0.74407876) * pow(y, 1.18) / pow(1. + sqrt(y/147.4), 13.36);
}

/* SiIV photoionization cross-section over [45.14, 146.6] eV. */
static inline double sigma_SiIV(const double E_eV) {
  if (E_eV < SiIV_eV || E_eV > 146.6)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 12.88;
  const double xm1 = x - 1.;
  return 6.083e-18 * xm1 * xm1 * pow(x, -3.8235) / pow(1. + sqrt(x/1.356e6), 3.353);
}

/* SiV photoionization cross-section over [166.8, 1887] eV. */
static inline double sigma_SiV(const double E_eV) {
  if (E_eV < SiV_eV || E_eV > 1887.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.7761 - 200.9;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 20.584369);
  return 8.863e-19 * (xm1 * xm1 + 1.697809) * pow(y, -0.51) / pow(1. + sqrt(y/154.1), 9.98);
}

/* SiVI photoionization cross-section over [205.1, 1946] eV. */
static inline double sigma_SiVI(const double E_eV) {
  if (E_eV < SiVI_eV || E_eV > 1946.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 63.05 - 1.115;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0064818601);
  return 7.293e-17 * (xm1 * xm1 + 8.934121e-06) * pow(y, -4.3) / pow(1. + sqrt(y/155.8), 2.4);
}

/* SiVII photoionization cross-section over [246.5, 2001] eV. */
static inline double sigma_SiVII(const double E_eV) {
  if (E_eV < SiVII_eV || E_eV > 2001.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.3277 - 0.01149;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.40908816);
  return 6.68e-20 * (xm1 * xm1 + 10.7584) * pow(y, 2.53) / pow(1. + sqrt(y/41.32), 16.06);
}

/* SiVIII photoionization cross-section over [303.2, 2058] eV. */
static inline double sigma_SiVIII(const double E_eV) {
  if (E_eV < SiVIII_eV || E_eV > 2058.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.7655 - 385.;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0080982001);
  return 3.477e-19 * (xm1 * xm1 + 2.178576e-06) * pow(y, -1.007) / pow(1. + sqrt(y/373.3), 8.986);
}

/* SiIX photoionization cross-section over [351.1, 2125] eV. */
static inline double sigma_SiIX(const double E_eV) {
  if (E_eV < SiIX_eV || E_eV > 2125.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.3343 - 1036.;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.08620096);
  return 1.465e-19 * (xm1 * xm1 + 2.709316) * pow(y, -1.2485) / pow(1. + sqrt(y/1404.), 8.503);
}

/* SiX photoionization cross-section over [401.4, 2194] eV. */
static inline double sigma_SiX(const double E_eV) {
  if (E_eV < SiX_eV || E_eV > 2194.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.8787 - 452.8;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 1.030225);
  return 1.95e-19 * (xm1 * xm1 + 0.20151121) * pow(y, -1.349) / pow(1. + sqrt(y/746.1), 8.302);
}

/* SiXI photoionization cross-section over [476.1, 2268] eV. */
static inline double sigma_SiXI(const double E_eV) {
  if (E_eV < SiXI_eV || E_eV > 2268.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 12.05 - 0.0199;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0001014049);
  return 1.992e-14 * (xm1 * xm1 + 572.1664) * pow(y, 6.625) / pow(1. + sqrt(y/1.582), 24.25);
}

/* SiXII photoionization cross-section over [523.5, 2336] eV. */
static inline double sigma_SiXII(const double E_eV) {
  if (E_eV < SiXII_eV || E_eV > 2336.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 35.6;
  const double xm1 = x - 1.;
  return 2.539e-17 * xm1 * xm1 * pow(x, -3.136) / pow(1. + sqrt(x/33.07), 4.728);
}

/**********/
/* Sulfer */
/**********/

/* SI photoionization cross-section over [10.36, 170] eV. */
static inline double sigma_SI(const double E_eV) {
  if (E_eV < SI_eV || E_eV > 170.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 18.08 - 0.9935;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.06180196);
  return 4.564e-14 * (xm1 * xm1 + 0.40768225) * pow(y, 1.305) / pow(1. + sqrt(y), 13.61);
}

/* SII photoionization cross-section over [23.33, 184.6] eV. */
static inline double sigma_SII(const double E_eV) {
  if (E_eV < SII_eV || E_eV > 184.6)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 8.787 - 2.782;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.03196944);
  return 3.136e-16 * (xm1 * xm1 + 0.54081316) * pow(y, 0.905) / pow(1. + sqrt(y/3.442), 12.81);
}

/* SIII photoionization cross-section over [34.83, 199.5] eV. */
static inline double sigma_SIII(const double E_eV) {
  if (E_eV < SIII_eV || E_eV > 199.5)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.027 - 15.68;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 88.755241);
  return 6.666e-18 * (xm1 * xm1 + 16.883881) * pow(y, -1.1945) / pow(1. + sqrt(y/54.54), 8.611);
}

/* SIV photoionization cross-section over [47.31, 216.4] eV. */
static inline double sigma_SIV(const double E_eV) {
  if (E_eV < SIV_eV || E_eV > 216.4)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.173 - 19.75;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 11.296321);
  return 2.606e-18 * (xm1 * xm1 + 3.470769) * pow(y, -1.1725) / pow(1. + sqrt(y/66.41), 8.655);
}

/* SV photoionization cross-section over [72.68, 235] eV. */
static inline double sigma_SV(const double E_eV) {
  if (E_eV < SV_eV || E_eV > 235.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1713 - 94.24;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.39250225);
  return 5.072e-22 * (xm1 * xm1 + 0.620944) * pow(y, 1.035) / pow(1. + sqrt(y/198.6), 13.07);
}

/* SVI photoionization cross-section over [88.05, 255.7] eV. */
static inline double sigma_SVI(const double E_eV) {
  if (E_eV < SVI_eV || E_eV > 255.7)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 14.13;
  const double xm1 = x - 1.;
  return 9.139e-18 * xm1 * xm1 * pow(x, -3.687) / pow(1. + sqrt(x/1656.), 3.626);
}

/* SVII photoionization cross-section over [280.9, 2569] eV. */
static inline double sigma_SVII(const double E_eV) {
  if (E_eV < SVII_eV || E_eV > 2569.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.3757 - 222.2;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 21.215236);
  return 5.703e-19 * (xm1 * xm1 + 2.259009) * pow(y, 0.175) / pow(1. + sqrt(y/146.), 11.35);
}

/* SVIII photoionization cross-section over [328.2, 2641] eV. */
static inline double sigma_SVIII(const double E_eV) {
  if (E_eV < SVIII_eV || E_eV > 2641.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 14.62 - 18.69;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.09223369);
  return 3.161e-17 * (xm1 * xm1 + 1.329409e-06) * pow(y, -1.179) / pow(1. + sqrt(y/16.11), 8.642);
}

/* SIX photoionization cross-section over [379.1, 2705] eV. */
static inline double sigma_SIX(const double E_eV) {
  if (E_eV < SIX_eV || E_eV > 2705.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1526 - 0.001615;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.16394401);
  return 9.646e-15 * (xm1 * xm1 + 2.226064) * pow(y, -2.5115) / pow(1. + sqrt(y/1438.), 5.977);
}

/* SX photoionization cross-section over [447.1, 2782] eV. */
static inline double sigma_SX(const double E_eV) {
  if (E_eV < SX_eV || E_eV > 2782.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 10.4 - 17.75;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 2.765569);
  return 5.364e-17 * (xm1 * xm1 + 5.3361) * pow(y, -1.955) / pow(1. + sqrt(y/36.41), 7.09);
}

/* SXI photoionization cross-section over [504.8, 2859] eV. */
static inline double sigma_SXI(const double E_eV) {
  if (E_eV < SXI_eV || E_eV > 2859.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 6.485 - 34.26;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.018769);
  return 1.275e-17 * (xm1 * xm1 + 2.815684) * pow(y, -1.654) / pow(1. + sqrt(y/65.83), 7.692);
}

/* SXII photoionization cross-section over [564.7, 2941] eV. */
static inline double sigma_SXII(const double E_eV) {
  if (E_eV < SXII_eV || E_eV > 2941.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.443 - 227.9;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 1.373584);
  return 3.49e-19 * (xm1 * xm1 + 0.49463089) * pow(y, -1.6155) / pow(1. + sqrt(y/541.1), 7.769);
}

/* SXIII photoionization cross-section over [651.7, 3029] eV. */
static inline double sigma_SXIII(const double E_eV) {
  if (E_eV < SXIII_eV || E_eV > 3029.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 14.74 - 0.02203;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.0001151329);
  return 2.294e-14 * (xm1 * xm1 + 749.6644) * pow(y, 7.34) / pow(1. + sqrt(y/1.529), 25.68);
}

/* SXIV photoionization cross-section over [707.2, 3107] eV. */
static inline double sigma_SXIV(const double E_eV) {
  if (E_eV < SXIV_eV || E_eV > 3107.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 33.1;
  const double xm1 = x - 1.;
  return 2.555e-17 * xm1 * xm1 * pow(x, -2.9815) / pow(1. + sqrt(x/38.21), 5.037);
}

/********/
/* Iron */
/********/

/* FeI photoionization cross-section over [7.902, 66] eV. */
static inline double sigma_FeI(const double E_eV) {
  if (E_eV < FeI_eV || E_eV > 66)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.05461 - 138.2;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.06155361);
  return 3.062e-19 * (xm1 * xm1 + 428.0761) * pow(y, -1.5385) / pow(1. + sqrt(y/2.671e7), 7.923);
}

/* FeII photoionization cross-section over [16.19, 76.17] eV. */
static inline double sigma_FeII(const double E_eV) {
  if (E_eV < FeII_eV || E_eV > 76.17)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1761 - 92.72;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 11556.25);
  return 4.365e-15 * (xm1 * xm1 + 130.1881) * pow(y, -2.898) / pow(1. + sqrt(y/6298), 5.204);
}

/* FeIII photoionization cross-section over [30.65, 87.05] eV. */
static inline double sigma_FeIII(const double E_eV) {
  if (E_eV < FeIII_eV || E_eV > 87.05)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1698 - 176;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 341.1409);
  return 6.107e-18 * (xm1 * xm1 + 75.655204) * pow(y, -1.4725) / pow(1. + sqrt(y/1555), 8.055);
}

/* FeIV photoionization cross-section over [54.8, 106.7] eV. */
static inline double sigma_FeIV(const double E_eV) {
  if (E_eV < FeIV_eV || E_eV > 106.7)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 25.44;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0);
  return 3.653e-16 * (xm1 * xm1 + 0.31382404) * pow(y, -2.231) / pow(1. + sqrt(y/8.913), 6.538);
}

/* FeV photoionization cross-section over [75.01, 128.8] eV. */
static inline double sigma_FeV(const double E_eV) {
  if (E_eV < FeV_eV || E_eV > 128.8)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.7256 - 88.71;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.00278784);
  return 1.523e-21 * (xm1 * xm1 + 2564.4096) * pow(y, 3.335) / pow(1. + sqrt(y/37.36), 17.67);
}

/* FeVI photoionization cross-section over [99.06, 152.7] eV. */
static inline double sigma_FeVI(const double E_eV) {
  if (E_eV < FeVI_eV || E_eV > 152.7)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 2.656 - 33.61;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 1.4010049e-05);
  return 5.259e-19 * (xm1 * xm1 + 242.7364) * pow(y, 2.66) / pow(1. + sqrt(y/14.5), 16.32);
}

/* FeVII photoionization cross-section over [125, 178.3] eV. */
static inline double sigma_FeVII(const double E_eV) {
  if (E_eV < FeVII_eV || E_eV > 178.3)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 5.059 - 0.4546;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 719.8489);
  return 2.42e-14 * (xm1 * xm1 + 6.330256e-06) * pow(y, -4.313) / pow(1. + sqrt(y/48500.), 2.374);
}

/* FeVIII photoionization cross-section over [151.1, 205.5] eV. */
static inline double sigma_FeVIII(const double E_eV) {
  if (E_eV < FeVIII_eV || E_eV > 205.5)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.07098 - 2542.;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 218275.84);
  return 1.979e-17 * (xm1 * xm1 + 46569.64) * pow(y, -2.125) / pow(1. + sqrt(y/17450.), 6.75);
}

/* FeIX photoionization cross-section over [233.6, 921.1] eV. */
static inline double sigma_FeIX(const double E_eV) {
  if (E_eV < FeIX_eV || E_eV > 921.1)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 6.741 - 24.94;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 68.079001);
  return 2.687e-17 * (xm1 * xm1 + 5.697769e-08) * pow(y, -2.355) / pow(1. + sqrt(y/180.7), 6.29);
}

/* FeX photoionization cross-section over [262.1, 959] eV. */
static inline double sigma_FeX(const double E_eV) {
  if (E_eV < FeX_eV || E_eV > 959.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 68.86 - 1.19e-05;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 4.31649e-05);
  return 6.47e-17 * (xm1 * xm1 + 7.717284e-08) * pow(y, -3.4445) / pow(1. + sqrt(y/20.62), 4.111);
}

/* FeXI photoionization cross-section over [290.2, 998.3] eV. */
static inline double sigma_FeXI(const double E_eV) {
  if (E_eV < FeXI_eV || E_eV > 998.3)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 8.284 - 29.71;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.272484);
  return 3.281e-18 * (xm1 * xm1 + 0.10751841) * pow(y, -1.2145) / pow(1. + sqrt(y/53.6), 8.571);
}

/* FeXII photoionization cross-section over [330.8, 1039] eV. */
static inline double sigma_FeXII(const double E_eV) {
  if (E_eV < FeXII_eV || E_eV > 1039.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 6.295 - 46.71;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 0.02030625);
  return 1.738e-18 * (xm1 * xm1 + 0.09585216) * pow(y, -1.4815) / pow(1. + sqrt(y/113.), 8.037);
}

/* FeXIII photoionization cross-section over [361, 1081] eV. */
static inline double sigma_FeXIII(const double E_eV) {
  if (E_eV < FeXIII_eV || E_eV > 1081.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.1317 - 2170.;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 4.6949904e-05);
  return 2.791e-21 * (xm1 * xm1 + 0.48135844) * pow(y, -0.6045) / pow(1. + sqrt(y/2487.), 9.791);
}

/* FeXIV photoionization cross-section over [392.2, 1125] eV. */
static inline double sigma_FeXIV(const double E_eV) {
  if (E_eV < FeXIV_eV || E_eV > 1125.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.8509 - 450.5;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 6.270016);
  return 1.454e-19 * (xm1 * xm1 + 0.24373969) * pow(y, -1.467) / pow(1. + sqrt(y/1239), 8.066);
}

/* FeXV photoionization cross-section over [457, 1181] eV. */
static inline double sigma_FeXV(const double E_eV) {
  if (E_eV < FeXV_eV || E_eV > 1181.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 0.05555 - 0.0002706;
  const double xm1 = x - 1.;
  const double y = sqrt(x*x + 2.650384);
  return 2.108e-16 * (xm1 * xm1 + 3.553225e-06) * pow(y, -2.4835) / pow(1. + sqrt(y/20450), 6.033);
}

/* FeXVI photoionization cross-section over [489.3, 1216] eV. */
static inline double sigma_FeXVI(const double E_eV) {
  if (E_eV < FeXVI_eV || E_eV > 1216.)
    return sigma_zero;                       // Check ionization threshold
  const double x = E_eV / 28.73;
  const double xm1 = x - 1.;
  return 1.207e-17 * xm1 * xm1 * pow(x, -3.577) / pow(1. + sqrt(x/515.), 3.846);
}
