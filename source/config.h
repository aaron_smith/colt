/*!
 * \file    config.h
 *
 * \brief   General configuration for the simulation.
 *
 * \details This file contains general functionality for reading config files.
 *          Everything here depends on utilities from the yamp-cpp library.
 */

#ifndef CONFIG_INCLUDED
#define CONFIG_INCLUDED

#include "proto.h"
#include "yaml-cpp/yaml.h"

//! Custom extensions for the yaml-cpp library.
namespace YAML {
//! Define yaml to Vec3 data conversion.
template<>
struct convert<Vec3> {
  /*! \brief Encode a Vec3 object into a yaml node.
   *
   *  \param[in] vec Vector of (x,y,z) data.
   *
   *  \return Node within a yaml file representation.
   */
  static Node encode(const Vec3& vec) {
    Node node;
    node.push_back(vec.x);
    node.push_back(vec.y);
    node.push_back(vec.z);
    return node;
  }

  /*! \brief Decode a yaml node as a Vec3 object.
   *
   *  \param[in] node Node within a yaml file representation.
   *  \param[in,out] vec Vector of (x,y,z) data.
   *
   *  \return Flag indicating a successful decode.
   */
  static bool decode(const Node& node, Vec3& vec) {
    if (!node.IsSequence() || node.size() != 3) {
      return false;
    }

    vec.x = node[0].as<double>();
    vec.y = node[1].as<double>();
    vec.z = node[2].as<double>();
    return true;
  }
};

/*! \brief Define yaml output stream operations for Vec3 objects.
 *
 *  \param[in] out Emitter for yaml nodes.
 *  \param[in] vec Vector of (x,y,z) data.
 *
 *  \return Emitter after updating with the Vec3 output stream.
 */
inline Emitter& operator << (Emitter& out, const Vec3& vec) {
  out << Flow;
  out << BeginSeq << vec.x << vec.y << vec.z << EndSeq;
  return out;
}

//! Define yaml to Vec2 data conversion.
template<>
struct convert<Vec2> {
  /*! \brief Encode a Vec2 object into a yaml node.
   *
   *  \param[in] vec Vector of (x,y) data.
   *
   *  \return Node within a yaml file representation.
   */
  static Node encode(const Vec2& vec) {
    Node node;
    node.push_back(vec.x);
    node.push_back(vec.y);
    return node;
  }

  /*! \brief Decode a yaml node as a Vec2 object.
   *
   *  \param[in] node Node within a yaml file representation.
   *  \param[in,out] vec Vector of (x,y) data.
   *
   *  \return Flag indicating a successful decode.
   */
  static bool decode(const Node& node, Vec2& vec) {
    if (!node.IsSequence() || node.size() != 2) {
      return false;
    }

    vec.x = node[0].as<double>();
    vec.y = node[1].as<double>();
    return true;
  }
};

/*! \brief Define yaml output stream operations for Vec2 objects.
 *
 *  \param[in] out Emitter for yaml nodes.
 *  \param[in] vec Vector of (x,y) data.
 *
 *  \return Emitter after updating with the Vec2 output stream.
 */
inline Emitter& operator << (Emitter& out, const Vec2& vec) {
  out << Flow;
  out << BeginSeq << vec.x << vec.y << EndSeq;
  return out;
}

//! Define yaml to a tuple of strings data conversion.
template<>
struct convert<tuple<string, string, string>> {
  /*! \brief Encode a tuple of strings into a yaml node.
   *
   *  \param[in] rhs Tuple of strings.
   *
   *  \return Node within a yaml file representation.
   */
  static Node encode(const tuple<string, string, string>& rhs) {
    Node node;
    node.push_back(std::get<0>(rhs));
    node.push_back(std::get<1>(rhs));
    node.push_back(std::get<2>(rhs));
    return node;
  }

  /*! \brief Decode a yaml node as a tuple of strings.
   *
   *  \param[in] node Node within a yaml file representation.
   *  \param[in,out] rhs Tuple of strings.
   *
   *  \return Flag indicating a successful decode.
   */
  static bool decode(const Node& node, tuple<string, string, string>& rhs) {
    if (!node.IsSequence() || node.size() != 3) {
      return false;
    }
    rhs = make_tuple(node[0].as<string>(), node[1].as<string>(), node[2].as<string>());
    return true;
  }
};

/*! \brief Define yaml output stream operations for a tuple of strings.
 *
 *  \param[in] out Emitter for yaml nodes.
 *  \param[in] rhs Tuple of strings.
 *
 *  \return Emitter after updating with the tuple of strings output stream.
 */
inline Emitter& operator << (Emitter& out, const tuple<string, string, string>& rhs) {
  out << Flow;
  out << BeginSeq << std::get<0>(rhs) << std::get<1>(rhs) << std::get<2>(rhs) << EndSeq;
  return out;
}
} // end YAML

//! Convenience load macro keeping defualt values if not specified.
#define load(...) load_info(file, __VA_ARGS__);

/*! \brief Perform sanity checking on a loaded value.
 *
 *  \param[in] name Name of the parameter or data to check.
 *  \param[in] val Value to check.
 *  \param[in] cond Sanity check condition for the value.
 */
template <typename T>
inline void sanity_check(const string name, const T& val, const string cond) {
  if (cond == ">0") {                        // Validate range (positive)
    if (!(val > static_cast<T>(0)))
      root_error(name + " must be > 0 but got " + to_string(val));
  } else if (cond == ">=0") {                // Validate range (non-negative)
    if (!(val >= static_cast<T>(0)))
      root_error(name + " must be >= 0 but got " + to_string(val));
  } else if (cond == ">=1") {                // Validate range (non-decreasing)
    if (!(val >= static_cast<T>(1)))
      root_error(name + " must be >= 1 but got " + to_string(val));
  } else if (cond == "[0,1]") {              // Validate range (fraction)
    if (!(val >= static_cast<T>(0) && val <= static_cast<T>(1)))
      root_error(name + " must be in the range [0,1] but got " + to_string(val));
  } else if (cond == "(0,1]") {              // Validate range (positive fraction)
    if (!(val > static_cast<T>(0) && val <= static_cast<T>(1)))
      root_error(name + " must be in the range (0,1] but got " + to_string(val));
  } else if (cond == "(0,1)") {              // Validate range (interior fraction)
    if (!(val > static_cast<T>(0) && val < static_cast<T>(1)))
      root_error(name + " must be in the range (0,1) but got " + to_string(val));
  } else if (cond == "[-1,1]") {             // Validate range (anisotropy)
    if (!(val >= static_cast<T>(-1) && val <= static_cast<T>(1)))
      root_error(name + " must be in the range [-1,1] but got " + to_string(val));
  } else if (cond == "[1,100]") {            // Validate range (iterations)
    if (!(val >= static_cast<T>(1) && val <= static_cast<T>(100)))
      root_error(name + " must be in the range [1,100] but got " + to_string(val));
  } else if (cond == "[0,30]") {             // Validate range (small angle)
    if (!(val >= static_cast<T>(0) && val <= static_cast<T>(30)))
      root_error(name + " must be in [0,30] degrees but got " + to_string(val));
  } else if (cond == "[0,0.01]") {           // Validate range (small fraction)
    if (!(val >= static_cast<T>(0) && val <= static_cast<T>(0.01)))
      root_error(name + " must be in the range [0,0.01] but got " + to_string(val));
  } else
    root_error(name + " has an unrecognized sanity condition: " + cond);
}

/*! \brief Load an optional scalar quantity from a yaml file.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded scalar value.
 *  \param[in] cond Sanity check condition for the value.
 */
template <typename T>
void load_info(YAML::Node& file, const string name, T& val, const string cond = "") {
  if (file[name]) {
    val = file[name].as<T>();                // Set value from file
    file.remove(name);                       // Remove from the file
    if constexpr (std::is_arithmetic<T>::value) if (cond != "")
      sanity_check(name, val, cond);         // Sanity check the value
  }
  if (verbose && root)
    cout << name << ": " << val << endl;     // Verbose configuration
}

/*! \brief Load an optional vector quantity from a yaml file.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded vector of values.
 */
template <typename T>
void load_info(YAML::Node& file, const string name, vector<T>& val) {
  if (file[name]) {
    vector<T> vec = file[name].as<vector<T>>();
    val.resize(0);                           // Overwrite the original vector
    for (auto& vec_i : vec)
      val.push_back(vec_i);                  // Set value from file
    file.remove(name);                       // Remove from the file
  }
  if (verbose && root) {
    cout << name << ": [ ";                  // Verbose configuration
    for (auto& val_i : val) {
      if constexpr (std::is_same_v<T, std::tuple<string, string, string>>) {
        cout << "(" << std::get<0>(val_i) << ", " << std::get<1>(val_i) << ", " << std::get<2>(val_i) << "), ";
      } else {
        cout << val_i << ", ";
      }
    }
    cout << "]" << endl;
  }
}

//! Convenience subload macro keeping defualt values if not specified.
#define subload(name, val) subload_info(subfile, name, val);

/*! \brief Load an optional scalar quantity from a yaml subfile.
 *
 *  \param[in] subfile Config subfile read as a yaml representation.
 *  \param[in] name Name of the parameter or data to subload.
 *  \param[in,out] val Reference for the decoded scalar value.
 */
template <typename T>
void subload_info(YAML::Node& subfile, const string name, T& val) {
  if (subfile[name]) {
    val = subfile[name].as<T>();             // Set value from subfile
    subfile.remove(name);                    // Remove from the subfile
  }
}

/*! \brief Load an optional vector quantity from a yaml subfile.
 *
 *  \param[in] subfile Config subfile read as a yaml representation.
 *  \param[in] name Name of the parameter or data to subload.
 *  \param[in,out] val Reference for the decoded vector of values.
 */
template <typename T>
void subload_info(YAML::Node& subfile, const string name, vector<T>& val) {
  if (subfile[name]) {
    vector<T> vec = subfile[name].as<vector<T>>();
    val.resize(0);                           // Overwrite the original vector
    for (auto& vec_i : vec)
      val.push_back(vec_i);                  // Set value from subfile
    subfile.remove(name);                    // Remove from the subfile
  }
}

//! Convenience require macro keeping defualt values if not specified.
#define require(name, val) require_info(file, name, val);

/*! \brief Load a required scalar and raise an error if unsuccessful.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded scalar value.
 */
template <typename T>
void require_info(YAML::Node& file, const string name, T& val) {
  if (!file[name])
    root_error("Required parameter '" + name + "' is not in " + config_file);
  load_info(file, name, val);                // Otherwise load normally
}

//! Convenience load_if_false macro keeping defualt values if not specified.
#define load_if_false(cond, name, val) load_if_false_info(file, cond, name, val);

/*! \brief Consider a dependency relation and load normally otherwise.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] cond Conditional determining whether or not to load.
 *  \param[in] name Name of the parameter or data to load.
 *  \param[in,out] val Reference for the decoded scalar value.
 */
void load_if_false_info(YAML::Node& file, const bool cond, const string name, bool& val);

//! Convenience macro raising an error if incompatible options are specified.
#define incompatible(...) incompatible_info(file, config_file, strings{__VA_ARGS__});

/*! \brief Raise an error if incompatible options are specified.
 *
 *  \param[in] file Config file read as a yaml representation.
 *  \param[in] config Name of the config file.
 *  \param[in] names Names of the incompatible parameters.
 */
void incompatible_info(YAML::Node& file, const string config, const strings names);

//! Convenience macro for checking runtime vs compile-time parameters.
#define runtime(...) runtime_info(file, __VA_ARGS__);

//! Convenience macro for checking runtime vs compile-time parameters.
inline void runtime_info(YAML::Node& file, const string name, const bool value) {
  bool flag = value;
  load(name, flag);
  if (flag != value)
    error(name + " is a compile-time parameter (use defines.yaml)");
};

/* Utility function for loading radial edges. */
template <typename T>
void load_radial_edges(YAML::Node& subfile, T& data) {
  const double Rvir = halo_radius;           // Virial radius [cm]
  // Radial bin information
  if (subfile["radial_linspace_pc"]) {       // Linearly spaced radial bins [pc]
    Vec3 rad_lin; subload("radial_linspace_pc", rad_lin);
    const int n_lin = int(rad_lin.z);
    if (n_lin <= 1)
      root_error("radial_linspace_pc must have n > 1");
    const double dx_lin = (rad_lin.y - rad_lin.x) / (n_lin - 1);
    data.radial_edges.resize(n_lin);
    #pragma omp parallel for
    for (int i = 0; i < n_lin; ++i)
      data.radial_edges[i] = pc * (rad_lin.x + double(i) * dx_lin); // Radial edges [cm]
  } else if (subfile["radial_logspace_pc"]) { // Logarithmically spaced radial bins [pc]
    Vec3 rad_log; subload("radial_logspace_pc", rad_log);
    const int n_log = int(rad_log.z);
    if (n_log <= 1)
      root_error("radial_logspace_pc must have n > 1");
    const double dx_log = (rad_log.y - rad_log.x) / (n_log - 1);
    data.radial_edges.resize(n_log + 1);
    #pragma omp parallel for
    for (int i = 0; i < n_log; ++i)
      data.radial_edges[i+1] = pc * pow(10., rad_log.x + double(i) * dx_log); // Radial edges [cm]
  } else if (subfile["radial_linspace_Rvir"] && Rvir > 0.) { // Linearly spaced radial bins [Rvir]
    Vec3 rad_lin; subload("radial_linspace_Rvir", rad_lin);
    const int n_lin = int(rad_lin.z);
    if (n_lin <= 1)
      root_error("radial_linspace_Rvir must have n > 1");
    const double dx_lin = (rad_lin.y - rad_lin.x) / (n_lin - 1);
    data.radial_edges.resize(n_lin);
    #pragma omp parallel for
    for (int i = 0; i < n_lin; ++i)
      data.radial_edges[i] = Rvir * (rad_lin.x + double(i) * dx_lin); // Radial edges [cm]
  } else if (subfile["radial_logspace_Rvir"] && Rvir > 0.) { // Logarithmically spaced radial bins [Rvir]
    Vec3 rad_log; subload("radial_logspace_Rvir", rad_log);
    const int n_log = int(rad_log.z);
    if (n_log <= 1)
      root_error("radial_logspace_Rvir must have n > 1");
    const double dx_log = (rad_log.y - rad_log.x) / (n_log - 1);
    data.radial_edges.resize(n_log + 1);
    #pragma omp parallel for
    for (int i = 0; i < n_log; ++i)
      data.radial_edges[i+1] = Rvir * pow(10., rad_log.x + double(i) * dx_log); // Radial edges [cm]
  } else if (subfile["radial_edges_pc"]) {
    subload("radial_edges_pc", data.radial_edges);
    const int n_radial_edges = data.radial_edges.size();
    for (int i = 0; i < n_radial_edges; ++i)
      data.radial_edges[i] *= pc;            // Convert to cm
  } else if (subfile["radial_edges_Rvir"] && Rvir > 0.) {
    subload("radial_edges_Rvir", data.radial_edges);
    const int n_radial_edges = data.radial_edges.size();
    for (int i = 0; i < n_radial_edges; ++i)
      data.radial_edges[i] *= Rvir;          // Convert to cm
  } else if (subfile["radial_edges_cm"]) {
    subload("radial_edges_cm", data.radial_edges); // Already in cm
  } else {
    data.radial_edges = {0., pc, 10.*pc, 1e2*pc, kpc, 10.*kpc, 100.*kpc, Mpc}; // Radial edges [cm]
  }
  data.n_radial_bins = data.radial_edges.size() - 1; // Number of radial bins
  if (data.n_radial_bins <= 0)
    root_error("Must have at least one radial bin!");
  data.radial_edges[0] = 0.;                 // Extend to the origin
  if (Rvir > 0. && data.radial_edges[data.n_radial_bins] < Rvir && root)
    cout << "Warning: Radial bin edges do not reach Rvir!" << endl;
  for (int radial_bin = 0; radial_bin < data.n_radial_bins; ++radial_bin) {
    if (data.radial_edges[radial_bin+1] <= data.radial_edges[radial_bin])
      root_error("Radial bin edges must be monotonically increasing!");
  }
}

/* Utility function for loading radial edges. */
template <typename T>
void load_radial_edges(YAML::Node& subfile, vector<T>& data, bool is_group) {
  // Radial bin information
  const int n_data = data.size();            // Size of data vector
  if (subfile["radial_linspace_pc"]) {       // Linearly spaced radial bins [pc]
    Vec3 rad_lin; subload("radial_linspace_pc", rad_lin);
    const int n_lin = int(rad_lin.z), n_2d = n_data * n_lin;
    if (n_lin <= 1)
      root_error("radial_linspace_pc must have n > 1");
    const double dx_lin = (rad_lin.y - rad_lin.x) / (n_lin - 1);
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_lin);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_lin, i_radial = i % n_lin;
      data[i_data].radial_edges[i_radial] = pc * (rad_lin.x + double(i_radial) * dx_lin); // Radial edges [cm]
    }
  } else if (subfile["radial_logspace_pc"]) { // Logarithmically spaced radial bins [pc]
    Vec3 rad_log; subload("radial_logspace_pc", rad_log);
    const int n_log = int(rad_log.z), n_2d = n_data * n_log;
    if (n_log <= 1)
      root_error("radial_logspace_pc must have n > 1");
    const double dx_log = (rad_log.y - rad_log.x) / (n_log - 1);
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_log + 1);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_log, i_radial = i % n_log;
      data[i_data].radial_edges[i_radial+1] = pc * pow(10., rad_log.x + double(i_radial) * dx_log); // Radial edges [cm]
    }
  } else if (subfile["radial_linspace_Rvir"]) { // Linearly spaced radial bins [Rvir]
    Vec3 rad_lin; subload("radial_linspace_Rvir", rad_lin);
    const auto& R_halo = is_group ? R_grp_vir : R_sub_vir;
    const int n_comp = R_halo.size();
    if (n_comp != n_data)
      root_error("Radial data must have the same size as the halo: " + to_string(n_comp) + " != " + to_string(n_data));
    const int n_lin = int(rad_lin.z), n_2d = n_data * n_lin;
    if (n_lin <= 1)
      root_error("radial_linspace_Rvir must have n > 1");
    const double dx_lin = (rad_lin.y - rad_lin.x) / (n_lin - 1);
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_lin);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_lin, i_radial = i % n_lin;
      data[i_data].radial_edges[i_radial] = R_halo[i_data] * (rad_lin.x + double(i_radial) * dx_lin); // Radial edges [cm]
    }
  } else if (subfile["radial_logspace_Rvir"]) { // Logarithmically spaced radial bins [Rvir]
    Vec3 rad_log; subload("radial_logspace_Rvir", rad_log);
    const auto& R_halo = is_group ? R_grp_vir : R_sub_vir;
    const int n_comp = R_halo.size();
    if (n_comp != n_data)
      root_error("Radial data must have the same size as the halo: " + to_string(n_comp) + " != " + to_string(n_data));
    const int n_log = int(rad_log.z), n_2d = n_data * n_log;
    if (n_log <= 1)
      root_error("radial_logspace_Rvir must have n > 1");
    const double dx_log = (rad_log.y - rad_log.x) / (n_log - 1);
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_log + 1);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_log, i_radial = i % n_log;
      data[i_data].radial_edges[i_radial+1] = R_halo[i_data] * pow(10., rad_log.x + double(i_radial) * dx_log); // Radial edges [cm]
    }
  } else if (subfile["radial_edges_pc"]) {
    vector<double> radial_edges; subload("radial_edges_pc", radial_edges);
    const int n_radial_edges = radial_edges.size(), n_2d = n_data * n_radial_edges;
    #pragma omp parallel for
    for (int i = 0; i < n_radial_edges; ++i)
      radial_edges[i] *= pc;                 // Convert to cm
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_radial_edges);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_radial_edges, i_radial = i % n_radial_edges;
      data[i_data].radial_edges[i_radial] = radial_edges[i_radial]; // Copy radial edges [cm]
    }
  } else if (subfile["radial_edges_Rvir"]) {
    vector<double> radial_edges; subload("radial_edges_Rvir", radial_edges);
    const auto& R_halo = is_group ? R_grp_vir : R_sub_vir;
    const int n_comp = R_halo.size();
    if (n_comp != n_data)
      root_error("Radial data must have the same size as the halo: " + to_string(n_comp) + " != " + to_string(n_data));
    const int n_radial_edges = radial_edges.size(), n_2d = n_data * n_radial_edges;
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_radial_edges);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_radial_edges, i_radial = i % n_radial_edges;
      data[i_data].radial_edges[i_radial] = R_halo[i_data] * radial_edges[i_radial]; // Convert to cm
    }
  } else if (subfile["radial_edges_cm"]) {
    vector<double> radial_edges; subload("radial_edges_cm", radial_edges);
    const int n_radial_edges = radial_edges.size(), n_2d = n_data * n_radial_edges;
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges.resize(n_radial_edges);
    #pragma omp parallel for
    for (int i = 0; i < n_2d; ++i) {
      const int i_data = i / n_radial_edges, i_radial = i % n_radial_edges;
      data[i_data].radial_edges[i_radial] = radial_edges[i_radial]; // Copy radial edges [cm]
    }
  } else {
    #pragma omp parallel for
    for (int i = 0; i < n_data; ++i)
      data[i].radial_edges = {0., pc, 10.*pc, 1e2*pc, kpc, 10.*kpc, 100.*kpc, Mpc}; // Radial edges [cm]
  }
  #pragma omp parallel for
  for (int i = 0; i < n_data; ++i) {
    data[i].n_radial_bins = data[i].radial_edges.size() - 1; // Number of radial bins
    data[i].radial_edges[0] = 0.;            // Extend to the origin
  }
  auto& data0 = data[0];                     // Check first radial data
  if (data0.n_radial_bins <= 0)
    root_error("Must have at least one radial bin!");
  for (int radial_bin = 0; radial_bin < data0.n_radial_bins; ++radial_bin) {
    if (data0.radial_edges[radial_bin+1] <= data0.radial_edges[radial_bin])
      root_error("Radial bin edges must be monotonically increasing!");
  }
}

#endif // CONFIG_INCLUDED
