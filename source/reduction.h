/*!
 * \file    reduction.h
 *
 * \brief   Reduction operations for the code.
 *
 * \details This file contains the declaration of the reduction operations used in the code.
 */

#ifndef REDUCTION_INCLUDED
#define REDUCTION_INCLUDED

//! Reduction operation for a vector of doubles.
#pragma omp declare reduction(vec_plus : vector<double> : \
                              std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<double>())) \
                    initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

//! Reduction operation for a vector of Vec3s.
#pragma omp declare reduction(vec3_plus : vector<Vec3> : \
                              std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<Vec3>())) \
                    initializer(omp_priv = decltype(omp_orig)(omp_orig.size()))

//! Reduction operation for an Image.
#pragma omp declare reduction(image_plus : Image : \
                              std::transform(omp_out.begin(), omp_out.end(), omp_in.begin(), omp_out.begin(), std::plus<double>())) \
                    initializer(omp_priv = decltype(omp_orig)(omp_orig.nx(), omp_orig.ny()))

//! Reduction function for dust_vectors.
inline void reduce_dust(dust_vectors& omp_out, const dust_vectors& omp_in) {
  for (int i_dust = 0; i_dust < n_dust_species; ++i_dust)
    std::transform(omp_out[i_dust].begin(), omp_out[i_dust].end(), omp_in[i_dust].begin(), omp_out[i_dust].begin(), std::plus<double>());
}

//! Initialization function for dust_vectors.
inline dust_vectors initialize_to_zero(const dust_vectors& orig) {
  dust_vectors priv;
  for (int i_dust = 0; i_dust < n_dust_species; ++i_dust)
    priv[i_dust].resize(orig[i_dust].size());
  return priv;
}

//! Reduction operation for dust_vectors.
#pragma omp declare reduction(dust_plus : dust_vectors : reduce_dust(omp_out, omp_in)) \
                    initializer(omp_priv = initialize_to_zero(omp_orig))

#endif // REDUCTION_INCLUDED
