/****************
 * mcrt/proto.h *
 ****************

 * Module function declarations.

*/

#ifndef MCRT_PROTO_INCLUDED
#define MCRT_PROTO_INCLUDED

// Inherited headers
#include "../proto.h"

// Dynamical core-skipping constants
constexpr double atau0_tolerance = 4.;       // Maximum relative density change for atau criteria
constexpr double dv_tolerance = 10.;         // Maximum velocity change (vth) for atau criteria

// Line parameters
extern string line;                          // Name of the line
extern double E0;                            // Line energy [erg]
extern double A21;                           // Einstein A coefficient [Hz]
extern double f12;                           // Oscillator strength
extern double g12;                           // Degeneracy ratio: g_lower / g_upper
extern double T0;                            // Line temperature [K]
extern double nu0;                           // Line frequency [Hz]
extern double lambda0;                       // Line wavelength [angstrom]
extern double B21;                           // Einstein B21 = A21 c^2 / (2 h ν0^3)
extern double DnuL;                          // Natural frequency broadening [Hz]
extern double p_dest;                        // Line destruction probability
extern double E0F;                           // Fluorescent line energy [erg]
extern double A2F;                           // Fluorescent Einstein A coefficient [Hz]
extern double P2F;                           // Probability of fluorescence
extern double nu0F;                          // Fluorescent line frequency [Hz]
extern double lambda0F;                      // Fluorescent line wavelength [angstrom]
extern double xFDa;                          // Fluorescent line: x / a
extern double avthDc;                        // Conversion for special relativity
extern double m_carrier;                     // Mass of carrier [g]
extern bool scaled_microturb;                // Scaled microturbulence model
extern double v_turb;                        // Microturbulent velocity [cm/s]
extern double T_turb;                        // Microturbulent temperature [K]
extern double vth_div_sqrtT;                 // Thermal velocity: vth = sqrt(2 kB T / m_carrier)
extern double DnuD_div_sqrtT;                // Doppler width: ΔνD = ν0 vth / c
extern double a_sqrtT;                       // "damping parameter": a = ΔνL / 2 ΔνD
extern double sigma0_sqrtT;                  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
extern double x_div_aDv;                     // Constant for stellar continuum emission
extern double gDa;                           // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)

// Deuterium line parameters
extern double E0D;                           // Line energy [erg]
extern double nu0D;                          // Line frequency [Hz]
extern double lambda0D;                      // Line wavelength [angstrom]
extern double xDA;                           // Frequency translation: x' = A x + B a
extern double xDB;                           // Frequency translation: x' = A x + B a
extern double gDaD;                          // Recoil parameter: g' / a'

// Doublet line parameters
extern double E0p;                           // Line energy [erg]
extern double A21p;                          // Einstein A coefficient [Hz]
extern double f12p;                          // Oscillator strength
extern double g12p;                          // Degeneracy ratio: g_lower / g_upper
extern double nu0p;                          // Line frequency [Hz]
extern double lambda0p;                      // Line wavelength [angstrom]
extern double DnuLp;                         // Natural frequency broadening [Hz]
extern double E0Fp;                          // Fluorescent line energy [erg]
extern double A2Fp;                          // Fluorescent Einstein A coefficient [Hz]
extern double P2Fp;                          // Probability of fluorescence
extern double nu0Fp;                         // Fluorescent line frequency [Hz]
extern double lambda0Fp;                     // Fluorescent line wavelength [angstrom]
extern double xFpDa;                         // Fluorescent line: x' / a'
extern double xpA;                           // Frequency translation: x' = A x + B a
extern double xpB;                           // Frequency translation: x' = A x + B a
extern double apA;                           // Damping paramter factor: a' = A a
extern double sigma0p_sqrtT;                 // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
extern double gDap;                          // Recoil parameter: g' / a'

// Information about the dust model
extern double kappa_dust;                    // Dust opacity [cm^2/g dust]
extern double sigma_dust;                    // Dust cross section [cm^2/Z/hydrogen atom]
extern double albedo;                        // Dust albedo for scattering vs. absorption
extern double g_dust;                        // Anisotropy parameter: <μ> for dust scattering
extern double _1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g; // Constants for Henyey-Greenstein dust scattering

// Configurable line transfer flags
extern bool recombinations;                  // Include recombination emission
extern double T_floor_rec;                   // Apply a recombination temperature floor: T [K]
extern bool collisions;                      // Include collisional excitation
extern bool output_collisions;               // Output collisional excitation data
extern double collisions_limiter;            // Limited by photoheating rate
extern bool spontaneous;                     // Include spontaneous emission
extern bool continuum;                       // Include stellar continuum emission
extern bool unresolved_HII;                  // Include unresolved HII regions
extern bool two_level_atom;                  // Two level atom line transfer
extern bool anisotropic_scattering;          // Turn anisotropic scattering on/off
extern bool resonant_scattering;             // Turn resonant scattering on/off
extern bool doppler_frequency;               // Output frequency in Doppler widths
extern bool recoil;                          // Include recoil with scattering

// Information about the source
extern int n_absorbed;                       // Number of absorbed photons
extern int n_escaped;                        // Number of escaped photons
extern int n_finished;                       // Track finished progress
extern double Ndot_tot;                      // Total emission rate [photons/s]
extern double L_tot;                         // Total luminosity [erg/s]
extern double L_point;                       // Point source luminosity [erg/s]
extern double L_plane_cont;                  // Plane continuum source luminosity [erg/s]
extern double L_sphere_cont;                 // Sphere continuum source luminosity [erg/s]
extern double L_shell_cont;                  // Shell continuum source luminosity [erg/s]
extern double L_shell_line;                  // Shell line source luminosity [erg/s]
extern double L_shock;                       // Shock source luminosity [erg/s]
extern double L_rec;                         // Recombination luminosity [erg/s]
extern double L_col;                         // Collisional excitation luminosity [erg/s]
extern double L_sp;                          // Spontaneous emission luminosity [erg/s]
extern double Lp_col;                        // Doublet collisional excitation luminosity [erg/s]
extern double L_cont;                        // Continuum emission luminosity [erg/s]
extern double M_cont;                        // Total M_cont [absolute magnitude]
extern double L_HII;                         // Unresolved HII regions luminosity [erg/s]
extern double f_esc_HII;                     // Escape fraction from unresolved HII regions
extern string source_model;                  // Spectral source model

// Point source variables
extern bool point_source;                    // Insert photons at a specified point (default)
extern int i_point;                          // Cell index to insert photons
extern double x_point;                       // Point source position [cm]
extern double y_point;
extern double z_point;
extern Vec3 r_point;                         // (x, y, z) [cm]

// Plane source variables
extern bool plane_cont_source;               // Insert photons at a specified plane
extern string plane_direction;               // Plane direction name
extern int plane_type;                       // Plane direction type
extern double F_plane_cont;                  // Plane continuum source flux [erg/s/angstrom]
extern double S_plane_cont;                  // Plane continuum surface flux [erg/s/cm^2/angstrom]
extern double plane_position;                // Plane position [cm]
extern double plane_center_1, plane_center_2; // Plane center positions
extern double plane_radius_1, plane_radius_2; // Plane box/beam radii
extern double plane_center_x_bbox, plane_center_y_bbox, plane_center_z_bbox; // Center [bbox]
extern double plane_radius_x_bbox, plane_radius_y_bbox, plane_radius_z_bbox; // Radius [bbox]
extern double plane_area;                    // Plane surface area [cm^2]
extern bool plane_beam;                      // Use an ellipsoidal beam instead of a rectangle
enum PlaneDirectionTypes { PosX = 0, NegX, PosY, NegY, PosZ, NegZ };

// Sphere source variables
extern bool sphere_cont_source;              // Insert continuum photons within a sphere
extern int i_sphere_cont;                    // Cell index to insert photons
extern double sphere_radius_cont;            // Sphere radius (continuum) [cm]
extern Vec3 sphere_center_cont;              // Sphere center (continuum) (x, y, z) [cm]

// Shell source variables
extern bool shell_cont_source;               // Insert photons from a shell (continuum)
extern bool shell_line_source;               // Insert photons from a shell (line)
extern bool shell_blackbody;                 // Luminosity based on a black body
extern double r_shell_cont;                  // Shell continuum source radius [cm]
extern double r_shell_line;                  // Shell line source radius [cm]
extern double T_shell;                       // Shell source temperature [K]

// Shock source variables
extern bool shock_source;                    // Insert photons from a shock
extern bool shock_blackbody;                 // Luminosity based on a black body
extern double r_shock;                       // Shock source radius [cm]
extern double T_shock;                       // Shock source temperature [K]

// Spatial emission from cells and stars
extern bool avoid_SFR;                       // Ignore cells with SFR > 0
extern double j_exp;                         // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
extern double V_exp;                         // Volume boosting exponent (V_exp < 1 favors smaller cells)
constexpr int n_cdf_cells = 10000;           // Number of entrees in the cdf lookup table (cells)
extern array<int, n_cdf_cells> j_map_cells;  // Lookup table for photon insertion (cells)
extern vector<double> j_cdf_cells;           // Cumulative distribution function for emission
extern vector<double> j_weights_cells;       // Initial photon weights for each cell
extern vector<double> L_int_cells;           // Intrinsic luminosities for each cell
extern vector<double> source_weight_cells;   // Photon weight at emission for each cell
extern vector<double> weight_cells;          // Photon weight at escape for each cell
extern vector<double> f_col_cells;           // Collisional excitation fractions for each cell
extern bool doublet_cell_based_emission;     // Include doublet cell-based sources
extern array<int, n_cdf_cells> jp_map_cells; // Lookup table for doublet photon insertion (cells)
extern vector<double> jp_cdf_cells;          // Cumulative distribution function for doublet emission
extern vector<double> jp_weights_cells;      // Initial doublet photon weights for each cell
extern vector<double> L_int_doublet_cells;   // Intrinsic doublet luminosities for each cell
extern vector<double> source_weight_doublet_cells; // Doublet photon weight at emission for each cell
extern vector<double> weight_doublet_cells;  // Doublet photon weight at escape for each cell
constexpr int n_cdf_stars = 10000;           // Number of entrees in the cdf lookup table (stars)
extern array<int, n_cdf_stars> j_map_stars;  // Lookup table for photon insertion (stars)
extern vector<double> j_cdf_stars;           // Cumulative distribution function for emission
extern vector<double> j_weights_stars;       // Initial photon weights for each star
extern vector<double> L_int_stars;           // Intrinsic luminosities for each star
extern vector<double> Ndot_int_stars;        // Intrinsic photon rates for each star
extern vector<double> source_weight_stars;   // Photon weight at emission for each star
extern vector<double> weight_stars;          // Photon weight at escape for each star
extern double emission_n_min;                // Minimum number density of emitting cells [cm^-3]
extern double emission_n_max;                // Maximum number density of emitting cells [cm^-3]
extern double emission_ne_min;               // Minimum electron density of emitting cells [cm^-3]
extern double emission_ne_max;               // Maximum electron density of emitting cells [cm^-3]

// Distinguish between source types
enum SourceTypes {
  CELLS = 0,                                 // Cell-based emission (recombinations/collisions/spontaneous)
  STARS,                                     // Star-based emission (continuum/unresolved_HII)
  DOUBLET_CELLS,                             // Doublet cell-based emission (collisions)
  POINT,                                     // Point source emission
  PLANE_CONT,                                // Plane source emission (continuum)
  SPHERE_CONT,                               // Sphere source emission (continuum)
  SHELL_CONT,                                // Shell source emission (continuum)
  SHELL_LINE,                                // Shell source emission (line)
  SHOCK,                                     // Shock source emission
  MAX_SOURCE_TYPES                           // Max number of source types
};
extern int n_source_types;                   // Number of source types used
extern array<double, MAX_SOURCE_TYPES> mixed_pdf; // Mixed source probability distribution function
extern array<double, MAX_SOURCE_TYPES> mixed_cdf; // Mixed source cumulative distribution function
extern array<double, MAX_SOURCE_TYPES> mixed_weights; // Mixed source weights (point, cells, stars)
extern vector<int> source_types_mask;        // Source type mask indices
extern strings source_names_mask;            // Source type names (masked)
extern vector<double> mixed_pdf_mask;        // Mixed source pdf (masked)
extern vector<double> mixed_cdf_mask;        // Mixed source cdf (masked)
extern vector<double> mixed_weights_mask;    // Mixed source weights (masked)

// Exit variables
extern bool exit_wrt_com;                    // Exit with respect to the center of mass frame
extern double T_exit;                        // Exit temperature [K]
extern double a_exit;                        // "Damping parameter" [T_exit]
extern double DnuD_exit;                     // Doppler width at T_exit
extern double vth_div_c_exit;                // Thermal velocity (vth/c) [T_exit]
extern double neg_vth_kms_exit;              // Thermal velocity (-km/s) [T_exit]
// extern double neg_a_vth_kms_exit;            // Constant for observable frequency conversion (x -> Δv)
extern bool adjust_camera_frequency;         // Adjust camera frequency based on offsets
extern vector<double> freq_offsets;          // Frequency offsets for each camera

// Information about escaped photons
extern double f_src;                         // Global source fraction: sum(w0)
extern double f2_src;                        // Global squared f_src: sum(w0^2)
extern double n_photons_src;                 // Effective number of emitted photons: 1/<w0>
extern double f_esc;                         // Global escape fraction: sum(w)
extern double f2_esc;                        // Global squared f_esc: sum(w^2)
extern double n_photons_esc;                 // Effective number of escaped photons: 1/<w>
extern double freq_avg;                      // Global average frequency [freq units]
extern double freq_std;                      // Global standard deviation [freq units]
extern double freq_skew;                     // Global frequency skewness [normalized]
extern double freq_kurt;                     // Global frequency kurtosis [normalized]
extern bool output_photons;                  // Output photon packets (esc/abs)
extern bool output_n_scat;                   // Output number of scattering events
extern bool output_path_length;              // Output total path length [cm]
extern bool output_source_position;          // Output position at emission [cm]
extern bool output_stars;                    // Output star emission and escape
extern bool output_cells;                    // Output cell emission and escape
extern string photon_file;                   // Output a separate photon file
// extern bool escaped_frequency_moments;       // Calculate escaped frequency moments
extern vector<int> source_ids;               // Emission cell or star index
extern vector<int> source_types;             // Emission type specifier
extern vector<int> n_scats;                  // Number of scattering events
extern vector<double> source_weights;        // Photon weight at emission
extern vector<double> f_cols;                // Collisional excitation fraction
extern vector<double> freqs;                 // Frequency at escape [code_freq]
extern vector<double> weights;               // Photon weight at escape
extern vector<Vec3> source_positions;        // Position at emission [cm]
extern vector<Vec3> positions;               // Position at escape [cm]
extern vector<Vec3> directions;              // Direction at escape
extern vector<double> path_lengths;          // Total path length [cm]

// Information about secondary cameras (mcrt)
extern bool output_mcrt_emission;            // Output intrinsic emission without transport
extern bool output_mcrt_attenuation;         // Output attenuated emission without scattering
extern vector<double> f_escs_ext;            // Attenuated escape fractions [fraction]
extern vector<double> freq_avgs_int;         // Intrinsic frequency averages [freq units]
extern vector<double> freq_avgs_ext;         // Attenuated frequency averages [freq units]
extern vectors fluxes_int;                   // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
extern vectors fluxes_ext;                   // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
extern Images images_int;                    // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
extern Images images_ext;                    // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
extern Cubes cubes_int;                      // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
extern Cubes cubes_ext;                      // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
extern vectors radial_images_int;            // Intrinsic radial surface brightness images [erg/s/cm^2/arcsec^2]
extern vectors radial_images_ext;            // Attenuated radial surface brightness images [erg/s/cm^2/arcsec^2]
extern Images radial_cubes_int;              // Intrinsic radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
extern Images radial_cubes_ext;              // Attenuated radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about secondary cameras (proj)
extern bool output_proj_emission;            // Output intrinsic emission without transport
extern bool output_proj_attenuation;         // Output attenuated emission without scattering
extern bool output_proj_cube_emission;       // Output intrinsic emission without transport (cubes)
extern bool output_proj_cube_attenuation;    // Output attenuated emission without scattering (cubes)
extern vector<double> proj_f_escs_ext;       // Attenuated escape fractions [fraction]
extern vector<double> proj_cube_f_escs_ext;  // Cube attenuated escape fractions [fraction]
extern Images proj_images_int;               // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
extern Images proj_images_ext;               // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
extern Cubes proj_cubes_int;                 // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
extern Cubes proj_cubes_ext;                 // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Collisional excitation camera data
extern double f_src_col;                     // Global source fraction: sum(w0)
extern double f2_src_col;                    // Global squared f_src_col: sum(w0^2)
extern double n_photons_src_col;             // Effective number of emitted photons: 1/<w0>
extern double f_esc_col;                     // Global escape fraction: sum(w)
extern double f2_esc_col;                    // Global squared f_esc_col: sum(w^2)
extern double n_photons_esc_col;             // Effective number of escaped photons: 1/<w>
extern double freq_avg_col;                  // Global average frequency [freq units]
extern double freq_std_col;                  // Global standard deviation [freq units]
extern double freq_skew_col;                 // Global frequency skewness [normalized]
extern double freq_kurt_col;                 // Global frequency kurtosis [normalized]
extern vector<double> f_escs_col;            // Escape fractions [fraction]
extern vector<double> freq_avgs_col;         // Frequency averages [freq units]
extern vector<double> freq_stds_col;         // Standard deviations [freq units]
extern vector<double> freq_skews_col;        // Frequency skewnesses [normalized]
extern vector<double> freq_kurts_col;        // Frequency kurtoses [normalized]
extern vectors fluxes_col;                   // Spectral fluxes [erg/s/cm^2/angstrom]
extern Images images_col;                    // Surface brightness images [erg/s/cm^2/arcsec^2]
extern Images images2_col;                   // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
extern Images freq_images_col;               // Average frequency images [freq units]
extern Images freq2_images_col;              // Frequency^2 images [freq^2 units]
extern Images freq3_images_col;              // Frequency^3 images [freq^3 units]
extern Images freq4_images_col;              // Frequency^4 images [freq^4 units]
extern vector<double> f_escs_ext_col;        // Attenuated escape fractions [fraction]
extern vector<double> freq_avgs_int_col;     // Intrinsic frequency averages [freq units]
extern vector<double> freq_avgs_ext_col;     // Attenuated frequency averages [freq units]
extern vectors fluxes_int_col;               // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
extern vectors fluxes_ext_col;               // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
extern Images images_int_col;                // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
extern Images images_ext_col;                // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
extern vector<double> proj_f_escs_ext_col;   // Attenuated escape fractions [fraction]
extern vector<double> proj_cube_f_escs_ext_col; // Cube attenuated escape fractions [fraction]
extern Images proj_images_int_col;           // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
extern Images proj_images_ext_col;           // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
extern Cubes proj_cubes_int_col;             // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
extern Cubes proj_cubes_ext_col;             // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

#define have_multiple_grids (output_radial_flow || output_group_flows || output_subhalo_flows)

#include "stats.h"                           // Stats definitions

// Convenience macros
#if use_tau
#define TAU_ON(...) __VA_ARGS__
#else
#define TAU_ON(...)
#endif

#endif // MCRT_PROTO_INCLUDED
