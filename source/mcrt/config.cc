/******************
 * mcrt/config.cc *
 ******************

 * Module configuration for the simulation.

*/

#include "proto.h"
#include "MCRT.h"
#include "../config.h" // Configuration functions
#include "../io_hdf5.h" // HDF5 read/write functions

dust_strings get_dust_tags(); // Dust species trailing tags
vector<Vec3> healpix_vectors(const int nside); // Healpix directions
void set_line_and_dust_parameters_default(); // Line and dust properties (default)
void set_line_and_dust_parameters_derived(); // Line and dust properties (derived)
void setup_bc03_imf_table_Lcont(const string& line); // Tabulated BC03 spectral properties
void setup_bpass_imf135_100_table_Lcont(const string& line); // Tabulated BPASS (135-100) spectral properties
void setup_bpass_chab_100_table_Lcont(const string& line); // Tabulated BPASS (chab_100) spectral properties
void setup_bpass_chab_100_table_Qion(); // Tabulated BPASS (chab_100) spectral properties

/* Image configuration. */
void Simulation::image_config(YAML::Node& file) {
  // Image size parameters
  incompatible("image_radii", "image_radii_bbox", "image_radii_Rvir", "image_widths", "image_widths_cMpc",
               "image_radius", "image_radius_bbox", "image_radius_Rvir", "image_width", "image_width_cMpc");
  if (file["image_radii_bbox"]) {            // Image radii [min distance to bbox edge]
    load("image_radii_bbox", image_radii_bbox, ">=0");
  } else if (file["image_radius_bbox"]) {    // Image radius [min distance to bbox edge]
    load("image_radius_bbox", image_radii_bbox.x, ">=0"); // Inherit default value
    image_radii_bbox.y = image_radii_bbox.x; // Set (x,y) image radii to be the same
  } else if (file["image_radii_Rvir"]) {     // Image radii [selected halo virial radius]
    load("image_radii_Rvir", image_radii_Rvir, ">=0");
  } else if (file["image_radius_Rvir"]) {    // Image radius [selected halo virial radius]
    load("image_radius_Rvir", image_radii_Rvir.x, ">=0"); // Inherit default value
    image_radii_Rvir.y = image_radii_Rvir.x; // Set (x,y) image radii to be the same
  } else if (file["image_radii"]) {          // Image radii [cm] (image_widths / 2)
    load("image_radii", image_radii, ">=0");
  } else if (file["image_radius"]) {         // Image radius [cm] (image_width / 2)
    load("image_radius", image_radii.x, ">=0"); // Inherit default value
    image_radii.y = image_radii.x;           // Set (x,y) image radii to be the same
  } else if (file["image_widths_cMpc"]) {    // Image widths [cMpc]
    load("image_widths_cMpc", image_widths_cMpc, ">=0");
  } else if (file["image_width_cMpc"]) {     // Image width [cMpc]
    load("image_width_cMpc", image_widths_cMpc.x, ">=0"); // Image width [cMpc]
    image_widths_cMpc.y = image_widths_cMpc.x; // Set (x,y) image widths to be the same
  } else if (file["image_widths"]) {         // Image widths [cm]
    load("image_widths", image_widths, ">=0");
  } else {                                   // Image width [cm] (defaults to entire domain)
    load("image_width", image_widths.x, ">=0"); // Inherit default value
    image_widths.y = image_widths.x;         // Set (x,y) image widths to be the same
  }

  // Image resolution parameters
  incompatible("n_pixels", "pixel_arcsec2", "pixel_arcsecs", "pixel_arcsec", "pixel_widths", "pixel_width", "pixel_widths_pc", "pixel_width_pc");
  if (file["pixel_arcsec2"]) {               // Pixel area [arcsec^2]
    load("pixel_arcsec2", pixel_arcsec2, ">0");
    pixel_arcsecs = sqrt(pixel_arcsec2);     // Set pixel arcsecs from arcsec^2
    if (!cosmological)
      root_error("Cannot accept pixel_arcsec2 for non-cosmological simulations in " + config_file);
  } else if (file["pixel_arcsecs"]) {        // Pixel (x,y) widths [arcsec]
    load("pixel_arcsecs", pixel_arcsecs, ">0");
    if (!cosmological)
      root_error("Cannot accept pixel_arcsecs for non-cosmological simulations in " + config_file);
  } else if (file["pixel_arcsec"]) {         // Pixel width [arcsec]
    load("pixel_arcsec", pixel_arcsecs.x, ">0"); // Inherit default value
    pixel_arcsecs.y = pixel_arcsecs.x;       // Set (x,y) pixel arcsecs to be the same
    if (!cosmological)
      root_error("Cannot accept pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["pixel_widths"]) {         // Pixel (x,y) widths [cm]
    load("pixel_widths", pixel_widths, ">0");
  } else if (file["pixel_width"]) {          // Pixel width [cm]
    load("pixel_width", pixel_widths.x, ">0"); // Inherit default value
    pixel_widths.y = pixel_widths.x;         // Set (x,y) pixel widths to be the same
  } else if (file["pixel_widths_pc"]) {      // Pixel (x,y) widths [pc]
    load("pixel_widths_pc", pixel_widths, ">0");
    pixel_widths *= pc;                      // Convert to cm
  } else if (file["pixel_width_pc"]) {       // Pixel width [pc]
    load("pixel_width_pc", pixel_widths.x, ">0"); // Inherit default value
    pixel_widths.y = pixel_widths.x;         // Set (x,y) pixel widths to be the same
    pixel_widths *= pc;                      // Convert to cm
  } else {                                   // Set number of pixels directly
    load("n_pixels", nx_pixels, ">0");       // Inherit default value
    ny_pixels = nx_pixels;                   // Set (x,y) number of pixels to be the same
    load("nx_pixels", nx_pixels, ">0");      // Allow overriding nx_pixels
    load("ny_pixels", ny_pixels, ">0");      // Allow overriding ny_pixels
  }
  load ("monoscopic", monoscopic);           // 360-degree view
  if (monoscopic && nx_pixels != 2 * ny_pixels) {
    cout << "Warning: Monoscopic view requires nx_pixels to be 2 x ny_pixels. "
            "Changing nx_pixels from " << nx_pixels << " to " << 2 * ny_pixels << endl;
    nx_pixels = 2 * ny_pixels;               // Update nx_pixels
  }
}

/* Slit configuration. */
void Simulation::slit_config(YAML::Node& file) {
  // Slit size parameters
  incompatible("slit_radius", "slit_radius_bbox", "slit_radius_Rvir", "slit_width", "slit_width_cMpc");
  if (file["slit_radius_bbox"]) {            // Slit radius [min distance to bbox edge]
    load("slit_radius_bbox", slit_radius_bbox, ">=0");
  } else if (file["slit_radius_Rvir"]) {     // Slit radius [selected halo virial radius]
    load("slit_radius_Rvir", slit_radius_Rvir, ">=0");
  } else if (file["slit_radius"]) {          // Slit radius [cm] (slit_width / 2)
    load("slit_radius", slit_radius, ">=0");
  } else if (file["slit_width_cMpc"]) {      // Slit width [cMpc]
    load("slit_width_cMpc", slit_width_cMpc, ">=0");
  } else {                                   // Slit width [cm] (defaults to entire domain)
    load("slit_width", slit_width, ">=0");   // Inherit default value
  }

  // Slit aperture parameters
  incompatible("slit_aperture", "slit_aperture_arcsec");
  if (file["slit_aperture_arcsec"]) {        // Slit aperture [arcsec]
    load("slit_aperture_arcsec", slit_aperture_arcsec, ">0");
    if (!cosmological)
      root_error("Cannot accept slit_aperture_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["slit_aperture"]) {        // Slit aperture [cm]
    load("slit_aperture", slit_aperture, ">0");
  }

  // Slit resolution parameters
  n_slit_pixels = nx_pixels;                 // Inherit default slit pixel resolution
  incompatible("n_slit_pixels", "slit_pixel_arcsec", "slit_pixel_width", "slit_pixel_width_pc");
  if (file["slit_pixel_arcsec"]) {           // Slit pixel width [arcsec]
    load("slit_pixel_arcsec", slit_pixel_arcsec, ">0");
    if (!cosmological)
      root_error("Cannot accept slit_pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["slit_pixel_width"]) {     // Slit pixel width [cm]
    load("slit_pixel_width", slit_pixel_width, ">0");
  } else if (file["slit_pixel_width_pc"]) {  // Slit pixel width [pc]
    load("slit_pixel_width_pc", slit_pixel_width, ">0");
    slit_pixel_width *= pc;                  // Convert to cm
  } else {                                   // Set number of slit pixels directly
    load("n_slit_pixels", n_slit_pixels, ">0"); // Inherit default value
  }
}

/* Cube configuration. */
void Simulation::cube_config(YAML::Node& file) {
  // Cube size parameters
  incompatible("cube_radii", "cube_radii_bbox", "cube_radii_Rvir", "cube_widths", "cube_widths_cMpc",
                "cube_radius", "cube_radius_bbox", "cube_radius_Rvir", "cube_width", "cube_width_cMpc");
  if (file["cube_radii_bbox"]) {             // Cube radii [min distance to bbox edge]
    load("cube_radii_bbox", cube_radii_bbox, ">=0");
  } else if (file["cube_radius_bbox"]) {     // Cube radius [min distance to bbox edge]
    load("cube_radius_bbox", cube_radii_bbox.x, ">=0"); // Inherit default value
    cube_radii_bbox.y = cube_radii_bbox.x;   // Set (x,y) cube radii to be the same
  } else if (file["cube_radii_Rvir"]) {      // Cube radii [selected halo virial radius]
    load("cube_radii_Rvir", cube_radii_Rvir, ">=0");
  } else if (file["cube_radius_Rvir"]) {     // Cube radius [selected halo virial radius]
    load("cube_radius_Rvir", cube_radii_Rvir.x, ">=0"); // Inherit default value
    cube_radii_Rvir.y = cube_radii_Rvir.x;   // Set (x,y) cube radii to be the same
  } else if (file["cube_radii"]) {           // Cube radii [cm] (cube_widths / 2)
    load("cube_radii", cube_radii, ">=0");
  } else if (file["cube_radius"]) {          // Cube radius [cm] (cube_width / 2)
    load("cube_radius", cube_radii.x, ">=0"); // Inherit default value
    cube_radii.y = cube_radii.x;             // Set (x,y) cube radii to be the same
  } else if (file["cube_widths_cMpc"]) {     // Cube widths [cMpc]
    load("cube_widths_cMpc", cube_widths_cMpc, ">=0");
  } else if (file["cube_width_cMpc"]) {      // Cube width [cMpc]
    load("cube_width_cMpc", cube_widths_cMpc.x, ">=0"); // Cube width [cMpc]
    cube_widths_cMpc.y = cube_widths_cMpc.x; // Set (x,y) cube widths to be the same
  } else if (file["cube_widths"]) {          // Cube widths [cm]
    load("cube_widths", cube_widths, ">=0");
  } else {                                   // Cube width [cm] (defaults to entire domain)
    load("cube_width", cube_widths.x, ">=0"); // Inherit default value
    cube_widths.y = cube_widths.x;           // Set (x,y) cube widths to be the same
  }

  // Cube resolution parameters
  nx_cube_pixels = nx_pixels;                // Inherit default cube pixel resolution
  ny_cube_pixels = ny_pixels;
  incompatible("n_cube_pixels", "cube_pixel_arcsec2", "cube_pixel_arcsecs", "cube_pixel_arcsec", "cube_pixel_widths", "cube_pixel_width", "cube_pixel_widths_pc", "cube_pixel_width_pc");
  if (file["cube_pixel_arcsec2"]) {          // Cube pixel area [arcsec^2]
    load("cube_pixel_arcsec2", cube_pixel_arcsec2, ">0");
    cube_pixel_arcsecs = sqrt(cube_pixel_arcsec2); // Set cube pixel arcsecs from arcsec^2
    if (!cosmological)
      root_error("Cannot accept cube_pixel_arcsec2 for non-cosmological simulations in " + config_file);
  } else if (file["cube_pixel_arcsecs"]) {   // Cube pixel (x,y) widths [arcsec]
    load("cube_pixel_arcsecs", cube_pixel_arcsecs, ">0");
    if (!cosmological)
      root_error("Cannot accept cube_pixel_arcsecs for non-cosmological simulations in " + config_file);
  } else if (file["cube_pixel_arcsec"]) {    // Cube pixel width [arcsec]
    load("cube_pixel_arcsec", cube_pixel_arcsecs.x, ">0"); // Inherit default value
    cube_pixel_arcsecs.y = cube_pixel_arcsecs.x; // Set (x,y) cube pixel arcsecs to be the same
    if (!cosmological)
      root_error("Cannot accept cube_pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["cube_pixel_widths"]) {    // Cube pixel (x,y) widths [cm]
    load("cube_pixel_widths", cube_pixel_widths, ">0");
  } else if (file["cube_pixel_width"]) {     // Cube pixel width [cm]
    load("cube_pixel_width", cube_pixel_widths.x, ">0"); // Inherit default value
    cube_pixel_widths.y = cube_pixel_widths.x; // Set (x,y) cube pixel widths to be the same
  } else if (file["cube_pixel_widths_pc"]) { // Cube pixel (x,y) widths [pc]
    load("cube_pixel_widths_pc", cube_pixel_widths, ">0");
    cube_pixel_widths *= pc;                 // Convert to cm
  } else if (file["cube_pixel_width_pc"]) {  // Cube pixel width [pc]
    load("cube_pixel_width_pc", cube_pixel_widths.x, ">0"); // Inherit default value
    cube_pixel_widths.y = cube_pixel_widths.x; // Set (x,y) cube pixel widths to be the same
    cube_pixel_widths *= pc;                 // Convert to cm
  } else {                                   // Set number of cube pixels directly
    load("n_cube_pixels", nx_cube_pixels, ">0"); // Inherit default value
    ny_cube_pixels = nx_cube_pixels;         // Set (x,y) number of pixels to be the same
    load("nx_cube_pixels", nx_cube_pixels, ">0"); // Allow overriding nx_cube_pixels
    load("ny_cube_pixels", ny_cube_pixels, ">0"); // Allow overriding ny_cube_pixels
  }
}

/* Radial image configuration. */
void Simulation::radial_image_config(YAML::Node& file) {
  // Radial image size parameters
  incompatible("radial_image_radius_bbox", "radial_image_radius_Rvir", "radial_image_radius");
  if (file["radial_image_radius_bbox"]) {    // Radial image radius [min distance to bbox edge]
    load("radial_image_radius_bbox", radial_image_radius_bbox, ">0");
  } else if (file["radial_image_radius_Rvir"]) { // Radial image radius [selected halo virial radius]
    load("radial_image_radius_Rvir", radial_image_radius_Rvir, ">0");
  } else if (file["radial_image_radius"]) {  // Radial image radius [cm]
    load("radial_image_radius", radial_image_radius, ">0");
  }

  // Radial image resolution parameters
  incompatible("radial_pixel_arcsec", "radial_pixel_width", "radial_pixel_width_pc", "n_radial_pixels");
  if (file["radial_pixel_arcsec"]) {         // Radial pixel width [arcsec]
    load("radial_pixel_arcsec", radial_pixel_arcsec, ">0");
    if (!cosmological)
      root_error("Cannot accept radial_pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["radial_pixel_width"]) {   // Radial pixel width [cm]
    load("radial_pixel_width", radial_pixel_width, ">0");
  } else if (file["radial_pixel_width_pc"]) { // Radial pixel width [pc]
    load("radial_pixel_width_pc", radial_pixel_width, ">0");
    radial_pixel_width *= pc;                // Convert to cm
  } else {                                   // Set n_radial_pixels directly
    load("n_radial_pixels", n_radial_pixels, ">0");
  }
}

/* Radial cube configuration. */
void Simulation::radial_cube_config(YAML::Node& file) {
  // Radial cube size parameters
  incompatible("radial_cube_radius_bbox", "radial_cube_radius_Rvir", "radial_cube_radius");
  if (file["radial_cube_radius_bbox"]) {     // Radial cube radius [min distance to bbox edge]
    load("radial_cube_radius_bbox", radial_cube_radius_bbox, ">0");
  } else if (file["radial_cube_radius_Rvir"]) { // Radial cube radius [selected halo virial radius]
    load("radial_cube_radius_Rvir", radial_cube_radius_Rvir, ">0");
  } else if (file["radial_cube_radius"]) {   // Radial cube radius [cm]
    load("radial_cube_radius", radial_cube_radius, ">0");
  }

  // Radial cube resolution parameters
  incompatible("radial_cube_pixel_arcsec", "radial_cube_pixel_width", "radial_cube_pixel_width_pc", "n_radial_cube_pixels");
  if (file["radial_cube_pixel_arcsec"]) {    // Radial pixel width [arcsec]
    load("radial_cube_pixel_arcsec", radial_cube_pixel_arcsec, ">0");
    if (!cosmological)
      root_error("Cannot accept radial_cube_pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["radial_cube_pixel_width"]) { // Radial cube pixel width [cm]
    load("radial_cube_pixel_width", radial_cube_pixel_width, ">0");
  } else if (file["radial_cube_pixel_width_pc"]) { // Radial cube pixel width [pc]
    load("radial_cube_pixel_width_pc", radial_cube_pixel_width, ">0");
    radial_cube_pixel_width *= pc;           // Convert to cm
  } else {                                   // Set n_radial_cube_pixels directly
    load("n_radial_cube_pixels", n_radial_cube_pixels, ">0");
  }
}

/* Line-of-sight healpix map configuration. */
void Simulation::map_config(YAML::Node& file) {
  incompatible("n_exp_map", "n_side_map");   // Cannot specify both
  if (file["n_exp_map"]) {
    load("n_exp_map", n_side_map, ">=0");    // Healpix exponent for map
    n_side_map = 1 << n_side_map;            // Convert n_exp to n_side
  } else {
    load("n_side_map", n_side_map, ">0");    // Healpix resolution for map
  }
}

/* Group line-of-sight healpix maps configuration. */
void Simulation::group_map_config(YAML::Node& file) {
  incompatible("n_exp_groups", "n_side_groups"); // Cannot specify both
  if (file["n_exp_groups"]) {
    load("n_exp_groups", n_side_grp, ">=0"); // Healpix exponent for group maps
    n_side_grp = 1 << n_side_grp;            // Convert n_exp to n_side
  } else {
    load("n_side_groups", n_side_grp, ">0"); // Healpix resolution for group maps
  }
}

/* Subhalo line-of-sight healpix maps configuration. */
void Simulation::subhalo_map_config(YAML::Node& file) {
  incompatible("n_exp_subhalos", "n_side_subhalos"); // Cannot specify both
  if (file["n_exp_subhalos"]) {
    load("n_exp_subhalos", n_side_sub, ">=0"); // Healpix exponent for subhalo maps
    n_side_sub = 1 << n_side_sub;            // Convert n_exp to n_side
  } else {
    load("n_side_subhalos", n_side_sub, ">0"); // Healpix resolution for subhalo maps
  }
}

/* Line-of-sight radial healpix map configuration. */
void Simulation::radial_map_config(YAML::Node& file) {
  incompatible("n_exp_radial", "n_side_radial"); // Cannot specify both
  if (file["n_exp_radial"]) {
    load("n_exp_radial", n_side_radial, ">=0"); // Healpix exponent for radial map
    n_side_radial = 1 << n_side_radial;      // Convert n_exp to n_side
  } else {
    load("n_side_radial", n_side_radial, ">0"); // Healpix resolution for radial map
  }

  // Radial map size parameters
  incompatible("map_radius_bbox", "map_radius_Rvir", "map_radius_kpc", "map_radius");
  if (file["map_radius_bbox"]) {             // Radial map radius [min distance to bbox edge]
    load("map_radius_bbox", map_radius_bbox, ">0");
  } else if (file["map_radius_Rvir"]) {      // Radial map radius [selected halo virial radius]
    load("map_radius_Rvir", map_radius_Rvir, ">0");
  } else if (file["map_radius_kpc"]) {       // Radial map radius [kpc]
    load("map_radius_kpc", map_radius, ">0");
    map_radius *= kpc;                       // Convert to cm
  } else if (file["map_radius"]) {           // Radial map radius [cm]
    load("map_radius", map_radius, ">0");
  }

  // Radial map resolution parameters
  incompatible("map_pixel_arcsec", "map_pixel_width", "map_pixel_width_pc", "n_map_pixels");
  if (file["map_pixel_arcsec"]) {            // Radial map pixel width [arcsec]
    load("map_pixel_arcsec", map_pixel_arcsec, ">0");
    if (!cosmological)
      root_error("Cannot accept map_pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["map_pixel_width"]) {      // Radial map pixel width [cm]
    load("map_pixel_width", map_pixel_width, ">0");
  } else if (file["map_pixel_width_pc"]) {   // Radial map pixel width [pc]
    load("map_pixel_width_pc", map_pixel_width, ">0");
    map_pixel_width *= pc;                   // Convert to cm
  } else {                                   // Set n_map_pixels directly
    load("n_map_pixels", n_map_pixels, ">0");
  }
}

/* Group line-of-sight radial healpix maps configuration. */
void Simulation::group_radial_map_config(YAML::Node& file) {
  incompatible("n_exp_radial_groups", "n_side_radial_groups"); // Cannot specify both
  if (file["n_exp_radial_groups"]) {
    load("n_exp_radial_groups", n_side_radial_grp, ">=0"); // Healpix exponent for radial group maps
    n_side_radial_grp = 1 << n_side_radial_grp; // Convert n_exp to n_side
  } else {
    load("n_side_radial_groups", n_side_radial_grp, ">0"); // Healpix resolution for radial group maps
  }

  // Radial group maps size parameters
  incompatible("map_radius_bbox_groups", "map_radius_kpc_groups", "map_radius_groups");
  if (file["map_radius_bbox_groups"]) {      // Radial group maps radius [min distance to bbox edge]
    load("map_radius_bbox_groups", map_radius_bbox_grp, ">0");
  } else if (file["map_radius_kpc_groups"]) { // Radial group maps radius [kpc]
    load("map_radius_kpc_groups", map_radius_grp, ">0");
    map_radius_grp *= kpc;                   // Convert to cm
  } else if (file["map_radius_groups"]) {    // Radial group maps radius [cm]
    load("map_radius_groups", map_radius_grp, ">0");
  }

  // Radial group maps resolution parameters
  incompatible("map_pixel_arcsec_groups", "map_pixel_width_groups", "map_pixel_width_pc_groups", "n_map_pixels_groups");
  incompatible("map_pixel_arcsec_groups", "map_pixel_width_groups", "map_pixel_width_pc_groups", "min_map_radius_pc_groups", "min_map_radius_kpc_groups");
  if (file["map_pixel_arcsec_groups"]) {     // Radial pixel width [arcsec]
    load("map_pixel_arcsec_groups", map_pixel_arcsec_grp, ">0");
    if (!cosmological)
      root_error("Cannot accept map_pixel_arcsec_groups for non-cosmological simulations in " + config_file);
  } else if (file["map_pixel_width_groups"]) { // Radial group maps pixel width [cm]
    load("map_pixel_width_groups", map_pixel_width_grp, ">0");
  } else if (file["map_pixel_width_pc_groups"]) { // Radial group maps pixel width [pc]
    load("map_pixel_width_pc_groups", map_pixel_width_grp, ">0");
    map_pixel_width_grp *= pc;               // Convert to cm
  } else {                                   // Set n_map_pixels_grp directly
    load("n_map_pixels_groups", n_map_pixels_grp, ">0");
  }

  // Logarithmic binning for radial group maps (incompatible with pixel width options)
  if (file["min_map_radius_pc_groups"]) {    // Minimum radial distance for group maps [pc]
    load("min_map_radius_pc_groups", min_map_radius_grp, ">=0");
    if (min_map_radius_grp > 0.)
      min_map_radius_grp *= pc;              // Convert to cm
  } else if (file["min_map_radius_kpc_groups"]) { // Minimum radial distance for group maps [kpc]
    load("min_map_radius_kpc_groups", min_map_radius_grp, ">=0");
    if (min_map_radius_grp > 0.)
      min_map_radius_grp *= kpc;             // Convert to cm
  }
  if (min_map_radius_grp > 0.) {
    n_map_pixels_grp++;                      // Add an extra pixel for the minimum radius
    log_min_map_radius_grp = log10(min_map_radius_grp); // Logarithmic binning
  }
}

/* Subhalo line-of-sight radial healpix maps configuration. */
void Simulation::subhalo_radial_map_config(YAML::Node& file) {
  incompatible("n_exp_radial_subhalos", "n_side_radial_subhalos"); // Cannot specify both
  if (file["n_exp_radial_subhalos"]) {
    load("n_exp_radial_subhalos", n_side_radial_sub, ">=0"); // Healpix exponent for radial subhalo maps
    n_side_radial_sub = 1 << n_side_radial_sub; // Convert n_exp to n_side
  } else {
    load("n_side_radial_subhalos", n_side_radial_sub, ">0"); // Healpix resolution for radial subhalo maps
  }

  // Radial subhalo maps size parameters
  incompatible("map_radius_bbox_subhalos", "map_radius_kpc_subhalos", "map_radius_subhalos");
  if (file["map_radius_bbox_subhalos"]) {    // Radial subhalo maps radius [min distance to bbox edge]
    load("map_radius_bbox_subhalos", map_radius_bbox_sub, ">0");
  } else if (file["map_radius_kpc_subhalos"]) { // Radial subhalo maps radius [kpc]
    load("map_radius_kpc_subhalos", map_radius_sub, ">0");
    map_radius_sub *= kpc;                   // Convert to cm
  } else if (file["map_radius_subhalos"]) {  // Radial subhalo maps radius [cm]
    load("map_radius_subhalos", map_radius_sub, ">0");
  }

  // Radial subhalo maps resolution parameters
  incompatible("map_pixel_arcsec_subhalos", "map_pixel_width_subhalos", "map_pixel_width_pc_subhalos", "n_map_pixels_subhalos");
  incompatible("map_pixel_arcsec_subhalos", "map_pixel_width_subhalos", "map_pixel_width_pc_subhalos", "min_map_radius_pc_subhalos", "min_map_radius_kpc_subhalos");
  if (file["map_pixel_arcsec_subhalos"]) {   // Radial subhalo pixel width [arcsec]
    load("map_pixel_arcsec_subhalos", map_pixel_arcsec_sub, ">0");
    if (!cosmological)
      root_error("Cannot accept map_pixel_arcsec_subhalos for non-cosmological simulations in " + config_file);
  } else if (file["map_pixel_width_subhalos"]) { // Radial subhalo maps pixel width [cm]
    load("map_pixel_width_subhalos", map_pixel_width_sub, ">0");
  } else if (file["map_pixel_width_pc_subhalos"]) { // Radial subhalo maps pixel width [pc]
    load("map_pixel_width_pc_subhalos", map_pixel_width_sub, ">0");
    map_pixel_width_sub *= pc;               // Convert to cm
  } else {                                   // Set n_map_pixels_sub directly
    load("n_map_pixels_subhalos", n_map_pixels_sub, ">0");
  }

  // Logarithmic binning for radial subhalo maps (incompatible with pixel width options)
  if (file["min_map_radius_pc_subhalos"]) {  // Minimum radial distance for subhalo maps [pc]
    load("min_map_radius_pc_subhalos", min_map_radius_sub, ">=0");
    if (min_map_radius_sub > 0.)
      min_map_radius_sub *= pc;              // Convert to cm
  } else if (file["min_map_radius_kpc_subhalos"]) { // Minimum radial distance for subhalo maps [kpc]
    load("min_map_radius_kpc_subhalos", min_map_radius_sub, ">=0");
    if (min_map_radius_sub > 0.)
      min_map_radius_sub *= kpc;             // Convert to cm
  }
  if (min_map_radius_sub > 0.) {
    n_map_pixels_sub++;                      // Add an extra pixel for the minimum radius
    log_min_map_radius_sub = log10(min_map_radius_sub); // Logarithmic binning
  }
}

/* Group angle-averaged radial surface brightness configuration. */
void Simulation::group_radial_avg_config(YAML::Node& file) {
  // Radial group avgs size parameters
  incompatible("radius_bbox_groups", "radius_kpc_groups", "radius_groups");
  if (file["radius_bbox_groups"]) {          // Radial group avgs radius [min distance to bbox edge]
    load("radius_bbox_groups", radius_bbox_grp, ">0");
  } else if (file["radius_kpc_groups"]) {    // Radial group avgs radius [kpc]
    load("radius_kpc_groups", radius_grp, ">0");
    radius_grp *= kpc;                       // Convert to cm
  } else if (file["radius_groups"]) {        // Radial group avgs radius [cm]
    load("radius_groups", radius_grp, ">0");
  }

  // Radial group avgs resolution parameters
  incompatible("pixel_arcsec_groups", "pixel_width_groups", "pixel_width_pc_groups", "n_pixels_groups");
  incompatible("pixel_arcsec_groups", "pixel_width_groups", "pixel_width_pc_groups", "min_radius_pc_groups", "min_radius_kpc_groups");
  if (file["pixel_arcsec_groups"]) {         // Radial pixel width [arcsec]
    load("pixel_arcsec_groups", pixel_arcsec_grp, ">0");
    if (!cosmological)
      root_error("Cannot accept pixel_arcsec_groups for non-cosmological simulations in " + config_file);
  } else if (file["pixel_width_groups"]) {   // Radial group avgs pixel width [cm]
    load("pixel_width_groups", pixel_width_grp, ">0");
  } else if (file["pixel_width_pc_groups"]) { // Radial group avgs pixel width [pc]
    load("pixel_width_pc_groups", pixel_width_grp, ">0");
    pixel_width_grp *= pc;                   // Convert to cm
  } else {                                   // Set n_pixels_grp directly
    load("n_pixels_groups", n_pixels_grp, ">0");
  }

  // Logarithmic binning for radial group avgs (incompatible with pixel width options)
  if (file["min_radius_pc_groups"]) {        // Minimum radial distance for group avgs [pc]
    load("min_radius_pc_groups", min_radius_grp, ">=0");
    if (min_radius_grp > 0.)
      min_radius_grp *= pc;                  // Convert to cm
  } else if (file["min_radius_kpc_groups"]) { // Minimum radial distance for group avgs [kpc]
    load("min_radius_kpc_groups", min_radius_grp, ">=0");
    if (min_radius_grp > 0.)
      min_radius_grp *= kpc;                 // Convert to cm
  }
  if (min_radius_grp > 0.) {
    n_pixels_grp++;                          // Add an extra pixel for the minimum radius
    log_min_radius_grp = log10(min_radius_grp); // Logarithmic binning
  }
}

/* Subhalo angle-averaged radial surface brightness configuration. */
void Simulation::subhalo_radial_avg_config(YAML::Node& file) {
  // Radial subhalo avgs size parameters
  incompatible("radius_bbox_subhalos", "radius_kpc_subhalos", "radius_subhalos");
  if (file["radius_bbox_subhalos"]) {        // Radial subhalo avgs radius [min distance to bbox edge]
    load("radius_bbox_subhalos", radius_bbox_sub, ">0");
  } else if (file["radius_kpc_subhalos"]) {  // Radial subhalo avgs radius [kpc]
    load("radius_kpc_subhalos", radius_sub, ">0");
    radius_sub *= kpc;                       // Convert to cm
  } else if (file["radius_subhalos"]) {      // Radial subhalo avgs radius [cm]
    load("radius_subhalos", radius_sub, ">0");
  }

  // Radial subhalo avgs resolution parameters
  incompatible("pixel_arcsec_subhalos", "pixel_width_subhalos", "pixel_width_pc_subhalos", "n_pixels_subhalos");
  incompatible("pixel_arcsec_subhalos", "pixel_width_subhalos", "pixel_width_pc_subhalos", "min_radius_pc_subhalos", "min_radius_kpc_subhalos");
  if (file["pixel_arcsec_subhalos"]) {       // Radial subhalo pixel width [arcsec]
    load("pixel_arcsec_subhalos", pixel_arcsec_sub, ">0");
    if (!cosmological)
      root_error("Cannot accept pixel_arcsec_subhalos for non-cosmological simulations in " + config_file);
  } else if (file["pixel_width_subhalos"]) { // Radial subhalo avgs pixel width [cm]
    load("pixel_width_subhalos", pixel_width_sub, ">0");
  } else if (file["pixel_width_pc_subhalos"]) { // Radial subhalo avgs pixel width [pc]
    load("pixel_width_pc_subhalos", pixel_width_sub, ">0");
    pixel_width_sub *= pc;                   // Convert to cm
  } else {                                   // Set n_pixels_sub directly
    load("n_pixels_subhalos", n_pixels_sub, ">0");
  }

  // Logarithmic binning for radial subhalo avgs (incompatible with pixel width options)
  if (file["min_radius_pc_subhalos"]) {      // Minimum radial distance for subhalo avgs [pc]
    load("min_radius_pc_subhalos", min_radius_sub, ">=0");
    if (min_radius_sub > 0.)
      min_radius_sub *= pc;                  // Convert to cm
  } else if (file["min_radius_kpc_subhalos"]) { // Minimum radial distance for subhalo avgs [kpc]
    load("min_radius_kpc_subhalos", min_radius_sub, ">=0");
    if (min_radius_sub > 0.)
      min_radius_sub *= kpc;                 // Convert to cm
  }
  if (min_radius_sub > 0.) {
    n_pixels_sub++;                          // Add an extra pixel for the minimum radius
    log_min_radius_sub = log10(min_radius_sub); // Logarithmic binning
  }
}

/* Radial spectral map configuration. */
void Simulation::cube_map_config(YAML::Node& file) {
  // Radial spectral map size parameters
  incompatible("cube_map_radius_bbox", "cube_map_radius_Rvir", "cube_map_radius");
  if (file["cube_map_radius_bbox"]) {        // Radial spectral map radius [min distance to bbox edge]
    load("cube_map_radius_bbox", cube_map_radius_bbox, ">0");
  } else if (file["cube_map_radius_Rvir"]) { // Radial spectral map radius [selected halo virial radius]
    load("cube_map_radius_Rvir", cube_map_radius_Rvir, ">0");
  } else if (file["cube_map_radius"]) {      // Radial spectral map radius [cm]
    load("cube_map_radius", cube_map_radius, ">0");
  }

  // Radial spectral map resolution parameters
  incompatible("cube_map_pixel_arcsec", "cube_map_pixel_width", "cube_map_pixel_width_pc", "n_cube_map_pixels");
  if (file["cube_map_pixel_arcsec"]) {       // Radial spectral map pixel width [arcsec]
    load("cube_map_pixel_arcsec", cube_map_pixel_arcsec, ">0");
    if (!cosmological)
      root_error("Cannot accept cube_map_pixel_arcsec for non-cosmological simulations in " + config_file);
  } else if (file["cube_map_pixel_width"]) { // Radial spectral map pixel width [cm]
    load("cube_map_pixel_width", cube_map_pixel_width, ">0");
  } else if (file["cube_map_pixel_width_pc"]) { // Radial spectral map pixel width [pc]
    load("cube_map_pixel_width_pc", cube_map_pixel_width, ">0");
    cube_map_pixel_width *= pc;              // Convert to cm
  } else {                                   // Set n_cube_map_pixels directly
    load("n_cube_map_pixels", n_cube_map_pixels, ">0");
  }
}

/* General camera configuration. */
void Simulation::camera_config(YAML::Node& file) {
  camera_directions.resize(0);               // Clear camera directions
  incompatible("n_exp", "n_side");           // Cannot specify both
  if (file["n_exp"] || file["n_side"]) {
    if (file["n_exp"]) {
      load("n_exp", n_side, ">=0");          // Healpix exponent for cameras
      n_side = 1 << n_side;                  // Convert n_exp to n_side
    } else {
      load("n_side", n_side, ">0");          // Healpix resolution for cameras
    }
    camera_directions = healpix_vectors(n_side); // Populate directions
    have_cameras = true;                     // Camera-based output
  }
  if (file["poly_cameras"]) {                // Polygon-based camera configuration
    string poly_cameras;                     // Platonic solids: tetrahedron, cube,
    load("poly_cameras", poly_cameras);      // octahedron, dodecahedron and icosahedron
    auto& cd = camera_directions;            // Camera directions alias
    if (poly_cameras == "tetrahedron") {     // Regular tetrahedron (4 directions)
      cd.push_back({0., 0., 1.});            // Add the z-axis
      cd.push_back({sqrt(8./9.), 0., -1./3.}); // Other vertices
      cd.push_back({-sqrt(2./9.), sqrt(2./3.), -1./3.});
      cd.push_back({-sqrt(2./9.), -sqrt(2./3.), -1./3.});
    } else if (poly_cameras == "cube") {     // Regular cube (6 directions)
      cd.push_back({0., 0., 1.}); cd.push_back({0., 0., -1.}); // z-axis
      cd.push_back({0., 1., 0.}); cd.push_back({0., -1., 0.}); // y-axis
      cd.push_back({1., 0., 0.}); cd.push_back({-1., 0., 0.}); // x-axis
    } else if (poly_cameras == "octahedron") { // Regular octahedron (8 directions)
      const double s19 = 1./3., s29 = sqrt(2./9.), s69 = sqrt(6./9.), s89 = sqrt(8./9.);
      cd.push_back({0., 0., 1.}); cd.push_back({0., 0., -1.}); // z-axis
      cd.push_back({0., -s89, s19}); cd.push_back({0., s89, -s19}); // Other vertices
      cd.push_back({s69, s29, s19}); cd.push_back({-s69, -s29, -s19});
      cd.push_back({-s69, s29, s19}); cd.push_back({s69, -s29, -s19});
    } else if (poly_cameras == "dodecahedron") { // Regular dodecahedron (12 directions)
      const double C1 = sqrt(1./2. - 1./sqrt(20.)), C2 = sqrt(1./2. + 1./sqrt(20.));
      cd.push_back({ C1,  0.,  C2}); cd.push_back({ C2,  C1,  0.}); cd.push_back({ 0.,  C2,  C1});
      cd.push_back({ 0.,  C2, -C1}); cd.push_back({ C1,  0., -C2}); cd.push_back({-C1,  0.,  C2});
      cd.push_back({ 0., -C2,  C1}); cd.push_back({ C2, -C1,  0.}); cd.push_back({ 0., -C2, -C1});
      cd.push_back({-C2,  C1,  0.}); cd.push_back({-C1,  0., -C2}); cd.push_back({-C2, -C1,  0.});
    } else if (poly_cameras == "icosahedron") { // Regular icosahedron (20 directions)
      auto tri = [](const Vec3& a, const Vec3& b, const Vec3& c) -> Vec3 {
        return cross(b - a, c - a);
      }; // Triangle normal direction (a,b,c)
      const double p = (1. + sqrt(5.)) / 2.; // Golden ratio
      const Vec3 a1 = {0., 1., p}, a2 = {0., 1., -p}, a3 = {0., -1., p}, a4 = {0., -1., -p};
      const Vec3 b1 = {1., p, 0.}, b2 = {1., -p, 0.}, b3 = {-1., p, 0.}, b4 = {-1., -p, 0.};
      const Vec3 c1 = {p, 0., 1.}, c2 = {p, 0., -1.}, c3 = {-p, 0., 1.}, c4 = {-p, 0., -1.};
      cd.push_back(tri(b3, a1, b1)); cd.push_back(tri(b1, a2, b3)); cd.push_back(tri(b1, a1, c1)); cd.push_back(tri(b1, c2, a2));
      cd.push_back(tri(c2, b1, c1)); cd.push_back(tri(b4, b2, a3)); cd.push_back(tri(b4, a4, b2)); cd.push_back(tri(b4, a3, c3));
      cd.push_back(tri(b4, c4, a4)); cd.push_back(tri(c3, c4, b4)); cd.push_back(tri(b3, c3, a1)); cd.push_back(tri(b3, a2, c4));
      cd.push_back(tri(b3, c4, c3)); cd.push_back(tri(b2, c1, a3)); cd.push_back(tri(b2, a4, c2)); cd.push_back(tri(b2, c2, c1));
      cd.push_back(tri(a4, c4, a2)); cd.push_back(tri(a4, a2, c2)); cd.push_back(tri(a1, c3, a3)); cd.push_back(tri(a1, a3, c1));
    } else {
      root_error("Invalid poly_cameras: " + poly_cameras); // Invalid poly_cameras
    }
    have_cameras = true;                     // Camera-based output
  }
  if (file["n_rot"]) {                       // Rotate around an axis
    load("n_rot", n_rot, ">0");              // Number of rotation vectors
    double inclination = 0.;                 // Viewing inclination angle [degrees]
    load("inclination", inclination);
    if (fabs(inclination) > 85.)
      root_error("inclination must be in the range [-85,85]"); // Validate range
    double phi_start = 0., phi_end = 360.;   // Starting and ending angles [degrees]
    load("phi_start", phi_start);
    load("phi_end", phi_end);
    inclination *= M_PI / 180.;              // Convert to radians
    phi_start *= M_PI / 180.;
    phi_end *= M_PI / 180.;
    const double dphi = (phi_end - phi_start) / double(n_rot); // Rotation per camera
    bool rotate_with_snaps = false;          // Offset rotations by the snapshot number
    load("rotate_with_snaps", rotate_with_snaps);
    if (rotate_with_snaps)
      phi_start += double(snap_num) * (phi_end - phi_start);
    if (file["rotation_axis"]) {
      load("rotation_axis", rotation_axis);  // Rotation axis, e.g. (0,0,1)
      rotation_axis.normalize();             // Ensure normalization
      camera_north = rotation_axis;          // Align north for rotation
    }
    double phi = 0.;                         // Azimuthal angle [radians]
    const double theta = M_PI_2 + inclination; // Polar angle [radians]
    const bool rotation_flag = (1. - fabs(rotation_axis.z) > 1e-9);
    for (int i = 0; i < n_rot; ++i) {
      phi = phi_start + double(i) * dphi;
      const double sin_theta = sin(theta);
      Vec3 k_cam = {cos(phi) * sin_theta, sin(phi) * sin_theta, cos(theta)};
      if (rotation_flag) {
        // Rotate from the z-axis to the rotation axis: (0,0,1) -> (kx,ky,kz)
        // (x',y',z') = (x - kx*CZ, y - ky*CZ, z*kz - x*kx - y*ky) where CZ = (x*kx + y*ky) / (1 + kz) - z
        // Note: This is singular only when k = (0,0,-1)
        const double xy = k_cam.x*rotation_axis.x + k_cam.y*rotation_axis.y; // x*kx + y*ky
        const double CZ = xy / (1. + rotation_axis.z) - k_cam.z;
        k_cam = {k_cam.x - rotation_axis.x * CZ, k_cam.y - rotation_axis.y * CZ, k_cam.z * rotation_axis.z - xy};
      }
      camera_directions.push_back(k_cam);    // Save each direction
    }
    have_cameras = true;                     // Camera-based output
  } else if (file["monoscopic"]) {
    // VR 360-degree camera setup
    camera_directions.push_back({1, 0, 0});  // Arbitrary direction
    n_cameras = 1;                           // Single camera
    output_slits = false;                    // Disable camera types that are
    output_cubes = false;                    // incompatible with 360 capture
    output_radial_images = false;
    output_radial_cubes = false;
    have_cameras = true;                     // Camera-based output
  }
  if (file["cameras"]) {
    vector<Vec3> k_cams;                     // List of camera directions
    load("cameras", k_cams);                 // Add camera directions manually
    for (const auto& k_cam : k_cams)
      camera_directions.push_back(k_cam);    // Add camera direction to list
    n_cameras += k_cams.size();              // Track the number of cameras
    have_cameras = true;                     // Camera-based output
  }
  if (file["camera"]) {
    Vec3 camera;
    load("camera", camera);                  // Add camera direction manually
    n_cameras++;                             // Track the number of cameras
    camera_directions.push_back(camera);     // Add camera direction to list
    have_cameras = true;                     // Camera-based output
  }
  if (have_cameras) {
    n_cameras = camera_directions.size();    // Update the number of cameras
    load("camera_center", camera_center);    // Camera target position [cm]
    load("camera_motion", camera_motion);    // Camera target velocity [cm/s]
    load("focus_cameras_on_emission", focus_cameras_on_emission); // Focus cameras on emission (position)
    load("shift_cameras_on_emission", shift_cameras_on_emission); // Shift cameras on emission (velocity)
    load("align_cameras_on_emission", align_cameras_on_emission); // Align cameras on emission (angular momentum)
    if (file["camera_north"]) {
      load("camera_north", camera_north);    // Camera north orientation
      camera_north.normalize();              // Ensure normalization
    }

    // Radial aperture parameters
    incompatible("aperture_radius_bbox", "aperture_radius_Rvir", "aperture_radius");
    if (file["aperture_radius_bbox"]) {      // Aperture radius [min distance to bbox edge]
      load("aperture_radius_bbox", aperture_radius_bbox, ">0");
    } else if (file["aperture_radius_Rvir"]) { // Aperture radius [selected halo virial radius]
      load("aperture_radius_Rvir", aperture_radius_Rvir, ">0");
    } else if (file["aperture_radius"]) {    // Aperture radius [cm]
      load("aperture_radius", aperture_radius, ">0");
    }
  }
}

/* After camera configuration. */
void Simulation::after_camera_config(YAML::Node& file) {
  if (output_images)
    image_config(file);                      // Image configuration
  if (output_slits)
    slit_config(file);                       // Slit configuration
  if (output_cubes)
    cube_config(file);                       // Cube configuration
  if (output_radial_images || output_radial_avg)
    radial_image_config(file);               // Radial image configuration
  if (output_radial_cubes || output_radial_cube_avg)
    radial_cube_config(file);                // Radial cube configuration
}

/* General escape configuration. */
void Simulation::escape_config(YAML::Node& file) {
  runtime("age_escape", age_escape);         // Limit distance by stellar age
  runtime("box_escape", box_escape);         // Restrict ray-tracing to a box region
  runtime("spherical_escape", spherical_escape); // Restrict ray-tracing a spherical region
  runtime("streaming_escape", streaming_escape); // Limit the distance by free-streaming
  #if spherical_escape
    load("escape_center", escape_center);    // Center of the escape region [cm]
    incompatible("escape_radius_bbox", "escape_radius_Rvir", "escape_radius"); // Cannot specify both
    if (file["escape_radius_bbox"]) {
      load("escape_radius_bbox", escape_radius_bbox, ">=0"); // Radius for spherical escape [min distance to bbox edge]
    } else if (file["escape_radius_Rvir"]) {
      load("escape_radius_Rvir", escape_radius_Rvir, ">=0"); // Radius for spherical escape [selected halo virial radius]
    } else {
      load("escape_radius", escape_radius, ">=0"); // Radius for spherical escape [cm]
    }
    incompatible("emission_radius_bbox", "emission_radius_Rvir", "emission_radius"); // Cannot specify both
    if (file["emission_radius_bbox"]) {
      load("emission_radius_bbox", emission_radius_bbox, ">=0"); // Radius for spherical emission [min distance to bbox edge]
    } else if (file["emission_radius_Rvir"]) {
      load("emission_radius_Rvir", emission_radius_Rvir, ">=0"); // Radius for spherical emission [selected halo virial radius]
    } else {
      load("emission_radius", emission_radius, ">=0"); // Radius for spherical emission [cm]
    }
  #endif
  #if box_escape
    // TODO: Implement box escape configuration
  #endif
  #if streaming_escape
    incompatible("max_streaming", "max_streaming_kpc"); // Cannot specify both
    if (file["max_streaming_kpc"]) {
      load("max_streaming_kpc", max_streaming, ">=0"); // Limit the distance by free-streaming
      max_streaming *= kpc;                  // Convert to cm
    } else {
      load("max_streaming", max_streaming, ">=0"); // Limit the distance by free-streaming
    }
  #endif
  #if check_escape
    if (!avoid_edges)
      root_error("avoid_edges = false requires spherical_escape || box_escape || streaming_escape = true"); // Require edge or escape criteria
  #endif
}

/* General gas conditions configuration. */
void Simulation::gas_config(YAML::Node& file) {
  // Density
  load("set_density_from_mass", set_density_from_mass); // Calculate density as mass / volume
  load("read_density_as_mass", read_density_as_mass); // Read density as mass

  // Equation of State (EoS)
  if (file["avoid_SFR"]) {
    load("avoid_SFR", avoid_SFR);            // Ignore cells with SFR > 0
    if (avoid_SFR)
      fields[StarFormationRate].read = true; // Ensure SFR data exists
  }

  // Temperature
  if (file["constant_temperature"]) {
    load("constant_temperature", constant_temperature, ">0"); // Constant temperature: T [K]
  } else
    load("use_internal_energy", use_internal_energy); // Set temperature from the internal energy
  load("T_floor", T_floor, ">=0");           // Apply a temperature floor: T [K]
  load("T_rtol", T_rtol, ">0");              // Relative tolerance for temperature changes

  // Dust
  load("T_sputter", T_sputter, ">=0");       // Thermal sputtering cutoff [K]
  load("f_ion", f_ion, "[0,1]");             // HII region survival fraction
  if (f_ion < 1.) read_HII = true;           // f_ion < 1 requires x_HII
  incompatible("dust_to_metal", "dust_to_gas", "f_dust");
  load("f_dust", f_dust, "[0,1]");           // Fraction of metals locked in dust
  #if graphite_scaled_PAH
    load("f_PAH", f_PAH, "[0,1]");           // Fraction of carbon in PAHs
    fm1_PAH = 1. - f_PAH;                    // Fraction of carbon in graphite
  #endif
  load("dust_to_metal", dust_to_metal, ">=0"); // Constant dust-to-metal ratio
  load("dust_to_gas", dust_to_gas, "[0,1]"); // Constant dust-to-gas ratio
  load("dust_boost", dust_boost, ">0");      // Optional dust boost factor

  // Metallicities
  if (file["hydrogen_fraction"]) {
    load("hydrogen_fraction", hydrogen_fraction, "[0,1]"); // Constant hydrogen mass fraction
    helium_fraction = 1. - hydrogen_fraction; // Update default helium mass fraction
  }
  load("helium_fraction", helium_fraction, "[0,1]"); // Constant helium mass fraction
  load("metallicity", metallicity, "[0,1]"); // Constant metallicity [mass fraction]
  const int Z_offset = CarbonMetallicity - C_ATOM; // Offset for metallicity fields
  for (int atom = 2; atom < n_atoms; ++atom) {
    double& val = fields[atom + Z_offset].constant; // Reference to metallicity field
    load(atom_names[atom] + "_metallicity", val, "[0,1]"); // Constant atom metallicity [mass fraction]
  }
  load("mass_weighted_metallicities", mass_weighted_metallicities); // Adopt mass-weighted values

  // Ionization fractions
  if (file["electron_fraction"]) {
    load("electron_fraction", electron_fraction, ">=0"); // Constant electron fraction: x_e
    read_electron_fraction = true;
  }
  if (file["HI_fraction"]) {
    load("HI_fraction", fields[HI_Fraction].constant, "[0,1]"); // Constant neutral fraction: x_HI
  } else if (file["neutral_fraction"]) {
    load("neutral_fraction", fields[HI_Fraction].constant, "[0,1]"); // Constant neutral fraction: x_HI
  }
  if (file["HII_fraction"]) {
    load("HII_fraction", fields[HII_Fraction].constant, "[0,1]"); // Constant ionized fraction: x_HII
  } else if (file["ionized_fraction"]) {
    load("ionized_fraction", fields[HII_Fraction].constant, "[0,1]"); // Constant ionized fraction: x_HII
  }
  incompatible("H2_fraction", "molecular_fraction"); // Should not specify both
  load("H2_fraction", fields[H2_Fraction].constant, "[0,1]"); // Constant molecular fraction: x_H2
  load("molecular_fraction", fields[H2_Fraction].constant, "[0,1]"); // Constant molecular fraction: x_H2
  if (fields[H2_Fraction].constant >= 0.) {
    read_H2 = true;
  } else {
    incompatible("read_H2", "read_molecular_fraction"); // Should not specify both
    load("read_H2", read_H2);                // Read x_H2 from the input file
    load("read_molecular_fraction", read_H2); // Read x_H2 from the input file
  }
  for (int ion = HeI_Fraction; ion <= FeXVII_Fraction; ++ion) {
    auto& field = fields[ion];               // Ionization fraction field
    string name = field.name.substr(2) + "_fraction"; // Parameter name
    load(name, field.constant, "[0,1]");     // Constant ion fraction: x_ion
  }
}

/* Module configuration for the simulation. */
void MCRT::module_config(YAML::Node& file) {
  // Parallelization strategy
  load("load_balancing", load_balancing);    // Use MPI load balancing algorithms

  // Set the line and dust properties: Opacity, albedo, scattering anisotropy
  load("line", line);                        // Name of the line (default "Lyman-alpha")
  load("recombinations", recombinations);    // Include recombination emission
  load("collisions", collisions);            // Include collisional excitation
  load("continuum", continuum);              // Include continuum emission
  load("unresolved_HII", unresolved_HII);    // Include unresolved HII regions
  load("two_level_atom", two_level_atom);    // Two level atom line transfer
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    const string dust_str = "dust_model" + tags[i];
    if (file[dust_str])
      load(dust_str, dust_models[i]);        // Dust model: SMC, MW, etc.
  }
  set_line_and_dust_parameters_default();    // Set default line/dust properties
  if (two_level_atom)
    load("spontaneous", spontaneous);        // Include spontaneous emission
  if (anisotropic_scattering)
    load("anisotropic_scattering", anisotropic_scattering); // Turn anisotropic scattering on/off
  if (resonant_scattering)
    load("resonant_scattering", resonant_scattering); // Turn resonant scattering on/off

  // Information about the dust model (allow overriding default model)
  incompatible("kappa_dust", "sigma_dust");  // Cannot specify both kappa_dust and sigma_dust
  if (dust_models[0] == "LAURSEN_SMC") {         // LAURSEN_SMC is a special case
    if (file["kappa_dust"])
      root_error("Cannot specify kappa_dust and dust_model = LAURSEN_SMC");
    if (file["sigma_dust"])
      root_error("Cannot specify sigma_dust and dust_model = LAURSEN_SMC");
  }
  load("kappa_dust", kappa_dust, ">=0");     // Dust opacity [cm^2/g dust]
  load("sigma_dust", sigma_dust, ">=0");     // Dust cross section [cm^2/Z/hydrogen atom]
  load("albedo", albedo, "[0,1]");           // Dust albedo for scattering vs. absorption
  load("g_dust", g_dust, "[-1,1]");          // Anisotropy parameter: <μ> for dust scattering
  if (fabs(g_dust) < 1e-6)
    g_dust = (g_dust < 0.) ? -1e-6 : 1e-6;   // Avoid division by zero
  load("p_dest", p_dest, "[0,1]");           // Line destruction probability
  gas_config(file);                          // General gas configuration
  set_line_and_dust_parameters_derived();    // Set derived properties after override

  // Microturbulence
  incompatible("T_turb", "v_turb", "v_turb_kms"); // Cannot specify both T_turb and v_turb[_kms]
  if (file["T_turb"]) {
    load("T_turb", T_turb, ">0");            // Microturbulent temperature [K]
    v_turb = vth_div_sqrtT * sqrt(T_turb);   // Set turbulent velocity [cm/s]
  } else if (file["v_turb_kms"]) {
    double v_turb_kms = 0.;                  // Microturbulent velocity [km/s]
    load("v_turb_kms", v_turb_kms, ">=0");
    v_turb = v_turb_kms * km;                // Convert to [cm/s]
    const double sqrt_T_turb = v_turb / vth_div_sqrtT;
    T_turb = sqrt_T_turb * sqrt_T_turb;      // Set turbulent temperature [K]
  } else {
    load("v_turb", v_turb, ">=0");           // Microturbulent velocity [cm/s]
    const double sqrt_T_turb = v_turb / vth_div_sqrtT;
    T_turb = sqrt_T_turb * sqrt_T_turb;      // Set turbulent temperature [K]
  }
  load("scaled_microturb", scaled_microturb); // Density scaling flag

  // Configurable line transfer flags
  load("T_floor_rec", T_floor_rec, ">=0");   // Apply a recombination temperature floor: T [K]
  load("output_collisions", output_collisions); // Output collisional excitation data
  if (output_collisions && !collisions)
    root_error("Requested output_collisions but collisions is false!");
  if (collisions && file["collisions_limiter"]) {
    load("collisions_limiter", collisions_limiter, ">=1"); // Limited by photoheating rate
    fields[PhotoheatingRate].read = true;    // Read photoheating rates from file
  }
  if (resonant_scattering) {
    runtime("dynamical_core_skipping", dynamical_core_skipping); // Dynamical core-skipping
    if constexpr (dynamical_core_skipping) {
      incompatible("n_exp_atau", "n_side_atau"); // Cannot specify both
      if (file["n_exp_atau"]) {
        load("n_exp_atau", n_side_atau, ">=0"); // Healpix exponent for a*tau0
        n_side_atau = 1 << n_side_atau;      // Convert n_exp to n_side
      } else {
        load("n_side_atau", n_side_atau, ">0"); // Healpix resolution for a*tau0
      }
    }
    load("recoil", recoil);                  // Include recoil with scattering
  }
  load("doppler_frequency", doppler_frequency); // Output frequency in Doppler widths
  if (doppler_frequency && cosmological)
    root_error("Doppler frequency units should not be used for cosmological simulations.");

  // Internal simulation flags
  if (recombinations || collisions)
    read_electron_fraction = true;           // Requires electrons
  if (recombinations || collisions || spontaneous)
    cell_based_emission = true;              // Include cell-based sources
  if (continuum || unresolved_HII)
    star_based_emission = true;              // Include star-based sources
  if (cell_based_emission || star_based_emission)
    load("j_exp", j_exp, "(0,1]");           // Luminosity boosting exponent
  if (cell_based_emission) {
    load("V_exp", V_exp);                    // Volume boosting exponent
    if (E0p > 0. && collisions)
      doublet_cell_based_emission = true;    // Doublet line emission
    load("emission_n_min", emission_n_min, ">=0"); // Minimum number density of emitting cells [cm^-3]
    load("emission_n_max", emission_n_max);  // Maximum number density of emitting cells [cm^-3]
    if (emission_n_min >= emission_n_max)
      root_error("emission_n_min must be smaller than emission_n_max")
    load("emission_ne_min", emission_ne_min, ">=0"); // Minimum electron density of emitting cells [cm^-3]
    load("emission_ne_max", emission_ne_max); // Maximum electron density of emitting cells [cm^-3]
    if (emission_ne_min >= emission_ne_max)
      root_error("emission_ne_min must be smaller than emission_ne_max")
  }
  if (star_based_emission) {
    load("source_model", source_model);      // Model for the sources
    if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BC03-IMF" || source_model == "BPASS-CHAB-100") {
      read_m_init_star = true;               // Requires initial stellar masses
      read_Z_star = true;                    // Requires stellar metallicities
      read_age_star = true;                  // Requires stellar ages
    } else if (source_model != "")
      root_error("Unrecognized value for source_model: " + source_model);
  }
  if (continuum) {
    if (source_model == "")
      read_continuum = true;                 // User provided continuum fluxes
    if (source_model == "BC03-IMF")
      setup_bc03_imf_table_Lcont(line);      // Tabulated BC03 spectral properties
    else if (source_model == "BPASS-IMF-135-100")
      setup_bpass_imf135_100_table_Lcont(line); // Tabulated BPASS (135-100) spectral properties
    else if (source_model == "BPASS-CHAB-100")
      setup_bpass_chab_100_table_Lcont(line); // Tabulated BPASS (chab_100) spectral properties
  }
  if (unresolved_HII) {
    load("f_esc_HII", f_esc_HII);            // Escape fraction from unresolved HII regions
    if (source_model == "BPASS-CHAB-100")
      setup_bpass_chab_100_table_Qion();     // Tabulated BPASS (chab_100) spectral properties
    else
      root_error("Unresolved HII regions are not implemented for " + source_model);
  }

  // Point source variables
  if (file["x_point"]) {
    load("x_point", x_point);                // Point source x position [cm]
    r_point.x = x_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["y_point"]) {
    load("y_point", y_point);                // Point source y position [cm]
    r_point.y = y_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["z_point"]) {
    load("z_point", z_point);                // Point source z position [cm]
    r_point.z = z_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["point"]) {
    load("point", r_point);                  // Specified as sequence
    x_point = r_point.x;                     // Update (x,y,z) for consistency
    y_point = r_point.y;
    z_point = r_point.z;
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (point_source) {
    L_point = 1.;                            // Change the default from zero
    load("L_point", L_point, ">0");          // Point source luminosity [erg/s]
    L_tot += L_point;                        // Add to total luminosity
  }

  // Plane source variables
  if (file["plane_direction"]) {
    load("plane_direction", plane_direction); // Plane source direction
    if (plane_direction == "+x")
      plane_type = PosX;
    else if (plane_direction == "-x")
      plane_type = NegX;
    else if (plane_direction == "+y")
      plane_type = PosY;
    else if (plane_direction == "-y")
      plane_type = NegY;
    else if (plane_direction == "+z")
      plane_type = PosZ;
    else if (plane_direction == "-z")
      plane_type = NegZ;
    else
      root_error("plane_direction must be {+x, -x, +y, -y, +z, -z} but got " + plane_direction);
    if (file["S_plane"]) {
      load("S_plane_cont", S_plane_cont, ">0"); // Plane continuum surface flux [erg/s/cm^2/angstrom]
      L_plane_cont = S_plane_cont;           // Fill L_plane_cont and multiply by area and waveband later
    } else if (file["F_plane_cont"]) {
      load("F_plane_cont", F_plane_cont, ">0"); // Plane continuum source flux [erg/s/angstrom]
    } else {
      L_plane_cont = 1.;                     // Change the default from zero
      load("L_plane_cont", L_plane_cont, ">0"); // Plane continuum source luminosity [erg/s]
    }
    load("plane_beam", plane_beam);          // Use an ellipsoidal beam instead of a rectangle
    if (plane_beam) {
      load("plane_center_x_bbox", plane_center_x_bbox, "(0,1)"); // Center [bbox]
      load("plane_center_y_bbox", plane_center_y_bbox, "(0,1)");
      load("plane_center_z_bbox", plane_center_z_bbox, "(0,1)");
      load("plane_radius_x_bbox", plane_radius_x_bbox, ">0"); // Radius [bbox]
      load("plane_radius_y_bbox", plane_radius_y_bbox, ">0");
      load("plane_radius_z_bbox", plane_radius_z_bbox, ">0");
      if (plane_center_x_bbox < plane_radius_x_bbox || plane_center_x_bbox + plane_radius_x_bbox > 1.)
        root_error("plane_radius_x_bbox must be < plane_center_x_bbox and < 1 - plane_center_x_bbox");
      if (plane_center_y_bbox < plane_radius_y_bbox || plane_center_y_bbox + plane_radius_y_bbox > 1.)
        root_error("plane_radius_y_bbox must be < plane_center_y_bbox and < 1 - plane_center_y_bbox");
      if (plane_center_z_bbox < plane_radius_z_bbox || plane_center_z_bbox + plane_radius_z_bbox > 1.)
        root_error("plane_radius_z_bbox must be < plane_center_z_bbox and < 1 - plane_center_z_bbox");
    }
    plane_cont_source = true;                // Update plane continuum source flag
  }

  // Sphere source variables
  if (file["sphere_radius_cont"]) {
    load("sphere_center_cont", sphere_center_cont); // Sphere center position (continuum) [cm]
    load("sphere_radius_cont", sphere_radius_cont); // Sphere radius (continuum) [cm]
    sphere_cont_source = true;               // Update sphere continuum source flag
    L_sphere_cont = 1.;                      // Change the default from zero
    load("L_sphere_cont", L_sphere_cont, ">0"); // Sphere source luminosity (continuum) [erg/s]
    L_tot += L_sphere_cont;                  // Add to total luminosity
  }

  // Shell source variables
  if (file["r_shell_cont"]) {
    load("r_shell_cont", r_shell_cont);      // Shell radius (continuum) [cm]
    shell_cont_source = true;                // Update shell continuum source flag
    L_shell_cont = 1.;                       // Change the default from zero
    load("shell_blackbody", shell_blackbody); // Set the luminosity based on a black body
    if (shell_blackbody) {
      load("T_shell", T_shell, ">0");        // Shell black body temperature [K]
    } else {
      load("L_shell_cont", L_shell_cont, ">0"); // Shell continuum source luminosity [erg/s]
    }
  }
  if (file["r_shell_line"]) {
    load("r_shell_line", r_shell_line);      // Shell radius (line) [cm]
    shell_line_source = true;                // Update shell line source flag
    L_shell_line = 1.;                       // Change the default from zero
    load("L_shell_line", L_shell_line, ">0"); // Shell line source luminosity [erg/s]
  }

  // Shock source variables
  if (file["r_shock"]) {
    load("r_shock", r_shock);                // Shock radius [cm]
    shock_source = true;                     // Update shock source flag
    L_shock = 1.;                            // Change the default from zero
    load("shock_blackbody", shock_blackbody); // Set the luminosity based on a black body
    if (shock_blackbody) {
      load("T_shock", T_shock, ">0");        // Shock black body temperature [K]
    } else {
      load("L_shock", L_shock, ">0");        // Shock source luminosity [erg/s]
    }
  }

  if (plane_cont_source || sphere_cont_source || shell_cont_source || shock_source) {
    if (continuum)                           // Avoid strange configurations
      root_error("Stellar continuum sources should not be used with plane/sphere/shell/shock sources.")
    continuum = true;                        // Continuum emission source
  }

  // Escape variables
  escape_config(file);                       // General escape setup
  load("output_photons", output_photons);    // Output escaped photon packets
  load("output_stars", output_stars);        // Output star emission and escape
  load("output_cells", output_cells);        // Output cell emission and escape
  if (output_stars && !star_based_emission)
    root_error("output_stars requires star_based_emission");
  if (output_cells && !cell_based_emission)
    root_error("output_cells requires cell_based_emission");
  if (output_photons) {
    load("photon_file", photon_file);        // Output a separate photon file
    load("output_n_scat", output_n_scat);    // Output number of scattering events
    load("output_path_length", output_path_length); // Output path length [cm]
    load("output_source_position", output_source_position); // Output position at emission [cm]
  }
  // if (output_photons)
  //   load("escaped_frequency_moments", escaped_frequency_moments); // Calculate escaped frequency moments
  if constexpr (!SPHERICAL) {
    load("exit_wrt_com", exit_wrt_com);      // Exit with respect to the center of mass frame
    if (!exit_wrt_com)
      load("v_exit", camera_motion);         // Exit velocity [cm/s]
  }
  load("T_exit", T_exit, ">0");              // Exit temperature [K]
  // Check runtime consistency against compile-time parameters
  runtime("output_radial_flow", output_radial_flow); // Radial flow [photons/s]
  runtime("output_group_flows", output_group_flows); // Group flows [photons/s]
  runtime("output_subhalo_flows", output_subhalo_flows); // Subhalo flows [photons/s]
  runtime("output_groups", output_groups);   // Output group emission and escape
  runtime("output_subhalos", output_subhalos); // Output subhalo emission and escape

  if constexpr (output_radial_flow) {
    YAML::Node subfile; load("radial_flow", subfile); // Load subfile
    load_radial_edges(subfile, radial_line_flow); // Radial bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized radial_flow options:\n" << subfile << endl;
      root_error("Unrecognized radial_flow options!");
    }
  }
  if constexpr (output_group_flows) {
    group_line_flows = vector<RadialLineFlow>(n_groups); // Group flows
    YAML::Node subfile; load("group_flows", subfile); // Load subfile
    load_radial_edges(subfile, group_line_flows, true); // Group bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized group_flows options:\n" << subfile << endl;
      root_error("Unrecognized group_flows options!");
    }
  }
  if constexpr (output_subhalo_flows) {
    subhalo_line_flows = vector<RadialLineFlow>(n_subhalos); // Subhalo flows
    YAML::Node subfile; load("subhalo_flows", subfile); // Load subfile
    load_radial_edges(subfile, subhalo_line_flows, false); // Subhalo bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized subhalo_flows options:\n" << subfile << endl;
      root_error("Unrecognized subhalo_flows options!");
    }
  }

  // Information about the simulation
  load("n_photons", n_photons, ">0");        // Number of photon packets
  if (file["n_photons_per_star"]) {
    load("n_photons_per_star", n_photons_per_star, ">0"); // Number of photon packets
    load("n_photons_max", n_photons_max);    // Maximum number of photons
    if (n_photons_max < n_photons)
      root_error("n_photons_max must be >= n_photons"); // Validate range
  }

  // Angle-averaged parameters
  load("output_flux_avg", output_flux_avg);  // Output angle-averaged flux
  load("output_radial_avg", output_radial_avg); // Output angle-averaged radial surface brightness
  load("output_radial_cube_avg", output_radial_cube_avg);  // Output angle-averaged radial spectral data cube

  // Frequency parameters
  load("n_bins", n_bins, ">0");              // Number of frequency bins
  if (file["freq_range"]) {
    vector<double> freq_lims = {freq_min, freq_max};
    load("freq_range", freq_lims);           // Generic frequency extrema [freq units]
    freq_min = freq_lims[0];
    freq_max = freq_lims[1];
  } else {
    load("freq_min", freq_min);              // Generic frequency extrema [freq units]
    load("freq_max", freq_max);
  }
  if (freq_min >= freq_max)
    root_error("freq_min must be < freq_max"); // Validate freq_min range
  if (continuum) {
    vector<double> c_lims = {Dv_cont_min, Dv_cont_max};
    load("continuum_range", c_lims);         // Continuum velocity offset range [km/s]
    Dv_cont_min = c_lims[0];                 // Continuum velocity offset lower limit [km/s]
    Dv_cont_max = c_lims[1];                 // Continuum velocity offset upper limit [km/s]
    if (Dv_cont_min >= Dv_cont_max)
      root_error("Dv_cont_min must be < Dv_cont_max"); // Validate Dv_cont_min range
  }

  // Information about the cameras
  camera_config(file);                       // General camera setup
  if (have_cameras) {
    load("output_freq_kurts", output_freq_kurts); // Output kurtosis values
    // Output skewness values (Note: kurt requires skew)
    load_if_false(output_freq_kurts, "output_freq_skews", output_freq_skews);
    // Output standard deviation values (Note: skew requires std)
    load_if_false(output_freq_skews, "output_freq_stds", output_freq_stds);
    // Output average frequency values (Note: std requires avg)
    load_if_false(output_freq_stds, "output_freq_avgs", output_freq_avgs);
    // Output camera escape fractions (Note: freq_avg requires f_esc)
    load_if_false(output_freq_avgs, "output_escape_fractions", output_escape_fractions);
    load("output_fluxes", output_fluxes);    // Output spectral fluxes
    load("output_images2", output_images2);  // Output statistical moment images
    load("output_rgb_images", output_rgb_images); // Output flux-weighted frequency RGB images
    load("output_freq4_images", output_freq4_images); // Output frequency^4 images
    // Output frequency^3 images (Note: freq4 requires freq3)
    load_if_false(output_freq4_images, "output_freq3_images", output_freq3_images);
    // Output frequency^2 images (Note: freq3 requires freq2)
    load_if_false(output_freq3_images, "output_freq2_images", output_freq2_images);
    // Output average frequency images (Note: freq2 requires freq)
    load_if_false(output_freq2_images, "output_freq_images", output_freq_images);
    // Output surface brightness images (Note: images2, rgb, and freq require images)
    load_if_false(output_images2 || output_rgb_images || output_freq_images, "output_images", output_images);
    load("output_slits", output_slits);      // Output spectral slits
    load("output_cubes", output_cubes);      // Output spectral data cubes
    load("output_radial_images2", output_radial_images2); // Output statistical moment radial images
    load("output_rgb_radial_images", output_rgb_radial_images); // Output flux-weighted frequency RGB radial images
    load("output_freq4_radial_images", output_freq4_radial_images); // Output frequency^4 radial images
    // Output frequency^3 radial images (Note: freq4 requires freq3)
    load_if_false(output_freq4_radial_images, "output_freq3_radial_images", output_freq3_radial_images);
    // Output frequency^2 radial images (Note: freq3 requires freq2)
    load_if_false(output_freq3_radial_images, "output_freq2_radial_images", output_freq2_radial_images);
    // Output average frequency radial images (Note: freq2 requires freq)
    load_if_false(output_freq2_radial_images, "output_freq_radial_images", output_freq_radial_images);
    // Output radial surface brightness images (Note: images2, rgb, and freq require images)
    load_if_false(output_radial_images2 || output_rgb_radial_images || output_freq_radial_images, "output_radial_images", output_radial_images);
    load("output_radial_cubes", output_radial_cubes); // Output radial spectral data cubes
    load("output_mcrt_emission", output_mcrt_emission); // Output intrinsic emission without transport (mcrt)
    load("output_mcrt_attenuation", output_mcrt_attenuation); // Output attenuated emission without scattering (mcrt)
    load("output_proj_emission", output_proj_emission); // Output intrinsic emission without transport (proj)
    load("output_proj_attenuation", output_proj_attenuation); // Output attenuated emission without scattering (proj)
    load("output_proj_cube_emission", output_proj_cube_emission); // Output intrinsic emission without transport (proj cube)
    load("output_proj_cube_attenuation", output_proj_cube_attenuation); // Output attenuated emission without scattering (proj cube)
    if (output_proj_emission || output_proj_attenuation || output_proj_cube_emission || output_proj_cube_attenuation) {
      projection_config(file);               // General projection setup
      save_line_emissivity = true;           // Save emissivity data for projections
    }
    // Check camera output compatibility
    if (output_mcrt_emission || output_mcrt_attenuation) {
      if (output_freq_avgs || output_fluxes || output_images || output_cubes || output_radial_images || output_radial_cubes)
        root_error("output_mcrt_emission and output_mcrt_attenuation require output_freq_avgs, output_fluxes, output_images, output_bin_images, output_radial_images, or output_bin_radial_images");
    }
    if (!(output_escape_fractions || output_fluxes || output_images || output_slits || output_cubes || output_radial_images || output_radial_cubes))
      root_error("Cameras were requested without any camera output types.");
    have_radial_cameras = (output_radial_images || output_radial_cubes);
    after_camera_config(file);               // After camera setup

    // Slit frequency parameters
    if (output_slits) {
      n_slit_bins = n_bins;                  // Reset default number of frequency bins
      slit_freq_min = freq_min;              // Reset default frequency extrema
      slit_freq_max = freq_max;
      load("n_slit_bins", n_slit_bins, ">0"); // Number of frequency bins for slits
      if (file["slit_freq_range"]) {
        vector<double> slit_freq_lims = {slit_freq_min, slit_freq_max};
        load("slit_freq_range", slit_freq_lims); // Generic frequency extrema [freq units]
        slit_freq_min = slit_freq_lims[0];
        slit_freq_max = slit_freq_lims[1];
      } else {
        load("slit_freq_min", slit_freq_min); // Generic frequency extrema [freq units]
        load("slit_freq_max", slit_freq_max);
      }
      if (slit_freq_min >= slit_freq_max)
        root_error("slit_freq_min must be < slit_freq_max"); // Validate range
    }

    // Cube frequency parameters
    if (output_cubes || output_proj_cube_emission || output_proj_cube_attenuation) {
      n_cube_bins = n_bins;                  // Reset default number of frequency bins
      cube_freq_min = freq_min;              // Reset default frequency extrema
      cube_freq_max = freq_max;
      load("n_cube_bins", n_cube_bins, ">0"); // Number of frequency bins for cubes
      if (file["cube_freq_range"]) {
        vector<double> cube_freq_lims = {cube_freq_min, cube_freq_max};
        load("cube_freq_range", cube_freq_lims); // Generic frequency extrema [freq units]
        cube_freq_min = cube_freq_lims[0];
        cube_freq_max = cube_freq_lims[1];
      } else {
        load("cube_freq_min", cube_freq_min); // Generic frequency extrema [freq units]
        load("cube_freq_max", cube_freq_max);
      }
      if (cube_freq_min >= cube_freq_max)
        root_error("cube_freq_min must be < cube_freq_max"); // Validate range
    }

    // Radial cube frequency parameters
    if (output_radial_cubes) {
      n_radial_cube_bins = n_bins;           // Reset default number of frequency bins
      radial_cube_freq_min = freq_min;       // Reset default frequency extrema
      radial_cube_freq_max = freq_max;
      load("n_radial_cube_bins", n_radial_cube_bins, ">0"); // Number of frequency bins for radial cubes
      if (file["radial_cube_freq_range"]) {
        vector<double> radial_cube_freq_lims = {radial_cube_freq_min, radial_cube_freq_max};
        load("radial_cube_freq_range", radial_cube_freq_lims); // Generic frequency extrema [freq units]
        radial_cube_freq_min = radial_cube_freq_lims[0];
        radial_cube_freq_max = radial_cube_freq_lims[1];
      } else {
        load("radial_cube_freq_min", radial_cube_freq_min); // Generic frequency extrema [freq units]
        load("radial_cube_freq_max", radial_cube_freq_max);
      }
      if (radial_cube_freq_min >= radial_cube_freq_max)
        root_error("radial_cube_freq_min must be < radial_cube_freq_max"); // Validate range
    }

    // Camera frequency offsets
    if (file["adjust_camera_frequency"])     // Apply camera frequency offsets
      load("adjust_camera_frequency", adjust_camera_frequency);
    if (adjust_camera_frequency) {
      string freq_offset_file = "";          // Frequency offset file
      if (!init_base.empty()) {
        if (!init_dir.empty()) {
          string freq_offset_dir = init_dir; // Set default from init_dir
          load("freq_offset_dir", freq_offset_dir);
          freq_offset_file = freq_offset_dir + "/"; // Prepend directory info
        }
        string freq_offset_base = init_base; // Set default from init_base
        load("freq_offset_base", freq_offset_base);
        string freq_offset_ext = init_ext;   // Set default from init_ext
        load("freq_offset_ext", freq_offset_ext);
        freq_offset_file += freq_offset_base + snap_str + halo_str + "." + freq_offset_ext;
      } else {
        freq_offset_file = init_file;        // Set default from init_file
        load("freq_offset_file", freq_offset_file);
      }
      // Read the frequency offset data from the hdf5 file
      if (FILE *file = fopen(freq_offset_file.c_str(), "r"))
        fclose(file);                        // First check if the file exists
      else
        root_error("adjust_camera_frequency = true but " + freq_offset_file +
          " is not a valid file. [See freq_offset_{dir,base,file}]");
      H5File f(freq_offset_file, H5F_ACC_RDONLY);
      if (!exists(f, "freq_avgs"))
        root_error("adjust_camera_frequency = true requires a valid file with freq_avgs data.");
      const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
      const int n_cams = read(f, "freq_avgs", freq_offsets, freq_units);
      if (n_cams != n_cameras)
        root_error("Requested " + to_string(n_cameras) + " cameras but read " +
              to_string(n_cams) + " frequency offsets. [adjust_camera_frequency]");
    }
  } else {
    output_escape_fractions = output_fluxes = output_images = false; // Reset defaults
  }

  // Line-of-sight healpix map configuration
  load("output_map2", output_map2);          // Output statistical moment map
  load("output_rgb_map", output_rgb_map);    // Output flux-weighted frequency RGB map
  load("output_freq4_map", output_freq4_map); // Output frequency^4 map
  // Output frequency^3 map (Note: freq4 requires freq3)
  load_if_false(output_freq4_map, "output_freq3_map", output_freq3_map);
  // Output frequency^2 map (Note: freq3 requires freq2)
  load_if_false(output_freq3_map, "output_freq2_map", output_freq2_map);
  // Output average frequency map (Note: freq2 requires freq)
  load_if_false(output_freq2_map, "output_freq_map", output_freq_map);
  // Output line-of-sight healpix map (Note: map2, rgb, and freq require map)
  load_if_false(output_map2 || output_rgb_map || output_freq_map, "output_map", output_map);
  load("output_flux_map", output_flux_map);  // Output spectral flux map
  load("output_radial_map", output_radial_map); // Output radial surface brightness map
  load("output_cube_map", output_cube_map);  // Output radial spectral map

  // Specific healpix map parameters
  if (output_map)
    map_config(file);                        // Healpix map configuration
  if (output_radial_map)
    radial_map_config(file);                 // Radial map configuration
  if (output_cube_map)
    cube_map_config(file);                   // Radial spectral map configuration

  // Group line-of-sight healpix map configuration and parameters
  if constexpr (output_groups) {
    load("output_map2_groups", output_map2_grp); // Output statistical moment group maps
    load("output_freq4_map_groups", output_freq4_map_grp); // Output frequency^4 group maps
    // Output frequency^3 group maps (Note: freq4 requires freq3)
    load_if_false(output_freq4_map_grp, "output_freq3_map_groups", output_freq3_map_grp);
    // Output frequency^2 group maps (Note: freq3 requires freq2)
    load_if_false(output_freq3_map_grp, "output_freq2_map_groups", output_freq2_map_grp);
    // Output average frequency group maps (Note: freq2 requires freq)
    load_if_false(output_freq2_map_grp, "output_freq_map_groups", output_freq_map_grp);
    // Output line-of-sight healpix group maps (Note: map2, rgb, and freq require map)
    load_if_false(output_map2_grp || output_freq_map_grp, "output_map_groups", output_map_grp);
    load("output_flux_map_groups", output_flux_map_grp); // Output spectral flux group maps
    load("output_flux_avg_groups", output_flux_avg_grp); // Output angle-averaged spectral flux
    load("output_radial_map_groups", output_radial_map_grp); // Output radial surface brightness group maps
    load("output_radial_avg_groups", output_radial_avg_grp); // Output angle-averaged radial surface brightness
    if (!output_grp_obs) {
      if (output_flux_map_grp)
        root_error("output_flux_map_groups requires output_groups_obs");
      if (output_flux_avg_grp)
        root_error("output_flux_avg_groups requires output_groups_obs");
      if (output_radial_map_grp)
        root_error("output_radial_map_groups requires output_groups_obs");
      if (output_radial_avg_grp)
        root_error("output_radial_avg_groups requires output_groups_obs");
    }

    // Specific healpix map parameters
    if (output_map_grp)
      group_map_config(file);                // Healpix map configuration
    if (output_radial_map_grp)
      group_radial_map_config(file);         // Radial map configuration
    if (output_radial_avg_grp)
      group_radial_avg_config(file);         // Radial average configuration
  }

  // Subhalo healpix map configuration and parameters
  if constexpr (output_subhalos) {
    load("output_map2_subhalos", output_map2_sub); // Output statistical moment subhalo maps
    load("output_freq4_map_subhalos", output_freq4_map_sub); // Output frequency^4 subhalo maps
    // Output frequency^3 subhalo maps (Note: freq4 requires freq3)
    load_if_false(output_freq4_map_sub, "output_freq3_map_subhalos", output_freq3_map_sub);
    // Output frequency^2 subhalo maps (Note: freq3 requires freq2)
    load_if_false(output_freq3_map_sub, "output_freq2_map_subhalos", output_freq2_map_sub);
    // Output average frequency subhalo maps (Note: freq2 requires freq)
    load_if_false(output_freq2_map_sub, "output_freq_map_subhalos", output_freq_map_sub);
    // Output line-of-sight healpix subhalo maps (Note: map2, rgb, and freq require map)
    load_if_false(output_map2_sub || output_freq_map_sub, "output_map_subhalos", output_map_sub);
    load("output_flux_map_subhalos", output_flux_map_sub); // Output spectral flux subhalo maps
    load("output_flux_avg_subhalos", output_flux_avg_sub); // Output angle-averaged spectral flux
    load("output_radial_map_subhalos", output_radial_map_sub); // Output radial surface brightness subhalo maps
    load("output_radial_avg_subhalos", output_radial_avg_sub); // Output angle-averaged radial surface brightness
    if (!output_sub_obs) {
      if (output_flux_map_sub)
        root_error("output_flux_map_subhalos requires output_subhalos_obs");
      if (output_flux_avg_sub)
        root_error("output_flux_avg_subhalos requires output_subhalos_obs");
      if (output_radial_map_sub)
        root_error("output_radial_map_subhalos requires output_subhalos_obs");
      if (output_radial_avg_sub)
        root_error("output_radial_avg_subhalos requires output_subhalos_obs");
    }

    // Specific healpix map parameters
    if (output_map_sub)
      subhalo_map_config(file);              // Healpix map configuration
    if (output_radial_map_sub)
      subhalo_radial_map_config(file);       // Radial map configuration
    if (output_radial_avg_sub)
      subhalo_radial_avg_config(file);       // Radial average configuration
  }

  // Line-of-sight healpix flux map parameters
  if (output_flux_map) {
    incompatible("n_exp_flux", "n_side_flux"); // Cannot specify both
    if (file["n_exp_flux"]) {
      load("n_exp_flux", n_side_flux, ">=0"); // Healpix exponent for flux map
      n_side_flux = 1 << n_side_flux;        // Convert n_exp to n_side
    } else {
      load("n_side_flux", n_side_flux, ">0"); // Healpix resolution for flux map
    }

    // Frequency parameters
    n_map_bins = n_bins;                     // Reset default number of frequency bins
    map_freq_min = freq_min;                 // Reset default frequency extrema
    map_freq_max = freq_max;
    load("n_map_bins", n_map_bins, ">0");    // Number of frequency bins for flux map
    if (file["map_freq_range"]) {
      vector<double> map_freq_lims = {map_freq_min, map_freq_max};
      load("map_freq_range", map_freq_lims); // Generic frequency extrema [freq units]
      map_freq_min = map_freq_lims[0];
      map_freq_max = map_freq_lims[1];
    } else {
      load("map_freq_min", map_freq_min);    // Generic frequency extrema [freq units]
      load("map_freq_max", map_freq_max);
    }
    if (map_freq_min >= map_freq_max)
      root_error("map_freq_min must be < map_freq_max"); // Validate range
  }

  // Line-of-sight healpix group flux map parameters
  if (output_flux_map_grp) {
    incompatible("n_exp_flux_groups", "n_side_flux_groups"); // Cannot specify both
    if (file["n_exp_flux_groups"]) {
      load("n_exp_flux_groups", n_side_flux_grp, ">=0"); // Healpix exponent for group flux map
      n_side_flux_grp = 1 << n_side_flux_grp; // Convert n_exp to n_side
    } else {
      load("n_side_flux_groups", n_side_flux_grp, ">0"); // Healpix resolution for group flux map
    }

    // Frequency parameters
    n_map_bins_grp = n_bins;                 // Reset default number of frequency bins
    map_freq_min_grp = freq_min;             // Reset default frequency extrema
    map_freq_max_grp = freq_max;
    load("n_map_bins_groups", n_map_bins_grp, ">0"); // Number of frequency bins for group flux map
    if (file["map_freq_range_groups"]) {
      vector<double> map_freq_lims_grp = {map_freq_min_grp, map_freq_max_grp};
      load("map_freq_range_groups", map_freq_lims_grp); // Generic frequency extrema [freq units]
      map_freq_min_grp = map_freq_lims_grp[0];
      map_freq_max_grp = map_freq_lims_grp[1];
    } else {
      load("map_freq_min_groups", map_freq_min_grp); // Generic frequency extrema [freq units]
      load("map_freq_max_groups", map_freq_max_grp);
    }
    if (map_freq_min_grp >= map_freq_max_grp)
      root_error("map_freq_min_groups must be < map_freq_max_groups"); // Validate range
  }

  // Line-of-sight healpix subhalo flux map parameters
  if (output_flux_map_sub) {
    incompatible("n_exp_flux_subhalos", "n_side_flux_subhalos"); // Cannot specify both
    if (file["n_exp_flux_subhalos"]) {
      load("n_exp_flux_subhalos", n_side_flux_sub, ">=0"); // Healpix exponent for subhalo flux map
      n_side_flux_sub = 1 << n_side_flux_sub; // Convert n_exp to n_side
    } else {
      load("n_side_flux_subhalos", n_side_flux_sub, ">0"); // Healpix resolution for subhalo flux map
    }

    // Frequency parameters
    n_map_bins_sub = n_bins;                  // Reset default number of frequency bins
    map_freq_min_sub = freq_min;              // Reset default frequency extrema
    map_freq_max_sub = freq_max;
    load("n_map_bins_subhalos", n_map_bins_sub, ">0"); // Number of frequency bins for subhalo flux map
    if (file["map_freq_range_subhalos"]) {
      vector<double> map_freq_lims_sub = {map_freq_min_sub, map_freq_max_sub};
      load("map_freq_range_subhalos", map_freq_lims_sub); // Generic frequency extrema [freq units]
      map_freq_min_sub = map_freq_lims_sub[0];
      map_freq_max_sub = map_freq_lims_sub[1];
    } else {
      load("map_freq_min_subhalos", map_freq_min_sub); // Generic frequency extrema [freq units]
      load("map_freq_max_subhalos", map_freq_max_sub);
    }
    if (map_freq_min_sub >= map_freq_max_sub)
      root_error("map_freq_min_subhalos must be < map_freq_max_subhalos"); // Validate range
  }

  // Angle-averaged group flux parameters (groups)
  if (output_flux_avg_grp) {
    // Frequency parameters
    n_bins_grp = n_bins;                     // Reset default number of frequency bins
    freq_min_grp = freq_min;                 // Reset default frequency extrema
    freq_max_grp = freq_max;
    load("n_bins_groups", n_bins_grp, ">0"); // Number of frequency bins for group flux avg
    if (file["freq_range_groups"]) {
      vector<double> freq_lims_grp = {freq_min_grp, freq_max_grp};
      load("freq_range_groups", freq_lims_grp); // Generic frequency extrema [freq units]
      freq_min_grp = freq_lims_grp[0];
      freq_max_grp = freq_lims_grp[1];
    } else {
      load("freq_min_groups", freq_min_grp); // Generic frequency extrema [freq units]
      load("freq_max_groups", freq_max_grp);
    }
    if (freq_min_grp >= freq_max_grp)
      root_error("freq_min_groups must be < freq_max_groups"); // Validate range
  }

  // Angle-averaged group flux parameters (subhalos)
  if (output_flux_avg_sub) {
    // Frequency parameters
    n_bins_sub = n_bins;                     // Reset default number of frequency bins
    freq_min_sub = freq_min;                 // Reset default frequency extrema
    freq_max_sub = freq_max;
    load("n_bins_subhalos", n_bins_sub, ">0"); // Number of frequency bins for subhalo flux avg
    if (file["freq_range_subhalos"]) {
      vector<double> freq_lims_sub = {freq_min_sub, freq_max_sub};
      load("freq_range_subhalos", freq_lims_sub); // Generic frequency extrema [freq units]
      freq_min_sub = freq_lims_sub[0];
      freq_max_sub = freq_lims_sub[1];
    } else {
      load("freq_min_subhalos", freq_min_sub); // Generic frequency extrema [freq units]
      load("freq_max_subhalos", freq_max_sub);
    }
    if (freq_min_sub >= freq_max_sub)
      root_error("freq_min_subhalos must be < freq_max_subhalos"); // Validate range
  }

  // Line-of-sight healpix radial spectral map parameters
  if (output_cube_map) {
    incompatible("n_exp_cube", "n_side_cube"); // Cannot specify both
    if (file["n_exp_cube"]) {
      load("n_exp_cube", n_side_cube, ">=0"); // Healpix exponent for cube map
      n_side_cube = 1 << n_side_cube;        // Convert n_exp to n_side
    } else {
      load("n_side_cube", n_side_cube, ">0"); // Healpix resolution for cube map
    }

    // Radial spectral map size parameters
    incompatible("cube_map_radius_bbox", "cube_map_radius_Rvir", "cube_map_radius_kpc", "cube_map_radius");
    if (file["cube_map_radius_bbox"]) {      // Radial spectral map radius [min distance to bbox edge]
      load("cube_map_radius_bbox", cube_map_radius_bbox, ">0");
    } else if (file["cube_map_radius_Rvir"]) { // Radial spectral map radius [selected halo virial radius]
      load("cube_map_radius_Rvir", cube_map_radius_Rvir, ">0");
    } else if (file["cube_map_radius_kpc"]) { // Radial spectral map radius [kpc]
      load("cube_map_radius_kpc", cube_map_radius, ">0");
      cube_map_radius *= kpc;                // Convert to cm
    } else if (file["cube_map_radius"]) {    // Radial spectral map radius [cm]
      load("cube_map_radius", cube_map_radius, ">0");
    }

    // Radial spectral map resolution parameters
    incompatible("cube_map_pixel_arcsec", "cube_map_pixel_width", "cube_map_pixel_width_pc", "n_cube_map_pixels");
    if (file["cube_map_pixel_arcsec"]) {     // Radial spectral pixel width [arcsec]
      load("cube_map_pixel_arcsec", cube_map_pixel_arcsec, ">0");
      if (!cosmological)
        root_error("Cannot accept cube_map_pixel_arcsec for non-cosmological simulations in " + config_file);
    } else if (file["cube_map_pixel_width"]) { // Radial spectral map pixel width [cm]
      load("cube_map_pixel_width", cube_map_pixel_width, ">0");
    } else if (file["cube_map_pixel_width_pc"]) { // Radial spectral map pixel width [pc]
      load("cube_map_pixel_width_pc", cube_map_pixel_width, ">0");
      cube_map_pixel_width *= pc;            // Convert to cm
    } else {                                 // Set n_cube_map_pixels directly
      load("n_cube_map_pixels", n_cube_map_pixels, ">0");
    }

    // Frequency parameters
    n_cube_map_bins = n_bins;                // Reset default number of frequency bins
    cube_map_freq_min = freq_min;            // Reset default frequency extrema
    cube_map_freq_max = freq_max;
    load("n_cube_map_bins", n_cube_map_bins, ">0"); // Number of frequency bins for radial spectral map
    if (file["cube_map_freq_range"]) {
      vector<double> cube_map_freq_lims = {cube_map_freq_min, cube_map_freq_max};
      load("cube_map_freq_range", cube_map_freq_lims); // Generic frequency extrema [freq units]
      cube_map_freq_min = cube_map_freq_lims[0];
      cube_map_freq_max = cube_map_freq_lims[1];
    } else {
      load("cube_map_freq_min", cube_map_freq_min); // Generic frequency extrema [freq units]
      load("cube_map_freq_max", cube_map_freq_max);
    }
    if (cube_map_freq_min >= cube_map_freq_max)
      root_error("cube_map_freq_min must be < cube_map_freq_max"); // Validate range
  }

  // Angular cosine (μ=cosθ) configuration
  load("output_mu2", output_mu2);            // Output statistical moment map
  load("output_freq4_mu", output_freq4_mu);  // Output frequency^4 map
  // Output frequency^3 map (Note: freq4 requires freq3)
  load_if_false(output_freq4_mu, "output_freq3_mu", output_freq3_mu);
  // Output frequency^2 map (Note: freq3 requires freq2)
  load_if_false(output_freq3_mu, "output_freq2_mu", output_freq2_mu);
  // Output average frequency map (Note: freq2 requires freq)
  load_if_false(output_freq2_mu, "output_freq_mu", output_freq_mu);
  // Output line-of-sight map (Note: map2 and freq require map)
  load_if_false(output_mu2 || output_freq_mu, "output_mu", output_mu);
  load("output_flux_mu", output_flux_mu);    // Output spectral flux

  // Line-of-sight angular cosine (μ=cosθ) parameters
  if (output_mu) {
    if (file["n_mu"])
      load("n_mu", n_mu, ">0");              // Number of mu bins
  }

  // Line-of-sight angular cosine (μ=cosθ) flux parameters
  if (output_flux_mu) {
    if (file["n_mu_flux"])
      load("n_mu_flux", n_mu_flux, ">0");    // Number of mu bins for flux

    // Frequency parameters
    n_mu_bins = n_bins;                      // Reset default number of frequency bins
    mu_freq_min = freq_min;                  // Reset default frequency extrema
    mu_freq_max = freq_max;
    load("n_mu_bins", n_mu_bins, ">0");      // Number of frequency bins for flux
    if (file["mu_freq_range"]) {
      vector<double> mu_freq_lims = {mu_freq_min, mu_freq_max};
      load("mu_freq_range", mu_freq_lims);   // Generic frequency extrema [freq units]
      mu_freq_min = mu_freq_lims[0];
      mu_freq_max = mu_freq_lims[1];
    } else {
      load("mu_freq_min", mu_freq_min);      // Generic frequency extrema [freq units]
      load("mu_freq_max", mu_freq_max);
    }
    if (mu_freq_min >= mu_freq_max)
      root_error("mu_freq_min must be < mu_freq_max"); // Validate range
  }

  // RGB frequency parameters
  if (output_rgb_images || output_rgb_radial_images || output_rgb_map) {
    if (file["rgb_freq_range"]) {
      vector<double> rgb_freq_lims = {rgb_freq_min, rgb_freq_max};
      load("rgb_freq_range", rgb_freq_lims); // Generic RGB frequency extrema [freq units]
      rgb_freq_min = rgb_freq_lims[0];
      rgb_freq_max = rgb_freq_lims[1];
    } else {
      load("rgb_freq_min", rgb_freq_min);    // Generic RGB frequency extrema [freq units]
      load("rgb_freq_max", rgb_freq_max);
    }
    if (rgb_freq_min >= rgb_freq_max)
      root_error("rgb_freq_min must be < rgb_freq_max"); // Validate rgb_freq_min range
  }
}