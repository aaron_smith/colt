/**********************
 * mcrt/initialize.cc *
 **********************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "proto.h"
#include "MCRT.h"
#include "../rates.h" // Thermochemistry
#include "../reduction.h" // Reduction operations

vector<Vec3> healpix_vectors(const int nside); // Healpix directions
bool avoid_cell_host_halo(const int cell); // Check if a cell is not in a valid group
bool avoid_star_host_halo(const int star); // Check if a star is not in a valid group
bool avoid_cell(const int cell); // Avoid calculations for certain cells
bool avoid_cell_strict(const int cell); // Avoid calculations without overrides
Vec3 cell_center(const int cell); // Center of the cell
int find_cell(const Vec3 point, int cell); // Cell index of a point
int get_index_sorted(const int val, const vector<int> vec); // Get index of a value in a sorted vector
extern double (*H)(const double a, const double x); // Line profile function pointer

void atau_NL_calculation(); // Non-local core-skipping calculation
double H_gaussian(const double a, const double x); // Gaussian line profile
double H_voigt(const double a, const double x); // Voigt-Hjerting function (2nd order approximation)
void setup_q_col_table(); // Setup collisional excitation rate table
double q_col_table(const double T, const double N); // Table evaluation
double qp_col_table(const double T, const double N); // Table evaluation (doublet)

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

static double q_coef, T_12, T_13, T_14, T_15; // Collisional excitation coefficient and temperatures [K]
static double E_12, E_13, E_14, E_15; // Collisional excitation energies [erg]
enum RecColForms { UNKNOWN, PEQ_1991, QCOL_TAB };
static int rec_form = UNKNOWN; // Recombination functional form
static int col_form = UNKNOWN; // Collisional excitation functional form
static double a_PEQ91, b_PEQ91, c_PEQ91, d_PEQ91, Br_PEQ91, T_PEQ91; // PEQ 1991 line parameters
static double a_PEQ91_tot, b_PEQ91_tot, c_PEQ91_tot, d_PEQ91_tot, A_PEQ91, B_PEQ91; // Total parameters
static double (*alpha_tot)(double){ return_zero };

// Avoid stars that are outside the emission region
static inline bool invalid_star(const int i) {
  return avoid_cell_strict(find_cell(r_star[i], 0)) ||
    #if spherical_escape
      (r_star[i] - escape_center).dot() >= emission_radius2 ||
    #endif
    #if box_escape
      (r_star[i].x < emission_box[0].x || r_star[i].x > emission_box[1].x ||
       r_star[i].y < emission_box[0].y || r_star[i].y > emission_box[1].y ||
       r_star[i].z < emission_box[0].z || r_star[i].z > emission_box[1].z) ||
    #endif
    avoid_star_host_halo(i);
};

// Avoid cells that are outside the emission region
static inline bool invalid_cell(const int i) {
  return avoid_cell_strict(i) ||
    #if spherical_escape
      (cell_center(i) - escape_center).dot() >= emission_radius2 ||
    #endif
    #if box_escape
      (r[i].x < emission_box[0].x || r[i].x > emission_box[1].x ||
       r[i].y < emission_box[0].y || r[i].y > emission_box[1].y ||
       r[i].z < emission_box[0].z || r[i].z > emission_box[1].z) ||
    #endif
    avoid_cell_host_halo(i) || (avoid_SFR && SFR[i] > 0.);
};

/* Modify a read flag based on a field index. */
void modify_read_flag(const int field_index, const int value = true) {
  if (field_index < 0 || field_index >= n_fields)
    error("Unrecognized field index in modify_read_flag: " + to_string(field_index));
  if (!value && field_index == HydrogenDensity)
    error("Cannot modify read flag for n_H [HydrogenDensity]"); // n_H
  fields[field_index].read = value; // Set the read flag
}

/* Set up the total radiative recombination rate coefficient translation. */
void setup_alpha_tot() {
  switch (line_type) {
    case CI_Line:
      alpha_tot = alpha_RR_CII; break;
    case CII_Line:
      alpha_tot = alpha_RR_CIII; break;
    case CIII_Line:
      alpha_tot = alpha_RR_CIV; break;
    case CIV_Line:
      alpha_tot = alpha_RR_CV; break;
    case CV_Line:
      alpha_tot = alpha_RR_CVI; break;
    case NI_Line:
      alpha_tot = alpha_RR_NII; break;
    case NII_Line:
      alpha_tot = alpha_RR_NIII; break;
    case NIII_Line:
      alpha_tot = alpha_RR_NIV; break;
    case NIV_Line:
      alpha_tot = alpha_RR_NV; break;
    case NV_Line:
      alpha_tot = alpha_RR_NVI; break;
    case NVI_Line:
      alpha_tot = alpha_RR_NVII; break;
    case OI_Line:
      alpha_tot = alpha_RR_OII; break;
    case OII_Line:
      alpha_tot = alpha_RR_OIII; break;
    case OIII_Line:
      alpha_tot = alpha_RR_OIV; break;
    case OIV_Line:
      alpha_tot = alpha_RR_OV; break;
    case OV_Line:
      alpha_tot = alpha_RR_OVI; break;
    case OVI_Line:
      alpha_tot = alpha_RR_OVII; break;
    case OVII_Line:
      alpha_tot = alpha_RR_OVIII; break;
    case NeVI_Line:
      alpha_tot = alpha_RR_NeVII; break;
    case NeVII_Line:
      alpha_tot = alpha_RR_NeVIII; break;
    case NeVIII_Line:
      alpha_tot = alpha_RR_NeIX; break;
    default:
      error("Line type not implemented for alpha_tot: " + to_string(line_type)); break;
  }
}

/* Set up the PEQ 1991 total recombination rate parameters. */
void setup_alpha_PEQ91() {
  switch (line_type) {
    // Carbon
    case CI_Line: a_PEQ91_tot = 5.068; b_PEQ91_tot = -0.6192; c_PEQ91_tot = -0.0815; d_PEQ91_tot = 1.291; break;
    case CII_Line: a_PEQ91_tot = 5.434; b_PEQ91_tot = -0.6116; c_PEQ91_tot = 0.0694; d_PEQ91_tot = 0.7866; break;
    case CIII_Line: a_PEQ91_tot = 4.742; b_PEQ91_tot = -0.6167; c_PEQ91_tot = 0.296; d_PEQ91_tot = 0.6167; break;
    case CIV_Line: a_PEQ91_tot = 4.051; b_PEQ91_tot = -0.627; c_PEQ91_tot = 0.5054; d_PEQ91_tot = 0.6692; break;
    case CV_Line: a_PEQ91_tot = 5.481; b_PEQ91_tot = -0.5978; c_PEQ91_tot = 0.5124; d_PEQ91_tot = 0.4082; break;
    // Nitrogen
    case NI_Line: a_PEQ91_tot = 3.874; b_PEQ91_tot = -0.6487; c_PEQ91_tot = 0.; d_PEQ91_tot = 1.; break;
    case NII_Line: a_PEQ91_tot = 4.974; b_PEQ91_tot = -0.6209; c_PEQ91_tot = 0.; d_PEQ91_tot = 1.; break;
    case NIII_Line: a_PEQ91_tot = 4.75; b_PEQ91_tot = -0.5942; c_PEQ91_tot = 0.8452; d_PEQ91_tot = 2.845; break;
    case NIV_Line: a_PEQ91_tot = 4.626; b_PEQ91_tot = -0.9521; c_PEQ91_tot = 0.4729; d_PEQ91_tot = -0.4477; break;
    case NV_Line: a_PEQ91_tot = 4.08; b_PEQ91_tot = -0.6253; c_PEQ91_tot = 0.583; d_PEQ91_tot = 0.7129; break;
    case NVI_Line: a_PEQ91_tot = 5.403; b_PEQ91_tot = -0.5991; c_PEQ91_tot = 0.5091; d_PEQ91_tot = 0.4185; break;
    // Oxygen
    case OI_Line: a_PEQ91_tot = 3.201; b_PEQ91_tot = -0.688; c_PEQ91_tot = -0.0174; d_PEQ91_tot = 1.707; break;
    case OII_Line: a_PEQ91_tot = 4.092; b_PEQ91_tot = -0.6413; c_PEQ91_tot = 0.; d_PEQ91_tot = 1.; break;
    case OIII_Line: a_PEQ91_tot = 4.89; b_PEQ91_tot = -0.6213; c_PEQ91_tot = 0.0184; d_PEQ91_tot = 1.555; break;
    case OIV_Line: a_PEQ91_tot = 14.665; b_PEQ91_tot = -0.514; c_PEQ91_tot = 2.73; d_PEQ91_tot = 0.2328; break;
    case OV_Line: a_PEQ91_tot = 4.626; b_PEQ91_tot = -0.9521; c_PEQ91_tot = 0.4729; d_PEQ91_tot = -0.4477; break;
    case OVI_Line: a_PEQ91_tot = 4.08; b_PEQ91_tot = -0.6253; c_PEQ91_tot = 0.583; d_PEQ91_tot = 0.7129; break;
    case OVII_Line: a_PEQ91_tot = 5.345; b_PEQ91_tot = -0.6001; c_PEQ91_tot = 0.5032; d_PEQ91_tot = 0.425; break;
    // Neon
    case NeI_Line: a_PEQ91_tot = 7.317; b_PEQ91_tot = -0.5358; c_PEQ91_tot = 2.413; d_PEQ91_tot = 0.4176; break;
    case NeII_Line: a_PEQ91_tot = 11.8; b_PEQ91_tot = -0.5404; c_PEQ91_tot = 3.03; d_PEQ91_tot = 0.205; break;
    case NeIII_Line: a_PEQ91_tot = 5.841; b_PEQ91_tot = -0.5921; c_PEQ91_tot = 0.4498; d_PEQ91_tot = 0.6395; break;
    case NeIV_Line: a_PEQ91_tot = 15.55; b_PEQ91_tot = -0.4825; c_PEQ91_tot = 3.274; d_PEQ91_tot = 0.303; break;
    case NeV_Line: a_PEQ91_tot = 7.538; b_PEQ91_tot = -0.554; c_PEQ91_tot = 1.296; d_PEQ91_tot = 0.3472; break;
    case NeVI_Line: a_PEQ91_tot = 5.239; b_PEQ91_tot = -0.5966; c_PEQ91_tot = 0.7135; d_PEQ91_tot = 0.4537; break;
    case NeVII_Line: a_PEQ91_tot = 4.626; b_PEQ91_tot = -0.9521; c_PEQ91_tot = 0.4729; d_PEQ91_tot = -0.4477; break;
    case NeVIII_Line: a_PEQ91_tot = 4.08; b_PEQ91_tot = -0.6253; c_PEQ91_tot = 0.583; d_PEQ91_tot = 0.7129; break;
    // case NeIX_Line: a_PEQ91_tot = 5.345; b_PEQ91_tot = -0.6001; c_PEQ91_tot = 0.5032; d_PEQ91_tot = 0.425; break;
    default: error("Line type not implemented for alpha_tot_PEQ91: " + to_string(line_type)); break;
  }
  const double z_PEQ91 = line_state + 1;     // Ionic numeral (e.g. III = 3)
  T_PEQ91 = 1e-4 / (z_PEQ91 * z_PEQ91);      // Define convenience constants
  A_PEQ91 = Br_PEQ91 * a_PEQ91 / a_PEQ91_tot; // Ratio of coefficients
  B_PEQ91 = b_PEQ91 - b_PEQ91_tot;           // Difference of exponents
  setup_alpha_tot();                         // Translation for the total rate
}

/* Set the dust parameters: Opacity, albedo, and scattering anisotropy. */
void set_line_and_dust_parameters_default() {
  if (line == "Lyman-alpha") {               // Hydrogen Lyman-alpha (1,2) [1215.67Å]
    // Lyα: 1s 2S 1/2 -> 2                   // ν0 = 2.466e15 Hz (NIST: A21 = 4.6986e8 Hz)
    E0 = h * c / (1215.6701 * angstrom);     // Line energy [erg] (3/4 E_H)
    A21 = 2. * M_PI * 9.936e7;               // Einstein A coefficient [Hz]
    f12 = 0.41641;                           // Oscillator strength
    g12 = 2. / 8.;                           // Degeneracy ratio: g_lower / g_upper
    if constexpr (deuterium_fraction > 0)
      E0D = h * c / (1215.339 * angstrom);   // Deuterium Lyman-alpha (1,2) [1215.339Å]
    if (dust_models[0] == "SMC") {
      kappa_dust = 7.172e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3304;                       // Dust scattering albedo
      g_dust = 0.5909;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 5.797e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3253;                       // Dust scattering albedo
      g_dust = 0.6761;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "LAURSEN_SMC") {
      double Z_SMC = pow(10., -0.6) * Zsun;  // SMC metallicity [mass fraction]
      sigma_dust = 3.95e-22 / Z_SMC;         // Dust cross section [cm^2/Z/hydrogen atom]
      dust_to_gas = 0.;                      // Dust-to-gas ratio is captured by sigma_dust
      albedo = 0.32;                         // Dust scattering albedo
      g_dust = 0.73;                         // Dust anisotropy parameter: <cosθ>
      f_ion = 0.01;                          // HII region survival fraction
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    anisotropic_scattering = true;           // Include anisotropic line scattering
  } else if (line == "Balmer-alpha") {       // Hydrogen Balmer-alpha (2,3) [6564.632Å]
    E0 = h * c / (6564.632 * angstrom);      // Line energy [erg] (5/36 E_H)
    A21 = 4.4101e7;                          // Einstein A coefficient [Hz]
    f12 = 0.64108;                           // Oscillator strength
    g12 = 8. / 18.;                          // Degeneracy ratio: g_lower / g_upper
    p_dest = 0.2203166;                      // Line destruction probability
    if (dust_models[0] == "SMC") {
      kappa_dust = 3266.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7287;                       // Dust scattering albedo
      g_dust = 0.5218;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6623.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6741;                       // Dust scattering albedo
      g_dust = 0.4966;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Balmer-beta") {        // Hydrogen Balmer-beta (2,4) [4862.691Å]
    E0 = h * c / (4862.691 * angstrom);      // Line energy [erg] (9/48 E_H)
    A21 = 8.4193e6;                          // Einstein A coefficient [Hz]
    f12 = 0.11938;                           // Oscillator strength
    g12 = 8. / 32.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4447.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7587;                       // Dust scattering albedo
      g_dust = 0.5613;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.021e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.665;                        // Dust scattering albedo
      g_dust = 0.5561;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Balmer-gamma") {       // Hydrogen Balmer-gamma (2,5) [4341.691Å]
    E0 = h * c / (4341.691 * angstrom);      // Line energy [erg] (21/100 E_H)
    A21 = 2.5304e6;                          // Einstein A coefficient [Hz]
    f12 = 0.044694;                          // Oscillator strength
    g12 = 8. / 50.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4995.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7648;                       // Dust scattering albedo
      g_dust = 0.5666;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.218e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6508;                       // Dust scattering albedo
      g_dust = 0.5663;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Balmer-delta") {       // Hydrogen Balmer-delta (2,6) [4102.8991Å]
    E0 = h * c / (4102.8991 * angstrom);     // Line energy [erg] (2/9 E_H)
    A21 = 9.732e5;                           // Einstein A coefficient [Hz]
    f12 = 2.2105e-2;                         // Oscillator strength
    g12 = 8. / 72.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 5291.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.767;                        // Dust scattering albedo
      g_dust = 0.5673;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.329e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6421;                       // Dust scattering albedo
      g_dust = 0.5691;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Balmer-epsilon") {     // Hydrogen Balmer-epsilon (2,7) [3971.202Å]
    E0 = h * c / (3971.202 * angstrom);      // Line energy [erg] (45/196 E_H)
    A21 = 4.3889e5;                          // Einstein A coefficient [Hz]
    f12 = 1.2711e-2;                         // Oscillator strength
    g12 = 8. / 98.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 5465.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7681;                       // Dust scattering albedo
      g_dust = 0.5678;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.396e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6367;                       // Dust scattering albedo
      g_dust = 0.5698;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Paschen-alpha") {      // Hydrogen Paschen-alpha (3,4) [18756.096Å]
    E0 = h * c / (18756.096 * angstrom);     // Line energy [erg] (7/144 E_H)
    A21 = 8.9860e6;                          // Einstein A coefficient [Hz]
    f12 = 0.84254;                           // Oscillator strength
    g12 = 18. / 32.;                         // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 1137.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.508;                        // Dust scattering albedo
      g_dust = 0.2283;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 2005.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.4837;                       // Dust scattering albedo
      g_dust = 0.1731;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Paschen-beta") {       // Hydrogen Paschen-beta (3,5) [12821.576Å]
    E0 = h * c / (12821.576 * angstrom);     // Line energy [erg] (16/225 E_H)
    A21 = 2.2008e6;                          // Einstein A coefficient [Hz]
    f12 = 0.15066;                           // Oscillator strength
    g12 = 18. / 50.;                         // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 1632.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.5961;                       // Dust scattering albedo
      g_dust = 0.3241;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 3055.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.5743;                       // Dust scattering albedo
      g_dust = 0.2731;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "Brackett-alpha") {     // Hydrogen Brackett-alpha (4,5) [40522.69Å]
    E0 = h * c / (40522.69 * angstrom);      // Line energy [erg] (9/400 E_H)
    A21 = 2.6993e6;                          // Einstein A coefficient [Hz]
    f12 = 1.0383;                            // Oscillator strength
    g12 = 32. / 50.;                         // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 536.3;                    // Dust opacity [cm^2/g dust]
      albedo = 0.2638;                       // Dust scattering albedo
      g_dust = 0.007949;                     // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 726.1;                    // Dust opacity [cm^2/g dust]
      albedo = 0.2211;                       // Dust scattering albedo
      g_dust = -0.02334;                     // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    resonant_scattering = false;             // Do not allow resonant scattering
  } else if (line == "CIII-1907-1909") {
    // CIII-1909: 2s^2 1S 0 -> 2s 2p 3P 1
    E0 = h * c / (1908.734 * angstrom);      // Line energy [erg]
    A21 = 114.;                              // Einstein A coefficient [Hz]
    f12 = 1.87e-7;                           // Oscillator strength
    g12 = 1. / 3.;                           // Degeneracy ratio: g_lower / g_upper
    // CIII-1907: 2s^2 1S 0 -> 2s 2p 3P 2
    E0p = h * c / (1906.683 * angstrom);     // Line energy [erg]
    A21p = 0.00519;                          // Einstein A coefficient [Hz]
    f12p = 1.413e-11;                        // Oscillator strength
    g12p = 1. / 5.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 1.746e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6911;                       // Dust scattering albedo
      g_dust = 0.5502;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 3.685e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.4654;                       // Dust scattering albedo
      g_dust = 0.5925;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = CIII_Line;                   // Update line type
  } else if (line == "CIV-1548-1550") {
    // CIV-1550: 1s^2 2s 2S 1/2 -> 1s^2 2p 2P 1/2
    E0 = h * c / (1550.772 * angstrom);      // Line energy [erg]
    A21 = 2.64e8;                            // Einstein A coefficient [Hz]
    f12 = 0.0952;                            // Oscillator strength
    g12 = 2. / 2.;                           // Degeneracy ratio: g_lower / g_upper
    // CIV-1548: 1s^2 2s 2S 1/2 -> 1s^2 2p 2P 3/2
    E0p = h * c / (1548.187 * angstrom);     // Line energy [erg]
    A21p = 2.65e8;                           // Einstein A coefficient [Hz]
    f12p = 0.19;                             // Oscillator strength
    g12p = 2. / 4.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4.393e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.4473;                       // Dust scattering albedo
      g_dust = 0.5744;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 4.088e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3895;                       // Dust scattering albedo
      g_dust = 0.6592;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = CIV_Line;                    // Update line type
    rec_form = PEQ_1991;                     // Recombination functional form
    a_PEQ91 = 3.772; b_PEQ91 = -0.59; c_PEQ91 = 0.889; d_PEQ91 = 0.549; Br_PEQ91 = 1.;
  } else if (line == "NII-6550") {           // Air wavelength: 6548.05Å
    // NII-6550: 2s^2 2p^2 3P 1 -> 2s^2 2p^2 1D 2
    E0 = h * c / (6549.86 * angstrom);       // Line energy [erg]
    A21 = 0.000984922;                       // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 3. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3274.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.729;                        // Dust scattering albedo
      g_dust = 0.5223;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6643.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6742;                       // Dust scattering albedo
      g_dust = 0.4972;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = NII_Line;                    // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "NII-6585") {           // Air wavelength: 6583.46Å
    // NII-6585: 2s^2 2p^2 3P 2 -> 2s^2 2p^2 1D 2
    E0 = h * c / (6585.27 * angstrom);       // Line energy [erg]
    A21 = 0.00291865;                        // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 5. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3256.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7283;                       // Dust scattering albedo
      g_dust = 0.5212;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6585.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6739;                       // Dust scattering albedo
      g_dust = 0.4958;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = NII_Line;                    // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "NIII-1747-1749") {
    // NIII-1749: 2s^2 2p 2P 1/2 -> 2s 2p^2 4P 1/2
    E0 = h * c / (1748.646 * angstrom);      // Line energy [erg]
    A21 = 497.;                              // Einstein A coefficient [Hz]
    f12 = 2.28e-7;                           // Oscillator strength
    g12 = 2. / 2.;                           // Degeneracy ratio: g_lower / g_upper
    // NIII-1747: 2s^2 2p 2P 1/2 -> 2s 2p^2 4P 3/2
    E0p = h * c / (1746.823 * angstrom);     // Line energy [erg]
    A21p = 9.49;                             // Einstein A coefficient [Hz]
    f12p = 8.68e-9;                          // Oscillator strength
    g12p = 2. / 4.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 2.61e4;                   // Dust opacity [cm^2/g dust]
      albedo = 0.5938;                       // Dust scattering albedo
      g_dust = 0.5623;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 3.599e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.4449;                       // Dust scattering albedo
      g_dust = 0.6255;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = NIII_Line;                   // Update line type
  } else if (line == "NIV-1486") {
    // NIV-1486: 2s^2 1S 0 -> 2s 2p 3P 1
    E0 = h * c / (1486.496 * angstrom);      // Line energy [erg]
    A21 = 595.;                              // Einstein A coefficient [Hz]
    f12 = 5.91e-7;                           // Oscillator strength
    g12 = 1. / 3.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 5.092e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.4053;                       // Dust scattering albedo
      g_dust = 0.5761;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 4.33e4;                   // Dust opacity [cm^2/g dust]
      albedo = 0.3748;                       // Dust scattering albedo
      g_dust = 0.6664;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = NIV_Line;                    // Update line type
  } else if (line == "OI-6302") {            // Air wavelength: 6300.304Å
    // OI-6302: 2s^2 2p^4 3P 2 -> 2s^2 2p^4 1D 2
    E0 = h * c / (6302.046 * angstrom);      // Line energy [erg]
    A21 = 0.0056511;                         // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 5. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3407.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.734;                        // Dust scattering albedo
      g_dust = 0.5295;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6996.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6753;                       // Dust scattering albedo
      g_dust = 0.5072;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OI_Line;                     // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "OI-6366") {            // Air wavelength: 6363.776Å
    // OI-6366: 2s^2 2p^4 3P 1 -> 2s^2 2p^4 1D 2
    E0 = h * c / (6365.536 * angstrom);      // Line energy [erg]
    A21 = 0.0018234;                         // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 3. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3372.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7327;                       // Dust scattering albedo
      g_dust = 0.5277;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6902.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.675;                        // Dust scattering albedo
      g_dust = 0.5047;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OI_Line;                     // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "OII-3727-3730") {      // Air wavelength: 3726.032Å, 3728.815Å (NIST: 3727.10Å, 3729.86Å)
    // OII-3730: 2s^2 2p^3 4S0 3/2 -> 2s^2 2p^3 2D0 5/2
    E0 = h * c / (3729.875 * angstrom);      // Line energy [erg]
    A21 = 0.00003058;                        // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 4. / 6.;                           // Degeneracy ratio: g_lower / g_upper
    // OII-3727: 2s^2 2p^3 4S0 3/2 -> 2s^2 2p^3 2D0 3/2
    E0p = h * c / (3727.092 * angstrom);     // Line energy [erg]
    A21p = 0.0001776;                        // Einstein A coefficient [Hz]
    f12p = 0.;                               // Oscillator strength
    g12p = 4. / 4.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 5820.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7701;                       // Dust scattering albedo
      g_dust = 0.5672;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.536e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6253;                       // Dust scattering albedo
      g_dust = 0.57;                         // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OII_Line;                    // Update line type
    rec_form = PEQ_1991;                     // Recombination functional form
    col_form = QCOL_TAB;                     // Collisional excitation function
    a_PEQ91 = 1.175; b_PEQ91 = -0.6182; c_PEQ91 = 0.0097; d_PEQ91 = -0.2717; Br_PEQ91 = 1.;
  } else if (line == "OIII-1661-1666") {
    // OIII-1666: 2s^2 2p^2 3P 2 -> 2s 2p^3 5S 2
    E0 = h * c / (1666.15 * angstrom);       // Line energy [erg]
    A21 = 548.;                              // Einstein A coefficient [Hz]
    f12 = 2.28e-7;                           // Oscillator strength
    g12 = 5. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    // OIII-1661: 2s^2 2p^2 3P 1 -> 2s 2p^3 5S 2
    E0p = h * c / (1660.809 * angstrom);     // Line energy [erg]
    A21p = 220.;                             // Einstein A coefficient [Hz]
    f12p = 1.52e-7;                          // Oscillator strength
    g12p = 3. / 5.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3.267e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.532;                        // Dust scattering albedo
      g_dust = 0.5689;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 3.747e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.4212;                       // Dust scattering albedo
      g_dust = 0.6425;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OIII_Line;                   // Update line type
  } else if (line == "OIII-4364") {          // Air wavelength: 4363.21Å
    // OIII-4364: 2s^2 2p^2 1D 2 -> 2s^2 2p^2 1S 0
    E0 = h * c / (4364.436 * angstrom);      // Line energy [erg]
    A21 = 1.71;                              // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 5. / 1.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4969.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7646;                       // Dust scattering albedo
      g_dust = 0.5663;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.208e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6516;                       // Dust scattering albedo
      g_dust = 0.566;                        // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OIII_Line;                   // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "OIII-4933") {
    // OIII-4933: 2s^2 2p^2 3P 0 -> 2s^2 2p^2 1D 2
    E0 = h * c / (4932.603 * angstrom);      // Line energy [erg]
    A21 = 2.41e-6;                           // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 1. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4382.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7577;                       // Dust scattering albedo
      g_dust = 0.5604;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 9993.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6664;                       // Dust scattering albedo
      g_dust = 0.5543;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OIII_Line;                   // Update line type
  } else if (line == "OIII-4960") {          // Air wavelength: 4958.911Å
    // OIII-4960: 2s^2 2p^2 3P 1 -> 2s^2 2p^2 1D 2
    E0 = h * c / (4960.295 * angstrom);      // Line energy [erg]
    A21 = 0.00621457;                        // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 3. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4357.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7573;                       // Dust scattering albedo
      g_dust = 0.56;                         // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 9908.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.667;                        // Dust scattering albedo
      g_dust = 0.5536;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OIII_Line;                   // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "OIII-5008") {          // Air wavelength: 5006.843Å
    // OIII-5008: 2s^2 2p^2 3P 2 -> 2s^2 2p^2 1D 2
    E0 = h * c / (5008.24 * angstrom);       // Line energy [erg]
    A21 = 0.0181352;                         // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 5. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 4314.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7567;                       // Dust scattering albedo
      g_dust = 0.5593;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 9761.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6678;                       // Dust scattering albedo
      g_dust = 0.5523;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = OIII_Line;                   // Update line type
    rec_form = PEQ_1991;                     // Recombination functional form
    col_form = QCOL_TAB;                     // Collisional excitation function
    a_PEQ91 = 1.635; b_PEQ91 = -0.592; c_PEQ91 = 0.432; d_PEQ91 = 0.085; Br_PEQ91 = 0.75;
  } else if (line == "NeIII-3969") {
    // NeIII-3969: 2s^2 2p^4 3P 1 -> 2s^2 2p^4 1D 2
    E0 = h * c / (3968.59 * angstrom);       // Line energy [erg]
    A21 = 0.054;                             // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 3. / 5.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 5469.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7682;                       // Dust scattering albedo
      g_dust = 0.5678;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 1.398e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.6365;                       // Dust scattering albedo
      g_dust = 0.5698;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = NeIII_Line;                  // Update line type
  } else if (line == "MgII-2796-2803") {
    // MgII-2803: 3s 2S 1/2 -> 3p 2P 1/2
    E0 = h * c / (2803.531 * angstrom);      // Line energy [erg]
    A21 = 2.57e8;                            // Einstein A coefficient [Hz]
    f12 = 0.303;                             // Oscillator strength
    g12 = 2. / 2.;                           // Degeneracy ratio: g_lower / g_upper
    // MgII-2796: 3s 2S 1/2 -> 3p 2P 3/2
    E0p = h * c / (2796.352 * angstrom);     // Line energy [erg]
    A21p = 2.6e8;                            // Einstein A coefficient [Hz]
    f12p = 0.608;                            // Oscillator strength
    g12p = 2. / 4.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 7679.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7797;                       // Dust scattering albedo
      g_dust = 0.5576;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 2.287e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.5717;                       // Dust scattering albedo
      g_dust = 0.5495;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = MgII_Line;                   // Update line type
  } else if (line == "SiII-1190-1193") {
    // Doublet with fluorescent transitions to 3s^2 3p 2P^0 3/2  (4 -> 2 or 3 -> 2)
    // SiII 1193: 3s^2 3p 2P^0 1/2 -> 3s 3p^2 2P 1/2  (1 -> 3)
    E0 = h * c / (1193.29 * angstrom);       // Line energy [erg]
    A21 = 2.69e9;                            // Einstein A coefficient [Hz]
    f12 = 0.575;                             // Oscillator strength
    g12 = 2. / 2.;                           // Degeneracy ratio: g_lower / g_upper
    E0F = h * c / (1197.394 * angstrom);     // Fluorescent line energy [erg]
    A2F = 1.4e9;                             // Fluorescent Einstein A coefficient [Hz]
    P2F = A2F / (A21 + A2F);                 // Probability of fluorescence
    // SiII 1190: 3s^2 3p 2P^0 1/2 -> 3s 3p^2 2P 3/2  (1 -> 4)
    E0p = h * c / (1190.416 * angstrom);     // Line energy [erg]
    A21p = 6.53e8;                           // Einstein A coefficient [Hz]
    f12p = 0.277;                            // Oscillator strength
    g12p = 2. / 4.;                          // Degeneracy ratio: g_lower / g_upper
    E0Fp = h * c / (1194.5 * angstrom);      // Fluorescent line energy [erg]
    A2Fp = 3.45e9;                           // Fluorescent Einstein A coefficient [Hz]
    P2Fp = A2Fp / (A21p + A2Fp);             // Probability of fluorescence
    if (dust_models[0] == "SMC") {
      kappa_dust = 7.366e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3266;                       // Dust scattering albedo
      g_dust = 0.5904;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 5.988e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3198;                       // Dust scattering albedo
      g_dust = 0.6743;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SiII_Line;                   // Update line type
  } else if (line == "SiII-1260") {
    // Singlet with fluorescent transition to 3s^2 3p 2P^0 3/2
    // SiII 1260: 3s^2 3p 2P^0 1/2 -> 3s^2 3d 2D 3/2
    // Note: We are completely ignoring the 1264.738Å transition
    E0 = h * c / (1260.422 * angstrom);      // Line energy [erg]
    A21 = 2.57e9;                            // Einstein A coefficient [Hz]
    f12 = 1.22;                              // Oscillator strength
    g12 = 2. / 4.;                           // Degeneracy ratio: g_lower / g_upper
    E0F = h * c / (1265.002 * angstrom);     // Fluorescent line energy [erg]
    A2F = 4.73e8;                            // Fluorescent Einstein A coefficient [Hz]
    P2F = A2F / (A21 + A2F);                 // Probability of fluorescence
    if (dust_models[0] == "SMC") {
      kappa_dust = 6.815e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3383;                       // Dust scattering albedo
      g_dust = 0.5911;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 5.447e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3362;                       // Dust scattering albedo
      g_dust = 0.6788;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SiII_Line;                   // Update line type
  } else if (line == "SiII-1304") {
    // Singlet with fluorescent transition to 3s^2 3p 2P^0 3/2
    // SiII 1304: 3s^2 3p 2P^0 1/2 -> 3s 3p^2 2S 1/2
    E0 = h * c / (1304.37 * angstrom);       // Line energy [erg]
    A21 = 3.64e8;                            // Einstein A coefficient [Hz]
    f12 = 0.0928;                            // Oscillator strength
    g12 = 2. / 2.;                           // Degeneracy ratio: g_lower / g_upper
    E0F = h * c / (1309.276 * angstrom);     // Fluorescent line energy [erg]
    A2F = 6.23e8;                            // Fluorescent Einstein A coefficient [Hz]
    P2F = A2F / (A21 + A2F);                 // Probability of fluorescence
    if (dust_models[0] == "SMC") {
      kappa_dust = 6.489e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3465;                       // Dust scattering albedo
      g_dust = 0.5904;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 5.141e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3462;                       // Dust scattering albedo
      g_dust = 0.6803;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SiII_Line;                   // Update line type
  } else if (line == "SiII-1527") {
    // Singlet with fluorescent transition to 3s^2 3p 2P^0 3/2
    // SiII 1527: 3s^2 3p 2P^0 1/2 -> 3s^2 4s 2S 1/2
    E0 = h * c / (1526.707 * angstrom);      // Line energy [erg]
    A21 = 3.81e8;                            // Einstein A coefficient [Hz]
    f12 = 0.133;                             // Oscillator strength
    g12 = 2. / 2.;                           // Degeneracy ratio: g_lower / g_upper
    E0F = h * c / (1533.431 * angstrom);     // Fluorescent line energy [erg]
    A2F = 7.52e8;                            // Fluorescent Einstein A coefficient [Hz]
    P2F = A2F / (A21 + A2F);                 // Probability of fluorescence
    if (dust_models[0] == "SMC") {
      kappa_dust = 4.639e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.4316;                       // Dust scattering albedo
      g_dust = 0.575;                        // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 4.173e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3839;                       // Dust scattering albedo
      g_dust = 0.6618;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SiII_Line;                   // Update line type
  } else if (line == "SiIII-1206") {
    // SiIII 1206: 3s^2 1S 0 -> 3s 3p 1P^0 1  (pure resonant transition)
    E0 = h * c / (1206.5 * angstrom);        // Line energy [erg]
    A21 = 2.55e9;                            // Einstein A coefficient [Hz]
    f12 = 1.67;                              // Oscillator strength
    g12 = 1. / 3.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 7.249e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3288;                       // Dust scattering albedo
      g_dust = 0.5907;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 5.873e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.323;                        // Dust scattering albedo
      g_dust = 0.6754;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SiIII_Line;                  // Update line type
  } else if (line == "SiIV-1394-1403") {
    // Doublet (pure resonant transitions)
    // SiIV 1394: 2p^63s 2s 1/2 -> 2p^63p 2P^0 3/2
    E0 = h * c / (1393.755 * angstrom);      // Line energy [erg]
    A21 = 8.8e8;                             // Einstein A coefficient [Hz]
    f12 = 0.513;                             // Oscillator strength
    g12 = 2. / 4.;                           // Degeneracy ratio: g_lower / g_upper
    // SiIV 1403: 2p^63s 2s 1/2 -> 2p^63p 2P^0 1/2
    E0p = h * c / (1402.77 * angstrom);      // Line energy [erg]
    A21p = 8.63e8;                           // Einstein A coefficient [Hz]
    f12p = 0.255;                            // Oscillator strength
    g12p = 2. / 2.;                          // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 5.893e4;                  // Dust opacity [cm^2/g dust]
      albedo = 0.3652;                       // Dust scattering albedo
      g_dust = 0.5852;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 4.7e4;                    // Dust opacity [cm^2/g dust]
      albedo = 0.3595;                       // Dust scattering albedo
      g_dust = 0.6782;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SiIV_Line;                   // Update line type
  } else if (line == "SII-6718") {           // Air wavelength: 6716.44Å (NIST: 6718.294Å)
    // SII-6718: 3s^2 3p^3 4S0 4P 3/2 -> 3s^2 3p^3 4D0 5/2
    E0 = h * c / (6718.295 * angstrom);      // Line energy [erg]
    A21 = 2.019e-4;                          // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 4. / 6.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3190.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7255;                       // Dust scattering albedo
      g_dust = 0.5171;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6425.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.6731;                       // Dust scattering albedo
      g_dust = 0.4903;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SII_Line;                    // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else if (line == "SII-6733") {           // Air wavelength: 6730.81Å (NIST: 6732.673Å)
    // SII-6731: 3s^2 3p^3 4S0 3/2 -> 3s^2 3p^3 2D0 3/2
    E0 = h * c / (6732.674 * angstrom);      // Line energy [erg]
    A21 = 6.84e-4;                           // Einstein A coefficient [Hz]
    f12 = 0.;                                // Oscillator strength
    g12 = 4. / 4.;                           // Degeneracy ratio: g_lower / g_upper
    if (dust_models[0] == "SMC") {
      kappa_dust = 3183.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.7252;                       // Dust scattering albedo
      g_dust = 0.5167;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] == "MW") {
      kappa_dust = 6407.;                    // Dust opacity [cm^2/g dust]
      albedo = 0.673;                        // Dust scattering albedo
      g_dust = 0.4897;                       // Dust anisotropy parameter: <cosθ>
    } else if (dust_models[0] != "")         // Catch unknown dust models
      root_error("Requested dust model is not implemented: " + line + " + " + dust_models[0]);
    line_type = SII_Line;                    // Update line type
    col_form = QCOL_TAB;                     // Collisional excitation function
  } else                                     // Catch unknown lines
    root_error("Requested line is not implemented: " + line);

  // Optimization when cross-sections are zero
  if (f12 == 0. && f12p == 0.)
    resonant_scattering = false;             // Do not allow resonant scattering

  // Determine the line carrier
  if (line_type >= HI_Line && line_type < HI_Line + N_Hydrogen) {
    // Hydrogen transition energies = E_H * (1/n_1^2 - 1/n_2^2)
    constexpr double e2_h = M_PI * ee * ee / h; // pi e^2 / h
    constexpr double E_H = 2. * me * e2_h * e2_h; // me e^4 / (2 hbar^2)
    E_12 = 3. * E_H / 4.;                    // Lyα energy [erg] (1,2)
    E_13 = 8. * E_H / 9.;                    // Lyβ energy [erg] (1,3)
    E_14 = 15. * E_H / 16.;                  // Lyγ energy [erg] (1,4)
    E_15 = 24. * E_H / 25.;                  // Lyδ energy [erg] (1,5)
    T_12 = E_12 / kB;                        // Lyα temperature [K] (1,2)
    T_13 = E_13 / kB;                        // Lyβ temperature [K] (1,3)
    T_14 = E_14 / kB;                        // Lyγ temperature [K] (1,4)
    T_15 = E_15 / kB;                        // Lyδ temperature [K] (1,5)
    // Lyman series collisional coefficient = sqrt(2π hbar^4 / kB me^3) / ω_1 where ω_1 = 2
    constexpr double hD2pime = h / (2. * M_PI * me);
    q_coef = 0.5 * sqrt(h * hD2pime * hD2pime * hD2pime / kB);
  } else if (line_type >= HeI_Line && line_type < HeI_Line + N_Helium)
    line_carrier = Helium_Line;
  else if (line_type >= CI_Line && line_type < CI_Line + N_Carbon)
    line_carrier = Carbon_Line;
  else if (line_type >= NI_Line && line_type < NI_Line + N_Nitrogen)
    line_carrier = Nitrogen_Line;
  else if (line_type >= OI_Line && line_type < OI_Line + N_Oxygen)
    line_carrier = Oxygen_Line;
  else if (line_type >= NeI_Line && line_type < NeI_Line + N_Neon)
    line_carrier = Neon_Line;
  else if (line_type >= MgI_Line && line_type < MgI_Line + N_Magnesium)
    line_carrier = Magnesium_Line;
  else if (line_type >= SiI_Line && line_type < SiI_Line + N_Silicon)
    line_carrier = Silicon_Line;
  else if (line_type >= SI_Line && line_type < SI_Line + N_Sulfer)
    line_carrier = Sulfer_Line;
  else if (line_type >= FeI_Line && line_type < FeI_Line + N_Iron)
    line_carrier = Iron_Line;
  else                                       // Catch unknown carriers
    root_error("Requested line carrier is not implemented: " + line);

  // Update read_* flags, m_carrier, and line_state
  const array<double, n_carriers> ms = {mH, mHe, mC, mN, mO, mNe, mMg, mSi, mS, mFe};
  const array<int, n_carriers> line_refs = {HI_Line, HeI_Line, CI_Line, NI_Line, OI_Line, NeI_Line, MgI_Line, SiI_Line, SI_Line, FeI_Line};
  const int carrier_index = line_carrier - Hydrogen_Line; // Relative index
  m_carrier = ms[carrier_index];             // Mass of carrier [g]
  line_state = line_type - line_refs[carrier_index]; // Ionic state
  modify_read_flag(line_carrier);            // Requires number densities (n_*)
  modify_read_flag(line_type);               // Requires lower ionization states (x_*)
  if (recombinations) {
    const array<int, n_carriers> n_states_all = {N_Hydrogen, N_Helium, N_Carbon, N_Nitrogen, N_Oxygen, N_Neon, N_Magnesium, N_Silicon, N_Sulfer, N_Iron};
    const int n_states = n_states_all[carrier_index]; // Number of included ionization states
    if (line_state+1 == n_states)
      root_error("Requested recombination upper ionization state exceeds the network!");
    modify_read_flag(line_type+1);           // Requires upper ionization states (x_*)
    if (line_state+2 == n_states)
      for (int i = line_refs[carrier_index]; i < line_type; ++i)
        modify_read_flag(i);                 // Infer upper state from lower ones (x_*)
  }
}

/* Set the derived dust parameters. */
void set_line_and_dust_parameters_derived() {
  // Constants for the Henyey-Greenstein dust scattering phase function
  _1_2g    = 0.5 / g_dust;                   // = 1 / (2g)
  _1pg2_2g = _1_2g + 0.5 * g_dust;           // = (1+g^2) / (2g) = 1/(2g) + g/2
  _1mg2_2g = _1_2g - 0.5 * g_dust;           // = (1-g^2) / (2g) = 1/(2g) - g/2
  _1mg_2g  = _1_2g - 0.5;                    // = (1-g) / (2g) = 1/(2g) - 1/2
  _1mg2_2D = _1mg2_2g * 0.5 * sqrt(_1_2g);   // = (1-g^2) / [2 (2g)^(3/2)]

  // Constants for the line properties
  T0 = E0 / kB;                              // Line temperature [K]
  nu0 = E0 / h;                              // Line frequency [Hz]
  lambda0 = c / nu0;                         // Line wavelength [cm]
  B21 = A21 * lambda0 * lambda0 / (2. * E0); // Einstein B21 = A21 c^2 / (2 h ν0^3)
  DnuL = A21 / (2. * M_PI);                  // Natural frequency broadening [Hz]
  if (P2F > 0.) {
    nu0F = E0F / h;                          // Fluorescent line frequency [Hz]
    lambda0F = c / nu0F / angstrom;          // Fluorescent line wavelength [angstrom]
    xFDa = 2. * (nu0F - nu0) / DnuL;         // Fluorescent line: x / a
  }
  // Conversion for special relativity (beta = v/c = avthDc u/a)
  avthDc = DnuL / (2. * nu0);                // a vth/c = DnuL / 2 nu0
  // Thermal velocity: vth_div_sqrtT = vth / sqrt(T)  (vth = sqrt(2*kB*T/mH))
  vth_div_sqrtT = sqrt(2. * kB / m_carrier);
  // Doppler width: DnuD_div_sqrtT = DnuD / sqrt(T)  (ΔνD = ν0*vth/c)
  DnuD_div_sqrtT = nu0 * vth_div_sqrtT / c;
  // "damping parameter": a_sqrtT = a * sqrt(T)  (a = ΔνL/2ΔνD)
  a_sqrtT = DnuL / (2. * DnuD_div_sqrtT);
  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
  sigma0_sqrtT = 2. * f12 * ee * ee / (M_2_SQRTPI * me * nu0 * vth_div_sqrtT);
  // Constant for stellar continuum emission
  x_div_aDv = -km / (a_sqrtT * vth_div_sqrtT); // -1/(a vth) = constant
  // Constant for observable frequency conversion (x -> Δv)
  // neg_a_vth_kms_exit = -vth_div_sqrtT * a_sqrtT / km; // Note: 1 km = 10^5 cm
  // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)
  gDa = 2. * h / (m_carrier * lambda0 * lambda0 * DnuL);
  lambda0 /= angstrom;                       // Convert wavelength [cm -> angstrom]

  if (two_level_atom)                        // Special case for two-level atoms
    H = H_gaussian;                          // Gaussian line profile
  else if (resonant_scattering)              // Allow resonant scattering
    H = H_voigt;                             // Voigt line profile (2nd order approximation)
  else                                       // Explicitly disable scattering
    H = return_zero;                         // Zero line scattering opacity

  if constexpr (deuterium_fraction > 0) {
    if (line_type >= HI_Line && line_type < HI_Line + N_Hydrogen) {
        nu0D = E0D / h;                      // Deuterium line frequency [Hz]
        lambda0D = c / nu0D;                 // Deuterium line wavelength [cm]
        // solve: xD = (ν - ν0D) / DnuD'  using  x = (ν - ν0) / DnuD  and  DnuD/DnuD' = sqrt(2) * ν0/ν0D
        xDA = M_SQRT2 * nu0 / nu0D;          // Frequency translation: xD = A x + B a
        xDB = 2. * xDA * (nu0 - nu0D) / DnuL; // Frequency translation: xD = A x + B a
        // solve: aD = DnuL' / 2 DnuD'  using  a = DnuL / 2 DnuD  and  DnuD/DnuD' = sqrt(2) * ν0/ν0D
        // Damping paramter factor: aD = A a, A = xDA here. This is also the cross-section correction
        // Recoil parameter / "damping parameter" = 2 h ν0^2 / ((2 * mH) c^2 ΔνL)
        gDaD = h / (m_carrier * lambda0D * lambda0D * DnuL); // Deuterium recoil parameter with 2 * m_carrier
        lambda0D /= angstrom;                // Convert wavelength [cm -> angstrom]
    }
  }

  if (E0p > 0.) {
    if constexpr (deuterium_fraction > 0)
      error("Deuterium is not compatible with doublet lines!");
    // Constants for the doublet line properties
    nu0p = E0p / h;                          // Line frequency [Hz]
    lambda0p = c / nu0p;                     // Line wavelength [cm]
    DnuLp = A21p / (2. * M_PI);              // Natural frequency broadening [Hz]
    // Solve: x' = (nu - nu0') / DnuD'  using  x = (nu - nu0) / DnuD  and  DnuD/DnuD' = nu0/nu0'
    xpA = nu0 / nu0p;                        // Frequency translation: x' = A x + B a
    xpB = 2. * xpA * (nu0 - nu0p) / DnuL;    // Frequency translation: x' = A x + B a
    // Solve: a' = DnuL' / 2 DnuD'  using  a = DnuL / 2 DnuD  and  DnuD/DnuD' = nu0/nu0'
    apA = xpA * DnuLp / DnuL;                // Damping paramter factor: a' = A a
    if (P2Fp > 0.) {
      nu0Fp = E0Fp / h;                      // Fluorescent line frequency [Hz]
      lambda0Fp = c / nu0Fp / angstrom;      // Fluorescent line wavelength [angstrom]
      xFpDa = 2. * (nu0Fp - nu0p) / DnuLp;   // Fluorescent line: x' / a'
    }
    // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
    sigma0p_sqrtT = 2. * f12p * ee * ee / (M_2_SQRTPI * me * nu0p * vth_div_sqrtT);
    // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)
    gDap = 2. * h / (m_carrier * lambda0p * lambda0p * DnuLp);
    lambda0p /= angstrom;                    // Convert wavelength [cm -> angstrom]
  }
}

// /* Lyman-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
// static double alpha_eff_B_Lya(const double T) {
//   // Case B recombination coefficient - Units: cm^3/s
//   // α_B(T) is from Hui & Gnedin (1997) good to 0.7% accuracy from 1 K < T < 10^9 K
//   const double lambda = 315614. / T;         // 2 T_i/T where T_i = 157,807 K
//   const double alpha_B = 2.753e-14 * pow(lambda, 1.5) / pow(1. + pow(lambda / 2.74, 0.407), 2.242);
//   // Probability of conversion to a Lyman-alpha photon - Units: dimensionless
//   // From Cantalupo et al. (2008) good to 0.1% accuracy from 100 K < T < 10^5 K
//   const double T4 = T * 1e-4;                // T4 = T / (10^4 K)
//   const double P_Lya_B = 0.686 - 0.106 * log10(T4) - 0.009 * pow(T4, -0.44);
//   return P_Lya_B * alpha_B;                  // α_eff_B = P_B(T) α_B(T)
// }

// /* Balmer-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
// static double alpha_eff_B_Ha(const double T) {
//   // Hα emission rate from Draine (2011)
//   const double T4 = T * 1e-4;                // T4 = T / (10^4 K)
//   return 1.17e-13 * pow(T4, -0.942 - 0.031 * log(T4)); // Units: cm^3/s
// }

// /* Balmer-beta effective recombination coefficient (Case B) - Units: cm^3/s */
// static double alpha_eff_B_Hb(const double T) {
//   // Hβ emission rate from Draine (2011)
//   const double T4 = T * 1e-4;                // T4 = T / (10^4 K)
//   return 3.03e-14 * pow(T4, -0.874 - 0.058 * log(T4)); // Units: cm^3/s
// }

// Temperature and density grid for Storey & Hummer (1995) recombination tables
// Note: T = {5e2, 1e3, 3e3, 5e3, 7.5e3, 1e4, 1.25e4, 1.5e4, 2e4, 3e4}
// Note: n_e = {1e2, 1e3, 1e4, 1e5, 1e6, 1e7, 1e8}
static const double logT_SH95[10] = {log10(5e2), 3., log10(3e3), log10(5e3), log10(7.5e3), 4.,
                                      log10(1.25e4), log10(1.5e4), log10(2e4), log10(3e4)};

// Case B probability of emitting a Lyman-alpha photon per recombination (T,n_e)
static const double P_Lya_SH95[10][7] = {
  {0.80793637, 0.90215197, 0.98157401, 0.99705340, 0.99933853, 0.99934845, 0.99967896},
  {0.78439303, 0.87743841, 0.97423464, 0.99680114, 0.99960243, 0.99967816, 0.99976710},
  {0.74159266, 0.82809599, 0.95734434, 0.99359891, 0.99892162, 0.99927343, 0.99979471},
  {0.71946081, 0.80133228, 0.94581237, 0.99246182, 0.99848958, 0.99940329, 0.99949246},
  {0.70117195, 0.77884530, 0.93441344, 0.99075058, 0.99831301, 0.99960223, 0.99940949},
  {0.68808416, 0.76220739, 0.92503048, 0.98912680, 0.99828523, 0.99940740, 0.99944339},
  {0.67755810, 0.74868826, 0.91762730, 0.98784252, 0.99835238, 0.99903921, 0.99977064},
  {0.66862468, 0.73785038, 0.91033402, 0.98701405, 0.99817046, 0.99892625, 0.99953078},
  {0.65534642, 0.72080463, 0.89823705, 0.98497224, 0.99798854, 0.99881330, 0.99929092},
  {0.63566376, 0.69600394, 0.87952243, 0.98121463, 0.99716593, 0.99896144, 0.99905958}
};

// Case B probability of emitting a Balmer-alpha photon per recombination (T,n_e)
static const double P_Ha_SH95[10][7] = {
  {0.60176416, 0.59203990, 0.57777268, 0.55757870, 0.53218274, 0.50309773, 0.47538990},
  {0.57146208, 0.56548156, 0.55537185, 0.54126081, 0.52171947, 0.49779442, 0.47325611},
  {0.51602376, 0.51308374, 0.50870615, 0.50163462, 0.49109775, 0.47695493, 0.46057657},
  {0.48842989, 0.48670009, 0.48384243, 0.47916786, 0.47205133, 0.46211067, 0.45011817},
  {0.46673159, 0.46571739, 0.46369825, 0.46057413, 0.45555148, 0.44857577, 0.43958049},
  {0.45180723, 0.45102825, 0.44947568, 0.44724222, 0.44349888, 0.43815604, 0.43129878},
  {0.44059820, 0.43967487, 0.43880459, 0.43692031, 0.43424518, 0.42968461, 0.42434709},
  {0.43139784, 0.43080340, 0.43015528, 0.42868403, 0.42629594, 0.42286913, 0.41840805},
  {0.41795682, 0.41743320, 0.41691031, 0.41586673, 0.41447832, 0.41195330, 0.40867726},
  {0.39957948, 0.39949886, 0.39933772, 0.39872322, 0.39820159, 0.39687074, 0.39529763}
};

// Case B probability of emitting a Balmer-beta photon per recombination (T,n_e)
static const double P_Hb_SH95[10][7] = {
  {0.11197758, 0.11382091, 0.11630471, 0.11882216, 0.12108799, 0.12288014, 0.12720456},
  {0.11559987, 0.11672083, 0.11811675, 0.11985224, 0.12129050, 0.12256368, 0.12615841},
  {0.11908521, 0.11942732, 0.11988399, 0.12040081, 0.12086866, 0.12131817, 0.12352423},
  {0.11897682, 0.11914759, 0.11943147, 0.11964361, 0.11981875, 0.11999444, 0.12172966},
  {0.11803310, 0.11815162, 0.11823874, 0.11840903, 0.11841351, 0.11845008, 0.11983194},
  {0.11688913, 0.11694270, 0.11695517, 0.11706977, 0.11706124, 0.11708462, 0.11828955},
  {0.11571266, 0.11566493, 0.11578498, 0.11586298, 0.11583936, 0.11573996, 0.11684562},
  {0.11460234, 0.11463318, 0.11469477, 0.11472462, 0.11466497, 0.11459548, 0.11563639},
  {0.11271958, 0.11270919, 0.11273303, 0.11274643, 0.11268794, 0.11258888, 0.11350468},
  {0.10960571, 0.10960828, 0.10961341, 0.10961006, 0.10954052, 0.10948666, 0.11028151}
};

// Case B probability of emitting a Balmer-gamma photon per recombination (T,n_e)
static const double P_Hg_SH95[10][7] = {
  {0.04201613, 0.04300207, 0.04442060, 0.04620360, 0.04833380, 0.05180051, 0.05900800},
  {0.04445549, 0.04508442, 0.04597248, 0.04723395, 0.04878288, 0.05153151, 0.05778401},
  {0.04780603, 0.04803281, 0.04839422, 0.04889559, 0.04959578, 0.05115426, 0.05535776},
  {0.04869447, 0.04880649, 0.04905029, 0.04932562, 0.04972818, 0.05084741, 0.05406425},
  {0.04894908, 0.04902770, 0.04914434, 0.04931948, 0.04956781, 0.05037671, 0.05290131},
  {0.04887845, 0.04892308, 0.04898677, 0.04910065, 0.04927799, 0.04990298, 0.05202193},
  {0.04867211, 0.04867532, 0.04876216, 0.04882675, 0.04896197, 0.04944524, 0.05131098},
  {0.04838950, 0.04842262, 0.04846499, 0.04852574, 0.04859925, 0.04902806, 0.05067135},
  {0.04785069, 0.04784778, 0.04787542, 0.04790009, 0.04793400, 0.04825843, 0.04963792},
  {0.04679314, 0.04678370, 0.04678686, 0.04679788, 0.04678062, 0.04701408, 0.04811984}
};

// Case B probability of emitting a Balmer-delta photon per recombination (T,n_e)
static const double P_Hd_SH95[10][7] = {
  {0.02109056, 0.02162223, 0.02242694, 0.02352853, 0.02523019, 0.02929151, 0.03647097},
  {0.02251388, 0.02286241, 0.02337582, 0.02416699, 0.02544500, 0.02872042, 0.03513255},
  {0.02461022, 0.02474142, 0.02495551, 0.02529094, 0.02594002, 0.02793451, 0.03223331},
  {0.02524504, 0.02531937, 0.02544631, 0.02564597, 0.02606706, 0.02753598, 0.03072347},
  {0.02549994, 0.02554921, 0.02562219, 0.02575119, 0.02603134, 0.02715664, 0.02951235},
  {0.02553874, 0.02557294, 0.02561725, 0.02569328, 0.02590854, 0.02682556, 0.02866888},
  {0.02548653, 0.02548937, 0.02554250, 0.02559362, 0.02575902, 0.02652537, 0.02803061},
  {0.02537701, 0.02539690, 0.02541418, 0.02545984, 0.02558400, 0.02626338, 0.02750274},
  {0.02513928, 0.02513614, 0.02514743, 0.02516997, 0.02525799, 0.02580427, 0.02670334},
  {0.02461963, 0.02461466, 0.02462555, 0.02460818, 0.02465908, 0.02507381, 0.02563413}
};

// Case B probability of emitting a Balmer-epsilon photon per recombination (T,n_e)
static const double P_He_SH95[10][7] = {
  {0.01236677, 0.01269669, 0.01320054, 0.01396256, 0.01560329, 0.02011311, 0.02620399},
  {0.01324123, 0.01345937, 0.01379294, 0.01434085, 0.01559826, 0.01928819, 0.02486383},
  {0.01455964, 0.01464171, 0.01478131, 0.01503404, 0.01572268, 0.01797897, 0.02178548},
  {0.01497012, 0.01501864, 0.01509961, 0.01525616, 0.01573583, 0.01739606, 0.02017733},
  {0.01514584, 0.01517759, 0.01522259, 0.01532706, 0.01567484, 0.01693436, 0.01892051},
  {0.01518076, 0.01520176, 0.01522822, 0.01530019, 0.01557785, 0.01659052, 0.01806633},
  {0.01516266, 0.01516008, 0.01519246, 0.01523581, 0.01547197, 0.01632220, 0.01743615},
  {0.01509442, 0.01510795, 0.01512412, 0.01516718, 0.01534991, 0.01609385, 0.01694092},
  {0.01495760, 0.01496112, 0.01496463, 0.01498559, 0.01513864, 0.01573279, 0.01619841},
  {0.01465647, 0.01465553, 0.01465163, 0.01465137, 0.01474681, 0.01520321, 0.01525805}
};

// Case B probability of emitting a Paschen-alpha photon per recombination (T,n_e)
static const double P_Paa_SH95[10][7] = {
  {0.32614959, 0.31160767, 0.29095068, 0.26378240, 0.23187496, 0.19849730, 0.16865559},
  {0.28573104, 0.27671586, 0.26290888, 0.24406397, 0.21991833, 0.19281748, 0.16664950},
  {0.21805802, 0.21450247, 0.20864657, 0.19975209, 0.18757159, 0.17207439, 0.15505924},
  {0.18842673, 0.18620048, 0.18256720, 0.17697689, 0.16891281, 0.15828873, 0.14579711},
  {0.16674031, 0.16533443, 0.16293730, 0.15925275, 0.15371473, 0.14629550, 0.13705592},
  {0.15267112, 0.15161900, 0.14988586, 0.14720832, 0.14315584, 0.13757837, 0.13039950},
  {0.14252317, 0.14166471, 0.14047895, 0.13835956, 0.13530560, 0.13082510, 0.12513531},
  {0.13471866, 0.13413161, 0.13311337, 0.13149314, 0.12898747, 0.12542193, 0.12069175},
  {0.12351353, 0.12309690, 0.12241688, 0.12132329, 0.11967100, 0.11724347, 0.11385228},
  {0.10969035, 0.10947782, 0.10914819, 0.10859574, 0.10778889, 0.10657109, 0.10464593}
};

// Case B probability of emitting a Paschen-beta photon per recombination (T,n_e)
static const double P_Pab_SH95[10][7] = {
  {0.07348988, 0.07341128, 0.07269308, 0.07070782, 0.06718798, 0.06260883, 0.05946774},
  {0.07078253, 0.07056248, 0.06978101, 0.06816808, 0.06522349, 0.06126369, 0.05828991},
  {0.06277961, 0.06253897, 0.06202437, 0.06098018, 0.05916578, 0.05653085, 0.05421758},
  {0.05773433, 0.05755307, 0.05717604, 0.05642753, 0.05512529, 0.05315164, 0.05131155},
  {0.05341396, 0.05329313, 0.05301332, 0.05249053, 0.05153516, 0.05006967, 0.04860267},
  {0.05031083, 0.05020266, 0.04998706, 0.04959691, 0.04886864, 0.04771556, 0.04653578},
  {0.04793166, 0.04783464, 0.04770007, 0.04736567, 0.04679777, 0.04583497, 0.04489711},
  {0.04599371, 0.04593356, 0.04581345, 0.04557400, 0.04509822, 0.04428626, 0.04348421},
  {0.04311101, 0.04305376, 0.04296500, 0.04279237, 0.04247102, 0.04187832, 0.04129479},
  {0.03928830, 0.03926736, 0.03921900, 0.03913283, 0.03896478, 0.03862739, 0.03828750}
};

// Case B probability of emitting a Brackett-alpha photon per recombination (T,n_e)
static const double P_Bra_SH95[10][7] = {
  {0.21885188, 0.20451948, 0.18492120, 0.16070099, 0.13417765, 0.10823662, 0.08695037},
  {0.18244521, 0.17373440, 0.16114577, 0.14461364, 0.12492214, 0.10407263, 0.08532126},
  {0.12619750, 0.12290608, 0.11783853, 0.11051795, 0.10092464, 0.08938717, 0.07733933},
  {0.10361101, 0.10162800, 0.09858645, 0.09407032, 0.08785312, 0.08005556, 0.07131783},
  {0.08789567, 0.08670423, 0.08470428, 0.08177624, 0.07762419, 0.07217453, 0.06584045},
  {0.07813115, 0.07725277, 0.07583222, 0.07370886, 0.07067981, 0.06661949, 0.06171954},
  {0.07127436, 0.07057650, 0.06956207, 0.06791772, 0.06561596, 0.06240387, 0.05852338},
  {0.06610734, 0.06560520, 0.06477995, 0.06351096, 0.06166134, 0.05908966, 0.05589889},
  {0.05886696, 0.05851187, 0.05795768, 0.05712213, 0.05588395, 0.05410592, 0.05185238},
  {0.05017424, 0.04999957, 0.04973270, 0.04930727, 0.04868878, 0.04776841, 0.04652094}
};

/* Interpolated probability for conversion to line photons. */
static double P_B(const double (&Ptab)[10][7], const double T, const double n_e) {
  const double T_rec = (T > T_floor_rec) ? T : T_floor_rec; // Temperature floor
  int i_L_T = 8, i_L_n = 0;                  // Lower interpolation index
  double frac_R_T = 1., frac_R_n = 0.;       // Upper interpolation fraction
  if (T_rec <= 5e2) {                        // Temperature minimum = 500 K
    i_L_T = 0;
    frac_R_T = 0.;
  } else if (T_rec < 3e4) {                  // Temperature maximum = 30,000 K
    const double logT = log10(T_rec);        // Interpolate in log space
    while (logT < logT_SH95[i_L_T])
      i_L_T--;                               // Search temperature indices
    frac_R_T = (logT - logT_SH95[i_L_T]) / (logT_SH95[i_L_T+1] - logT_SH95[i_L_T]);
  }
  if (n_e >= 1e8) {                          // Density maximum = 10^8 cm^-3
    i_L_n = 5;
    frac_R_n = 1.;
  } else if (n_e > 1e2) {                    // Density minimum = 10^2 cm^-3
    const double d_logn = log10(n_e) - 2.;   // Table coordinates (log n_e)
    const double f_logn = floor(d_logn);     // Floor coordinate
    frac_R_n = d_logn - f_logn;              // Interpolation fraction
    i_L_n = int(f_logn);                     // Lower table index
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_T = i_L_T + 1, i_R_n = i_L_n + 1;
  const double frac_L_T = 1. - frac_R_T, frac_L_n = 1. - frac_R_n;
  return frac_L_T * (Ptab[i_L_T][i_L_n]*frac_L_n + Ptab[i_L_T][i_R_n]*frac_R_n)
       + frac_R_T * (Ptab[i_R_T][i_L_n]*frac_L_n + Ptab[i_R_T][i_R_n]*frac_R_n);
}

/* Lyman-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Lya(const double T, const double n_e) {
  return P_B(P_Lya_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Ha(const double T, const double n_e) {
  return P_B(P_Ha_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-beta effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Hb(const double T, const double n_e) {
  return P_B(P_Hb_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-gamma effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Hg(const double T, const double n_e) {
  return P_B(P_Hg_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-delta effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Hd(const double T, const double n_e) {
  return P_B(P_Hd_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Balmer-epsilon effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_He(const double T, const double n_e) {
  return P_B(P_He_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Paschen-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Paa(const double T, const double n_e) {
  return P_B(P_Paa_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Paschen-beta effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Pab(const double T, const double n_e) {
  return P_B(P_Pab_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* Brackett-alpha effective recombination coefficient (Case B) - Units: cm^3/s */
static double alpha_eff_B_Bra(const double T, const double n_e) {
  return P_B(P_Bra_SH95, T, n_e) * alpha_B_HII(T); // α_eff_B = P_B α_B
}

/* PEQ 1991 effective probability for conversion to line photons. */
static inline double P_PEQ91(const double T) {
  const double T_rec = (T > T_floor_rec) ? T : T_floor_rec; // Temperature floor
  const double t = fmax(T_PEQ91 * T_rec, 0.004); // Avoid extrapolation
  return A_PEQ91 * pow(t, B_PEQ91) * (1. + c_PEQ91_tot * pow(t, d_PEQ91_tot)) / (1. + c_PEQ91 * pow(t, d_PEQ91));
  // This is the ratio of alpha_eff / alpha_tot where each are given by:
  // t = fmax(1e-4 * T / (z * z), 0.004) with z = ionic numeral (e.g. III = 3)
  // alpha = 1e-13 * Br * z * a * pow(t, b) / (1 + c * pow(t, d))
}

/* PEQ 1991 effective recombination coefficient - Units: cm^3/s */
static inline double alpha_eff_PEQ91(const double T, [[maybe_unused]] const double n_e) {
  return P_PEQ91(T) * alpha_tot(T);          // Recombination rate translation
}

// static const double // Table 3 from Scholz & Walters (1991)
//   c0L = -1.630155e2,   c0M =  5.279996e2,   c0H = -2.8133632e3,
//   c1L =  8.795711e1,   c1M = -1.939399e2,   c1H =  8.1509685e2,
//   c2L = -2.057117e1,   c2M =  2.718982e1,   c2H = -9.4418414e1,
//   c3L =  2.359573,     c3M = -1.883399,     c3H =  5.4280565,
//   c4L = -1.339059e-1,  c4M =  6.462462e-2,  c4H = -1.546712e-1,
//   c5L =  3.021507e-3,  c5M = -8.811076e-4,  c5H =  1.7439112e-3;
// /* Lyman-alpha collisional excitation rate coefficient - Units: cm^3/s */
// static double q_col_Lya(const double T) {
//   // q_1s2p = Equation 17 from Scholz & Walters (1991) (divided by hν)
//   const double y = log(T);
//   if (T < 6e4)                                // Low temperature range
//     return exp(-T0/T + c0L + y * (c1L + y * (c2L + y * (c3L + y * (c4L + y * c5L)))));
//   else if (T > 6e6)                           // High temperature range
//     return exp(-T0/T + c0H + y * (c1H + y * (c2H + y * (c3H + y * (c4H + y * c5H)))));
//   else                                       // Mid temperature range
//     return exp(-T0/T + c0M + y * (c1M + y * (c2M + y * (c3M + y * (c4M + y * c5M)))));
// }

// /* Balmer-alpha collisional excitation rate coefficient - Units: cm^3/s */
// static double q_col_Ha(const double T) {
//   // Hα emission based on the fitting formula from Aggarwal (1983)
//   // Collisional excitation (1->3) followed by radiative de-excitation (3->2)
//   double Gamma_13; // Maxwellian-averaged effective collision strength
//   if (T < 4e3 || T > 5e5)
//     return 0.;     // Likely an unphysical range for collisional excitation
//   else if (T < 2.5e4)
//     Gamma_13 = 0.35 - T * (2.62e-7 + T * (8.15e-11 - 6.19e-15 * T));
//   else
//     Gamma_13 = 0.276 + T * (4.99e-6 - T * (8.85e-12 - 7.18e-18 * T));
//   return q_coef * Gamma_13 / sqrt(T) * exp(-T_13/T);
// }

/* Neutral hydrogen collisional excitation cooling coefficient - Units: erg cm^3/s */
static double e_col_HI(const double T) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (6.107169036302359e-11 * exp(-T_12/T) + 1.5693136239683913e-11 * exp(-T_13/T)
      + 6.665146531965045e-12 * exp(-T_14/T) + 3.4378000369213142e-12 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( E_12 * (0.616414 + T6 * (16.8152 + T6 * (-32.0571 + 35.5428 * T6))) * exp(-T_12/T)
         + E_13 * (0.217382 + T6 * (3.92604 + T6 * (-10.6349 + 13.7721 * T6))) * exp(-T_13/T)
         + E_14 * (0.0959324 + T6 * (1.89951 + T6 * (-6.96467 + 10.6362 * T6))) * exp(-T_14/T)
         + E_15 * (0.0747075 + T6 * (0.670939 + T6 * (-2.28512 + 3.4796 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[E_ij G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Lyman-alpha collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Lya(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (3.2642689 * exp(-T_12/T) + 0.23382237 * exp(-T_13/T)
      + 0.13772838 * exp(-T_14/T) + 0.07453572 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.348928 + T6 * (15.2426 + T6 * (-25.6168 + 24.0027 * T6))) * exp(-T_12/T)
         + (0.1233 + T6 * (1.01857 + T6 * (-3.93958 + 5.90791 * T6))) * exp(-T_13/T)
         + (0.0544296 + T6 * (0.762786 + T6 * (-2.99013 + 4.57684 * T6))) * exp(-T_14/T)
         + (0.0424305 + T6 * (0.262379 + T6 * (-0.939078 + 1.40402 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Balmer-alpha collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Ha(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (0.8098997 * exp(-T_13/T) + 0.09820212 * exp(-T_14/T) + 0.052600338 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.217382 + T6 * (3.92604 + T6 * (-10.6349 + 13.7721 * T6))) * exp(-T_13/T)
         + (0.0372036 + T6 * (0.508162 + T6 * (-1.86495 + 2.82946 * T6))) * exp(-T_14/T)
         + (0.029568 + T6 * (0.165593 + T6 * (-0.52784 + 0.772594 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Balmer-beta collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Hb(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (0.22794233 * exp(-T_14/T) + 0.016138328 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.0587288 + T6 * (1.39135 + T6 * (-5.09972 + 7.80679 * T6))) * exp(-T_14/T)
         + (0.00844856 + T6 * (0.0682112 + T6 * (-0.277137 + 0.450694 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Balmer-gamma collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Hg(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return 0.09553887 * exp(-T_15/T) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return (0.0366909 + T6 * (0.437134 + T6 * (-1.48014 + 2.25631 * T6))) * exp(-T_15/T)
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Paschen-alpha collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Paa(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return (0.09820212 * exp(-T_14/T) + 0.008669065 * exp(-T_15/T)) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return ( (0.0372036 + T6 * (0.508162 + T6 * (-1.86495 + 2.82946 * T6))) * exp(-T_14/T)
         + (0.00825154 + T6 * (-0.0217447 + T6 * (0.177446 - 0.334415 * T6))) * exp(-T_15/T) )
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Paschen-beta collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Pab(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return 0.04393133 * exp(-T_15/T) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return (0.0213164 + T6 * (0.187338 + T6 * (-0.705286 + 1.10701 * T6))) * exp(-T_15/T)
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* Brackett-alpha collisional excitation rate coefficient - Units: cm^3/s */
static double q_col_Bra(const double T, [[maybe_unused]] const double n_e) {
  // Based on Maxwellian-averaged effective collision strengths (N <= 5)
  if (T >= 3e5) // Physical range for collisional excitation (0.5 - 25 eV)
    return 0.024807456 * exp(-T_15/T) * q_coef / sqrt(T);
  const double T6 = 1e-6 * T;                // T6 = T / (10^6 K)
  return (0.0167001 + T6 * (0.0464665 + T6 * (-0.0996903 + 0.116279 * T6))) * exp(-T_15/T)
         * q_coef / sqrt(T);                 // 4.3e-6 sum[G_ij exp(-T_ij/T)] / sqrt(T)
}

/* General setup for cameras. */
void Simulation::setup_cameras() {
  Vec3 x_axis, y_axis;                       // Image orientation axes
  for (auto& dir : camera_directions) {
    dir.normalize();                         // Normalize camera directions
    const double mu_north = dir.dot(camera_north); // North angular cosine
    if (1. - fabs(mu_north) > 1e-9) {        // Use north to build basis
      y_axis = camera_north - dir * mu_north; // Orthogonal projection
    } else if (mu_north > 0.) {              // Degenerate case (+north)
      // Use the rotated y-axis as the new north vector
      const double CZ = camera_north.y / (1. + camera_north.z);
      Vec3 new_north = {-camera_north.x * CZ, 1. - camera_north.y * CZ, -camera_north.y};
      const double new_mu_north = dir.dot(new_north);
      y_axis = new_north - dir * new_mu_north; // Orthogonal projection (+y)
    } else {                                 // Degenerate case (-north)
      // Use the rotated x-axis as the new north vector
      const double CZ = camera_north.x / (1. + camera_north.z);
      Vec3 new_north = {1. - camera_north.x * CZ, -camera_north.y * CZ, -camera_north.x};
      const double new_mu_north = dir.dot(new_north);
      y_axis = new_north - dir * new_mu_north; // Orthogonal projection (+x)
    }
    y_axis.normalize();                      // Make it a unit vector
    x_axis = y_axis.cross(dir);              // Final orthogonal axis
    x_axis.normalize();                      // Ensure normalization
    camera_xaxes.push_back(x_axis);          // Populate x-axes
    camera_yaxes.push_back(y_axis);          // Populate y-axes
  }

  // Halo center and virial radius
  if (!(group_file.empty() && subhalo_file.empty()) && halo_num >= 0)
    camera_center = halo_position;           // Halo position

  // Camera center
  const double min_bbox_dist = min_bbox_distance(camera_center); // Min bbox distance
  const double max_bbox_dist = max_bbox_distance(camera_center); // Max bbox distance
  if (min_bbox_dist < 0.)
    root_error("camera_center " + to_string(camera_center) +
          " is outside the bounding box [" + to_string(bbox[0]) +
          ", " + to_string(bbox[1]) + "]");  // Validate escape_center range

  // Consistency checks for image width and number of pixels
  if (image_radii_bbox > 0.)
    image_radii = min_bbox_dist * image_radii_bbox; // Set based on distance to bbox
  if (image_radii_Rvir > 0.)
    image_radii = halo_radius * image_radii_Rvir; // Set based on virial radius
  if (image_radii >= 0.)
    image_widths = 2. * image_radii;         // Set image widths from radii
  if (image_widths_cMpc > 0.)
    image_widths = image_widths_cMpc * Mpc / (1. + z); // Image widths from comoving Mpc
  if (image_widths <= 0.)
    image_widths = 2. * max_bbox_dist;       // Default to capture the entire domain
  if (image_widths > 1e3 * max_bbox_width)
    image_widths = 1e3 * max_bbox_width;     // Avoid very large images

  // Consistency checks for slit width and number of slit pixels
  if (slit_radius_bbox > 0.)
    slit_radius = min_bbox_dist * slit_radius_bbox; // Set based on distance to bbox
  if (slit_radius_Rvir > 0.)
    slit_radius = halo_radius * slit_radius_Rvir; // Set based on virial radius
  if (slit_radius >= 0.)
    slit_width = 2. * slit_radius;           // Set slit width from radius
  if (slit_width_cMpc > 0.)
    slit_width = slit_width_cMpc * Mpc / (1. + z); // Slit width from comoving Mpc
  if (slit_width <= 0.)
    slit_width = 2. * max_bbox_dist;         // Default to capture the entire domain
  if (slit_width > 1e3 * max_bbox_width)
    slit_width = 1e3 * max_bbox_width;       // Avoid very large slits
  if (slit_aperture_arcsec > 0.)
    slit_aperture = slit_aperture_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Aperture width from arcseconds
  if (slit_aperture <= 0.)
    slit_aperture = 0.2 * slit_width;        // Default to the slit width

  // Consistency checks for cube width and number of cube pixels
  if (cube_radii_bbox > 0.)
    cube_radii = min_bbox_dist * cube_radii_bbox; // Set based on distance to bbox
  if (cube_radii_Rvir > 0.)
    cube_radii = halo_radius * cube_radii_Rvir; // Set based on virial radius
  if (cube_radii >= 0.)
    cube_widths = 2. * cube_radii;           // Set cube widths from radii
  if (cube_widths_cMpc > 0.)
    cube_widths = cube_widths_cMpc * Mpc / (1. + z); // Cube widths from comoving Mpc
  if (cube_widths <= 0.)
    cube_widths = 2. * max_bbox_dist;        // Default to capture the entire domain
  if (cube_widths > 1e3 * max_bbox_width)
    cube_widths = 1e3 * max_bbox_width;      // Avoid very large cubes

  // Consistency checks for aperture radius
  if (aperture_radius_bbox > 0.)
    aperture_radius = min_bbox_dist * aperture_radius_bbox; // Set based on distance to bbox
  if (aperture_radius_Rvir > 0.)
    aperture_radius = halo_radius * aperture_radius_Rvir; // Set based on virial radius

  // Set the pixel widths from pixel_arcsec or number of pixels
  if (pixel_arcsecs > 0.)
    pixel_widths = pixel_arcsecs * d_L / (pow(1. + z, 2) * arcsec); // Pixel widths from arcseconds
  if (pixel_widths > 0.) {
    nx_pixels = sane_int(image_widths.x, pixel_widths.x); // Number of full pixels that can fit
    ny_pixels = sane_int(image_widths.x, pixel_widths.y);
    n_pixels = {nx_pixels, ny_pixels};       // Set the number of pixels
    image_widths = n_pixels * pixel_widths;  // Update the widths
  } else {
    n_pixels = {nx_pixels, ny_pixels};       // Set the number of pixels
    pixel_widths = image_widths / n_pixels;  // Pixel widths
    if (cosmological)
      pixel_arcsecs = pixel_widths * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a pixel
  }

  // Set the slit pixel widths from slit_pixel_arcsec or number of slit pixels
  if (slit_pixel_arcsec > 0.)
    slit_pixel_width = slit_pixel_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Pixel width from arcseconds
  if (slit_pixel_width > 0.) {
    n_slit_pixels = sane_int(slit_width, slit_pixel_width); // Number of full pixels that can fit
    slit_width = n_slit_pixels * slit_pixel_width; // Update the width
  } else {
    slit_pixel_width = slit_width / double(n_slit_pixels); // Pixel widths
    if (cosmological)
      slit_pixel_arcsec = slit_pixel_width * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a pixel
  }
  if (cosmological && slit_aperture_arcsec <= 0.)
    slit_aperture_arcsec = slit_aperture * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for an aperture

  // Set the cube pixel widths from cube_pixel_arcsec or number of cube pixels
  if (cube_pixel_arcsecs > 0.)
    cube_pixel_widths = cube_pixel_arcsecs * d_L / (pow(1. + z, 2) * arcsec); // Pixel widths from arcseconds
  if (cube_pixel_widths > 0.) {
    nx_cube_pixels = sane_int(cube_widths.x, cube_pixel_widths.x); // Number of full pixels that can fit
    ny_cube_pixels = sane_int(cube_widths.x, cube_pixel_widths.y);
    n_cube_pixels = {nx_cube_pixels, ny_cube_pixels}; // Set the number of pixels
    cube_widths = n_cube_pixels * cube_pixel_widths; // Update the widths
  } else {
    n_cube_pixels = {nx_cube_pixels, ny_cube_pixels}; // Set the number of pixels
    cube_pixel_widths = cube_widths / n_cube_pixels; // Pixel widths
    if (cosmological)
      cube_pixel_arcsecs = cube_pixel_widths * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a pixel
  }

  // Set redundant image parameters
  image_radii = 0.5 * image_widths;          // Set radius based on width
  inverse_pixel_widths = 1. / pixel_widths;  // Inverse pixel widths
  pixel_offsets = image_radii / pixel_widths; // Image radii / pixel widths
  pixel_area = pixel_widths.x * pixel_widths.y; // Pixel area [cm^2]
  if (cosmological)
    pixel_arcsec2 = pixel_arcsecs.x * pixel_arcsecs.y; // Set pixel arcsec^2
  if (aperture_radius > 0.)
    aperture_radius2 = aperture_radius * aperture_radius; // Radius^2 [cm^2]
  else
    aperture_radius2 = positive_infinity;    // Include every photon

  // Set redundant slit parameters
  slit_half_aperture = 0.5 * slit_aperture;  // Half aperture width
  slit_radius = 0.5 * slit_width;            // Set radius based on width
  inverse_slit_pixel_width = 1. / slit_pixel_width; // Inverse pixel width
  slit_pixel_offset = slit_radius / slit_pixel_width; // Slit radius / pixel width
  slit_pixel_area = slit_pixel_width * slit_aperture; // Pixel area [cm^2]
  if (cosmological)
    slit_pixel_arcsec2 = slit_aperture_arcsec * slit_pixel_arcsec; // Set pixel arcsec^2

  // Set redundant cube parameters
  cube_radii = 0.5 * cube_widths;            // Set radius based on width
  inverse_cube_pixel_widths = 1. / cube_pixel_widths; // Inverse pixel widths
  cube_pixel_offsets = cube_radii / cube_pixel_widths; // Cube radii / pixel widths
  cube_pixel_area = cube_pixel_widths.x * cube_pixel_widths.y; // Pixel area [cm^2]
  if (cosmological)
    cube_pixel_arcsec2 = cube_pixel_arcsecs.x * cube_pixel_arcsecs.y; // Set pixel arcsec^2

  // Calculate image edge coordinates
  if (module == "projections" || output_proj_emission || output_proj_attenuation) {
    image_xedges = linspace(-image_radii.x, image_radii.x, nx_pixels+1);
    image_yedges = linspace(-image_radii.y, image_radii.y, ny_pixels+1);
  }

  // Calculate slit edge coordinates
  // slit_edges = linspace(-slit_radius, slit_radius, n_slit_pixels+1);

  // Calculate cube edge coordinates
  if (output_proj_cube_emission || output_proj_cube_attenuation) {
    cube_xedges = linspace(-cube_radii.x, cube_radii.x, nx_cube_pixels+1);
    cube_yedges = linspace(-cube_radii.y, cube_radii.y, ny_cube_pixels+1);
  }

  if (output_radial_images) {
    // Consistency checks for radial image radius and number of radial pixels
    if (radial_image_radius_bbox > 0.)
      radial_image_radius = min_bbox_dist * radial_image_radius_bbox; // Set based on distance to bbox
    if (radial_image_radius_Rvir > 0.)
      radial_image_radius = halo_radius * radial_image_radius_Rvir; // Set based on virial radius
    if (radial_image_radius <= 0.)
      radial_image_radius = max_bbox_dist;   // Capture the entire domain
    if (radial_image_radius > 1e3 * max_bbox_width)
      radial_image_radius = 1e3 * max_bbox_width; // Avoid very large images

    // Set the radial pixel width from radial_pixel_arcsec or n_radial_pixels
    if (radial_pixel_arcsec > 0.)
      radial_pixel_width = radial_pixel_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (radial_pixel_width > 0.) {
      n_radial_pixels = sane_int(radial_image_radius, radial_pixel_width); // Number of full radial pixels that can fit
      radial_image_radius = double(n_radial_pixels) * radial_pixel_width; // Update the radial width
    } else {
      radial_pixel_width = radial_image_radius / double(n_radial_pixels); // Radial pixel width
      if (cosmological)
        radial_pixel_arcsec = radial_pixel_width * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial pixel
    }

    // Set redundant radial image parameters
    inverse_radial_pixel_width = 1. / radial_pixel_width; // Inverse radial pixel width
    radial_pixel_areas = vector<double>(n_radial_pixels); // Radial pixel areas [cm^2]
    const double pi_RPW2 = M_PI * radial_pixel_width * radial_pixel_width;
    #pragma omp parallel for
    for (int i = 0; i < n_radial_pixels; ++i) {
      // Note: r_inner = i RPW; r_outer = (i+1) RPW; area = pi [(i+1)^2 - i^2] RPW^2 = pi (1+2i) RPW^2
      radial_pixel_areas[i] = double(1 + 2*i) * pi_RPW2;
    }
    if (cosmological) {
      radial_pixel_arcsec2 = vector<double>(n_radial_pixels); // Radial pixel areas [arcsec^2]
      const double pi_RPA2 = M_PI * radial_pixel_arcsec * radial_pixel_arcsec;
      #pragma omp parallel for
      for (int i = 0; i < n_radial_pixels; ++i) {
        // Note: r_inner = i RPA; r_outer = (i+1) RPA; area = pi [(i+1)^2 - i^2] RPA^2 = pi (1+2i) RPA^2
        radial_pixel_arcsec2[i] = double(1 + 2*i) * pi_RPA2;
      }
    }
  }

  if (output_radial_cubes) {
    // Consistency checks for radial cube radius and number of radial cube pixels
    if (radial_cube_radius_bbox > 0.)
      radial_cube_radius = min_bbox_dist * radial_cube_radius_bbox; // Set based on distance to bbox
    if (radial_cube_radius_Rvir > 0.)
      radial_cube_radius = halo_radius * radial_cube_radius_Rvir; // Set based on virial radius
    if (radial_cube_radius <= 0.)
      radial_cube_radius = max_bbox_dist;    // Capture the entire domain
    if (radial_cube_radius > 1e3 * max_bbox_width)
      radial_cube_radius = 1e3 * max_bbox_width; // Avoid very large cubes

    // Set the radial cube pixel width from radial_cube_pixel_arcsec or n_radial_cube_pixels
    if (radial_cube_pixel_arcsec > 0.)
      radial_cube_pixel_width = radial_cube_pixel_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (radial_cube_pixel_width > 0.) {
      n_radial_cube_pixels = sane_int(radial_cube_radius, radial_cube_pixel_width); // Number of full radial cube pixels that can fit
      radial_cube_radius = double(n_radial_cube_pixels) * radial_cube_pixel_width; // Update the radial cube width
    } else {
      radial_cube_pixel_width = radial_cube_radius / double(n_radial_cube_pixels); // Radial cube pixel width
      if (cosmological)
        radial_cube_pixel_arcsec = radial_cube_pixel_width * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial cube pixel
    }

    // Set redundant radial cube parameters
    inverse_radial_cube_pixel_width = 1. / radial_cube_pixel_width; // Inverse radial pixel width
    radial_cube_pixel_areas = vector<double>(n_radial_cube_pixels); // Radial cube pixel areas [cm^2]
    const double pi_RCPW2 = M_PI * radial_cube_pixel_width * radial_cube_pixel_width;
    #pragma omp parallel for
    for (int i = 0; i < n_radial_cube_pixels; ++i) {
      // Note: r_inner = i RCPW; r_outer = (i+1) RCPW; area = pi [(i+1)^2 - i^2] RCPW^2 = pi (1+2i) RCPW^2
      radial_cube_pixel_areas[i] = double(1 + 2*i) * pi_RCPW2;
    }
    if (cosmological) {
      radial_cube_pixel_arcsec2 = vector<double>(n_radial_cube_pixels); // Radial pixel areas [arcsec^2]
      const double pi_RCPA2 = M_PI * radial_cube_pixel_arcsec * radial_cube_pixel_arcsec;
      #pragma omp parallel for
      for (int i = 0; i < n_radial_cube_pixels; ++i) {
        // Note: r_inner = i RCPA; r_outer = (i+1) RCPA; area = pi [(i+1)^2 - i^2] RCPA^2 = pi (1+2i) RCPA^2
        radial_cube_pixel_arcsec2[i] = double(1 + 2*i) * pi_RCPA2;
      }
    }
  }

  if (output_radial_map) {
    // Consistency checks for radial map radius and number of radial map pixels
    if (map_radius_bbox > 0.)
      map_radius = min_bbox_dist * map_radius_bbox; // Set based on distance to bbox
    if (map_radius_Rvir > 0.)
      map_radius = halo_radius * map_radius_Rvir; // Set based on virial radius
    if (map_radius <= 0.)
      map_radius = max_bbox_dist;            // Capture the entire domain
    if (map_radius > 1e3 * max_bbox_width)
      map_radius = 1e3 * max_bbox_width;     // Avoid very large radii

    // Set the radial map pixel width from map_pixel_arcsec or n_map_pixels
    if (map_pixel_arcsec > 0.)
      map_pixel_width = map_pixel_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (map_pixel_width > 0.) {
      n_map_pixels = sane_int(map_radius, map_pixel_width); // Number of full radial map pixels that can fit
      map_radius = double(n_map_pixels) * map_pixel_width; // Update the radial map width
    } else {
      map_pixel_width = map_radius / double(n_map_pixels); // Radial map pixel width
      if (cosmological)
        map_pixel_arcsec = map_pixel_width * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial map pixel
    }

    // Set redundant radial map parameters
    inverse_map_pixel_width = 1. / map_pixel_width; // Inverse radial pixel width
    map_pixel_areas = vector<double>(n_map_pixels); // Radial map pixel areas [cm^2]
    const double pi_RMPW2 = M_PI * map_pixel_width * map_pixel_width;
    #pragma omp parallel for
    for (int i = 0; i < n_map_pixels; ++i) {
      // Note: r_inner = i RMPW; r_outer = (i+1) RMPW; area = pi [(i+1)^2 - i^2] RMPW^2 = pi (1+2i) RCPW^2
      map_pixel_areas[i] = double(1 + 2*i) * pi_RMPW2;
    }
    if (cosmological) {
      map_pixel_arcsec2 = vector<double>(n_map_pixels); // Radial pixel areas [arcsec^2]
      const double pi_RMPA2 = M_PI * map_pixel_arcsec * map_pixel_arcsec;
      #pragma omp parallel for
      for (int i = 0; i < n_map_pixels; ++i) {
        // Note: r_inner = i RMPA; r_outer = (i+1) RMPA; area = pi [(i+1)^2 - i^2] RMPA^2 = pi (1+2i) RMPA^2
        map_pixel_arcsec2[i] = double(1 + 2*i) * pi_RMPA2;
      }
    }
  }

  if (output_radial_map_grp) {
    // Consistency checks for radial map radius and number of radial map pixels
    if (map_radius_bbox_grp > 0.)
      map_radius_grp = min_bbox_dist * map_radius_bbox_grp; // Set based on distance to bbox
    if (map_radius_grp <= 0.)
      map_radius_grp = max_bbox_dist;        // Capture the entire domain
    if (map_radius_grp > 1e3 * max_bbox_width)
      map_radius_grp = 1e3 * max_bbox_width; // Avoid very large radii

    // Set the radial map pixel width from map_pixel_arcsec_grp or n_map_pixels_grp
    if (map_pixel_arcsec_grp > 0.)
      map_pixel_width_grp = map_pixel_arcsec_grp * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (map_pixel_width_grp > 0.) {
      n_map_pixels_grp = sane_int(map_radius_grp, map_pixel_width_grp); // Number of full radial map pixels that can fit
      map_radius_grp = double(n_map_pixels_grp) * map_pixel_width_grp; // Update the radial map width
    } else {
      map_pixel_width_grp = map_radius_grp / double(n_map_pixels_grp); // Radial map pixel width
      if (cosmological)
        map_pixel_arcsec_grp = map_pixel_width_grp * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial map pixel
    }
    if (min_map_radius_grp > 0.)
      map_pixel_width_grp = (log10(map_radius_grp) - log_min_map_radius_grp) / double(n_map_pixels_grp - 1); // Radial map pixel width [dex]

    // Set redundant radial map parameters
    inverse_map_pixel_width_grp = 1. / map_pixel_width_grp; // Inverse radial pixel width
    map_pixel_areas_grp = vector<double>(n_map_pixels_grp); // Radial map pixel areas [cm^2]
    if (min_map_radius_grp > 0.) {
      map_pixel_areas_grp[0] = M_PI * min_map_radius_grp * min_map_radius_grp;
      #pragma omp parallel for
      for (int i = 1; i < n_map_pixels_grp; ++i) {
        const double r_inner = pow(10., double(i - 1) * map_pixel_width_grp + log_min_map_radius_grp);
        const double r_outer = pow(10., double(i) * map_pixel_width_grp + log_min_map_radius_grp);
        map_pixel_areas_grp[i] = M_PI * (r_outer * r_outer - r_inner * r_inner);
      }
    } else {
      // Note: r_inner = i RMPW; r_outer = (i+1) RMPW; area = pi [(i+1)^2 - i^2] RMPW^2 = pi (1+2i) RCPW^2
      const double pi_RMPW2 = M_PI * map_pixel_width_grp * map_pixel_width_grp;
      #pragma omp parallel for
      for (int i = 0; i < n_map_pixels_grp; ++i)
        map_pixel_areas_grp[i] = double(1 + 2*i) * pi_RMPW2;
    }
    if (cosmological) {
      map_pixel_arcsec2_grp = vector<double>(n_map_pixels_grp); // Radial pixel areas [arcsec^2]
      const double cm_to_arcsec = arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial map pixel
      const double cm2_to_arcsec2 = cm_to_arcsec * cm_to_arcsec; // Convert cm^2 to arcsec^2
      #pragma omp parallel for
      for (int i = 0; i < n_map_pixels_grp; ++i)
        map_pixel_arcsec2_grp[i] = cm2_to_arcsec2 * map_pixel_areas_grp[i]; // Convert cm^2 to arcsec^2
    }
  }

  if (output_radial_avg_grp) {
    // Consistency checks for radial avg radius and number of radial avg pixels
    if (radius_bbox_grp > 0.)
      radius_grp = min_bbox_dist * radius_bbox_grp; // Set based on distance to bbox
    if (radius_grp <= 0.)
      radius_grp = max_bbox_dist;            // Capture the entire domain
    if (radius_grp > 1e3 * max_bbox_width)
      radius_grp = 1e3 * max_bbox_width;     // Avoid very large radii

    // Set the radial avg pixel width from pixel_arcsec_grp or n_pixels_grp
    if (pixel_arcsec_grp > 0.)
      pixel_width_grp = pixel_arcsec_grp * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (pixel_width_grp > 0.) {
      n_pixels_grp = sane_int(radius_grp, pixel_width_grp); // Number of full radial avg pixels that can fit
      radius_grp = double(n_pixels_grp) * pixel_width_grp; // Update the radial avg width
    } else {
      pixel_width_grp = radius_grp / double(n_pixels_grp); // Radial avg pixel width
      if (cosmological)
        pixel_arcsec_grp = pixel_width_grp * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial avg pixel
    }
    if (min_radius_grp > 0.)
      pixel_width_grp = (log10(radius_grp) - log_min_radius_grp) / double(n_pixels_grp - 1); // Radial avg pixel width [dex]

    // Set redundant radial avg parameters
    inverse_pixel_width_grp = 1. / pixel_width_grp; // Inverse radial pixel width
    pixel_areas_grp = vector<double>(n_pixels_grp); // Radial avg pixel areas [cm^2]
    if (min_radius_grp > 0.) {
      pixel_areas_grp[0] = M_PI * min_radius_grp * min_radius_grp;
      #pragma omp parallel for
      for (int i = 1; i < n_pixels_grp; ++i) {
        const double r_inner = pow(10., double(i - 1) * pixel_width_grp + log_min_radius_grp);
        const double r_outer = pow(10., double(i) * pixel_width_grp + log_min_radius_grp);
        pixel_areas_grp[i] = M_PI * (r_outer * r_outer - r_inner * r_inner);
      }
    } else {
      // Note: r_inner = i RMPW; r_outer = (i+1) RMPW; area = pi [(i+1)^2 - i^2] RMPW^2 = pi (1+2i) RCPW^2
      const double pi_RMPW2 = M_PI * pixel_width_grp * pixel_width_grp;
      #pragma omp parallel for
      for (int i = 0; i < n_pixels_grp; ++i)
        pixel_areas_grp[i] = double(1 + 2*i) * pi_RMPW2;
    }
    if (cosmological) {
      pixel_arcsec2_grp = vector<double>(n_pixels_grp); // Radial pixel areas [arcsec^2]
      const double cm_to_arcsec = arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial avg pixel
      const double cm2_to_arcsec2 = cm_to_arcsec * cm_to_arcsec; // Convert cm^2 to arcsec^2
      #pragma omp parallel for
      for (int i = 0; i < n_pixels_grp; ++i)
        pixel_arcsec2_grp[i] = cm2_to_arcsec2 * pixel_areas_grp[i]; // Convert cm^2 to arcsec^2
    }
  }

  if (output_radial_map_sub) {
    // Consistency checks for radial map radius and number of radial map pixels
    if (map_radius_bbox_sub > 0.)
      map_radius_sub = min_bbox_dist * map_radius_bbox_sub; // Set based on distance to bbox
    if (map_radius_sub <= 0.)
      map_radius_sub = max_bbox_dist;        // Capture the entire domain
    if (map_radius_sub > 1e3 * max_bbox_width)
      map_radius_sub = 1e3 * max_bbox_width; // Avoid very large radii

    // Set the radial map pixel width from map_pixel_arcsec_sub or n_map_pixels_sub
    if (map_pixel_arcsec_sub > 0.)
      map_pixel_width_sub = map_pixel_arcsec_sub * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (map_pixel_width_sub > 0.) {
      n_map_pixels_sub = sane_int(map_radius_sub, map_pixel_width_sub); // Number of full radial map pixels that can fit
      map_radius_sub = double(n_map_pixels_sub) * map_pixel_width_sub; // Update the radial map width
    } else {
      map_pixel_width_sub = map_radius_sub / double(n_map_pixels_sub); // Radial map pixel width
      if (cosmological)
        map_pixel_arcsec_sub = map_pixel_width_sub * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial map pixel
    }
    if (min_map_radius_sub > 0.)
      map_pixel_width_sub = (log10(map_radius_sub) - log_min_map_radius_sub) / double(n_map_pixels_sub - 1); // Radial map pixel width [dex]

    // Set redundant radial map parameters
    inverse_map_pixel_width_sub = 1. / map_pixel_width_sub; // Inverse radial pixel width
    map_pixel_areas_sub = vector<double>(n_map_pixels_sub); // Radial map pixel areas [cm^2]
    if (min_map_radius_sub > 0.) {
      map_pixel_areas_sub[0] = M_PI * min_map_radius_sub * min_map_radius_sub;
      #pragma omp parallel for
      for (int i = 1; i < n_map_pixels_sub; ++i) {
        const double r_inner = pow(10., double(i - 1) * map_pixel_width_sub + log_min_map_radius_sub);
        const double r_outer = pow(10., double(i) * map_pixel_width_sub + log_min_map_radius_sub);
        map_pixel_areas_sub[i] = M_PI * (r_outer * r_outer - r_inner * r_inner);
      }
    } else {
      // Note: r_inner = i RMPW; r_outer = (i+1) RMPW; area = pi [(i+1)^2 - i^2] RMPW^2 = pi (1+2i) RCPW^2
      const double pi_RMPW2 = M_PI * map_pixel_width_sub * map_pixel_width_sub;
      #pragma omp parallel for
      for (int i = 0; i < n_map_pixels_sub; ++i)
        map_pixel_areas_sub[i] = double(1 + 2*i) * pi_RMPW2;
    }
    if (cosmological) {
      map_pixel_arcsec2_sub = vector<double>(n_map_pixels_sub); // Radial pixel areas [arcsec^2]
      const double cm_to_arcsec = arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial map pixel
      const double cm2_to_arcsec2 = cm_to_arcsec * cm_to_arcsec; // Convert cm^2 to arcsec^2
      #pragma omp parallel for
      for (int i = 0; i < n_map_pixels_sub; ++i)
        map_pixel_arcsec2_sub[i] = cm2_to_arcsec2 * map_pixel_areas_sub[i]; // Convert cm^2 to arcsec^2
    }
  }

  if (output_radial_avg_sub) {
    // Consistency checks for radial avg radius and number of radial avg pixels
    if (radius_bbox_sub > 0.)
      radius_sub = min_bbox_dist * radius_bbox_sub; // Set based on distance to bbox
    if (radius_sub <= 0.)
      radius_sub = max_bbox_dist;            // Capture the entire domain
    if (radius_sub > 1e3 * max_bbox_width)
      radius_sub = 1e3 * max_bbox_width;     // Avoid very large radii

    // Set the radial avg pixel width from pixel_arcsec_sub or n_pixels_sub
    if (pixel_arcsec_sub > 0.)
      pixel_width_sub = pixel_arcsec_sub * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (pixel_width_sub > 0.) {
      n_pixels_sub = sane_int(radius_sub, pixel_width_sub); // Number of full radial avg pixels that can fit
      radius_sub = double(n_pixels_sub) * pixel_width_sub; // Update the radial avg width
    } else {
      pixel_width_sub = radius_sub / double(n_pixels_sub); // Radial avg pixel width
      if (cosmological)
        pixel_arcsec_sub = pixel_width_sub * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial avg pixel
    }
    if (min_radius_sub > 0.)
      pixel_width_sub = (log10(radius_sub) - log_min_radius_sub) / double(n_pixels_sub - 1); // Radial avg pixel width [dex]

    // Set redundant radial avg parameters
    inverse_pixel_width_sub = 1. / pixel_width_sub; // Inverse radial pixel width
    pixel_areas_sub = vector<double>(n_pixels_sub); // Radial avg pixel areas [cm^2]
    if (min_radius_sub > 0.) {
      pixel_areas_sub[0] = M_PI * min_radius_sub * min_radius_sub;
      #pragma omp parallel for
      for (int i = 1; i < n_pixels_sub; ++i) {
        const double r_inner = pow(10., double(i - 1) * pixel_width_sub + log_min_radius_sub);
        const double r_outer = pow(10., double(i) * pixel_width_sub + log_min_radius_sub);
        pixel_areas_sub[i] = M_PI * (r_outer * r_outer - r_inner * r_inner);
      }
    } else {
      // Note: r_inner = i RMPW; r_outer = (i+1) RMPW; area = pi [(i+1)^2 - i^2] RMPW^2 = pi (1+2i) RCPW^2
      const double pi_RMPW2 = M_PI * pixel_width_sub * pixel_width_sub;
      #pragma omp parallel for
      for (int i = 0; i < n_pixels_sub; ++i)
        pixel_areas_sub[i] = double(1 + 2*i) * pi_RMPW2;
    }
    if (cosmological) {
      pixel_arcsec2_sub = vector<double>(n_pixels_sub); // Radial pixel areas [arcsec^2]
      const double cm_to_arcsec = arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial avg pixel
      const double cm2_to_arcsec2 = cm_to_arcsec * cm_to_arcsec; // Convert cm^2 to arcsec^2
      #pragma omp parallel for
      for (int i = 0; i < n_pixels_sub; ++i)
        pixel_arcsec2_sub[i] = cm2_to_arcsec2 * pixel_areas_sub[i]; // Convert cm^2 to arcsec^2
    }
  }

  if (output_cube_map) {
    // Consistency checks for radius and number of radial pixels
    if (cube_map_radius_bbox > 0.)
      cube_map_radius = min_bbox_dist * cube_map_radius_bbox; // Set based on distance to bbox
    if (cube_map_radius_Rvir > 0.)
      cube_map_radius = halo_radius * cube_map_radius_Rvir; // Set based on virial radius
    if (cube_map_radius <= 0.)
      cube_map_radius = max_bbox_dist;       // Capture the entire domain
    if (cube_map_radius > 1e3 * max_bbox_width)
      cube_map_radius = 1e3 * max_bbox_width; // Avoid very large radii

    // Set the radial pixel width from cube_map_pixel_arcsec or n_cube_map_pixels
    if (cube_map_pixel_arcsec > 0.)
      cube_map_pixel_width = cube_map_pixel_arcsec * d_L / (pow(1. + z, 2) * arcsec); // Radial pixel width from arcseconds
    if (cube_map_pixel_width > 0.) {
      n_cube_map_pixels = sane_int(cube_map_radius, cube_map_pixel_width); // Number of full radial pixels that can fit
      cube_map_radius = double(n_cube_map_pixels) * cube_map_pixel_width; // Update the radial width
    } else {
      cube_map_pixel_width = cube_map_radius / double(n_cube_map_pixels); // Radial pixel width
      if (cosmological)
        cube_map_pixel_arcsec = cube_map_pixel_width * arcsec * pow(1. + z, 2) / d_L; // Calculate arcseconds for a radial pixel
    }

    // Set redundant radial spectral map parameters
    inverse_cube_map_pixel_width = 1. / cube_map_pixel_width; // Inverse radial pixel width
    cube_map_pixel_areas = vector<double>(n_cube_map_pixels); // Radial pixel areas [cm^2]
    const double pi_CMPW2 = M_PI * cube_map_pixel_width * cube_map_pixel_width;
    #pragma omp parallel for
    for (int i = 0; i < n_cube_map_pixels; ++i) {
      // Note: r_inner = i CMPW; r_outer = (i+1) CMPW; area = pi [(i+1)^2 - i^2] CMPW^2 = pi (1+2i) CMPW^2
      cube_map_pixel_areas[i] = double(1 + 2*i) * pi_CMPW2;
    }
    if (cosmological) {
      cube_map_pixel_arcsec2 = vector<double>(n_cube_map_pixels); // Radial pixel areas [arcsec^2]
      const double pi_CMPA2 = M_PI * cube_map_pixel_arcsec * cube_map_pixel_arcsec;
      #pragma omp parallel for
      for (int i = 0; i < n_cube_map_pixels; ++i) {
        // Note: r_inner = i CMPA; r_outer = (i+1) CMPA; area = pi [(i+1)^2 - i^2] CMPA^2 = pi (1+2i) CMPA^2
        cube_map_pixel_arcsec2[i] = double(1 + 2*i) * pi_CMPA2;
      }
    }
  }
}

/* General setup for general escape. */
void Simulation::setup_escape() {
#if spherical_escape
  // Halo center
  if (!(group_file.empty() && subhalo_file.empty()) && halo_num >= 0)
    escape_center = halo_position;           // Halo position

  // Escape center
  const double min_bbox_dist = min_bbox_distance(escape_center); // Min bbox distance
  if (min_bbox_dist < 0.)
    root_error("escape_center " + to_string(escape_center) +
          " is outside the bounding box [" + to_string(bbox[0]) +
          ", " + to_string(bbox[1]) + "]");  // Validate escape_center range

  // Escape radius
  if (escape_radius_bbox > 0.)
    escape_radius = min_bbox_dist * escape_radius_bbox; // Set based on distance to bbox
  if (escape_radius_Rvir > 0.)
    escape_radius = halo_radius * escape_radius_Rvir; // Set based on virial radius
  if (escape_radius == 0.)
    escape_radius = min_bbox_dist;           // Min distance to bbox
  escape_epsilon = 1e-10 * escape_radius;    // Escape uncertainty [cm]
  escape_radius2 = escape_radius * escape_radius; // Escape radius^2 [cm^2]

  // Emission radius
  if (emission_radius_bbox > 0.)
    emission_radius = min_bbox_dist * emission_radius_bbox; // Set based on distance to bbox
  if (emission_radius_Rvir > 0.)
    emission_radius = halo_radius * emission_radius_Rvir; // Set based on virial radius
  if (emission_radius == 0. || emission_radius > escape_radius - escape_epsilon)
    emission_radius = escape_radius - escape_epsilon;
  emission_radius2 = emission_radius * emission_radius; // Emission radius^2 [cm^2]
  if (point_source) {
    const Vec3 dr_point = r_point - escape_center; // Position relative to center
    if (dr_point.dot() >= emission_radius2)
      root_error("Point source location must be within the spherical emission region");
  }
  if (sphere_cont_source) {
    const Vec3 dr_sphere = sphere_center_cont - escape_center; // Position relative to center
    if (dr_sphere.norm() + sphere_radius_cont >= emission_radius)
      root_error("Sphere continuum source must be within the spherical emission region");
  }
#endif

#if box_escape
  // Emission bounding box
  const Vec3 box_epsilon = (bbox[1] - bbox[0]) * 1e-10; // Box epsilon [cm]
  escape_box = {bbox[0] + box_epsilon, bbox[1] - box_epsilon}; // Escape box [cm]
  emission_box = {escape_box[0] + box_epsilon, escape_box[1] - box_epsilon}; // Emission box [cm]
#endif

#if streaming_escape
  // TODO: Implement streaming escape setup
#endif
}

/* Group and subhalo statistics. */
static inline void add_L_halo(vector<double>& L_halo, vector<double>& L2_halo, vector<double>& n_eff_halo, vector<double>& L_tot_halo) {
  const int n_halos = L_halo.size();         // Number of halos
  #pragma omp parallel for
  for (int i_halo = 0; i_halo < n_halos; ++i_halo) {
    const double L = L_halo[i_halo];         // Halo total rate [photons/s]
    const double L2 = L2_halo[i_halo];       // Halo total rate^2 [photons^2/s^2]
    n_eff_halo[i_halo] = calc_n_eff(L, L2);  // Effective number of sources: <L>^2/<L^2>
    L_tot_halo[i_halo] += L;                 // Global rate [photons/s]
  }
}

/* Setup everything for line radiative transfer. */
void MCRT::setup() {
  // Point source insertion at r = (x_point, y_point, z_point)
  if (point_source)
    i_point = find_cell(r_point, 0);         // Start with default cell = 0
  if (sphere_cont_source)
    i_sphere_cont = find_cell(sphere_center_cont, 0); // Start with default cell = 0

  // Setup frequency information
  // Make sure n_bins and freq_bin_width are compatible
  if (freq_bin_width > 0.)
    n_bins = sane_int(freq_max - freq_min, freq_bin_width);
  else
    freq_bin_width = (freq_max - freq_min) / double(n_bins); // Bin width
  inverse_freq_bin_width = double(n_bins) / (freq_max - freq_min); // Bin width^-1
  // Camera quantities are in units of redshifted wavelength [obs. angstrom]
  if (cosmological)                          // Convert from velocity units [km/s]
    observed_bin_width = freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength

  // Setup slit frequency information
  if (output_slits) {
    if (slit_freq_bin_width > 0.)
      n_slit_bins = sane_int(slit_freq_max - slit_freq_min, slit_freq_bin_width);
    else
      slit_freq_bin_width = (slit_freq_max - slit_freq_min) / double(n_slit_bins); // Slit bin width
    inverse_slit_freq_bin_width = double(n_slit_bins) / (slit_freq_max - slit_freq_min); // Slit bin width^-1
    // Camera quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_slit_bin_width = slit_freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength
  }

  // Setup cube frequency information
  if (output_cubes || output_proj_cube_emission || output_proj_cube_attenuation) {
    if (cube_freq_bin_width > 0.)
      n_cube_bins = sane_int(cube_freq_max - cube_freq_min, cube_freq_bin_width);
    else
      cube_freq_bin_width = (cube_freq_max - cube_freq_min) / double(n_cube_bins); // Cube bin width
    inverse_cube_freq_bin_width = double(n_cube_bins) / (cube_freq_max - cube_freq_min); // Cube bin width^-1
    // Camera quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_cube_bin_width = cube_freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength
  }

  // Setup radial cube frequency information
  if (output_radial_cubes) {
    if (radial_cube_freq_bin_width > 0.)
      n_radial_cube_bins = sane_int(radial_cube_freq_max - radial_cube_freq_min, radial_cube_freq_bin_width);
    else
      radial_cube_freq_bin_width = (radial_cube_freq_max - radial_cube_freq_min) / double(n_radial_cube_bins); // Radial cube bin width
    inverse_radial_cube_freq_bin_width = double(n_radial_cube_bins) / (radial_cube_freq_max - radial_cube_freq_min); // Radial cube bin width^-1
    // Camera quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_radial_cube_bin_width = radial_cube_freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength
  }

  // Setup map frequency information
  if (output_flux_map) {
    if (map_freq_bin_width > 0.)
      n_map_bins = sane_int(map_freq_max - map_freq_min, map_freq_bin_width);
    else
      map_freq_bin_width = (map_freq_max - map_freq_min) / double(n_map_bins); // Flux map bin width
    inverse_map_freq_bin_width = double(n_map_bins) / (map_freq_max - map_freq_min); // Flux map bin width^-1
    // Flux map quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_map_bin_width = map_freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength
  }

  // Setup radial spectral map frequency information
  if (output_cube_map) {
    if (cube_map_freq_bin_width > 0.)
      n_cube_map_bins = sane_int(cube_map_freq_max - cube_map_freq_min, cube_map_freq_bin_width);
    else
      cube_map_freq_bin_width = (cube_map_freq_max - cube_map_freq_min) / double(n_cube_map_bins); // Frequency bin width
    inverse_cube_map_freq_bin_width = double(n_cube_map_bins) / (cube_map_freq_max - cube_map_freq_min); // Frequency bin width^-1
    // Radial spectral map quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_cube_map_bin_width = cube_map_freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength
  }

  // Setup angular cosine (μ=cosθ) frequency information
  if (output_flux_mu) {
    if (mu_freq_bin_width > 0.)
      n_mu_bins = sane_int(mu_freq_max - mu_freq_min, mu_freq_bin_width);
    else
      mu_freq_bin_width = (mu_freq_max - mu_freq_min) / double(n_mu_bins); // Flux bin width
    inverse_mu_freq_bin_width = double(n_mu_bins) / (mu_freq_max - mu_freq_min); // Flux bin width^-1
    // Flux quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_mu_bin_width = mu_freq_bin_width * km * lambda0 * (1. + z) / c; // Wavelength
  }

  // Setup RGB frequency ranges
  if (output_rgb_images || output_rgb_radial_images || output_rgb_map)
    inverse_rgb_freq_bin_width = 256. / (rgb_freq_max - rgb_freq_min); // Bin width^-1

  // Setup continuum ranges
  if (continuum) {
    Dv_cont_res = Dv_cont_max - Dv_cont_min; // Velocity offset [km/s]
    l_cont_res = Dv_cont_res * km * lambda0 / c; // Wavelength [angstrom]
    R_cont_res = c / (Dv_cont_res * km);     // Spectral resolution
  }

  // Allocate group properties
  if constexpr (output_groups) {
    if (n_groups > 0) {                      // Have groups
      L_grp = vector<double>(n_groups);      // Group photon rates [photons/s]
      f_src_grp = vector<double>(n_groups);  // Group source fractions: sum(w0)
      f2_src_grp = vector<double>(n_groups); // Group squared source fractions: sum(w0^2)
      n_photons_src_grp = vector<double>(n_groups); // Effective number of emitted photons: 1/<w0>
      if (cell_based_emission) {
        L_gas_grp = vector<double>(n_groups); // Group gas photon rates [photons/s]
        L2_gas_grp = vector<double>(n_groups); // Group gas rates^2 [photons^2/s^2]
        n_cells_eff_grp = vector<double>(n_groups); // Effective number of cells: 1/<w>
      }
      if (star_based_emission) {
        L_stars_grp = vector<double>(n_groups); // Group stars photon rates [photons/s]
        L2_stars_grp = vector<double>(n_groups); // Group stars rates^2 [photons^2/s^2]
        n_stars_eff_grp = vector<double>(n_groups); // Effective number of stars: 1/<w>
      }
      if (output_grp_obs) {
        f_esc_grp = vector<double>(n_groups); // Group escape fractions
        f2_esc_grp = vector<double>(n_groups); // Group squared escape fractions
        n_photons_esc_grp = vector<double>(n_groups); // Effective number of escaped photons: 1/<w>
      }
      if (output_grp_vir) {
        L_grp_vir = vector<double>(n_groups); // Group photon rates [photons/s]
        f_src_grp_vir = vector<double>(n_groups); // Group source fractions: sum(w0)
        f2_src_grp_vir = vector<double>(n_groups); // Group squared source fractions: sum(w0^2)
        n_photons_src_grp_vir = vector<double>(n_groups); // Effective number of emitted photons: 1/<w0>
        f_esc_grp_vir = vector<double>(n_groups); // Group escape fractions
        f2_esc_grp_vir = vector<double>(n_groups); // Group squared escape fractions
        n_photons_esc_grp_vir = vector<double>(n_groups); // Effective number of escaped photons: 1/<w>
        if (cell_based_emission) {
          L_gas_grp_vir = vector<double>(n_groups); // Group gas photon rates [photons/s]
          L2_gas_grp_vir = vector<double>(n_groups); // Group gas rates^2 [photons^2/s^2]
          n_cells_eff_grp_vir = vector<double>(n_groups); // Effective number of cells: 1/<w>
        }
        if (star_based_emission) {
          L_stars_grp_vir = vector<double>(n_groups); // Group stars photon rates [photons/s]
          L2_stars_grp_vir = vector<double>(n_groups); // Group stars rates^2 [photons^2/s^2]
          n_stars_eff_grp_vir = vector<double>(n_groups); // Effective number of stars: 1/<w>
        }
      }
      if (output_grp_gal) {
        L_grp_gal = vector<double>(n_groups); // Group photon rates [photons/s]
        f_src_grp_gal = vector<double>(n_groups); // Group source fractions: sum(w0)
        f2_src_grp_gal = vector<double>(n_groups); // Group squared source fractions: sum(w0^2)
        n_photons_src_grp_gal = vector<double>(n_groups); // Effective number of emitted photons: 1/<w0>
        f_esc_grp_gal = vector<double>(n_groups); // Group escape fractions
        f2_esc_grp_gal = vector<double>(n_groups); // Group squared escape fractions
        n_photons_esc_grp_gal = vector<double>(n_groups); // Effective number of escaped photons: 1/<w>
        if (cell_based_emission) {
          L_gas_grp_gal = vector<double>(n_groups); // Group gas photon rates [photons/s]
          L2_gas_grp_gal = vector<double>(n_groups); // Group gas rates^2 [photons^2/s^2]
          n_cells_eff_grp_gal = vector<double>(n_groups); // Effective number of cells: 1/<w>
        }
        if (star_based_emission) {
          L_stars_grp_gal = vector<double>(n_groups); // Group stars photon rates [photons/s]
          L2_stars_grp_gal = vector<double>(n_groups); // Group stars rates^2 [photons^2/s^2]
          n_stars_eff_grp_gal = vector<double>(n_groups); // Effective number of stars: 1/<w>
        }
      }
    }
    if (n_ugroups > 0) {                     // Have unfiltered groups
      L_ugrp = vector<double>(n_ugroups);    // Unfiltered group photon rates [photons/s]
      f_src_ugrp = vector<double>(n_ugroups); // Unfiltered group source fractions: sum(w0)
      f2_src_ugrp = vector<double>(n_ugroups); // Unfiltered group squared source fractions: sum(w0^2)
      n_photons_src_ugrp = vector<double>(n_ugroups); // Effective number of emitted photons: 1/<w0>
      if (cell_based_emission) {
        L_gas_ugrp = vector<double>(n_ugroups); // Unfiltered group gas photon rates [photons/s]
        L2_gas_ugrp = vector<double>(n_ugroups); // Unfiltered group gas rates^2 [photons^2/s^2]
        n_cells_eff_ugrp = vector<double>(n_ugroups); // Effective number of cells: 1/<w>
      }
      if (star_based_emission) {
        L_stars_ugrp = vector<double>(n_ugroups); // Unfiltered group stars photon rates [photons/s]
        L2_stars_ugrp = vector<double>(n_ugroups); // Unfiltered group stars rates^2 [photons^2/s^2]
        n_stars_eff_ugrp = vector<double>(n_ugroups); // Effective number of stars: 1/<w>
      }
    }
  }

  // Allocate subhalo properties
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {                    // Have subhalos
      L_sub = vector<double>(n_subhalos);    // Subhalo photon rates [photons/s]
      f_src_sub = vector<double>(n_subhalos); // Subhalo source fractions: sum(w0)
      f2_src_sub = vector<double>(n_subhalos); // Subhalo squared source fractions: sum(w0^2)
      n_photons_src_sub = vector<double>(n_subhalos); // Effective number of emitted photons: 1/<w0>
      if (cell_based_emission) {
        L_gas_sub = vector<double>(n_subhalos); // Subhalo gas photon rates [photons/s]
        L2_gas_sub = vector<double>(n_subhalos); // Subhalo gas rates^2 [photons^2/s^2]
        n_cells_eff_sub = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
      }
      if (star_based_emission) {
        L_stars_sub = vector<double>(n_subhalos); // Subhalo stars photon rates [photons/s]
        L2_stars_sub = vector<double>(n_subhalos); // Subhalo stars rates^2 [photons^2/s^2]
        n_stars_eff_sub = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
      }
      if (output_sub_obs) {
        f_esc_sub = vector<double>(n_subhalos); // Subhalo escape fractions
        f2_esc_sub = vector<double>(n_subhalos); // Subhalo squared escape fractions
        n_photons_esc_sub = vector<double>(n_subhalos); // Effective number of escaped photons: 1/<w>
      }
      if (output_sub_vir) {
        L_sub_vir = vector<double>(n_subhalos); // Subhalo photon rates [photons/s]
        f_src_sub_vir = vector<double>(n_subhalos); // Subhalo source fractions: sum(w0)
        f2_src_sub_vir = vector<double>(n_subhalos); // Subhalo squared source fractions: sum(w0^2)
        n_photons_src_sub_vir = vector<double>(n_subhalos); // Effective number of emitted photons: 1/<w0>
        f_esc_sub_vir = vector<double>(n_subhalos); // Subhalo escape fractions
        f2_esc_sub_vir = vector<double>(n_subhalos); // Subhalo squared escape fractions
        n_photons_esc_sub_vir = vector<double>(n_subhalos); // Effective number of escaped photons: 1/<w>
        if (cell_based_emission) {
          L_gas_sub_vir = vector<double>(n_subhalos); // Subhalo gas photon rates [photons/s]
          L2_gas_sub_vir = vector<double>(n_subhalos); // Subhalo gas rates^2 [photons^2/s^2]
          n_cells_eff_sub_vir = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
        }
        if (star_based_emission) {
          L_stars_sub_vir = vector<double>(n_subhalos); // Subhalo stars photon rates [photons/s]
          L2_stars_sub_vir = vector<double>(n_subhalos); // Subhalo stars rates^2 [photons^2/s^2]
          n_stars_eff_sub_vir = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
        }
      }
      if (output_sub_gal) {
        L_sub_gal = vector<double>(n_subhalos); // Subhalo photon rates [photons/s]
        f_src_sub_gal = vector<double>(n_subhalos); // Subhalo source fractions: sum(w0)
        f2_src_sub_gal = vector<double>(n_subhalos); // Subhalo squared source fractions: sum(w0^2)
        n_photons_src_sub_gal = vector<double>(n_subhalos); // Effective number of emitted photons: 1/<w0>
        f_esc_sub_gal = vector<double>(n_subhalos); // Subhalo escape fractions
        f2_esc_sub_gal = vector<double>(n_subhalos); // Subhalo squared escape fractions
        n_photons_esc_sub_gal = vector<double>(n_subhalos); // Effective number of escaped photons: 1/<w>
        if (cell_based_emission) {
          L_gas_sub_gal = vector<double>(n_subhalos); // Subhalo gas photon rates [photons/s]
          L2_gas_sub_gal = vector<double>(n_subhalos); // Subhalo gas rates^2 [photons^2/s^2]
          n_cells_eff_sub_gal = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
        }
        if (star_based_emission) {
          L_stars_sub_gal = vector<double>(n_subhalos); // Subhalo stars photon rates [photons/s]
          L2_stars_sub_gal = vector<double>(n_subhalos); // Subhalo stars rates^2 [photons^2/s^2]
          n_stars_eff_sub_gal = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
        }
      }
    }
    if (n_usubhalos > 0) {                   // Have unfiltered subhalos
      L_usub = vector<double>(n_usubhalos);  // Unfiltered subhalo photon rates [photons/s]
      f_src_usub = vector<double>(n_usubhalos); // Unfiltered subhalo source fractions: sum(w0)
      f2_src_usub = vector<double>(n_usubhalos); // Unfiltered subhalo squared source fractions: sum(w0^2)
      n_photons_src_usub = vector<double>(n_usubhalos); // Effective number of emitted photons: 1/<w0>
      if (cell_based_emission) {
        L_gas_usub = vector<double>(n_usubhalos); // Unfiltered subhalo gas photon rates [photons/s]
        L2_gas_usub = vector<double>(n_usubhalos); // Unfiltered subhalo gas rates^2 [photons^2/s^2]
        n_cells_eff_usub = vector<double>(n_usubhalos); // Effective number of cells: 1/<w>
      }
      if (star_based_emission) {
        L_stars_usub = vector<double>(n_usubhalos); // Unfiltered subhalo stars photon rates [photons/s]
        L2_stars_usub = vector<double>(n_usubhalos); // Unfiltered subhalo stars rates^2 [photons^2/s^2]
        n_stars_eff_usub = vector<double>(n_usubhalos); // Effective number of stars: 1/<w>
      }
    }
  }

  // Group and subhalo flags
  const bool save_groups = output_groups && n_groups > 0;
  const bool save_ugroups = output_groups && n_ugroups > 0;
  const bool save_subhalos = output_subhalos && n_subhalos > 0;
  const bool save_usubhalos = output_subhalos && n_usubhalos > 0;
  const bool need_group_indices = save_groups || save_ugroups;
  const bool need_subhalo_indices = save_subhalos || save_usubhalos;

  // Setup plane source
  if (plane_cont_source) {
    const double cX = plane_center_x_bbox * bbox[0].x + (1. - plane_center_x_bbox) * bbox[1].x; // Box center [cm]
    const double cY = plane_center_y_bbox * bbox[0].y + (1. - plane_center_y_bbox) * bbox[1].y;
    const double cZ = plane_center_z_bbox * bbox[0].z + (1. - plane_center_z_bbox) * bbox[1].z;
    const double rX = plane_radius_x_bbox * (bbox[1].x - bbox[0].x); // Box radius [cm]
    const double rY = plane_radius_y_bbox * (bbox[1].y - bbox[0].y);
    const double rZ = plane_radius_z_bbox * (bbox[1].z - bbox[0].z);
    switch (plane_type) {                    // Determine the plane properties
      case PosX:                             // +x direction
        plane_position = bbox[0].x + 1e-12*rX;
        plane_center_1 = cY;
        plane_center_2 = cZ;
        plane_radius_1 = rY;
        plane_radius_2 = rZ;
        plane_area = rY * rZ;
        break;
      case NegX:                             // -x direction
        plane_position = bbox[1].x - 1e-12*rX;
        plane_center_1 = cY;
        plane_center_2 = cZ;
        plane_radius_1 = rY;
        plane_radius_2 = rZ;
        plane_area = rY * rZ;
        break;
      case PosY:                             // +y direction
        plane_position = bbox[0].y + 1e-12*rY;
        plane_center_1 = cX;
        plane_center_2 = cZ;
        plane_radius_1 = rX;
        plane_radius_2 = rZ;
        plane_area = rX * rZ;
        break;
      case NegY:                             // -y direction
        plane_position = bbox[1].y - 1e-12*rY;
        plane_center_1 = cX;
        plane_center_2 = cZ;
        plane_radius_1 = rX;
        plane_radius_2 = rZ;
        plane_area = rX * rZ;
        break;
      case PosZ:                             // +z direction
        plane_position = bbox[0].z + 1e-12*rZ;
        plane_center_1 = cX;
        plane_center_2 = cY;
        plane_radius_1 = rX;
        plane_radius_2 = rY;
        plane_area = rX * rY;
        break;
      case NegZ:                             // -z direction
        plane_position = bbox[1].z - 1e-12*rZ;
        plane_center_1 = cX;
        plane_center_2 = cY;
        plane_radius_1 = rX;
        plane_radius_2 = rY;
        plane_area = rX * rY;
        break;
      default:
        error("Unrecognized plane direction type.");
    }
    plane_area *= plane_beam ? M_PI : 4.;    // Turn plane into a ellipsoidal beam
    if (S_plane_cont > 0.) {                 // Read as a surface density
      F_plane_cont = S_plane_cont * plane_area; // Plane continuum source flux [erg/s/angstrom]
      L_plane_cont = F_plane_cont * l_cont_res; // Plane continuum source luminosity [erg/s]
    } else if (F_plane_cont) {
      S_plane_cont = F_plane_cont / plane_area; // Plane continuum surface density [erg/s/cm^2/angstrom]
      L_plane_cont = F_plane_cont * l_cont_res; // Plane continuum source luminosity [erg/s]
    } else {
      F_plane_cont = L_plane_cont / l_cont_res; // Plane continuum source flux [erg/s/angstrom]
      S_plane_cont = F_plane_cont / plane_area; // Plane continuum surface density [erg/s/cm^2/angstrom]
    }
    L_tot += L_plane_cont;                   // Add to total luminosity
  }

  // Setup shell radius
  if constexpr (SPHERICAL) {
    if (shell_cont_source) {
      if (r_shell_cont < 1.000001 * r[0].x)
        r_shell_cont = 1.000001 * r[0].x;    // Minimum shell radius [cm]
      if (r_shell_cont > 0.999999 * r[n_cells].x)
        r_shell_cont = 0.999999 * r[n_cells].x; // Maximum shell radius [cm]
      if (shell_blackbody) {
        if (T_shell <= 0.) {
          const Vec3 point = {r_shell_cont, 0., 0.}; // Arbitrary point on the sphere
          const int i_shell = find_cell(point, 0); // Find the host cell
          T_shell = T[i_shell];              // Temperature at the shell [K]
        }
        const double x_shell = T0 / T_shell; // Spectral coordinate of the line
        const double xmin = x_shell / (Dv_cont_max/c + 1.); // Spectral coordinates
        const double xmax = x_shell / (Dv_cont_min/c + 1.); // of the continuum range
        const double frac = planck_fraction_Lbol(xmin) - planck_fraction_Lbol(xmax); // Fraction in range
        const double A_shell = 4. * M_PI * r_shell_cont * r_shell_cont; // Shell surface area [cm^2]
        const double sigmaB = 2. * pow(M_PI,5) * pow(kB,4) / (15. * pow(h,3) * pow(c,2)); // Stephan-Boltzmann constant [erg/cm^2/K^4/s]
        L_shell_cont = frac * A_shell * sigmaB * pow(T_shell, 4); // Stefan-Boltzmann law [erg/s]
      }
    }
    if (shell_line_source) {
      if (r_shell_line < 1.000001 * r[0].x)
        r_shell_line = 1.000001 * r[0].x;    // Minimum shell radius [cm]
      if (r_shell_line > 0.999999 * r[n_cells].x)
        r_shell_line = 0.999999 * r[n_cells].x; // Maximum shell radius [cm]
    }
  }
  if (shell_cont_source)
    L_tot += L_shell_cont;                   // Add to total luminosity
  if (shell_line_source)
    L_tot += L_shell_line;                   // Add to total luminosity

  // Setup shock radius
  if constexpr (SPHERICAL) if (shock_source) {
    if (r_shock < 1.000001 * r[0].x)
      r_shock = 1.000001 * r[0].x;           // Minimum shock radius [cm]
    if (r_shock > 0.999999 * r[n_cells].x)
      r_shock = 0.999999 * r[n_cells].x;     // Maximum shock radius [cm]
    if (shock_blackbody) {
      if (T_shock <= 0.) {
        const Vec3 point = {r_shock, 0., 0.}; // Arbitrary point on the sphere
        const int i_shock = find_cell(point, 0); // Find the host cell
        T_shock = T[i_shock];                // Temperature at the shock [K]
      }
      const double x_shock = T0 / T_shock;   // Spectral coordinate of the line
      const double xmin = x_shock / (Dv_cont_max/c + 1.); // Spectral coordinates
      const double xmax = x_shock / (Dv_cont_min/c + 1.); // of the continuum range
      const double frac = planck_fraction_Lbol(xmin) - planck_fraction_Lbol(xmax); // Fraction in range
      const double A_shock = 4. * M_PI * r_shock * r_shock; // Shock surface area [cm^2]
      const double sigmaB = 2. * pow(M_PI,5) * pow(kB,4) / (15. * pow(h,3) * pow(c,2)); // Stephan-Boltzmann constant [erg/cm^2/K^4/s]
      L_shock = frac * A_shock * sigmaB * pow(T_shock, 4); // Stefan-Boltzmann law [erg/s]
    }
  }
  if (shock_source)
    L_tot += L_shock;                        // Add to total luminosity
  if (plane_cont_source || sphere_cont_source || shell_cont_source || shock_source) {
    const double lambda_cont = lambda0 * angstrom; // Continuum wavelength [cm]
    constexpr double R_10pc = 10. * pc;      // Reference distance for continuum [cm]
    const double fnu_cont_fac = lambda_cont * lambda_cont / (4. * M_PI * c * R_10pc * R_10pc * angstrom);
    const double L_geo_cont = L_plane_cont + L_sphere_cont + L_shell_cont + L_shock;
    M_cont = -2.5 * log10(fnu_cont_fac * L_geo_cont) - 48.6; // Continuum absolute magnitude
  }

  // Setup general escape
  if constexpr (check_escape)
    setup_escape();                          // General escape setup

  // Setup cameras
  if (have_cameras || output_radial_avg || output_radial_cube_avg || output_radial_map || output_cube_map ||
      output_radial_map_grp || output_radial_avg_grp || output_radial_map_sub || output_radial_avg_sub)
    setup_cameras();                         // General camera setup (radial maps need centering)
  if (have_cameras) {
    if (root && output_cubes)
      cout << "\nSpectral Data Cubes: "
           << double(n_cameras) * double(nx_cube_pixels) * double(ny_cube_pixels) * double(n_cube_bins) * 8. / 1024. / 1024. / 1024.
           << " GB  (" << n_cameras << ", " << nx_cube_pixels << ", " << ny_cube_pixels << ", " << n_cube_bins << ")" << endl;

    // Initialize camera data
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_freq_avgs)
      freq_avgs = vector<double>(n_cameras); // Frequency averages
    if (output_freq_stds)
      freq_stds = vector<double>(n_cameras); // Frequency standard deviations
    if (output_freq_skews)
      freq_skews = vector<double>(n_cameras); // Frequency skewnesses
    if (output_freq_kurts)
      freq_kurts = vector<double>(n_cameras); // Frequency kurtoses
    if (output_fluxes)
      fluxes = vectors(n_cameras, vector<double>(n_bins)); // Fluxes
    if (output_images)
      images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_images2)
      images2 = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_freq_images)
      freq_images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_freq2_images)
      freq2_images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_freq3_images)
      freq3_images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_freq4_images)
      freq4_images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_rgb_images)
      rgb_images = Image3s(n_cameras, Image3(nx_pixels, ny_pixels));
    if (output_slits) {
      slits_x = Images(n_cameras, Image(n_slit_pixels, n_slit_bins));
      slits_y = Images(n_cameras, Image(n_slit_pixels, n_slit_bins));
    }
    if (output_cubes)
      cubes = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));

    // Initialize radial camera data
    if (output_radial_images)
      radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_radial_images2)
      radial_images2 = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_freq_radial_images)
      freq_radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_freq2_radial_images)
      freq2_radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_freq3_radial_images)
      freq3_radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_freq4_radial_images)
      freq4_radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_rgb_radial_images)
      rgb_radial_images = vector<vector<Vec3>>(n_cameras, vector<Vec3>(n_radial_pixels));
    if (output_radial_cubes)
      radial_cubes = Images(n_cameras, Image(n_radial_cube_pixels, n_radial_cube_bins));

    // Initialize intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      if (output_freq_avgs)
        freq_avgs_int = vector<double>(n_cameras); // Frequency averages
      if (output_fluxes)
        fluxes_int = vectors(n_cameras, vector<double>(n_bins)); // Fluxes
      if (output_images)
        images_int = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_cubes)
        cubes_int = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));
      if (output_radial_images)
        radial_images_int = vectors(n_cameras, vector<double>(n_radial_pixels));
      if (output_radial_cubes)
        radial_cubes_int = Images(n_cameras, Image(n_radial_cube_pixels, n_radial_cube_bins));
    }

    // Initialize attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        f_escs_ext = vector<double>(n_cameras); // Escape fractions
      if (output_freq_avgs)
        freq_avgs_ext = vector<double>(n_cameras); // Frequency averages
      if (output_fluxes)
        fluxes_ext = vectors(n_cameras, vector<double>(n_bins)); // Fluxes
      if (output_images)
        images_ext = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_cubes)
        cubes_ext = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));
      if (output_radial_images)
        radial_images_ext = vectors(n_cameras, vector<double>(n_radial_pixels));
      if (output_radial_cubes)
        radial_cubes_ext = Images(n_cameras, Image(n_radial_cube_pixels, n_radial_cube_bins));
    }

    // Initialize intrinsic cameras (proj)
    if (output_proj_emission)
      proj_images_int = Images(n_cameras, Image(nx_pixels, ny_pixels));

    // Initialize attenuation cameras (proj)
    if (output_proj_attenuation)
      proj_images_ext = Images(n_cameras, Image(nx_pixels, ny_pixels));

    // Initialize intrinsic cameras (proj cube)
    if (output_proj_cube_emission)
      proj_cubes_int = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));

    // Initialize attenuation cameras (proj cube)
    if (output_proj_cube_attenuation)
      proj_cubes_ext = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));

    if (output_proj_emission || output_proj_attenuation || output_proj_cube_emission || output_proj_cube_attenuation)
      setup_projections();                   // General projections setup

    // Collisional excitation camera data
    if (output_collisions) {
      // Initialize camera data
      if (output_escape_fractions)
        f_escs_col = vector<double>(n_cameras); // Escape fractions
      if (output_freq_avgs)
        freq_avgs_col = vector<double>(n_cameras); // Frequency averages
      if (output_freq_stds)
        freq_stds_col = vector<double>(n_cameras); // Frequency standard deviations
      if (output_freq_skews)
        freq_skews_col = vector<double>(n_cameras); // Frequency skewnesses
      if (output_freq_kurts)
        freq_kurts_col = vector<double>(n_cameras); // Frequency kurtoses
      if (output_fluxes)
        fluxes_col = vectors(n_cameras, vector<double>(n_bins)); // Fluxes
      if (output_images)
        images_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_images2)
        images2_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_freq_images)
        freq_images_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_freq2_images)
        freq2_images_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_freq3_images)
        freq3_images_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_freq4_images)
        freq4_images_col = Images(n_cameras, Image(nx_pixels, ny_pixels));

      // Initialize intrinsic cameras (mcrt)
      if (output_mcrt_emission) {
        if (output_freq_avgs)
          freq_avgs_int_col = vector<double>(n_cameras); // Frequency averages
        if (output_fluxes)
          fluxes_int_col = vectors(n_cameras, vector<double>(n_bins)); // Fluxes
        if (output_images)
          images_int_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      }

      // Initialize attenuation cameras (mcrt)
      if (output_mcrt_attenuation) {
        if (output_escape_fractions)
          f_escs_ext_col = vector<double>(n_cameras); // Escape fractions
        if (output_freq_avgs)
          freq_avgs_ext_col = vector<double>(n_cameras); // Frequency averages
        if (output_fluxes)
          fluxes_ext_col = vectors(n_cameras, vector<double>(n_bins)); // Fluxes
        if (output_images)
          images_ext_col = Images(n_cameras, Image(nx_pixels, ny_pixels));
      }

      // Initialize intrinsic cameras (proj)
      if (output_proj_emission)
        proj_images_int_col = Images(n_cameras, Image(nx_pixels, ny_pixels));

      // Initialize attenuation cameras (proj)
      if (output_proj_attenuation)
        proj_images_ext_col = Images(n_cameras, Image(nx_pixels, ny_pixels));

      // Initialize intrinsic cameras (proj cube)
      if (output_proj_cube_emission)
        proj_cubes_int_col = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));

      // Initialize attenuation cameras (proj cube)
      if (output_proj_cube_attenuation)
        proj_cubes_ext_col = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_cube_bins));
    }
  }

  // Setup non-camera quantities
  if (output_flux_avg)
    flux_avg = vector<double>(n_bins);       // Angle-averaged flux
  if (output_radial_avg)
    radial_avg = vector<double>(n_radial_pixels); // Angle-averaged radial surface brightness
  if (output_radial_cube_avg)
    radial_cube_avg = Image(n_radial_cube_pixels, n_radial_cube_bins); // Angle-averaged radial spectral data cube

  // Initialize line-of-sight healpix maps
  if (output_map) {
    const int n_pix_map = 12 * n_side_map * n_side_map; // Number of pixels
    map = vector<double>(n_pix_map);
    if (output_map2)
      map2 = vector<double>(n_pix_map);
    if (output_freq_map)
      freq_map = vector<double>(n_pix_map);
    if (output_freq2_map)
      freq2_map = vector<double>(n_pix_map);
    if (output_freq3_map)
      freq3_map = vector<double>(n_pix_map);
    if (output_freq4_map)
      freq4_map = vector<double>(n_pix_map);
    if (output_rgb_map)
      rgb_map = vector<Vec3>(n_pix_map);
  }

  // Initialize line-of-sight healpix flux map
  if (output_flux_map) {
    const int n_pix_flux = 12 * n_side_flux * n_side_flux; // 12 * n_side^2
    flux_map = Image(n_pix_flux, n_map_bins);
  }

  // Initialize line-of-sight healpix radial map
  if (output_radial_map) {
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // 12 * n_side^2
    radial_map = Image(n_pix_radial, n_map_pixels);
  }

  // Initialize line-of-sight healpix radial spectral map
  if (output_cube_map) {
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // 12 * n_side^2
    cube_map = Cube(n_pix_cube, n_cube_map_pixels, n_cube_map_bins);
  }

  // Initialize line-of-sight angular cosine (μ=cosθ) map
  if (output_mu) {
    inverse_dmu = double(n_mu) * 0.5;
    mu1 = vector<double>(n_mu);
    if (output_mu2)
      mu2 = vector<double>(n_mu);
    if (output_freq_mu)
      freq_mu = vector<double>(n_mu);
    if (output_freq2_mu)
      freq2_mu = vector<double>(n_mu);
    if (output_freq3_mu)
      freq3_mu = vector<double>(n_mu);
    if (output_freq4_mu)
      freq4_mu = vector<double>(n_mu);
  }

  // Initialize line-of-sight angular cosine (μ=cosθ) flux map
  if (output_flux_mu) {
    inverse_dmu_flux = double(n_mu_flux) * 0.5;
    flux_mu = Image(n_mu_flux, n_mu_bins);
  }

  // Initialize line-of-sight healpix maps for groups
  if (output_map_grp) {
    const int n_pix_grp = 12 * n_side_grp * n_side_grp; // Number of pixels
    if (output_grp_obs)
      map_grp = Image(n_groups, n_pix_grp);
    if (output_grp_vir)
      map_grp_vir = Image(n_groups, n_pix_grp);
    if (output_grp_gal)
      map_grp_gal = Image(n_groups, n_pix_grp);
    if (output_map2_grp) {
      if (output_grp_obs)
        map2_grp = Image(n_groups, n_pix_grp);
      if (output_grp_vir)
        map2_grp_vir = Image(n_groups, n_pix_grp);
      if (output_grp_gal)
        map2_grp_gal = Image(n_groups, n_pix_grp);
    }
    if (output_freq_map_grp)
      freq_map_grp = Image(n_groups, n_pix_grp);
    if (output_freq2_map_grp)
      freq2_map_grp = Image(n_groups, n_pix_grp);
    if (output_freq3_map_grp)
      freq3_map_grp = Image(n_groups, n_pix_grp);
    if (output_freq4_map_grp)
      freq4_map_grp = Image(n_groups, n_pix_grp);
  }

  // Initialize line-of-sight healpix maps for subhalos
  if (output_map_sub) {
    const int n_pix_sub = 12 * n_side_sub * n_side_sub; // Number of pixels
    if (output_sub_obs)
      map_sub = Image(n_subhalos, n_pix_sub);
    if (output_sub_vir)
      map_sub_vir = Image(n_subhalos, n_pix_sub);
    if (output_sub_gal)
      map_sub_gal = Image(n_subhalos, n_pix_sub);
    if (output_map2_sub) {
      if (output_sub_obs)
        map2_sub = Image(n_subhalos, n_pix_sub);
      if (output_sub_vir)
        map2_sub_vir = Image(n_subhalos, n_pix_sub);
      if (output_sub_gal)
        map2_sub_gal = Image(n_subhalos, n_pix_sub);
    }
    if (output_freq_map_sub)
      freq_map_sub = Image(n_subhalos, n_pix_sub);
    if (output_freq2_map_sub)
      freq2_map_sub = Image(n_subhalos, n_pix_sub);
    if (output_freq3_map_sub)
      freq3_map_sub = Image(n_subhalos, n_pix_sub);
    if (output_freq4_map_sub)
      freq4_map_sub = Image(n_subhalos, n_pix_sub);
  }

  // Initialize line-of-sight healpix flux map for groups
  if (output_flux_map_grp) {
    const double map_dfreq_grp = map_freq_max_grp - map_freq_min_grp; // Flux map frequency range
    if (map_freq_bin_width_grp > 0.)
      n_map_bins_grp = sane_int(map_dfreq_grp, map_freq_bin_width_grp); // Flux map bins
    else
      map_freq_bin_width_grp = map_dfreq_grp / double(n_map_bins_grp); // Flux map bin width
    inverse_map_freq_bin_width_grp = double(n_map_bins_grp) / map_dfreq_grp; // Flux map bin width^-1
    // Flux map quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_map_bin_width_grp = map_freq_bin_width_grp * km * lambda0 * (1. + z) / c; // Wavelength
    const int n_pix_flux_grp = 12 * n_side_flux_grp * n_side_flux_grp; // 12 * n_side^2
    flux_map_grp = Cube(n_groups, n_pix_flux_grp, n_map_bins_grp);
  }

  // Initialize line-of-sight healpix flux map for subhalos
  if (output_flux_map_sub) {
    const double map_dfreq_sub = map_freq_max_sub - map_freq_min_sub; // Flux map frequency range
    if (map_freq_bin_width_sub > 0.)
      n_map_bins_sub = sane_int(map_dfreq_sub, map_freq_bin_width_sub);
    else
      map_freq_bin_width_sub = map_dfreq_sub / double(n_map_bins_sub); // Flux map bin width
    inverse_map_freq_bin_width_sub = double(n_map_bins_sub) / map_dfreq_sub; // Flux map bin width^-1
    // Flux map quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_map_bin_width_sub = map_freq_bin_width_sub * km * lambda0 * (1. + z) / c; // Wavelength
    const int n_pix_flux_sub = 12 * n_side_flux_sub * n_side_flux_sub; // 12 * n_side^2
    flux_map_sub = Cube(n_subhalos, n_pix_flux_sub, n_map_bins_sub);
  }

  // Initialize angle-averaged radial surface brightness for groups
  if (output_flux_avg_grp) {
    const double dfreq_grp = freq_max_grp - freq_min_grp; // Flux avg frequency range
    if (freq_bin_width_grp > 0.)
      n_bins_grp = sane_int(dfreq_grp, freq_bin_width_grp); // Flux avg bins
    else
      freq_bin_width_grp = dfreq_grp / double(n_bins_grp); // Flux avg bin width
    inverse_freq_bin_width_grp = double(n_bins_grp) / dfreq_grp; // Flux avg bin width^-1
    // Flux avg quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_bin_width_grp = freq_bin_width_grp * km * lambda0 * (1. + z) / c; // Wavelength
    flux_avg_grp = Image(n_groups, n_bins_grp);
  }

  // Initialize angle-averaged radial surface brightness for subhalos
  if (output_flux_avg_sub) {
    const double dfreq_sub = freq_max_sub - freq_min_sub; // Flux avg frequency range
    if (freq_bin_width_sub > 0.)
      n_bins_sub = sane_int(dfreq_sub, freq_bin_width_sub); // Flux avg bins
    else
      freq_bin_width_sub = dfreq_sub / double(n_bins_sub); // Flux avg bin width
    inverse_freq_bin_width_sub = double(n_bins_sub) / dfreq_sub; // Flux avg bin width^-1
    // Flux avg quantities are in units of redshifted wavelength [obs. angstrom]
    if (cosmological)                        // Convert from velocity units [km/s]
      observed_bin_width_sub = freq_bin_width_sub * km * lambda0 * (1. + z) / c; // Wavelength
    flux_avg_sub = Image(n_subhalos, n_bins_sub);
  }

  // Initialize line-of-sight healpix maps for groups
  if (output_radial_map_grp) {
    const int n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // 12 * n_side^2
    radial_map_grp = Cube(n_groups, n_pix_radial_grp, n_map_pixels_grp);
  }

  // Initialize line-of-sight healpix maps for subhalos
  if (output_radial_map_sub) {
    const int n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // 12 * n_side^2
    radial_map_sub = Cube(n_subhalos, n_pix_radial_sub, n_map_pixels_grp);
  }

  // Initialize ange-averaged radial surface brightness for groups
  if (output_radial_avg_grp)
    radial_avg_grp = Image(n_groups, n_pixels_grp);

  // Initialize ange-averaged radial surface brightness for subhalos
  if (output_radial_avg_sub)
    radial_avg_sub = Image(n_subhalos, n_pixels_grp);

  // Setup cell-based emission sources
  const double V_norm = (!(cell_based_emission || doublet_cell_based_emission) || V_exp == 1.)
    ? 1. : (CARTESIAN ? 1. / dV : double(n_photons) / omp_sum(V)); // Volume normalization factor
  if (cell_based_emission) {
    // Assign a function pointer to the correct lines
    double (*alpha_eff)(double,double){ return_zero };
    double (*q_col)(double,double){ return_zero };
    // Effective recombination rate coefficient
    if (recombinations) {
      if (line == "Lyman-alpha")
        alpha_eff = alpha_eff_B_Lya;
      else if (line == "Balmer-alpha")
        alpha_eff = alpha_eff_B_Ha;
      else if (line == "Balmer-beta")
        alpha_eff = alpha_eff_B_Hb;
      else if (line == "Balmer-gamma")
        alpha_eff = alpha_eff_B_Hg;
      else if (line == "Balmer-delta")
        alpha_eff = alpha_eff_B_Hd;
      else if (line == "Balmer-epsilon")
        alpha_eff = alpha_eff_B_He;
      else if (line == "Paschen-alpha")
        alpha_eff = alpha_eff_B_Paa;
      else if (line == "Paschen-beta")
        alpha_eff = alpha_eff_B_Pab;
      else if (line == "Brackett-alpha")
        alpha_eff = alpha_eff_B_Bra;
      else if (rec_form == PEQ_1991) {       // PEQ 1991 functional form
        alpha_eff = alpha_eff_PEQ91;
        setup_alpha_PEQ91();
      } else
        error("Recombination emission rec_form is not set! "+line);
    }
    // Effective collisional excitation rate coefficient
    if (collisions) {
      if (line == "Lyman-alpha")
        q_col = q_col_Lya;
      else if (line == "Balmer-alpha")
        q_col = q_col_Ha;
      else if (line == "Balmer-beta")
        q_col = q_col_Hb;
      else if (line == "Balmer-gamma")
        q_col = q_col_Hg;
      else if (line == "Paschen-alpha")
        q_col = q_col_Paa;
      else if (line == "Paschen-beta")
        q_col = q_col_Pab;
      else if (line == "Brackett-alpha")
        q_col = q_col_Bra;
      else if (col_form == QCOL_TAB) {       // Tabulated form
        q_col = q_col_table;
        setup_q_col_table();
      } else
        error("Collisional excitation emission col_form is not set! "+line);
    }

    // Calculate the pdf based on cell luminosities
    const bool restrict_n = (emission_n_min > 0.) || (emission_n_max < positive_infinity);
    const bool restrict_ne = (emission_ne_min > 0.) || (emission_ne_max < positive_infinity);
    const double hc_A21 = h * c * A21;       // Coefficient for spontaneous emission
    const vector<double>& n_carrier = fields[line_carrier].data; // Carrier number density [cm^-3]
    const vector<double>& x_lower = fields[line_type].data; // Lower state fraction
    const int line_type_upper = line_type + (recombinations ? 1 : 0); // Requires recombinations
    const vector<double>& x_upper = fields[line_type_upper].data; // Upper state fraction
    j_cdf_cells.resize(n_cells);             // Cumulative distribution function
    j_weights_cells.resize(n_cells);         // Cell luminosity boosting weights
    if (save_line_emissivity)
      j_line.resize(n_cells);                // Cell emissivities
    if (output_collisions)
      f_col_cells.resize(n_cells);           // Collisional excitation fractions
    Vec3 Lr = 0, Lv = 0, Lr2 = 0, Lv2 = 0;   // Center of luminosity (squared) position and velocity
    double L2_gas = 0.;                      // Effective number of cells statistic
    if (focus_groups_on_emission) {
      r_light_grp = vector<Vec3>(n_groups);  // Group center of luminosity position [cm]
      v_light_grp = vector<Vec3>(n_groups);  // Group center of luminosity velocity [cm/s]
    }
    if (focus_subhalos_on_emission) {
      r_light_sub = vector<Vec3>(n_subhalos); // Subhalo center of luminosity position [cm]
      v_light_sub = vector<Vec3>(n_subhalos); // Subhalo center of luminosity velocity [cm/s]
    }
    #pragma omp parallel for reduction(+:L_rec, L_col, L_sp, L2_gas) reduction(+:Lr, Lv, Lr2, Lv2) \
                             reduction(vec3_plus:r_light_grp, v_light_grp, r_light_sub, v_light_sub)
    for (int i = 0; i < n_cells; ++i) {
      // Avoid cells that are special or outside the emission region
      if (invalid_cell(i)) {
        j_cdf_cells[i] = 0.;                 // No emission
        if (save_line_emissivity)
          j_line[i] = 0.;                    // No emission
        if (output_collisions)
          f_col_cells[i] = 0.;               // No emission
        continue;
      }
      // Local group and subhalo indices
      int i_grp = -1, i_ugrp = -1, i_sub = -1, i_usub = -1;
      if (need_group_indices) {
        const int grp_id = group_id_cell[i]; // IDs
        if (save_groups)
          i_grp = get_index_sorted(grp_id, group_id); // Group ID index
        if (save_ugroups)
          i_ugrp = get_index_sorted(grp_id, ugroup_id); // Unfiltered group ID index
      }
      if (need_subhalo_indices) {
        const int sub_id = subhalo_id_cell[i]; // IDs
        if (save_subhalos)
          i_sub = get_index_sorted(sub_id, subhalo_id); // Subhalo ID index
        if (save_usubhalos)
          i_usub = get_index_sorted(sub_id, usubhalo_id); // Unfiltered subhalo ID index
      }
      if (restrict_n) {
        const double n_tot = rho[i] * (1. + hydrogen_fraction * (3. + 4. * x_e[i])) / (4. * mH);
        if (n_tot < emission_n_min || n_tot > emission_n_max) {
          j_cdf_cells[i] = 0.;               // No emission
          if (save_line_emissivity)
            j_line[i] = 0.;                  // No emission
          if (output_collisions)
            f_col_cells[i] = 0.;             // No emission
          continue;
        }
      }
      if (restrict_ne) {
        const double ne_tot = x_e[i] * n_H[i];
        if (ne_tot < emission_ne_min || ne_tot > emission_ne_max) {
          j_cdf_cells[i] = 0.;               // No emission
          if (save_line_emissivity)
            j_line[i] = 0.;                  // No emission
          if (output_collisions)
            f_col_cells[i] = 0.;             // No emission
          continue;
        }
      }
      const Vec3 r_cell = cell_center(i);    // Cell position [cm]
      const double V_cell = VOLUME(i);       // Cell volume [cm^3]
      const double n_e = x_e[i] * n_H[i];    // Electron number density [cm^-3]
      const double E0_nc_ne = E0 * n_carrier[i] * n_e; // Shared constant [erg/cm^6]
      // Recombination emission: L_rec = hν ∫ α_eff(T) n_e n_{i+1} dV
      const double j_rec_cell = E0_nc_ne * x_upper[i] * alpha_eff(T[i], n_e); // [erg/s/cm^3]
      const double L_rec_cell = j_rec_cell * V_cell;
      L_rec += L_rec_cell;                   // Recombination luminosity [erg/s]
      // Collisional excitation: L_col = hν ∫ q_col(T) n_e n_i dV
      const double q_col_T = q_col(T[i], n_e); // Save the collisional coefficient
      double j_col_cell = E0_nc_ne * x_lower[i] * q_col_T; // [erg/s/cm^3]
      double L_col_cell = j_col_cell * V_cell;
      if (collisions_limiter > 0.) {         // Limit collisional excitation
        const double e_HI = e_col_HI(T[i]);  // Total collisional excitation [erg/s]
        if (e_HI > 0.) {
          const double f_line = E0 * q_col_T / e_HI; // Fraction in this line
          const double L_col_lim = collisions_limiter * G_ion[i] * f_line;
          if (L_col_cell > L_col_lim) {
            L_col_cell = L_col_lim;          // Limited by photoheating rate
            j_col_cell = L_col_lim / V_cell; // Update emissivity too
          }
        }
      }
      L_col += L_col_cell;                   // Collisional luminosity [erg/s]
      // Spontaneous emission: L_sp = hc A21 ∫ n2 dV
      const double j_sp_cell = spontaneous ? hc_A21 * n_upper[i] : 0.;
      const double L_sp_cell = j_sp_cell * V_cell;
      L_sp += L_sp_cell;                     // Spontaneous emission luminosity [erg/s]
      // Track the total luminosity from each cell
      const double L_cell = L_rec_cell + L_col_cell + L_sp_cell;
      const double L2_cell = L_cell * L_cell;
      j_cdf_cells[i] = L_cell;
      const Vec3 Lr_cell = L_cell * r_cell;  // Center of luminosity position [cm erg/s]
      const Vec3 Lv_cell = L_cell * v[i];    // Center of luminosity velocity [cm/s erg/s]
      Lr += Lr_cell;                         // Center of luminosity position [cm erg/s]
      Lr2 += Lr_cell * r_cell;               // Center of luminosity position^2 [cm^2 erg/s^2]
      Lv += Lv_cell;                         // Center of luminosity velocity [cm/s erg/s]
      Lv2 += Lv_cell * v[i];                 // Center of luminosity velocity^2 [cm^2/s^2 erg/s]
      L2_gas += L2_cell;                     // Effective number of cells statistic
      if (save_line_emissivity)
        j_line[i] = j_rec_cell + j_col_cell + j_sp_cell;
      if (output_collisions)                 // Collisional excitation fraction
        f_col_cells[i] = safe_division(L_col_cell, L_cell);
      if (i_grp >= 0) {
        #pragma omp atomic
        L_gas_grp[i_grp] += L_cell;
        #pragma omp atomic
        L2_gas_grp[i_grp] += L2_cell;
        if (output_grp_vir && norm2(r_cell - r_grp_vir[i_grp]) < R2_grp_vir[i_grp]) {
          #pragma omp atomic
          L_gas_grp_vir[i_grp] += L_cell;
          #pragma omp atomic
          L2_gas_grp_vir[i_grp] += L2_cell;
        }
        if (output_grp_gal && norm2(r_cell - r_grp_gal[i_grp]) < R2_grp_gal[i_grp]) {
          #pragma omp atomic
          L_gas_grp_gal[i_grp] += L_cell;
          #pragma omp atomic
          L2_gas_grp_gal[i_grp] += L2_cell;
        }
        if (focus_groups_on_emission) {
          r_light_grp[i_grp] += Lr_cell;       // Center of luminosity position [cm erg/s]
          v_light_grp[i_grp] += Lv_cell;       // Center of luminosity velocity [cm/s erg/s]
        }
      } else if (i_ugrp >= 0) {
        #pragma omp atomic
        L_gas_ugrp[i_ugrp] += L_cell;
        #pragma omp atomic
        L2_gas_ugrp[i_ugrp] += L2_cell;
      }
      if (i_sub >= 0) {
        #pragma omp atomic
        L_gas_sub[i_sub] += L_cell;
        #pragma omp atomic
        L2_gas_sub[i_sub] += L2_cell;
        if (output_sub_vir && norm2(r_cell - r_sub_vir[i_sub]) < R2_sub_vir[i_sub]) {
          #pragma omp atomic
          L_gas_sub_vir[i_sub] += L_cell;
          #pragma omp atomic
          L2_gas_sub_vir[i_sub] += L2_cell;
        }
        if (output_sub_gal && norm2(r_cell - r_sub_gal[i_sub]) < R2_sub_gal[i_sub]) {
          #pragma omp atomic
          L_gas_sub_gal[i_sub] += L_cell;
          #pragma omp atomic
          L2_gas_sub_gal[i_sub] += L2_cell;
        }
        if (focus_subhalos_on_emission) {
          r_light_sub[i_sub] += Lr_cell;       // Center of luminosity position [cm erg/s]
          v_light_sub[i_sub] += Lv_cell;       // Center of luminosity velocity [cm/s erg/s]
        }
      } else if (i_usub >= 0) {
        #pragma omp atomic
        L_gas_usub[i_usub] += L_cell;
        #pragma omp atomic
        L2_gas_usub[i_usub] += L2_cell;
      }
    }

    if (output_cells) {
      L_int_cells.resize(n_cells);           // Intrinsic cell luminosities [erg/s]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i)
        L_int_cells[i] = j_cdf_cells[i];     // Copy before cdf conversion
    }

    const double L_gas = L_rec + L_col + L_sp; // Total from cell-based emission
    if (L_gas <= 0.)
      root_error("Total gas luminosity must be positive");
    L_tot += L_gas;                          // Add to the total luminosity
    n_cells_eff = L_gas * L_gas / L2_gas;    // Effective number of cells: <L>^2/<L^2>

    const double _1_L_tot = 1. / L_gas;
    r_light = Lr * _1_L_tot;                 // Center of light position [cm]
    r2_light = Lr2 * _1_L_tot;               // Center of light position^2 [cm^2]
    v_light = Lv * _1_L_tot;                 // Center of light velocity [cm/s]
    v2_light = Lv2 * _1_L_tot;               // Center of light velocity^2 [cm^2/s^2]
    if (focus_cameras_on_emission)
      camera_center = r_light;               // Focus cameras on emission (position)
    if (shift_cameras_on_emission)
      camera_motion = v_light;               // Shift cameras on emission (velocity)
    if constexpr (output_ion_radial_age_freq || output_radial_flow) {
      if (focus_radial_on_emission)
        radial_center = r_light;
      if (shift_radial_on_emission)
        radial_motion = v_light;
    }
    if (focus_groups_on_emission) {
      for (int i = 0; i < n_groups; ++i) {
        const double _1_L_grp = safe_division(1., L_gas_grp[i]); // Group normalization factor
        r_light_grp[i] *= _1_L_grp;          // Center of light position [cm]
        v_light_grp[i] *= _1_L_grp;          // Center of light velocity [cm/s]
      }
    }
    if (focus_subhalos_on_emission) {
      for (int i = 0; i < n_subhalos; ++i) {
        const double _1_L_sub = safe_division(1., L_gas_sub[i]); // Subhalo normalization factor
        r_light_sub[i] *= _1_L_sub;          // Center of light position [cm]
        v_light_sub[i] *= _1_L_sub;          // Center of light velocity [cm/s]
      }
    }
    const double _1_n_photons = 1. / double(n_photons);
    if (j_exp == 1. && V_exp == 1.) {        // No luminosity boosting
      for (int i = 1; i < n_cells; ++i)
        j_cdf_cells[i] += j_cdf_cells[i - 1]; // Turn the pdf into a cdf
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        j_cdf_cells[i] *= _1_L_tot;          // Normalize cdf
        j_weights_cells[i] = _1_n_photons;   // Equal weights
      }
    } else {                                 // Apply luminosity and volume boosting
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        j_weights_cells[i] = j_cdf_cells[i] * _1_L_tot; // Copy the normalized pdf
        if (j_weights_cells[i] > 0.)         // Apply power law boost
          j_cdf_cells[i] = pow(j_weights_cells[i], j_exp); // pdf^j_exp (unnormalized)
        else
          j_cdf_cells[i] = 0.;               // No probability
        if (V_exp != 1.)                     // Apply volume boost
          j_cdf_cells[i] *= pow(V_norm * VOLUME(i), V_exp);
      }
      const double _1_j_exp_sum = 1. / omp_sum(j_cdf_cells);
      const double dn_photons = double(n_photons);
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        j_cdf_cells[i] *= _1_j_exp_sum;      // pdf^j_exp (normalized)
        if (j_cdf_cells[i] > 0.)
          j_weights_cells[i] /= j_cdf_cells[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
      }
      for (int i = 1; i < n_cells; ++i)
        j_cdf_cells[i] += j_cdf_cells[i - 1]; // Turn the pdf into a cdf
    }

    // Populate a lookup table to quickly find cdf indices (cells)
    const double half_cdf = 0.5 / double(n_cdf_cells); // Half interval width
    for (int i = 0, cell = 0; i < n_cdf_cells; ++i) {
      const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
      while (j_cdf_cells[cell] <= frac_cdf && cell < n_cells - 1)
        cell++;                              // Move to the right bin ( --> )
      j_map_cells[i] = cell;                 // Lookup table for insertion
    }
  }

  // Setup doublet cell-based emission sources
  if (doublet_cell_based_emission) {
    // Assign a function pointer to the correct lines
    double (*qp_col)(double,double){ return_zero };
    // Effective recombination rate coefficient
    if (recombinations)
      error("Recombination emission is not implemented for doublet lines!");
    if (spontaneous)
      error("Spontaneous emission is not implemented for doublet lines!");
    if (collisions_limiter > 0.)
      error("collisions_limiter is not implemented for doublet lines!");
    // Effective collisional excitation rate coefficient
    if (collisions) {
      if (col_form == QCOL_TAB) // Tabulated form
        qp_col = qp_col_table;
      else
        error("Doublet collisional excitation emission col_form is not set! "+line);
    }

    // Calculate the pdf based on cell luminosities
    const bool restrict_n = (emission_n_min > 0.) || (emission_n_max < positive_infinity);
    const bool restrict_ne = (emission_ne_min > 0.) || (emission_ne_max < positive_infinity);
    const vector<double>& n_carrier = fields[line_carrier].data; // Carrier number density [cm^-3]
    const vector<double>& x_lower = fields[line_type].data; // Lower state fraction
    jp_cdf_cells.resize(n_cells);            // Cumulative distribution function
    jp_weights_cells.resize(n_cells);        // Cell luminosity boosting weights
    if (save_line_emissivity)
      jp_line.resize(n_cells);               // Cell emissivities
    Vec3 Lpr = 0, Lpv = 0, Lpr2 = 0, Lpv2 = 0; // Center of luminosity (squared) position and velocity
    double Lp2_gas = 0.;                     // Effective number of cells statistic
    #pragma omp parallel for reduction(+:Lp_col, Lp2_gas) reduction(+:Lpr, Lpv, Lpr2, Lpv2)
    for (int i = 0; i < n_cells; ++i) {
      // Avoid cells that are special or outside the emission region
      if (invalid_cell(i)) {
        jp_cdf_cells[i] = 0.;                // No emission
        if (save_line_emissivity)
          jp_line[i] = 0.;                   // No emission
        continue;
      }
      if (restrict_n) {
        const double Y_cell = read_helium_fraction ? Y[i] : helium_fraction * (1. - Z[i]);
        const double yHe = (Y_cell < 0.) ? 0. : Y_cell * rho[i] / (mHe * n_H[i]); // y_He = n_He / n_H
        const double n_tot = n_H[i] * (1. + yHe + x_e[i]); // n_tot = n_e + n_H + n_He (+ n_C + ...)
        // Assuming mHe = 4 mH, Y = 1 - X  =>  n_tot = rho * (1 + X * (3 + 4 * x_e)) / (4 * mH)
        if (n_tot < emission_n_min || n_tot > emission_n_max) {
          jp_cdf_cells[i] = 0.;              // No emission
          if (save_line_emissivity)
            jp_line[i] = 0.;                 // No emission
          continue;
        }
      }
      if (restrict_ne) {
        const double ne_tot = x_e[i] * n_H[i];
        if (ne_tot < emission_ne_min || ne_tot > emission_ne_max) {
          jp_cdf_cells[i] = 0.;              // No emission
          if (save_line_emissivity)
            jp_line[i] = 0.;                 // No emission
          continue;
        }
      }
      const Vec3 r_cell = cell_center(i);    // Cell position [cm]
      const double V_cell = VOLUME(i);       // Cell volume [cm^3]
      const double n_e = x_e[i] * n_H[i];    // Electron number density [cm^-3]
      const double E0_nc_ne = E0 * n_carrier[i] * n_e; // Shared constant [erg/cm^6]
      // Collisional excitation: Lp_col = hν ∫ qp_col(T) n_e n_i dV
      const double qp_col_T = qp_col(T[i], n_e); // Save the collisional coefficient
      double jp_col_cell = E0_nc_ne * x_lower[i] * qp_col_T; // [erg/s/cm^3]
      double Lp_col_cell = jp_col_cell * V_cell;
      Lp_col += Lp_col_cell;                 // Collisional luminosity [erg/s]
      // Track the total luminosity from each cell
      jp_cdf_cells[i] = Lp_col_cell;
      Lpr += Lp_col_cell * r_cell;           // Center of luminosity position [cm]
      Lpr2 += Lp_col_cell * r_cell * r_cell; // Center of luminosity position^2 [cm^2]
      Lpv += Lp_col_cell * v[i];             // Center of luminosity velocity [cm/s]
      Lpv2 += Lp_col_cell * v[i] * v[i];     // Center of luminosity velocity^2 [cm^2/s^2]
      Lp2_gas += Lp_col_cell * Lp_col_cell;  // Effective number of cells statistic
      if (save_line_emissivity)
        jp_line[i] = jp_col_cell;
    }

    if (output_cells) {
      L_int_doublet_cells.resize(n_cells);   // Intrinsic doublet cell luminosities [erg/s]
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i)
        L_int_doublet_cells[i] = jp_cdf_cells[i]; // Copy before cdf conversion
    }

    const double Lp_gas = Lp_col;            // Total from doublet cell-based emission
    if (Lp_gas <= 0.)
      root_error("Total doublet gas luminosity must be positive");
    r_light *= L_tot;
    r2_light *= L_tot;
    v_light *= L_tot;
    v2_light *= L_tot;
    L_tot += Lp_gas;                         // Add to the total luminosity
    n_cells_effp = Lp_gas * Lp_gas / Lp2_gas; // Effective number of cells: <L>^2/<L^2>

    const double _1_Lp_tot = 1. / Lp_gas;
    r_light += Lpr;                          // Center of light position [cm]
    r2_light += Lpr2;                        // Center of light position^2 [cm^2]
    v_light += Lpv;                          // Center of light velocity [cm/s]
    v2_light += Lpv2;                        // Center of light velocity^2 [cm^2/s^2]
    r_light /= L_tot;
    r2_light /= L_tot;
    v_light /= L_tot;
    v2_light /= L_tot;
    if (focus_cameras_on_emission)
      camera_center = r_light;               // Focus cameras on emission (position)
    if (shift_cameras_on_emission)
      camera_motion = v_light;               // Shift cameras on emission (velocity)
    if constexpr (output_ion_radial_age_freq || output_radial_flow) {
      if (focus_radial_on_emission)
        radial_center = r_light;
      if (shift_radial_on_emission)
        radial_motion = v_light;
    }
    const double _1_n_photons = 1. / double(n_photons);
    if (j_exp == 1. && V_exp == 1.) {        // No luminosity boosting
      for (int i = 1; i < n_cells; ++i)
        jp_cdf_cells[i] += jp_cdf_cells[i - 1]; // Turn the pdf into a cdf
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        jp_cdf_cells[i] *= _1_Lp_tot;        // Normalize cdf
        jp_weights_cells[i] = _1_n_photons;  // Equal weights
      }
    } else {                                 // Apply luminosity and volume boosting
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        jp_weights_cells[i] = jp_cdf_cells[i] * _1_Lp_tot; // Copy the normalized pdf
        if (jp_weights_cells[i] > 0.)        // Apply power law boost
          jp_cdf_cells[i] = pow(jp_weights_cells[i], j_exp); // pdf^j_exp (unnormalized)
        else
          jp_cdf_cells[i] = 0.;              // No probability
        if (V_exp != 1.)                     // Apply volume boost
          jp_cdf_cells[i] *= pow(V_norm * VOLUME(i), V_exp);
      }
      const double _1_j_exp_sum = 1. / omp_sum(jp_cdf_cells);
      const double dn_photons = double(n_photons);
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        jp_cdf_cells[i] *= _1_j_exp_sum;     // pdf^j_exp (normalized)
        if (jp_cdf_cells[i] > 0.)
          jp_weights_cells[i] /= jp_cdf_cells[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
      }
      for (int i = 1; i < n_cells; ++i)
        jp_cdf_cells[i] += jp_cdf_cells[i - 1]; // Turn the pdf into a cdf
    }

    // Populate a lookup table to quickly find cdf indices (cells)
    const double half_cdf = 0.5 / double(n_cdf_cells); // Half interval width
    for (int i = 0, cell = 0; i < n_cdf_cells; ++i) {
      const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
      while (jp_cdf_cells[cell] <= frac_cdf && cell < n_cells - 1)
        cell++;                              // Move to the right bin ( --> )
      jp_map_cells[i] = cell;                // Lookup table for insertion
    }
  }

  // Setup star-based emission sources
  if (star_based_emission) {
    // Calculate the pdf based on star luminosities
    j_cdf_stars.resize(n_stars);             // Cumulative distribution function
    j_weights_stars.resize(n_stars);         // Star luminosity boosting weights
    double L2_stars = 0.;                    // Effective number of stars statistic
    if (continuum) {
      if (source_model == "") {
        #pragma omp parallel for reduction(+:L_cont, L2_stars)
        for (int i = 0; i < n_stars; ++i) {
          // Avoid stars that are in special cells or outside the emission region
          if (invalid_star(i)) {
            j_cdf_stars[i] = 0.;             // No emission
            continue;
          }
          L_cont_star[i] *= l_cont_res;      // Band range conversion [erg/s]
          L_cont += L_cont_star[i];          // Line continuum luminosity [erg/s]
          L2_stars += L_cont_star[i] * L_cont_star[i]; // Effective number of stars statistic
          j_cdf_stars[i] = L_cont_star[i];   // Copy luminosities to build pdf
        }
      } else {                               // Tabulated by age and metallicity
        #pragma omp parallel for reduction(+:L_cont, L2_stars)
        for (int i = 0; i < n_stars; ++i) {
          // Avoid stars that are in special cells or outside the emission region
          if (invalid_star(i)) {
            j_cdf_stars[i] = 0.;             // No emission
            continue;
          }
          // Local group and subhalo indices
          int i_grp = -1, i_ugrp = -1, i_sub = -1, i_usub = -1;
          if (need_group_indices) {
            const int grp_id = group_id_star[i]; // IDs
            if (save_groups)
              i_grp = get_index_sorted(grp_id, group_id); // Group ID index
            if (save_ugroups)
              i_ugrp = get_index_sorted(grp_id, ugroup_id); // Unfiltered group ID index
          }
          if (need_subhalo_indices) {
            const int sub_id = subhalo_id_star[i]; // IDs
            if (save_subhalos)
              i_sub = get_index_sorted(sub_id, subhalo_id); // Subhalo ID index
            if (save_usubhalos)
              i_usub = get_index_sorted(sub_id, usubhalo_id); // Unfiltered subhalo ID index
          }
          const double mass = m_init_star[i]; // Initial mass of the star [Msun]
          const double Z = Z_star[i];        // Metallicity of the star
          const double age = age_star[i];    // Age of the star [Gyr]

          int i_L_Z = 0, i_L_age = 0;        // Lower interpolation index
          double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
          if (Z >= 4e-2) {                   // Metallicity maximum = 0.04
            i_L_Z = 11;                      // 13 metallicity bins
            frac_R_Z = 1.;
          } else if (Z > 1e-5) {             // Metallicity minimum = 10^-5
            const double logZ = log10(Z);    // Interpolate in log space
            while (logZ > logZ_BP[i_L_Z+1])
              i_L_Z++;                       // Search metallicity indices
            frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
          }
          if (age >= 100.) {                 // Age maximum = 100 Gyr
            i_L_age = 49;                    // 51 age bins
            frac_R_age = 1.;
          } else if (age > 1e-3) {           // Age minimum = 1 Myr
            const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
            const double f_age = floor(d_age); // Floor coordinate
            frac_R_age = d_age - f_age;      // Interpolation fraction (right)
            i_L_age = int(f_age);            // Table age index (left)
          }

          // Bilinear interpolation (based on left and right fractions)
          const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
          const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
          const double L_cont_star = l_cont_res * mass // Band range and mass conversion [erg/s]
            * pow(10., (log_Lcont_Z_age[i_L_Z][i_L_age]*frac_L_Z + log_Lcont_Z_age[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                     + (log_Lcont_Z_age[i_L_Z][i_R_age]*frac_L_Z + log_Lcont_Z_age[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
          L_cont += L_cont_star;             // Line continuum luminosity [erg/s]
          L2_stars += L_cont_star * L_cont_star; // Effective number of stars statistic
          j_cdf_stars[i] = L_cont_star;      // Copy luminosities to build pdf

          if (i_grp >= 0) {
            #pragma omp atomic
            L_stars_grp[i_grp] += L_cont_star;
            #pragma omp atomic
            L2_stars_grp[i_grp] += L2_stars;
            if (output_grp_vir && norm2(r_star[i] - r_grp_vir[i_grp]) < R2_grp_vir[i_grp]) {
              #pragma omp atomic
              L_stars_grp_vir[i_grp] += L_cont_star;
              #pragma omp atomic
              L2_stars_grp_vir[i_grp] += L2_stars;
            }
            if (output_grp_gal && norm2(r_star[i] - r_grp_gal[i_grp]) < R2_grp_gal[i_grp]) {
              #pragma omp atomic
              L_stars_grp_gal[i_grp] += L_cont_star;
              #pragma omp atomic
              L2_stars_grp_gal[i_grp] += L2_stars;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            L_stars_ugrp[i_ugrp] += L_cont_star;
            #pragma omp atomic
            L2_stars_ugrp[i_ugrp] += L2_stars;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            L_stars_sub[i_sub] += L_cont_star;
            #pragma omp atomic
            L2_stars_sub[i_sub] += L2_stars;
            if (output_sub_vir && norm2(r_star[i] - r_sub_vir[i_sub]) < R2_sub_vir[i_sub]) {
              #pragma omp atomic
              L_stars_sub_vir[i_sub] += L_cont_star;
              #pragma omp atomic
              L2_stars_sub_vir[i_sub] += L2_stars;
            }
            if (output_sub_gal && norm2(r_star[i] - r_sub_gal[i_sub]) < R2_sub_gal[i_sub]) {
              #pragma omp atomic
              L_stars_sub_gal[i_sub] += L_cont_star;
              #pragma omp atomic
              L2_stars_sub_gal[i_sub] += L2_stars;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            L_stars_usub[i_usub] += L_cont_star;
            #pragma omp atomic
            L2_stars_usub[i_usub] += L2_stars;
          }
        }
      }
      const double lambda_cont = lambda0 * angstrom; // Continuum wavelength [cm]
      constexpr double R_10pc = 10. * pc;    // Reference distance for continuum [cm]
      const double fnu_cont_fac = lambda_cont * lambda_cont / (4. * M_PI * c * R_10pc * R_10pc * angstrom);
      M_cont = -2.5 * log10(fnu_cont_fac * L_cont) - 48.6; // Continuum absolute magnitude
    } else if (unresolved_HII) {
      double conv_factor = 0.;               // Line conversion factor
      if (line == "Lyman-alpha")
        conv_factor = 0.68 * E0 * (1. - f_esc_HII);
      else if (line == "Balmer-alpha")
        conv_factor = 0.45 * E0 * (1. - f_esc_HII);
      else if (line == "Balmer-beta")
        conv_factor = 0.12 * E0 * (1. - f_esc_HII);
      else
        error("Line not implemented for unresolved HII emission: " + line);
      #pragma omp parallel for reduction(+:L_HII, L2_stars)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are in special cells or outside the emission region
        if (invalid_star(i)) {
          j_cdf_stars[i] = 0.;               // No emission
          continue;
        }
        const double mass = m_init_star[i];  // Initial mass of the star [Msun]
        const double Z = Z_star[i];          // Metallicity of the star
        const double age = age_star[i];      // Age of the star [Gyr]

        int i_L_Z = 0, i_L_age = 0;          // Lower interpolation index
        double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
        if (Z >= 4e-2) {                     // Metallicity maximum = 0.04
          i_L_Z = 11;                        // 13 metallicity bins
          frac_R_Z = 1.;
        } else if (Z > 1e-5) {               // Metallicity minimum = 10^-5
          const double logZ = log10(Z);      // Interpolate in log space
          while (logZ > logZ_BP[i_L_Z+1])
            i_L_Z++;                         // Search metallicity indices
          frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
        }
        if (age >= 100.) {                   // Age maximum = 100 Gyr
          i_L_age = 49;                      // 51 age bins
          frac_R_age = 1.;
        } else if (age > 1e-3) {             // Age minimum = 1 Myr
          const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
          const double f_age = floor(d_age); // Floor coordinate
          frac_R_age = d_age - f_age;        // Interpolation fraction (right)
          i_L_age = int(f_age);              // Table age index (left)
        }

        // Bilinear interpolation (based on left and right fractions)
        const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
        const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
        const double L_HII_star = conv_factor * mass
          * pow(10., (log_Qion_Z_age[i_L_Z][i_L_age]*frac_L_Z + log_Qion_Z_age[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                   + (log_Qion_Z_age[i_L_Z][i_R_age]*frac_L_Z + log_Qion_Z_age[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        L_HII += L_HII_star;                 // Unresolved HII regions luminosity [erg/s]
        L2_stars += L_HII_star * L_HII_star; // Effective number of stars statistic
        j_cdf_stars[i] = L_HII_star;         // Copy luminosities to build pdf
      }
    }

    if (output_stars || save_line_emissivity) {
      L_int_stars.resize(n_stars);           // Intrinsic star luminosities [erg/s]
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i)
        L_int_stars[i] = j_cdf_stars[i];     // Copy before cdf conversion
    }

    const double L_stars = L_cont + L_HII;   // Total from star-based emission
    if (L_stars <= 0.)
      root_error("Total stellar luminosity must be positive");
    L_tot += L_stars;                        // Add to the total luminosity
    n_stars_eff = L_stars * L_stars / L2_stars; // Effective number of stars: <L>^2/<L^2>

    const double _1_L_tot = 1. / L_stars;
    const double _1_n_photons = 1. / double(n_photons);
    if (j_exp == 1.) {                       // No luminosity boosting
      for (int i = 1; i < n_stars; ++i)
        j_cdf_stars[i] += j_cdf_stars[i - 1]; // Turn the pdf into a cdf
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_cdf_stars[i] *= _1_L_tot;          // Normalize cdf
        j_weights_stars[i] = _1_n_photons;   // Equal weights
      }
    } else {                                 // Apply luminosity boosting
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_weights_stars[i] = j_cdf_stars[i] * _1_L_tot;  // Copy the normalized pdf
        if (j_weights_stars[i] > 0.)         // Apply power law boost
          j_cdf_stars[i] = pow(j_weights_stars[i], j_exp); // pdf^j_exp (unnormalized)
        else
          j_cdf_stars[i] = 0.;               // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(j_cdf_stars);
      const double dn_photons = double(n_photons);
      #pragma omp parallel for
      for (int i = 0; i < n_stars; ++i) {
        j_cdf_stars[i] *= _1_j_exp_sum;      // pdf^j_exp (normalized)
        if (j_cdf_stars[i] > 0.)
          j_weights_stars[i] /= j_cdf_stars[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
      }
      for (int i = 1; i < n_stars; ++i)
        j_cdf_stars[i] += j_cdf_stars[i - 1]; // Turn the pdf into a cdf
    }

    // Populate a lookup table to quickly find cdf indices (stars)
    const double half_cdf = 0.5 / double(n_cdf_stars); // Half interval width
    for (int i = 0, star = 0; i < n_cdf_stars; ++i) {
      const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
      while (j_cdf_stars[star] <= frac_cdf && star < n_stars - 1)
        star++;                              // Move to the right bin ( --> )
      j_map_stars[i] = star;                 // Lookup table for insertion
    }
  }

  // Calculate group and subhalo properties
  if constexpr (output_groups) {
    if (n_groups > 0) {
      if (star_based_emission) {
        add_L_halo(L_stars_grp, L2_stars_grp, n_stars_eff_grp, L_grp);
        if (output_grp_vir)
          add_L_halo(L_stars_grp_vir, L2_stars_grp_vir, n_stars_eff_grp_vir, L_grp_vir);
        if (output_grp_gal)
          add_L_halo(L_stars_grp_gal, L2_stars_grp_gal, n_stars_eff_grp_gal, L_grp_gal);
      }
      if (cell_based_emission) {
        add_L_halo(L_gas_grp, L2_gas_grp, n_cells_eff_grp, L_grp);
        if (output_grp_vir)
          add_L_halo(L_gas_grp_vir, L2_gas_grp_vir, n_cells_eff_grp_vir, L_grp_vir);
        if (output_grp_gal)
          add_L_halo(L_gas_grp_gal, L2_gas_grp_gal, n_cells_eff_grp_gal, L_grp_gal);
      }
    }
    if (n_ugroups > 0) {
      if (star_based_emission)
        add_L_halo(L_stars_ugrp, L2_stars_ugrp, n_stars_eff_ugrp, L_ugrp);
      if (cell_based_emission)
        add_L_halo(L_gas_ugrp, L2_gas_ugrp, n_cells_eff_ugrp, L_ugrp);
    }
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {
      if (star_based_emission) {
        add_L_halo(L_stars_sub, L2_stars_sub, n_stars_eff_sub, L_sub);
        if (output_grp_vir)
          add_L_halo(L_stars_sub_vir, L2_stars_sub_vir, n_stars_eff_sub_vir, L_sub_vir);
        if (output_grp_gal)
          add_L_halo(L_stars_sub_gal, L2_stars_sub_gal, n_stars_eff_sub_gal, L_sub_gal);
      }
      if (cell_based_emission) {
        add_L_halo(L_gas_sub, L2_gas_sub, n_cells_eff_sub, L_sub);
        if (output_sub_vir)
          add_L_halo(L_gas_sub_vir, L2_gas_sub_vir, n_cells_eff_sub_vir, L_sub_vir);
        if (output_sub_gal)
          add_L_halo(L_gas_sub_gal, L2_gas_sub_gal, n_cells_eff_sub_gal, L_sub_gal);
      }
    }
    if (n_usubhalos > 0) {
      if (star_based_emission)
        add_L_halo(L_stars_usub, L2_stars_usub, n_stars_eff_usub, L_usub);
      if (cell_based_emission)
        add_L_halo(L_gas_usub, L2_gas_usub, n_cells_eff_usub, L_usub);
    }
  }

  // Calculate the mixed source pdf based on luminosities
  {
    // Setup mixed source properties (names, flags, and probabilities)
    const array<string, MAX_SOURCE_TYPES> source_names = {"cells", "stars", "doublet_cells", "point", "plane_cont", "sphere_cont", "shell_cont", "shell_line", "shock"};
    const array<bool, MAX_SOURCE_TYPES> source_flags = {cell_based_emission, star_based_emission, doublet_cell_based_emission, point_source, plane_cont_source, sphere_cont_source, shell_cont_source, shell_line_source, shock_source};
    mixed_cdf = mixed_pdf = {L_rec + L_col + L_sp, L_cont + L_HII, Lp_col, L_point, L_plane_cont, L_sphere_cont, L_shell_cont, L_shell_line, L_shock}; // Unnormalized pdf
    n_source_types = 0;
    for (int i = 0; i < MAX_SOURCE_TYPES; ++i)
      if (source_flags[i])
        n_source_types++;
    if (n_source_types == 0)
      root_error("No source types have been specified.");

    // Same procedure as above
    const double _1_L_tot = 1. / omp_sum(mixed_pdf);
    if (j_exp == 1.) {                       // No luminosity boosting
      for (int i = 1; i < MAX_SOURCE_TYPES; ++i)
        mixed_cdf[i] += mixed_cdf[i - 1];    // Turn the pdf into a cdf
      for (int i = 0; i < MAX_SOURCE_TYPES; ++i) {
        mixed_pdf[i] *= _1_L_tot;            // Normalize pdf
        mixed_cdf[i] *= _1_L_tot;            // Normalize cdf
        mixed_weights[i] = 1.;               // Equal weights (no boost)
      }
    } else {                                 // Apply luminosity boosting
      for (int i = 0; i < MAX_SOURCE_TYPES; ++i) {
        mixed_weights[i] = mixed_pdf[i] *= _1_L_tot; // Copy the normalized pdf
        if (mixed_pdf[i] > 0.)               // Apply power law boost
          mixed_cdf[i] = pow(mixed_pdf[i], j_exp); // pdf^j_exp (unnormalized)
        else
          mixed_cdf[i] = 0.;                 // No probability
      }
      const double _1_j_exp_sum = 1. / omp_sum(mixed_cdf);
      for (int i = 0; i < MAX_SOURCE_TYPES; ++i) {
        mixed_cdf[i] *= _1_j_exp_sum;        // pdf^j_exp (normalized)
        if (mixed_cdf[i] > 0.)
          mixed_weights[i] /= mixed_cdf[i];  // weight = pdf / pdf^j_exp
      }
      for (int i = 1; i < MAX_SOURCE_TYPES; ++i)
        mixed_cdf[i] += mixed_cdf[i - 1];    // Turn the pdf into a cdf
    }

    // Setup masked versions
    for (int i = 0; i < MAX_SOURCE_TYPES; ++i)
      if (source_flags[i]) {
        source_types_mask.push_back(i);
        source_names_mask.push_back(source_names[i]);
        mixed_pdf_mask.push_back(mixed_pdf[i]);
        mixed_cdf_mask.push_back(mixed_cdf[i]);
        mixed_weights_mask.push_back(mixed_weights[i]);
      }
  }

  // Reserve photon output buffers
  if (output_photons) {
    source_ids.resize(n_photons);            // Emission cell or star index
    if (n_source_types > 1)
      source_types.resize(n_photons);        // Emission type specifier
    source_weights.resize(n_photons);        // Photon weight at emission
    if (output_n_scat)
      n_scats.resize(n_photons);             // Number of scattering events
    if (output_collisions)
      f_cols.resize(n_photons);              // Collisional excitation fraction
    freqs.resize(n_photons);                 // Frequency at escape [freq_units]
    weights.resize(n_photons);               // Photon weight at escape
    if (output_source_position)
      source_positions.resize(n_photons);    // Position at emission [cm]
    positions.resize(n_photons);             // Position at escape [cm]
    directions.resize(n_photons);            // Direction at escape
    if (output_path_length)
      path_lengths.resize(n_photons);        // Path length [cm]
  }
  // Reserve remaining star buffers
  if (output_stars) {
    source_weight_stars.resize(n_stars);     // Photon weight at emission for each star
    weight_stars.resize(n_stars);            // Photon weight at escape for each star
  }
  // Reserve remaining cell buffers
  if (output_cells) {
    source_weight_cells.resize(n_cells);     // Photon weight at emission for each cell
    weight_cells.resize(n_cells);            // Photon weight at escape for each cell
    if (doublet_cell_based_emission) {
      source_weight_doublet_cells.resize(n_cells); // Doublet photon weight at emission for each cell
      weight_doublet_cells.resize(n_cells);  // Doublet photon weight at escape for each cell
    }
  }
  if constexpr (output_energy_density)
    u_rad = vector<double>(n_cells);         // Radiation energy density [erg/cm^3]
  if constexpr (SPHERICAL) {
    if constexpr (output_acceleration)
      a_rad_r = vector<double>(n_cells);     // Radial radiation acceleration [cm/s^2]
    if constexpr (output_acceleration_scat)
      a_rad_r_scat = vector<double>(n_cells); // Scattering-based radiation acceleration [cm/s^2]
    if constexpr (output_pressure)
      P_rad_r = vector<double>(n_cells);     // Radial radiation pressure [erg/cm^3]
    if constexpr (output_f_src_r)
      f_src_r = vector<double>(n_cells);     // Cumulative source fraction
    if constexpr (output_tau_dust_f_esc)
      tau_dust_f_esc = vector<double>(n_tau_dust_sims); // Dust escape fraction
    if constexpr (output_f_esc_r)
      f_esc_r = vector<double>(n_cells);     // Cumulative escape fraction
    if constexpr (output_rescaled_f_esc_r)
      rescaled_f_esc_r = vector<double>(n_cells); // Rescaled cumulative escape fraction
    if constexpr (output_trap_r)
      trap_r = vector<double>(n_cells);      // Cumulative t_trap / t_light
    if constexpr (output_M_F_r)
      M_F_r = vector<double>(n_cells);       // Cumulative force multiplier
    if constexpr (output_M_F_r_scat)
      M_F_r_scat = vector<double>(n_cells);  // Scattering-based cumulative force multiplier
    if constexpr (output_M_F_src)
      M_F_src = vector<double>(n_cells);     // Cumulative force multiplier (by source)
    if constexpr (output_M_F_src_scat)
      M_F_src_scat = vector<double>(n_cells); // Scattering-based cumulative force multiplier (by source)
    if constexpr (output_P_u_r)
      P_u_r = vector<double>(n_cells);       // Cumulative pressure-to-energy density ratio (P/u)
  } else {
    if constexpr (output_acceleration)
      a_rad = vector<Vec3>(n_cells);         // Radiation acceleration [cm/s^2]
    if constexpr (output_acceleration_scat)
      a_rad_scat = vector<Vec3>(n_cells);    // Scattering-based radiation acceleration [cm/s^2]
  }

  // Calculate the emission properties (without continuum)
  const double L_geo_cont = L_plane_cont + L_sphere_cont + L_shell_cont + L_shock;
  Ndot_tot = (L_tot - L_cont - L_geo_cont) / E0; // erg/s -> photons/s

  // Calculate exit factors
  if (doppler_frequency && (!v_grp.empty() || !v_sub.empty()))
    root_error("Doppler frequency shift is not allowed with group or subhalo velocities.");
  const double sqrtT_exit = sqrt(T_exit);
  a_exit = a_sqrtT / sqrtT_exit;
  DnuD_exit = DnuD_div_sqrtT * sqrtT_exit;
  vth_div_c_exit = vth_div_sqrtT * sqrtT_exit / c;
  neg_vth_kms_exit = -vth_div_sqrtT * sqrtT_exit / km;
  if (exit_wrt_com) { // Allow photons to exit w.r.t. the center of mass frame
    double m_tot = 0.;                       // Total mass [g]
    Vec3 mv_tot = 0;                         // Total momentum [g cm/s]
    #pragma omp parallel for reduction(+:m_tot) reduction(+:mv_tot)
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i))
        continue;
      const double m_i = rho[i] * VOLUME(i); // m = rho V
      m_tot += m_i;                          // sum(m)
      mv_tot += m_i * v[i];                  // sum(m * v)
    }
    camera_motion = mv_tot / m_tot;          // sum(m * v) / sum(m)
  }

  // Convert the exit velocity to code units: u = v / vth
  camera_motion /= vth_div_sqrtT * sqrtT_exit;

  constexpr double gamma = 5. / 3.;          // Adiabatic index
  constexpr double turb_coef = km * km / (gamma * kB); // Density scaling [K cm^3/g]

  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell_strict(i)) {
      v[i] = 0.;                             // Velocity
      if constexpr (cell_velocity_gradients)
        dvdr[i] = v0[i] = 0.;                // Velocity gradient and extrapolation
      continue;
    }

    // Convert from cm/s to unitless velocity: u = v / vth = v / sqrt(2*kB*T/m_carrier)
    double T_turb_local = T_turb;            // Minimum microturbulence value
    if (scaled_microturb && turb_coef * rho[i] > T_turb)
      T_turb_local = turb_coef * rho[i];     // Update T_turb based on scaling
    const double _1_vth = 1. / (vth_div_sqrtT * sqrt(T[i] + T_turb_local));
    v[i] *= _1_vth;
    if constexpr (cell_velocity_gradients) {
      dvdr[i] *= _1_vth;                     // Velocity gradient
      v0[i] *= _1_vth;                       // Velocity extrapolation
    }
  }

  if constexpr (dynamical_core_skipping)
    atau_NL_calculation();                   // Note: Needs v in units of vth

  if constexpr (output_radial_flow)
    radial_line_flow.setup();                // Radial flows
  if constexpr (output_group_flows)
    for (auto& group_line_flow : group_line_flows)
      group_line_flow.setup();               // Group flows
  if constexpr (output_subhalo_flows)
    for (auto& subhalo_line_flow : subhalo_line_flows)
      subhalo_line_flow.setup();             // Subhalo flows
}
