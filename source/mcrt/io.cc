/**************
 * mcrt/io.cc *
 **************

 * All I/O operations related to line radiative transfer.

*/

#include "proto.h"
#include "MCRT.h"
#include "../io_hdf5.h" // HDF5 read/write functions

dust_strings get_dust_tags(); // Dust species trailing tags
bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
tuple<double, double> min_max(vector<double>& vec); // min/max of a vector
void print_min_max(string name, vector<double> vec, string units = "", double conv = 1.);
void print_min_max(string name, vector<Vec3> vec, string units = "", double conv = 1.);
double ray_integral(vector<double>& field, Vec3 point, Vec3 direction, int cell); // Integrate a scalar field
void write_sim_info(const H5File& f); // Write general simulation information

/* Functions to print a vector as a scalar if the components are equal. */
static void pprint(const int a, const int b) {
  cout << (a == b ? to_string(a) : "(" + to_string(a) + ", " + to_string(b) + ")");
}
static void pprint(const Vec2& vec, const double conv = 1., const string units = "") {
  if (vec.x == vec.y)
    cout << vec.x / conv << " " << units;
  else
    cout << "(" << vec.x / conv << ", " << vec.y / conv << ") " << units;
}

/* Print general camera parameters. */
void Simulation::print_cameras() {
  cout << "\nCamera parameters:"
       << "\n  n_cameras  = " << n_cameras
       << "\n  n_pixels   = "; pprint(nx_pixels, ny_pixels);
  if (n_side > 0)
    cout << "\n  n_side     = " << n_side << " (Note: Healpix in RING ordering)";
  cout << "\n  center     = " << camera_center / kpc << " kpc" << endl;
  if (camera_north.z < 1.)
    cout << "  north      = " << camera_north << endl;
  if (verbose) {
    print("directions", camera_directions);
    print("x-axes", camera_xaxes);
    print("y-axes", camera_yaxes);
  }
  if (aperture_radius > 0.)
    cout << "  aperture  = " << aperture_radius / kpc << " kpc (radius)" << endl;

  // Print image information
  cout << "\nImage information:"
       << "\n  width      = "; pprint(image_widths, kpc, "kpc");
  cout << "\n  radius     = "; pprint(image_radii, kpc, "kpc");
  cout << "\n  pixel dx   = "; pprint(pixel_widths, pc, "pc");
  if (cosmological) {
    cout << "\n  pixel dl   = "; pprint(pixel_arcsecs, 1., "arcsec");
    cout << "\n  pixel dA   = " << pixel_arcsec2 << " arcsec^2";
  }
  cout << endl;

  // Print slit information
  if (output_slits) {
    cout << "\nSlit information:"
         << "\n  aperture   = " << slit_aperture /kpc << " kpc";
    if (cosmological)
      cout << " = " << slit_aperture_arcsec << " arcsec";
    cout << "\n  width      = " << slit_width / kpc << " kpc"
         << "\n  radius     = " << slit_radius / kpc << " kpc"
         << "\n  pixel dx   = " << slit_pixel_width / pc << " pc";
    if (cosmological)
      cout << "\n  pixel dl   = " << slit_pixel_arcsec << " arcsec";
    cout << endl;
  }

  // Print cube information
  if (output_cubes && (n_cube_pixels != n_pixels || cube_widths != image_widths)) {
    cout << "\nCube information:"
         << "\n  width      = "; pprint(cube_widths, kpc, "kpc");
    cout << "\n  radius     = "; pprint(cube_radii, kpc, "kpc");
    cout << "\n  pixel dx   = "; pprint(cube_pixel_widths, pc, "pc");
    if (cosmological) {
      cout << "\n  pixel dl   = "; pprint(cube_pixel_arcsecs, 1., "arcsec");
      cout << "\n  pixel dA   = " << cube_pixel_arcsec2 << " arcsec^2";
    }
    cout << endl;
  }

  if (output_radial_images) {
    cout << "\nRadial image information:"
         << "\n  n_pixels   = " << n_radial_pixels
         << "\n  radius     = " << radial_image_radius / kpc << " kpc"
         << "\n  pixel dr   = " << radial_pixel_width / pc << " pc" << endl;
    if (cosmological)
      cout << "  pixel dl   = " << radial_pixel_arcsec << " arcsec" << endl;
  }

  if (output_radial_cubes && (n_radial_cube_pixels != n_radial_pixels || radial_cube_radius != radial_image_radius)) {
    cout << "\nRadial cube information:"
         << "\n  n_pixels   = " << n_radial_cube_pixels
         << "\n  radius     = " << radial_cube_radius / kpc << " kpc"
         << "\n  pixel dr   = " << radial_cube_pixel_width / pc << " pc" << endl;
    if (cosmological)
      cout << "  pixel dl   = " << radial_cube_pixel_arcsec << " arcsec" << endl;
  }
}

/* Print line constants and parameters. */
void Simulation::print_line_info() {
  cout << "\nLine properties: " << line << endl;
  if (verbose) {
    cout << "  E0         = " << E0 << " erg"
         << "\n  A21        = " << A21 << " Hz"
         << "\n  f12        = " << f12
         << "\n  g12        = " << g12
         << "\n  T0         = " << T0 << " K"
         << "\n  nu0        = " << nu0 << " Hz"
         << "\n  lambda0    = " << lambda0 << " angstrom"
         << "\n  B21        = " << B21 << " cgs"
         << "\n  DnuL       = " << DnuL << " Hz"
         << "\n  p_dest     = " << p_dest;
    if (P2F > 0.)
      cout << "\n  E0F        = " << E0F << " erg"
           << "\n  A2F        = " << A2F << " Hz"
           << "\n  P2F        = " << P2F
           << "\n  nu0F       = " << nu0F << " Hz"
           << "\n  lambda0F   = " << lambda0F << " angstrom";
    if (v_turb > 0.) {
      cout << "\n  v_turb     = " << v_turb / km << " km/s"
           << "\n  T_turb     = " << T_turb << " K";
      if (scaled_microturb)
        cout << "  (scales with density)";
    }
    if (resonant_scattering)
      cout << "\n  gDa        = " << gDa;
    cout << "\n  m_carrier  = " << m_carrier << " g" << endl;
    if (E0p > 0.) {
      cout << "\n  E0p        = " << E0p << " erg"
           << "\n  A21p       = " << A21p << " Hz"
           << "\n  f12p       = " << f12p
           << "\n  g12p       = " << g12p
           << "\n  nu0p       = " << nu0p << " Hz"
           << "\n  lambda0p   = " << lambda0p << " angstrom";
      if (P2Fp > 0.)
        cout << "\n  E0Fp       = " << E0Fp << " erg"
             << "\n  A2Fp       = " << A2Fp << " Hz"
             << "\n  P2Fp       = " << P2Fp
             << "\n  nu0Fp      = " << nu0Fp << " Hz"
             << "\n  lambda0Fp  = " << lambda0Fp << " angstrom";
      if (resonant_scattering)
        cout << "\n  gDap       = " << gDap;
      cout << endl;
    }
  }
}

/* Print additional line radiative transfer information. */
void MCRT::print_info() {
  // Print line constants and parameters
  print_line_info();

  // Calculate the frequency resolution
  double x_res, nu_res, l_res, Dv_res, R_res;
  const double sqrtT_point = sqrt(T[i_point] + T_turb);
  const double vth_0 = vth_div_sqrtT * sqrtT_point;
  const double DnuD_0 = DnuD_div_sqrtT * sqrtT_point;
  if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
    x_res = freq_bin_width;
    nu_res = x_res * DnuD_0;
    l_res = x_res * lambda0 * vth_0 / c;
    Dv_res = x_res * vth_0 / km;
    R_res = c / (x_res * vth_0);
  } else { // Velocity offset (km/s)
    Dv_res = freq_bin_width;
    x_res = Dv_res * km / vth_0;
    nu_res = Dv_res * km * nu0 / c;
    l_res = Dv_res * km * lambda0 / c;
    R_res = c / (Dv_res * km);
  }

  // Print photon parameters
  string freq_units = ((doppler_frequency) ? "" : "km/s");
  cout << "\nPhoton parameters:"
       << "\n  n_photons  = " << n_photons
       << "\n  T_exit     = " << T_exit << " K"
       << "\n  v_exit     = " << -neg_vth_kms_exit * camera_motion << " km/s  "
       << (exit_wrt_com ? "(center of mass)" : "(user specified)") << endl;
  if (recombinations || collisions || spontaneous) {
    const Vec3 sigma_r = sqrt(r2_light - r_light * r_light); // Emission position standard deviation
    const Vec3 sigma_v = sqrt(v2_light - v_light * v_light); // Emission velocity standard deviation
    cout << "  r_light    = " << r_light / kpc << " kpc\n"
         << "  sigma(r)   = " << sigma_r / kpc << " kpc"
         << ", |sigma(r)| = " << norm(sigma_r) / kpc << " kpc\n"
         << "  v_light    = " << v_light / km << " km/s\n"
         << "  sigma(v)   = " << sigma_v / km << " km/s"
         << ", |sigma(v)| = " << norm(sigma_v) / km << " km/s" << endl;
  }
  #if spherical_escape
    cout << "  r_escape   = " << escape_radius / kpc << " kpc\n"
         << "  r_emission = " << emission_radius / kpc << " kpc\n"
         << "  esc center = " << escape_center / kpc << " kpc" << endl;
  #endif
  #if box_escape
    cout << "  box emission = [" << emission_box[0] / kpc << ", " << emission_box[1] / kpc << "] kpc\n"
         << "  box escape = [" << escape_box[0] / kpc << ", " << escape_box[1] / kpc << "] kpc" << endl;
  #endif
  #if streaming_escape
    cout << "  streaming  = " << max_streaming / kpc << " kpc" << endl;
  #endif

  cout << "\nDust properties:" << (multiple_dust_species ? " [" : " ");
  for (int i = 0; i < n_dust_species; ++i) {
    if (i > 0) cout << ", ";
    if (dust_models[i] != "")
      cout << get_filename(dust_models[i]);
  }
  cout << (multiple_dust_species ? "] models" : " model") << endl;
  if (sigma_dust > 0.)
    cout << "  sigma_dust = " << sigma_dust << " cm^2/H (Z)" << endl;
  else
    cout << "  kappa_dust = " << kappa_dust << " cm^2/g (D)" << endl;
  if (kappa_dust > 0. || sigma_dust > 0.) {
    cout << "  albedo     = " << albedo
         << "\n  g_dust     = " << g_dust << endl;
    if (f_ion < 1.) cout << "  f_ion      = " << f_ion << endl;
    if (f_dust > 0.) cout << "  f_dust     = " << f_dust << endl;
    #if graphite_scaled_PAH
      cout << "  f_PAH      = " << f_PAH << endl;
    #endif
    if (T_sputter < 1e6) cout << "  T_sputter  = " << T_sputter << " K" << endl;
  }

  if (point_source) {
    cout << "\nEmission includes a point source:"
         << "\n  r_point    = " << r_point / pc << " pc" << endl;
    if (verbose)
      cout << "  L_point    = " << L_point << " erg/s"
         << "\n  i_point    = " << i_point
         << "\nConsistency check:"
         << "\n  r[i_point] = " << cell_center(i_point) / pc << " pc"
         << "\n  v[i_point] = " << v[i_point] * (vth_div_sqrtT * sqrtT_point / km) << " km/s" << endl;
  }

  if (plane_cont_source) {
    cout << "\nEmission includes a plane continuum source:"
         << "\n  direction  = " << plane_direction
         << "\n  position   = " << plane_position / kpc << " kpc"
         << "\n  center     = (" << plane_center_1 / kpc << ", " << plane_center_2 / kpc << ") kpc"
         << "\n  radii      = (" << plane_radius_1 / kpc << ", " << plane_radius_2 / kpc << ") kpc"
         << "\n  plane area = " << plane_area / (kpc * kpc) << " kpc^2"
         << "\n  S_plane    = " << S_plane_cont << " erg/s/cm^2/angstrom"
         << "\n  F_plane    = " << F_plane_cont << " erg/s/angstrom"
         << "\n  L_plane    = " << L_plane_cont << " erg/s" << endl;
    if (plane_beam)
      cout << "  Using a beam instead of a rectangle." << endl;
  }

  if (sphere_cont_source) {
    cout << "\nEmission includes a sphere continuum source:"
         << "\n  radius     = " << sphere_radius_cont / pc << " pc"
         << "\n  center     = " << sphere_center_cont / pc << " pc" << endl;
    if (verbose)
      cout << "  L_sphere   = " << L_sphere_cont << " erg/s"
         << "\n  i_sphere   = " << i_sphere_cont
         << "\nConsistency check:"
         << "\n r[i_sphere] = " << cell_center(i_sphere_cont) / pc << " pc" << endl;
  }

  if (shell_cont_source) {
    cout << "\nEmission includes a shell continuum source:"
         << "\n  r_shell    = " << r_shell_cont / pc << " pc";
    if (shell_blackbody)
      cout << "\n  T_shell    = " << T_shell << " K";
    cout << "\n  L_shell    = " << L_shell_cont << " erg/s" << endl;
  }

  if (shell_line_source) {
    cout << "\nEmission includes a shell line source:"
         << "\n  r_shell    = " << r_shell_line / pc << " pc"
         << "\n  L_shell    = " << L_shell_line << " erg/s" << endl;
  }

  if (shock_source) {
    cout << "\nEmission includes a shock source:"
         << "\n  r_shock    = " << r_shock / pc << " pc";
    if (shock_blackbody)
      cout << "\n  T_shock    = " << T_shock << " K";
    cout << "\n  L_shock    = " << L_shock << " erg/s" << endl;
  }

  const double L_tot_cont = L_cont + L_plane_cont + L_sphere_cont + L_shell_cont + L_shock; // Continuum luminosity [erg/s]
  const double L_tot_no_cont = L_tot - L_tot_cont; // Non-continuum luminosity [erg/s]

  if (unresolved_HII)
    cout << "\nEmission includes unresolved HII regions: (L_HII/L_tot = "
         << L_HII / L_tot_no_cont << ")\n  <n_stars>  = " << n_stars_eff;
  if (cell_based_emission) {
    if (recombinations)
      cout << "\nEmission includes recombination sources:  (L_rec/L_tot = "
           << L_rec / L_tot_no_cont << ")";
    if (collisions)
      cout << "\nEmission includes collisional excitation: (L_col/L_tot = "
           << L_col / L_tot_no_cont << ")";
    if (collisions_limiter > 0.)
      cout << "\nNote: Collisional excitation is limited to " << collisions_limiter
           << " times the photoheating rate";
    if (spontaneous)
      cout << "\nEmission includes spontaneous emission:  (L_sp/L_tot = "
           << L_sp / L_tot_no_cont << ")";
    cout << "\n  <n_cells>  = " << n_cells_eff << endl;
    if (verbose) {
      print("j_cdf", j_cdf_cells);
      print("j_weights", j_weights_cells);
      if (output_collisions)
        print("f_col", f_col_cells);
    }
  }
  if (doublet_cell_based_emission && collisions) {
    cout << "\nEmission includes doublet collisional excitation: (Lp_col/L_tot = "
         << Lp_col / L_tot_no_cont << ")\n  (L_col + Lp_col) / L_tot = "
         << (L_col + Lp_col) / L_tot_no_cont << "  |  Lp_col / (L_col + Lp_col) = "
         << Lp_col / (L_col + Lp_col) << "\n  <n'_cells> = " << n_cells_effp << endl;
    if (verbose) {
      print("j_cdf", jp_cdf_cells);
      print("j_weights", jp_weights_cells);
    }
  }

  if (continuum)
    cout << "\nEmission includes stellar continuum sources:"
         << "\n  <n_stars>  = " << n_stars_eff
         << "\n  L_cont     = " << L_tot_cont << " erg/s"
         << "\n  L_cont/ang = " << L_tot_cont / l_cont_res << " erg/s/angstrom"
         << "\n  M_cont     = " << M_cont
         << "\n  Dv (range) = [" << Dv_cont_min << ", "
                                 << Dv_cont_max << "] km/s"
         << "\n  lambda res = " << l_cont_res << " angstoms" << endl;
  if (star_based_emission && verbose) {
    print("j_cdf", j_cdf_stars);
    print("j_weights", j_weights_stars);
  }

  if (n_source_types > 1)
    cout << "\nMixed source calculation uses: " << to_string(source_names_mask)
         << "\n  mixed pdf  = " << to_string(mixed_pdf_mask)
         << "\n  boost cdf  = " << to_string(mixed_cdf_mask)
         << "\n   weights   = " << to_string(mixed_weights_mask) << endl;

  cout << "\nStrength of the source:" << (continuum ? " (without continuum)" : "")
       << "\n  Luminosity = " << L_tot_no_cont << " erg/s"
       << "\n             = " << L_tot_no_cont / Lsun << " Lsun"
       << "\n  Prod. Rate = " << Ndot_tot << " photons/s" << endl;
  if (cosmological) {
    const double flux_tot = L_tot_no_cont / (4. * M_PI * d_L * d_L);
    cout << "  Flux (Int) = " << flux_tot << " erg/s/cm^2" << endl;
  }

  // Print camera parameters
  if (have_cameras) {
    string freq_units = ((doppler_frequency) ? "" : "km/s");
    cout << "\nFrequency resolution parameters:"
         << "\n  n_bins     = " << n_bins
         << "\n  Note: Frequencies are given in the rest frame.";
    if (adjust_camera_frequency)
      cout << "\n  Note: Applying camera frequency offsets.";
    cout << "\n  freq_type  = " << ((doppler_frequency) ? "Doppler Frequency" : "Velocity Offset")
         << "\n  freq_lims  = (" << freq_min << ", " << freq_max << ") " << freq_units
         << "\n  resolution = " << freq_bin_width << " " << freq_units;
    if (doppler_frequency)
      cout << "\n    |  x_res = " << x_res;
    cout << "\n    | nu_res = " << nu_res << " Hz"
         << "\n    |  l_res = " << l_res << " angstrom"
         << "\n    | Dv_res = " << Dv_res << " km/s"
         << "\n    |  R_res = " << R_res << endl;

    if (output_slits && (n_slit_bins != n_bins || slit_freq_min != freq_min || slit_freq_max != freq_max)) {
      double slit_x_res, slit_nu_res, slit_l_res, slit_Dv_res, slit_R_res;
      if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
        slit_x_res = slit_freq_bin_width;
        slit_nu_res = slit_x_res * DnuD_0;
        slit_l_res = slit_x_res * lambda0 * vth_0 / c;
        slit_Dv_res = slit_x_res * vth_0 / km;
        slit_R_res = c / (slit_x_res * vth_0);
      } else { // Velocity offset (km/s)
        slit_Dv_res = slit_freq_bin_width;
        slit_x_res = slit_Dv_res * km / vth_0;
        slit_nu_res = slit_Dv_res * km * nu0 / c;
        slit_l_res = slit_Dv_res * km * lambda0 / c;
        slit_R_res = c / (slit_Dv_res * km);
      }
      cout << "\nSlit frequency resolution parameters:"
           << "\n  n_bins     = " << n_slit_bins
           << "\n  freq_lims  = (" << slit_freq_min << ", " << slit_freq_max << ") " << freq_units
           << "\n  resolution = " << slit_freq_bin_width << " " << freq_units;
      if (doppler_frequency)
        cout << "\n    |  x_res = " << slit_x_res;
      cout << "\n    | nu_res = " << slit_nu_res << " Hz"
           << "\n    |  l_res = " << slit_l_res << " angstrom"
           << "\n    | Dv_res = " << slit_Dv_res << " km/s"
           << "\n    |  R_res = " << slit_R_res << endl;
    }

    if (output_cubes && (n_cube_bins != n_bins || cube_freq_min != freq_min || cube_freq_max != freq_max)) {
      double cube_x_res, cube_nu_res, cube_l_res, cube_Dv_res, cube_R_res;
      if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
        cube_x_res = cube_freq_bin_width;
        cube_nu_res = cube_x_res * DnuD_0;
        cube_l_res = cube_x_res * lambda0 * vth_0 / c;
        cube_Dv_res = cube_x_res * vth_0 / km;
        cube_R_res = c / (cube_x_res * vth_0);
      } else { // Velocity offset (km/s)
        cube_Dv_res = cube_freq_bin_width;
        cube_x_res = cube_Dv_res * km / vth_0;
        cube_nu_res = cube_Dv_res * km * nu0 / c;
        cube_l_res = cube_Dv_res * km * lambda0 / c;
        cube_R_res = c / (cube_Dv_res * km);
      }
      cout << "\nCube frequency resolution parameters:"
           << "\n  n_bins     = " << n_cube_bins
           << "\n  freq_lims  = (" << cube_freq_min << ", " << cube_freq_max << ") " << freq_units
           << "\n  resolution = " << cube_freq_bin_width << " " << freq_units;
      if (doppler_frequency)
        cout << "\n    |  x_res = " << cube_x_res;
      cout << "\n    | nu_res = " << cube_nu_res << " Hz"
           << "\n    |  l_res = " << cube_l_res << " angstrom"
           << "\n    | Dv_res = " << cube_Dv_res << " km/s"
           << "\n    |  R_res = " << cube_R_res << endl;
    }

    if (output_radial_cubes && (n_radial_cube_bins != n_bins || radial_cube_freq_min != freq_min || radial_cube_freq_max != freq_max)) {
      double radial_cube_x_res, radial_cube_nu_res, radial_cube_l_res, radial_cube_Dv_res, radial_cube_R_res;
      if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
        radial_cube_x_res = radial_cube_freq_bin_width;
        radial_cube_nu_res = radial_cube_x_res * DnuD_0;
        radial_cube_l_res = radial_cube_x_res * lambda0 * vth_0 / c;
        radial_cube_Dv_res = radial_cube_x_res * vth_0 / km;
        radial_cube_R_res = c / (radial_cube_x_res * vth_0);
      } else { // Velocity offset (km/s)
        radial_cube_Dv_res = radial_cube_freq_bin_width;
        radial_cube_x_res = radial_cube_Dv_res * km / vth_0;
        radial_cube_nu_res = radial_cube_Dv_res * km * nu0 / c;
        radial_cube_l_res = radial_cube_Dv_res * km * lambda0 / c;
        radial_cube_R_res = c / (radial_cube_Dv_res * km);
      }
      cout << "\nRadial cube frequency resolution parameters:"
           << "\n  n_bins     = " << n_radial_cube_bins
           << "\n  freq_lims  = (" << radial_cube_freq_min << ", " << radial_cube_freq_max << ") " << freq_units
           << "\n  resolution = " << radial_cube_freq_bin_width << " " << freq_units;
      if (doppler_frequency)
        cout << "\n    |  x_res = " << radial_cube_x_res;
      cout << "\n    | nu_res = " << radial_cube_nu_res << " Hz"
           << "\n    |  l_res = " << radial_cube_l_res << " angstrom"
           << "\n    | Dv_res = " << radial_cube_Dv_res << " km/s"
           << "\n    |  R_res = " << radial_cube_R_res << endl;
    }

    // Radial flow statistics
    if constexpr (output_radial_flow) {
      cout << "\nSetup for radial flow:"
           << "\n  n_rad_bins = " << radial_line_flow.n_radial_bins << endl;
      print("rad_edges", radial_line_flow.radial_edges, "pc", pc);
    }

    // Group flow statistics
    if constexpr (output_group_flows) {
      if (n_groups > 0) {
        cout << "\nSetup for group flows: [n_radial_bins = "
            << group_line_flows[0].n_radial_bins << "]" << endl;
        const int n_groups_print = (n_groups > 5) ? 5 : n_groups;
        for (int i = 0; i < n_groups_print; ++i) {
          string name = "g" + to_string(group_id[i]) + " radii";
          print(name, group_line_flows[i].radial_edges, "kpc", kpc);
        }
        if (n_groups > 5)
          cout << "  ... and " << n_groups - 5 << " more groups." << endl;
      }
    }

    // Subhalo flow statistics
    if constexpr (output_subhalo_flows) {
      if (n_subhalos > 0) {
        cout << "\nSetup for subhalo flows: [n_radial_bins = "
            << subhalo_line_flows[0].n_radial_bins << "]" << endl;
        const int n_subhalos_print = (n_subhalos > 5) ? 5 : n_subhalos;
        for (int i = 0; i < n_subhalos_print; ++i) {
          string name = "s" + to_string(subhalo_id[i]) + " radii";
          print(name, subhalo_line_flows[i].radial_edges, "kpc", kpc);
        }
        if (n_subhalos > 5)
          cout << "  ... and " << n_subhalos - 5 << " more subhalos." << endl;
      }
    }

    print_cameras();                         // General camera parameters

    if (output_proj_emission || output_proj_attenuation)
      cout << "\nProjection information: (" << (output_proj_emission ?
              (output_proj_attenuation ? "int, ext)" : "int)") : "ext)");
    if (output_proj_cube_emission || output_proj_cube_attenuation)
      cout << "\nProjection cube information: (" << (output_proj_cube_emission ?
              (output_proj_cube_attenuation ? "int, ext)" : "int)") : "ext)");
    if (output_proj_emission || output_proj_attenuation || output_proj_cube_emission || output_proj_cube_attenuation)
      cout << "\n  depth      = " << proj_depth / kpc << " kpc"
           << "\n  radius     = " << proj_radius / kpc << " kpc"
           << "\n  pixel_rtol = " << 100. * pixel_rtol << "%" << endl;
  }

  // Print map parameters
  if (output_map) {
    const int n_pix_map = 12 * n_side_map * n_side_map; // Number of pixels
    cout << "\nHealpix map parameters:"
         << "\n  n_side     = " << n_side_map
         << "\n  n_pix_map  = " << n_pix_map;
  }

  // Print flux map parameters
  if (output_flux_map) {
    const int n_pix_map = 12 * n_side_flux * n_side_flux; // Number of pixels
    cout << "\nHealpix flux map parameters:"
         << "\n  n_side     = " << n_side_flux
         << "\n  n_pix_map  = " << n_pix_map;
    if (n_map_bins != n_bins || map_freq_min != freq_min || map_freq_max != freq_max) {
      string freq_units = ((doppler_frequency) ? "" : "km/s");
      double map_x_res, map_nu_res, map_l_res, map_Dv_res, map_R_res;
      if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
        map_x_res = map_freq_bin_width;
        map_nu_res = map_x_res * DnuD_0;
        map_l_res = map_x_res * lambda0 * vth_0 / c;
        map_Dv_res = map_x_res * vth_0 / km;
        map_R_res = c / (map_x_res * vth_0);
      } else { // Velocity offset (km/s)
        map_Dv_res = map_freq_bin_width;
        map_x_res = map_Dv_res * km / vth_0;
        map_nu_res = map_Dv_res * km * nu0 / c;
        map_l_res = map_Dv_res * km * lambda0 / c;
        map_R_res = c / (map_Dv_res * km);
      }
      cout << "\n  n_bins     = " << n_map_bins
           << "\n  freq_lims  = (" << map_freq_min << ", " << map_freq_max << ") " << freq_units
           << "\n  resolution = " << map_freq_bin_width << " " << freq_units;
      if (doppler_frequency)
        cout << "\n    |  x_res = " << map_x_res
             << "\n    | nu_res = " << map_nu_res << " Hz"
             << "\n    |  l_res = " << map_l_res << " angstrom"
             << "\n    | Dv_res = " << map_Dv_res << " km/s"
             << "\n    |  R_res = " << map_R_res << endl;
    }
  }

  // Print radial map parameters
  if (output_radial_map) {
    const int n_pix_map = 12 * n_side_radial * n_side_radial; // Number of pixels
    cout << "\nHealpix radial map parameters:"
         << "\n  n_side     = " << n_side_radial
         << "\n  n_pix_map  = " << n_pix_map
         << "\n  n_pixels   = " << n_map_pixels
         << "\n  radius     = " << map_radius / kpc << " kpc"
         << "\n  pixel dr   = " << map_pixel_width / pc << " pc" << endl;
    if (cosmological)
      cout << "  pixel dl   = " << map_pixel_arcsec << " arcsec" << endl;
  }

  // Print radial spectral map parameters
  if (output_cube_map) {
    const int n_pix_map = 12 * n_side_cube * n_side_cube; // Number of pixels
    cout << "\n\nHealpix radial cube map parameters:"
         << "\n  n_side     = " << n_side_cube
         << "\n  n_pix_map  = " << n_pix_map
         << "\n  n_pixels   = " << n_cube_map_pixels
         << "\n  radius     = " << cube_map_radius / kpc << " kpc"
         << "\n  pixel dr   = " << cube_map_pixel_width / pc << " pc" << endl;
    if (cosmological)
      cout << "  pixel dl   = " << cube_map_pixel_arcsec << " arcsec" << endl;
    if (n_cube_map_bins != n_bins || cube_map_freq_min != freq_min || cube_map_freq_max != freq_max) {
      string freq_units = ((doppler_frequency) ? "" : "km/s");
      double cube_map_x_res, cube_map_nu_res, cube_map_l_res, cube_map_Dv_res, cube_map_R_res;
      if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
        cube_map_x_res = cube_map_freq_bin_width;
        cube_map_nu_res = cube_map_x_res * DnuD_0;
        cube_map_l_res = cube_map_x_res * lambda0 * vth_0 / c;
        cube_map_Dv_res = cube_map_x_res * vth_0 / km;
        cube_map_R_res = c / (cube_map_x_res * vth_0);
      } else { // Velocity offset (km/s)
        cube_map_Dv_res = cube_map_freq_bin_width;
        cube_map_x_res = cube_map_Dv_res * km / vth_0;
        cube_map_nu_res = cube_map_Dv_res * km * nu0 / c;
        cube_map_l_res = cube_map_Dv_res * km * lambda0 / c;
        cube_map_R_res = c / (cube_map_Dv_res * km);
      }
      cout << "\n  n_bins     = " << n_cube_map_bins
           << "\n  freq_lims  = (" << cube_map_freq_min << ", " << cube_map_freq_max << ") " << freq_units
           << "\n  resolution = " << cube_map_freq_bin_width << " " << freq_units;
      if (doppler_frequency)
        cout << "\n    |  x_res = " << cube_map_x_res
             << "\n    | nu_res = " << cube_map_nu_res << " Hz"
             << "\n    |  l_res = " << cube_map_l_res << " angstrom"
             << "\n    | Dv_res = " << cube_map_Dv_res << " km/s"
             << "\n    |  R_res = " << cube_map_R_res << endl;
    }
  }

  // Print angular cosine (μ=cosθ) parameters
  if (output_mu) {
    cout << "\nAngular cosine (mu) map parameters:"
         << "\n  n_mu       = " << n_mu;
  }

  // Print angular cosine (μ=cosθ) flux parameters
  if (output_flux_mu) {
    cout << "\nAngular cosine (mu) flux parameters:"
         << "\n  n_mu       = " << n_mu_flux;
    if (n_mu_bins != n_bins || mu_freq_min != freq_min || mu_freq_max != freq_max) {
      string freq_units = ((doppler_frequency) ? "" : "km/s");
      double mu_x_res, mu_nu_res, mu_l_res, mu_Dv_res, mu_R_res;
      if (doppler_frequency) { // Doppler frequency: x = (nu - nu0) / Delta_nu_D
        mu_x_res = mu_freq_bin_width;
        mu_nu_res = mu_x_res * DnuD_0;
        mu_l_res = mu_x_res * lambda0 * vth_0 / c;
        mu_Dv_res = mu_x_res * vth_0 / km;
        mu_R_res = c / (mu_x_res * vth_0);
      } else { // Velocity offset (km/s)
        mu_Dv_res = mu_freq_bin_width;
        mu_x_res = mu_Dv_res * km / vth_0;
        mu_nu_res = mu_Dv_res * km * nu0 / c;
        mu_l_res = mu_Dv_res * km * lambda0 / c;
        mu_R_res = c / (mu_Dv_res * km);
      }
      cout << "\n  n_bins     = " << n_mu_bins
           << "\n  freq_lims  = (" << mu_freq_min << ", " << mu_freq_max << ") " << freq_units
           << "\n  resolution = " << mu_freq_bin_width << " " << freq_units;
      if (doppler_frequency)
        cout << "\n    |  x_res = " << mu_x_res
             << "\n    | nu_res = " << mu_nu_res << " Hz"
             << "\n    |  l_res = " << mu_l_res << " angstrom"
             << "\n    | Dv_res = " << mu_Dv_res << " km/s"
             << "\n    |  R_res = " << mu_R_res << endl;
    }
  }

  // Core-skipping parameters
  if constexpr (dynamical_core_skipping) {
    cout << "\nUsing a dynamical core-skipping acceleration scheme:"
         << "\n  [ atau0 (ratio) < " << atau0_tolerance
         << " ]  [ Delta_v < " << dv_tolerance << " vth ]";
    // Calculate how many cells have atau > 0 to estimate the efficiency
    int count = 0, n_avoid = 0;
    #pragma omp parallel for reduction(+:count, n_avoid)
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell(i))
        n_avoid++;
      else if (atau[i] > 1.)
        count++;
    }
    cout << "\nEfficiency of non-local pre-calculation: (atau > 1) = "
         << 100. * double(count) / double(n_cells - n_avoid) << "%" << endl;
  } else if constexpr (x_crit > 0. && deuterium_fraction == 0)
    cout << "\nUsing constant core-skipping with x_crit = " << x_crit << endl;

  // Line data
  if (verbose) {
    cout << "\nLine data:" << endl;
    print("k_0", k_0, "1/cm");
    if (E0p > 0.)
      print("kp_0", kp_0, "1/cm");
    print("k_dust", k_dust, "1/cm");
    print("a", a);
    if constexpr (dynamical_core_skipping)
      print("atau", atau);

    double T_min, T_max;
    tie(T_min, T_max) = min_max(T); // Find the min/max values
    const double sqrtT_min = sqrt(T_min + T_turb);
    const double sqrtT_max = sqrt(T_max + T_turb);

    cout << "\nmin/max line data:" << endl;
    print_min_max("(vx,vy,xz)", v, "vth");
    if constexpr (cell_velocity_gradients) {
      print_min_max("dv/dr", dvdr, "vth/cm");
      print_min_max("v0", v0, "vth");
    }
    print_min_max("k_0", k_0, "1/cm");
    if (E0p > 0.)
      print_min_max("kp_0", kp_0, "1/cm");
    print_min_max("k_dust", k_dust, "1/cm");
    cout << "  T          = [" << T_min << ", " << T_max << "] K"
         << "\n  vth        = sqrt(2*kB*T/m_carrier + v_turb^2)"
         << "\n             = [" << vth_div_sqrtT * sqrtT_min / km << ", "
                                 << vth_div_sqrtT * sqrtT_max / km << "] km/s"
         << "\n  DnuD       = vth*nu0/c"
         << "\n             = [" << DnuD_div_sqrtT * sqrtT_min << ", "
                                 << DnuD_div_sqrtT * sqrtT_max << "] Hz"
         << "\n  a          = 0.5*DnuL/DnuD"
         << "\n             = [" << a_sqrtT / sqrtT_max << ", "
                                 << a_sqrtT / sqrtT_min << "]"
         << "\n  sigma0     = f12*sqrt(pi)*ee^2/(me*c*DnuD)"
         << "\n             = [" << sigma0_sqrtT / sqrtT_max << ", "
                                 << sigma0_sqrtT / sqrtT_min << "] cm^2";
    if (E0p > 0.)
      cout << "\n  sigma0p    = f12*sqrt(pi)*ee^2/(me*c*DnuD)"
         << "\n             = [" << sigma0p_sqrtT / sqrtT_max << ", "
                                 << sigma0p_sqrtT / sqrtT_min << "] cm^2";
    cout << endl;
    if constexpr (dynamical_core_skipping)
      print_min_max("atau_NL", atau);
    if (point_source && have_cameras) { // Check ray_integral behavior
      double tau0_min = positive_infinity, tau0_max = negative_infinity;
      double tau_dust_min = positive_infinity, tau_dust_max = negative_infinity;
      double comp;  // Temporary comparison
      for (const auto& direction : camera_directions) {
        comp = ray_integral(k_0, r_point, direction, i_point); // τ_0 = Σ kH_0 dl
        if (comp < tau0_min)
          tau0_min = comp;
        if (comp > tau0_max)
          tau0_max = comp;
        comp = ray_integral(k_dust, r_point, direction, i_point); // τ_dust = Σ k_dust dl
        if (comp < tau_dust_min)
          tau_dust_min = comp;
        if (comp > tau_dust_max)
          tau_dust_max = comp;
      }
      cout <<   "  tau0 (LOS) = [" << tau0_min << ", " << tau0_max << "]"
           << "\n  tau_dust   = [" << tau_dust_min << ", " << tau_dust_max << "]" << endl;
    }
    if constexpr (electron_scattering && SPHERICAL) {
      double tau_e = 0.;                     // Electron scattering optical depth
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i)
        tau_e += k_e[i] * w[i];              // Add cell optical depth
      cout << "\n  sum(tau_e) = " << tau_e << endl;
    }
  }
}

/* Function to print additional data. */
void print_observables() {
  const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
  cout << "\nGlobal statistics:"
       << "\n   Escape fraction = " << f_esc
       << "\n   Eff. n_phot_src = " << n_photons_src
       << "\n   Eff. n_phot_esc = " << n_photons_src
       << "\n Average frequency = " << freq_avg << " " << freq_units
       << "\nStandard deviation = " << freq_std << " " << freq_units
       << "\n Spectral skewness = " << freq_skew
       << "\n Spectral kurtosis = " << freq_kurt << endl;

  if (output_escape_fractions || output_proj_attenuation || output_proj_cube_attenuation)
    cout << "\nCamera escape fractions:" << endl;
  if (output_escape_fractions) {
    print("f_esc (LOS)", f_escs);
    if (output_mcrt_attenuation)
      print("f_esc (ext)", f_escs_ext);
  }
  if (output_proj_attenuation)
    print("f_esc (ray)", proj_f_escs_ext);

  if (output_proj_cube_attenuation)
    print("f_esc (cube)", proj_cube_f_escs_ext);

  if (output_freq_avgs) {
    cout << "\nCamera frequency statistics:" << endl;
    if (output_mcrt_emission)
      print("avgs (int)", freq_avgs_int, freq_units);
    if (output_mcrt_attenuation)
      print("avgs (ext)", freq_avgs_ext, freq_units);
    print("avgs (LOS)", freq_avgs, freq_units);
    if (output_freq_stds) {
      print("stds (LOS)", freq_stds, freq_units);
      if (output_freq_skews) {
        print("skews (LOS)", freq_skews);
        if (output_freq_kurts)
          print("kurts (LOS)", freq_kurts);
      }
    }
  }

  // Print additional collisional excitation data
  if (output_collisions) {
    cout << "\nCollisional excitation global statistics:"
         << "\n   Escape fraction = " << f_esc_col
         << "\n   Eff. n_phot_src = " << n_photons_src_col
         << "\n   Eff. n_phot_esc = " << n_photons_src_col
         << "\n Average frequency = " << freq_avg_col << " " << freq_units
         << "\nStandard deviation = " << freq_std_col << " " << freq_units
         << "\n Spectral skewness = " << freq_skew_col
         << "\n Spectral kurtosis = " << freq_kurt_col << endl;

    if (output_escape_fractions || output_proj_attenuation || output_proj_cube_attenuation)
      cout << "\nCollisional excitation camera escape fractions:" << endl;
    if (output_escape_fractions) {
      print("f_esc (LOS)", f_escs_col);
      if (output_mcrt_attenuation)
        print("f_esc (ext)", f_escs_ext_col);
    }
    if (output_proj_attenuation)
      print("f_esc (ray)", proj_f_escs_ext_col);

    if (output_proj_cube_attenuation)
      print("f_esc (cube)", proj_cube_f_escs_ext_col);

    if (output_freq_avgs) {
      cout << "\nCollisional excitation camera frequency statistics:" << endl;
      if (output_mcrt_emission)
        print("avgs (int)", freq_avgs_int_col, freq_units);
      if (output_mcrt_attenuation)
        print("avgs (ext)", freq_avgs_ext_col, freq_units);
      print("avgs (LOS)", freq_avgs_col, freq_units);
      if (output_freq_stds) {
        print("stds (LOS)", freq_stds_col, freq_units);
        if (output_freq_skews) {
          print("skews (LOS)", freq_skews_col);
          if (output_freq_kurts)
            print("kurts (LOS)", freq_kurts_col);
        }
      }
    }
  }
}

/* Write general camera parameters for a specified HDF5 File or Group. */
void Simulation::write_camera_info(const H5Object& f, const bool globals, const bool locals) {
  if (globals) {
    write(f, "n_cameras", n_cameras);        // Number of cameras
    if (n_side > 0) {
      write(f, "n_side", n_side);            // Healpix parameter for cameras
    } else if (n_rot > 0) {
      write(f, "n_rot", n_rot);              // Number of rotation directions
      write(f, "rotation_axis", rotation_axis); // Rotation axis, e.g. (0,0,1)
    } else {
      write(f, "camera_directions", camera_directions); // Normalized camera directions
      write(f, "camera_xaxes", camera_xaxes); // Camera "x-axis" direction vectors (x,y,z)
      write(f, "camera_yaxes", camera_yaxes); // Camera "y-axis" direction vectors (x,y,z)
    }
    if (camera_motion != 0)
      write(f, "camera_motion", camera_motion, "cm/s"); // Camera target velocity [cm/s]
    write(f, "camera_north", camera_north);  // Camera north orientation
    if (aperture_radius > 0.)
      write(f, "aperture_radius", aperture_radius); // Aperture radius [cm]
  }

  if (locals) {
    write(f, "camera_center", camera_center, "cm"); // Camera target position [cm]

    // Write image parameters
    if (image_radii.x == image_radii.y) {
      write(f, "image_radius", image_radii.x); // Image radius [cm] (image_width / 2)
    } else {
      write(f, "image_radius_x", image_radii.x); // Image x radius [cm]
      write(f, "image_radius_y", image_radii.y); // Image y radius [cm]
    }
  if (globals) {
    if (nx_pixels == ny_pixels) {
      write(f, "n_pixels", nx_pixels);       // Number of x,y image pixels
    } else {
      write(f, "nx_pixels", nx_pixels);      // Number of x image pixels
      write(f, "ny_pixels", ny_pixels);      // Number of y image pixels
    }
    if (image_widths.x == image_widths.y) {
      write(f, "image_width", image_widths.x); // Image width [cm] (defaults to entire domain)
    } else {
      write(f, "image_width_x", image_widths.x); // Image x width [cm]
      write(f, "image_width_y", image_widths.y); // Image y width [cm]
    }
    if (pixel_widths.x == pixel_widths.y) {
      write(f, "pixel_width", pixel_widths.x); // Pixel width [cm]
    } else {
      write(f, "pixel_width_x", pixel_widths.x); // Pixel x width [cm]
      write(f, "pixel_width_y", pixel_widths.y); // Pixel y width [cm]
    }
    write(f, "pixel_area", pixel_area);      // Pixel area [cm^2]
    if (cosmological) {
      if (pixel_arcsecs.x == pixel_arcsecs.y)
        write(f, "pixel_arcsec", pixel_arcsecs.x); // Pixel width [arcsec] (optional)
      else {
        write(f, "pixel_arcsec_x", pixel_arcsecs.x); // Pixel x width [arcsec]
        write(f, "pixel_arcsec_y", pixel_arcsecs.y); // Pixel y width [arcsec]
      }
      write(f, "pixel_arcsec2", pixel_arcsec2); // Pixel area [arcsec^2] (optional)
    }
  }

    // Write slit parameters
    if (output_slits) {
      write(f, "n_slit_pixels", n_slit_pixels); // Number of slit pixels
      write(f, "slit_aperture", slit_aperture); // Slit aperture [cm]
      write(f, "slit_width", slit_width);    // Slit width [cm] (defaults to entire domain)
      write(f, "slit_radius", slit_radius);  // Slit radius [cm] (slit_width / 2)
      write(f, "slit_pixel_width", slit_pixel_width); // Pixel width [cm]
      write(f, "slit_pixel_area", slit_pixel_area); // Pixel area [cm^2]
      if (cosmological) {
        write(f, "slit_aperture_arcsec", slit_aperture_arcsec); // Slit aperture [arcsec]
        write(f, "slit_pixel_arcsec", slit_pixel_arcsec); // Pixel width [arcsec] (optional)
        write(f, "slit_pixel_arcsec2", slit_pixel_arcsec2); // Pixel area [arcsec^2] (optional)
      }
    }

    // Write cube parameters
    if (output_cubes) {
      if (nx_cube_pixels == ny_cube_pixels) {
        write(f, "n_cube_pixels", nx_cube_pixels); // Number of x,y cube pixels
      } else {
        write(f, "nx_cube_pixels", nx_cube_pixels); // Number of x cube pixels
        write(f, "ny_cube_pixels", ny_cube_pixels); // Number of y cube pixels
      }
      if (cube_widths.x == cube_widths.y) {
        write(f, "cube_width", cube_widths.x); // Cube width [cm] (defaults to entire domain)
      } else {
        write(f, "cube_width_x", cube_widths.x); // Cube x width [cm]
        write(f, "cube_width_y", cube_widths.y); // Cube y width [cm]
      }
      if (cube_radii.x == cube_radii.y) {
        write(f, "cube_radius", cube_radii.x); // Cube radius [cm] (cube_width / 2)
      } else {
        write(f, "cube_radius_x", cube_radii.x); // Cube x radius [cm]
        write(f, "cube_radius_y", cube_radii.y); // Cube y radius [cm]
      }
      if (cube_pixel_widths.x == cube_pixel_widths.y) {
        write(f, "cube_pixel_width", cube_pixel_widths.x); // Pixel width [cm]
      } else {
        write(f, "cube_pixel_width_x", cube_pixel_widths.x); // Pixel x width [cm]
        write(f, "cube_pixel_width_y", cube_pixel_widths.y); // Pixel y width [cm]
      }
      write(f, "cube_pixel_area", cube_pixel_area); // Pixel area [cm^2]
      if (cosmological) {
        if (cube_pixel_arcsecs.x == cube_pixel_arcsecs.y)
          write(f, "cube_pixel_arcsec", cube_pixel_arcsecs.x); // Pixel width [arcsec] (optional)
        else {
          write(f, "cube_pixel_arcsec_x", cube_pixel_arcsecs.x); // Pixel x width [arcsec]
          write(f, "cube_pixel_arcsec_y", cube_pixel_arcsecs.y); // Pixel y width [arcsec]
        }
        write(f, "cube_pixel_arcsec2", cube_pixel_arcsec2); // Pixel area [arcsec^2] (optional)
      }
    }

    // Write radial image parameters
    if (output_radial_images) {
      write(f, "n_radial_pixels", n_radial_pixels); // Number of radial image pixels
      write(f, "radial_image_radius", radial_image_radius); // Radial image radius [cm] (defaults to half domain)
      write(f, "radial_pixel_width", radial_pixel_width); // Radial pixel width [cm]
      write(f, "radial_pixel_areas", radial_pixel_areas, "cm^2"); // Radial pixel areas [cm^2]
      if (cosmological) {
        write(f, "radial_pixel_arcsec", radial_pixel_arcsec); // Radial pixel width [arcsec] (optional)
        write(f, "radial_pixel_arcsec2", radial_pixel_arcsec2, "arcsec^2"); // Radial pixel areas [arcsec^2] (optional)
      }
    }

    // Write radial cube parameters
    if (output_radial_cubes) {
      write(f, "n_radial_cube_pixels", n_radial_cube_pixels); // Number of radial cube pixels
      write(f, "radial_cube_radius", radial_cube_radius); // Radial cube radius [cm] (defaults to half domain)
      write(f, "radial_cube_pixel_width", radial_cube_pixel_width); // Radial cube pixel width [cm]
      write(f, "radial_cube_pixel_areas", radial_cube_pixel_areas, "cm^2"); // Radial cube pixel areas [cm^2]
      if (cosmological) {
        write(f, "radial_cube_pixel_arcsec", radial_cube_pixel_arcsec); // Radial cube pixel width [arcsec] (optional)
        write(f, "radial_cube_pixel_arcsec2", radial_cube_pixel_arcsec2, "arcsec^2"); // Radial cube pixel areas [arcsec^2] (optional)
      }
    }
  }
}

/* Write general dust parameters. */
void Simulation::write_dust_info(const H5File& f) {
  Group g = f.createGroup("/dust");
  dust_strings tags = get_dust_tags();       // Get dust tags
  for (int i = 0; i < n_dust_species; ++i)
    write(g, "dust_model"+tags[i], dust_models[i]); // Dust model: SMC, MW, etc.
  if (hydrogen_fraction >= 0.)
    write(g, "hydrogen_fraction", hydrogen_fraction); // Constant hydrogen mass fraction
  if (helium_fraction >= 0.)
    write(g, "helium_fraction", helium_fraction); // Constant helium mass fraction
  if (metallicity >= 0.)
    write(g, "metallicity", metallicity);    // Constant metallicity
  const int Z_offset = CarbonMetallicity - C_ATOM; // Offset for metallicity fields
  for (int atom = 2; atom < n_atoms; ++atom) {
    double& val = fields[atom + Z_offset].constant; // Reference to metallicity value
    if (val >= 0.)
      write(g, atom_names[atom] + "_metallicity", val); // Constant atom metallicity
  }
  if (mass_weighted_metallicities) {         // Adopt mass-weighted metallicities
    write(g, "mass_weighted_metallicities", mass_weighted_metallicities);
    write(g, "Z", Z[0]);
    for (int atom = 0; atom < n_atoms; ++atom) {
      auto& field = fields[atom + Z_offset]; // Reference to metallicity field
      if (!field.data.empty())
        write(g, field.name, field.data[0]); // Constant atom metallicity
    }
  }
  if (dust_to_metal >= 0.)
    write(g, "dust_to_metal", dust_to_metal); // Constant dust-to-metal ratio
  if (dust_to_gas >= 0.)
    write(g, "dust_to_gas", dust_to_gas);    // Constant dust-to-gas ratio
  if (dust_boost > 0.)
    write(g, "dust_boost", dust_boost);      // Optional dust boost factor
  write(g, "f_ion", f_ion);                  // HII region survival fraction
  write(g, "f_dust", f_dust);                // Fraction of metals locked in dust
  #if graphite_scaled_PAH
    write(g, "f_PAH", f_PAH);                // Fraction of carbon in PAHs
  #endif
  write(g, "T_sputter", T_sputter);          // Thermal sputtering cutoff [K]
}

/* Write general simulation information. */
static void write_mcrt_info(const H5File& f) {
  // Line data
  {
    Group g = f.createGroup("/line");
    write(g, "name", line);                  // Line name, e.g. Lyman-alpha
    write(g, "E0", E0);                      // Line energy [erg]
    write(g, "A21", A21);                    // Einstein A coefficient [Hz]
    write(g, "f12", f12);                    // Oscillator strength
    write(g, "g12", g12);                    // Degeneracy ratio: g_lower / g_upper
    write(g, "T0", T0);                      // Line temperature [K]
    write(g, "nu0", nu0);                    // Line frequency [Hz]
    write(g, "lambda0", lambda0);            // Line wavelength [angstrom]
    write(g, "B21", B21);                    // Einstein B21 = A21 c^2 / (2 h ν0^3)
    write(g, "DnuL", DnuL);                  // Natural frequency broadening [Hz]
    write(g, "p_dest", p_dest);              // Line destruction probability
    write(g, "m_carrier", m_carrier);        // Mass of carrier [g]
    if constexpr (deuterium_fraction > 0.) {
      write(g, "E0D", E0D);                  // Line energy for deuterium [erg]
      write(g, "nu0D", nu0D);                // Line frequency for deuterium [Hz]
      write(g, "lambda0D", lambda0D);        // Line wavelength for deuterium [angstrom]
    }
    if (v_turb > 0.) {
      write(g, "v_turb", v_turb);            // Microturbulent velocity [cm/s]
      write(g, "T_turb", T_turb);            // Microturbulent temperature [K]
      if (scaled_microturb)
        write(g, "scaled_microturb", scaled_microturb); // Density scaling flag
    }
    write(g, "vth_div_sqrtT", vth_div_sqrtT); // Thermal velocity: vth = sqrt(2 kB T / m_carrier)
    write(g, "DnuD_div_sqrtT", DnuD_div_sqrtT); // Doppler width: ΔνD = ν0 vth / c
    write(g, "a_sqrtT", a_sqrtT);            // "damping parameter": a = ΔνL / 2 ΔνD
    write(g, "sigma0_sqrtT", sigma0_sqrtT);  // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
    write(g, "gDa", gDa);                    // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)
    // Doublet data
    if (E0p > 0.) {
      write(g, "E0p", E0p);                  // Line energy [erg]
      write(g, "A21p", A21p);                // Einstein A coefficient [Hz]
      write(g, "f12p", f12p);                // Oscillator strength
      write(g, "nu0p", nu0p);                // Line frequency [Hz]
      write(g, "lambda0p", lambda0p);        // Line wavelength [angstrom]
      write(g, "DnuLp", DnuLp);              // Natural frequency broadening [Hz]
      write(g, "gDap", gDap);                // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)
    }
  }

  // Exit variables
  {
    Group g = f.createGroup("/exit");
    write(g, "v_exit", camera_motion * vth_div_sqrtT * sqrt(T_exit) / km, "km/s"); // Exit velocity [km/s]
    write(g, "T_exit", T_exit);              // Exit temperature [K]
    write(g, "a_exit", a_exit);              // "Damping parameter" [T_exit]
    write(g, "DnuD_exit", DnuD_exit);        // Doppler width at T_exit
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    if (cell_based_emission) {
      write(g, "n_cells", n_cells);          // Number of cells
      write(g, "n_cells_eff", n_cells_eff);  // Effective number of cells: 1/<w>
    }
    if (star_based_emission) {
      write(g, "n_stars", n_stars);          // Number of stars
      write(g, "n_stars_eff", n_stars_eff);  // Effective number of stars: 1/<w>
    }
    if (point_source) {
      write(g, "L_point", L_point);          // Point source luminosity [erg/s]
      write(g, "r_point", r_point, "cm");    // Point source position [cm]
    }
    if (plane_cont_source) {
      write(g, "plane_direction", plane_direction); // Plane direction name
      write(g, "plane_position", plane_position); // Plane position [cm]
      write(g, "plane_center_1", plane_center_1); // Plane center positions
      write(g, "plane_center_2", plane_center_2);
      write(g, "plane_radius_1", plane_radius_1); // Plane box/disk radii
      write(g, "plane_radius_2", plane_radius_2);
      write(g, "plane_area", plane_area);    // Plane area [cm^2]
      write(g, "plane_beam", plane_beam);    // Use an ellipsoidal beam instead of a rectangle
      write(g, "S_plane_cont", S_plane_cont); // Plane continuum surface flux [erg/s/cm^2/angstrom]
      write(g, "F_plane_cont", F_plane_cont); // Plane continuum source flux [erg/s/angstrom]
      write(g, "L_plane_cont", L_plane_cont); // Plane continuum source luminosity [erg/s]
    }
    if (sphere_cont_source) {
      write(g, "L_sphere_cont", L_sphere_cont); // Sphere continuum source luminosity [erg/s]
      write(g, "sphere_radius_cont", sphere_radius_cont); // Radius for spherical continuum source [cm]
      write(g, "sphere_center_cont", sphere_center_cont, "cm"); // Center of the sphere region [cm]
    }
    if (shell_cont_source) {
      write(g, "L_shell_cont", L_shell_cont); // Shell continuum source luminosity [erg/s]
      write(g, "r_shell_cont", r_shell_cont); // Shell continuum source radius [cm]
      if (shell_blackbody)
        write(g, "T_shell", T_shell);        // Shell source temperature [K]
    }
    if (shell_line_source) {
      write(g, "L_shell_line", L_shell_line); // Shell line source luminosity [erg/s]
      write(g, "r_shell_line", r_shell_line); // Shell line source radius [cm]
    }
    if (shock_source) {
      write(g, "L_shock", L_shock);          // Shock source luminosity [erg/s]
      write(g, "r_shock", r_shock);          // Shock source radius [cm]
      if (shock_blackbody)
        write(g, "T_shock", T_shock);        // Shock source temperature [K]
    }
    if (recombinations)
      write(g, "L_rec", L_rec);              // Recombination luminosity [erg/s]
    if (collisions)
      write(g, "L_col", L_col);              // Collisional excitation luminosity [erg/s]
    if (collisions_limiter > 0.)
      write(g, "collisions_limiter", collisions_limiter); // Limited by photoheating rate
    if (spontaneous)
      write(g, "L_sp", L_sp);                // Spontaneous emission luminosity [erg/s]
    if (doublet_cell_based_emission && collisions) {
      write(g, "n_cells_effp", n_cells_effp); // Effective number of doublet cells: 1/<w>
      write(g, "Lp_col", Lp_col);            // Doublet collisional excitation luminosity [erg/s]
    }
    if (unresolved_HII)
      write(g, "L_HII", L_HII);              // Unresolved HII regions luminosity [erg/s]
    if (recombinations || collisions || spontaneous) {
      write(g, "r_light", r_light, "cm");    // Center of luminosity position [cm]
      write(g, "r2_light", r2_light, "cm^2"); // Center of luminosity position^2 [cm^2]
      write(g, "v_light", v_light, "cm/s");  // Center of luminosity velocity [cm/s]
      write(g, "v2_light", v2_light, "cm^2/s^2"); // Center of luminosity velocity^2 [cm^2/s^2]
      write(g, "j_light", j_light, "cm^2/s"); // Center of luminosity specific angular momentum [cm^2/s]
    }

    if (continuum) {
      const double L_tot_cont = L_cont + L_plane_cont + L_sphere_cont + L_shell_cont + L_shock;
      write(g, "L_cont", L_tot_cont);        // Total L_cont [erg/s]
      write(g, "M_cont", M_cont);            // Total M_cont [absolute magnitude]
      write(g, "Dv_cont_min", Dv_cont_min);  // Continuum velocity offset lower limit [km/s]
      write(g, "Dv_cont_max", Dv_cont_max);  // Continuum velocity offset upper limit [km/s]
      write(g, "Dv_cont_res", Dv_cont_res);  // Continuum velocity offset resolution [km/s]
      write(g, "l_cont_res", l_cont_res);    // Continuum wavelength resolution [angstrom]
      write(g, "R_cont_res", R_cont_res);    // Spectral resolution: R = lambda / Delta_lambda
    }

    write(g, "n_source_types", n_source_types); // Number of source types used
    write_attr(g, "source_types", source_types_mask); // Source type mask indices
    write_attr(g, "source_names", source_names_mask); // Source type names (masked)
    write_attr(g, "mixed_pdf", mixed_pdf_mask); // Mixed source pdf (masked)
    write_attr(g, "mixed_cdf", mixed_cdf_mask); // Mixed source cdf (masked)
    write_attr(g, "mixed_weights", mixed_weights_mask); // Mixed source weights (masked)

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    #if spherical_escape
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    #endif
    #if box_escape
      write(g, "escape_box_min", escape_box[0], "cm"); // Escape bounding box [cm]
      write(g, "escape_box_max", escape_box[1], "cm");
      write(g, "emission_box_min", emission_box[0], "cm"); // Emission bounding box [cm]
      write(g, "emission_box_max", emission_box[1], "cm");
    #endif
    #if streaming_escape
      write(g, "max_streaming", max_streaming); // Maximum streaming distance [cm]
    #endif
  }

  // Simulation information
  write(f, "n_photons", n_photons);          // Actual number of photon packets used
  write(f, "Ndot_tot", Ndot_tot);            // Total emission rate [photons/s]
  write(f, "L_tot", L_tot);                  // Total luminosity [erg/s]
  write(f, "f_src", f_src);                  // Global source fraction: sum(w0)
  write(f, "n_photons_src", n_photons_src);  // Effective number of emitted photons: 1/<w0>
  write(f, "f_esc", f_esc);                  // Global escape fraction: sum(w)
  write(f, "n_photons_esc", n_photons_esc);  // Effective number of escaped photons: 1/<w>
  write(f, "freq_avg", freq_avg);            // Global average frequency [freq units]
  write(f, "freq_std", freq_std);            // Global standard deviation [freq units]
  write(f, "freq_skew", freq_skew);          // Global frequency skewness [normalized]
  write(f, "freq_kurt", freq_kurt);          // Global frequency kurtosis [normalized]
  if (output_collisions) {
    write(f, "f_src_col", f_src_col);        // Global source fraction: sum(w0)
    write(f, "n_photons_src_col", n_photons_src_col); // Effective number of emitted photons: 1/<w0>
    write(f, "f_esc_col", f_esc_col);        // Global escape fraction: sum(w)
    write(f, "n_photons_esc_col", n_photons_esc_col); // Effective number of escaped photons: 1/<w>
    write(f, "freq_avg_col", freq_avg_col);  // Global average frequency [freq units]
    write(f, "freq_std_col", freq_std_col);  // Global standard deviation [freq units]
    write(f, "freq_skew_col", freq_skew_col); // Global frequency skewness [normalized]
    write(f, "freq_kurt_col", freq_kurt_col); // Global frequency kurtosis [normalized]
  }
  write(f, "doppler_frequency", doppler_frequency); // Output frequency in Doppler widths
  write(f, "recoil", recoil);                // Include recoil with scattering
  if constexpr (dynamical_core_skipping)
    write(f, "n_side_atau", n_side_atau);    // Healpix parameter for a*tau0

  // Information about dust
  {
    Group g = exists(f, "/dust") ? f.openGroup("/dust") : f.createGroup("/dust");
    write(g, "kappa_dust", kappa_dust);      // Dust opacity [cm^2/g dust]
    write(g, "sigma_dust", sigma_dust);      // Dust cross section [cm^2/Z/hydrogen atom]
    write(g, "albedo", albedo);              // Dust albedo for scattering vs. absorption
    write(g, "g_dust", g_dust);              // Anisotropy parameter: <μ> for dust scattering
  }
}

/* Write photon data to a specified group or file. */
template <typename File_or_Group>
static void write_photons(const File_or_Group& f) {
  const string freq_units = (doppler_frequency) ? "Doppler" : "km/s"; // Code frequency units
  // const string freq_units2 = "(" + freq_units + ")^2"; // Code frequency units^2
  write(f, "n_escaped", n_escaped);          // Number of escaped photons
  write(f, "n_absorbed", n_absorbed);        // Number of photons absorbed
  write(f, "source_id", source_ids);         // Emission cell or star index
  if (n_source_types > 1)
    write(f, "source_type", source_types);   // Emission type specifier
  write(f, "source_weight", source_weights); // Photon weight at emission
  if (output_n_scat)
    write(f, "n_scat", n_scats);             // Number of scattering events
  if (output_collisions)
    write(f, "f_col", f_cols);               // Collisional excitation fraction
  write(f, "frequency", freqs, freq_units);  // Frequency at escape [freq_units]
  write(f, "weight", weights);               // Photon weight at escape
  if (output_source_position)
    write(f, "source_position", source_positions, "cm"); // Position at emission [cm]
  write(f, "position", positions, "cm");     // Position at escape [cm]
  write(f, "direction", directions);         // Direction at escape
  if (output_path_length)
    write(f, "path_length", path_lengths, "cm"); // Path length [cm]
}

/* Write radial flow statistics to a specified group or file. */
template <typename File_or_Group>
static void write_radial_flow(const File_or_Group& f) {
  // Histogram data
  if (exists(f, "radial_flow")) {
    H5Ldelete(f.getId(), "radial_flow", H5P_DEFAULT);
    cout << "\nWarning: Replacing radial_flow group." << endl;
  }
  Group g = f.createGroup("/radial_flow");   // Write to a separate group
  write(g, "n_radial_bins", radial_line_flow.n_radial_bins);
  write(g, "radial_edges", radial_line_flow.radial_edges, "cm");
  write(g, "L_src", radial_line_flow.src, "erg/s");
  write(g, "L_esc", radial_line_flow.esc, "erg/s");
  write(g, "L_abs", radial_line_flow.abs, "erg/s");
  write(g, "L_pass", radial_line_flow.pass, "erg/s");
  write(g, "L_flow", radial_line_flow.flow, "erg/s");
}

/* Write group and subhalo flows statistics to a specified group or file. */
template <typename File_or_Group>
static void write_halo_flows(const File_or_Group& f, const bool is_group) {
  // Histogram data
  string name = is_group ? "group_flows" : "subhalo_flows";
  vector<int>& halo_id = is_group ? group_id : subhalo_id;
  vector<RadialLineFlow>& halo_flows = is_group ? group_line_flows : subhalo_line_flows;
  const int n_data = halo_flows.size();
  if (n_data == 0) return;                   // No data to write
  if (exists(f, name)) {
    H5Ldelete(f.getId(), name.c_str(), H5P_DEFAULT);
    cout << "\nWarning: Replacing " << name << " group." << endl;
  }
  Group g = f.createGroup(name);             // Write to a separate group
  write(g, "n_radial_bins", halo_flows[0].n_radial_bins);
  if (!output_groups )
    write(g, "id", halo_id);                 // Halo IDs
  for (int i = 0; i < n_data; ++i) {
    Group g_i = g.createGroup(to_string(halo_id[i]));
    write(g_i, "radial_edges", halo_flows[i].radial_edges, "cm");
    write(g_i, "L_src", halo_flows[i].src, "erg/s");
    write(g_i, "L_esc", halo_flows[i].esc, "erg/s");
    write(g_i, "L_abs", halo_flows[i].abs, "erg/s");
    write(g_i, "L_pass", halo_flows[i].pass, "erg/s");
    write(g_i, "L_flow", halo_flows[i].flow, "erg/s");
  }
}

/* Writes MCRT data and info to the specified hdf5 filename */
void MCRT::write_module(const H5File& f) {
  // Write general MCRT info
  write_mcrt_info(f);

  // Additional camera info
  if (have_cameras) {
    write(f, "n_bins", n_bins);              // Number of frequency bins
    write(f, "freq_min", freq_min);          // Generic frequency extrema [freq units]
    write(f, "freq_max", freq_max);          // Calculated when freq is initialized
    write(f, "freq_bin_width", freq_bin_width); // Frequency bin width [freq units]
    if (cosmological)
      write(f, "observed_bin_width", observed_bin_width); // Observed wavelength resolution [angstrom]
    if (output_slits && (n_slit_bins != n_bins || slit_freq_min != freq_min || slit_freq_max != freq_max)) {
      write(f, "n_slit_bins", n_slit_bins);  // Number of slit frequency bins
      write(f, "slit_freq_min", slit_freq_min); // Slit frequency extrema [freq units]
      write(f, "slit_freq_max", slit_freq_max); // Calculated when freq is initialized
      write(f, "slit_freq_bin_width", slit_freq_bin_width); // Slit frequency bin width [freq units]
      if (cosmological)
        write(f, "observed_slit_bin_width", observed_slit_bin_width); // Observed wavelength resolution [angstrom]
    }
    if (output_cubes && (n_cube_bins != n_bins || cube_freq_min != freq_min || cube_freq_max != freq_max)) {
      write(f, "n_cube_bins", n_cube_bins);  // Number of cube frequency bins
      write(f, "cube_freq_min", cube_freq_min); // Cube frequency extrema [freq units]
      write(f, "cube_freq_max", cube_freq_max); // Calculated when freq is initialized
      write(f, "cube_freq_bin_width", cube_freq_bin_width); // Cube frequency bin width [freq units]
      if (cosmological)
        write(f, "observed_cube_bin_width", observed_cube_bin_width); // Observed wavelength resolution [angstrom]
    }
    if (output_radial_cubes && (n_radial_cube_bins != n_bins || radial_cube_freq_min != freq_min || radial_cube_freq_max != freq_max)) {
      write(f, "n_radial_cube_bins", n_radial_cube_bins); // Number of radial cube frequency bins
      write(f, "radial_cube_freq_min", radial_cube_freq_min); // Radial cube frequency extrema [freq units]
      write(f, "radial_cube_freq_max", radial_cube_freq_max); // Calculated when freq is initialized
      write(f, "radial_cube_freq_bin_width", radial_cube_freq_bin_width); // Cube frequency bin width [freq units]
      if (cosmological)
        write(f, "observed_radial_cube_bin_width", observed_radial_cube_bin_width); // Observed wavelength resolution [angstrom]
    }
    string flux_units, image_units, cube_units;
    const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
    if (cosmological) {
      flux_units = "erg/s/cm^2/angstrom";
      image_units = "erg/s/cm^2/arcsec^2";
      cube_units = "erg/s/cm^2/arcsec^2/angstrom";
    } else { // Intrinsic units (independent of distance)
      const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
      flux_units = "erg/s" + per_freq_units;
      image_units = "erg/s/cm^2";
      cube_units = "erg/s/cm^2" + per_freq_units;
    }
    if (adjust_camera_frequency) {
      write(f, "adjust_camera_frequency", adjust_camera_frequency);
      write(f, "freq_offsets", freq_offsets, freq_units); // Frequency offsets [km/s]
    }
    if (output_flux_avg)
      write(f, "flux_avg", flux_avg, flux_units); // Angle-averaged flux [erg/s/cm^2/angstrom]
    if (output_radial_avg)
      write(f, "radial_avg", radial_avg, image_units); // Angle-averaged radial surface brightness [erg/s/cm^2/arcsec^2]
    if (output_radial_cube_avg)
      write(f, "radial_cube_avg", radial_cube_avg, image_units); // Angle-averaged radial spectral data cube [erg/s/cm^2/arcsec^2/angstrom]
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_freq_avgs)
      write(f, "freq_avgs", freq_avgs, freq_units); // Frequency averages [freq units]
    if (output_freq_stds)
      write(f, "freq_stds", freq_stds, freq_units); // Standard deviations [freq units]
    if (output_freq_skews)
      write(f, "freq_skews", freq_skews);    // Frequency skewnesses [normalized]
    if (output_freq_kurts)
      write(f, "freq_kurts", freq_kurts);    // Frequency kurtoses [normalized]
    if (output_fluxes)
      write(f, "fluxes", fluxes, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_images)
      write(f, "images", images, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images2)
      write(f, "images2", images2, "("+image_units+")^2"); // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
    if (output_freq_images)
      write(f, "freq_images", freq_images, freq_units); // Average frequency images [freq units]
    if (output_freq2_images)
      write(f, "freq2_images", freq2_images, "("+freq_units+")^2"); // Frequency^2 images [freq^2 units]
    if (output_freq3_images)
      write(f, "freq3_images", freq3_images, "("+freq_units+")^3"); // Frequency^3 images [freq^3 units]
    if (output_freq4_images)
      write(f, "freq4_images", freq4_images, "("+freq_units+")^4"); // Frequency^4 images [freq^4 units]
    if (output_rgb_images)
      write(f, "rgb_images", rgb_images);    // Flux-weighted frequency RGB images
    if (output_slits) {
      write(f, "slits_x", slits_x, cube_units); // Spectral slits (x) [erg/s/cm^2/arcsec^2/angstrom]
      write(f, "slits_y", slits_y, cube_units); // Spectral slits (y) [erg/s/cm^2/arcsec^2/angstrom]
    }
    if (output_cubes)
      write(f, "cubes", cubes, cube_units);  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

    // Radial camera outputs
    if (output_radial_images)
      write(f, "radial_images", radial_images, image_units); // Radial surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_radial_images2)
      write(f, "radial_images2", radial_images2, "("+image_units+")^2"); // Statistical moment radial images [erg^2/s^2/cm^4/arcsec^4]
    if (output_freq_radial_images)
      write(f, "freq_radial_images", freq_radial_images, freq_units); // Average frequency radial images [freq units]
    if (output_freq2_radial_images)
      write(f, "freq2_radial_images", freq2_radial_images, "("+freq_units+")^2"); // Frequency^2 radial images [freq^2 units]
    if (output_freq3_radial_images)
      write(f, "freq3_radial_images", freq3_radial_images, "("+freq_units+")^3"); // Frequency^3 radial images [freq^3 units]
    if (output_freq4_radial_images)
      write(f, "freq4_radial_images", freq4_radial_images, "("+freq_units+")^4"); // Frequency^4 radial images [freq^4 units]
    if (output_rgb_radial_images)
      write(f, "rgb_radial_images", rgb_radial_images); // Flux-weighted frequency RGB radial images
    if (output_radial_cubes)
      write(f, "radial_cubes", radial_cubes, cube_units); // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

    // Intrinsic emission (mcrt)
    if (output_mcrt_emission) {
      if (output_freq_avgs)
        write(f, "freq_avgs_int", freq_avgs_int, freq_units); // Frequency averages [freq units]
      if (output_fluxes)
        write(f, "fluxes_int", fluxes_int, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_images)
        write(f, "images_int", images_int, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_cubes)
        write(f, "cubes_int", cubes_int, cube_units);  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
      if (output_radial_images)
        write(f, "radial_images_int", radial_images_int, image_units); // Radial surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_radial_cubes)
        write(f, "radial_cubes_int", radial_cubes_int, cube_units); // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    }

    // Attenuated emission (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        write(f, "f_escs_ext", f_escs_ext);            // Escape fractions [fraction]
      if (output_freq_avgs)
        write(f, "freq_avgs_ext", freq_avgs_ext, freq_units); // Frequency averages [freq units]
      if (output_fluxes)
        write(f, "fluxes_ext", fluxes_ext, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_images)
        write(f, "images_ext", images_ext, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_cubes)
        write(f, "cubes_ext", cubes_ext, cube_units);  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
      if (output_radial_images)
        write(f, "radial_images_ext", radial_images_ext, image_units); // Radial surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_radial_cubes)
        write(f, "radial_cubes_ext", radial_cubes_ext, cube_units); // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    }

    // Projection camera data
    if (output_proj_emission || output_proj_attenuation || output_proj_cube_emission || output_proj_cube_attenuation) {
      write(f, "proj_depth", proj_depth);    // Projection depth [cm]
      write(f, "proj_radius", proj_radius);  // Projection radius [cm]
      write(f, "pixel_rtol", pixel_rtol);    // Relative tolerance per pixel
    }

    // Intrinsic emission (proj)
    if (output_proj_emission)
      write(f, "proj_images_int", proj_images_int, image_units); // Surface brightness images [erg/s/cm^2]

    // Attenuated emission (proj)
    if (output_proj_attenuation) {
      write(f, "proj_f_escs_ext", proj_f_escs_ext);    // Escape fractions [fraction]
      write(f, "proj_images_ext", proj_images_ext, image_units); // Surface brightness images [erg/s/cm^2]
    }

    // Intrinsic emission (proj cube)
    if (output_proj_cube_emission)
      write(f, "proj_cubes_int", proj_cubes_int, cube_units); // Projection spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

    // Attenuated emission (proj cube)
    if (output_proj_cube_attenuation) {
      write(f, "proj_cube_f_escs_ext", proj_cube_f_escs_ext); // Escape fractions [fraction]
      write(f, "proj_cubes_ext", proj_cubes_ext, cube_units); // Projection spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    }

    // Collisional excitation camera data
    if (output_collisions) {
      if (output_escape_fractions)
        write(f, "f_escs_col", f_escs_col);  // Escape fractions [fraction]
      if (output_freq_avgs)
        write(f, "freq_avgs_col", freq_avgs_col, freq_units); // Frequency averages [freq units]
      if (output_freq_stds)
        write(f, "freq_stds_col", freq_stds_col, freq_units); // Standard deviations [freq units]
      if (output_freq_skews)
        write(f, "freq_skews_col", freq_skews_col); // Frequency skewnesses [normalized]
      if (output_freq_kurts)
        write(f, "freq_kurts_col", freq_kurts_col); // Frequency kurtoses [normalized]
      if (output_fluxes)
        write(f, "fluxes_col", fluxes_col, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_images)
        write(f, "images_col", images_col, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images2)
        write(f, "images2_col", images2_col, "("+image_units+")^2"); // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
      if (output_freq_images)
        write(f, "freq_images_col", freq_images_col, freq_units); // Average frequency images [freq units]
      if (output_freq2_images)
        write(f, "freq2_images_col", freq2_images_col, "("+freq_units+")^2"); // Frequency^2 images [freq^2 units]
      if (output_freq3_images)
        write(f, "freq3_images_col", freq3_images_col, "("+freq_units+")^3"); // Frequency^3 images [freq^3 units]
      if (output_freq4_images)
        write(f, "freq4_images_col", freq4_images_col, "("+freq_units+")^4"); // Frequency^4 images [freq^4 units]

      // Intrinsic emission (mcrt)
      if (output_mcrt_emission) {
        if (output_freq_avgs)
          write(f, "freq_avgs_int_col", freq_avgs_int_col, freq_units); // Frequency averages [freq units]
        if (output_fluxes)
          write(f, "fluxes_int_col", fluxes_int_col, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
        if (output_images)
          write(f, "images_int_col", images_int_col, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      }

      // Attenuated emission (mcrt)
      if (output_mcrt_attenuation) {
        if (output_escape_fractions)
          write(f, "f_escs_ext_col", f_escs_ext_col); // Escape fractions [fraction]
        if (output_freq_avgs)
          write(f, "freq_avgs_ext_col", freq_avgs_ext_col, freq_units); // Frequency averages [freq units]
        if (output_fluxes)
          write(f, "fluxes_ext_col", fluxes_ext_col, flux_units); // Spectral fluxes [erg/s/cm^2/angstrom]
        if (output_images)
          write(f, "images_ext_col", images_ext_col, image_units); // Surface brightness images [erg/s/cm^2/arcsec^2]
      }

      // Intrinsic emission (proj)
      if (output_proj_emission)
        write(f, "proj_images_int_col", proj_images_int_col, image_units); // Surface brightness images [erg/s/cm^2]

      // Attenuated emission (proj)
      if (output_proj_attenuation) {
        write(f, "proj_f_escs_ext_col", proj_f_escs_ext_col); // Escape fractions [fraction]
        write(f, "proj_images_ext_col", proj_images_ext_col, image_units); // Surface brightness images [erg/s/cm^2]
      }

      // Intrinsic emission (proj cube)
      if (output_proj_cube_emission)
        write(f, "proj_cubes_int_col", proj_cubes_int_col, cube_units); // Projection spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

      // Attenuated emission (proj cube)
      if (output_proj_cube_attenuation) {
        write(f, "proj_cube_f_escs_ext_col", proj_cube_f_escs_ext_col); // Escape fractions [fraction]
        write(f, "proj_cubes_ext_col", proj_cubes_ext_col, cube_units); // Projection spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
      }
    }
  }

  // Line-of-sight healpix maps
  if (output_map) {
    const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
    const int n_pix_map = 12 * n_side_map * n_side_map; // 12 n_side^2
    write(f, "n_side_map", n_side_map);      // Healpix n_side parameter
    write(f, "n_pix_map", n_pix_map);        // Number of map pixels
    write(f, "map", map);                    // Escape fraction map [fraction]
    if (output_map2)
      write(f, "map2", map2);                // Escape fraction^2 map [statistic]
    if (output_freq_map)
      write(f, "freq_map", freq_map, freq_units); // Average frequency map [freq units]
    if (output_freq2_map)
      write(f, "freq2_map", freq2_map, "("+freq_units+")^2"); // Frequency^2 map [freq^2 units]
    if (output_freq3_map)
      write(f, "freq3_map", freq3_map, "("+freq_units+")^3"); // Frequency^3 map [freq^3 units]
    if (output_freq4_map)
      write(f, "freq4_map", freq4_map, "("+freq_units+")^4"); // Frequency^4 map [freq^4 units]
    if (output_rgb_map)
      write(f, "rgb_map", rgb_map);          // Flux-weighted frequency RGB map
  }

  // Line-of-sight healpix flux map
  if (output_flux_map) {
    const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
    const string flux_units = (cosmological) ? "erg/s/cm^2/angstrom" : "erg/s" + per_freq_units;
    const int n_pix_flux = 12 * n_side_flux * n_side_flux; // 12 n_side^2
    write(f, "n_side_flux", n_side_flux);    // Healpix n_side parameter
    write(f, "n_pix_flux", n_pix_flux);      // Number of flux map pixels
    write(f, "flux_map", flux_map, flux_units); // Spectral flux map [erg/s/cm^2/angstrom]
    if (!have_cameras || n_map_bins != n_bins || map_freq_min != freq_min || map_freq_max != freq_max) {
      write(f, "n_map_bins", n_map_bins);    // Number of flux map frequency bins
      write(f, "map_freq_min", map_freq_min); // Flux map frequency extrema [freq units]
      write(f, "map_freq_max", map_freq_max); // Calculated when freq is initialized
      write(f, "map_freq_bin_width", map_freq_bin_width); // Flux map frequency bin width [freq units]
      if (cosmological)
        write(f, "observed_map_bin_width", observed_map_bin_width); // Observed wavelength resolution [angstrom]
    }
  }

  // Line-of-sight healpix radial map
  if (output_radial_map) {
    string image_units = (cosmological) ? "erg/s/cm^2/arcsec^2" : "erg/s/cm^2";
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // 12 n_side^2
    write(f, "n_side_radial", n_side_radial); // Healpix n_side parameter
    write(f, "n_pix_radial", n_pix_radial);  // Number of radial map pixels
    write(f, "radial_map", radial_map, image_units); // Radial surface brightness map [erg/s/cm^2/arcsec^2]
    if (!have_cameras || n_map_pixels != n_radial_pixels || map_radius != radial_image_radius) {
      write(f, "n_map_pixels", n_map_pixels); // Number of radial map pixels
      write(f, "map_radius", map_radius);    // Radial map radius [cm] (defaults to half domain)
      write(f, "map_pixel_width", map_pixel_width); // Radial map pixel width [cm]
      write(f, "map_pixel_areas", map_pixel_areas, "cm^2"); // Radial map pixel areas [cm^2]
      if (cosmological) {
        write(f, "map_pixel_arcsec", map_pixel_arcsec); // Radial map pixel width [arcsec] (optional)
        write(f, "map_pixel_arcsec2", map_pixel_arcsec2, "arcsec^2"); // Radial map pixel areas [arcsec^2] (optional)
      }
    }
  }

  // Line-of-sight healpix radial spectral map
  if (output_cube_map) {
    const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
    const string cube_units = (cosmological) ? "erg/s/cm^2/arcsec^2/angstrom" : "erg/s/cm^2" + per_freq_units;
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // 12 n_side^2
    write(f, "n_side_cube", n_side_cube);    // Healpix n_side parameter
    write(f, "n_pix_cube", n_pix_cube);      // Number of radial spectral map pixels
    write(f, "cube_map", cube_map, cube_units); // Radial spectral map [erg/s/cm^2/arcsec^2/angstrom]
    if (!have_cameras || n_cube_map_pixels != n_radial_pixels || cube_map_radius != radial_image_radius
        || n_cube_map_bins != n_bins || cube_map_freq_min != freq_min || cube_map_freq_max != freq_max) {
      write(f, "n_cube_map_pixels", n_cube_map_pixels); // Number of radial pixels
      write(f, "cube_map_radius", cube_map_radius); // Radius [cm] (defaults to half domain)
      write(f, "cube_map_pixel_width", cube_map_pixel_width); // Radial pixel width [cm]
      write(f, "cube_map_pixel_areas", cube_map_pixel_areas, "cm^2"); // Radial pixel areas [cm^2]
      if (cosmological) {
        write(f, "cube_map_pixel_arcsec", cube_map_pixel_arcsec); // Radial pixel width [arcsec] (optional)
        write(f, "cube_map_pixel_arcsec2", cube_map_pixel_arcsec2, "arcsec^2"); // Radial pixel areas [arcsec^2] (optional)
      }
      write(f, "n_cube_map_bins", n_cube_map_bins); // Number of frequency bins
      write(f, "cube_map_freq_min", cube_map_freq_min); // Frequency extrema [freq units]
      write(f, "cube_map_freq_max", cube_map_freq_max); // Calculated when freq is initialized
      write(f, "cube_map_freq_bin_width", cube_map_freq_bin_width); // Frequency bin width [freq units]
      if (cosmological)
        write(f, "observed_cube_map_bin_width", observed_cube_map_bin_width); // Observed wavelength resolution [angstrom]
    }
  }

  // Line-of-sight angular cosine (μ=cosθ) map
  if (output_mu) {
    const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
    write(f, "n_mu", n_mu);                  // Number of mu bins
    write(f, "mu", mu1);                     // Escape fraction map [fraction]
    if (output_mu2)
      write(f, "mu2", mu2);                  // Escape fraction^2 map [statistic]
    if (output_freq_mu)
      write(f, "freq_mu", freq_mu, freq_units); // Average frequency map [freq units]
    if (output_freq2_mu)
      write(f, "freq2_mu", freq2_mu, "("+freq_units+")^2"); // Frequency^2 map [freq^2 units]
    if (output_freq3_mu)
      write(f, "freq3_mu", freq3_mu, "("+freq_units+")^3"); // Frequency^3 map [freq^3 units]
    if (output_freq4_mu)
      write(f, "freq4_mu", freq4_mu, "("+freq_units+")^4"); // Frequency^4 map [freq^4 units]
  }

  // Line-of-sight angular cosine (μ=cosθ) flux map
  if (output_flux_mu) {
    const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
    const string flux_units = (cosmological) ? "erg/s/cm^2/angstrom" : "erg/s" + per_freq_units;
    write(f, "n_mu_flux", n_mu_flux);        // Healpix n_side parameter
    write(f, "flux_mu", flux_mu, flux_units); // Spectral flux map [erg/s/cm^2/angstrom]
    if (!have_cameras || n_mu_bins != n_bins || mu_freq_min != freq_min || mu_freq_max != freq_max) {
      write(f, "n_mu_bins", n_mu_bins);      // Number of flux map frequency bins
      write(f, "mu_freq_min", mu_freq_min);  // Flux map frequency extrema [freq units]
      write(f, "mu_freq_max", mu_freq_max);  // Calculated when freq is initialized
      write(f, "mu_freq_bin_width", mu_freq_bin_width); // Flux map frequency bin width [freq units]
      if (cosmological)
        write(f, "observed_mu_bin_width", observed_mu_bin_width); // Observed wavelength resolution [angstrom]
    }
  }

  // Photon data
  if (output_photons) {
    if (photon_file.empty()) {               // Output photons to the same file
      Group g = f.createGroup("/photons");
      write_photons(g);                      // Write to the photons group
    } else {                                 // Output a separate photon file
      H5File f_photons(output_dir + "/" + output_base + "_" + photon_file +
                       snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
      write_sim_info(f_photons);             // Write general simulation info
      write_mcrt_info(f_photons);            // Write general MCRT info
      write_photons(f_photons);              // Write to the main group
    }
  }

  if (output_stars) {
    rescale(source_weight_stars, L_tot);     // Convert to erg/s
    rescale(weight_stars, L_tot);
    Group g = exists(f, "/stars") ? f.openGroup("/stars") : f.createGroup("/stars"); // Stars group
    write(g, "L_int", L_int_stars, "erg/s"); // Intrinsic luminosity for each star [erg/s]
    write(g, "L_src", source_weight_stars, "erg/s"); // Photon luminosity at emission for each star [erg/s]
    write(g, "L_esc", weight_stars, "erg/s"); // Photon luminosity at escape for each star [erg/s]
  }

  if (output_cells) {
    rescale(source_weight_cells, L_tot);     // Convert to erg/s
    rescale(weight_cells, L_tot);
    Group g = f.createGroup("/cells");       // Write to the cells group
    write(g, "L_int", L_int_cells, "erg/s"); // Intrinsic luminosity for each cell [erg/s]
    write(g, "L_src", source_weight_cells, "erg/s"); // Photon luminosity at emission for each cell [erg/s]
    write(g, "L_esc", weight_cells, "erg/s"); // Photon luminosity at escape for each cell [erg/s]
  }

  if (output_cells && doublet_cell_based_emission) {
    rescale(source_weight_doublet_cells, L_tot); // Convert to erg/s
    rescale(weight_doublet_cells, L_tot);
    Group g = f.createGroup("/doublet_cells"); // Write to the doublet cells group
    write(g, "L_int", L_int_doublet_cells, "erg/s"); // Intrinsic doublet luminosity for each cell [erg/s]
    write(g, "L_src", source_weight_doublet_cells, "erg/s"); // Doublet photon luminosity at emission for each cell [erg/s]
    write(g, "L_esc", weight_doublet_cells, "erg/s"); // Doublet photon luminosity at escape for each cell [erg/s]
  }

  // Dual grid data
  if constexpr (output_radial_flow)
    write_radial_flow(f);                    // Radial flow statistics [photons/s]
  if constexpr (output_group_flows)
    write_halo_flows(f, true);               // Group flow statistics [photons/s]
  if constexpr (output_subhalo_flows)
    write_halo_flows(f, false);              // Subhalo flow statistics [photons/s]

  // Group information
  if constexpr (output_groups) {
    if (n_groups > 0) {                      // Have groups
      Group g = f.createGroup("/group");
      write(g, "id", group_id);              // Group IDs
      write(g, "L", L_grp, "photons/s");     // Group photon rates [photons/s]
      write(g, "f_src", f_src_grp);          // Group source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_grp); // Effective number of emitted photons: 1/<w0>
      if (output_grp_obs) {
        write(g, "f_esc", f_esc_grp);        // Group escape fractions: sum(w)
        write(g, "n_photons_esc", n_photons_esc_grp); // Effective number of escaped photons: 1/<w>
      }
      if (output_grp_vir) {
        write(g, "L_vir", L_grp_vir, "photons/s"); // Group photon rates [photons/s]
        write(g, "f_src_vir", f_src_grp_vir); // Group source fractions: sum(w0)
        write(g, "n_photons_src_vir", n_photons_src_grp_vir); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_vir", f_esc_grp_vir); // Group escape fractions: sum(w)
        write(g, "n_photons_esc_vir", n_photons_esc_grp_vir); // Effective number of escaped photons: 1/<w>
      }
      if (output_grp_gal) {
        write(g, "L_gal", L_grp_gal, "photons/s"); // Group photon rates [photons/s]
        write(g, "f_src_gal", f_src_grp_gal); // Group source fractions: sum(w0)
        write(g, "n_photons_src_gal", n_photons_src_grp_gal); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_gal", f_esc_grp_gal); // Group escape fractions: sum(w)
        write(g, "n_photons_esc_gal", n_photons_esc_grp_gal); // Effective number of escaped photons: 1/<w>
      }
      if (focus_groups_on_emission) {
        write(g, "r_light", r_light_grp, "cm"); // Center of luminosity position [cm]
        write(g, "v_light", v_light_grp, "cm/s"); // Center of luminosity velocity [cm/s]
      }

      // Line-of-sight healpix group maps
      if (output_map_grp) {
        const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
        const int n_pix_grp = 12 * n_side_grp * n_side_grp; // 12 n_side^2
        write(g, "n_side", n_side_grp);      // Healpix n_side parameter
        write(g, "n_pix", n_pix_grp);        // Number of map pixels
        if (output_grp_obs)
          write(g, "map", map_grp);          // Escape fraction maps [fraction]
        if (output_grp_vir)
          write(g, "map_vir", map_grp_vir);  // Escape fraction maps [fraction] (virial radius)
        if (output_grp_gal)
          write(g, "map_gal", map_grp_gal);  // Escape fraction maps [fraction] (galaxy radius)
        if (output_map2_grp) {
          if (output_grp_obs)
            write(g, "map2", map2_grp);      // Escape fraction^2 maps [statistic]
          if (output_grp_vir)
            write(g, "map2_vir", map2_grp_vir); // Escape fraction^2 maps [statistic] (virial radius)
          if (output_grp_gal)
            write(g, "map2_gal", map2_grp_gal); // Escape fraction^2 maps [statistic] (galaxy radius)
        }
        if (output_freq_map_grp)
          write(g, "freq_map", freq_map_grp, freq_units); // Average frequency maps [freq units]
        if (output_freq2_map_grp)
          write(g, "freq2_map", freq2_map_grp, "("+freq_units+")^2"); // Frequency^2 maps [freq^2 units]
        if (output_freq3_map_grp)
          write(g, "freq3_map", freq3_map_grp, "("+freq_units+")^3"); // Frequency^3 maps [freq^3 units]
        if (output_freq4_map_grp)
          write(g, "freq4_map", freq4_map_grp, "("+freq_units+")^4"); // Frequency^4 maps [freq^4 units]
      }

      // Line-of-sight healpix group flux map
      if (output_flux_map_grp) {
        const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
        const string flux_units = (cosmological) ? "erg/s/cm^2/angstrom" : "erg/s" + per_freq_units;
        const int n_pix_flux_grp = 12 * n_side_flux_grp * n_side_flux_grp; // 12 n_side^2
        write(g, "n_side_flux", n_side_flux_grp); // Healpix n_side parameter
        write(g, "n_pix_flux", n_pix_flux_grp); // Number of flux map pixels
        write(g, "flux_map", flux_map_grp, flux_units); // Spectral flux maps [erg/s/cm^2/angstrom]
        if (!have_cameras || n_map_bins_grp != n_bins || map_freq_min_grp != freq_min || map_freq_max_grp != freq_max) {
          write(g, "n_map_bins", n_map_bins_grp); // Number of flux map frequency bins
          write(g, "map_freq_min", map_freq_min_grp); // Flux map frequency extrema [freq units]
          write(g, "map_freq_max", map_freq_max_grp); // Calculated when freq is initialized
          write(g, "map_freq_bin_width", map_freq_bin_width_grp); // Flux map frequency bin width [freq units]
          if (cosmological)
            write(g, "observed_map_bin_width", observed_map_bin_width_grp); // Observed wavelength resolution [angstrom]
        }
      }

      // Angle-averaged spectral flux (groups)
      if (output_flux_avg_grp) {
        const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
        const string flux_units = (cosmological) ? "erg/s/cm^2/angstrom" : "erg/s" + per_freq_units;
        write(g, "flux_avg", flux_avg_grp, flux_units); // Spectral flux avgs [erg/s/cm^2/angstrom]
        if (!have_cameras || n_bins_grp != n_bins || freq_min_grp != freq_min || freq_max_grp != freq_max) {
          write(g, "n_bins", n_bins_grp);    // Number of flux avg frequency bins
          write(g, "freq_min", freq_min_grp); // Flux avg frequency extrema [freq units]
          write(g, "freq_max", freq_max_grp); // Calculated when freq is initialized
          write(g, "freq_bin_width", freq_bin_width_grp); // Flux avg frequency bin width [freq units]
          if (cosmological)
            write(g, "observed_bin_width", observed_bin_width_grp); // Observed wavelength resolution [angstrom]
        }
      }

      // Line-of-sight healpix radial group maps
      if (output_radial_map_grp) {
        string image_units = (cosmological) ? "erg/s/cm^2/arcsec^2" : "erg/s/cm^2";
        const int n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // 12 n_side^2
        write(g, "n_side_radial", n_side_radial_grp); // Healpix n_side parameter
        write(g, "n_pix_radial", n_pix_radial_grp); // Number of radial map pixels
        write(g, "radial_map", radial_map_grp, image_units); // Radial surface brightness group maps [erg/s/cm^2/arcsec^2]
        if (!have_cameras || n_map_pixels_grp != n_radial_pixels || map_radius_grp != radial_image_radius) {
          write(g, "n_map_pixels", n_map_pixels_grp); // Number of radial pixels
          write(g, "map_radius", map_radius_grp); // Radius [cm] (defaults to half domain)
          write(g, "map_pixel_width", map_pixel_width_grp); // Radial pixel width [cm]
          write(g, "map_pixel_areas", map_pixel_areas_grp, "cm^2"); // Radial pixel areas [cm^2]
          if (cosmological) {
            write(g, "map_pixel_arcsec", map_pixel_arcsec_grp); // Radial pixel width [arcsec] (optional)
            write(g, "map_pixel_arcsec2", map_pixel_arcsec2_grp, "arcsec^2"); // Radial pixel areas [arcsec^2] (optional)
          }
        }
      }

      // Angle-averaged radial surface brightness
      if (output_radial_avg_grp) {
        string image_units = (cosmological) ? "erg/s/cm^2/arcsec^2" : "erg/s/cm^2";
        write(g, "radial_avg", radial_avg_grp, image_units); // Radial surface brightness group avgs [erg/s/cm^2/arcsec^2]
        if (!have_cameras || n_pixels_grp != n_radial_pixels || radius_grp != radial_image_radius) {
          write(g, "n_pixels", n_pixels_grp); // Number of radial pixels
          write(g, "radius", radius_grp);    // Radius [cm] (defaults to half domain)
          write(g, "pixel_width", pixel_width_grp); // Radial pixel width [cm]
          write(g, "pixel_areas", pixel_areas_grp, "cm^2"); // Radial pixel areas [cm^2]
          if (cosmological) {
            write(g, "pixel_arcsec", pixel_arcsec_grp); // Radial pixel width [arcsec] (optional)
            write(g, "pixel_arcsec2", pixel_arcsec2_grp, "arcsec^2"); // Radial pixel areas [arcsec^2] (optional)
          }
        }
      }

      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission) {
          write(g, "L_stars", L_stars_grp, "photons/s"); // Group stellar photon rates [photons/s]
          if (output_grp_vir)
            write(g, "L_stars_vir", L_stars_grp_vir, "photons/s"); // Group stellar photon rates [photons/s]
          if (output_grp_gal)
            write(g, "L_stars_gal", L_stars_grp_gal, "photons/s"); // Group stellar photon rates [photons/s]
        }
        write(g, "n_stars_eff", n_stars_eff_grp); // Effective number of stars: 1/<w>
        if (output_grp_vir)
          write(g, "n_stars_eff_vir", n_stars_eff_grp_vir); // Effective number of stars: 1/<w>
        if (output_grp_gal)
          write(g, "n_stars_eff_gal", n_stars_eff_grp_gal); // Effective number of stars: 1/<w>
      }
      if (cell_based_emission) {
        if (star_based_emission) {
          write(g, "L_gas", L_gas_grp, "photons/s"); // Group gas photon rates [photons/s]
          if (output_grp_vir)
            write(g, "L_gas_vir", L_gas_grp_vir, "photons/s"); // Group gas photon rates [photons/s]
          if (output_grp_gal)
            write(g, "L_gas_gal", L_gas_grp_gal, "photons/s"); // Group gas photon rates [photons/s]
        }
        write(g, "n_cells_eff", n_cells_eff_grp); // Effective number of cells: 1/<w>
        if (output_grp_vir)
          write(g, "n_cells_eff_vir", n_cells_eff_grp_vir); // Effective number of cells: 1/<w>
        if (output_grp_gal)
          write(g, "n_cells_eff_gal", n_cells_eff_grp_gal); // Effective number of cells: 1/<w>
      }
    }
    if (n_ugroups > 0) {      // Have unfiltered groups
      Group g = f.createGroup("/group_unfiltered"), g_HI, g_bin;
      write(g, "id", ugroup_id);             // Unfiltered group IDs
      write(g, "L", L_ugrp, "photons/s");    // Unfiltered group photon rates [photons/s]
      write(g, "f_src", f_src_ugrp);         // Unfiltered group source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_ugrp); // Effective number of emitted photons: 1/<w0>

      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission)
          write(g, "L_stars", L_stars_ugrp, "photons/s"); // Unfiltered group stellar photon rates [photons/s]
        write(g, "n_stars_eff", n_stars_eff_ugrp); // Effective number of stars: 1/<w>
      }
      if (cell_based_emission) {
        if (star_based_emission)
          write(g, "L_gas", L_gas_ugrp, "photons/s"); // Unfiltered group gas photon rates [photons/s]
        write(g, "n_cells_eff", n_cells_eff_ugrp); // Effective number of cells: 1/<w>
      }
    }
  }

  // Subhalo information
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {                    // Have subhalos
      Group g = f.createGroup("/subhalo"), g_HI, g_bin;
      write(g, "id", subhalo_id);            // Subhalo IDs
      write(g, "L", L_sub, "photons/s");     // Subhalo photon rates [photons/s]
      write(g, "f_src", f_src_sub);          // Subhalo source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_sub); // Effective number of emitted photons: 1/<w0>
      if (output_sub_obs) {
        write(g, "f_esc", f_esc_sub);        // Subhalo escape fractions: sum(w)
        write(g, "n_photons_esc", n_photons_esc_sub); // Effective number of escaped photons: 1/<w>
      }
      if (output_sub_vir) {
        write(g, "L_vir", L_sub_vir, "photons/s"); // Subhalo photon rates [photons/s]
        write(g, "f_src_vir", f_src_sub_vir); // Subhalo source fractions: sum(w0)
        write(g, "n_photons_src_vir", n_photons_src_sub_vir); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_vir", f_esc_sub_vir); // Subhalo escape fractions: sum(w)
        write(g, "n_photons_esc_vir", n_photons_esc_sub_vir); // Effective number of escaped photons: 1/<w>
      }
      if (output_sub_gal) {
        write(g, "L_gal", L_sub_gal, "photons/s"); // Subhalo photon rates [photons/s]
        write(g, "f_src_gal", f_src_sub_gal); // Subhalo source fractions: sum(w0)
        write(g, "n_photons_src_gal", n_photons_src_sub_gal); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_gal", f_esc_sub_gal); // Subhalo escape fractions: sum(w)
        write(g, "n_photons_esc_gal", n_photons_esc_sub_gal); // Effective number of escaped photons: 1/<w>
      }
      if (focus_subhalos_on_emission) {
        write(g, "r_light", r_light_sub, "cm"); // Center of luminosity position [cm]
        write(g, "v_light", v_light_sub, "cm/s"); // Center of luminosity velocity [cm/s]
      }

      // Line-of-sight healpix subhalo maps
      if (output_map_sub) {
        const string freq_units = (doppler_frequency) ? "Doppler" : "km/s";
        const int n_pix_sub = 12 * n_side_sub * n_side_sub; // 12 n_side^2
        write(g, "n_side", n_side_sub);      // Healpix n_side parameter
        write(g, "n_pix", n_pix_sub);        // Number of map pixels
        if (output_sub_obs)
          write(g, "map", map_sub);          // Escape fraction maps [fraction]
        if (output_sub_vir)
          write(g, "map_vir", map_sub_vir);  // Escape fraction maps [fraction] (virial radius)
        if (output_sub_gal)
          write(g, "map_gal", map_sub_gal);  // Escape fraction maps [fraction] (galaxy radius)
        if (output_map2_sub) {
          if (output_sub_obs)
            write(g, "map2", map2_sub);      // Escape fraction^2 maps [statistic]
          if (output_sub_vir)
            write(g, "map2_vir", map2_sub_vir); // Escape fraction^2 maps [statistic] (virial radius)
          if (output_sub_gal)
            write(g, "map2_gal", map2_sub_gal); // Escape fraction^2 maps [statistic] (galaxy radius)
        }
        if (output_freq_map_sub)
          write(g, "freq_map", freq_map_sub, freq_units); // Average frequency maps [freq units]
        if (output_freq2_map_sub)
          write(g, "freq2_map", freq2_map_sub, "("+freq_units+")^2"); // Frequency^2 maps [freq^2 units]
        if (output_freq3_map_sub)
          write(g, "freq3_map", freq3_map_sub, "("+freq_units+")^3"); // Frequency^3 maps [freq^3 units]
        if (output_freq4_map_sub)
          write(g, "freq4_map", freq4_map_sub, "("+freq_units+")^4"); // Frequency^4 maps [freq^4 units]
      }

      // Line-of-sight healpix subhalo flux map
      if (output_flux_map_sub) {
        const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
        const string flux_units = (cosmological) ? "erg/s/cm^2/angstrom" : "erg/s" + per_freq_units;
        const int n_pix_flux_sub = 12 * n_side_flux_sub * n_side_flux_sub; // 12 n_side^2
        write(g, "n_side_flux", n_side_flux_sub); // Healpix n_side parameter
        write(g, "n_pix_flux", n_pix_flux_sub); // Number of flux map pixels
        write(g, "flux_map", flux_map_sub, flux_units); // Spectral flux maps [erg/s/cm^2/angstrom]
        if (!have_cameras || n_map_bins_sub != n_bins || map_freq_min_sub != freq_min || map_freq_max_sub != freq_max) {
          write(g, "n_map_bins", n_map_bins_sub); // Number of flux map frequency bins
          write(g, "map_freq_min", map_freq_min_sub); // Flux map frequency extrema [freq units]
          write(g, "map_freq_max", map_freq_max_sub); // Calculated when freq is initialized
          write(g, "map_freq_bin_width", map_freq_bin_width_sub); // Flux map frequency bin width [freq units]
          if (cosmological)
            write(g, "observed_map_bin_width", observed_map_bin_width_sub); // Observed wavelength resolution [angstrom]
        }
      }

      // Angle-averaged spectral flux (subhalos)
      if (output_flux_avg_sub) {
        const string per_freq_units = (doppler_frequency) ? "/Doppler" : "/(km/s)";
        const string flux_units = (cosmological) ? "erg/s/cm^2/angstrom" : "erg/s" + per_freq_units;
        write(g, "flux_avg", flux_avg_sub, flux_units); // Spectral flux avgs [erg/s/cm^2/angstrom]
        if (!have_cameras || n_bins_sub != n_bins || freq_min_sub != freq_min || freq_max_sub != freq_max) {
          write(g, "n_bins", n_bins_sub);    // Number of flux avg frequency bins
          write(g, "freq_min", freq_min_sub); // Flux avg frequency extrema [freq units]
          write(g, "freq_max", freq_max_sub); // Calculated when freq is initialized
          write(g, "freq_bin_width", freq_bin_width_sub); // Flux avg frequency bin width [freq units]
          if (cosmological)
            write(g, "observed_bin_width", observed_bin_width_sub); // Observed wavelength resolution [angstrom]
        }
      }

      // Line-of-sight healpix radial subhalo maps
      if (output_radial_map_sub) {
        string image_units = (cosmological) ? "erg/s/cm^2/arcsec^2" : "erg/s/cm^2";
        const int n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // 12 n_side^2
        write(g, "n_side_radial", n_side_radial_sub); // Healpix n_side parameter
        write(g, "n_pix_radial", n_pix_radial_sub); // Number of radial map pixels
        write(g, "radial_map", radial_map_sub, image_units); // Radial surface brightness subhalo maps [erg/s/cm^2/arcsec^2]
        if (!have_cameras || n_map_pixels_sub != n_radial_pixels || map_radius_sub != radial_image_radius) {
          write(g, "n_map_pixels", n_map_pixels_sub); // Number of radial pixels
          write(g, "map_radius", map_radius_sub); // Radius [cm] (defaults to half domain)
          write(g, "map_pixel_width", map_pixel_width_sub); // Radial pixel width [cm]
          write(g, "map_pixel_areas", map_pixel_areas_sub, "cm^2"); // Radial pixel areas [cm^2]
          if (cosmological) {
            write(g, "map_pixel_arcsec", map_pixel_arcsec_sub); // Radial pixel width [arcsec] (optional)
            write(g, "map_pixel_arcsec2", map_pixel_arcsec2_sub, "arcsec^2"); // Radial pixel areas [arcsec^2] (optional)
          }
        }
      }

      // Angle-averaged radial surface brightness
      if (output_radial_avg_sub) {
        string image_units = (cosmological) ? "erg/s/cm^2/arcsec^2" : "erg/s/cm^2";
        write(g, "radial_avg", radial_avg_sub, image_units); // Radial surface brightness group avgs [erg/s/cm^2/arcsec^2]
        if (!have_cameras || n_pixels_sub != n_radial_pixels || radius_sub != radial_image_radius) {
          write(g, "n_pixels", n_pixels_sub); // Number of radial pixels
          write(g, "radius", radius_sub);    // Radius [cm] (defaults to half domain)
          write(g, "pixel_width", pixel_width_sub); // Radial pixel width [cm]
          write(g, "pixel_areas", pixel_areas_sub, "cm^2"); // Radial pixel areas [cm^2]
          if (cosmological) {
            write(g, "pixel_arcsec", pixel_arcsec_sub); // Radial pixel width [arcsec] (optional)
            write(g, "pixel_arcsec2", pixel_arcsec2_sub, "arcsec^2"); // Radial pixel areas [arcsec^2] (optional)
          }
        }
      }

      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission) {
          write(g, "L_stars", L_stars_sub, "photons/s"); // Subhalo stellar photon rates [photons/s]
          if (output_sub_vir)
            write(g, "L_stars_vir", L_stars_sub_vir, "photons/s"); // Subhalo stellar photon rates [photons/s]
          if (output_sub_gal)
            write(g, "L_stars_gal", L_stars_sub_gal, "photons/s"); // Subhalo stellar photon rates [photons/s]
        }
        write(g, "n_stars_eff", n_stars_eff_sub); // Effective number of stars: 1/<w>
        if (output_sub_vir)
          write(g, "n_stars_eff_vir", n_stars_eff_sub_vir); // Effective number of stars: 1/<w>
        if (output_sub_gal)
          write(g, "n_stars_eff_gal", n_stars_eff_sub_gal); // Effective number of stars: 1/<w>
      }
      if (cell_based_emission) {
        if (star_based_emission) {
          write(g, "L_gas", L_gas_sub, "photons/s"); // Subhalo gas photon rates [photons/s]
          if (output_sub_vir)
            write(g, "L_gas_vir", L_gas_sub_vir, "photons/s"); // Subhalo gas photon rates [photons/s]
          if (output_sub_gal)
            write(g, "L_gas_gal", L_gas_sub_gal, "photons/s"); // Subhalo gas photon rates [photons/s]
        }
        write(g, "n_cells_eff", n_cells_eff_sub); // Effective number of cells: 1/<w>
        if (output_sub_vir)
          write(g, "n_cells_eff_vir", n_cells_eff_sub_vir); // Effective number of cells: 1/<w>
        if (output_sub_gal)
          write(g, "n_cells_eff_gal", n_cells_eff_sub_gal); // Effective number of cells: 1/<w>
      }
    }
    if (n_usubhalos > 0) {  // Have unfiltered subhalos
      Group g = f.createGroup("/subhalo_unfiltered"), g_HI, g_bin;
      write(g, "id", usubhalo_id);           // Unfiltered subhalo IDs
      write(g, "L", L_usub, "photons/s");    // Unfiltered subhalo photon rates [photons/s]
      write(g, "f_src", f_src_usub);         // Unfiltered subhalo source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_usub); // Effective number of emitted photons: 1/<w0>

      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission)
          write(g, "L_stars", L_stars_usub, "photons/s"); // Unfiltered group stellar photon rates [photons/s]
        write(g, "n_stars_eff", n_stars_eff_usub); // Effective number of stars: 1/<w>
      }
      if (cell_based_emission) {
        if (star_based_emission)
          write(g, "L_gas", L_gas_usub, "photons/s"); // Unfiltered group gas photon rates [photons/s]
        write(g, "n_cells_eff", n_cells_eff_usub); // Effective number of cells: 1/<w>
      }
    }
  }

  // Radiation data
  if constexpr (output_energy_density)
    write(f, "u_rad", u_rad, "erg/cm^3");    // Radiation energy density [erg/cm^3]
  if constexpr (SPHERICAL) {
    if constexpr (output_acceleration)
      write(f, "a_rad_r", a_rad_r, "cm/s^2"); // Radial radiation acceleration [cm/s^2]
    if constexpr (output_acceleration_scat)
      write(f, "a_rad_r_scat", a_rad_r_scat, "cm/s^2"); // Radial scattering-based acceleration [cm/s^2]
    if constexpr (output_pressure)
      write(f, "P_rad_r", P_rad_r, "erg/cm^3"); // Radial radiation pressure [erg/cm^3]
    if constexpr (output_f_src_r)
      write(f, "f_src_r", f_src_r);          // Cumulative source fraction
    if constexpr (output_tau_dust_f_esc)
      write(f, "tau_dust_f_esc", tau_dust_f_esc); // Cumulative dust optical depth at escape
    if constexpr (output_f_esc_r)
      write(f, "f_esc_r", f_esc_r);          // Cumulative escape fraction
    if constexpr (output_rescaled_f_esc_r)
      write(f, "rescaled_f_esc_r", rescaled_f_esc_r); // Cumulative rescaled escape fraction
    if constexpr (output_trap_r)
      write(f, "trap_r", trap_r);            // Cumulative t_trap / t_light
    if constexpr (output_M_F_r)
      write(f, "M_F_r", M_F_r);              // Cumulative force multiplier
    if constexpr (output_M_F_r_scat)
      write(f, "M_F_r_scat", M_F_r_scat);    // Cumulative force multiplier (scattering-based)
    if constexpr (output_M_F_src)
      write(f, "M_F_src", M_F_src);          // Cumulative source force multiplier (by source)
    if constexpr (output_M_F_src_scat)
      write(f, "M_F_src_scat", M_F_src_scat); // Cumulative source force multiplier (by source, scattering-based)
    if constexpr (output_P_u_r)
      write(f, "P_u_r", P_u_r);              // Cumulative pressure-to-energy density ratio (P/u)
  } else {
    if constexpr (output_acceleration)
      write(f, "a_rad", a_rad, "cm/s^2");    // Radiation acceleration [cm/s^2]
    if constexpr (output_acceleration_scat)
      write(f, "a_rad_scat", a_rad_scat, "cm/s^2"); // Scattering-based acceleration [cm/s^2]
  }
}
