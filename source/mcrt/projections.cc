/***********************
 * mcrt/projections.cc *
 ***********************

 * Driver: Assign rays for projections, etc.

*/

#include "proto.h"
#include "MCRT.h"
#include "../timing.h" // Timing functionality

extern Timer mcrt_proj_timer; // Clock timing
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

// Quad tree data structure
struct QuadTreeNode {
  int children;                              // Index of the first child node
  double rtol;                               // Node relative tolerance
  double xL, xR;                             // Node x range [xmin, xmax]
  double yL, yR;                             // Node y range [ymin, ymax]
  double fLL, fLR, fRL, fRR;                 // Pass pre-calculated values to children
  double fc;                                 // Cumulative integral over node region

  QuadTreeNode() = default;
  QuadTreeNode(double rtol, double xL, double xR, double yL, double yR,
               double fLL, double fLR, double fRL, double fRR) :
               children(-1), rtol(rtol), xL(xL), xR(xR), yL(yL), yR(yR),
               fLL(fLL), fLR(fLR), fRL(fRL), fRR(fRR), fc(0.) {};
};

static int start_cell;                       // Starting cell for searches
#pragma omp threadprivate(start_cell)        // Each thread needs start_cell
static Image corners;                        // Pixel corner values
static vector<vector<QuadTreeNode>> trees;   // Quad tree arrangement of nodes
static Vec3 direction;                       // Camera direction
static Vec3 xaxis;                           // Camera x-axis
static Vec3 yaxis;                           // Camera y-axis

/* Intrinsic emission rule function. */
static void emission_rule(double& result, double dl, int cell) {
  result += j_line[cell] * dl;               // Volume integrated emissivity
}

/* Intrinsic doublet emission rule function. */
static void doublet_emission_rule(double& result, double dl, int cell) {
  result += (j_line[cell] + jp_line[cell]) * dl; // Volume integrated emissivity
}

/* Attenuated emission rule function. */
static void attenuation_rule(double& result, double dl, int cell) {
  const double dtau = k_dust[cell] * dl;
  if (dtau < 1e-5) {                         // Numerical stability
    result *= 1. - dtau;                     // Attenuate incoming flux
    result += j_line[cell] * (1. - 0.5 * dtau) * dl; // Add escaping emission
  } else {
    const double exp_tau = exp(-dtau);
    result *= exp_tau;                       // Attenuate incoming flux
    result += j_line[cell] * (1. - exp_tau) / k_dust[cell]; // Add escaping emission
  }
}

/* Attenuated doublet emission rule function. */
static void doublet_attenuation_rule(double& result, double dl, int cell) {
  const double dtau = k_dust[cell] * dl;
  if (dtau < 1e-5) {                         // Numerical stability
    result *= 1. - dtau;                     // Attenuate incoming flux
    result += (j_line[cell] + jp_line[cell]) * (1. - 0.5 * dtau) * dl; // Add escaping emission
  } else {
    const double exp_tau = exp(-dtau);
    result *= exp_tau;                       // Attenuate incoming flux
    result += (j_line[cell] + jp_line[cell]) * (1. - exp_tau) / k_dust[cell]; // Add escaping emission
  }
}

/* Calculate projections for a single line of sight. */
template <void (*rule)(double &, double, int)>
double MCRT::ray_trace(const double rx, const double ry) {
  // Set the ray starting point based on (rx,ry,-proj_radius)
  Vec3 point = {camera_center.x + rx * xaxis.x
                                + ry * yaxis.x
                                - proj_radius * direction.x,
                camera_center.y + rx * xaxis.y
                                + ry * yaxis.y
                                - proj_radius * direction.y,
                camera_center.z + rx * xaxis.z
                                + ry * yaxis.z
                                - proj_radius * direction.z};
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell
  int next_cell;                             // Next cell index
  double dl, l_stop = proj_depth;            // Path lengths [cm]
  double result = 0.;                        // Integration result

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }
    rule(result, dl, cell);                  // Volume-weighted integration
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        break;
    }
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += direction * dl;                 // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }
  return result;
}

// Adaptive quadrature 2D integrator
template <void (*rule)(double &, double, int)>
double MCRT::pixel_quad_2D(const int ix, const int iy) {
  // Additional quadtree variables
  double rtol, xL, xM, xR, yL, yM, yR;
  double fLL, fLR, fRL, fRR, fLM, fML, fMM, fMR, fRM, f_trap, f_simp;
  auto& tree = trees[thread];                // Reference to the local tree

  // Initialize the base node
  int n_nodes = 1;                           // Initialize the node counter
  tree.resize(0);                            // Clear the tree (retain capacity)
  tree.emplace_back(                         // Start with no children
    pixel_rtol,                              // Base tolerance is target tolerance
    image_xedges[ix], image_xedges[ix+1],    // Domain: x in (xL,xR)
    image_yedges[iy], image_yedges[iy+1],    // Domain: y in (yL,yR)
    corners(ix,iy), corners(ix,iy+1),        // LL, LR corners
    corners(ix+1,iy), corners(ix+1,iy+1)     // RL, RR corners, cumulative integral
  );

  // Populate the quadtree
  for (int i = 0; i < n_nodes; ++i) {        // Note: n_nodes changes size
    rtol = tree[i].rtol;                     // Relative tolerance
    xL = tree[i].xL;  xR = tree[i].xR;       // Domain x edges
    yL = tree[i].yL;  yR = tree[i].yR;       // Domain y edges
    fLL = tree[i].fLL;  fLR = tree[i].fLR;   // LL, LR corners
    fRL = tree[i].fRL;  fRR = tree[i].fRR;   // RL, RR corners
    xM = 0.5 * (xL + xR);                    // Local node x midpoint
    yM = 0.5 * (yL + yR);                    // Local node y midpoint
    fLM = ray_trace<rule>(xL, yM);           // Midpoint evaluations
    fML = ray_trace<rule>(xM, yL);
    fMM = ray_trace<rule>(xM, yM);
    fMR = ray_trace<rule>(xM, yR);
    fRM = ray_trace<rule>(xR, yM);

    // Note: Estimates are multiplied by 36/Area for convenience and corrected later
    f_trap = fLL + fLR + fRL + fRR;          // Trapezoid rule
    f_simp = f_trap + 4.*(fLM + fML + fMR + fRM) + 16.*fMM; // Simpson's rule
    // Check whether the node is converged
    if (fabs(9.*f_trap - f_simp) <= rtol * fabs(f_simp)) {
      tree[i].fc = f_simp;                   // Save converged value
      continue;                              // No need to refine this node
    }

    // Refine the current node
    tree[i].children = n_nodes;              // Append children to the end
    n_nodes += 4;                            // Add four children nodes
    const double child_rtol = 2. * rtol;     // Factor of sqrt(4) per refinement level

    //  Layout of children:
    //
    // (yR) fLR-----fMR-----fRR
    //       |       |       |
    //       |   2   |   4   |
    //       |       |       |
    // (yM) fLM-----fMM-----fRM
    //       |       |       |
    //       |   1   |   3   |
    //       |       |       |
    // (yL) fLL-----fML-----fRL
    //
    //      (xL)    (xM)    (xR)

    // Initialize 1st child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yL, yM,                        // (xL,xR,yL,yR) = (xL,xM,yL,xM)
      fLL, fLM, fML, fMM                     // (LL,LR,RL,RR) = (LL,LM,ML,MM)
    );

    // Initialize 2nd child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yM, yR,                        // (xL,xR,yL,yR) = (xL,xM,yM,xR)
      fLM, fLR, fMM, fMR                     // (LL,LR,RL,RR) = (LM,LR,MM,MR)
    );

    // Initialize 3rd child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yL, yM,                        // (xL,xR,yL,yR) = (xM,xR,yL,yM)
      fML, fMM, fRL, fRM                     // (LL,LR,RL,RR) = (ML,MM,RL,RM)
    );

    // Initialize 4th child
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yM, yR,                        // (xL,xR,yL,yR) = (xM,xR,yM,yR)
      fMM, fMR, fRM, fRR                     // (LL,LR,RL,RR) = (MM,MR,RM,RR)
    );
  }

  // Aggregation step for converged sub-domain integrals
  for (int ci, i = n_nodes - 1; i >= 0; --i) {
    ci = tree[i].children;
    if (ci > 0) // The node has children (i.e. not a leaf node)
      tree[i].fc = 0.25 * (tree[ci].fc + tree[ci+1].fc + tree[ci+2].fc + tree[ci+3].fc);
  }

  // Note: Simpson estimate was multiplied by 36/Area for efficiency
  return tree[0].fc / 36.;
}

/* Ray trace for projection calculations. */
template <void (*rule)(double &, double, int)>
void MCRT::calculate_projections(Image& proj_image) {
  // Pre-calculate the pixel corner values
  const int Np1 = nx_pixels + 1;              // Number of pixel edges
  const int Np2 = ny_pixels + 1;
  const int n_corners = Np1 * Np2;           // Total number of corners
  const int corner_interval = n_corners / 100; // Interval between updates
  int n_finished = 0;                        // Progress for printing
  if (root)
    cout << "\n    Pixel corner calculations:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n_corners; ++i) {
    // Ray tracing for each corner
    const int ix = i / Np1;                  // Corner ix,iy indices
    const int iy = i % Np1;
    const double rx = image_xedges[ix];      // Corner position
    const double ry = image_yedges[iy];
    corners(ix,iy) = ray_trace<rule>(rx, ry);

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % corner_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n_corners) << "%\b\b\b\b" << std::flush;
    }
  }

  // Perform adaptive convergence for all pixels
  if (root)
    cout << "100%\n    Pixel convergence calculations:   0%\b\b\b\b" << std::flush;
  const int n2_pixels = nx_pixels * ny_pixels; // Total number of pixels
  const int pixel_interval = n2_pixels / 100; // Interval between updates
  n_finished = 0;                            // Reset progress counter
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n2_pixels; ++i) {
    // Ray tracing for each pixel
    const int ix = i / ny_pixels;             // Pixel ix,iy indices
    const int iy = i % ny_pixels;
    proj_image(ix,iy) = pixel_quad_2D<rule>(ix, iy);

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % pixel_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n2_pixels) << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Bin luminosity from unresolved HII regions. */
void MCRT::calculate_projections_HII(Image& proj_image) {
  const double inverse_pixel_area = inverse_pixel_widths.x * inverse_pixel_widths.y;
  #pragma omp parallel for
  for (int i = 0; i < n_stars; ++i) {
    if (L_int_stars[i] <= 0.)
      continue;                              // Only add stars with positive luminosity
    // Bin the photon's position - Only add to bins in the specified range
    const Vec3 r_cam = r_star[i] - camera_center; // Position relative to camera center
    const Vec2 pos = {dot(r_cam, xaxis), dot(r_cam, yaxis)}; // Position in camera coordinates
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    const double dz_cam = r_cam.dot(direction); // Camera line-of-sight distance
    if (pix >= 0. && pix < n_pixels && dz_cam > -proj_radius && dz_cam < proj_radius) {
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      proj_image(ix,iy) += inverse_pixel_area * L_int_stars[i]; // Add SB [erg/s/cm^2]
    }
  }
}

/* Driver for projection calculations. */
void MCRT::run_projections() {
  mcrt_proj_timer.start();

  // Allocate pixel corner values and quad trees
  corners = Image(nx_pixels+1, ny_pixels+1); // Pixel corner values
  trees.resize(n_threads);                   // Each thread has its own tree
  #pragma omp parallel
  {
    start_cell = 0;                          // Reset the starting cell
    trees[thread].reserve(4097);             // Reserve reasonable tree sizes
  }

  const int n_cam_rank = n_cameras / n_ranks; // Equal assignment
  const int remainder = n_cameras - n_cam_rank * n_ranks;
  int start_cam = rank * n_cam_rank;         // Camera start range
  int end_cam = (rank + 1) * n_cam_rank;     // Camera end range
  if (rank < remainder) {
    start_cam += rank;                       // Correct start range
    end_cam += (rank + 1);                   // Correct end range
  } else {
    start_cam += remainder;                  // Correct start range
    end_cam += remainder;                    // Correct end range
  }

  // Setup intrinsic emission cameras
  if (output_proj_emission) {
    if (root)
      cout << "\nIntrinsic emission:";
    for (int camera = start_cam; camera < end_cam; ++camera) {
      if (root) {
        cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
        if (n_ranks > 1)
          cout << " (root)";
      }
      direction = camera_directions[camera]; // Camera direction
      xaxis = camera_xaxes[camera];          // Camera x-axis
      yaxis = camera_yaxes[camera];          // Camera y-axis

      // Perform the actual projection calculations
      if (doublet_cell_based_emission)
        calculate_projections<doublet_emission_rule>(proj_images_int[camera]);
      else
        calculate_projections<emission_rule>(proj_images_int[camera]);

      // Add emission from unresolved HII regions
      if (unresolved_HII)
        calculate_projections_HII(proj_images_int[camera]);
    }
  }

  // Setup attenuated emission cameras
  if (output_proj_attenuation) {
    if (root)
      cout << "\nAttenuated emission:";
    for (int camera = start_cam; camera < end_cam; ++camera) {
      if (root) {
        cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
        if (n_ranks > 1)
          cout << " (root)";
      }
      direction = camera_directions[camera]; // Camera direction
      xaxis = camera_xaxes[camera];          // Camera x-axis
      yaxis = camera_yaxes[camera];          // Camera y-axis

      // Perform the actual projection calculations
      if (doublet_cell_based_emission)
        calculate_projections<doublet_attenuation_rule>(proj_images_ext[camera]);
      else
        calculate_projections<attenuation_rule>(proj_images_ext[camera]);
    }
  }

  // Collisional excitation cameras
  if (output_collisions) {
    // Mulitply j_line by the cell collisional excitation fraction
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      j_line[i] *= f_col_cells[i];

    // Setup intrinsic emission cameras
    if (output_proj_emission) {
      if (root)
        cout << "\nCollisional excitation intrinsic emission:";
      for (int camera = start_cam; camera < end_cam; ++camera) {
        if (root) {
          cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
          if (n_ranks > 1)
            cout << " (root)";
        }
        direction = camera_directions[camera]; // Camera direction
        xaxis = camera_xaxes[camera];        // Camera x-axis
        yaxis = camera_yaxes[camera];        // Camera y-axis

        // Perform the actual projection calculations
        if (doublet_cell_based_emission)
          calculate_projections<doublet_emission_rule>(proj_images_int_col[camera]);
        else
          calculate_projections<emission_rule>(proj_images_int_col[camera]);
      }
    }

    // Setup attenuated emission cameras
    if (output_proj_attenuation) {
      if (root)
        cout << "\nAttenuated emission:";
      for (int camera = start_cam; camera < end_cam; ++camera) {
        if (root) {
          cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
          if (n_ranks > 1)
            cout << " (root)";
        }
        direction = camera_directions[camera]; // Camera direction
        xaxis = camera_xaxes[camera];        // Camera x-axis
        yaxis = camera_yaxes[camera];        // Camera y-axis

        // Perform the actual projection calculations
        if (doublet_cell_based_emission)
          calculate_projections<doublet_attenuation_rule>(proj_images_ext_col[camera]);
        else
          calculate_projections<attenuation_rule>(proj_images_ext_col[camera]);
      }
    }
  }

  mcrt_proj_timer.stop();
}
