/*********************
 * mcrt/ray_trace.cc *
 *********************

 * Ray tracer: Follow individual photon packets.

*/

#include "proto.h"
#include "photon.h" // Photon packets
#include "colormaps.h" // RGB colormaps
#if velocity_gradients
  #include <gsl/gsl_cdf.h>

  #if voigt_velocity_gradients
    #include <gsl/gsl_sf_dawson.h>

    static inline double dawson(const double x) {
        return gsl_sf_dawson(x);
    }
  #endif
  #if absorb_lines
    #error "ABSORB_LINES is not compatible with VELOCITY_GRADIENTS"
  #elif sobolev
    #error "SOBOLEV is not compatible with VELOCITY_GRADIENTS"
  #endif

// Define the inverse error function based on the GSL implementation
static inline double erf_inv(const double x) {
  return M_SQRT1_2 * gsl_cdf_ugaussian_Pinv(0.5 * (x + 1.));
}
#endif

#if voigt_velocity_gradients
#define VOIGT_ON(...) __VA_ARGS__
#else
#define VOIGT_ON(...)
#endif

extern double (*H)(const double a, const double x); // Voigt-Hjerting function
int vec2pix_ring(const int nside, const Vec3& vec); // Healpix pixel index

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
Vec3 anisotropic_direction(const int scatter_type, const Vec3 k); // Anisotropic distributions
#if spherical_escape
double spherical_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding sphere
double spherical_emission_distance(const Vec3& point, const Vec3& direction); // Distance to escape the emission sphere
#endif
#if box_escape
double box_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding box
double box_emission_distance(const Vec3& point, const Vec3& direction); // Distance to escape the emission box
#endif
bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
Vec3 random_point_in_cell(const int cell); // Uniform position in cell volume
double escape_distance(const Vec3& dr, const Vec3& direction, const double radius); // Generalized spherical escape
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell
int get_index_sorted(const int val, const vector<int> vec); // Get index of a value in a sorted vector

static const int ISOTROPIC = 1, OUTWARD_SQRT = 5;
static constexpr double _2pi = 2. * M_PI;
static constexpr double _1_3 = 1. / 3.;
#if velocity_gradients
static constexpr double M_SQRTPI_2 = 1. / M_2_SQRTPI;
static const double dx_static = 1e-6, dtau_conv = 1e-9, dl_conv = 1e-9; // Convergence criteria
#endif

/* Assign the group ID based on the source type and index. */
static inline int get_group_id(const int source_type, const int source_id) {
  if (source_type == STARS) {
    return group_id_star[source_id];         // Star group ID
  } else if (source_type == CELLS) {
    return group_id_cell[source_id];         // Cell group ID
  } else {
    return -3;                               // Not assigned
  }
}

/* Assign the subhalo ID based on the source type and index. */
static inline int get_subhalo_id(const int source_type, const int source_id) {
  if (source_type == STARS) {
    return subhalo_id_star[source_id];       // Star subhalo ID
  } else if (source_type == CELLS) {
    return subhalo_id_cell[source_id];       // Cell subhalo ID
  } else {
    return -3;                               // Not assigned
  }
}

/* Assign the group ID index based on the source type and index. */
static inline int get_group_index(const int source_type, const int source_id) {
  const int gid = get_group_id(source_type, source_id); // Group ID
  return get_index_sorted(gid, group_id);    // Group ID index
}

/* Assign the subhalo ID index based on the source type and index. */
static inline int get_subhalo_index(const int source_type, const int source_id) {
  const int sid = get_subhalo_id(source_type, source_id); // Subhalo ID
  return get_index_sorted(sid, subhalo_id);  // Subhalo ID index
}

/* Assign the unfiltered group ID index based on the source type and index. */
static inline int get_ugroup_index(const int source_type, const int source_id) {
  const int gid = get_group_id(source_type, source_id); // Unfiltered group ID
  return get_index_sorted(gid, ugroup_id);   // Unfiltered group ID index
}

/* Assign the unfiltered subhalo ID index based on the source type and index. */
static inline int get_usubhalo_index(const int source_type, const int source_id) {
  const int sid = get_subhalo_id(source_type, source_id); // Unfiltered subhalo ID
  return get_index_sorted(sid, usubhalo_id); // Unfiltered subhalo ID index
}

Photon::Photon() {
  // Draw the source that the photon comes from (cells, stars, point, sphere_cont, shell_cont, shell_line, shock)
  const double mixed_comp = ran();
  if (mixed_comp < mixed_cdf[CELLS]) {       // Cell-based emission (recombinations/collisions/spontaneous)
    const double ran_comp = ran();           // Draw the cell that the photon comes from
    int i = j_map_cells[int(ran_comp * double(n_cdf_cells))]; // Start search from lookup table
    while (j_cdf_cells[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf_cells[i] <= ran_comp && i < n_cells - 1)
      ++i;                                   // Move "outward" ( --> )
    source_id = i;                           // Cell index
    source_type = CELLS;                     // Cell source
    source_weight = mixed_weights[CELLS] * j_weights_cells[i]; // Cell-based luminosity boosting
    current_cell = i;                        // Cell index assigned directly
    position = random_point_in_cell(current_cell); // Uniform distribution within the cell volume
    direction = isotropic_direction();       // Initial direction (isotropic)
    #if spherical_escape                     // Ensure emission is within spherical region
      if (norm2(position - escape_center) > emission_radius2) { // Point is outside emission sphere
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 sphere_direction = position - point; // Outward facing
        sphere_direction.normalize();        // Normalize the direction
        const double dl = spherical_emission_distance(point, sphere_direction);
        position = point + sphere_direction * dl; // Move photon to a valid position on the sphere
      }
    #endif
    #if box_escape
      if (position.x < emission_box[0].x || position.x > emission_box[1].x ||
          position.y < emission_box[0].y || position.y > emission_box[1].y ||
          position.z < emission_box[0].z || position.z > emission_box[1].z) {
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 box_direction = position - point; // Outward facing
        box_direction.normalize();           // Normalize the direction
        const double dl = box_emission_distance(point, box_direction);
        position = point + box_direction * dl; // Move photon to a valid position on the box
      }
    #endif
    if (output_cells) {
      #pragma omp atomic
      source_weight_cells[i] += source_weight; // Add photon weight at emission to the cell
    }
  } else if (mixed_comp < mixed_cdf[STARS]) { // Star-based emission (continuum/unresolved_HII)
    const double ran_comp = ran();           // Draw the star that the photon comes from
    int i = j_map_stars[int(ran_comp * double(n_cdf_stars))]; // Start search from lookup table
    while (j_cdf_stars[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf_stars[i] <= ran_comp && i < n_stars - 1)
      ++i;                                   // Move "outward" ( --> )
    source_id = i;                           // Star index
    source_type = STARS;                     // Star source
    source_weight = mixed_weights[STARS] * j_weights_stars[i]; // Star-based luminosity boosting
    current_cell = find_cell(r_star[i], 0);  // Cell index hosting the star (cell = 0)
    position = r_star[i];                    // Set the photon position to the star position
    direction = isotropic_direction();       // Initial direction (isotropic)
    if (output_stars) {
      #pragma omp atomic
      source_weight_stars[i] += source_weight; // Add photon weight at emission to the star
    }
  } else if (mixed_comp < mixed_cdf[DOUBLET_CELLS]) { // Doublet cell-based emission (collisions)
    const double ran_comp = ran();           // Draw the cell that the photon comes from
    int i = jp_map_cells[int(ran_comp * double(n_cdf_cells))]; // Start search from lookup table
    while (jp_cdf_cells[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (jp_cdf_cells[i] <= ran_comp && i < n_cells - 1)
      ++i;                                   // Move "outward" ( --> )
    source_id = i;                           // Cell index
    source_type = DOUBLET_CELLS;             // Cell source
    source_weight = mixed_weights[DOUBLET_CELLS] * jp_weights_cells[i]; // Cell-based luminosity boosting
    current_cell = i;                        // Cell index assigned directly
    position = random_point_in_cell(current_cell); // Uniform distribution within the cell volume
    direction = isotropic_direction();       // Initial direction (isotropic)
    l_tot = 0.;                              // Total path length [cm]
    #if spherical_escape                     // Ensure emission is within spherical region
      if (norm2(position - escape_center) > emission_radius2) { // Point is outside emission sphere
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 sphere_direction = position - point; // Outward facing
        sphere_direction.normalize();        // Normalize the direction
        const double dl = spherical_emission_distance(point, sphere_direction);
        position = point + sphere_direction * dl; // Move photon to a valid position on the sphere
      }
    #endif
    #if box_escape
      if (position.x < emission_box[0].x || position.x > emission_box[1].x ||
          position.y < emission_box[0].y || position.y > emission_box[1].y ||
          position.z < emission_box[0].z || position.z > emission_box[1].z) {
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 box_direction = position - point; // Outward facing
        box_direction.normalize();           // Normalize the direction
        const double dl = box_emission_distance(point, box_direction);
        position = point + box_direction * dl; // Move photon to a valid position on the sphere
      }
    #endif
    if (output_cells) {
      #pragma omp atomic
      source_weight_doublet_cells[i] += source_weight; // Add doublet photon weight at emission to the cell
    }
  } else if (mixed_comp < mixed_cdf[POINT]) { // Point source emission
    source_id = i_point;                     // Cell index (only one point id)
    source_type = POINT;                     // Point source
    source_weight = mixed_weights[POINT] / double(n_photons); // Equal weight photons
    current_cell = i_point;                  // Known cell index
    position = r_point;                      // Known position
    direction = isotropic_direction();       // Initial direction (isotropic)
  } else if (mixed_comp < mixed_cdf[PLANE_CONT]) { // Plane continuum source emission
    double x1, x2;
    do {
      x1 = 2. * ran() - 1.;                  // Random draws in [-1,1]
      x2 = 2. * ran() - 1.;
    } while (plane_beam && x1*x1 + x2*x2 >= 1.);
    const double p1 = plane_center_1 + x1 * plane_radius_1; // Positions
    const double p2 = plane_center_2 + x2 * plane_radius_2;
    switch (plane_type) {                    // Assign plane properties
      case PosX:                             // +x direction
        position = {plane_position, p1, p2};
        direction = {1., 0., 0.};
        break;
      case NegX:                             // -x direction
        position = {plane_position, p1, p2};
        direction = {-1., 0., 0.};
        break;
      case PosY:                             // +y direction
        position = {p1, plane_position, p2};
        direction = {0., 1., 0.};
        break;
      case NegY:                             // -y direction
        position = {p1, plane_position, p2};
        direction = {0., -1., 0.};
        break;
      case PosZ:                             // +z direction
        position = {p1, p2, plane_position};
        direction = {0., 0., 1.};
        break;
      case NegZ:                             // -z direction
        position = {p1, p2, plane_position};
        direction = {0., 0., -1.};
        break;
      default:
        error("Unrecognized plane direction type.");
    }
    current_cell = find_cell(position, 0);   // Cell index hosting the photon (cell = 0)
    source_id = current_cell;                // Track the source cell index
    source_type = PLANE_CONT;                // Plane continuum source
    source_weight = mixed_weights[PLANE_CONT] / double(n_photons); // Equal weight photons
  } else if (mixed_comp < mixed_cdf[SPHERE_CONT]) { // Sphere continuum source emission
    const double r0 = sphere_radius_cont * pow(ran(), _1_3); // Drawn radius
    position = sphere_center_cont + isotropic_direction() * r0; // Random position
    current_cell = find_cell(position, i_sphere_cont); // Find cell index
    source_id = current_cell;                // Cell index
    source_type = SPHERE_CONT;                    // Sphere source
    source_weight = mixed_weights[SPHERE_CONT] / double(n_photons); // Equal weight photons
    direction = isotropic_direction();       // Initial direction (isotropic)
  } else if (mixed_comp < mixed_cdf[SHELL_CONT]) { // Shell continuum source emission
    position = isotropic_direction() * r_shell_cont; // Known radius
    current_cell = find_cell(position, 0);   // Cell index hosting the sampled photon
    source_id = current_cell;                // Cell index
    source_type = SHELL_CONT;                // Shell continuum source
    source_weight = mixed_weights[SHELL_CONT] / double(n_photons); // Equal weight photons
    direction = anisotropic_direction(OUTWARD_SQRT, position / r_shell_cont);
  } else if (mixed_comp < mixed_cdf[SHELL_LINE]) { // Shell line source emission
    position = isotropic_direction() * r_shell_line; // Known radius
    current_cell = find_cell(position, 0);   // Cell index hosting the sampled photon
    source_id = current_cell;                // Cell index
    source_type = SHELL_LINE;                // Shell line source
    source_weight = mixed_weights[SHELL_LINE] / double(n_photons); // Equal weight photons
    direction = anisotropic_direction(OUTWARD_SQRT, position / r_shell_line);
  } else if (mixed_comp < mixed_cdf[SHOCK]) { // Shock source emission
    position = isotropic_direction() * r_shock; // Known radius
    current_cell = find_cell(position, 0);   // Cell index hosting the sampled photon
    source_id = current_cell;                // Cell index
    source_type = SHOCK;                     // Shock source
    source_weight = mixed_weights[SHOCK] / double(n_photons); // Equal weight photons
    direction = isotropic_direction();       // Initial direction (isotropic)
  } else
    error("Unexpected source probability behavior");

  if (avoid_cell(current_cell))
    error("Attempting to insert photon in an invalid cell");

#if use_tau
  tau_eff_abs = -log(source_weight);         // Effective abs. optical depth
  weight = 0.;                               // Placeholder until escape
#else
  weight = source_weight;                    // Set the initial photon weight
#endif
  if constexpr (output_f_src_r) {
    max_cell = current_cell;                 // Initialize the maximum cell index
    #pragma omp atomic
    f_src_r[max_cell] += source_weight;      // Cumulative source fraction
  }
#if output_tau_dust_f_esc
  tau_dust_weights.resize(n_tau_dust_sims);  // Dust optical depth weights
  for (int i = 0; i < n_tau_dust_sims; ++i)
    tau_dust_weights[i] = weight;            // Initialize each dust simulation to be same weigh
  k_dust_sims.resize(n_tau_dust_sims);       // Dust absorption coefficients
  const double dk_dust = tau_dust_max / (r[n_cells].x * double(n_tau_dust_sims - 1));
  for (int i = 1; i < n_tau_dust_sims; ++i)
    k_dust_sims[i] = dk_dust * double(i);    // Initialize each dust absoprtion coefficient
#endif
  n_scat = 0;                                // Number of scattering events
  source_position = position;                // Remember position at emission
  if constexpr (SPHERICAL)
    radius = position.norm();                // Current radius
  if (output_collisions)                     // Collisional excitation fraction
    f_col = (source_type == CELLS) ? f_col_cells[current_cell] : ((source_type == DOUBLET_CELLS) ? 1. : 0.);

  // Note: calculate_LOS requires an initialized photon so should be done last
  if ((source_type == STARS && continuum) || source_type == SPHERE_CONT || source_type == SHOCK) {
    frequency = Dv_cont_res * ran() + Dv_cont_min; // Note: Dv_res = Dv_max - Dv_min
    frequency = x_div_aDv * a[current_cell] * frequency; // Convert: Dv -> x

    // LOS Flux calculation for optically thin cases
    calculate_LOS(ISOTROPIC);                // Frequency doesn't matter here
    if (output_mcrt_emission)
      calculate_LOS_int();                   // Intrinsic emission cameras
    if (output_mcrt_attenuation)
      calculate_LOS_ext();                   // Attenuated emission cameras
  } else if (source_type == PLANE_CONT) {
    frequency = Dv_cont_res * ran() + Dv_cont_min; // Note: Dv_res = Dv_max - Dv_min
    frequency = x_div_aDv * a[current_cell] * frequency; // Convert: Dv -> x

    // LOS Flux calculation for optically thin cases
    for (int camera = 0; camera < n_cameras; ++camera) {
      if (direction.dot(camera_directions[camera]) > 0.999) {
        calculate_LOS(ISOTROPIC, camera);
        if (output_mcrt_emission)
          calculate_LOS_int(camera);         // Intrinsic emission cameras
        if (output_mcrt_attenuation)
          calculate_LOS_ext(camera);         // Attenuated emission cameras
      }
    }
  } else if (source_type == SHELL_CONT) {
    frequency = Dv_cont_res * ran() + Dv_cont_min; // Note: Dv_res = Dv_max - Dv_min
    frequency = x_div_aDv * a[current_cell] * frequency; // Convert: Dv -> x

    // LOS Flux calculation for optically thin cases
    calculate_LOS(OUTWARD_SQRT);                 // Frequency doesn't matter here
    if (output_mcrt_emission || output_mcrt_attenuation) {
      const double inv_norm = 1. / position.norm();
      for (int camera = 0; camera < n_cameras; ++camera) {
        const double phase_mu = position.dot(camera_directions[camera]) * inv_norm;
        const double phase_weight = (phase_mu > 0.) ? 2. * phase_mu : 0.; // Radially outward phase function
        if (output_mcrt_emission)
          calculate_LOS_int(camera, phase_weight); // Intrinsic emission cameras
        if (output_mcrt_attenuation)
          calculate_LOS_ext(camera, phase_weight); // Attenuated emission cameras
      }
    }
  } else if (source_type == SHELL_LINE) {
    // Initialize velocity to draw from Maxwellian where u = v_atom / v_th
    Vec3 u_atom {
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran())
    };

    // Don't assume emission from line center, i.e. don't set x = 0
    const double x_natural = a[current_cell] / tan(M_PI * ran());

    // LOS Flux calculation for optically thin cases
    if (output_mcrt_emission || output_mcrt_attenuation) {
      const double inv_norm = 1. / position.norm();
      for (int camera = 0; camera < n_cameras; ++camera) {
        frequency = u_atom.dot(camera_directions[camera]) - x_natural;
        calculate_LOS(OUTWARD_SQRT, camera);
        const double phase_mu = position.dot(camera_directions[camera]) * inv_norm;
        const double phase_weight = (phase_mu > 0.) ? 2. * phase_mu : 0.; // Radially outward phase function
        if (output_mcrt_emission)
          calculate_LOS_int(camera, phase_weight); // Intrinsic emission cameras
        if (output_mcrt_attenuation)
          calculate_LOS_ext(camera, phase_weight); // Attenuated emission cameras
      }
    }

    // Draw the frequency from the natural Lorentzian and then doppler shift
    frequency = u_atom.dot(direction) - x_natural;
  } else if (source_type == DOUBLET_CELLS) { // emission == {collisions}
    // Initialize velocity to draw from Maxwellian where u = v_atom / v_th
    Vec3 u_atom {
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran())
    };

    // Don't assume emission from line center, i.e. don't set x = 0
    const double xp_natural = apA * a[current_cell] / tan(M_PI * ran());

    // LOS Flux calculation for optically thin cases
    for (int camera = 0; camera < n_cameras; ++camera) {
      frequency = u_atom.dot(camera_directions[camera]) - xp_natural;
      // Transform back to primary line frequency
      frequency = (frequency - xpB * a[current_cell]) / xpA;
      calculate_LOS(ISOTROPIC, camera);
      if (output_mcrt_emission)
        calculate_LOS_int(camera);           // Intrinsic emission cameras
      if (output_mcrt_attenuation)
        calculate_LOS_ext(camera);           // Attenuated emission cameras
    }

    // Draw the frequency from the natural Lorentzian and then doppler shift
    frequency = u_atom.dot(direction) - xp_natural;
    // Transform back to primary line frequency
    frequency = (frequency - xpB * a[current_cell]) / xpA;
  } else { // emission == {recombinations, collisions, spontaneous, unresolved_HII, point}
    // Initialize velocity to draw from Maxwellian where u = v_atom / v_th
    Vec3 u_atom {
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran()),
      sqrt(-log(ran())) * cos(_2pi * ran())
    };

    // Don't assume emission from line center, i.e. don't set x = 0
    const double x_natural = a[current_cell] / tan(M_PI * ran());

    // LOS Flux calculation for optically thin cases
    for (int camera = 0; camera < n_cameras; ++camera) {
      frequency = u_atom.dot(camera_directions[camera]) - x_natural;
      calculate_LOS(ISOTROPIC, camera);
      if (output_mcrt_emission)
        calculate_LOS_int(camera);           // Intrinsic emission cameras
      if (output_mcrt_attenuation)
        calculate_LOS_ext(camera);           // Attenuated emission cameras
    }

    // Draw the frequency from the natural Lorentzian and then doppler shift
    frequency = u_atom.dot(direction) - x_natural;
  }
  #if output_radial_flow
    // Calculate radial bin from radial position [cm]
    const double rf_radius = (position - radial_center).norm();
    rf_bin_max = rf_bin_src = rf_bin = radial_line_flow.find_radial_bin(rf_radius);
    rf_in_range = (rf_bin >= 0);
    if (rf_in_range) {
      #pragma omp atomic
      radial_line_flow.src[rf_bin_src] += source_weight;
    }
  #endif

  // Group statistics histogram bins
  #if output_groups || output_group_flows
    igrp = get_group_index(source_type, source_id); // Group ID index
    valid_igrp = (igrp >= 0 && igrp < n_groups); // Valid index
  #endif
  #if output_group_flows
    if (valid_igrp) {
      auto& group_line_flow = group_line_flows[igrp];
      vector<Vec3>& r_grp_flows = focus_groups_on_emission ? r_light_grp : (output_grp_gal ? r_grp_gal : r_grp_vir); // Group flow positions [cm]
      // Calculate radial bin from radial position [cm]
      const double gf_radius = (position - r_grp_flows[igrp]).norm();
      gf_bin_max = gf_bin_src = gf_bin = group_line_flow.find_radial_bin(gf_radius);
      gf_in_range = (gf_bin >= 0);
      if (gf_in_range) {
        #pragma omp atomic
        group_line_flow.src[gf_bin_src] += source_weight;
      }
    } else {
      gf_in_range = false;                   // Group ID is invalid
    }
  #endif

  // Subhalo statistics histogram bins
  #if output_subhalos || output_subhalo_flows
    isub = get_subhalo_index(source_type, source_id); // Subhalo ID index
    valid_isub = (isub >= 0 && isub < n_subhalos); // Valid index
  #endif
  #if output_subhalo_flows
    if (valid_isub) {
      auto& subhalo_line_flow = subhalo_line_flows[isub];
      vector<Vec3>& r_sub_flows = focus_subhalos_on_emission ? r_light_sub : (output_sub_gal ? r_sub_gal : r_sub_vir); // Subhalo flow positions [cm]
      // Calculate radial bin from radial position [cm]
      const double sf_radius = (position - r_sub_flows[isub]).norm();
      sf_bin_max = sf_bin_src = sf_bin = subhalo_line_flow.find_radial_bin(sf_radius);
      sf_in_range = (sf_bin >= 0);
      if (sf_in_range) {
        #pragma omp atomic
        subhalo_line_flow.src[sf_bin_src] += source_weight;
      }
    } else {
      sf_in_range = false;
    }
  #endif

  // Group statistics
  #if output_groups
    if (valid_igrp) {
      #pragma omp atomic
      f_src_grp[igrp] += source_weight;      // Source fraction
      #pragma omp atomic
      f2_src_grp[igrp] += source_weight * source_weight;   // Squared f_src

      // Check whether the photon is inside the group virial radius
      if (output_grp_vir && norm2(position - r_grp_vir[igrp]) < R2_grp_vir[igrp]) {
        in_grp_vir = true;                   // Flag for virial statistics
        #pragma omp atomic
        f_src_grp_vir[igrp] += source_weight; // Source fraction
        #pragma omp atomic
        f2_src_grp_vir[igrp] += source_weight * source_weight; // Squared f_src
      }
      // Check whether the photon is inside the group galaxy radius
      if (output_grp_gal && norm2(position - r_grp_gal[igrp]) < R2_grp_gal[igrp]) {
        in_grp_gal = true;                   // Flag for galaxy statistics
        #pragma omp atomic
        f_src_grp_gal[igrp] += source_weight; // Source fraction
        #pragma omp atomic
        f2_src_grp_gal[igrp] += source_weight * source_weight; // Squared f_src
      }
    } else {
      const int uigrp = get_ugroup_index(source_type, source_id); // Unfiltered group ID index
      if (uigrp < 0 || uigrp >= n_ugroups) {
        print(); error("Invalid unfiltered group ID: " + to_string(uigrp));
      }
      #pragma omp atomic
      f_src_ugrp[uigrp] += source_weight;    // Source fraction
      #pragma omp atomic
      f2_src_ugrp[uigrp] += source_weight * source_weight; // Squared f_src
    }
  #endif

  // Subhalo statistics
  #if output_subhalos
    if (valid_isub) {
      #pragma omp atomic
      f_src_sub[isub] += source_weight;      // Source fraction
      #pragma omp atomic
      f2_src_sub[isub] += source_weight * source_weight;   // Squared f_src

      // Check whether the photon is inside the subhalo virial radius
      if (output_sub_vir && norm2(position - r_sub_vir[isub]) < R2_sub_vir[isub]) {
        in_sub_vir = true;                   // Flag for virial statistics
        #pragma omp atomic
        f_src_sub_vir[isub] += source_weight; // Source fraction
        #pragma omp atomic
        f2_src_sub_vir[isub] += source_weight * source_weight; // Squared f_src
      }
      // Check whether the photon is inside the subhalo galaxy radius
      if (output_sub_gal && norm2(position - r_sub_gal[isub]) < R2_sub_gal[isub]) {
        in_sub_gal = true;                   // Flag for galaxy statistics
        #pragma omp atomic
        f_src_sub_gal[isub] += source_weight; // Source fraction
        #pragma omp atomic
        f2_src_sub_gal[isub] += source_weight * source_weight; // Squared f_src
      }
    } else {
      const int uisub = get_usubhalo_index(source_type, source_id); // Unfiltered subhalo ID index
      if (uisub < 0 || uisub >= n_usubhalos) {
        print(); error("Invalid unfiltered subhalo ID: " + to_string(uisub));
      }
      #pragma omp atomic
      f_src_usub[uisub] += source_weight;    // Source fraction
      #pragma omp atomic
      f2_src_usub[uisub] += source_weight * source_weight; // Squared f_src
    }
  #endif
}

#if sobolev
/* Calculate the dl_scat assuming the Sobolev approximation. */
void Photon::update_dl_scat() {
  // Check for an absence of line scattering
  if (k_line_scat <= 0.) {                   // No resonant point in the cell
    dl_scat = dtau_scat / k_grey_scat;       // Actual length traveled (grey)
    return;
  }

  // Optical depth to resonant point within the cell
  const double dtau_pre = k_grey_scat * dl_line;

  // Check for grey scattering before the resonant point
  if (dtau_scat <= dtau_pre) {
    k_scat = k_grey_scat;                    // Grey scattering only
    dl_scat = dtau_scat / k_grey_scat;       // Actual length traveled (pre)
    return;
  }

  // Subtract the optical depth to the resonant point
  dtau_scat -= dtau_pre;

  // Check for a line scattering event
  if (dtau_scat <= tau_sobolev) {
    k_dust_scat = k_grey_scat = 0.;          // No dust or grey scattering
    dl_scat = dl_line;                       // Actual length traveled (line)
    return;
  }

  // Subtract the Sobolev optical depth
  dtau_scat -= tau_sobolev;                  // Subtract traversed optical depth

  // At this point there is grey scattering after the resonant point
  k_scat = k_grey_scat;                      // Grey scattering only
  dl_scat = dl_line + dtau_scat / k_grey_scat; // Actual length traveled (post)
}
#elif velocity_gradients
/* Calculate the distance to the next scattering event with veloctiy gradients using a root finder. */
void Photon::update_dl_scat() {
  if (fabs(dx) < dx_static) {
    dl_scat = dtau_scat / k_scat;            // Static distance
    return;
  }
  // Initial guess based on Gaussian line profile or grey scattering only
  const double k0_scat = (1. - p_dest) * k_0[current_cell]; // Scattering coefficient (x=0)
  const double dtau_ref = safe_division(M_2_SQRTPI * dvdr_LOS * dtau_scat, k_0[current_cell]);
  const double a0 = a[current_cell];         // Damping parameter
  const double erf_x = erf(frequency);       // Cache function evaluations
  const double dx_arg = dtau_ref - erf_x;    // Use error function as the approx inverse
  if (1. - dx_arg < 1e-10 || 1. + dx_arg < 1e-10) {
    dx = 0.;                                 // Revert to the static approximation
    const double dl_next_static = dtau_scat / k_scat; // Static distance
    if (dl_next_static < dl_next)
      dl_next = dl_next_static;              // Consistent with neighbor distance
    k_line_scat = k0_scat * H(a0, frequency);
    k_scat = k_line_scat + k_grey_scat;
    dtau_next = k_scat * dl_next;            // Update optical depth
    dl_scat = dl_next;                       // Actual length traveled
    k_line_abs = (p_dest > 0.) ? k_line_scat * p_dest / (1. - p_dest) : 0.;
    k_dust_abs = k_dust[current_cell] - k_dust_scat + k_line_abs; // k_line_abs dependency
    return;
  }

  // Initialize the root finder based on a partial inversion
  double dl_L = 0., dl_R = dl_next, dtau_L = 0., dtau_R = dtau_next; // Search bounds
  double dl_line = dl_next, dtau_line = k_line_scat * dl_next; // Initial guess
  if (dtau_line > dtau_scat) {               // Guaranteed line scattering
    dx = frequency + erf_inv(dx_arg);        // Estimate frequency shift (line only)
    dl_line = dx / dvdr_LOS;                 // Update distance
    dtau_line = dtau_scat;                   // Update optical depth
    const double dl_max = 0.99 * dl_next;    // Maximum distance
    if (dl_line > dl_max) {
      dl_line = dl_max;                      // Limit the Gaussian only guess
      dx = dvdr_LOS * dl_line;               // Update frequency shift
    }
  }
  // Combine the line and grey scattering initial guesses
  dl_next = dtau_scat * dl_line / (dtau_line + k_grey_scat * dl_line);
  // Evaluate the initial guess and update the bounds
  const double exp_x2 = exp(-frequency * frequency); // exp(-x^2)
  VOIGT_ON(const double dawson_x = dawson(frequency));
  VOIGT_ON(const double freq_exp_x2 = frequency * exp_x2;) // x * exp(-x^2)
  double freq_dx = frequency - dx;           // Final frequency
  k_line_scat = k0_scat * ((fabs(dx) < dx_static) ? H(a0, frequency) :
                          ((erf_x - erf(freq_dx)) * M_SQRTPI_2
               VOIGT_ON(+ ((dawson(freq_dx) - dawson_x) * M_2_SQRTPI
                        + (freq_exp_x2 - freq_dx * exp(-freq_dx * freq_dx)) * a0) * a0) ) / dx);
  k_scat = k_line_scat + k_grey_scat;        // Update scattering coefficient
  dtau_next = k_scat * dl_next;              // Update optical depth
  double fx0 = dtau_next - dtau_scat;        // Root finding function
  if (dtau_next < dtau_scat) {
    dl_L = dl_next; dtau_L = dtau_next;      // Update lower bounds
  } else {
    dl_R = dl_next; dtau_R = dtau_next;      // Update upper bounds
  }

  // Decrease the search range until underestimating the optical depth
  while (dtau_next > dtau_scat) {
    // Bisection lower 10% update (dl,dtau)
    dl_next = 0.9 * dl_L + 0.1 * dl_R;       // Guess lower 10%
    dx = dvdr_LOS * dl_next;                 // Update frequency shift
    freq_dx = frequency - dx;                // Final frequency
    k_line_scat = k0_scat * ((fabs(dx) < dx_static) ? H(a0, frequency) :
                            ((erf_x - erf(freq_dx)) * M_SQRTPI_2
                 VOIGT_ON(+ ((dawson(freq_dx) - dawson_x) * M_2_SQRTPI
                          + (freq_exp_x2 - freq_dx * exp(-freq_dx * freq_dx)) * a0) * a0) ) / dx);
    k_scat = k_line_scat + k_grey_scat;      // Update scattering coefficient
    dtau_next = k_scat * dl_next;            // Update optical depth
    fx0 = dtau_next - dtau_scat;             // Root finding function
    if (dtau_next < dtau_scat) {
      dl_L = dl_next; dtau_L = dtau_next;    // Update lower bounds
    } else {
      dl_R = dl_next; dtau_R = dtau_next;    // Update upper bounds
    }
  }

  // Combination of Bisection, Halley's, and Secant methods
  int count = 0;
  while (true) {                             // Root finder refines the search range
    // Bisection midpoint update (dl,dtau)
    dl_next = 0.5 * (dl_L + dl_R);           // Guess midpoint
    dx = dvdr_LOS * dl_next;                 // Update frequency shift
    freq_dx = frequency - dx;                // Final frequency
    const double exp_dx2 = exp(-freq_dx * freq_dx); // exp(-dx^2)
    VOIGT_ON(const double freq_exp_dx2 = freq_dx * exp_dx2;) // dx * exp(-dx^2)
    VOIGT_ON(const double dawson_dx = dawson(freq_dx)); // Final Dawson term
    k_line_scat = k0_scat * ((fabs(dx) < dx_static) ? H(a0, frequency) :
                            ((erf_x - erf(freq_dx)) * M_SQRTPI_2
                 VOIGT_ON(+ ((dawson_dx - dawson_x) * M_2_SQRTPI
                          + (freq_exp_x2 - freq_exp_dx2) * a0) * a0) ) / dx);
    k_scat = k_line_scat + k_grey_scat;      // Update scattering coefficient
    dtau_next = k_scat * dl_next;            // Update optical depth
    fx0 = dtau_next - dtau_scat;             // Root finding function
    if (fabs(fx0) < dtau_conv)
      break;                                 // Early termination (dtau)
    if (dtau_next < dtau_scat) {
      dl_L = dl_next; dtau_L = dtau_next;    // Update lower bounds
    } else {
      dl_R = dl_next; dtau_R = dtau_next;    // Update upper bounds
    }
    if (dl_R - dl_L < dl_conv * (dl_L + dl_R))
      break;                                 // Early termination (dl)

    // Halley's update (dl,dtau)
    const double fx1 = k_0[current_cell] * H(a0, freq_dx) + k_grey_scat; // First derivative
#if voigt_velocity_gradients                 // Simplify fx2_2fx1 without the Voigt term
    const double fx2 = 2. * dvdr_LOS * k_0[current_cell] * (freq_exp_dx2 * (1. + a0 * a0 * (3. - 2. * freq_dx * freq_dx))
                     - M_2_SQRTPI * a0 * (dawson_dx * (1. + 2. * freq_dx * freq_dx) - freq_dx)); // Second derivative
    dl_next -= fx0 * fx1 / (fx1 * fx1 - 0.5 * fx0 * fx2); // Adjust distance
#else
    const double fx0_fx1 = fx0 / fx1;        // Ratio of terms
    const double fx2_2fx1 = dvdr_LOS * freq_dx * k_0[current_cell] / (k_0[current_cell] + exp_dx2 * k_grey_scat);
    dl_next -= fx0_fx1 / (1. - fx0_fx1 * fx2_2fx1); // Adjust distance
#endif
    dx = dvdr_LOS * dl_next;                 // Update frequency shift
    freq_dx = frequency - dx;                // Final frequency
    k_line_scat = k0_scat * ((fabs(dx) < dx_static) ? H(a0, frequency) :
                            ((erf_x - erf(freq_dx)) * M_SQRTPI_2
                 VOIGT_ON(+ ((dawson(freq_dx) - dawson_x) * M_2_SQRTPI
                          + (freq_exp_x2 - freq_dx * exp(-freq_dx * freq_dx)) * a0) * a0) ) / dx);
    k_scat = k_line_scat + k_grey_scat;      // Update scattering coefficient
    dtau_next = k_scat * dl_next;            // Update optical depth
    fx0 = dtau_next - dtau_scat;             // Root finding function
    if (fabs(fx0) < dtau_conv)
      break;                                 // Early termination (dtau)
    if (dtau_next < dtau_scat) {
      if (dtau_next > dtau_L) {
        dl_L = dl_next; dtau_L = dtau_next;  // Update lower bounds
        if (dl_R - dl_L < dl_conv * (dl_L + dl_R))
          break;                             // Early termination
      }
    } else {
      if (dtau_next < dtau_R) {
        dl_R = dl_next; dtau_R = dtau_next;  // Update upper bounds
        if (dl_R - dl_L < dl_conv * (dl_L + dl_R))
          break;                             // Early termination
      }
    }

    if (count++ > 100) {
      cout << "\ndl_L = " << dl_L << ", dl_R = " << dl_R
           << "\ndl_R - dl_L = " << dl_R - dl_L << ", dl_L + dl_R = " << dl_L + dl_R
           << "\ndtau_scat = " << dtau_scat << ", dtau_L = " << dtau_L << ", dtau_R = " << dtau_R
           << "\ndtau_R - dtau_L = " << dtau_R - dtau_L << ", dtau_L + dtau_R = " << dtau_L + dtau_R << endl;
      error("Exceeded 100 iterations in root finder!");
    }
  }
  k_line_abs = (p_dest > 0.) ? k_line_scat * p_dest / (1. - p_dest) : 0.;
  k_dust_abs = k_dust[current_cell] - k_dust_scat + k_line_abs; // k_line_abs dependency
  dl_scat = dl_next;                         // Actual length traveled
}
#else
/* Calculate the distance to the next scattering event. */
void Photon::update_dl_scat() {
  dl_scat = dtau_scat / k_scat;              // Actual length traveled
}
#endif

/* Print photon data. */
void Photon::print() {
  cout << "\nPhoton {"
       << "\n  source_id       = " << source_id
       << "\n  source_type     = " << source_type << " (" << source_names_mask[source_type] << ")"
       << "\n  source_weight   = " << source_weight
       << "\n  f_col           = " << f_col
       << "\n  current_cell    = " << current_cell
       << "\n  n_scat          = " << n_scat
       << "\n  source_position = " << source_position / kpc << " kpc"
       << "\n  position        = " << position / kpc << " kpc"
       << "\n  l_tot           = " << l_tot / kpc << " kpc";
  if constexpr (SPHERICAL)
    cout << "\n  radius          = " << radius / kpc << " kpc";
  cout << "\n  direction       = " << direction
       << "\n  frequency       = " << frequency
#if use_tau
       << "\n  tau_eff_abs     = " << tau_eff_abs
       << "\n  weight          = " << exp(-tau_eff_abs)
#else
       << "\n  weight          = " << weight
#endif
       << "\n}" << endl;
  if (source_type == STARS) {
    cout << "\nEmitting star data:"
         << "\n  position        = " << r_star[source_id] / kpc << " kpc";
    if (read_v_star)
      cout << "\n  velocity        = " << v_star[source_id] / km << " km/s";
    if (read_continuum)
      cout << "\n  L_cont          = " << L_cont_star[source_id] << " erg/s/angstrom";
    if (read_m_massive_star)
      cout << "\n  m_star(OB)      = " << m_massive_star[source_id] << " Msun";
    if (read_m_init_star)
      cout << "\n  m_star(0)       = " << m_init_star[source_id] << " Msun";
    if (read_Z_star)
      cout << "\n  Z_star          = " << Z_star[source_id];
    if (read_age_star)
      cout << "\n  age_star        = " << age_star[source_id] << " Gyr";
    if (!group_id_star.empty())
      cout << "\n  group_id        = " << get_group_id(source_type, source_id);
    if (!subhalo_id_star.empty())
      cout << "\n  subhalo_id      = " << get_subhalo_id(source_type, source_id);
    cout << endl;
  }
  #if output_groups || output_group_flows
    cout << "\nEmitting group " << get_group_id(source_type, source_id) << " data:";
    if (valid_igrp) {
      cout << "\n  index            = " << igrp
           << "\n  n_groups         = " << n_groups
           << "\n  L_grp            = " << L_grp[igrp] << " erg/s";
      if (output_grp_vir)
        cout << "\n   in_grp_vir      = " << (in_grp_vir ? "true" : "false")
             << "\n    L_grp_vir      = " << L_grp_vir[igrp] << " erg/s"
             << "\n    r_grp_vir      = " << r_grp_vir[igrp] / kpc << " kpc"
             << "\n    R_grp_vir      = " << R_grp_vir[igrp] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_grp_vir[igrp]) / kpc << " kpc";
      if (output_grp_gal)
        cout << "\n   in_grp_gal      = " << (in_grp_gal ? "true" : "false")
             << "\n    L_grp_gal      = " << L_grp_gal[igrp] << " erg/s"
             << "\n    r_grp_gal      = " << r_grp_gal[igrp] / kpc << " kpc"
             << "\n    R_grp_gal      = " << R_grp_gal[igrp] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_grp_gal[igrp]) / kpc << " kpc";
    } else {
      cout << "\n  index            = " << get_ugroup_index(source_type, source_id)
           << "\n  n_ugroups        = " << n_ugroups
           << "\n  L_ugrp           = " << L_ugrp[igrp] << " erg/s";
      if (output_grp_vir)
        cout << "\n   in_grp_vir      = " << (in_grp_vir ? "true" : "false");
      if (output_grp_gal)
        cout << "\n   in_grp_gal      = " << (in_grp_gal ? "true" : "false");
    }
    cout << endl;
  #endif
  #if output_subhalos || output_subhalo_flows
    cout << "\nEmitting subhalo " << get_subhalo_id(source_type, source_id) << " data:";
    if (valid_isub) {
      cout << "\n  index            = " << isub
           << "\n  n_subhalos       = " << n_subhalos
           << "\n  L_sub            = " << L_sub[isub] << " erg/s";
      if (output_sub_vir)
        cout << "\n   in_sub_vir      = " << (in_sub_vir ? "true" : "false")
             << "\n    L_sub_vir      = " << L_sub_vir[isub] << " erg/s"
             << "\n    r_sub_vir      = " << r_sub_vir[isub] / kpc << " kpc"
             << "\n    R_sub_vir      = " << R_sub_vir[isub] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_sub_vir[isub]) / kpc << " kpc";
      if (output_sub_gal)
        cout << "\n   in_sub_gal      = " << (in_sub_gal ? "true" : "false")
             << "\n    L_sub_gal      = " << L_sub_gal[isub] << " erg/s"
             << "\n    r_sub_gal      = " << r_sub_gal[isub] / kpc << " kpc"
             << "\n    R_sub_gal      = " << R_sub_gal[isub] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_sub_gal[isub]) / kpc << " kpc";
    } else {
      cout << "\n  index            = " << get_usubhalo_index(source_type, source_id)
           << "\n  n_usubhalos      = " << n_usubhalos
           << "\n  L_usub           = " << L_usub[isub] << " erg/s";
      if (output_sub_vir)
        cout << "\n   in_sub_vir      = " << (in_sub_vir ? "true" : "false");
      if (output_sub_gal)
        cout << "\n   in_sub_gal      = " << (in_sub_gal ? "true" : "false");
    }
    cout << endl;
  #endif
}

//! Spherical geometry modifies the frequency during propagation.
#if SPHERICAL
  #if cell_velocity_gradients
    #define dfreq(distance) v0[current_cell] * (mu_old - mu) - dvdr[current_cell] * distance
  #else // No velocity gradients but still spherical geometry effects
    #define dfreq(distance) v[current_cell].x * (mu_old - mu)
  #endif
#endif

/* Calculate the directional cosine based on the current radius. */
void Photon::update_mu() {
  mu_old = mu;                               // Copy directional cosine
  mu = (radius > 0.) ? direction.dot(position) / radius : 0.; // Update directional cosine
}

/* Updates the position and weight of the photon */
void Photon::move_photon(const double distance) {
#if !use_tau
  const double dtau_abs = k_dust_abs * distance; // Absorption optical depth
  const double exp_dtau_abs = exp(-dtau_abs);
  #if output_tau_dust_f_esc
    for (int i = 1; i < n_tau_dust_sims; ++i) {
      double dtau_abs_i = k_dust_sims[i] * distance;
      double exp_dtau_abs_i = exp(-dtau_abs_i);
      tau_dust_weights[i] *= exp_dtau_abs_i; // Update the dust optical depth weights
    }
  #endif
  const double rate_weight = weight * ((dtau_abs < 1e-5) ? // Numerical stability
    (1. - 0.5 * dtau_abs) * distance :       // Series approx (dtau_abs << 1)
    (1. - exp_dtau_abs) / k_dust_abs);       // Radiative transfer solution [cm]
  // Number of absorptions by radial and frequency bin [photons/s]
  #if output_radial_flow
    if (rf_in_range)
      #pragma omp atomic
      radial_line_flow.abs[rf_bin] += k_dust_abs * rate_weight;
  #endif
  // Group number of absorptions by radial and frequency bin [photons/s]
  #if output_group_flows
    if (gf_in_range)
      #pragma omp atomic
      group_line_flows[igrp].abs[gf_bin] += k_dust_abs * rate_weight;
  #endif
  // Subhalo number of absorptions by radial and frequency bin [photons/s]
  #if output_subhalo_flows
    if (sf_in_range)
      #pragma omp atomic
      subhalo_line_flows[isub].abs[sf_bin] += k_dust_abs * rate_weight;
  #endif
  #if output_acceleration
  const double tau_weight = weight * (k_scat + k_dust_abs) * rate_weight; // Total optical depth weight
  #endif
#endif

  // Update the position, total distance, spherical quantities, and weight
  position += direction * distance;          // Update photon position
  l_tot += distance;                         // Update total distance
#if SPHERICAL
  #if output_pressure || output_P_u_r
    const double sin_theta = sqrt(1. - mu * mu); // sin(theta)
    const double rmin = sin_theta * radius;  // Impact parameter
    const double dmu2_w = (rmin > 0.) ? (distance + rmin * (atan2(mu, sin_theta) - atan2(distance + mu * radius, rmin))) : distance;
    const double dmu2 = weight * dmu2_w;     // Directional weight squared * distance
  #endif
  radius_old = radius;                       // Copy the old radius
  radius = position.norm();                  // Update current radius
  update_mu();                               // Update directional cosines
  frequency += dfreq(distance);              // Apply Doppler shift
  #if output_acceleration || output_M_F_r || output_M_F_src
    const double dradius = radius - radius_old; // Change in radius
    const double dtau_r = weight * (k_scat + k_dust_abs) * dradius;
  #endif
#endif
#if use_tau
  tau_eff_abs += k_dust_abs * distance;      // Cumulative absorption optical depth
#else
  weight *= exp_dtau_abs;                    // Reduce weight by exp(-dtau_abs)
#endif

#if output_energy_density
  #pragma omp atomic
  u_rad[current_cell] += rate_weight;        // Radiation energy density [erg/cm^3]
#endif
#if SPHERICAL
  #if output_acceleration
    #pragma omp atomic
    a_rad_r[current_cell] += dtau_r;        // Radial radiation acceleration [cm/s^2]
  #endif
  #if output_pressure
    #pragma omp atomic
    P_rad_r[current_cell] += dmu2;           // Radial radiation pressure [erg/cm^3]
  #endif
  #if output_trap_r
    #pragma omp atomic
    trap_r[max_cell] += rate_weight;         // Cumulative u_rad (later ->  t_trap / t_light)
  #endif
  #if output_M_F_r
    #pragma omp atomic
    M_F_r[max_cell] += dtau_r;              // Cumulative a_rad_r (later -> M_F_r)
  #endif
  #if output_M_F_src
    #pragma omp atomic
    M_F_src[source_id] += dtau_r;           // Cumulative a_rad_r (later -> M_F_src)
  #endif
  #if output_P_u_r
    #pragma omp atomic
    P_u_r[max_cell] += dmu2;                 // Cumulative P_r (later -> P/u)
  #endif
#else // Cartesian-like geometry
  #if output_acceleration
    #pragma omp atomic
    a_rad[current_cell] += tau_weight * direction; // Radiation acceleration [cm/s^2]
  #endif
#endif
}

/* Ray tracer (MCRT) */
void Photon::ray_trace() {
  // Initialize the grid traversal variables
  #if have_multiple_grids
    int event;                               // Event flag
  #endif
  dtau_scat = -log(ran());                   // Optical depth to next scatter
  #if check_escape
    double dl_exit = positive_infinity;      // Path length to exit cell [cm]
  #endif
  #if spherical_escape
    inplace_min(dl_exit, spherical_escape_distance(position, direction));
  #endif
  #if box_escape
    inplace_min(dl_exit, box_escape_distance(position, direction));
  #endif
  #if streaming_escape
    inplace_min(dl_exit, max_streaming);
  #endif
  #if age_escape
    const bool age_escape_active = (source_type == STARS); // Age escape flag
    double dl_age = (age_escape_active) ? age_star[source_id] * cGyr : positive_infinity; // Initialize age distance
  #endif
  double v_r, beta_r;                        // Spherical geometry and aberration
  if constexpr (SPHERICAL)
    update_mu();                             // Initialize directional cosine

  // Compute the maximum distance the photon can travel in the cell
  #if output_radial_flow                     // Initialize radial flow
    int neib_rf;                             // Radial flow neighbor index
    double dl_rf = 0.;                       // Radial flow distance
    tie(dl_rf, neib_rf) = radial_line_flow.face_distance(position - radial_center, direction, rf_bin);
  #endif
  #if output_groups || output_group_flows    // Initialize group flow
    vector<Vec3>& r_grp_flows = focus_groups_on_emission ? r_light_grp : (output_grp_gal ? r_grp_gal : r_grp_vir); // Group flow positions [cm]
  #endif
  #if output_subhalos || output_subhalo_flows // Initialize subhalo flow
    vector<Vec3>& r_sub_flows = focus_subhalos_on_emission ? r_light_sub : (output_sub_gal ? r_sub_gal : r_sub_vir); // Subhalo flow positions [cm]
  #endif
  #if output_group_flows                     // Group flow requires a valid group ID index
    int neib_gf;                             // Group flow neighbor index
    double dl_gf = 0.;                       // Group flow distance
    if (valid_igrp) tie(dl_gf, neib_gf) = group_line_flows[igrp].face_distance(position - r_grp_flows[igrp], direction, gf_bin);
  #endif
  #if output_subhalo_flows                   // Subhalo flow requires a valid subhalo ID index
    int neib_sf;                             // Subhalo flow neighbor index
    double dl_sf = 0.;                       // Subhalo flow distance
    if (valid_isub) tie(dl_sf, neib_sf) = subhalo_line_flows[isub].face_distance(position - r_sub_flows[isub], direction, sf_bin);
  #endif
  #if output_groups                          // Initialize group escape distance (virial, galaxy radius)
    double dl_grp_vir = 0., dl_grp_gal = 0.; // Escape distances
    if (in_grp_vir) dl_grp_vir = escape_distance(position - r_grp_vir[igrp], direction, R2_grp_vir[igrp]);
    if (in_grp_gal) dl_grp_gal = escape_distance(position - r_grp_gal[igrp], direction, R2_grp_gal[igrp]);
  #endif
  #if output_subhalos                        // Initialize subhalo escape distance (virial, galaxy radius)
    double dl_sub_vir = 0., dl_sub_gal = 0.; // Escape distances
    if (in_sub_vir) dl_sub_vir = escape_distance(position - r_sub_vir[isub], direction, R2_sub_vir[isub]);
    if (in_sub_gal) dl_sub_gal = escape_distance(position - r_sub_gal[isub], direction, R2_sub_gal[isub]);
  #endif
  tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell); // Exit if the photon leaves the grid

  while (true) {
    // Determine the next event
    dl_next = dl_neib;                       // Reset comparison distance
    #if have_multiple_grids
      event = GRID;                          // Flag for grid traversal
    #endif
    #if check_escape
      if (dl_exit <= dl_next) {              // Exit region before cell
        dl_next = dl_exit;                   // Set neighbor distance to exit
        neib_cell = OUTSIDE;                 // Flag for escape condition
      }
    #endif
    #if age_escape
      if (dl_age <= dl_next) {               // Exit due to stellar age
        dl_next = dl_age;                    // Set neighbor distance to age limit
        neib_cell = OUTSIDE;                 // Flag for escape condition
      }
    #endif
    #if output_radial_flow
      if (dl_rf <= dl_next) {
        dl_next = dl_rf;                     // Update to rf distance
        event = RADIAL_FLOW;                 // Flag for rf traversal
      }
    #endif
    #if output_group_flows
      if (valid_igrp && dl_gf <= dl_next) {
        dl_next = dl_gf;                     // Update to gf distance
        event = GROUP_FLOW;                  // Flag for gf traversal
      }
    #endif
    #if output_subhalo_flows
      if (valid_isub && dl_sf <= dl_next) {
        dl_next = dl_sf;                     // Update to sf distance
        event = SUBHALO_FLOW;                // Flag for sf traversal
      }
    #endif
    #if output_groups
      if (in_grp_vir && dl_grp_vir <= dl_next) {
        dl_next = dl_grp_vir;                // Update to group escape distance
        event = GROUP_VIR_ESCAPE;            // Flag for group escape
      }
      if (in_grp_gal && dl_grp_gal <= dl_next) {
        dl_next = dl_grp_gal;                // Update to group escape distance
        event = GROUP_GAL_ESCAPE;            // Flag for group escape
      }
    #endif
    #if output_subhalos
      if (in_sub_vir && dl_sub_vir <= dl_next) {
        dl_next = dl_sub_vir;                // Update to subhalo escape distance
        event = SUBHALO_VIR_ESCAPE;          // Flag for subhalo escape
      }
      if (in_sub_gal && dl_sub_gal <= dl_next) {
        dl_next = dl_sub_gal;                // Update to subhalo escape distance
        event = SUBHALO_GAL_ESCAPE;          // Flag for subhalo escape
      }
    #endif

    // Check whether the photon is scattered
#if cell_velocity_gradients
    // Might change this if we can find a way to pass in previous l_scat here...see spherical geometry velocity gradients paper
    dvdr_LOS = (radius > 0.) ? dvdr[current_cell] + (1. - mu*mu) * v0[current_cell] / radius : dvdr[current_cell];
#endif
#if sobolev
    tau_sobolev = 0., k_line_scat = 0., dl_line = -1.; // Assume no line interaction
    if (dl_next > 0. && dvdr_LOS != 0.) {
      const double inv_dvdr = 1. / dvdr_LOS; // Inverse velocity gradient
      dl_line = frequency * inv_dvdr;        // Distance to resonant point
      if (dl_line > 0. && dl_line < dl_next) { // Resonant point is in the cell
        tau_sobolev = k_0[current_cell] * inv_dvdr; // Sobolev optical depth
        k_line_scat = tau_sobolev / dl_next; // Sobolev line coefficient (effective)
      }
    }
    k_line_abs = 0., kp_line_scat = 0.;      // Not implemented
#elif velocity_gradients
    dx = dvdr_LOS * dl_next;                 // Use the LOS velocity gradient
    const double a0 = a[current_cell];       // a in current cell
    const double freq_dx = frequency - dx;   // Final frequency
    double k_line = k_0[current_cell] * ((fabs(dx) < dx_static) ? H(a0, frequency) :
                    ((erf(frequency) - erf(freq_dx)) * M_SQRTPI_2
         VOIGT_ON(+ ((dawson(freq_dx) - dawson(frequency)) * M_2_SQRTPI
                  + (frequency * exp(-frequency * frequency) - freq_dx * exp(-freq_dx * freq_dx)) * a0) * a0) ) / dx);
    k_line_abs = p_dest * k_line;            // Line destruction channels
    k_line_scat = k_line - k_line_abs;       // Remainder scatters
    kp_line_scat = 0.;                       // Not implemented
#else // Normal case (no velocity gradients or sobolev)
    const double k_line = k_0[current_cell] * H(a[current_cell], frequency);
    k_line_abs = p_dest * k_line;            // Line destruction channels
    k_line_scat = k_line - k_line_abs;       // Remainder scatters
    // Check for either deuterium or doublet line presence and reuse logic (they are incompatible)
    // Frequency translation: x' = A x + B a  Damping paramter factor: a' = A a
    if constexpr (deuterium_fraction > 0)    // xDA out front to correct the cross section, and the A in a' is the A in x'
      kp_line_scat = xDA * deuterium_fraction * k_0[current_cell] * H(xDA * a[current_cell], xDA * frequency + xDB * a[current_cell]);
    else
      kp_line_scat = (E0p > 0.) ? kp_0[current_cell] * H(apA*a[current_cell], xpA*frequency+xpB*a[current_cell]) : 0.;
#endif
    k_dust_scat = albedo * k_dust[current_cell];
    k_grey_scat = k_dust_scat;
    if constexpr (electron_scattering)
      k_grey_scat += k_e[current_cell];
    k_dust_abs = k_dust[current_cell] - k_dust_scat + k_line_abs;
    k_scat = k_grey_scat;                    // Normally lines undergo scattering
    if constexpr (absorb_lines)
      k_dust_abs += k_line_scat + kp_line_scat; // Line opacity treated as absorption
    else
      k_scat += k_line_scat + kp_line_scat;  // Line opacity treated as scattering
    dtau_next = k_scat * dl_next;            // Optical depth to next event

    if (dtau_scat <= dtau_next) {            // A scattering event takes place
      // Calculate the new position from the remaining optical depth
      update_dl_scat();                      // Calculate the distance to scatter
      move_photon(dl_scat);                  // Update position and weight
      n_scat++;                              // Update number of scattering events
#if use_tau
      if (tau_eff_abs > tau_discard) {       // Consider the photon packet absorbed
#else
      if (weight < weight_discard) {         // Consider the photon packet absorbed
#endif
        weight = 0.;                         // Flag the photon as absorbed
        break;                               // Stop ray-tracing procedure
      }
#if !SPHERICAL && output_acceleration_scat
      const Vec3 direction_old = direction;  // Save the old direction
#endif

      // Scatter: Update direction, frequency, cameras, etc.
      const double k_ran = ran() * k_scat;   // Save drawn interaction
      if (k_ran <= k_dust_scat)
        dust_scatter();                      // Interaction is with dust
#if electron_scattering
      else if (k_ran <= k_grey_scat)
        electron_scatter();                  // Interaction is with electrons
#endif
#if coherent_line_scattering
      else
        coherent_line_scatter();             // Coherent line scattering
#elif complete_redistribution
      else
        crd_line_scatter();                  // Complete redistribution
#else // Normal case (partial redistribution)
      else if (k_ran <= k_grey_scat + kp_line_scat)
        resonant_scatter(true);              // Interaction is with line doublet or Deuterium
      else
        resonant_scatter(false);             // Interaction is with line carrier
#endif

      // Update directional cosine after scatter
#if SPHERICAL
      update_mu();
#if output_acceleration_scat || output_M_F_r_scat || output_M_F_src_scat
      const double weight_dmu = weight * (mu_old - mu); // Change in directional cosine
#endif
#if output_acceleration_scat
      #pragma omp atomic
      a_rad_r_scat[current_cell] += weight_dmu; // Scattering-based acceleration [cm/s^2]
#endif
#if output_M_F_r_scat
      #pragma omp atomic
      M_F_r_scat[max_cell] += weight_dmu;    // Cumulative a_rad_r (later -> M_F_r)
#endif
#if output_M_F_src_scat
      #pragma omp atomic
      M_F_src_scat[source_id] += weight_dmu; // Cumulative a_rad_r (later -> M_F_src)
#endif
#else // Cartesian-like geometry
#if output_acceleration_scat
      #pragma omp atomic
      a_rad_scat[current_cell] += weight * (direction_old - direction); // Scattering-based acceleration [cm/s^2]
#endif
#endif

      // Reset the escape distance (new direction)
      tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);
      #if check_escape
        dl_exit = positive_infinity;
      #endif
      #if spherical_escape
        inplace_min(dl_exit, spherical_escape_distance(position, direction));
      #endif
      #if box_escape
        inplace_min(dl_exit, box_escape_distance(position, direction));
      #endif
      #if streaming_escape
        inplace_min(dl_exit, max_streaming);
      #endif
      #if output_radial_flow
        tie(dl_rf, neib_rf) = radial_line_flow.face_distance(position - radial_center, direction, rf_bin);
      #endif
      #if output_group_flows
        if (valid_igrp) tie(dl_gf, neib_gf) = group_line_flows[igrp].face_distance(position - r_grp_flows[igrp], direction, gf_bin);
      #endif
      #if output_subhalo_flows
        if (valid_isub) tie(dl_sf, neib_sf) = subhalo_line_flows[isub].face_distance(position - r_sub_flows[isub], direction, sf_bin);
      #endif
      #if output_groups
        if (in_grp_vir) dl_grp_vir = escape_distance(position - r_grp_vir[igrp], direction, R2_grp_vir[igrp]);
        if (in_grp_gal) dl_grp_gal = escape_distance(position - r_grp_gal[igrp], direction, R2_grp_gal[igrp]);
      #endif
      #if output_subhalos
        if (in_sub_vir) dl_sub_vir = escape_distance(position - r_sub_vir[isub], direction, R2_sub_vir[isub]);
        if (in_sub_gal) dl_sub_gal = escape_distance(position - r_sub_gal[isub], direction, R2_sub_gal[isub]);
      #endif

      // Generate a new random optical depth
      dtau_scat = -log(ran());
      continue;
    }

    // No scattering event
    dtau_scat -= dtau_next;                  // Subtract traversed optical depth
    if constexpr (have_multiple_grids)
      dl_neib -= dl_next;                    // Subtract traversed distance (grid)
    #if check_escape
      dl_exit -= dl_next;                    // Subtract traversed distance
    #endif
    #if age_escape
      if (age_escape_active) dl_age -= dl_next; // Subtract traversed distance
    #endif
    #if output_radial_flow
      dl_rf -= dl_next;                      // Subtract traversed distance (rf)
    #endif
    #if output_group_flows
      dl_gf -= dl_next;                      // Subtract traversed distance (gf)
    #endif
    #if output_subhalo_flows
      dl_sf -= dl_next;                      // Subtract traversed distance (sf)
    #endif
    #if output_groups
      if (in_grp_vir) dl_grp_vir -= dl_next; // Subtract traversed distance (group virial)
      if (in_grp_gal) dl_grp_gal -= dl_next; // Subtract traversed distance (group galaxy)
    #endif
    #if output_subhalos
      if (in_sub_vir) dl_sub_vir -= dl_next; // Subtract traversed distance (subhalo virial)
      if (in_sub_gal) dl_sub_gal -= dl_next; // Subtract traversed distance (subhalo galaxy)
    #endif

    // Calculate the new position of the photon
    move_photon(dl_next);                    // Update position and weight
#if use_tau
      if (tau_eff_abs > tau_discard) {       // Consider the photon packet absorbed
#else
      if (weight < weight_discard) {         // Consider the photon packet absorbed
#endif
        weight = 0.;                         // Flag the photon as absorbed
        break;                               // Stop ray-tracing procedure
      }

    #if output_radial_flow
      if (event == RADIAL_FLOW) { // Check for rf traversal
        if (!rf_in_range) {                  // Photon re-enters the grid
          rf_in_range = true;                // Flag as in-range
          #pragma omp atomic                 // Inward flow from outside
          radial_line_flow.flow(rf_bin_src, neib_rf) -= weight;
        } else if (neib_rf < 0) {            // Photon escapes the grid
          rf_in_range = false;               // Flag as out-of-range
          #pragma omp atomic                 // Outward flow to outside
          radial_line_flow.flow(rf_bin_src, rf_bin) += weight;
          if (rf_bin == rf_bin_max) {        // Check for first passage
            #pragma omp atomic
            radial_line_flow.pass(rf_bin_src, rf_bin) += weight; // First passage
            rf_bin_max++;                    // Update the maximum radial bin
          }
        } else if (neib_rf > rf_bin) {       // Photon moves outward in the grid
          #pragma omp atomic
          radial_line_flow.flow(rf_bin_src, rf_bin) += weight;
          if (rf_bin == rf_bin_max) {        // Check for first passage
            #pragma omp atomic
            radial_line_flow.pass(rf_bin_src, rf_bin) += weight; // First passage
            rf_bin_max++;                    // Update the maximum radial bin
          }
        } else {                             // Photon moves inward in the grid
          #pragma omp atomic
          radial_line_flow.flow(rf_bin_src, neib_rf) -= weight;
        }
        rf_bin = neib_rf;                    // Update radial bin index
        tie(dl_rf, neib_rf) = radial_line_flow.face_distance(position - radial_center, direction, rf_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_group_flows
      if (event == GROUP_FLOW) {             // Check for gf traversal
        auto& group_line_flow = group_line_flows[igrp];  // Group flow object
        if (!gf_in_range) {                  // Photon re-enters the grid
          gf_in_range = true;                // Flag as in-range
          #pragma omp atomic                 // Inward flow from outside
          group_line_flow.flow(gf_bin_src, neib_gf) -= weight;
        } else if (neib_gf < 0) {            // Photon escapes the grid
          gf_in_range = false;               // Flag as out-of-range
          #pragma omp atomic                 // Outward flow to outside
          group_line_flow.flow(gf_bin_src, gf_bin) += weight;
          if (gf_bin == gf_bin_max) {        // Check for first passage
            #pragma omp atomic
            group_line_flow.pass(gf_bin_src, gf_bin) += weight; // First passage
            gf_bin_max++;                    // Update the maximum radial bin
          }
        } else if (neib_gf > gf_bin) {       // Photon moves outward in the grid
          #pragma omp atomic
          group_line_flow.flow(gf_bin_src, gf_bin) += weight;
          if (gf_bin == gf_bin_max) {        // Check for first passage
            #pragma omp atomic
            group_line_flow.pass(gf_bin_src, gf_bin) += weight; // First passage
            gf_bin_max++;                    // Update the maximum radial bin
          }
        } else {                             // Photon moves inward in the grid
          #pragma omp atomic
          group_line_flow.flow(gf_bin_src, neib_gf) -= weight;
        }
        gf_bin = neib_gf;                    // Update radial bin index
        tie(dl_gf, neib_gf) = group_line_flow.face_distance(position - r_grp_flows[igrp], direction, gf_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_subhalo_flows
      if (event == SUBHALO_FLOW) {           // Check for sf traversal
        auto& subhalo_line_flow = subhalo_line_flows[isub]; // Subhalo flow object
        if (!sf_in_range) {                  // Photon re-enters the grid
          sf_in_range = true;                // Flag as in-range
          #pragma omp atomic                 // Inward flow from outside
          subhalo_line_flow.flow(sf_bin_src, neib_sf) -= weight;
        } else if (neib_sf < 0) {            // Photon escapes the grid
          sf_in_range = false;               // Flag as out-of-range
          #pragma omp atomic                 // Outward flow to outside
          subhalo_line_flow.flow(sf_bin_src, sf_bin) += weight;
          if (sf_bin == sf_bin_max) {        // Check for first passage
            #pragma omp atomic
            subhalo_line_flow.pass(sf_bin_src, sf_bin) += weight; // First passage
            sf_bin_max++;                    // Update the maximum radial bin
          }
        } else if (neib_sf > sf_bin) {       // Photon moves outward in the grid
          #pragma omp atomic
          subhalo_line_flow.flow(sf_bin_src, sf_bin) += weight;
          if (sf_bin == sf_bin_max) {        // Check for first passage
            #pragma omp atomic
            subhalo_line_flow.pass(sf_bin_src, sf_bin) += weight; // First passage
            sf_bin_max++;                    // Update the maximum radial bin
          }
        } else {                             // Photon moves inward in the grid
          #pragma omp atomic
          subhalo_line_flow.flow(sf_bin_src, neib_sf) -= weight;
        }
        sf_bin = neib_sf;                    // Update radial bin index
        tie(dl_sf, neib_sf) = subhalo_line_flow.face_distance(position - r_sub_flows[isub], direction, sf_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_groups
      if (event == GROUP_VIR_ESCAPE) {       // Check for group escape (virial radius)
        TAU_ON(weight = exp(-tau_eff_abs);)  // Optical depth -> weight
        if (weight > 0.) {
          #pragma omp atomic
          f_esc_grp_vir[igrp] += weight;     // Escape fraction
          #pragma omp atomic
          f2_esc_grp_vir[igrp] += weight * weight; // Squared f_esc
          if (output_map_grp) {
            const int imap = vec2pix_ring(n_side_grp, direction); // Healpix index
            #pragma omp atomic
            map_grp_vir(igrp, imap) += weight;
            if (output_map2_grp)
              #pragma omp atomic
              map2_grp_vir(igrp, imap) += weight * weight; // Statistical convergence
          }
        }
        in_grp_vir = false;                  // Finished group escape
        continue;                            // Continue ray-tracing procedure
      }
      if (event == GROUP_GAL_ESCAPE) {       // Check for group escape (galaxy radius)
        TAU_ON(weight = exp(-tau_eff_abs);)  // Optical depth -> weight
        if (weight > 0.) {
          #pragma omp atomic
          f_esc_grp_gal[igrp] += weight;     // Escape fraction
          #pragma omp atomic
          f2_esc_grp_gal[igrp] += weight * weight; // Squared f_esc
          if (output_map_grp) {
            const int imap = vec2pix_ring(n_side_grp, direction); // Healpix index
            #pragma omp atomic
            map_grp_gal(igrp, imap) += weight;
            if (output_map2_grp)
              #pragma omp atomic
              map2_grp_gal(igrp, imap) += weight * weight; // Statistical convergence
          }
        }
        in_grp_gal = false;                  // Finished group escape
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_subhalos
      if (event == SUBHALO_VIR_ESCAPE) {     // Check for subhalo escape (virial radius)
        TAU_ON(weight = exp(-tau_eff_abs);)  // Optical depth -> weight
        if (weight > 0.) {
          #pragma omp atomic
          f_esc_sub_vir[isub] += weight;     // Escape fraction
          #pragma omp atomic
          f2_esc_sub_vir[isub] += weight * weight; // Squared f_esc
          if (output_map_sub) {
            const int imap = vec2pix_ring(n_side_sub, direction); // Healpix index
            #pragma omp atomic
            map_sub_vir(isub, imap) += weight;
            if (output_map2_sub)
              #pragma omp atomic
              map2_sub_vir(isub, imap) += weight * weight; // Statistical convergence
          }
        }
        in_sub_vir = false;                  // Finished subhalo escape
        continue;                            // Continue ray-tracing procedure
      }
      if (event == SUBHALO_GAL_ESCAPE) {     // Check for subhalo escape (galaxy radius)
        TAU_ON(weight = exp(-tau_eff_abs);)  // Optical depth -> weight
        if (weight > 0.) {
          #pragma omp atomic
          f_esc_sub_gal[isub] += weight;     // Escape fraction
          #pragma omp atomic
          f2_esc_sub_gal[isub] += weight * weight; // Squared f_esc
          if (output_map_sub) {
            const int imap = vec2pix_ring(n_side_sub, direction); // Healpix index
            #pragma omp atomic
            map_sub_gal(isub, imap) += weight;
            if (output_map2_sub)
              #pragma omp atomic
              map2_sub_gal(isub, imap) += weight * weight; // Statistical convergence
          }
        }
        in_sub_gal = false;                  // Finished subhalo escape
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    // Doppler-shift from current cell comoving frame to grid frame
    if constexpr (SPHERICAL) {
      if (neib_cell == INSIDE)               // Check if the photon is trapped
        break;                               // Stop ray-tracing procedure
      if constexpr (cell_velocity_gradients)
        v_r = v0[current_cell] + dvdr[current_cell] * radius;
      else
        v_r = v[current_cell].x;             // Radial velocity
      frequency += mu * v_r;                 // Use the spherical cosine
      if constexpr (special_relativity) if (radius > 0.) {
        beta_r = avthDc * v_r / (a[current_cell] * radius);
        direction += position * beta_r;      // Aberration: beta = v/c = avthDc u/a
        direction.normalize();               // Maintain vector normalization
        update_mu();                         // Update directional cosine
      }
    } else // Cartesian-like geometry
      frequency += direction.dot(v[current_cell]);

    if (neib_cell == OUTSIDE) {              // Check if the photon escapes
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      frequency *= a_exit / a[current_cell];
      // Doppler-shift from grid frame to exit comoving frame
      if constexpr (!SPHERICAL)
        frequency -= direction.dot(camera_motion);
      current_cell = OUTSIDE;                // Update the cell index
      TAU_ON(weight = exp(-tau_eff_abs);)    // Optical depth -> weight
      break;
    } else {                                 // Continue ray tracing in next cell
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      frequency *= a[neib_cell] / a[current_cell];
      // Doppler-shift from grid frame to next cell comoving frame
      if constexpr (SPHERICAL) {
        if constexpr (cell_velocity_gradients)
          v_r = v0[neib_cell] + dvdr[neib_cell] * radius;
        else
          v_r = v[neib_cell].x;              // Radial velocity
        if constexpr (special_relativity) if (radius > 0.) {
          beta_r = avthDc * v_r / (a[neib_cell] * radius);
          direction -= position * beta_r;    // Aberration: beta = v/c = avthDc u/a
          direction.normalize();             // Maintain vector normalization
          update_mu();                       // Update directional cosine
        }
        frequency -= mu * v_r;               // Use the spherical cosine
      } else // Cartesian-like geometry
        frequency -= direction.dot(v[neib_cell]);
      current_cell = neib_cell;              // Update the cell index
      tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);
      if constexpr (output_f_src_r) {
        if (current_cell > max_cell) {       // Update the maximum cell index
          max_cell = current_cell;
          if constexpr (output_f_esc_r)
            #pragma omp atomic
            f_esc_r[max_cell-1] += weight;   // Cumulative escape fraction
          #if output_rescaled_f_esc_r
            double rescaled_weight = source_weight * exp(-k_dust_abs * l_tot * r[n_cells].x / r[current_cell].x); // Rescaled escape fraction
            #pragma omp atomic
            rescaled_f_esc_r[max_cell-1] += rescaled_weight; // Rescaled escape fraction
          #endif
        }
      }
    }
  }

  // Convert to frequency output type
  if (!doppler_frequency)
    frequency *= neg_vth_kms_exit;           // Convert: x -> Delta_v

  // Calculate global statistics
  if (weight > 0.) {
    const double weighted_freq = weight * frequency;
    const double weighted_freq2 = weighted_freq * frequency;
    const double weighted_freq3 = weighted_freq2 * frequency;
    const double weighted_freq4 = weighted_freq3 * frequency;
    #pragma omp atomic
    f_src += source_weight;                  // Global source weight
    #pragma omp atomic
    f2_src += source_weight * source_weight; // Global squared source weight
    #pragma omp atomic
    f_esc += weight;                         // Global escape fraction
    #pragma omp atomic
    f2_esc += weight * weight;               // Global squared f_esc
    #pragma omp atomic
    freq_avg += weighted_freq;               // Global average frequency
    #pragma omp atomic
    freq_std += weighted_freq2;              // Global standard deviation
    #pragma omp atomic
    freq_skew += weighted_freq3;             // Global frequency skewness
    #pragma omp atomic
    freq_kurt += weighted_freq4;             // Global frequency kurtosis

    #if output_tau_dust_f_esc
      tau_dust_f_esc[0] = f_esc;
      for (int i = 1; i < n_tau_dust_sims; ++i)
        tau_dust_f_esc[i] += tau_dust_weights[i]; // Dust optical depth escape fraction
    #endif

    // Angle-averaged flux
    if (output_flux_avg) {
      const double freq_bin = inverse_freq_bin_width * (frequency - freq_min);
      if (freq_bin >= 0. && freq_bin < double(n_bins)) {
        const int ibin = floor(freq_bin);
        #pragma omp atomic
        flux_avg[ibin] += weight;            // Angle-averaged flux
      }
    }

    // Line-of-sight healpix maps
    if (output_map) {
      const int imap = vec2pix_ring(n_side_map, direction); // Healpix index
      // if (imap < 0 || imap >= map.size())
      //   error("Debugging: Out of range map index!");
      #pragma omp atomic
      map[imap] += weight;
      if (output_map2)
        #pragma omp atomic
        map2[imap] += weight * weight;       // Statistical convergence
      if (output_freq_map) {
        #pragma omp atomic
        freq_map[imap] += weighted_freq;     // Average frequency map
        if (output_freq2_map) {
          #pragma omp atomic
          freq2_map[imap] += weighted_freq2; // Frequency^2 map
          if (output_freq3_map) {
            #pragma omp atomic
            freq3_map[imap] += weighted_freq3; // Frequency^3 map
            if (output_freq4_map)
              #pragma omp atomic
              freq4_map[imap] += weighted_freq4;   // Frequency^4 map
          }
        }
      }
      if (output_rgb_map) {
        const Vec3 weighted_color = get_rgb_color(frequency) * weight;
        Vec3& rgb_pixel = rgb_map[imap];
        #pragma omp atomic
        rgb_pixel.x += weighted_color.x;
        #pragma omp atomic
        rgb_pixel.y += weighted_color.y;
        #pragma omp atomic
        rgb_pixel.z += weighted_color.z;
      }
    }

    // Line-of-sight healpix flux map
    if (output_flux_map) {
      const double freq_bin = inverse_map_freq_bin_width * (frequency - map_freq_min);
      if (freq_bin >= 0. && freq_bin < double(n_map_bins)) {
        const int imap = vec2pix_ring(n_side_flux, direction); // Healpix index
        const int ibin = floor(freq_bin);
        #pragma omp atomic
        flux_map(imap, ibin) += weight;
      }
    }

    // Perpendicular distance
    if (output_radial_map || output_cube_map || output_radial_avg || output_radial_cube_avg) {
      const Vec3 r_cam = position - camera_center; // Position relative to camera center
      const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance

      // Angle-averaged radial surface brightness
      if (output_radial_avg) {
        const double r_pix = inverse_radial_pixel_width * r_perp;
        if (r_pix < double(n_radial_pixels)) {
          const int ir = floor(r_pix);
          #pragma omp atomic
          radial_avg[ir] += weight;
        }
      }

      // Angle-averaged radial spectral data cube
      if (output_radial_cube_avg) {
        const double r_pix = inverse_radial_cube_pixel_width * r_perp;
        const double freq_bin = inverse_radial_cube_freq_bin_width * (frequency - radial_cube_freq_min);
        if (r_pix < double(n_radial_cube_pixels) && freq_bin >= 0. && freq_bin < double(n_radial_cube_bins)) {
          const int ir = floor(r_pix);
          const int ibin = floor(freq_bin);
          #pragma omp atomic
          radial_cube_avg(ir, ibin) += weight;
        }
      }

      // Line-of-sight radial map
      if (output_radial_map) {
        const double r_pix = inverse_map_pixel_width * r_perp;
        if (r_pix < double(n_map_pixels)) {
          const int imap = vec2pix_ring(n_side_radial, direction); // Healpix index
          const int ir = floor(r_pix);
          #pragma omp atomic
          radial_map(imap, ir) += weight;
        }
      }

      // Line-of-sight radial cube map
      if (output_cube_map) {
        const double r_pix = inverse_cube_map_pixel_width * r_perp;
        const double freq_bin = inverse_cube_map_freq_bin_width * (frequency - cube_map_freq_min);
        if (r_pix < double(n_cube_map_pixels) && freq_bin >= 0. && freq_bin < double(n_cube_map_bins)) {
          const int imap = vec2pix_ring(n_side_cube, direction); // Healpix index
          const int ir = floor(r_pix);
          const int ibin = floor(freq_bin);
          #pragma omp atomic
          cube_map(imap, ir, ibin) += weight;
        }
      }
    }

    // Group statistics
    #if output_groups
      if (output_grp_obs && valid_igrp) {
        const double freq_grp = frequency + direction.dot(v_grp[igrp]); // Frequency in group frame
        #pragma omp atomic
        f_esc_grp[igrp] += weight;           // Escape fraction
        #pragma omp atomic
        f2_esc_grp[igrp] += weight * weight; // Squared f_esc

        // Line-of-sight healpix maps for groups
        if (output_map_grp) {
          const int imap = vec2pix_ring(n_side_grp, direction); // Healpix index
          #pragma omp atomic
          map_grp(igrp, imap) += weight;
          if (output_map2_grp) {
            #pragma omp atomic
            map2_grp(igrp, imap) += weight * weight; // Statistical convergence
            if (output_freq_map_grp) {
              const double weighted_freq_grp = weight * freq_grp; // Weighted frequency
              #pragma omp atomic
              freq_map_grp(igrp, imap) += weighted_freq_grp; // Average frequency map
              if (output_freq2_map_grp) {
                const double weighted_freq2_grp = weighted_freq_grp * freq_grp; // Weighted frequency^2
                #pragma omp atomic
                freq2_map_grp(igrp, imap) += weighted_freq2_grp; // Frequency^2 map
                if (output_freq3_map_grp) {
                  const double weighted_freq3_grp = weighted_freq2_grp * freq_grp; // Weighted frequency^3
                  #pragma omp atomic
                  freq3_map_grp(igrp, imap) += weighted_freq3_grp; // Frequency^3 map
                  if (output_freq4_map_grp)
                    #pragma omp atomic
                    freq4_map_grp(igrp, imap) += weighted_freq3_grp * freq_grp; // Frequency^4 map
                }
              }
            }
          }
        }

        // Line-of-sight healpix flux group maps
        if (output_flux_map_grp) {
          const double freq_bin = inverse_map_freq_bin_width_grp * (freq_grp - map_freq_min_grp);
          if (freq_bin >= 0. && freq_bin < double(n_map_bins_grp)) {
            const int imap = vec2pix_ring(n_side_flux_grp, direction); // Healpix index
            const int ibin = floor(freq_bin);
            #pragma omp atomic
            flux_map_grp(igrp, imap, ibin) += weight;
          }
        }

        // Angle-averaged spectral flux for groups
        if (output_flux_avg_grp) {
          const double freq_bin = inverse_freq_bin_width_grp * (freq_grp - freq_min_grp);
          if (freq_bin >= 0. && freq_bin < double(n_bins_grp)) {
            const int ibin = floor(freq_bin);
            #pragma omp atomic
            flux_avg_grp(igrp, ibin) += weight;
          }
        }

        // Line-of-sight radial map for groups (perpendicular distance)
        if (output_radial_map_grp) {
          const Vec3 r_cam = position - r_grp_flows[igrp]; // Position relative to group center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_map_radius_grp > 0.) {     // Logarithmically spaced radial map
            r_pix = (r_perp < min_map_radius_grp) ? 0. : 1. + (log10(r_perp) - log_min_map_radius_grp) * inverse_map_pixel_width_grp;
          } else {                           // Linearly spaced radial map
            r_pix = inverse_map_pixel_width_grp * r_perp;
          }
          if (r_pix < double(n_map_pixels_grp)) {
            const int imap = vec2pix_ring(n_side_radial_grp, direction); // Healpix index
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_map_grp(igrp, imap, ir) += weight;
          }
        }

        // Angle-averaged radial surface brightness for groups (perpendicular distance)
        if (output_radial_avg_grp) {
          const Vec3 r_cam = position - r_grp_flows[igrp]; // Position relative to group center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_radius_grp > 0.) {         // Logarithmically spaced radial avg
            r_pix = (r_perp < min_radius_grp) ? 0. : 1. + (log10(r_perp) - log_min_radius_grp) * inverse_pixel_width_grp;
          } else {                           // Linearly spaced radial avg
            r_pix = inverse_pixel_width_grp * r_perp;
          }
          if (r_pix < double(n_pixels_grp)) {
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_avg_grp(igrp, ir) += weight;
          }
        }
      }
    #endif

    // Subhalo statistics
    #if output_subhalos
      if (output_sub_obs && valid_isub) {
        const double freq_sub = frequency + direction.dot(v_sub[isub]); // Frequency in subhalo frame
        #pragma omp atomic
        f_esc_sub[isub] += weight;           // Escape fraction
        #pragma omp atomic
        f2_esc_sub[isub] += weight * weight; // Squared f_esc

        // Line-of-sight healpix maps for subhalos
        if (output_map_sub) {
          const int imap = vec2pix_ring(n_side_sub, direction); // Healpix index
          #pragma omp atomic
          map_sub(isub, imap) += weight;
          if (output_map2_sub) {
            #pragma omp atomic
            map2_sub(isub, imap) += weight * weight; // Statistical convergence
            if (output_freq_map_sub) {
              const double weighted_freq_sub = weight * freq_sub; // Weighted frequency
              #pragma omp atomic
              freq_map_sub(isub, imap) += weighted_freq_sub; // Average frequency map
              if (output_freq2_map_sub) {
                const double weighted_freq2_sub = weighted_freq_sub * freq_sub; // Weighted frequency^2
                #pragma omp atomic
                freq2_map_sub(isub, imap) += weighted_freq2_sub; // Frequency^2 map
                if (output_freq3_map_sub) {
                  const double weighted_freq3_sub = weighted_freq2_sub * freq_sub; // Weighted frequency^3
                  #pragma omp atomic
                  freq3_map_sub(isub, imap) += weighted_freq3_sub; // Frequency^3 map
                  if (output_freq4_map_sub)
                    #pragma omp atomic
                    freq4_map_sub(isub, imap) += weighted_freq3_sub * freq_sub; // Frequency^4 map
                }
              }
            }
          }
        }

        // Line-of-sight healpix flux subhalo maps
        if (output_flux_map_sub) {
          const double freq_bin = inverse_map_freq_bin_width_sub * (freq_sub - map_freq_min_sub);
          if (freq_bin >= 0. && freq_bin < double(n_map_bins_sub)) {
            const int imap = vec2pix_ring(n_side_flux_sub, direction); // Healpix index
            const int ibin = floor(freq_bin);
            #pragma omp atomic
            flux_map_sub(isub, imap, ibin) += weight;
          }
        }

        // Angle-averaged spectral flux for subhalos
        if (output_flux_avg_sub) {
          const double freq_bin = inverse_freq_bin_width_sub * (freq_sub - freq_min_sub);
          if (freq_bin >= 0. && freq_bin < double(n_bins_sub)) {
            const int ibin = floor(freq_bin);
            #pragma omp atomic
            flux_avg_sub(isub, ibin) += weight;
          }
        }

        // Line-of-sight radial map for subhalos (perpendicular distance)
        if (output_radial_map_sub) {
          const Vec3 r_cam = position - r_sub_flows[isub]; // Position relative to subhalo center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_map_radius_sub > 0.) {     // Logarithmically spaced radial map
            r_pix = (r_perp < min_map_radius_sub) ? 0. : 1. + (log10(r_perp) - log_min_map_radius_sub) * inverse_map_pixel_width_sub;
          } else {                           // Linearly spaced radial map
            r_pix = inverse_map_pixel_width_sub * r_perp;
          }
          if (r_pix < double(n_map_pixels_sub)) {
            const int imap = vec2pix_ring(n_side_radial_sub, direction); // Healpix index
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_map_sub(isub, imap, ir) += weight;
          }
        }

        // Angle-averaged radial surface brightness for subhalos (perpendicular distance)
        if (output_radial_avg_sub) {
          const Vec3 r_cam = position - r_sub_flows[isub]; // Position relative to subhalo center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_radius_sub > 0.) {         // Logarithmically spaced radial avg
            r_pix = (r_perp < min_radius_sub) ? 0. : 1. + (log10(r_perp) - log_min_radius_sub) * inverse_pixel_width_sub;
          } else {                           // Linearly spaced radial avg
            r_pix = inverse_pixel_width_sub * r_perp;
          }
          if (r_pix < double(n_pixels_sub)) {
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_avg_sub(isub, ir) += weight;
          }
        }
      }
    #endif

    // Line-of-sight angular cosine (μ=cosθ) map
    if (output_mu) {
      const double mu_r = dot(mu_north, direction); // cos(theta) in [-1,1]
      const int imu = floor(inverse_dmu * (mu_r + 1.));
      #pragma omp atomic
      mu1[imu] += weight;
      if (output_mu2)
        #pragma omp atomic
        mu2[imu] += weight * weight;         // Statistical convergence
      if (output_freq_mu) {
        #pragma omp atomic
        freq_mu[imu] += weighted_freq;       // Average frequency
        if (output_freq2_mu) {
          #pragma omp atomic
          freq2_mu[imu] += weighted_freq2;   // Frequency^2
          if (output_freq3_mu) {
            #pragma omp atomic
            freq3_mu[imu] += weighted_freq3; // Frequency^3
            if (output_freq4_mu)
              #pragma omp atomic
              freq4_mu[imu] += weighted_freq4; // Frequency^4
          }
        }
      }
    }

    // Line-of-sight angular cosine (μ=cosθ) flux map
    if (output_flux_mu) {
      const double freq_bin = inverse_mu_freq_bin_width * (frequency - mu_freq_min);
      if (freq_bin >= 0. && freq_bin < double(n_mu_bins)) {
        const double mu_r = dot(mu_north, direction); // cos(theta) in [-1,1]
        const int imu = floor(inverse_dmu_flux * (mu_r + 1.));
        const int ibin = floor(freq_bin);
        #pragma omp atomic
        flux_mu(imu, ibin) += weight;
      }
    }

    // Radial flow statistics
    #if output_radial_flow
      #pragma omp atomic
      radial_line_flow.esc[rf_bin_src] += weight;
    #endif

    // Group flow statistics
    #if output_group_flows
      if (valid_igrp)
        #pragma omp atomic
        group_line_flows[igrp].esc[gf_bin_src] += weight;
    #endif

    // Subhalo flow statistics
    #if output_subhalo_flows
      if (valid_isub)
        #pragma omp atomic
        subhalo_line_flows[isub].esc[sf_bin_src] += weight;
    #endif

    // Collisional excitation global statistics
    if (output_collisions && f_col > 0.) {
      const double weight_col = weight * f_col;
      const double weighted_freq_col = weight_col * frequency;
      const double weighted_freq2_col = weighted_freq_col * frequency;
      const double weighted_freq3_col = weighted_freq2_col * frequency;
      #pragma omp atomic
      f_esc_col += weight_col;               // Global escape fraction
      #pragma omp atomic
      f2_esc_col += weight_col * weight_col; // Global squared f_esc_col
      #pragma omp atomic
      freq_avg_col += weighted_freq_col;     // Global average frequency
      #pragma omp atomic
      freq_std_col += weighted_freq2_col;    // Global standard deviation
      #pragma omp atomic
      freq_skew_col += weighted_freq3_col;   // Global frequency skewness
      #pragma omp atomic
      freq_kurt_col += weighted_freq3_col * frequency; // Global frequency kurtosis
    }

    if (output_stars && source_type == STARS) {
      #pragma omp atomic
      weight_stars[source_id] += weight;     // Add photon weight at escape to the star
    }

    if (output_cells) {
      if (source_type == CELLS) {
        #pragma omp atomic
        weight_cells[source_id] += weight;   // Add photon weight at escape to the cell
      }
      if (source_type == DOUBLET_CELLS) {
        #pragma omp atomic
        weight_doublet_cells[source_id] += weight; // Add doublet photon weight at escape to the cell
      }
    }
  }
}
