/***************
 * mcrt/MCRT.h *
 ***************

 * Module declarations.

*/

#ifndef MCRT_INCLUDED
#define MCRT_INCLUDED

#include "../Simulation.h" // Base simulation class

/* Monte Carlo radiative transfer module. */
class MCRT : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  void correct_units();                      // Convert to observed units

  // Image projections
  template <void (*rule)(double &, double, int)>
  double ray_trace(const double rx, const double ry); // Single ray
  template <void (*rule)(double &, double, int)>
  double pixel_quad_2D(const int ix, const int iy); // Adaptive 2D integrator
  template <void (*rule)(double &, double, int)>
  void calculate_projections(Image& proj_image); // Single image and direction
  void calculate_projections_HII(Image& proj_image); // Unresolved HII regions
  void run_projections();                    // Driver for image projections

  // Cube projections
  template <void (*rule)(double *, double, int)>
  void ray_trace(double *results, const double rx, const double ry); // Single ray
  template <void (*rule)(double *, double, int)>
  void pixel_quad_2D(double *pixel, const int ix, const int iy); // Adaptive 2D integrator
  template <void (*rule)(double *, double, int)>
  void calculate_projections(Cube& proj_cube); // Single cube and direction
  void calculate_projections_HII(Cube& proj_cube); // Unresolved HII regions
  void run_cube_projections();               // Driver for cube projections
};

#endif // MCRT_INCLUDED
