/*************
 * mcrt/H.cc *
 *************

 * Computes the Voigt-Hjerting function from continued fraction
   expansions centered at 0, \infty, and the interval x^2 in (3, 25)

*/

#include <math.h> // Math functions
#include <gsl/gsl_sf_dawson.h> // Dawson's integral

// #define A0 15.75328153963877
// #define A1 286.9341762324778
// #define A2 19.05706700907019
// #define A3 28.22644017233441
// #define A4 9.526399802414186
// #define A5 35.2921702628613
// #define A6 0.8681020834678775
// #define B0 0.0003300469163682737
// #define B1 0.5403095364583999
// #define B2 2.676724102580895
// #define B3 12.8202608260622
// #define B4 3.21166435627278
// #define B5 32.03298193342
// #define B6 9.0328158696
// #define B7 23.748999906
// #define B8 1.8210617057
// #define INV_SQRT_PI 0.5641895835477563 // 1 / sqrt(pi)

// // Voigt-Hjerting function: a = "damping parameter", x = frequency (Smith+15 Eqs. 19 and A1)
// double H_continued_fractions(const double a, const double x) {
//   const double z = x * x; // x^2
//   if (z < 3.)
//     return exp(-z) * (1. - a * (A0 + A1 / (z - A2 + A3 / (z - A4 + A5 / (z - A6)))));
//   else if (z > 25.)
//     return INV_SQRT_PI * a / (z - 1.5 - 1.5 / (z - 3.5 - 5. / (z - 5.5)));
//   else
//     return exp(-z) + a * (B0 + B1 / (z - B2 + B3 / (z + B4 + B5 / (z - B6 + B7 / (z - B8)))));
// }

// Voigt-Hjerting function: a = "damping parameter", x = frequency (Smith+15 Eq. 19)
double H_voigt(const double a, const double x) {
  const double z = x * x; // x^2
  return exp(-z) * (1. + a * a * (1. - 2. * z)) + M_2_SQRTPI * a * (2. * x * gsl_sf_dawson(x) - 1.);
}

// Gaussian line profile function: a = "damping parameter", x = frequency
double H_gaussian([[maybe_unused]] const double a, const double x) {
  return exp(-x * x); // Gaussian exp(-x^2)
}

double (*H)(const double, const double); // Function pointer for different line types
