/****************
 * mcrt/vars.cc *
 ****************

 * Module global variable declarations (defaults).

*/

#include "proto.h"

// Line parameters
string line = "Lyman-alpha";                 // Name of the line
double E0;                                   // Line energy [erg]
double A21;                                  // Einstein A coefficient [Hz]
double f12;                                  // Oscillator strength
double g12;                                  // Degeneracy ratio: g_lower / g_upper
double T0;                                   // Line temperature [K]
double nu0;                                  // Line frequency [Hz]
double lambda0;                              // Line wavelength [angstrom]
double B21;                                  // Einstein B21 = A21 c^2 / (2 h ν0^3)
double DnuL;                                 // Natural frequency broadening [Hz]
double p_dest = 0.;                          // Line destruction probability
double E0F = 0.;                             // Fluorescent line energy [erg]
double A2F = 0.;                             // Fluorescent Einstein A coefficient [Hz]
double P2F = 0.;                             // Probability of fluorescence
double nu0F = 0.;                            // Fluorescent line frequency [Hz]
double lambda0F = 0.;                        // Fluorescent line wavelength [angstrom]
double xFDa = 0.;                            // Fluorescent line: x / a
double avthDc;                               // Conversion for special relativity
double m_carrier;                            // Mass of carrier [g]
bool scaled_microturb = false;               // Scaled microturbulence model
double v_turb = 0.;                          // Microturbulent velocity [cm/s]
double T_turb;                               // Microturbulent temperature [K]
double vth_div_sqrtT;                        // Thermal velocity: vth = sqrt(2 kB T / m_carrier)
double DnuD_div_sqrtT;                       // Doppler width: ΔνD = ν0 vth / c
double a_sqrtT;                              // "damping parameter": a = ΔνL / 2 ΔνD
double sigma0_sqrtT;                         // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
double x_div_aDv;                            // Constant for stellar continuum emission
double gDa;                                  // Recoil parameter / "damping parameter" = 2 h ν0^2 / (mH c^2 ΔνL)

// Deuterium line parameters
double E0D = 0.;                             // Line energy [erg]
double nu0D = 0.;                            // Line frequency [Hz]
double lambda0D = 0.;                        // Line wavelength [angstrom]
double xDA = 0.;                             // Frequency translation: x' = A x + B a
double xDB = 0.;                             // Frequency translation: x' = A x + B a
double gDaD = 0.;                            // Recoil parameter: g' / a'

// Doublet line parameters
double E0p = 0.;                             // Line energy [erg]
double A21p = 0.;                            // Einstein A coefficient [Hz]
double f12p = 0.;                            // Oscillator strength
double g12p = 0.;                            // Degeneracy ratio: g_lower / g_upper
double nu0p = 0.;                            // Line frequency [Hz]
double lambda0p = 0.;                        // Line wavelength [angstrom]
double DnuLp = 0.;                           // Natural frequency broadening [Hz]
double E0Fp = 0.;                            // Fluorescent line energy [erg]
double A2Fp = 0.;                            // Fluorescent Einstein A coefficient [Hz]
double P2Fp = 0.;                            // Probability of fluorescence
double nu0Fp = 0.;                           // Fluorescent line frequency [Hz]
double lambda0Fp = 0.;                       // Fluorescent line wavelength [angstrom]
double xFpDa = 0.;                           // Fluorescent line: x' / a'
double xpA = 0.;                             // Frequency translation: x' = A x + B a
double xpB = 0.;                             // Frequency translation: x' = A x + B a
double apA = 0.;                             // Damping paramter factor: a' = A a
double sigma0p_sqrtT = 0.;                   // Cross section: sigma0_sqrtT = sigma0 * sqrt(T)
double gDap = 0.;                            // Recoil parameter: g' / a'

// Information about the dust model
double kappa_dust = 0.;                      // Dust opacity [cm^2/g dust]
double sigma_dust = 0.;                      // Dust cross section [cm^2/Z/hydrogen atom]
double albedo = 0.;                          // Dust albedo for scattering vs. absorption
double g_dust = 0.;                          // Anisotropy parameter: <μ> for dust scattering
double _1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g; // Constants for Henyey-Greenstein dust scattering

// Configurable line transfer flags
bool recombinations = false;                 // Include recombination emission
double T_floor_rec = 0.;                     // Apply a recombination temperature floor: T [K]
bool collisions = false;                     // Include collisional excitation
bool output_collisions = false;              // Output collisional excitation data
double collisions_limiter = 0.;              // Limited by photoheating rate
bool spontaneous = false;                    // Include spontaneous emission
bool continuum = false;                      // Include stellar continuum emission
bool unresolved_HII = false;                 // Include unresolved HII regions
bool two_level_atom = false;                 // Two level atom line transfer
bool anisotropic_scattering = false;         // Turn anisotropic scattering on/off
bool resonant_scattering = true;             // Turn resonant scattering on/off
bool doppler_frequency = false;              // Output frequency in Doppler widths
bool recoil = true;                          // Include recoil with scattering

// Information about the source
int n_absorbed;                              // Number of absorbed photons
int n_escaped;                               // Number of escaped photons
int n_finished;                              // Track finished progress
double Ndot_tot = 0.;                        // Total emission rate [photons/s]
double L_tot = 0.;                           // Total luminosity [erg/s]
double L_point = 0.;                         // Point source luminosity [erg/s]
double L_plane_cont = 0.;                    // Plane continuum source luminosity [erg/s]
double L_sphere_cont = 0.;                   // Sphere continuum source luminosity [erg/s]
double L_shell_cont = 0.;                    // Shell continuum source luminosity [erg/s]
double L_shell_line = 0.;                    // Shell line source luminosity [erg/s]
double L_shock = 0.;                         // Shock source luminosity [erg/s]
double L_rec = 0.;                           // Recombination luminosity [erg/s]
double L_col = 0.;                           // Collisional excitation luminosity [erg/s]
double L_sp = 0.;                            // Spontaneous emission luminosity [erg/s]
double Lp_col = 0.;                          // Doublet collisional excitation luminosity [erg/s]
double L_cont = 0.;                          // Continuum emission luminosity [erg/s]
double M_cont = 0.;                          // Total M_cont [absolute magnitude]
double L_HII = 0.;                           // Unresolved HII regions luminosity [erg/s]
double f_esc_HII = 0.;                       // Escape fraction from unresolved HII regions

// Point source variables
bool point_source = false;                   // Insert photons at a specified point (default)
int i_point = 0;                             // Cell index to insert photons
double x_point = 1e-12;                      // Point source position [cm]
double y_point = 1e-12;
double z_point = 1e-12;
Vec3 r_point = {x_point, y_point, z_point};  // (x, y, z) [cm]

// Plane source variables
bool plane_cont_source = false;              // Insert photons at a specified plane
double F_plane_cont = 0.;                    // Plane continuum source flux [erg/s/angstrom]
double S_plane_cont = 0.;                    // Plane continuum surface flux [erg/s/cm^2/angstrom]

// Sphere source variables
bool sphere_cont_source = false;             // Insert continuum photons within a sphere
int i_sphere_cont = 0;                       // Cell index to insert photons
double sphere_radius_cont = 0.;              // Sphere radius (continuum) [cm]
Vec3 sphere_center_cont = {1e-12, 1e-12, 1e-12}; // Sphere center (continuum) (x, y, z) [cm]

// Shell source variables
bool shell_cont_source = false;              // Insert photons from a shell (continuum)
bool shell_line_source = false;              // Insert photons from a shell (line)
bool shell_blackbody = false;                // Luminosity based on a black body
double r_shell_cont = 1e-12;                 // Shell continuum source radius [cm]
double r_shell_line = 1e-12;                 // Shell line source radius [cm]
double T_shell = 0.;                         // Shell source temperature [K]

// Shock source variables
bool shock_source = false;                   // Insert photons from a shock
bool shock_blackbody = false;                // Luminosity based on a black body
double r_shock = 1e-12;                      // Shock source radius [cm]
double T_shock = 0.;                         // Shock source temperature [K]

// Spatial emission from cells and stars
bool avoid_SFR = false;                      // Ignore cells with SFR > 0
double j_exp = 1.;                           // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
double V_exp = 1.;                           // Volume boosting exponent (V_exp < 1 favors smaller cells)
array<int, n_cdf_cells> j_map_cells;         // Lookup table for photon insertion (cells)
vector<double> j_cdf_cells;                  // Cumulative distribution function for emission
vector<double> j_weights_cells;              // Initial photon weights for each cell
vector<double> L_int_cells;                  // Intrinsic luminosities for each cell
vector<double> Ndot_int_cells;               // Intrinsic photon rates for each cell
vector<double> source_weight_cells;          // Photon weight at emission for each cell
vector<double> weight_cells;                 // Photon weight at escape for each cell
vector<double> f_col_cells;                  // Collisional excitation fractions for each cell
bool doublet_cell_based_emission = false;    // Include doublet cell-based sources
array<int, n_cdf_cells> jp_map_cells;        // Lookup table for doublet photon insertion (cells)
vector<double> jp_cdf_cells;                 // Cumulative distribution function for doublet emission
vector<double> jp_weights_cells;             // Initial doublet photon weights for each cell
vector<double> L_int_doublet_cells;          // Intrinsic doublet luminosities for each cell
vector<double> source_weight_doublet_cells;  // Doublet photon weight at emission for each cell
vector<double> weight_doublet_cells;         // Doublet photon weight at escape for each cell
array<int, n_cdf_stars> j_map_stars;         // Lookup table for photon insertion (stars)
vector<double> j_cdf_stars;                  // Cumulative distribution function for emission
vector<double> j_weights_stars;              // Initial photon weights for each star
vector<double> L_int_stars;                  // Intrinsic luminosities for each star
vector<double> Ndot_int_stars;               // Intrinsic photon rates for each star
vector<double> source_weight_stars;          // Photon weight at emission for each star
vector<double> weight_stars;                 // Photon weight at escape for each star
vector<double> weight_stars_grp_vir;         // Photon weight at escape for each star (group virial)
vector<double> weight_stars_grp_gal;         // Photon weight at escape for each star (group galaxy)
vector<double> weight_stars_sub_vir;         // Photon weight at escape for each star (subhalo virial)
vector<double> weight_stars_sub_gal;         // Photon weight at escape for each star (subhalo galaxy)
double emission_n_min = 0.;                  // Minimum number density of emitting cells [cm^-3]
double emission_n_max = positive_infinity;   // Maximum number density of emitting cells [cm^-3]
double emission_ne_min = 0.;                 // Minimum electron density of emitting cells [cm^-3]
double emission_ne_max = positive_infinity;  // Maximum electron density of emitting cells [cm^-3]

// Distinguish between source types
int n_source_types;                          // Number of source types used
array<double, MAX_SOURCE_TYPES> mixed_pdf;   // Mixed source probability distribution function
array<double, MAX_SOURCE_TYPES> mixed_cdf;   // Mixed source cumulative distribution function
array<double, MAX_SOURCE_TYPES> mixed_weights; // Mixed source weights (point, cells, stars)
vector<int> source_types_mask;               // Source type mask indices
strings source_names_mask;                   // Source type names (masked)
vector<double> mixed_pdf_mask;               // Mixed source pdf (masked)
vector<double> mixed_cdf_mask;               // Mixed source cdf (masked)
vector<double> mixed_weights_mask;           // Mixed source weights (masked)

// Exit variables
bool exit_wrt_com = false;                   // Exit with respect to the center of mass frame
double T_exit = 1e4;                         // Exit temperature [K]
double a_exit;                               // "Damping parameter" [T_exit]
double DnuD_exit;                            // Doppler width at T_exit
double vth_div_c_exit;                       // Thermal velocity (vth/c) [T_exit]
double neg_vth_kms_exit;                     // Thermal velocity (-km/s) [T_exit]
// double neg_a_vth_kms_exit;                   // Constant for observable frequency conversion (x -> Δv)
bool adjust_camera_frequency = false;        // Adjust camera frequency based on offsets
vector<double> freq_offsets;                 // Frequency offsets for each camera

RadialLineFlow radial_line_flow;             // Number of events by radial flow [photons/s]
vector<RadialLineFlow> group_line_flows;     // Number of events by radial flow for each group [photons/s]
vector<RadialLineFlow> subhalo_line_flows;   // Number of events by radial flow for each subhalo [photons/s]

// Information about escaped photons
double f_src = 0.;                           // Global source fraction: sum(w0)
double f2_src = 0.;                          // Global squared f_src: sum(w0^2)
double n_photons_src = 0.;                   // Effective number of emitted photons: 1/<w0>
double f_esc = 0.;                           // Global escape fraction: sum(w)
double f2_esc = 0.;                          // Global squared f_esc: sum(w^2)
double n_photons_esc = 0.;                   // Effective number of escaped photons: 1/<w>
double freq_avg = 0.;                        // Global average frequency [freq units]
double freq_std = 0.;                        // Global standard deviation [freq units]
double freq_skew = 0.;                       // Global frequency skewness [normalized]
double freq_kurt = 0.;                       // Global frequency kurtosis [normalized]
bool output_photons = true;                  // Output photon packets (esc/abs)
bool output_n_scat = false;                  // Output number of scattering events
bool output_path_length = false;             // Output total path length [cm]
bool output_source_position = false;         // Output position at emission [cm]
bool output_stars = false;                   // Output star emission and escape
bool output_cells = false;                   // Output cell emission and escape
string photon_file = "";                     // Output a separate photon file
// bool escaped_frequency_moments = false;      // Calculate escaped frequency moments
vector<int> source_ids;                      // Emission cell or star index
vector<int> source_types;                    // Emission type specifier
vector<int> n_scats;                         // Number of scattering events
vector<double> source_weights;               // Photon weight at emission
vector<double> f_cols;                       // Collisional excitation fraction
vector<double> freqs;                        // Frequency at escape [code_freq]
vector<double> weights;                      // Photon weight at escape
vector<Vec3> source_positions;               // Position at emission [cm]
vector<Vec3> positions;                      // Position at escape [cm]
vector<Vec3> directions;                     // Direction at escape
vector<double> path_lengths;                 // Total path length [cm]

// Information about secondary cameras (mcrt)
bool output_mcrt_emission = false;           // Output intrinsic emission without transport
bool output_mcrt_attenuation = false;        // Output attenuated emission without scattering
vector<double> f_escs_ext;                   // Attenuated escape fractions [fraction]
vector<double> freq_avgs_int;                // Intrinsic frequency averages [freq units]
vector<double> freq_avgs_ext;                // Attenuated frequency averages [freq units]
vectors fluxes_int;                          // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
vectors fluxes_ext;                          // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
Images images_int;                           // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
Images images_ext;                           // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
Cubes cubes_int;                             // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
Cubes cubes_ext;                             // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
vectors radial_images_int;                   // Intrinsic radial surface brightness images [erg/s/cm^2/arcsec^2]
vectors radial_images_ext;                   // Attenuated radial surface brightness images [erg/s/cm^2/arcsec^2]
Images radial_cubes_int;                     // Intrinsic radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
Images radial_cubes_ext;                     // Attenuated radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about secondary cameras (proj)
bool output_proj_emission = false;           // Output intrinsic emission without transport
bool output_proj_attenuation = false;        // Output attenuated emission without scattering
bool output_proj_cube_emission = false;      // Output intrinsic emission without transport (cubes)
bool output_proj_cube_attenuation = false;   // Output attenuated emission without scattering (cubes)
vector<double> proj_f_escs_ext;              // Attenuated escape fractions [fraction]
vector<double> proj_cube_f_escs_ext;         // Cube attenuated escape fractions [fraction]
Images proj_images_int;                      // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
Images proj_images_ext;                      // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
Cubes proj_cubes_int;                        // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
Cubes proj_cubes_ext;                        // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Collisional excitation camera data
double f_src_col = 0.;                       // Global source fraction: sum(w0)
double f2_src_col = 0.;                      // Global squared f_src_col: sum(w0^2)
double n_photons_src_col = 0.;               // Effective number of emitted photons: 1/<w0>
double f_esc_col = 0.;                       // Global escape fraction: sum(w)
double f2_esc_col = 0.;                      // Global squared f_esc_col: sum(w^2)
double n_photons_esc_col = 0.;               // Effective number of escaped photons: 1/<w>
double freq_avg_col = 0.;                    // Global average frequency [freq units]
double freq_std_col = 0.;                    // Global standard deviation [freq units]
double freq_skew_col = 0.;                   // Global frequency skewness [normalized]
double freq_kurt_col = 0.;                   // Global frequency kurtosis [normalized]
vector<double> f_escs_col;                   // Escape fractions [fraction]
vector<double> freq_avgs_col;                // Frequency averages [freq units]
vector<double> freq_stds_col;                // Standard deviations [freq units]
vector<double> freq_skews_col;               // Frequency skewnesses [normalized]
vector<double> freq_kurts_col;               // Frequency kurtoses [normalized]
vectors fluxes_col;                          // Spectral fluxes [erg/s/cm^2/angstrom]
Images images_col;                           // Surface brightness images [erg/s/cm^2/arcsec^2]
Images images2_col;                          // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
Images freq_images_col;                      // Average frequency images [freq units]
Images freq2_images_col;                     // Frequency^2 images [freq^2 units]
Images freq3_images_col;                     // Frequency^3 images [freq^3 units]
Images freq4_images_col;                     // Frequency^4 images [freq^4 units]
vector<double> f_escs_ext_col;               // Attenuated escape fractions [fraction]
vector<double> freq_avgs_int_col;            // Intrinsic frequency averages [freq units]
vector<double> freq_avgs_ext_col;            // Attenuated frequency averages [freq units]
vectors fluxes_int_col;                      // Intrinsic spectral fluxes [erg/s/cm^2/angstrom]
vectors fluxes_ext_col;                      // Attenuated spectral fluxes [erg/s/cm^2/angstrom]
Images images_int_col;                       // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
Images images_ext_col;                       // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
vector<double> proj_f_escs_ext_col;          // Attenuated escape fractions [fraction]
vector<double> proj_cube_f_escs_ext_col;     // Cube attenuated escape fractions [fraction]
Images proj_images_int_col;                  // Intrinsic surface brightness images [erg/s/cm^2/arcsec^2]
Images proj_images_ext_col;                  // Attenuated surface brightness images [erg/s/cm^2/arcsec^2]
Cubes proj_cubes_int_col;                    // Intrinsic spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
Cubes proj_cubes_ext_col;                    // Attenuated spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]