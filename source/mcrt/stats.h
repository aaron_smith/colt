/**********************
 * mcrt/stats.h *
 **********************

 * Module stats utilities.

*/

#ifndef MCRT_STATS_INCLUDED
#define MCRT_STATS_INCLUDED

// Calculate radial bin from radial position [cm]
static inline int find_radial_bin(const double radius, const int n_radial_bins, const vector<double>& radial_edges) {
  if (radius < radial_edges[0])
    error("Photon cannot have radius < 0: radius = "+to_string(radius));
  if (radius >= radial_edges[n_radial_bins])
    return OUTSIDE_DUAL;                     // Outside radial range
  int radial_bin = 0;                        // Initial radial bin
  while (radius >= radial_edges[radial_bin+1])
    radial_bin++;                            // Search radial bins
  return radial_bin;
}

// Calculate age bin from stellar age [Gyr]
static inline int find_age_bin(const double age, const int n_age_bins, const vector<double>& age_edges) {
  int age_bin = -1;                          // Initial age bin
  if (age < age_edges[n_age_bins])
    while (age >= age_edges[age_bin+1])
      age_bin++;                             // Search age bins
  return age_bin;
}


// Compute the maximum distance the photon can travel in the radial cell.
static inline tuple<double, int> radial_face_distance(const Vec3& point, const Vec3& direction,
  const int cell, const int n_radial_bins, const vector<double>& radial_edges) {
  const double mu_r = direction.dot(point);  // Unnormalized radial cosine
  const double r2 = point.dot();             // Radius^2
  const double r_diff = mu_r*mu_r - r2;      // (k*r)^2 - r^2
  if (cell == 0) {                           // No inner radius
    const double rp = radial_edges[1];       // First outer radius
    return make_tuple(sqrt(r_diff + rp*rp) - mu_r, 1);
  }
  if (cell == OUTSIDE_DUAL) {                // Outside radial range
    if (mu_r >= 0.)                          // No intersection
      return make_tuple(positive_infinity, OUTSIDE_DUAL);
    const double rm = radial_edges[n_radial_bins]; // Inner radius
    const double disc_inner = r_diff + rm*rm; // Inner discriminant
    if (disc_inner <= 0.)                    // No intersection
      return make_tuple(positive_infinity, OUTSIDE_DUAL);
    return make_tuple(-sqrt(disc_inner) - mu_r, n_radial_bins-1);
  }
  const double rm = radial_edges[cell];      // Inner radius
  const double disc_inner = r_diff + rm*rm;  // Inner discriminant
  // Outward radial propagation: l = -k*r + sqrt(|k*r|^2 - r^2 + r_outer^2)
  // Case: k*r >= 0 or k*r can be < 0 but not cross inner radius
  if (mu_r >= 0. || disc_inner <= 0.) {
    const int next = cell + 1;               // Outer face values
    const double rp = radial_edges[next];    // Outer radius
    return make_tuple(sqrt(r_diff + rp*rp) - mu_r, (next < n_radial_bins) ? next : OUTSIDE_DUAL);
  }

  // Inward radial propagation: l = -k*r - sqrt(|k*r|^2 - r^2 + r_inner^2)
  // Case: k*r < 0, cell > 0, and disc_inner > 0
  return make_tuple(-sqrt(disc_inner) - mu_r, cell - 1);
}

struct RadialLineFlow {
  int n_radial_bins = 0;                     // Number of radial bin
  vector<double> radial_edges;               // Radial bin edges [cm]
  vector<double> src;                        // Emission (source radial)
  vector<double> esc;                        // Escape (source radial)
  vector<double> abs;                        // Absorption (event radial)
  Image pass;                                // First passage (source radial and shell boundaries)
  Image flow;                                // Flow (source radial and shell boundaries)

  // Initialize data structures
  void setup() {
    src = vector<double>(n_radial_bins);     // Allocate emission
    esc = vector<double>(n_radial_bins);     // Allocate escape
    abs = vector<double>(n_radial_bins);     // Allocate absorption
    pass = Image(n_radial_bins, n_radial_bins); // Allocate first passage
    flow = Image(n_radial_bins, n_radial_bins); // Allocate flow
  }

  // Calculate radial bin from radial position [cm]
  inline int find_radial_bin(const double radius) {
    return ::find_radial_bin(radius, n_radial_bins, radial_edges);
  }

  // Compute the maximum distance the photon can travel in the radial cell.
  tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
    return radial_face_distance(point, direction, cell, n_radial_bins, radial_edges);
  }
};

extern RadialLineFlow radial_line_flow;               // Number of events by radial flow [photons/s]
extern vector<RadialLineFlow> group_line_flows;       // Number of events by radial flow for each group [photons/s]
extern vector<RadialLineFlow> subhalo_line_flows;     // Number of events by radial flow for each subhalo [photons/s]

#endif // MCRT_STATS_INCLUDED
