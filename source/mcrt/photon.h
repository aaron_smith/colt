/************
 * photon.h *
 ************

 * Photon packets.

*/

#ifndef PHOTON_INCLUDED
#define PHOTON_INCLUDED

#include "../proto.h"                        // Function prototypes and data structures

struct Photon {
  Photon();                                  // Initialization constructor
  void print();                              // Print photon data
  void ray_trace();                          // Perform MCRT calculations

  int source_id;                             // Index of emission source
  int source_type;                           // Type of emission source
  double source_weight;                      // Photon weight at emission
  double f_col;                              // Collisional excitation fraction
  int current_cell, max_cell;                // Index of the current cell (and maximum)
  int n_scat;                                // Number of scattering events
  Vec3 source_position;                      // Photon position at emission [cm]
  Vec3 position;                             // Photon position [cm]
  Vec3 direction;                            // Photon direction (normalized)
#if output_radial_flow
  int rf_bin, rf_bin_src, rf_bin_max;        // Radial flow radial bins
  bool rf_in_range;                          // Radial flow in range flags
#endif
#if output_groups || output_group_flows
  int igrp;                                  // Group ID index
  bool valid_igrp;                           // Valid group ID index
#endif
#if output_groups
  bool in_grp_vir = false, in_grp_gal = false; // Group flags
#endif
#if output_group_flows
  int gf_bin, gf_bin_src, gf_bin_max;        // Group flow radial bins
  bool gf_in_range;                          // Group flow in range flags
#endif
#if output_subhalos || output_subhalo_flows
  int isub;                                  // Subhalo ID index
  bool valid_isub;                           // Valid subhalo ID index
#endif
#if output_subhalos
  bool in_sub_vir = false, in_sub_gal = false; // Subhalo flags
#endif
#if output_subhalo_flows
  int sf_bin, sf_bin_src, sf_bin_max;        // Subhalo flow radial bins
  bool sf_in_range;                          // Subhalo flow in range flags
#endif
  double frequency;                          // Photon frequency
  double weight;                             // Photon weight (at escape)
#if output_tau_dust_f_esc
  vector<double> tau_dust_weights;           // Dust optical depth weights
  vector<double> k_dust_sims;                // Dust absorption coefficients [1/cm]
#endif
  double l_tot;                              // Total path length [cm]
  TAU_ON(double tau_eff_abs;)                // Optical depth traversed so far
  double radius;                             // Photon radius [cm] (spherical)
  double radius_old;                         // Previous photon radius [cm] (spherical)
  double mu;                                 // Radial cosine (spherical)
  double mu_old;                             // Previous radial cosine (spherical)
#if cell_velocity_gradients
  double dvdr_LOS;                           // Velocity gradient along LOS
#endif

  // Radiative transfer properties
  double k_dust_abs;                         // Dust absorption coefficient [1/cm]
  double k_dust_scat;                        // Dust scattering coefficient [1/cm]
  double k_grey_abs;                         // Grey absorption coefficient [1/cm]
  double k_grey_scat;                        // Grey scattering coefficient [1/cm]
  double k_line_abs;                         // Line absorption coefficient [1/cm]
  double k_line_scat;                        // Line scattering coefficient [1/cm]
  double kp_line_abs;                        // Partial line absorption coefficient [1/cm]
  double kp_line_scat;                       // Partial line scattering coefficient [1/cm]
  double k_scat;                             // Total scattering coefficient [1/cm]
  double k_abs;                              // Total absorption coefficient [1/cm]
  double dl_next;                            // Next cell path length [cm]
  double dl_neib;                            // Neighbor path length [cm]
  double dl_scat;                            // Scattering path length [cm]
  double dl_line;                            // Line path length [cm]
  double dtau_scat;                          // Scattering optical depth
  double dtau_next;                          // Optical depth to next event
  double neib_cell;                          // Neighbor cell index
  double tau_sobolev;                        // Sobolev optical depth
  double dx;                                 // frequency shift

private:
  void update_dl_scat();                     // Update the scattering path length
  void move_photon(const double distance);   // Updates the position and weight
  void update_mu();                          // Update the radial cosine
  void resonant_scatter(const bool shift_line);
  void crd_line_scatter();
  void coherent_line_scatter();
  void dust_scatter();
  void electron_scatter();

  void calculate_LOS_int();
  void calculate_LOS_int(const int camera, const double phase_weight = 0.5);

  void calculate_LOS_ext();
  void calculate_LOS_ext(const int camera, const double phase_weight = 0.5);

  void calculate_LOS(const int scatter_type);
  void calculate_LOS(const int scatter_type, const int camera);
};

#endif // PHOTON_INCLUDED
