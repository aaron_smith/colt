/****************
 * mcrt/mcrt.cc *
 ****************

 * Driver: Assign photon packets for radiative transfer, etc.

*/

#include "proto.h"
#include "MCRT.h"
#include "../mpi_helpers.h" // MPI helper functions
#include "../timing.h" // Timing functionality
#include "photon.h" // Photon packets
#include <chrono>

extern Timer mcrt_timer; // Clock timing
extern Timer reduce_timer; // MPI timing

void print_observables(); // Post-calculation printing
bool avoid_cell_strict(const int cell); // Avoid calculations without overrides

static inline void print_top() {
  cout << "\n+----------+------------+------------+------------+"
       << "\n| Progress |  Elapsed   |  Estimate  |  Remaining |"
       << "\n+----------+------------+------------+------------+";
}

static inline void print_bottom() {
  cout << "\n+----------+------------+------------+------------+" << endl;
}

/* Print the simulation progress and remaining time estimate. */
static inline void print_progress(const std::chrono::system_clock::time_point start, const int finished, const int total) {
  auto time_elapsed = std::chrono::system_clock::now() - start;
  auto time_remaining = time_elapsed * double(total - finished) / double(finished);
  long long seconds_elapsed = std::chrono::duration_cast<std::chrono::seconds>(time_elapsed).count();
  long long seconds_remaining = std::chrono::duration_cast<std::chrono::seconds>(time_remaining).count();
  long long seconds_estimate = seconds_elapsed + seconds_remaining;
  printf("\n|  %5.1f%%  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |",
         100. * double(finished) / double(total),
         seconds_elapsed / 3600, (seconds_elapsed / 60) % 60, seconds_elapsed % 60,
         seconds_estimate / 3600, (seconds_estimate / 60) % 60, seconds_estimate % 60,
         seconds_remaining / 3600, (seconds_remaining / 60) % 60, seconds_remaining % 60);
}

/* Move vector elements within a vector over disjoint ranges. */
template <typename T>
static inline void disjoint_move(vector<T>& vec, const int last, const int move) {
  #pragma omp parallel for
  for (int i = n_escaped; i < last; ++i)
    vec[i+move] = vec[i];                    // Move non-overlapping elements
}

/* Move absorbed photons in the info vectors. */
static inline void move_absorbed_photons(const int dest) {
  const int offset = dest - n_escaped;       // Memory offset for absorbed photons
  if (offset == 0 || n_absorbed == 0)
    return;                                  // No need to move absorbed photons
  if (offset < 0 || n_absorbed < 0 || dest + n_absorbed > n_photons)
    error("Attemping to move absorbed photons out of expected bounds: n_absorbed = " +
          to_string(n_absorbed) + ", dest = " + to_string(dest) + ", n_escaped = " +
          to_string(n_escaped) + ", n_photons = " + to_string(n_photons));
  // Move range: [n_escaped, n_escaped + n_absorbed) -> [dest, dest + n_absorbed)
  // These overlap if n_absorbed > offset, so only move non-overlapping elements
  const int last = (n_absorbed > offset) ? dest : n_escaped + n_absorbed;
  const int move = (n_absorbed > offset) ? n_absorbed : offset;
  disjoint_move(source_ids, last, move);
  if (n_source_types > 1) disjoint_move(source_types, last, move);
  disjoint_move(source_weights, last, move);
  if (output_n_scat) disjoint_move(n_scats, last, move);
  if (output_collisions) disjoint_move(f_cols, last, move);
  disjoint_move(freqs, last, move);
  disjoint_move(weights, last, move);
  if (output_source_position) disjoint_move(source_positions, last, move);
  disjoint_move(positions, last, move);
  disjoint_move(directions, last, move);
  if (output_path_length) disjoint_move(path_lengths, last, move);
}

/* Swap photons in the info vectors. */
static inline void swap_photons(const int i, const int j) {
  std::swap(source_ids[i], source_ids[j]);
  if (n_source_types > 1) std::swap(source_types[i], source_types[j]);
  std::swap(source_weights[i], source_weights[j]);
  if (output_n_scat) std::swap(n_scats[i], n_scats[j]);
  if (output_collisions) std::swap(f_cols[i], f_cols[j]);
  std::swap(freqs[i], freqs[j]);
  std::swap(weights[i], weights[j]);
  if (output_source_position) std::swap(source_positions[i], source_positions[j]);
  std::swap(positions[i], positions[j]);
  std::swap(directions[i], directions[j]);
  if (output_path_length) std::swap(path_lengths[i], path_lengths[j]);
}

/* Add photon information to the output buffers. */
static inline void add_to_output_buffers(const struct Photon& p, const int index) {
  source_ids[index] = p.source_id;
  if (n_source_types > 1) source_types[index] = p.source_type;
  source_weights[index] = p.source_weight;
  if (output_n_scat) n_scats[index] = p.n_scat;
  if (output_collisions) f_cols[index] = p.f_col;
  freqs[index] = p.frequency;
  weights[index] = p.weight;
  if (output_source_position) source_positions[index] = p.source_position;
  positions[index] = p.position;
  directions[index] = p.direction;
  if (output_path_length) path_lengths[index] = p.l_tot;
}

/* Sort escaped photons to the beginning and absorbed photons to the end. */
static void sort_photons() {
  // Count the number of escaped and absorbed photons
  n_escaped = n_absorbed = 0;
  #pragma omp parallel for reduction(+:n_escaped, n_absorbed)
  for (int i = 0; i < n_finished; ++i) {
    if (weights[i] > 0.)
      ++n_escaped;                           // Escaped photon
    else
      ++n_absorbed;                          // Absorbed photon
  }

  // Sort the escaped and absorbed photons
  for (int i_esc = 0, i_abs = 0; i_esc < n_escaped; ++i_esc) {
    if (weights[i_esc] > 0.)
      continue;                              // Valid escaped photon
    do {
      ++i_abs;                               // Search from end of list
    } while (weights[n_finished-i_abs] <= 0.); // Find next escaped photon
    swap_photons(i_esc, n_finished-i_abs);   // Swap esc/abs photons
  }
}

/* All tasks have a static assignment of equal work. */
static void equal_workers() {
  if (root)
    print_top();                             // Progress top
  const auto start = std::chrono::system_clock::now(); // Reference time
  int work = n_photons / n_ranks;            // Work assignment
  if (rank < n_photons - work * n_ranks)
    ++work;                                  // Assign remaining work
  const int checks = pow(double(work+1), 0.25); // Number of time checks
  const int interval = work / checks;        // Interval between updates

  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < work; ++i) {
    // Create photon packet and perform ray tracing
    auto p = Photon();
    p.ray_trace();

    // Print completed progress and remaining time estimate
    int index;                               // Photon array index
    #pragma omp atomic capture
    index = n_finished++;                    // Update finished counter
    if (output_photons)
      add_to_output_buffers(p, index);       // Populate output buffers
    if (root && ++index % interval == 0)     // Index -> print counter
      print_progress(start, index, work);
  }
  if (root)
    print_bottom();                          // Progress bottom
}

/* The root task/thread assigns all of the work. */
static void master() {
  print_top();                               // Progress top
  const auto start = std::chrono::system_clock::now(); // Reference time
  const int n_slaves = n_ranks * n_threads - 1; // Number of slaves
  const int f_slaves = 2 * n_slaves;         // Slaves work factor
  int work, n_assigned;                      // Fine-grained load balancing
  if (n_slaves < n_photons) {
    work = n_photons / f_slaves + 1;         // Keep assigning more photons
    n_assigned = work * n_slaves;            // Slaves start with photons
  } else {
    work = 0;                                // Send stop signal immediately
    n_assigned = n_photons;                  // Do not assign beyond n_photons
  }
  const int checks = pow(double(n_photons+1), 0.25); // Number of time checks
  const int interval = n_photons / checks;   // Interval between updates
  int next_check = interval;                 // Next checkpoint for printing
  int finished = 0;                          // Track finished progress
  int message[2];                            // Slave work and thread numbers
  MPI_Status status;                         // MPI status for slave rank

  do {
    // Receive load balancing message from any source
    MPI_Recv(message, 2, MPI_INT, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
    if (work > 0) {
      work = (n_photons - n_assigned) / f_slaves + 1;
      n_assigned += work;
      if (n_assigned > n_photons)
        work = 0;                            // Send done messages now
    }

    // Send work flag back to sender
    MPI_Send(&work, 1, MPI_INT, status.MPI_SOURCE, message[1], MPI_COMM_WORLD);

    // Print completed progress and remaining time estimate
    finished += message[0];                  // Work finished by thread
    if (finished >= next_check) {
      next_check += interval;                // Update print checkpoint
      print_progress(start, finished, n_photons);
    }
  } while (finished < n_photons);            // Finish all work
  print_bottom();                            // Progress bottom
}

/* All other tasks/threads request work. */
static void slave() {
  const int f_slaves = 2 * (n_ranks * n_threads - 1); // Slaves work factor
  const int mpi_thread = rank * n_threads + thread; // Global thread number
  if (mpi_thread > n_photons)
    return;                                  // More slaves than photons
  int work = n_photons / f_slaves + 1;       // Initial amount of work
  int message[2] = {work, mpi_thread};       // Load balancing message

  do {
    for (int i = 0; i < work; ++i) {
      // Create photon packet and perform ray tracing
      auto p = Photon();
      p.ray_trace();

      // Add to output buffers (optional)
      if (output_photons) {
        int index;                           // Photon array index
        #pragma omp atomic capture
        index = n_finished++;                // Update finished counter
        add_to_output_buffers(p, index);     // Populate output buffers
      }
    }

    // Send load balancing message to root
    message[0] = work;                       // Update work message
    MPI_Send(message, 2, MPI_INT, ROOT, 0, MPI_COMM_WORLD);

    // Receive work flag back from root
    MPI_Recv(&work, 1, MPI_INT, ROOT, mpi_thread, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } while (work > 0);                        // New work was assigned
}

/* Convenience function to MPI_Reduce disjoint elements to root. */
template <class T>
static inline void mpi_reduce_disjoint(vector<T>& vs) {
  const int size = vs.size();
  const int size_rank = size / n_ranks;      // Equal assignment
  const int remainder = size - size_rank * n_ranks;
  int i_start = rank * size_rank;            // Start range
  int i_end = (rank + 1) * size_rank;        // End range
  if (rank < remainder) {
    i_start += rank;                         // Correct start range
    i_end += (rank + 1);                     // Correct end range
  } else {
    i_start += remainder;                    // Correct start range
    i_end += remainder;                      // Correct end range
  }
  if (root) {
    for (int i = i_end; i < size; ++i)
      MPI_Recv(vs[i].data(), vs[i].size(), MPI_DOUBLE, MPI_ANY_SOURCE, i, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  } else {
    for (int i = i_start; i < i_end; ++i)
      MPI_Send(vs[i].data(), vs[i].size(), MPI_DOUBLE, ROOT, i, MPI_COMM_WORLD);
  }
}

// MPI collection helper struct
struct RecvInfo {
  void *buffer;                              // Address of receive buffer
  int count;                                 // Number of elements in buffer
  MPI_Datatype datatype;                     // Datatype of buffer elements
  int source;                                // Rank of the source
  int tag;                                   // Message tag
};

/* Convenience function to MPI_Recv data to root. */
static inline void mpi_recv(struct RecvInfo& recv_info) {
  MPI_Recv(recv_info.buffer, recv_info.count, recv_info.datatype,
           recv_info.source, recv_info.tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

/* Ensure unique MPI message tags for safety. */
enum {
  TAG_EI0, TAG_AI0,                          // Emission cell or star index
  TAG_ET0, TAG_AT0,                          // Emission type specifier
  TAG_EW0, TAG_AW0,                          // Photon weight at emission
  TAG_ENS, TAG_ANS,                          // Number of scattering events
  TAG_EC,  TAG_AC,                           // Collisional excitation fraction
  TAG_EF,  TAG_AF,                           // Frequency at escape [code_freq]
  TAG_EW,  TAG_AW,                           // Photon weight at escape
  TAG_EP0, TAG_AP0,                          // Position at emission [cm]
  TAG_EP,  TAG_AP,                           // Position at escape [cm]
  TAG_ED,  TAG_AD,                           // Direction at escape
  TAG_EL,  TAG_AL                            // Path length [cm]
};

/* Convenience function to MPI_Send photon info to root. */
static void mpi_collect_photons() {
  // Gather the number of escaped and absorbed photons to root
  vector<int> rank_esc, rank_abs;            // Number of esc/abs photons
  if (root) {
    rank_esc.resize(n_ranks);                // Allocate space for esc counts
    rank_abs.resize(n_ranks);                // Allocate space for abs counts
  }
  MPI_Gather(&n_escaped, 1, MPI_INT, rank_esc.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);
  MPI_Gather(&n_absorbed, 1, MPI_INT, rank_abs.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);

  if (root) {
    vector<int> ind_esc, ind_abs;            // Index ranges for rank collection
    ind_esc.resize(n_ranks+1);               // Allocate space for esc indices
    ind_abs.resize(n_ranks+1);               // Allocate space for abs indices
    ind_esc[0] = 0;                          // Start root index at zero
    for (int i = 0; i < n_ranks; ++i)
      ind_esc[i+1] = ind_esc[i] + rank_esc[i];
    ind_abs[0] = ind_esc[n_ranks];           // Start root index at end of esc
    for (int i = 0; i < n_ranks; ++i)
      ind_abs[i+1] = ind_abs[i] + rank_abs[i];

    // Move root absorbed photons to correct global position
    move_absorbed_photons(ind_abs[0]);

    // Build a master list of buffers and receive counts
    vector<struct RecvInfo> buffers;
    for (int i = 1; i < n_ranks; ++i) {
      // Receive escaped photon info
      const int i_esc = ind_esc[i];
      const int n_esc = ind_esc[i+1] - ind_esc[i];
      if (n_esc > 0) {
        buffers.push_back({&source_ids[i_esc], n_esc, MPI_INT, i, TAG_EI0});
        if (n_source_types > 1) buffers.push_back({&source_types[i_esc], n_esc, MPI_INT, i, TAG_ET0});
        buffers.push_back({&source_weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EW0});
        if (output_n_scat) buffers.push_back({&n_scats[i_esc], n_esc, MPI_INT, i, TAG_ENS});
        if (output_collisions) buffers.push_back({&f_cols[i_esc], n_esc, MPI_DOUBLE, i, TAG_EC});
        buffers.push_back({&freqs[i_esc], n_esc, MPI_DOUBLE, i, TAG_EF});
        buffers.push_back({&weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EW});
        if (output_source_position) buffers.push_back({&source_positions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_EP0});
        buffers.push_back({&positions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_EP});
        buffers.push_back({&directions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_ED});
        if (output_path_length) buffers.push_back({&path_lengths[i_esc], n_esc, MPI_DOUBLE, i, TAG_EL});
      }

      // Receive absorbed photon info
      const int i_abs = ind_abs[i];
      const int n_abs = ind_abs[i+1] - ind_abs[i];
      if (n_abs > 0) {
        buffers.push_back({&source_ids[i_abs], n_abs, MPI_INT, i, TAG_AI0});
        if (n_source_types > 1) buffers.push_back({&source_types[i_abs], n_abs, MPI_INT, i, TAG_AT0});
        buffers.push_back({&source_weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_AW0});
        if (output_n_scat) buffers.push_back({&n_scats[i_abs], n_abs, MPI_INT, i, TAG_ANS});
        if (output_collisions) buffers.push_back({&f_cols[i_abs], n_abs, MPI_DOUBLE, i, TAG_AC});
        buffers.push_back({&freqs[i_abs], n_abs, MPI_DOUBLE, i, TAG_AF});
        buffers.push_back({&weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_AW});
        if (output_source_position) buffers.push_back({&source_positions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AP0});
        buffers.push_back({&positions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AP});
        buffers.push_back({&directions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AD});
        if (output_path_length) buffers.push_back({&path_lengths[i_abs], n_abs, MPI_DOUBLE, i, TAG_AL});
      }
    }

    // Receive data from all buffers
    const int n_buffers = buffers.size();    // Number of buffers
    #pragma omp parallel for
    for (int i = 0; i < n_buffers; ++i)
      mpi_recv(buffers[i]);                  // Helper to receive data

    // Update global photon counts
    n_escaped = ind_esc[n_ranks] - ind_esc[0];
    n_absorbed = ind_abs[n_ranks] - ind_abs[0];
  } else {
    // Send escaped photon info
    if (n_escaped > 0) {
      MPI_Send(source_ids.data(), n_escaped, MPI_INT, ROOT, TAG_EI0, MPI_COMM_WORLD);
      if (n_source_types > 1) MPI_Send(source_types.data(), n_escaped, MPI_INT, ROOT, TAG_ET0, MPI_COMM_WORLD);
      MPI_Send(source_weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EW0, MPI_COMM_WORLD);
      if (output_n_scat) MPI_Send(n_scats.data(), n_escaped, MPI_INT, ROOT, TAG_ENS, MPI_COMM_WORLD);
      if (output_collisions) MPI_Send(f_cols.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EC, MPI_COMM_WORLD);
      MPI_Send(freqs.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EF, MPI_COMM_WORLD);
      MPI_Send(weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EW, MPI_COMM_WORLD);
      if (output_source_position) MPI_Send(source_positions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_EP0, MPI_COMM_WORLD);
      MPI_Send(positions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_EP, MPI_COMM_WORLD);
      MPI_Send(directions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_ED, MPI_COMM_WORLD);
      if (output_path_length) MPI_Send(path_lengths.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EL, MPI_COMM_WORLD);
    }

    // Send absorbed photon info
    if (n_absorbed > 0) {
      MPI_Send(&source_ids[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AI0, MPI_COMM_WORLD);
      if (n_source_types > 1) MPI_Send(&source_types[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AT0, MPI_COMM_WORLD);
      MPI_Send(&source_weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AW0, MPI_COMM_WORLD);
      if (output_n_scat) MPI_Send(&n_scats[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_ANS, MPI_COMM_WORLD);
      if (output_collisions) MPI_Send(&f_cols[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AC, MPI_COMM_WORLD);
      MPI_Send(&freqs[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AF, MPI_COMM_WORLD);
      MPI_Send(&weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AW, MPI_COMM_WORLD);
      if (output_source_position) MPI_Send(&source_positions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AP0, MPI_COMM_WORLD);
      MPI_Send(&positions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AP, MPI_COMM_WORLD);
      MPI_Send(&directions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AD, MPI_COMM_WORLD);
      if (output_path_length) MPI_Send(&path_lengths[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AL, MPI_COMM_WORLD);
    }
  }
}

/* MPI reductions of all output data. */
static void reduce_mcrt() {
  if (root)
    cout << "\nMCRT: MPI Reductions ..." << endl;

  // If we do not synchronize all ranks, the reductions may time out
  MPI_Barrier(MPI_COMM_WORLD);

  reduce_timer.start();

  // Global statistics
  mpi_reduce(f_src);                         // Global source fraction: sum(w0)
  mpi_reduce(f2_src);                        // Global squared f_src: sum(w0^2)
  mpi_reduce(f_esc);                         // Global escape fraction: sum(w)
  mpi_reduce(f2_esc);                        // Global squared f_esc: sum(w^2)
  mpi_reduce(freq_avg);                      // Global average frequency
  mpi_reduce(freq_std);                      // Global standard deviation
  mpi_reduce(freq_skew);                     // Global frequency skewness
  mpi_reduce(freq_kurt);                     // Global frequency kurtosis

  // Angle-averaged flux [erg/s/cm^2/angstrom]
  if (output_flux_avg)
    mpi_reduce(flux_avg);                    // Apply flux_avg reductions

  // Angle-averaged group flux [erg/s/cm^2/angstrom]
  if (output_flux_avg_grp)
    mpi_reduce(flux_avg_grp);                // Apply flux_avg_grp reductions

  // Angle-averaged subhalo flux [erg/s/cm^2/angstrom]
  if (output_flux_avg_sub)
    mpi_reduce(flux_avg_sub);                // Apply flux_avg_sub reductions

  // Angle-averaged radial surface brightness [erg/s/cm^2/arcsec^2]
  if (output_radial_avg)
    mpi_reduce(radial_avg);                  // Apply radial_avg reductions

  // Angle-averaged group radial surface brightness [erg/s/cm^2/arcsec^2]
  if (output_radial_avg_grp)
    mpi_reduce(radial_avg_grp);              // Apply radial_avg_grp reductions

  // Angle-averaged subhalo radial surface brightness [erg/s/cm^2/arcsec^2]
  if (output_radial_avg_sub)
    mpi_reduce(radial_avg_sub);              // Apply radial_avg_sub reductions

  // Angle-averaged radial spectral data cube [erg/s/cm^2/arcsec^2/angstrom]
  if (output_radial_cube_avg)
    mpi_reduce(radial_cube_avg);             // Apply radial_cube_avg reductions

  // Escape fractions [fraction]
  if (output_escape_fractions)
    mpi_reduce(f_escs);                      // Apply f_esc reductions

  // Frequency averages [freq units]
  if (output_freq_avgs)
    mpi_reduce(freq_avgs);                   // Apply frequency reductions

  // Frequency standard deviations [freq units]
  if (output_freq_stds)
    mpi_reduce(freq_stds);                   // Apply deviation reductions

  // Frequency skewnesses [normalized]
  if (output_freq_skews)
    mpi_reduce(freq_skews);                  // Apply skewness reductions

  // Frequency kurtoses [normalized]
  if (output_freq_kurts)
    mpi_reduce(freq_kurts);                  // Apply kurtosis reductions

  // Spectral fluxes [erg/s/cm^2/angstrom]
  if (output_fluxes)
    mpi_reduce(fluxes);                      // Apply flux reductions

  // Surface brightness images [erg/s/cm^2/arcsec^2]
  if (output_images)
    mpi_reduce(images);                      // Apply image reductions

  // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
  if (output_images2)
    mpi_reduce(images2);                     // Apply image2 reductions

  // Average frequency images [freq units]
  if (output_freq_images)
    mpi_reduce(freq_images);                 // Apply freq_images reductions

  // Frequency^2 images [freq^2 units]
  if (output_freq2_images)
    mpi_reduce(freq2_images);                // Apply freq2_images reductions

  // Frequency^3 images [freq^3 units]
  if (output_freq3_images)
    mpi_reduce(freq3_images);                // Apply freq3_images reductions

  // Frequency^4 images [freq^4 units]
  if (output_freq4_images)
    mpi_reduce(freq4_images);                // Apply freq4_images reductions

  // Flux-weighted frequency RGB images
  if (output_rgb_images)
    mpi_reduce(rgb_images);                  // Apply rgb_image reductions

  // Spectral slits [erg/s/cm^2/arcsec^2/angstrom]
  if (output_slits) {
    mpi_reduce(slits_x);
    mpi_reduce(slits_y);
  }

  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_cubes)
    mpi_reduce(cubes);                       // Apply cube reductions

  // Radial surface brightness images [erg/s/cm^2/arcsec^2]
  if (output_radial_images)
    mpi_reduce(radial_images);               // Apply radial_image reductions

  // Statistical moment radial images [erg^2/s^2/cm^4/arcsec^4]
  if (output_radial_images2)
    mpi_reduce(radial_images2);              // Apply radial_image2 reductions

  // Average frequency radial images [freq units]
  if (output_freq_radial_images)
    mpi_reduce(freq_radial_images);          // Apply freq_radial_images reductions

  // Frequency^2 radial images [freq^2 units]
  if (output_freq2_radial_images)
    mpi_reduce(freq2_radial_images);         // Apply freq2_radial_images reductions

  // Frequency^3 radial images [freq^3 units]
  if (output_freq3_radial_images)
    mpi_reduce(freq3_radial_images);         // Apply freq3_radial_images reductions

  // Frequency^4 radial_images [freq^4 units]
  if (output_freq4_radial_images)
    mpi_reduce(freq4_radial_images);         // Apply freq4_radial_images reductions

  // Flux-weighted frequency RGB radial images
  if (output_rgb_radial_images)
    mpi_reduce(rgb_radial_images);           // Apply rgb_radial_image reductions

  // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_radial_cubes)
    mpi_reduce(radial_cubes);                // Apply radial_cube reductions

  // Line-of-sight healpix maps
  if (output_map) {
    mpi_reduce(map);                         // Apply map reductions

    if (output_map2)
      mpi_reduce(map2);                      // Apply map2 reductions

    if (output_freq_map) {
      mpi_reduce(freq_map);                  // Apply freq_map reductions

      if (output_freq2_map) {
        mpi_reduce(freq2_map);               // Apply freq2_map reductions

        if (output_freq3_map) {
          mpi_reduce(freq3_map);             // Apply freq3_map reductions

          if (output_freq4_map)
            mpi_reduce(freq4_map);           // Apply freq4_map reductions
        }
      }
    }

    if (output_rgb_map)
      mpi_reduce(rgb_map);                   // Apply rgb_map reductions
  }

  // Line-of-sight healpix group maps
  if (output_map_grp) {
    if (output_grp_obs)
      mpi_reduce(map_grp);                   // Apply map reductions
    if (output_grp_vir)
      mpi_reduce(map_grp_vir);               // Apply map_grp_vir reductions
    if (output_grp_gal)
      mpi_reduce(map_grp_gal);               // Apply map_grp_gal reductions

    if (output_map2_grp) {
      if (output_grp_obs)
        mpi_reduce(map2_grp);                // Apply map2 reductions
      if (output_grp_vir)
        mpi_reduce(map2_grp_vir);            // Apply map2_grp_vir reductions
      if (output_grp_gal)
        mpi_reduce(map2_grp_gal);            // Apply map2_grp_gal reductions
    }

    if (output_freq_map_grp) {
      mpi_reduce(freq_map_grp);              // Apply freq_map_grp reductions

      if (output_freq2_map_grp) {
        mpi_reduce(freq2_map_grp);           // Apply freq2_map_grp reductions

        if (output_freq3_map_grp) {
          mpi_reduce(freq3_map_grp);         // Apply freq3_map_grp reductions

          if (output_freq4_map_grp)
            mpi_reduce(freq4_map_grp);       // Apply freq4_map_grp reductions
        }
      }
    }
  }

  // Line-of-sight healpix subhalo maps
  if (output_map_sub) {
    if (output_sub_obs)
      mpi_reduce(map_sub);                   // Apply map reductions
    if (output_sub_vir)
      mpi_reduce(map_sub_vir);               // Apply map_sub_vir reductions
    if (output_sub_gal)
      mpi_reduce(map_sub_gal);               // Apply map_sub_gal reductions

    if (output_map2_sub) {
      if (output_sub_obs)
        mpi_reduce(map2_sub);                // Apply map2 reductions
      if (output_sub_vir)
        mpi_reduce(map2_sub_vir);            // Apply map2_sub_vir reductions
      if (output_sub_gal)
        mpi_reduce(map2_sub_gal);            // Apply map2_sub_gal reductions
    }

    if (output_freq_map_sub) {
      mpi_reduce(freq_map_sub);              // Apply freq_map_sub reductions

      if (output_freq2_map_sub) {
        mpi_reduce(freq2_map_sub);           // Apply freq2_map_sub reductions

        if (output_freq3_map_sub) {
          mpi_reduce(freq3_map_sub);         // Apply freq3_map_sub reductions

          if (output_freq4_map_sub)
            mpi_reduce(freq4_map_sub);       // Apply freq4_map_sub reductions
        }
      }
    }
  }

  // Line-of-sight healpix flux map
  if (output_flux_map)
    mpi_reduce(flux_map);                    // Apply flux_map reductions

  // Line-of-sight healpix flux map
  if (output_flux_map_grp)
    mpi_reduce(flux_map_grp);                // Apply flux_map_grp reductions

  // Line-of-sight healpix flux map
  if (output_flux_map_sub)
    mpi_reduce(flux_map_sub);                // Apply flux_map_sub reductions

  // Angle-averaged spectral flux
  if (output_flux_avg_grp)
    mpi_reduce(flux_avg_grp);                // Apply flux_avg_grp reductions

  // Angle-averaged spectral flux
  if (output_flux_avg_sub)
    mpi_reduce(flux_avg_sub);                // Apply flux_avg_sub reductions

  // Line-of-sight healpix radial map
  if (output_radial_map)
    mpi_reduce(radial_map);                  // Apply radial_map reductions

  // Line-of-sight healpix radial map
  if (output_radial_map_grp)
    mpi_reduce(radial_map);                  // Apply radial_map reductions

  // Line-of-sight healpix radial map
  if (output_radial_map_sub)
    mpi_reduce(radial_map_sub);              // Apply radial_map reductions

  // Line-of-sight healpix radial spectral map
  if (output_cube_map)
    mpi_reduce(cube_map);                    // Apply cube_map reductions

  // Line-of-sight angular cosine (μ=cosθ) map
  if (output_mu) {
    mpi_reduce(mu1);                         // Apply map reductions

    if (output_mu2)
      mpi_reduce(map2);                      // Apply map2 reductions

    if (output_freq_mu) {
      mpi_reduce(freq_mu);                   // Apply freq_mu reductions

      if (output_freq2_mu) {
        mpi_reduce(freq2_mu);                // Apply freq2_mu reductions

        if (output_freq3_mu) {
          mpi_reduce(freq3_mu);              // Apply freq3_mu reductions

          if (output_freq4_mu)
            mpi_reduce(freq4_mu);            // Apply freq4_mu reductions
        }
      }
    }
  }

  // Line-of-sight angular cosine (μ=cosθ) flux map
  if (output_flux_mu)
    mpi_reduce(flux_mu);                     // Apply flux_mu reductions

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Frequency averages [freq units]
    if (output_freq_avgs)
      mpi_reduce(freq_avgs_int);             // Apply frequency reductions

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      mpi_reduce(fluxes_int);                // Apply flux reductions

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      mpi_reduce(images_int);                // Apply image reductions

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      mpi_reduce(cubes_int);                 // Apply cube reductions

    // Radial surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_radial_images)
      mpi_reduce(radial_images_int);         // Apply radial_image reductions

    // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_radial_cubes)
      mpi_reduce(radial_cubes_int);          // Apply radial_cube reductions
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Escape fractions [fraction]
    if (output_escape_fractions)
      mpi_reduce(f_escs_ext);                // Apply f_esc reductions

    // Frequency averages [freq units]
    if (output_freq_avgs)
      mpi_reduce(freq_avgs_ext);             // Apply frequency reductions

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      mpi_reduce(fluxes_ext);                // Apply flux reductions

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      mpi_reduce(images_ext);                // Apply image reductions

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      mpi_reduce(cubes_ext);                 // Apply cube reductions

    // Radial surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_radial_images)
      mpi_reduce(radial_images_ext);         // Apply radial_image reductions

    // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_radial_cubes)
      mpi_reduce(radial_cubes_ext);          // Apply radial_cube reductions
  }

  // Intrinsic cameras (proj) surface brightness images [erg/s/cm^2]
  if (output_proj_emission)
    mpi_reduce_disjoint(proj_images_int);    // Apply image reductions

  // Attenuation cameras (proj) surface brightness images [erg/s/cm^2]
  if (output_proj_attenuation)
    mpi_reduce_disjoint(proj_images_ext);    // Apply image reductions

  // Intrinsic cameras (proj) spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_proj_cube_emission)
    mpi_reduce_disjoint(proj_cubes_int);     // Apply cube reductions

  // Attenuation cameras (proj) spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_proj_cube_attenuation)
    mpi_reduce_disjoint(proj_cubes_ext);     // Apply cube reductions

  // Collisional excitation camera data
  if (output_collisions) {
    // Global statistics
    mpi_reduce(f_src_col);                   // Global source fraction: sum(w0)
    mpi_reduce(f2_src_col);                  // Global squared f_src_col: sum(w0^2)
    mpi_reduce(f_esc_col);                   // Global escape fraction: sum(w)
    mpi_reduce(f2_esc_col);                  // Global squared f_esc_col: sum(w^2)
    mpi_reduce(freq_avg_col);                // Global average frequency
    mpi_reduce(freq_std_col);                // Global standard deviation
    mpi_reduce(freq_skew_col);               // Global frequency skewness
    mpi_reduce(freq_kurt_col);               // Global frequency kurtosis

    // Escape fractions [fraction]
    if (output_escape_fractions)
      mpi_reduce(f_escs_col);                // Apply f_esc reductions

    // Frequency averages [freq units]
    if (output_freq_avgs)
      mpi_reduce(freq_avgs_col);             // Apply frequency reductions

    // Frequency standard deviations [freq units]
    if (output_freq_stds)
      mpi_reduce(freq_stds_col);             // Apply deviation reductions

    // Frequency skewnesses [normalized]
    if (output_freq_skews)
      mpi_reduce(freq_skews_col);            // Apply skewness reductions

    // Frequency kurtoses [normalized]
    if (output_freq_kurts)
      mpi_reduce(freq_kurts_col);            // Apply kurtosis reductions

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      mpi_reduce(fluxes_col);                // Apply flux reductions

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      mpi_reduce(images_col);                // Apply image reductions

    // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
    if (output_images2)
      mpi_reduce(images2_col);               // Apply image2 reductions

    // Average frequency images [freq units]
    if (output_freq_images)
      mpi_reduce(freq_images_col);           // Apply freq_images reductions

    // Frequency^2 images [freq^2 units]
    if (output_freq2_images)
      mpi_reduce(freq2_images_col);          // Apply freq2_images reductions

    // Frequency^3 images [freq^3 units]
    if (output_freq3_images)
      mpi_reduce(freq3_images_col);          // Apply freq3_images reductions

    // Frequency^4 images [freq^4 units]
    if (output_freq4_images)
      mpi_reduce(freq4_images_col);          // Apply freq4_images reductions

    // Intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      // Frequency averages [freq units]
      if (output_freq_avgs)
        mpi_reduce(freq_avgs_int_col);       // Apply frequency reductions

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        mpi_reduce(fluxes_int_col);          // Apply flux reductions

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        mpi_reduce(images_int_col);          // Apply image reductions
    }

    // Attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      // Escape fractions [fraction]
      if (output_escape_fractions)
        mpi_reduce(f_escs_ext_col);          // Apply f_esc reductions

      // Frequency averages [freq units]
      if (output_freq_avgs)
        mpi_reduce(freq_avgs_ext_col);       // Apply frequency reductions

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        mpi_reduce(fluxes_ext_col);          // Apply flux reductions

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        mpi_reduce(images_ext_col);          // Apply image reductions
    }

    // Intrinsic cameras (proj) surface brightness images [erg/s/cm^2]
    if (output_proj_emission)
      mpi_reduce_disjoint(proj_images_int_col); // Apply image reductions

    // Attenuation cameras (proj) surface brightness images [erg/s/cm^2]
    if (output_proj_attenuation)
      mpi_reduce_disjoint(proj_images_ext_col); // Apply image reductions

    // Intrinsic cameras (proj) spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_proj_cube_emission)
      mpi_reduce_disjoint(proj_cubes_int_col); // Apply cube reductions

    // Attenuation cameras (proj) spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_proj_cube_attenuation)
      mpi_reduce_disjoint(proj_cubes_ext_col); // Apply cube reductions
  }

  // Absorbed and escaped photon info
  if (output_photons)
    mpi_collect_photons();                   // Collect photons to root

  // Radiation energy density [erg/cm^3], acceleration [cm/s^2], and pressure [erg/cm^3]
  if constexpr (output_energy_density)
    mpi_reduce(u_rad);                       // Radiation energy density
  if constexpr (SPHERICAL) {
    if constexpr (output_acceleration)
      mpi_reduce(a_rad_r);                   // Radiation acceleration
    if constexpr (output_acceleration_scat)
      mpi_reduce(a_rad_r_scat);              // Scattering-based acceleration
    if constexpr (output_pressure)
      mpi_reduce(P_rad_r);                   // Radiation pressure
    if constexpr (output_f_src_r)
      mpi_reduce(f_src_r);                   // Cumulative source fraction
    if constexpr (output_tau_dust_f_esc)
      mpi_reduce(tau_dust_f_esc);            // Cumulative dust optical depth
    if constexpr (output_f_esc_r)
      mpi_reduce(f_esc_r);                   // Cumulative escape fraction
    if constexpr (output_rescaled_f_esc_r)
      mpi_reduce(rescaled_f_esc_r);          // Cumulative rescaled escape fraction
    if constexpr (output_trap_r)
      mpi_reduce(trap_r);                    // Cumulative t_trap / t_light
    if constexpr (output_M_F_r)
      mpi_reduce(M_F_r);                     // Cumulative force multiplier
    if constexpr (output_M_F_r_scat)
      mpi_reduce(M_F_r_scat);                // Cumulative force multiplier (scattering-based)
    if constexpr (output_M_F_src)
      mpi_reduce(M_F_src);                   // Cumulative source force multiplier (by source)
    if constexpr (output_M_F_src_scat)
      mpi_reduce(M_F_src_scat);              // Cumulative source force multiplier (by source, scattering-based)
    if constexpr (output_P_u_r)
      mpi_reduce(P_u_r);                     // Cumulative pressure-to-energy density (P/u)
  } else {
    if constexpr (output_acceleration)
      mpi_reduce(a_rad);                     // Radiation acceleration
    if constexpr (output_acceleration_scat)
      mpi_reduce(a_rad_scat);                // Scattering-based acceleration
  }

  // Radial flow statistics [photons/s]
  if constexpr (output_radial_flow) {
    mpi_reduce(radial_line_flow.src);
    mpi_reduce(radial_line_flow.esc);
    mpi_reduce(radial_line_flow.abs);
    mpi_reduce(radial_line_flow.pass);
    mpi_reduce(radial_line_flow.flow);
  }
  // Group flow statistics [photons/s]
  if constexpr (output_group_flows)
    for (auto& group_line_flow : group_line_flows) {
      mpi_reduce(group_line_flow.src);
      mpi_reduce(group_line_flow.esc);
      mpi_reduce(group_line_flow.abs);
      mpi_reduce(group_line_flow.pass);
      mpi_reduce(group_line_flow.flow);
    }
  // Subhalo flow statistics [photons/s]
  if constexpr (output_subhalo_flows)
    for (auto& subhalo_line_flow : subhalo_line_flows) {
      mpi_reduce(subhalo_line_flow.src);
      mpi_reduce(subhalo_line_flow.esc);
      mpi_reduce(subhalo_line_flow.abs);
      mpi_reduce(subhalo_line_flow.pass);
      mpi_reduce(subhalo_line_flow.flow);
    }
  reduce_timer.stop();
}

/* Convert observables to the correct units. */
void MCRT::correct_units() {
  cout << "\nMCRT: Converting to physical units ..." << endl;

  double to_flux_units, to_image_units, to_slit_units, to_cube_units, to_map_flux_units,
    to_map_flux_grp_units, to_map_flux_sub_units, to_avg_flux_grp_units, to_avg_flux_sub_units, to_mu_flux_units;
  if (cosmological) {
    const double z2a = (1. + z) * (1. + z) * arcsec; // (1 + z)^2 * arcsec
    const double z4a2 = z2a * z2a;           // (1 + z)^4 * arcsec^2
    to_flux_units = L_tot / (2. * M_PI * d_L * d_L * observed_bin_width); // L / (2π d_L^2 Δλ_obs)
    to_image_units = L_tot / (2. * M_PI * pixel_area * z4a2); // L / (2π dθ^2)
    to_slit_units = L_tot / (2. * M_PI * slit_pixel_area * z4a2 * observed_slit_bin_width); // L / (2π dθ^2 Δλ_obs)
    to_cube_units = L_tot / (2. * M_PI * cube_pixel_area * z4a2 * observed_cube_bin_width); // L / (2π dθ^2 Δλ_obs)
    const double n_pix_flux = 12 * n_side_flux * n_side_flux; // Number of pixels
    to_map_flux_units = n_pix_flux * L_tot / (4. * M_PI * d_L * d_L * observed_map_bin_width); // L / (4π d_L^2 Δλ_obs)
    const double n_pix_flux_grp = 12 * n_side_flux_grp * n_side_flux_grp; // Number of pixels
    to_map_flux_grp_units = n_pix_flux_grp * L_tot / (4. * M_PI * d_L * d_L * observed_map_bin_width_grp); // L / (4π d_L^2 Δλ_obs)
    const double n_pix_flux_sub = 12 * n_side_flux_sub * n_side_flux_sub; // Number of pixels
    to_map_flux_sub_units = n_pix_flux_sub * L_tot / (4. * M_PI * d_L * d_L * observed_map_bin_width_sub); // L / (4π d_L^2 Δλ_obs)
    to_avg_flux_grp_units = L_tot / (4. * M_PI * d_L * d_L * observed_bin_width_grp); // L / (4π d_L^2 Δλ_obs)
    to_avg_flux_sub_units = L_tot / (4. * M_PI * d_L * d_L * observed_bin_width_sub); // L / (4π d_L^2 Δλ_obs)
    const double n_mu_d = n_mu;              // Number of mu bins
    to_mu_flux_units = n_mu_d * L_tot / (4. * M_PI * d_L * d_L * observed_mu_bin_width); // L / (4π d_L^2 Δλ_obs)
  } else {
    to_flux_units = 2. * L_tot / freq_bin_width; // 2 L / Δv
    to_image_units = 2. * L_tot / pixel_area; // 2 L / dA
    to_slit_units = 2. * L_tot / (slit_pixel_area * slit_freq_bin_width); // 2 L / (dA Δv)
    to_cube_units = 2. * L_tot / (cube_pixel_area * cube_freq_bin_width); // 2 L / (dA Δv)
    const double n_pix_flux = 12 * n_side_flux * n_side_flux; // Number of pixels
    to_map_flux_units = n_pix_flux * L_tot / map_freq_bin_width; // L / Δv
    const double n_pix_flux_grp = 12 * n_side_flux_grp * n_side_flux_grp; // Number of pixels
    to_map_flux_grp_units = n_pix_flux_grp * L_tot / map_freq_bin_width_grp; // L / Δv
    const double n_pix_flux_sub = 12 * n_side_flux_sub * n_side_flux_sub; // Number of pixels
    to_map_flux_sub_units = n_pix_flux_sub * L_tot / map_freq_bin_width_sub; // L / Δv
    to_avg_flux_grp_units = L_tot / freq_bin_width_grp; // L / Δv
    to_avg_flux_sub_units = L_tot / freq_bin_width_sub; // L / Δv
    const double n_mu_d = n_mu;              // Number of mu bins
    to_mu_flux_units = n_mu_d * L_tot / mu_freq_bin_width; // L / Δv
  }

  // Group statistics (source)
  if constexpr (output_groups) for (int i = 0; i < n_groups; ++i) {
    const double halo_factor = safe_division(L_tot, L_grp[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_grp[i] *= halo_factor;             // Group source fraction
    f2_src_grp[i] *= halo_factor_2;          // Group squared f_src
    n_photons_src_grp[i] = calc_n_eff(f_src_grp[i], f2_src_grp[i]); // Emitted: <f_src>^2 / <f_src^2>
  }
  // Group statistics (observed)
  if constexpr (output_groups) if (output_grp_obs) for (int i = 0; i < n_groups; ++i) {
    const double halo_factor = safe_division(L_tot, L_grp[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_esc_grp[i] *= halo_factor;             // Group escape fraction
    f2_esc_grp[i] *= halo_factor_2;          // Group squared f_esc
    n_photons_esc_grp[i] = calc_n_eff(f_esc_grp[i], f2_esc_grp[i]); // Escaped: <f_esc>^2 / <f_esc^2>
  }
  // Group statistics (virial radius)
  if constexpr (output_groups) if (output_grp_vir) for (int i = 0; i < n_groups; ++i) {
    const double halo_factor = safe_division(L_tot, L_grp_vir[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_grp_vir[i] *= halo_factor;         // Group source fraction
    f2_src_grp_vir[i] *= halo_factor_2;      // Group squared f_src
    n_photons_src_grp_vir[i] = calc_n_eff(f_src_grp_vir[i], f2_src_grp_vir[i]); // Emitted: <f_src>^2 / <f_src^2>
    f_esc_grp_vir[i] *= halo_factor;         // Group escape fraction
    f2_esc_grp_vir[i] *= halo_factor_2;      // Group squared f_esc
    n_photons_esc_grp_vir[i] = calc_n_eff(f_esc_grp_vir[i], f2_esc_grp_vir[i]); // Escaped: <f_esc>^2 / <f_esc^2>
  }
  // Group statistics (galaxy radius)
  if constexpr (output_groups) if (output_grp_gal) for (int i = 0; i < n_groups; ++i) {
    const double halo_factor = safe_division(L_tot, L_grp_gal[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_grp_gal[i] *= halo_factor;         // Group source fraction
    f2_src_grp_gal[i] *= halo_factor_2;      // Group squared f_src
    n_photons_src_grp_gal[i] = calc_n_eff(f_src_grp_gal[i], f2_src_grp_gal[i]); // Emitted: <f_src>^2 / <f_src^2>
    f_esc_grp_gal[i] *= halo_factor;         // Group escape fraction
    f2_esc_grp_gal[i] *= halo_factor_2;      // Group squared f_esc
    n_photons_esc_grp_gal[i] = calc_n_eff(f_esc_grp_gal[i], f2_esc_grp_gal[i]); // Escaped: <f_esc>^2 / <f_esc^2>
  }
  // Unfiltered group statistics
  if constexpr (output_groups) for (int i = 0; i < n_ugroups; ++i) {
    const double halo_factor = safe_division(L_tot, L_ugrp[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_ugrp[i] *= halo_factor;            // Group source fraction
    f2_src_ugrp[i] *= halo_factor_2;         // Group squared f_src
    n_photons_src_ugrp[i] = calc_n_eff(f_src_ugrp[i], f2_src_ugrp[i]); // Emitted: <f_src>^2 / <f_src^2>
  }

  // Subhalo statistics (source)
  if constexpr (output_subhalos) for (int i = 0; i < n_subhalos; ++i) {
    const double halo_factor = safe_division(L_tot, L_sub[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_sub[i] *= halo_factor;             // Subhalo source fraction
    f2_src_sub[i] *= halo_factor_2;          // Subhalo squared f_src
    n_photons_src_sub[i] = calc_n_eff(f_src_sub[i], f2_src_sub[i]); // Emitted: <f_src>^2 / <f_src^2>
  }
  // Subhalo statistics (observed)
  if constexpr (output_subhalos) if (output_sub_obs) for (int i = 0; i < n_subhalos; ++i) {
    const double halo_factor = safe_division(L_tot, L_sub[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_esc_sub[i] *= halo_factor;             // Subhalo escape fraction
    f2_esc_sub[i] *= halo_factor_2;          // Subhalo squared f_esc
    n_photons_esc_sub[i] = calc_n_eff(f_esc_sub[i], f2_esc_sub[i]); // Escaped: <f_esc>^2 / <f_esc^2>
  }
  // Subhalo statistics (virial radius)
  if constexpr (output_subhalos) if (output_sub_vir) for (int i = 0; i < n_subhalos; ++i) {
    const double halo_factor = safe_division(L_tot, L_sub_vir[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_sub_vir[i] *= halo_factor;         // Subhalo source fraction
    f2_src_sub_vir[i] *= halo_factor_2;      // Subhalo squared f_src
    n_photons_src_sub_vir[i] = calc_n_eff(f_src_sub_vir[i], f2_src_sub_vir[i]); // Emitted: <f_src>^2 / <f_src^2>
    f_esc_sub_vir[i] *= halo_factor;         // Subhalo escape fraction
    f2_esc_sub_vir[i] *= halo_factor_2;      // Subhalo squared f_esc
    n_photons_esc_sub_vir[i] = calc_n_eff(f_esc_sub_vir[i], f2_esc_sub_vir[i]); // Escaped: <f_esc>^2 / <f_esc^2>
  }
  // Subhalo statistics (galaxy radius)
  if constexpr (output_subhalos) if (output_sub_gal) for (int i = 0; i < n_subhalos; ++i) {
    const double halo_factor = safe_division(L_tot, L_sub_gal[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_sub_gal[i] *= halo_factor;         // Subhalo source fraction
    f2_src_sub_gal[i] *= halo_factor_2;      // Subhalo squared f_src
    n_photons_src_sub_gal[i] = calc_n_eff(f_src_sub_gal[i], f2_src_sub_gal[i]); // Emitted: <f_src>^2 / <f_src^2>
    f_esc_sub_gal[i] *= halo_factor;         // Subhalo escape fraction
    f2_esc_sub_gal[i] *= halo_factor_2;      // Subhalo squared f_esc
    n_photons_esc_sub_gal[i] = calc_n_eff(f_esc_sub_gal[i], f2_esc_sub_gal[i]); // Escaped: <f_esc>^2 / <f_esc^2>
  }
  // Unfiltered subhalo statistics
  if constexpr (output_subhalos) for (int i = 0; i < n_usubhalos; ++i) {
    const double halo_factor = safe_division(L_tot, L_usub[i]);
    const double halo_factor_2 = halo_factor * halo_factor;
    f_src_usub[i] *= halo_factor;            // Group source fraction
    f2_src_usub[i] *= halo_factor_2;         // Group squared f_src
    n_photons_src_usub[i] = calc_n_eff(f_src_usub[i], f2_src_usub[i]); // Emitted: <f_src>^2 / <f_src^2>
  }

  // Set up radial image units
  vector<double> to_radial_image_units, to_radial_cube_units; // 2 L / dA
  vector<double> to_radial_map_units, to_radial_map_grp_units, to_radial_map_sub_units, to_cube_map_units; // n_pix L / dA
  if (output_radial_images || output_radial_avg) {
    to_radial_image_units = vector<double>(n_radial_pixels);
    const double to_radial_image_units_area = cosmological ?
      L_tot / (2. * M_PI * arcsec * arcsec * pow(1. + z, 4)) : 2. * L_tot; // L dA / (2π dθ^2) or 2 L
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_pixels; ++ir)
      to_radial_image_units[ir] = to_radial_image_units_area / radial_pixel_areas[ir]; // L / (2π dθ^2) or 2 L / dA
  }
  if (output_radial_cubes || output_radial_cube_avg) {
    to_radial_cube_units = vector<double>(n_radial_cube_pixels);
    const double to_radial_cube_units_area = cosmological ?
      L_tot / (2. * M_PI * arcsec * arcsec * pow(1. + z, 4) * observed_radial_cube_bin_width) :
      2. * L_tot / radial_cube_freq_bin_width; // L dA / (2π dθ^2 Δλ_obs) or 2 L / Δv
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_cube_pixels; ++ir)
      to_radial_cube_units[ir] = to_radial_cube_units_area / radial_cube_pixel_areas[ir]; // L / (2π dθ^2 Δλ_obs) or 2 L / (dA Δv)
  }
  if (output_radial_map) {
    to_radial_map_units = vector<double>(n_map_pixels);
    const double n_pix_radial = 12 * n_side_radial * n_side_radial; // Number of pixels
    const double to_radial_map_units_area = n_pix_radial * (cosmological ?
      L_tot / (4. * M_PI * arcsec * arcsec * pow(1. + z, 4)) : L_tot); // L dA / (4π dθ^2) or L
    #pragma omp parallel for
    for (int ir = 0; ir < n_map_pixels; ++ir)
      to_radial_map_units[ir] = to_radial_map_units_area / map_pixel_areas[ir]; // L / (2π dθ^2) or 2 L / dA
  }
  if (output_radial_map_grp) {
    to_radial_map_grp_units = vector<double>(n_map_pixels_grp);
    const double n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // Number of pixels
    const double to_radial_map_grp_units_area = n_pix_radial_grp * (cosmological ?
      L_tot / (4. * M_PI * arcsec * arcsec * pow(1. + z, 4)) : L_tot); // L dA / (4π dθ^2) or L
    #pragma omp parallel for
    for (int ir = 0; ir < n_map_pixels_grp; ++ir)
      to_radial_map_grp_units[ir] = to_radial_map_grp_units_area / map_pixel_areas_grp[ir]; // L / (2π dθ^2) or 2 L / dA
  }
  if (output_radial_map_sub) {
    to_radial_map_sub_units = vector<double>(n_map_pixels_sub);
    const double n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // Number of pixels
    const double to_radial_map_sub_units_area = n_pix_radial_sub * (cosmological ?
      L_tot / (4. * M_PI * arcsec * arcsec * pow(1. + z, 4)) : L_tot); // L dA / (4π dθ^2) or L
    #pragma omp parallel for
    for (int ir = 0; ir < n_map_pixels_sub; ++ir)
      to_radial_map_sub_units[ir] = to_radial_map_sub_units_area / map_pixel_areas_sub[ir]; // L / (2π dθ^2) or 2 L / dA
  }
  if (output_cube_map) {
    to_cube_map_units = vector<double>(n_cube_map_pixels);
    const double n_pix_cube = 12 * n_side_cube * n_side_cube; // Number of pixels
    const double to_cube_map_units_area = n_pix_cube * (cosmological ?
      L_tot / (4. * M_PI * arcsec * arcsec * pow(1. + z, 4) * observed_cube_map_bin_width) :
      L_tot / cube_map_freq_bin_width);      // L dA / (4π dθ^2 Δλ_obs) or L / Δv
    #pragma omp parallel for
    for (int ir = 0; ir < n_cube_map_pixels; ++ir)
      to_cube_map_units[ir] = to_cube_map_units_area / cube_map_pixel_areas[ir]; // L / (2π dθ^2 Δλ_obs) or 2 L / (dA Δv)
  }
  if (output_radial_avg_grp) {
    const double to_radial_grp_units_area = cosmological ? L_tot / (4. * M_PI * arcsec * arcsec * pow(1. + z, 4)) : L_tot; // L dA / (4π dθ^2) or L
    #pragma omp parallel for
    for (int ir = 0; ir < n_pixels_grp; ++ir)
      radial_avg_grp[ir] *= to_radial_grp_units_area / pixel_areas_grp[ir]; // L / (2π dθ^2) or 2 L / dA
  }
  if (output_radial_avg_sub) {
    const double to_radial_sub_units_area = cosmological ? L_tot / (4. * M_PI * arcsec * arcsec * pow(1. + z, 4)) : L_tot; // L dA / (4π dθ^2) or L
    #pragma omp parallel for
    for (int ir = 0; ir < n_pixels_sub; ++ir)
      radial_avg_sub[ir] *= to_radial_sub_units_area / pixel_areas_sub[ir]; // L / (2π dθ^2) or 2 L / dA
  }

  // Effective number of photon statistics
  n_photons_src = calc_n_eff(f_src, f2_src); // Emitted: <f_src>^2 / <f_src^2>
  n_photons_esc = calc_n_eff(f_esc, f2_esc); // Escaped: <f_esc>^2 / <f_esc^2>

  // Convert global frequency moments to statistics
  if (f_esc > 0.) {                          // Avoid division by zero
    // Frequency average [freq units]
    const double f_avg = freq_avg / f_esc;   // Save for later
    freq_avg = f_avg;                        // Average

    // Frequency standard deviation [freq units]
    const double f_E2 = freq_std / f_esc;    // Normalized 2nd moment
    const double f_avg2 = f_avg * f_avg;     // Average^2
    const double f_std2 = f_E2 - f_avg2;     // Variance
    const double f_std = sqrt(f_std2);       // Standard deviation
    freq_std = f_std;

    // Frequency skewness [normalized]
    const double f_E3 = freq_skew / f_esc;   // Normalized 3rd moment
    const double f_avg3 = f_avg2 * f_avg;    // Average^3
    const double f_std3 = f_std2 * f_std;    // Deviation^3
    const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
    freq_skew = f_skew_std3 / f_std3;        // Skewness

    // Frequency kurtosis [normalized]
    const double f_E4 = freq_kurt / f_esc;   // Normalized 4th moment
    const double f_avg4 = f_avg2 * f_avg2;   // Average^4
    const double f_std4 = f_std2 * f_std2;   // Deviation^4
    const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
    freq_kurt = f_kurt_std4 / f_std4;        // Kurtosis
  }

  // Angle-averaged flux [erg/s/cm^2/angstrom]
  if (output_flux_avg)
    rescale(flux_avg, 0.5 * to_flux_units);  // Apply flux_avg correction

  // Angle-averaged radial surface brightness [erg/s/cm^2/arcsec^2]
  if (output_radial_avg) {
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_pixels; ++ir)
      radial_avg[ir] *= 0.5 * to_radial_image_units[ir];
  }

  // Angle-averaged radial spectral data cube [erg/s/cm^2/arcsec^2/angstrom]
  if (output_radial_cube_avg) {
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
      for (int bin = 0; bin < n_radial_cube_bins; ++bin)
        radial_cube_avg(ir, bin) *= 0.5 * to_radial_cube_units[ir];
    }
  }

  // Convert frequency moments to statistics // Note: Before f_esc correction
  if (output_freq_avgs)
    #pragma omp parallel for
    for (int i = 0; i < n_cameras; ++i) {
      if (f_escs[i] <= 0.)
        continue;                            // Avoid division by zero

      // Frequency averages [freq units]
      const double f_avg = freq_avgs[i] / f_escs[i]; // Save for later
      freq_avgs[i] = f_avg;                  // Average

      // Frequency standard deviations [freq units]
      if (output_freq_stds) {
        const double f_E2 = freq_stds[i] / f_escs[i]; // Normalized 2nd moment
        const double f_avg2 = f_avg * f_avg; // Average^2
        const double f_std2 = f_E2 - f_avg2; // Variance
        const double f_std = sqrt(f_std2);   // Standard deviation
        freq_stds[i] = f_std;

        // Frequency skewnesses [normalized]
        if (output_freq_skews) {
          const double f_E3 = freq_skews[i] / f_escs[i]; // Normalized 3rd moment
          const double f_avg3 = f_avg2 * f_avg; // Average^3
          const double f_std3 = f_std2 * f_std; // Deviation^3
          const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
          freq_skews[i] = f_skew_std3 / f_std3; // Skewness

          // Frequency kurtoses [normalized]
          if (output_freq_kurts) {
            const double f_E4 = freq_kurts[i] / f_escs[i]; // Normalized 4th moment
            const double f_avg4 = f_avg2 * f_avg2; // Average^4
            const double f_std4 = f_std2 * f_std2; // Deviation^4
            const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
            freq_kurts[i] = f_kurt_std4 / f_std4; // Kurtosis
          }
        }
      }
    }

  // Escape fractions [fraction]
  if (output_escape_fractions)
    rescale(f_escs, 2.);                     // Apply phase function correction

  // Spectral fluxes [erg/s/cm^2/angstrom]
  if (output_fluxes)
    rescale(fluxes, to_flux_units);          // Apply flux correction

  // Average frequency images [freq units]   // Note: Before image correction
  if (output_freq_images)
    divide(freq_images, images);             // Apply freq_image correction

  // Frequency^2 images [freq^2 units]
  if (output_freq2_images)
    divide(freq2_images, images);            // Apply freq2_image correction

  // Frequency^3 images [freq^3 units]
  if (output_freq3_images)
    divide(freq3_images, images);            // Apply freq3_image correction

  // Frequency^4 images [freq^4 units]
  if (output_freq4_images)
    divide(freq4_images, images);            // Apply freq4_image correction

  // Flux-weighted frequency RGB images      // Note: Before image correction
  if (output_rgb_images)
    divide(rgb_images, images);              // Apply rgb_image correction

  // Surface brightness images [erg/s/cm^2/arcsec^2]
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
  if (output_images2)
    rescale(images2, to_image_units * to_image_units); // Apply image2 correction

  // Spectral slits [erg/s/cm^2/arcsec^2/angstrom]
  if (output_slits) {
    rescale(slits_x, to_slit_units);         // Apply slit correction
    rescale(slits_y, to_slit_units);
  }

  // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_cubes)
    rescale(cubes, to_cube_units);           // Apply cube correction

  // Average frequency radial images [freq units] // Note: Before radial image correction
  if (output_freq_radial_images)
    divide(freq_radial_images, radial_images); // Apply freq_radial_image correction

  // Frequency^2 radial images [freq^2 units]
  if (output_freq2_radial_images)
    divide(freq2_radial_images, radial_images); // Apply freq2_radial_image correction

  // Frequency^3 radial images [freq^3 units]
  if (output_freq3_radial_images)
    divide(freq3_radial_images, radial_images); // Apply freq3_radial_image correction

  // Frequency^4 radial images [freq^4 units]
  if (output_freq4_radial_images)
    divide(freq4_radial_images, radial_images); // Apply freq4_radial_image correction

  // Flux-weighted frequency RGB radial images // Note: Before radial image correction
  if (output_rgb_radial_images)
    divide(rgb_radial_images, radial_images); // Apply rgb_radial_image correction

  // Radial surface brightness images [erg/s/cm^2/arcsec^2]
  if (output_radial_images) {
    for (auto& radial_image : radial_images) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_pixels; ++ir)
        radial_image[ir] *= to_radial_image_units[ir];
    }
  }

  // Statistical moment radial images [erg^2/s^2/cm^4/arcsec^4]
  if (output_radial_images2) {
    for (auto& radial_image2 : radial_images2) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_pixels; ++ir)
        radial_image2[ir] *= to_radial_image_units[ir] * to_radial_image_units[ir];
    }
  }

  // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
  if (output_radial_cubes) {
    for (auto& radial_cube : radial_cubes) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
        for (int bin = 0; bin < n_radial_cube_bins; ++bin)
          radial_cube(ir, bin) *= to_radial_cube_units[ir];
      }
    }
  }

  // Line-of-sight healpix maps
  if (output_map) {
    // Average frequency map [freq units]    // Note: Before map correction
    if (output_freq_map) {
      divide(freq_map, map);                 // Apply freq_map correction

      // Frequency^2 map [freq^2 units]
      if (output_freq2_map) {
        divide(freq2_map, map);              // Apply freq2_map correction

        // Frequency^3 map [freq^3 units]
        if (output_freq3_map) {
          divide(freq3_map, map);            // Apply freq3_map correction

          // Frequency^4 map [freq^4 units]
          if (output_freq4_map)
            divide(freq4_map, map);          // Apply freq4_map correction
        }
      }
    }

    // Flux-weighted frequency RGB map       // Note: Before map correction
    if (output_rgb_map)
      divide(rgb_map, map);                  // Apply rgb_map correction

    // Escape fractions [fraction]           // Note: After previous divisions
    const double n_pix_map = 12 * n_side_map * n_side_map; // Number of pixels
    rescale(map, n_pix_map);                 // Apply solid angle correction

    // Statistical moment maps
    if (output_map2)
      rescale(map2, n_pix_map * n_pix_map);  // Apply solid angle^2 correction
  }

  // Group healpix maps
  if (output_map_grp) {
    // Average frequency maps [freq units]   // Note: Before map correction
    if (output_freq_map) {
      divide(freq_map_grp, map_grp);         // Apply freq_map correction

      // Frequency^2 maps [freq^2 units]
      if (output_freq2_map) {
        divide(freq2_map_grp, map_grp);      // Apply freq2_map correction

        // Frequency^3 maps [freq^3 units]
        if (output_freq3_map) {
          divide(freq3_map_grp, map_grp);    // Apply freq3_map correction

          // Frequency^4 maps [freq^4 units]
          if (output_freq4_map)
            divide(freq4_map_grp, map_grp);  // Apply freq4_map correction
        }
      }
    }

    // Escape fractions [fraction]           // Note: After previous divisions
    const int n_pix_grp = 12 * n_side_grp * n_side_grp; // Number of pixels
    const double L_pix = L_tot * static_cast<double>(n_pix_grp); // Pixel factor
    if (output_grp_obs) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(L_pix, L_grp[i]);
        for (int j = 0; j < n_pix_grp; ++j)
          map_grp(i, j) *= halo_factor;      // Apply halo solid angle correction
      }
    }
    if (output_grp_vir) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(L_pix, L_grp_vir[i]);
        for (int j = 0; j < n_pix_grp; ++j)
          map_grp_vir(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }
    if (output_grp_gal) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(L_pix, L_grp_gal[i]);
        for (int j = 0; j < n_pix_grp; ++j)
          map_grp_gal(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }

    // Statistical moment maps
    if (output_map2) {
      const double L2_pix = L_pix * L_pix;   // Pixel factor^2
      if (output_grp_obs) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, L_grp[i] * L_grp[i]);
          for (int j = 0; j < n_pix_grp; ++j)
            map2_grp(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_grp_vir) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, L_grp_vir[i] * L_grp_vir[i]);
          for (int j = 0; j < n_pix_grp; ++j)
            map2_grp_vir(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_grp_gal) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, L_grp_gal[i] * L_grp_gal[i]);
          for (int j = 0; j < n_pix_grp; ++j)
            map2_grp_gal(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
    }
  }

  // Subhalo healpix maps
  if (output_map_sub) {
    // Average frequency maps [freq units]   // Note: Before map correction
    if (output_freq_map) {
      divide(freq_map_sub, map_sub);         // Apply freq_map correction

      // Frequency^2 maps [freq^2 units]
      if (output_freq2_map) {
        divide(freq2_map_sub, map_sub);      // Apply freq2_map correction

        // Frequency^3 maps [freq^3 units]
        if (output_freq3_map) {
          divide(freq3_map_sub, map_sub);    // Apply freq3_map correction

          // Frequency^4 maps [freq^4 units]
          if (output_freq4_map)
            divide(freq4_map_sub, map_sub);  // Apply freq4_map correction
        }
      }
    }

    // Escape fractions [fraction]           // Note: After previous divisions
    const int n_pix_sub = 12 * n_side_sub * n_side_sub; // Number of pixels
    const double L_pix = L_tot * static_cast<double>(n_pix_sub); // Pixel factor
    if (output_sub_obs) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(L_pix, L_sub[i]);
        for (int j = 0; j < n_pix_sub; ++j)
          map_sub(i, j) *= halo_factor;      // Apply halo solid angle correction
      }
    }
    if (output_sub_vir) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(L_pix, L_sub_vir[i]);
        for (int j = 0; j < n_pix_sub; ++j)
          map_sub_vir(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }
    if (output_sub_gal) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(L_pix, L_sub_gal[i]);
        for (int j = 0; j < n_pix_sub; ++j)
          map_sub_gal(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }

    // Statistical moment maps
    if (output_map2) {
      const double L2_pix = L_pix * L_pix;   // Pixel factor^2
      if (output_sub_obs) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, L_sub[i] * L_sub[i]);
          for (int j = 0; j < n_pix_sub; ++j)
            map2_sub(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_sub_vir) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, L_sub_vir[i] * L_sub_vir[i]);
          for (int j = 0; j < n_pix_sub; ++j)
            map2_sub_vir(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_sub_gal) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, L_sub_gal[i] * L_sub_gal[i]);
          for (int j = 0; j < n_pix_sub; ++j)
            map2_sub_gal(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
    }
  }

  // Line-of-sight healpix flux map
  if (output_flux_map)
    rescale(flux_map, to_map_flux_units);    // Apply flux_map correction

  // Group healpix flux map
  if (output_flux_map_grp)
    rescale(flux_map_grp, to_map_flux_grp_units); // Apply flux_map_grp correction

  // Subhalo healpix flux map
  if (output_flux_map_sub)
    rescale(flux_map_sub, to_map_flux_sub_units); // Apply flux_map_sub correction

  // Group angle-averaged spectral flux
  if (output_flux_avg_grp)
    rescale(flux_avg_grp, to_avg_flux_grp_units); // Apply flux_avg_grp correction

  // Subhalo angle-averaged spectral flux
  if (output_flux_avg_sub)
    rescale(flux_avg_sub, to_avg_flux_sub_units); // Apply flux_avg_sub correction

  // Line-of-sight healpix radial map
  if (output_radial_map) {
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // Number of pixels
    const int n_pix2 = n_pix_radial * n_map_pixels; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      radial_map[i] *= to_radial_map_units[i % n_map_pixels]; // Apply radial_map correction
  }

  // Group healpix radial map
  if (output_radial_map_grp) {
    const int n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // Number of pixels
    const int n_pix2 = n_pix_radial_grp * n_map_pixels_grp; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      radial_map_grp[i] *= to_radial_map_grp_units[i % n_map_pixels_grp]; // Apply radial_map_grp correction
  }

  // Subhalo healpix radial map
  if (output_radial_map_sub) {
    const int n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // Number of pixels
    const int n_pix2 = n_pix_radial_sub * n_map_pixels; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      radial_map_sub[i] *= to_radial_map_sub_units[i % n_map_pixels_sub]; // Apply radial_map_sub correction
  }

  // Line-of-sight healpix radial spectral map
  if (output_cube_map) {
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // Number of pixels
    const int n_pix2 = n_pix_cube * n_cube_map_pixels; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i) {
      const int imap = i / n_cube_map_pixels; // Map pixel index
      const int ir = i - imap * n_cube_map_pixels; // Radial pixel index
      for (int ibin = 0; ibin < n_cube_map_bins; ++ibin)
        cube_map(imap, ir, ibin) *= to_cube_map_units[ir]; // Apply cube_map correction
    }
  }

  // Line-of-sight angular cosine (μ=cosθ) map
  if (output_mu) {
    // Average frequency map [freq units]    // Note: Before map correction
    if (output_freq_mu) {
      divide(freq_mu, mu1);                  // Apply freq_mu correction

      // Frequency^2 map [freq^2 units]
      if (output_freq2_mu) {
        divide(freq2_mu, mu1);               // Apply freq2_mu correction

        // Frequency^3 map [freq^3 units]
        if (output_freq3_mu) {
          divide(freq3_mu, mu1);             // Apply freq3_mu correction

          // Frequency^4 map [freq^4 units]
          if (output_freq4_mu)
            divide(freq4_mu, mu1);           // Apply freq4_mu correction
        }
      }
    }

    // Escape fractions [fraction]           // Note: After previous divisions
    const double n_mu_d = n_mu;              // Number of mu bins
    rescale(mu1, n_mu_d);                    // Apply solid angle correction

    // Statistical moment maps
    if (output_mu2)
      rescale(mu2, n_mu_d * n_mu_d);         // Apply solid angle^2 correction
  }

  // Line-of-sight angular cosine (μ=cosθ) flux map
  if (output_flux_mu)
    rescale(flux_mu, to_mu_flux_units);      // Apply flux_mu correction

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Note: No correction needed for freq_avgs_int

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      rescale(fluxes_int, to_flux_units);    // Apply flux correction

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      rescale(cubes_int, to_cube_units);     // Apply cube correction

    // Radial surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_radial_images) {
      for (auto& radial_image_int : radial_images_int) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_pixels; ++ir)
          radial_image_int[ir] *= to_radial_image_units[ir];
      }
    }

    // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_radial_cubes) {
      for (auto& radial_cube_int : radial_cubes_int) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
          for (int bin = 0; bin < n_radial_cube_bins; ++bin)
            radial_cube_int(ir, bin) *= to_radial_cube_units[ir];
        }
      }
    }
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Frequency averages [freq units]       // Note: Before f_esc_ext correction
    if (output_freq_avgs)
      divide(freq_avgs_ext, f_escs_ext);     // Apply frequency correction

    // Escape fractions [fraction]
    if (output_escape_fractions)
      rescale(f_escs_ext, 2.);               // Apply phase function correction

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      rescale(fluxes_ext, to_flux_units);    // Apply flux correction

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      rescale(images_ext, to_image_units);   // Apply image correction

    // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_cubes)
      rescale(cubes_ext, to_cube_units);     // Apply cube correction

    // Radial surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_radial_images) {
      for (auto& radial_image_ext : radial_images_ext) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_pixels; ++ir)
          radial_image_ext[ir] *= to_radial_image_units[ir];
      }
    }

    // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]
    if (output_radial_cubes) {
      for (auto& radial_cube_ext : radial_cubes_ext) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
          for (int bin = 0; bin < n_radial_cube_bins; ++bin)
            radial_cube_ext(ir, bin) *= to_radial_cube_units[ir];
        }
      }
    }
  }

  // Attenuation cameras (proj)
  if (output_proj_attenuation) {
    const double undo_units = pixel_area / L_tot;
    proj_f_escs_ext = vector<double>(n_cameras); // Escape fractions
    for (int camera = 0; camera < n_cameras; ++camera) {
      const double ext_tot = proj_images_ext[camera].omp_sum();
      proj_f_escs_ext[camera] = ext_tot * undo_units; // Escape fractions
    }
  }

  // Attenuation cameras (proj cube)
  if (output_proj_cube_attenuation) {
    const double undo_units = cube_pixel_area * cube_freq_bin_width / L_tot;
    proj_cube_f_escs_ext = vector<double>(n_cameras); // Escape fractions
    for (int camera = 0; camera < n_cameras; ++camera) {
      const double ext_tot = proj_cubes_ext[camera].omp_sum();
      proj_cube_f_escs_ext[camera] = ext_tot * undo_units; // Escape fractions
    }
  }

  // Collisional excitation camera data
  if (output_collisions) {
    // Effective number of photon statistics
    n_photons_src_col = calc_n_eff(f_src_col, f2_src_col); // Emitted: <f_src_col>^2 / <f_src_col^2>
    n_photons_esc_col = calc_n_eff(f_esc_col, f2_esc_col); // Escaped: <f_esc_col>^2 / <f_esc_col^2>

    // Convert global frequency moments to statistics
    if (f_esc_col > 0.) {                    // Avoid division by zero
      // Frequency average [freq units]
      const double f_avg = freq_avg_col / f_esc_col; // Save for later
      freq_avg_col = f_avg;                  // Average

      // Frequency standard deviation [freq units]
      const double f_E2 = freq_std_col / f_esc_col; // Normalized 2nd moment
      const double f_avg2 = f_avg * f_avg;   // Average^2
      const double f_std2 = f_E2 - f_avg2;   // Variance
      const double f_std = sqrt(f_std2);     // Standard deviation
      freq_std_col = f_std;

      // Frequency skewness [normalized]
      const double f_E3 = freq_skew_col / f_esc_col; // Normalized 3rd moment
      const double f_avg3 = f_avg2 * f_avg;  // Average^3
      const double f_std3 = f_std2 * f_std;  // Deviation^3
      const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
      freq_skew_col = f_skew_std3 / f_std3;  // Skewness

      // Frequency kurtosis [normalized]
      const double f_E4 = freq_kurt_col / f_esc_col; // Normalized 4th moment
      const double f_avg4 = f_avg2 * f_avg2; // Average^4
      const double f_std4 = f_std2 * f_std2; // Deviation^4
      const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
      freq_kurt_col = f_kurt_std4 / f_std4;  // Kurtosis
    }

    // Convert frequency moments to statistics // Note: Before f_esc_col correction
    if (output_freq_avgs)
      #pragma omp parallel for
      for (int i = 0; i < n_cameras; ++i) {
        if (f_escs_col[i] <= 0.)
          continue;                          // Avoid division by zero

        // Frequency averages [freq units]
        const double f_avg = freq_avgs_col[i] / f_escs_col[i]; // Save for later
        freq_avgs_col[i] = f_avg;            // Average

        // Frequency standard deviations [freq units]
        if (output_freq_stds) {
          const double f_E2 = freq_stds_col[i] / f_escs_col[i]; // Normalized 2nd moment
          const double f_avg2 = f_avg * f_avg; // Average^2
          const double f_std2 = f_E2 - f_avg2; // Variance
          const double f_std = sqrt(f_std2); // Standard deviation
          freq_stds_col[i] = f_std;

          // Frequency skewnesses [normalized]
          if (output_freq_skews) {
            const double f_E3 = freq_skews_col[i] / f_escs_col[i]; // Normalized 3rd moment
            const double f_avg3 = f_avg2 * f_avg; // Average^3
            const double f_std3 = f_std2 * f_std; // Deviation^3
            const double f_skew_std3 = f_E3 - 3.*f_std2*f_avg - f_avg3;
            freq_skews_col[i] = f_skew_std3 / f_std3; // Skewness

            // Frequency kurtoses [normalized]
            if (output_freq_kurts) {
              const double f_E4 = freq_kurts_col[i] / f_escs_col[i]; // Normalized 4th moment
              const double f_avg4 = f_avg2 * f_avg2; // Average^4
              const double f_std4 = f_std2 * f_std2; // Deviation^4
              const double f_kurt_std4 = f_E4 - 4.*f_skew_std3*f_avg - 6.*f_std2*f_avg2 - f_avg4;
              freq_kurts_col[i] = f_kurt_std4 / f_std4; // Kurtosis
            }
          }
        }
      }

    // Escape fractions relative to collisions [fraction]
    const double col_factor = L_tot / (L_col + Lp_col); // Includes doublet emission
    f_src_col *= col_factor;                 // Global source fraction: sum(w0)
    f2_src_col *= col_factor * col_factor;   // Global squared f_src_col: sum(w0^2)
    f_esc_col *= col_factor;                 // Global escape fraction: sum(w)
    f2_esc_col *= col_factor * col_factor;   // Global squared f_esc_col: sum(w^2)
    if (output_escape_fractions)
      rescale(f_escs_col, 2. * col_factor);  // Apply phase function correction

    // Spectral fluxes [erg/s/cm^2/angstrom]
    if (output_fluxes)
      rescale(fluxes_col, to_flux_units);    // Apply flux correction

    // Average frequency images [freq units] // Note: Before image_col correction
    if (output_freq_images)
      divide(freq_images_col, images_col);   // Apply freq_image_col correction

    // Frequency^2 images [freq^2 units]
    if (output_freq2_images)
      divide(freq2_images_col, images_col);  // Apply freq2_image_col correction

    // Frequency^3 images [freq^3 units]
    if (output_freq3_images)
      divide(freq3_images_col, images_col);  // Apply freq3_image_col correction

    // Frequency^4 images [freq^4 units]
    if (output_freq4_images)
      divide(freq4_images_col, images_col);  // Apply freq4_image_col correction

    // Surface brightness images [erg/s/cm^2/arcsec^2]
    if (output_images)
      rescale(images_col, to_image_units);   // Apply image_col correction

    // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
    if (output_images2)
      rescale(images2_col, to_image_units * to_image_units); // Apply image2_col correction

    // Intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      // Frequency averages [freq units]
      if (output_freq_avgs)
        rescale(freq_avgs_int_col, col_factor); // Relative to collisions

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        rescale(fluxes_int_col, to_flux_units); // Apply flux correction

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        rescale(images_int_col, to_image_units); // Apply image correction
    }

    // Attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      // Frequency averages [freq units]     // Note: Before f_esc_ext_col correction
      if (output_freq_avgs)
        divide(freq_avgs_ext_col, f_escs_ext_col); // Apply frequency correction

      // Escape fractions [fraction]
      if (output_escape_fractions)
        rescale(f_escs_ext_col, 2. * col_factor); // Apply phase function correction

      // Spectral fluxes [erg/s/cm^2/angstrom]
      if (output_fluxes)
        rescale(fluxes_ext_col, to_flux_units); // Apply flux correction

      // Surface brightness images [erg/s/cm^2/arcsec^2]
      if (output_images)
        rescale(images_ext_col, to_image_units); // Apply image correction
    }

    // Attenuation cameras (proj)
    if (output_proj_attenuation) {
      const double undo_units = pixel_area / (L_col + Lp_col); // Includes doublet emission
      proj_f_escs_ext_col = vector<double>(n_cameras); // Escape fractions
      for (int camera = 0; camera < n_cameras; ++camera) {
        const double ext_tot = proj_images_ext_col[camera].omp_sum();
        proj_f_escs_ext_col[camera] = ext_tot * undo_units; // Escape fractions
      }
    }

    // Attenuation cameras (proj cube)
    if (output_proj_cube_attenuation) {
      const double undo_units = cube_pixel_area * cube_freq_bin_width / (L_col + Lp_col); // Includes doublet emission
      proj_cube_f_escs_ext_col = vector<double>(n_cameras); // Escape fractions
      for (int camera = 0; camera < n_cameras; ++camera) {
        const double ext_tot = proj_cubes_ext_col[camera].omp_sum();
        proj_cube_f_escs_ext_col[camera] = ext_tot * undo_units; // Escape fractions
      }
    }
  }

  if constexpr (output_radial_flow) {        // Radial line flow statistics
    const size_t n_src_vals = radial_line_flow.src.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_src_vals; ++i)
      radial_line_flow.src[i] *= L_tot;      // Convert to erg/s
    const size_t n_esc_vals = radial_line_flow.esc.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_esc_vals; ++i)
      radial_line_flow.esc[i] *= L_tot;      // Convert to erg/s
    const size_t n_abs_vals = radial_line_flow.abs.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_abs_vals; ++i)
      radial_line_flow.abs[i] *= L_tot;      // Convert to erg/s
    const size_t n_pass_vals = radial_line_flow.pass.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_pass_vals; ++i)
      radial_line_flow.pass[i] *= L_tot;     // Convert to erg/s
    const size_t n_flow_vals = radial_line_flow.flow.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_flow_vals; ++i)
      radial_line_flow.flow[i] *= L_tot;     // Convert to erg/s
  }
  if constexpr (output_group_flows)
    for (auto& group_line_flow : group_line_flows) {
      const size_t n_src_vals = group_line_flow.src.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_src_vals; ++i)
        group_line_flow.src[i] *= L_tot;     // Convert to erg/s
      const size_t n_esc_vals = group_line_flow.esc.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_esc_vals; ++i)
        group_line_flow.esc[i] *= L_tot;     // Convert to erg/s
      const size_t n_abs_vals = group_line_flow.abs.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_abs_vals; ++i)
        group_line_flow.abs[i] *= L_tot;     // Convert to erg/s
      const size_t n_pass_vals = group_line_flow.pass.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_pass_vals; ++i)
        group_line_flow.pass[i] *= L_tot;    // Convert to erg/s
      const size_t n_flow_vals = group_line_flow.flow.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_flow_vals; ++i)
        group_line_flow.flow[i] *= L_tot;    // Convert to erg/s
    }
  if constexpr (output_subhalo_flows)
    for (auto& subhalo_line_flow : subhalo_line_flows) {
      const size_t n_src_vals = subhalo_line_flow.src.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_src_vals; ++i)
        subhalo_line_flow.src[i] *= L_tot;   // Convert to erg/s
      const size_t n_esc_vals = subhalo_line_flow.esc.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_esc_vals; ++i)
        subhalo_line_flow.esc[i] *= L_tot;   // Convert to erg/s
      const size_t n_abs_vals = subhalo_line_flow.abs.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_abs_vals; ++i)
        subhalo_line_flow.abs[i] *= L_tot;   // Convert to erg/s
      const size_t n_pass_vals = subhalo_line_flow.pass.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_pass_vals; ++i)
        subhalo_line_flow.pass[i] *= L_tot;  // Convert to erg/s
      const size_t n_flow_vals = subhalo_line_flow.flow.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_flow_vals; ++i)
        subhalo_line_flow.flow[i] *= L_tot;  // Convert to erg/s
    }

  // Radiation energy density [erg/cm^3], acceleration [cm/s^2], and pressure [erg/cm^3]
  if constexpr (output_energy_density || output_acceleration || output_acceleration_scat || output_pressure) {
    const double L_c = L_tot / c;            // Force units: L / c
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (avoid_cell_strict(i)) {
        if constexpr (output_energy_density)
          u_rad[i] = 0.;                     // Radiation energy density
        if constexpr (SPHERICAL) {
          if constexpr (output_acceleration)
            a_rad_r[i] = 0.;                 // Radial radiation acceleration
          if constexpr (output_acceleration_scat)
            a_rad_r_scat[i] = 0.;            // Radial scattering-based acceleration
          if constexpr (output_pressure)
            P_rad_r[i] = 0.;                 // Radial radiation pressure
        } else {
          if constexpr (output_acceleration)
            a_rad[i] = 0.;                   // Radiation acceleration
          if constexpr (output_acceleration_scat)
            a_rad_scat[i] = 0.;              // Scattering-based acceleration
        }
        continue;                            // Avoid cell
      }
      const double L_cV = L_c / VOLUME(i);   // Force density units: L / (c V)
      const double L_cm = L_cV / rho[i];     // Acceleration units: L / (c m)
      if constexpr (output_energy_density)
        u_rad[i] *= L_cV;                    // Energy density (convert to erg/cm^3)
      if constexpr (SPHERICAL) {
        if constexpr (output_acceleration)
          a_rad_r[i] *= L_cm;                // Radial radiation acceleration (convert to cm/s^2)
        if constexpr (output_acceleration_scat)
          a_rad_r_scat[i] *= L_cm;           // Radial scattering-based acceleration (convert to cm/s^2)
        if constexpr (output_pressure)
          P_rad_r[i] *= L_cV;                // Radial radiation pressure (convert to erg/cm^3)
      } else {
        if constexpr (output_acceleration)
          a_rad[i] *= L_cm;                  // Radiation acceleration (convert to cm/s^2)
        if constexpr (output_acceleration_scat)
          a_rad_scat[i] *= L_cm;             // Scattering-based acceleration (convert to cm/s^2)
      }
    }
  }

  // Cumulative radial radiation statistics (f_src, f_esc, t_trap/t_light, M_F, P/u)
  if constexpr (output_f_src_r) {
    for (int i = 1; i < n_cells; ++i)
      f_src_r[i] += f_src_r[i-1];            // Cumulative conversion
  }
  if constexpr (output_f_esc_r) {
    f_esc_r[n_cells-1] = f_esc;              // Copy global escape fraction
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        f_esc_r[i] /= f_src_r[i];            // Normalize to source fraction
    }
  }
  if constexpr (output_rescaled_f_esc_r) {
    rescaled_f_esc_r[n_cells-1] = f_esc;     // Copy global escape fraction (no need to re-adjust tau_dust)
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        rescaled_f_esc_r[i] /= f_src_r[i];   // Normalize to source fraction
    }
  }
  if constexpr (output_trap_r) {
    for (int i = 1; i < n_cells; ++i)
      trap_r[i] += trap_r[i-1];              // Cumulative conversion
  }
  if constexpr (output_M_F_r) {
    for (int i = 1; i < n_cells; ++i)
      M_F_r[i] += M_F_r[i-1];                // Cumulative conversion
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        M_F_r[i] /= f_src_r[i];              // Normalize to source fraction
    }
  }
  if constexpr (output_M_F_r_scat) {
    for (int i = 1; i < n_cells; ++i)
      M_F_r_scat[i] += M_F_r_scat[i-1];      // Cumulative conversion
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        M_F_r_scat[i] /= f_src_r[i];         // Normalize to source fraction
    }
  }
  if constexpr (output_M_F_src) {
    for (int i = 1; i < n_cells; ++i)
      M_F_src[i] += M_F_src[i-1];            // Cumulative conversion
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        M_F_src[i] /= f_src_r[i];              // Normalize to source fraction
    }
  }
  if constexpr (output_M_F_src_scat) {
    for (int i = 1; i < n_cells; ++i)
      M_F_src_scat[i] += M_F_src_scat[i-1];      // Cumulative conversion
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        M_F_src_scat[i] /= f_src_r[i];         // Normalize to source fraction
    }
  }
  if constexpr (output_P_u_r) {
    for (int i = 1; i < n_cells; ++i)
      P_u_r[i] += P_u_r[i-1];                // Cumulative conversion
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (trap_r[i] > 0.)
        P_u_r[i] /= trap_r[i];               // Normalize to energy density
    }
  }
  if constexpr (output_trap_r) {             // Needs to happen after P_u_r normalization
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      if (f_src_r[i] > 0.)
        trap_r[i] /= f_src_r[i] * r[i+1].x;  // Normalize to source fraction and t_light
    }
  }
}

/* Driver for MCRT calculations. */
void MCRT::run() {
  mcrt_timer.start();
  // Perform the actual MCRT calculations
  n_finished = 0;                            // Reset finished progress
  if (load_balancing)                        // MPI task/thread work assignment
    #pragma omp parallel
    {
      // One of the root process's threads runs the master
      if (root && (thread == 0))
        master();
      else
        slave();
    }
  else
    equal_workers();                         // Assign equal work across ranks

  // Sort escaped and absorbed photons
  if (output_photons)
    sort_photons();

  // Projected emission and attenuation images
  if (output_proj_emission || output_proj_attenuation)
    run_projections();

  // Projected emission and attenuation spectral data cubes
  if (output_proj_cube_emission || output_proj_cube_attenuation)
    run_cube_projections();

  // Add all independent photons from different processors
  if (n_ranks > 1)
    reduce_mcrt();

  if (root) {
    // Convert observables to the correct units
    correct_units();
    print_observables();
  }
  mcrt_timer.stop();
}
