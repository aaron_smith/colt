/********************
 * mcrt/colormaps.h *
 ********************

 * Colormap lookup tables.

*/

#ifndef COLORMAPS_INCLUDED
#define COLORMAPS_INCLUDED

#include <array> // Standard arrays
#include "../Vec3.h" // 3D vectors in (x,y,z) order

// RGB colormap "Blue-Red"
const std::array<Vec3, 256> BlueRed = {{
  {0.       , 0.       , 0.5117188},
  {0.       , 0.       , 0.5117188},
  {0.       , 0.       , 0.5273438},
  {0.       , 0.       , 0.5429688},
  {0.       , 0.       , 0.5585938},
  {0.       , 0.       , 0.5742188},
  {0.       , 0.       , 0.5898438},
  {0.       , 0.       , 0.6054688},
  {0.       , 0.       , 0.6210938},
  {0.       , 0.       , 0.6367188},
  {0.       , 0.       , 0.6523438},
  {0.       , 0.       , 0.6679688},
  {0.       , 0.       , 0.6835938},
  {0.       , 0.       , 0.6992188},
  {0.       , 0.       , 0.7148438},
  {0.       , 0.       , 0.7304688},
  {0.       , 0.       , 0.7460938},
  {0.       , 0.       , 0.7617188},
  {0.       , 0.       , 0.7773438},
  {0.       , 0.       , 0.7929688},
  {0.       , 0.       , 0.8085938},
  {0.       , 0.       , 0.8242188},
  {0.       , 0.       , 0.8398438},
  {0.       , 0.       , 0.8554688},
  {0.       , 0.       , 0.8710938},
  {0.       , 0.       , 0.8867188},
  {0.       , 0.       , 0.9023438},
  {0.       , 0.       , 0.9179688},
  {0.       , 0.       , 0.9335938},
  {0.       , 0.       , 0.9492188},
  {0.       , 0.       , 0.9648438},
  {0.       , 0.       , 0.9804688},
  {0.       , 0.       , 0.9960938},
  {0.       , 0.       , 0.9960938},
  {0.       , 0.0117188, 0.9960938},
  {0.       , 0.0273438, 0.9960938},
  {0.       , 0.0429688, 0.9960938},
  {0.       , 0.0585938, 0.9960938},
  {0.       , 0.0742188, 0.9960938},
  {0.       , 0.0898438, 0.9960938},
  {0.       , 0.1054688, 0.9960938},
  {0.       , 0.1210938, 0.9960938},
  {0.       , 0.1367188, 0.9960938},
  {0.       , 0.1523438, 0.9960938},
  {0.       , 0.1679688, 0.9960938},
  {0.       , 0.1835938, 0.9960938},
  {0.       , 0.1992188, 0.9960938},
  {0.       , 0.2148438, 0.9960938},
  {0.       , 0.2304688, 0.9960938},
  {0.       , 0.2460938, 0.9960938},
  {0.       , 0.2617188, 0.9960938},
  {0.       , 0.2773438, 0.9960938},
  {0.       , 0.2929688, 0.9960938},
  {0.       , 0.3085938, 0.9960938},
  {0.       , 0.3242188, 0.9960938},
  {0.       , 0.3398438, 0.9960938},
  {0.       , 0.3554688, 0.9960938},
  {0.       , 0.3710938, 0.9960938},
  {0.       , 0.3867188, 0.9960938},
  {0.       , 0.4023438, 0.9960938},
  {0.       , 0.4179688, 0.9960938},
  {0.       , 0.4335938, 0.9960938},
  {0.       , 0.4492188, 0.9960938},
  {0.       , 0.4648438, 0.9960938},
  {0.       , 0.4804688, 0.9960938},
  {0.       , 0.4960938, 0.9960938},
  {0.       , 0.5117188, 0.9960938},
  {0.       , 0.5273438, 0.9960938},
  {0.       , 0.5429688, 0.9960938},
  {0.       , 0.5585938, 0.9960938},
  {0.       , 0.5742188, 0.9960938},
  {0.       , 0.5898438, 0.9960938},
  {0.       , 0.6054688, 0.9960938},
  {0.       , 0.6210938, 0.9960938},
  {0.       , 0.6367188, 0.9960938},
  {0.       , 0.6523438, 0.9960938},
  {0.       , 0.6679688, 0.9960938},
  {0.       , 0.6835938, 0.9960938},
  {0.       , 0.6992188, 0.9960938},
  {0.       , 0.7148438, 0.9960938},
  {0.       , 0.7304688, 0.9960938},
  {0.       , 0.7460938, 0.9960938},
  {0.       , 0.7617188, 0.9960938},
  {0.       , 0.7773438, 0.9960938},
  {0.       , 0.7929688, 0.9960938},
  {0.       , 0.8085938, 0.9960938},
  {0.       , 0.8242188, 0.9960938},
  {0.       , 0.8398438, 0.9960938},
  {0.       , 0.8554688, 0.9960938},
  {0.       , 0.8710938, 0.9960938},
  {0.       , 0.8867188, 0.9960938},
  {0.       , 0.9023438, 0.9960938},
  {0.       , 0.9179688, 0.9960938},
  {0.       , 0.9335938, 0.9960938},
  {0.       , 0.9492188, 0.9960938},
  {0.       , 0.9648438, 0.9960938},
  {0.       , 0.9804688, 0.9960938},
  {0.       , 0.9960938, 0.9960938},
  {0.       , 0.9960938, 0.9960938},
  {0.0117188, 0.9960938, 0.9804688},
  {0.0273438, 0.9960938, 0.9648438},
  {0.0429688, 0.9960938, 0.9492188},
  {0.0585938, 0.9960938, 0.9335938},
  {0.0742188, 0.9960938, 0.9179688},
  {0.0898438, 0.9960938, 0.9023438},
  {0.1054688, 0.9960938, 0.8867188},
  {0.1210938, 0.9960938, 0.8710938},
  {0.1367188, 0.9960938, 0.8554688},
  {0.1523438, 0.9960938, 0.8398438},
  {0.1679688, 0.9960938, 0.8242188},
  {0.1835938, 0.9960938, 0.8085938},
  {0.1992188, 0.9960938, 0.7929688},
  {0.2148438, 0.9960938, 0.7773438},
  {0.2304688, 0.9960938, 0.7617188},
  {0.2460938, 0.9960938, 0.7460938},
  {0.2617188, 0.9960938, 0.7304688},
  {0.2773438, 0.9960938, 0.7148438},
  {0.2929688, 0.9960938, 0.6992188},
  {0.3085938, 0.9960938, 0.6835938},
  {0.3242188, 0.9960938, 0.6679688},
  {0.3398438, 0.9960938, 0.6523438},
  {0.3554688, 0.9960938, 0.6367188},
  {0.3710938, 0.9960938, 0.6210938},
  {0.3867188, 0.9960938, 0.6054688},
  {0.4023438, 0.9960938, 0.5898438},
  {0.4179688, 0.9960938, 0.5742188},
  {0.4335938, 0.9960938, 0.5585938},
  {0.4492188, 0.9960938, 0.5429688},
  {0.4648438, 0.9960938, 0.5273438},
  {0.4804688, 0.9960938, 0.5117188},
  {0.4960938, 0.9960938, 0.4960938},
  {0.5117188, 0.9960938, 0.4804688},
  {0.5273438, 0.9960938, 0.4648438},
  {0.5429688, 0.9960938, 0.4492188},
  {0.5585938, 0.9960938, 0.4335938},
  {0.5742188, 0.9960938, 0.4179688},
  {0.5898438, 0.9960938, 0.4023438},
  {0.6054688, 0.9960938, 0.3867188},
  {0.6210938, 0.9960938, 0.3710938},
  {0.6367188, 0.9960938, 0.3554688},
  {0.6523438, 0.9960938, 0.3398438},
  {0.6679688, 0.9960938, 0.3242188},
  {0.6835938, 0.9960938, 0.3085938},
  {0.6992188, 0.9960938, 0.2929688},
  {0.7148438, 0.9960938, 0.2773438},
  {0.7304688, 0.9960938, 0.2617188},
  {0.7460938, 0.9960938, 0.2460938},
  {0.7617188, 0.9960938, 0.2304688},
  {0.7773438, 0.9960938, 0.2148438},
  {0.7929688, 0.9960938, 0.1992188},
  {0.8085938, 0.9960938, 0.1835938},
  {0.8242188, 0.9960938, 0.1679688},
  {0.8398438, 0.9960938, 0.1523438},
  {0.8554688, 0.9960938, 0.1367188},
  {0.8710938, 0.9960938, 0.1210938},
  {0.8867188, 0.9960938, 0.1054688},
  {0.9023438, 0.9960938, 0.0898438},
  {0.9179688, 0.9960938, 0.0742188},
  {0.9335938, 0.9960938, 0.0585938},
  {0.9492188, 0.9960938, 0.0429688},
  {0.9648438, 0.9960938, 0.0273438},
  {0.9804688, 0.9960938, 0.0117188},
  {0.9960938, 0.9960938, 0.       },
  {0.9960938, 0.9804688, 0.       },
  {0.9960938, 0.9648438, 0.       },
  {0.9960938, 0.9492188, 0.       },
  {0.9960938, 0.9335938, 0.       },
  {0.9960938, 0.9179688, 0.       },
  {0.9960938, 0.9023438, 0.       },
  {0.9960938, 0.8867188, 0.       },
  {0.9960938, 0.8710938, 0.       },
  {0.9960938, 0.8554688, 0.       },
  {0.9960938, 0.8398438, 0.       },
  {0.9960938, 0.8242188, 0.       },
  {0.9960938, 0.8085938, 0.       },
  {0.9960938, 0.7929688, 0.       },
  {0.9960938, 0.7773438, 0.       },
  {0.9960938, 0.7617188, 0.       },
  {0.9960938, 0.7460938, 0.       },
  {0.9960938, 0.7304688, 0.       },
  {0.9960938, 0.7148438, 0.       },
  {0.9960938, 0.6992188, 0.       },
  {0.9960938, 0.6835938, 0.       },
  {0.9960938, 0.6679688, 0.       },
  {0.9960938, 0.6523438, 0.       },
  {0.9960938, 0.6367188, 0.       },
  {0.9960938, 0.6210938, 0.       },
  {0.9960938, 0.6054688, 0.       },
  {0.9960938, 0.5898438, 0.       },
  {0.9960938, 0.5742188, 0.       },
  {0.9960938, 0.5585938, 0.       },
  {0.9960938, 0.5429688, 0.       },
  {0.9960938, 0.5273438, 0.       },
  {0.9960938, 0.5117188, 0.       },
  {0.9960938, 0.4960938, 0.       },
  {0.9960938, 0.4804688, 0.       },
  {0.9960938, 0.4648438, 0.       },
  {0.9960938, 0.4492188, 0.       },
  {0.9960938, 0.4335938, 0.       },
  {0.9960938, 0.4179688, 0.       },
  {0.9960938, 0.4023438, 0.       },
  {0.9960938, 0.3867188, 0.       },
  {0.9960938, 0.3710938, 0.       },
  {0.9960938, 0.3554688, 0.       },
  {0.9960938, 0.3398438, 0.       },
  {0.9960938, 0.3242188, 0.       },
  {0.9960938, 0.3085938, 0.       },
  {0.9960938, 0.2929688, 0.       },
  {0.9960938, 0.2773438, 0.       },
  {0.9960938, 0.2617188, 0.       },
  {0.9960938, 0.2460938, 0.       },
  {0.9960938, 0.2304688, 0.       },
  {0.9960938, 0.2148438, 0.       },
  {0.9960938, 0.1992188, 0.       },
  {0.9960938, 0.1835938, 0.       },
  {0.9960938, 0.1679688, 0.       },
  {0.9960938, 0.1523438, 0.       },
  {0.9960938, 0.1367188, 0.       },
  {0.9960938, 0.1210938, 0.       },
  {0.9960938, 0.1054688, 0.       },
  {0.9960938, 0.0898438, 0.       },
  {0.9960938, 0.0742188, 0.       },
  {0.9960938, 0.0585938, 0.       },
  {0.9960938, 0.0429688, 0.       },
  {0.9960938, 0.0273438, 0.       },
  {0.9960938, 0.0117188, 0.       },
  {0.9960938, 0.       , 0.       },
  {0.9765625, 0.       , 0.       },
  {0.9609375, 0.       , 0.       },
  {0.9414062, 0.       , 0.       },
  {0.9257812, 0.       , 0.       },
  {0.9101562, 0.       , 0.       },
  {0.890625 , 0.       , 0.       },
  {0.875    , 0.       , 0.       },
  {0.8554688, 0.       , 0.       },
  {0.8398438, 0.       , 0.       },
  {0.8242188, 0.       , 0.       },
  {0.8046875, 0.       , 0.       },
  {0.7890625, 0.       , 0.       },
  {0.7695312, 0.       , 0.       },
  {0.7539062, 0.       , 0.       },
  {0.7382812, 0.       , 0.       },
  {0.71875  , 0.       , 0.       },
  {0.703125 , 0.       , 0.       },
  {0.6835938, 0.       , 0.       },
  {0.6679688, 0.       , 0.       },
  {0.6523438, 0.       , 0.       },
  {0.6328125, 0.       , 0.       },
  {0.6171875, 0.       , 0.       },
  {0.5976562, 0.       , 0.       },
  {0.5820312, 0.       , 0.       },
  {0.5664062, 0.       , 0.       },
  {0.546875 , 0.       , 0.       },
  {0.53125  , 0.       , 0.       },
  {0.5117188, 0.       , 0.       },
  {0.5117188, 0.       , 0.       }
}};

inline Vec3 get_rgb_color(const double freq) {
  const double rgb_freq_bin = inverse_rgb_freq_bin_width * (freq - rgb_freq_min);
  const int rgb_bin = floor(rgb_freq_bin);   // Lookup table index
  if (rgb_bin < 0)
    return BlueRed[0];                       // Saturated lower limit
  if (rgb_bin >= 255)
    return BlueRed[255];                     // Saturated upper limit
  const double rgb_fac = rgb_freq_bin - double(rgb_bin); // Interpolation factor
  return BlueRed[rgb_bin] * (1. - rgb_fac) + BlueRed[rgb_bin+1] * rgb_fac;
}

#endif // COLORMAPS_INCLUDED
