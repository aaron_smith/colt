/*******************
 * mcrt/scatter.cc *
 *******************

 * Scattering Event: Scattering angle, atom's velocity, optical depth, etc.

*/

#include "proto.h"
#include "photon.h" // Photon packets
#include "colormaps.h" // RGB colormaps

extern double (*H)(const double a, const double x); // Voigt-Hjerting function

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
#if spherical_escape
double spherical_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding sphere
#endif
#if box_escape
double box_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding box
#endif
double min_face_distance(const Vec3& point, const int cell); // Largest sphere around a point in the cell
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

constexpr double x_crit_2 = x_crit * x_crit;

/* Constants needed to calculate the core-wing transition */
static const double X0 = 6.9184721, X1 = 81.766279, X2 = 14.651253;

static const int ISOTROPIC = 1, CORE_2P32 = 2, WING_SCAT = 3, DUST_SCAT = 4, OUTWARD_SQRT = 5;

static constexpr double _2pi = 2. * M_PI;
static constexpr double _343_27 = 343. / 27.;
static constexpr double _1_3 = 1. / 3.;
static constexpr double _2_3 = 2. / 3.;
static constexpr double _7_3 = 7. / 3.;

/* Line-of-sight flux emission calculation. */
void Photon::calculate_LOS_int() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_int(camera);
}

// Note: Use this version when the camera directional Doppler-shift matters.
void Photon::calculate_LOS_int(const int camera, const double phase_weight) {
  double freq = frequency;                   // Photon frequency
  Vec3 k_cam = camera_directions[camera];    // Camera direction

  // Doppler-shift from original cell comoving frame to grid frame
  if constexpr (SPHERICAL) {
    if (radius > 0.) {
      const double mu = k_cam.dot(position) / radius;
      if constexpr (cell_velocity_gradients)
        freq += mu * (v0[current_cell] + dvdr[current_cell] * radius);
      else
        freq += mu * v[current_cell].x;      // Use the spherical cosine
    }
  } else // Cartesian-like geometry
    freq += k_cam.dot(v[current_cell]);
  // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
  freq *= a_exit / a[current_cell];
  // Doppler-shift from grid frame to exit comoving frame
  if constexpr (!SPHERICAL)
    freq -= k_cam.dot(camera_motion);
  // Convert to the frequency output type
  if (!doppler_frequency)
    freq *= neg_vth_kms_exit;                // Convert: x -> Delta_v
  if (adjust_camera_frequency)
    freq -= freq_offsets[camera];            // Adjust camera frequency

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_int = phase_weight * source_weight;
  const double weight_col = weight_int * f_col; // Collisional excitation weight

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(position, camera); // Position in camera coordinates
  const double r2 = norm2(pos);              // Projected radius^2

  // Capture photons in the aperture
  if (r2 < aperture_radius2) {
    if (output_freq_avgs) {
      #pragma omp atomic
      freq_avgs_int[camera] += freq * weight_int;

      if (output_collisions)
        #pragma omp atomic
        freq_avgs_int_col[camera] += freq * weight_col;
    }

    if (output_fluxes) {
      // Bin the photon's frequency - Only add to bins in the specified range
      const double freq_bin = inverse_freq_bin_width * (freq - freq_min);
      const bool in_freq_range = (freq_bin >= 0. && freq_bin < double(n_bins));
      if (in_freq_range) {
        const int ibin = floor(freq_bin);
        #pragma omp atomic
        fluxes_int[camera][ibin] += weight_int;

        if (output_collisions)
          #pragma omp atomic
          fluxes_int_col[camera][ibin] += weight_col;
      }
    }
  }

  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images_int[camera](ix, iy) += weight_int;

      if (output_collisions)
        #pragma omp atomic
        images_int_col[camera](ix, iy) += weight_col;
    }
  }

  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    const double freq_bin = inverse_cube_freq_bin_width * (freq - cube_freq_min);
    if (freq_bin >= 0. && freq_bin < double(n_cube_bins) && pix >= 0. && pix < n_cube_pixels) { // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      const int ibin = floor(freq_bin);
      #pragma omp atomic
      cubes_int[camera](ix, iy, ibin) += weight_int;
    }
  }

  if (have_radial_cameras) {
    const double r_pos = sqrt(r2);           // Projected radius
    if (output_radial_images) {
      const double r_pix = inverse_radial_pixel_width * r_pos;
      if (r_pix < double(n_radial_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        radial_images_int[camera][ir] += weight_int;
      }
    }

    if (output_radial_cubes) {
      const double freq_bin = inverse_radial_cube_freq_bin_width * (freq - radial_cube_freq_min);
      const double r_pix = inverse_radial_cube_pixel_width * r_pos;
      if (freq_bin >= 0. && freq_bin < double(n_radial_cube_bins) && r_pix < double(n_radial_cube_pixels)) { // Range check
        const int ir = floor(r_pix), ibin = floor(freq_bin);
        #pragma omp atomic
        radial_cubes_int[camera](ir, ibin) += weight_int;
      }
    }
  }
}

/* Line-of-sight flux attenuation calculation. */
void Photon::calculate_LOS_ext() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_ext(camera);
}

// Note: Use this version when the camera directional Doppler-shift matters.
void Photon::calculate_LOS_ext(const int camera, const double phase_weight) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double freq = frequency;                   // Photon frequency
#if use_tau
  double tau_esc = tau_eff_abs;              // Optical depth to escape
#else
  double tau_esc = -log(weight);             // Optical depth to escape
#endif
  double l;                                  // Path length [cm]
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  #if check_escape
    double l_exit = positive_infinity;       // Escape distance
  #endif
  #if spherical_escape                       // Spherical escape distance
    inplace_min(l_exit, spherical_escape_distance(point, k_cam));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(l_exit, box_escape_distance(point, k_cam));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(l_exit, max_streaming);
  #endif
  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        return;                              // No point to continue
    }

    // Check for spherical or box escape
    #if check_escape
      if (l_exit <= l) {                     // Exit region before cell
        tau_esc += k_dust[cell] * l_exit;    // Cumulative LOS optical depth
        if (tau_esc > tau_discard)           // Early exit condition
          return;                            // No point to continue
        break;                               // Valid tau_esc completion
      }
      l_exit -= l;                           // Remaining distance to exit
    #endif

    // Cumulative LOS optical depth
    tau_esc += k_dust[cell] * l;

    // Discard photons from the calculation if tau_esc > tau_discard
    if (tau_esc > tau_discard)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;

    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Doppler-shift from original cell comoving frame to grid frame
  if constexpr (SPHERICAL) {
    if (radius > 0.) {
      const double mu = k_cam.dot(position) / radius;
      if constexpr (cell_velocity_gradients)
        freq += mu * (v0[current_cell] + dvdr[current_cell] * radius);
      else
        freq += mu * v[current_cell].x;      // Use the spherical cosine
    }
  } else // Cartesian-like geometry
    freq += k_cam.dot(v[current_cell]);
  // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
  freq *= a_exit / a[current_cell];
  // Doppler-shift from grid frame to exit comoving frame
  if constexpr (!SPHERICAL)
    freq -= k_cam.dot(camera_motion);
  // Convert to the frequency output type
  if (!doppler_frequency)
    freq *= neg_vth_kms_exit;                // Convert: x -> Delta_v
  if (adjust_camera_frequency)
    freq -= freq_offsets[camera];            // Adjust camera frequency

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_ext = phase_weight * exp(-tau_esc);
  const double weight_col = weight_ext * f_col; // Collisional excitation weight

  // Calculate the escape fraction for each camera
  if (output_escape_fractions) {
    #pragma omp atomic
    f_escs_ext[camera] += weight_ext;

    if (output_collisions)
      #pragma omp atomic
      f_escs_ext_col[camera] += weight_col;
  }

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(position, camera); // Position in camera coordinates
  const double r2 = norm2(pos);              // Projected radius^2

  // Capture photons in the aperture
  if (r2 < aperture_radius2) {
    if (output_freq_avgs) {
      #pragma omp atomic
      freq_avgs_ext[camera] += freq * weight_ext;

      if (output_collisions)
        #pragma omp atomic
        freq_avgs_ext_col[camera] += freq * weight_col;
    }

    if (output_fluxes) {
      // Bin the photon's frequency - Only add to bins in the specified range
      const double freq_bin = inverse_freq_bin_width * (freq - freq_min);
      const bool in_freq_range = (freq_bin >= 0. && freq_bin < double(n_bins));
      if (in_freq_range) {
        const int ibin = floor(freq_bin);
        #pragma omp atomic
        fluxes_ext[camera][ibin] += weight_ext;

        if (output_collisions)
          #pragma omp atomic
          fluxes_ext_col[camera][ibin] += weight_col;
      }
    }
  }

  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images_ext[camera](ix, iy) += weight_ext;

      if (output_collisions)
        #pragma omp atomic
        images_ext_col[camera](ix, iy) += weight_col;
    }
  }

  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    const double freq_bin = inverse_cube_freq_bin_width * (freq - cube_freq_min);
    if (freq_bin >= 0. && freq_bin < double(n_cube_bins) && pix >= 0. && pix < n_cube_pixels) { // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      const int ibin = floor(freq_bin);
      #pragma omp atomic
      cubes_ext[camera](ix, iy, ibin) += weight_ext;
    }
  }

  if (have_radial_cameras) {
    const double r_pos = sqrt(r2);           // Projected radius
    if (output_radial_images) {
      const double r_pix = inverse_radial_pixel_width * r_pos;
      if (r_pix < double(n_radial_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        radial_images_ext[camera][ir] += weight_ext;
      }
    }

    if (output_radial_cubes) {
      const double freq_bin = inverse_radial_cube_freq_bin_width * (freq - radial_cube_freq_min);
      const double r_pix = inverse_radial_cube_pixel_width * r_pos;
      if (freq_bin >= 0. && freq_bin < double(n_radial_cube_bins) && r_pix < double(n_radial_cube_pixels)) { // Range check
        const int ir = floor(r_pix), ibin = floor(freq_bin);
        #pragma omp atomic
        radial_cubes_ext[camera](ir, ibin) += weight_ext;
      }
    }
  }
}

/* Line-of-sight flux calculation via next event estimation. */
void Photon::calculate_LOS(const int scatter_type) {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS(scatter_type, camera);
}

// Note: Use this version when the camera directional Doppler-shift matters.
void Photon::calculate_LOS(const int scatter_type, const int camera) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double freq = frequency;                   // Photon frequency
#if use_tau
  double tau_esc = tau_eff_abs;              // Optical depth to escape
#else
  double tau_esc = -log(weight);             // Optical depth to escape
#endif
  double l;                                  // Path lengths [cm]
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  #if check_escape
    double l_exit = positive_infinity;       // Escape distance
  #endif
  #if spherical_escape                       // Spherical escape distance
    inplace_min(l_exit, spherical_escape_distance(point, k_cam));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(l_exit, box_escape_distance(point, k_cam));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(l_exit, max_streaming);
  #endif
  double radius_LOS, mu, v_r, beta_r;        // Spherical geometry and aberration
  if constexpr (SPHERICAL) {
    radius_LOS = radius;                     // Copy the photon radius
    mu = (radius > 0.) ? k_cam.dot(point) / radius : 0.; // Directional cosine
  }

  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        return;                              // No point to continue
    }

    // Check for spherical or box escape
    #if check_escape
      if (l_exit <= l) {                     // Exit region before cell
        l = l_exit;                          // Set neighbor distance to exit
        next_cell = OUTSIDE;                 // Flag for escape condition
      }
      l_exit -= l;                           // Remaining distance to exit
    #endif

    // Cumulative LOS optical depth
#if cell_velocity_gradients
    const double dvdr_LOS = dvdr[cell] + (1. - mu*mu) * v0[cell] / radius_LOS;
#endif
#if sobolev
    double k_line_scat = 0.;
    if (l > 0. && dvdr_LOS != 0.) {
      const double dl_line = frequency / dvdr_LOS; // Distance to resonant point
      if (dl_line > 0. && dl_line < l)       // Resonant point is in the cell
        k_line_scat = k_0[current_cell] / (l * dvdr_LOS); // Sobolev line coefficient (effective)
    }
#elif velocity_gradients
    const double dx = dvdr_LOS * l;          // Frequency shift
    const double k_line_scat = k_0[cell] * ((fabs(dx) < 1e-6) ? H(a[cell], freq) :
      (erf(freq) - erf(freq - dx)) / (M_2_SQRTPI * dx));
#else // Normal case (no velocity gradients or sobolev)
    double k_line_scat = k_0[cell] * H(a[cell], freq);
    if constexpr (deuterium_fraction > 0)
      k_line_scat += xDA * deuterium_fraction * k_0[cell] * H(xDA * a[cell], xDA * freq + xDB * a[cell]);
    else if (E0p > 0.)                       // Add doublet line (x' = A x + B a, a' = A a)
      k_line_scat += kp_0[cell] * H(apA*a[cell], xpA*freq+xpB*a[cell]);
#endif
    if constexpr (electron_scattering)       // Line + dust + electrons
      tau_esc += (k_line_scat + k_dust[cell] + k_e[cell]) * l;
    else                                     // Line opacity + dust absorption
      tau_esc += (k_line_scat + k_dust[cell]) * l;

    // Discard photons from the calculation if tau_esc > tau_discard
    if (tau_esc > tau_discard)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;                      // Update photon position
    // Note: Spherical geometry modifies frequency during propagation
    if constexpr (SPHERICAL) {
      const double mu_old = mu;              // Copy directional cosine
      radius_LOS = point.norm();             // Update current radius
      mu = (radius_LOS > 0.) ? k_cam.dot(point) / radius_LOS : 0.; // Update directional cosine
      if constexpr (cell_velocity_gradients)
        freq += v0[cell] * (mu_old - mu) - dvdr[cell] * l; // Apply Doppler shift
      else // No velocity gradients but still spherical geometry effects
        freq += v[cell].x * (mu_old - mu);   // Apply Doppler shift
    }

    // Doppler-shift from current cell comoving frame to grid frame
    if constexpr (SPHERICAL) {
      if constexpr (cell_velocity_gradients)
        v_r = v0[cell] + dvdr[cell] * radius_LOS;
      else
        v_r = v[cell].x;                     // Radial velocity
      freq += mu * v_r;                      // Use the spherical cosine
      if constexpr (special_relativity) if (radius_LOS > 0.) {
        beta_r = avthDc * v_r / (a[cell] * radius_LOS);
        k_cam += point * beta_r;             // Aberration: beta = v/c = avthDc u/a
        k_cam.normalize();                   // Maintain vector normalization
        mu = k_cam.dot(point) / radius_LOS;  // Update directional cosine
      }
    } else // Cartesian-like geometry
      freq += k_cam.dot(v[cell]);

    if (next_cell == OUTSIDE) {              // Check if the photon escapes
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      freq *= a_exit / a[cell];
      // Doppler-shift from grid frame to exit comoving frame
      if constexpr (!SPHERICAL)
        freq -= k_cam.dot(camera_motion);
      break;
    } else {                                 // Continue ray tracing in next cell
      // Transformation of sqrt(T/T') because a ∝ T^(-1/2)
      freq *= a[next_cell] / a[cell];
      // Doppler-shift from grid frame to next cell comoving frame
      if constexpr (SPHERICAL) {
        if constexpr (cell_velocity_gradients)
          v_r = v0[next_cell] + dvdr[next_cell] * radius_LOS;
        else
          v_r = v[next_cell].x;              // Radial velocity
        if constexpr (special_relativity) if (radius_LOS > 0.) {
          beta_r = avthDc * v_r / (a[next_cell] * radius_LOS);
          k_cam -= point * beta_r;           // Aberration: beta = v/c = avthDc u/a
          k_cam.normalize();                 // Maintain vector normalization
          mu = k_cam.dot(point) / radius_LOS; // Update directional cosine
        }
        freq -= mu * v_r;                    // Use the spherical cosine
      } else // Cartesian-like geometry
        freq -= k_cam.dot(v[next_cell]);
    }
    cell = next_cell;                        // Update the cell index
  }

  // Convert to the frequency output type
  if (!doppler_frequency)
    freq *= neg_vth_kms_exit;                // Convert: x -> Delta_v
  if (adjust_camera_frequency)
    freq -= freq_offsets[camera];            // Adjust camera frequency

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  double mu_cam, weight_cam;
  switch (scatter_type) {                    // Determine the scattering weight
    case ISOTROPIC:                          // Isotropic scattering
      weight_cam = 0.5;
      break;
    case CORE_2P32:                          // Here W = 7/16 * (1 + 3/7 * mu^2)
      mu_cam = direction.dot(k_cam);
      weight_cam = 0.4375 + 0.1875 * mu_cam * mu_cam;
      break;
    case WING_SCAT:                          // Here W = 3/8 * (1 + mu^2)
      mu_cam = direction.dot(k_cam);
      weight_cam = 0.375 * (1. + mu_cam * mu_cam);
      break;
    case DUST_SCAT:                          // Here W = 1/2 (1 - g^2) / (1 + g^2 - 2gμ)^(3/2)
      mu_cam = direction.dot(k_cam);
      weight_cam = _1mg2_2D * pow(_1pg2_2g - mu_cam, -1.5);
      break;
    case OUTWARD_SQRT:
      mu_cam = position.dot(k_cam) / position.norm();
      weight_cam = (mu_cam > 0.) ? 2. * mu_cam : 0.; // Radially outward phase function
      break;
    default:
      weight_cam = 0.;
      error("Unrecognized scatter type in camera calculation.");
  }

  // Combine the phase and optical depth weights
  weight_cam *= exp(-tau_esc);
  const double weight_col = weight_cam * f_col; // Collisional excitation weight

  // Calculate the escape fraction for each camera
  if (output_escape_fractions) {
    #pragma omp atomic
    f_escs[camera] += weight_cam;

    if (output_collisions)
      #pragma omp atomic
      f_escs_col[camera] += weight_col;
  }

  // Note: Convert the position and frequency to image/spectrum coordinates
  // *before* casting to an int to avoid overflow and entering UB territory

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(position, camera); // Position in camera coordinates
  const double r2 = norm2(pos);              // Projected radius^2

  // Capture photons in the aperture
  if (r2 < aperture_radius2) {
    if (output_freq_avgs) {
      const double weighted_freq = weight_cam * freq;
      #pragma omp atomic
      freq_avgs[camera] += weighted_freq;

      if (output_freq_stds) {
        const double weighted_freq2 = weighted_freq * freq;
        #pragma omp atomic
        freq_stds[camera] += weighted_freq2;

        if (output_freq_skews) {
          const double weighted_freq3 = weighted_freq2 * freq;
          #pragma omp atomic
          freq_skews[camera] += weighted_freq3;

          if (output_freq_kurts)
            #pragma omp atomic
            freq_kurts[camera] += weighted_freq3 * freq;
        }
      }

      // Collisional excitation averages
      if (output_collisions) {
        const double weighted_freq_col = weight_col * freq;
        #pragma omp atomic
        freq_avgs_col[camera] += weighted_freq_col;

        if (output_freq_stds) {
          const double weighted_freq2_col = weighted_freq_col * freq;
          #pragma omp atomic
          freq_stds_col[camera] += weighted_freq2_col;

          if (output_freq_skews) {
            const double weighted_freq3_col = weighted_freq2_col * freq;
            #pragma omp atomic
            freq_skews_col[camera] += weighted_freq3_col;

            if (output_freq_kurts)
              #pragma omp atomic
              freq_kurts_col[camera] += weighted_freq3_col * freq;
          }
        }
      }
    }

    if (output_fluxes) {
      // Bin the photon's frequency - Only add to bins in the specified range
      const double freq_bin = inverse_freq_bin_width * (freq - freq_min);
      const bool in_freq_range = (freq_bin >= 0. && freq_bin < double(n_bins));
      if (in_freq_range) {
        const int ibin = floor(freq_bin);
        #pragma omp atomic
        fluxes[camera][ibin] += weight_cam;

        if (output_collisions)
          #pragma omp atomic
          fluxes_col[camera][ibin] += weight_col;
      }
    }
  }

  // Add the photon to the images
  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images[camera](ix, iy) += weight_cam;

      if (output_images2)
        #pragma omp atomic
        images2[camera](ix, iy) += weight_cam * weight_cam;

      if (output_freq_images) {
        const double weighted_freq = weight_cam * freq;
        #pragma omp atomic
        freq_images[camera](ix, iy) += weighted_freq;

        if (output_freq2_images) {
          const double weighted_freq2 = weighted_freq * freq;
          #pragma omp atomic
          freq2_images[camera](ix, iy) += weighted_freq2;

          if (output_freq3_images) {
            const double weighted_freq3 = weighted_freq2 * freq;
            #pragma omp atomic
            freq3_images[camera](ix, iy) += weighted_freq3;

            if (output_freq4_images)
              #pragma omp atomic
              freq4_images[camera](ix, iy) += weighted_freq3 * freq;
          }
        }
      }

      if (output_rgb_images) {
        const Vec3 weighted_color = get_rgb_color(freq) * weight_cam;
        Vec3& rgb_pixel = rgb_images[camera](ix, iy);
        #pragma omp atomic
        rgb_pixel.x += weighted_color.x;
        #pragma omp atomic
        rgb_pixel.y += weighted_color.y;
        #pragma omp atomic
        rgb_pixel.z += weighted_color.z;
      }

      // Collisional excitation images
      if (output_collisions) {
        #pragma omp atomic
        images_col[camera](ix, iy) += weight_col;

        if (output_images2)
          #pragma omp atomic
          images2_col[camera](ix, iy) += weight_col * weight_col;

        if (output_freq_images) {
          const double weighted_freq_col = weight_col * freq;
          #pragma omp atomic
          freq_images_col[camera](ix, iy) += weighted_freq_col;

          if (output_freq2_images) {
            const double weighted_freq2_col = weighted_freq_col * freq;
            #pragma omp atomic
            freq2_images_col[camera](ix, iy) += weighted_freq2_col;

            if (output_freq3_images) {
              const double weighted_freq3_col = weighted_freq2_col * freq;
              #pragma omp atomic
              freq3_images_col[camera](ix, iy) += weighted_freq3_col;

              if (output_freq4_images)
                #pragma omp atomic
                freq4_images_col[camera](ix, iy) += weighted_freq3_col * freq;
            }
          }
        }
      }
    }
  }

  // Add the photon to the slits
  if (output_slits) {
    const Vec2 pix = inverse_slit_pixel_width * pos + slit_pixel_offset; // Pixel coordinates
    const double freq_bin = inverse_slit_freq_bin_width * (freq - slit_freq_min);
    if (freq_bin >= 0. && freq_bin < double(n_slit_bins)) {
      const int ibin = floor(freq_bin);      // Bin index
      if (fabs(pos.y) < slit_half_aperture && pix.x >= 0. && pix.x < double(n_slit_pixels)) { // Range check
        const int ix = floor(pix.x);         // Pixel index
        #pragma omp atomic
        slits_x[camera](ix, ibin) += weight_cam;
      }

      if (fabs(pos.x) < slit_half_aperture && pix.y >= 0. && pix.y < double(n_slit_pixels)) { // Range check
        const int iy = floor(pix.y);         // Pixel index
        #pragma omp atomic
        slits_y[camera](iy, ibin) += weight_cam;
      }
    }
  }

  // Add the photon to the cubes
  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    const double freq_bin = inverse_cube_freq_bin_width * (freq - cube_freq_min);
    if (freq_bin >= 0. && freq_bin < double(n_cube_bins) && pix >= 0. && pix < n_cube_pixels) { // Range check
      const int ix = floor(pix.x), iy = floor(pix.y), ibin = floor(freq_bin); // Pixel indices
      #pragma omp atomic
      cubes[camera](ix, iy, ibin) += weight_cam;
    }
  }

  if (have_radial_cameras) {
    const double r_pos = sqrt(r2);           // Projected radius

    // Add the photon to the radial images
    if (output_radial_images) {
      const double r_pix = inverse_radial_pixel_width * r_pos;
      if (r_pix < double(n_radial_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        radial_images[camera][ir] += weight_cam;

        if (output_radial_images2)
          #pragma omp atomic
          radial_images2[camera][ir] += weight_cam * weight_cam;

        if (output_freq_radial_images) {
          const double weighted_freq = weight_cam * freq;
          #pragma omp atomic
          freq_radial_images[camera][ir] += weighted_freq;

          if (output_freq2_radial_images) {
            const double weighted_freq2 = weighted_freq * freq;
            #pragma omp atomic
            freq2_radial_images[camera][ir] += weighted_freq2;

            if (output_freq3_radial_images) {
              const double weighted_freq3 = weighted_freq2 * freq;
              #pragma omp atomic
              freq3_radial_images[camera][ir] += weighted_freq3;

              if (output_freq4_radial_images)
                #pragma omp atomic
                freq4_radial_images[camera][ir] += weighted_freq3 * freq;
            }
          }
        }

        if (output_rgb_radial_images) {
          const Vec3 weighted_color = get_rgb_color(freq) * weight_cam;
          Vec3& rgb_pixel = rgb_radial_images[camera][ir];
          #pragma omp atomic
          rgb_pixel.x += weighted_color.x;
          #pragma omp atomic
          rgb_pixel.y += weighted_color.y;
          #pragma omp atomic
          rgb_pixel.z += weighted_color.z;
        }
      }
    }

    // Add the photon to the radial cubes
    if (output_radial_cubes) {
      const double r_pix = inverse_radial_cube_pixel_width * r_pos;
      const double freq_bin = inverse_radial_cube_freq_bin_width * (freq - radial_cube_freq_min);
      if (freq_bin >= 0. && freq_bin < double(n_radial_cube_bins) && r_pix < double(n_radial_cube_pixels)) { // Range check
        const int ir = floor(r_pix), ibin = floor(freq_bin);
        #pragma omp atomic
        radial_cubes[camera](ir, ibin) += weight_cam;
      }
    }
  }
}

/* Generate a random direction from an isotropic distribution. */
Vec3 isotropic_direction() {
  const double theta = acos(2. * ran() - 1.);
  const double sin_theta = sin(theta);
  const double phi = _2pi * ran();

  // Outgoing direction is a unit vector described by (theta, phi)
  Vec3 k_out {
    sin_theta * cos(phi),
    sin_theta * sin(phi),
    cos(theta),
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

/* Generate random angles from an anisotropic distribution. */
Vec3 anisotropic_direction(const int scatter_type, const Vec3 k) {
  if (scatter_type == ISOTROPIC)
    return isotropic_direction();
  double muS = 0.;                           // Directional cosine
  if (scatter_type == CORE_2P32) {
    // Scatter: W(theta) propto 1 + R/Q cos(theta)^2  (with R/Q = 3/7)
    // Inverse CDF method gives cubic polynomial solution
    const double rv = 8. * ran() - 4.;       // Random variable
    const double dv = pow(sqrt(_343_27 + rv * rv) - rv, _1_3);
    // = ( sqrt( 343/27 + rv^2 ) - rv )^(1/3) = cubic discriminant
    muS = _7_3 / dv - dv;                    // mu = (7/3) / dv - dv
  } else if (scatter_type == WING_SCAT) {
    // Dipole approximation gives a different cubic polynomial solution
    const double rv = 4. * ran() - 2.;       // Random variable
    const double dv = pow(sqrt(1. + rv * rv) - rv, _1_3);
    // = ( sqrt( 1 + rv^2 ) - rv )^(1/3) = cubic discriminant
    muS = 1. / dv - dv;                      // mu = 1 / dv - dv
  } else if (scatter_type == OUTWARD_SQRT) {
    muS = sqrt(ran());                       // Outward limb darkening cosine
  } else
    error("Invalid scattering mode!");

  // Precompute sin and cos multiplied by sqrt(1-mu^2)
  const double phi = _2pi * ran();           // Arbitrary azimuthal angle
  const double sqrt_1muS2 = sqrt(1. - muS * muS);
  const double sin_phi = sqrt_1muS2 * sin(phi);
  const double cos_phi = sqrt_1muS2 * cos(phi);
  const double zS = sqrt(1. - k.z * k.z);    // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  // Outgoing direction in terms of the angles mu and phi
  Vec3 k_out {
    muS * k.x + (k.y * cos_phi + k.x * k.z * sin_phi) / zS,
    muS * k.y - (k.x * cos_phi - k.y * k.z * sin_phi) / zS,
    muS * k.z - sin_phi * zS,
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

/* Generate random angles from an anisotropic distribution (dust). */
static Vec3 anisotropic_direction_HG(Vec3 k) {
  // Henyey-Greenstein phase function for <μ>: ζ = random number in [0,1]
  const double xiS = _1mg2_2g / (_1mg_2g + ran()); // ξ = (1-g^2) / (1-g+2gζ)
  const double muS = _1pg2_2g - _1_2g * xiS * xiS; // μ = (1+g^2-ξ^2) / (2g)

  // Precompute sin and cos multiplied by sqrt(1-mu^2)
  const double phi = _2pi * ran();           // Arbitrary azimuthal angle
  const double sqrt_1muS2 = sqrt(1. - muS * muS);
  const double sin_phi = sqrt_1muS2 * sin(phi);
  const double cos_phi = sqrt_1muS2 * cos(phi);
  const double zS = sqrt(1. - k.z * k.z);    // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  // Outgoing direction in terms of the angles mu and phi
  Vec3 k_out {
    muS * k.x + (k.y * cos_phi + k.x * k.z * sin_phi) / zS,
    muS * k.y - (k.x * cos_phi - k.y * k.z * sin_phi) / zS,
    muS * k.z - sin_phi * zS,
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

/* Calculate atau0 from non-local (precalculated) and local cell values. */
static inline double get_atau0(Vec3 point, int cell) {
  // Find the largest sphere centered around the point contained in the cell
  double dl_face = min_face_distance(point, cell);

  // Check whether the photon is closer to an exit condition
  #if spherical_escape
    const double dl_sphere = escape_radius - (point - escape_center).norm();
    if (dl_sphere < dl_face)
      dl_face = dl_sphere;                   // Update the face distance
  #endif
  #if box_escape
    const double dl_box = std::min((point - escape_box[0]).min(), (escape_box[1] - point).min());
    if (dl_box < dl_face)
      dl_face = dl_box;                      // Update the face distance
  #endif

  return atau[cell] + a[cell] * k_0[cell] * dl_face;
}

/* Efficient composite parallel velocity generator combining accept/reject and ratio of uniforms. */
static inline double draw_parallel(const double x, const double a, const double xcw) {
  double u_par;                              // Atom's parallel velocity

  // Use the Gaussian approximation when x >> 1 (far wings)
  if constexpr (x_wing > 0.) if (x > x_wing)
    return sqrt(-log(ran())) * cos(_2pi * ran()) + 1. / x; // Off-center Gaussian

  // Accept/reject based on a Gaussian for small x (core)
  if constexpr (x_core > 0.) if (x < x_core) {
    do {
      u_par = x + a * tan(M_PI * (ran() - 0.5));
    } while (ran() > exp(-u_par * u_par));
    return u_par;
  }

  // Accept/reject for the core and ratio of uniforms for the wings
  if (x > xcw) {                             // Seon+20 for the wings
    double one_b0, beta1, dbeta, one_b1, beta;
    double pb, pb1, Cb;
    double S0, S1, S2, Stot;
    double t1, t2, delt;
    double h1, h2, hmax;

    const double api = a * M_PI;
    const double beta0 = exp(-0.5 * x * x);
    const double h0_two = beta0 / a;
    const double h0 = 0.5 * h0_two;

    // This is using a piecewise comparison function that depends on x to draw
    // numbers using the ratio of uniforms method. The complexity arises from
    // drawing the parameterized beta value and is outlined in Seon & Kim (2020)
    h2 = 0.3861 / (x * x - 1.373);
    if (h0_two < h2) {
      do {
        beta = ran();
        Cb = h2;
        pb = sqrt(-2. * log(beta));
        t2 = atan((pb - x) / a);
        t1 = atan((-pb - x) / a);
        delt = t2 - t1;
      } while (ran() * Cb * api > beta * delt);
    } else if (h0 < h2) {
      S0 = beta0 * h0;
      one_b0 = 1. - beta0;
      S1 = one_b0 * h0;
      Stot = S0 + S1;
      do {
        if (ran() < S0 / Stot) {
          beta = beta0 * sqrt(ran());
          Cb = beta / a;
        } else {
          beta = beta0 + one_b0 * ran();
          Cb = h2;
        }
        pb = sqrt(-2 * log(beta));
        t2 = atan((pb - x) / a);
        t1 = atan((-pb - x) / a);
        delt = t2 - t1;
      } while (ran() * Cb * api > beta * delt);
    } else {
      dbeta = sqrt(M_2_PI * (1. - beta0) * beta0 * x);
      beta1 = beta0 + dbeta;
      one_b1 = 1. - beta1;
      pb1 = sqrt(-2. * log(beta1));
      h1 = M_2_PI * beta1 * pb1 / (x * x - pb1 * pb1);
      hmax = fmax(h1, h2);
      S0 = beta0 * h0;
      S1 = dbeta * h0;
      S2 = one_b1 * hmax;
      Stot = S0 + S1 + S2;
      do {
        const double r = ran();
        if (r < S0 / Stot) {
          beta = beta0 * sqrt(ran());
          Cb = beta / a;
        } else if (r < 1 - S2 / Stot) {
          beta = beta0 + dbeta * ran();
          Cb = h0;
        } else {
          beta = beta1 + one_b1 * ran();
          Cb = hmax;
        }
        pb = sqrt(-2. * log(beta));
        t2 = atan((pb - x) / a);
        t1 = atan((-pb - x) / a);
        delt = t2 - t1;
      } while (ran() * Cb * api > beta * delt);
    }
    u_par = x + a * tan(delt*ran() + t1);
  } else {                                   // Smith+15 for the core
    // Acceleration scheme: compare to exp(u0^2 - upar^2) / (a^2 + (upar-nu)^2)
    double u0, comp;
    if (x < xcw)
      u0 = x - 0.5 / (x + M_PI_4 * exp(-x * x) / a); // Based on series expansion
      // u0 = x - 1. / (x + exp(1. - x * x) / a); // Attempts to extend the core
    else
      u0 = xcw - 1. / xcw + 0.15 * (x - xcw);

    const double t0 = atan((u0 - x) / a);
    const double exp_u2 = exp(-u0 * u0);
    const double p0 = (t0 + M_PI_2) / ((1. - exp_u2) * t0 + (1. + exp_u2) * M_PI_2);
    do {
      if (ran() <= p0)                       // Tan argument from [-pi/2, theta0]
        u_par = a * tan(ran() * (t0 + M_PI_2) - M_PI_2) + x;
      else                                   // Otherwise from [theta0, pi/2]
        u_par = a * tan(ran() * (t0 - M_PI_2) + M_PI_2) + x;
      if (u_par <= u0)                       // Compare to g1
        comp = exp(-u_par * u_par);
      else                                   // Compare to g2
        comp = exp(u0 * u0 - u_par * u_par);
    } while (ran() > comp);                  // Only accept proper values
  }
  return u_par;
}

/* Dynamical core-skipping algorithm */
static inline double dynamical_core_skip(const Vec3& position, const int current_cell, const bool shift_line) {
  double atau0 = get_atau0(position, current_cell);
  if constexpr (deuterium_fraction > 0.)
    if (shift_line)
      atau0 *= xDA * xDA * deuterium_fraction;
  if (atau0 > 1.)                            // Optically thick => atau0 > 1
    return sqrt(0.04 * pow(atau0, _2_3) - log(ran())); // Volcano gaussian
  else                                       // Optically thin => Don't skip
    return sqrt(-log(ran()));                // Bypass core-skipping => gaussian
}

/* Isloate core skipping logic with a deuterium line */
static inline double draw_perp(const double absx, const double xcw, const Vec3& position,
                               const int current_cell, const bool shift_line) {
  if constexpr (x_crit == 0.)                // No core-skipping
    return sqrt(-log(ran()));                // Bypass core-skipping => gaussian

  if constexpr (dynamical_core_skipping) {   // Dynamical core-skipping
    if (absx < xcw)                          // Check if photon is in the core
      return dynamical_core_skip(position, current_cell, shift_line);
    else                                     // Don't accelerate wing photons
      return sqrt(-log(ran()));              // Bypass core-skipping => gaussian
  } else if constexpr (deuterium_fraction > 0.) { // Constant core-skipping with deuterium
    if (shift_line || absx >= xcw)           // Deuterium scattering or wing scattering -> No constant core-skipping
      return sqrt(-log(ran()));              // Bypass core-skipping => gaussian
    else                                     // Ly-alpha scattering -> Static Core-skipping
      return sqrt(x_crit_2 - log(ran()));    // Volcano gaussian
  } else {                                   // Constant core-skipping without deuterium
    if (absx < xcw)                          // Check if photon is in the core
      return sqrt(x_crit_2 - log(ran()));    // Volcano gaussian
    else                                     // Don't accelerate wing photons
      return sqrt(-log(ran()));              // Bypass core-skipping => gaussian
  }
}

/* Isolate everything that happens during the scattering event. */
void Photon::resonant_scatter(const bool shift_line) {
  double x, a0, prob_F, xF_div_a, g_div_a;
  if (shift_line) {
    if constexpr (deuterium_fraction > 0) {
      x = xDA * frequency + xDB * a[current_cell]; // Transformation: x' = A x + B a
      a0 = xDA * a[current_cell];            // Transformation: a' = A a
      prob_F = P2F;                          // Probability of fluorescence
      xF_div_a = xFDa;                       // Fluorescent line: x' / a'
      g_div_a = gDaD;                        // Recoil parameter: g' / a'
    } else {
      x = xpA * frequency + xpB * a[current_cell]; // Transformation: x' = A x + B a
      a0 = apA * a[current_cell];            // Transformation: a' = A a
      prob_F = P2Fp;                         // Probability of fluorescence
      xF_div_a = xFpDa;                      // Fluorescent line: x' / a'
      g_div_a = gDap;                        // Recoil parameter: g' / a'
    }
  } else {
    x = frequency;                           // Frequency before scattering
    a0 = a[current_cell];                    // Save the damping parameter
    prob_F = P2F;                            // Probability of fluorescence
    xF_div_a = xFDa;                         // Fluorescent line: x / a
    g_div_a = gDa;                           // Recoil parameter: g / a
  }
  const double absx = fabs(x);               // Assume x > 0 and correct sign later
  const double xcw = X0 + X1 / (log(a0) - X2); // Transition from the core to the wing

  // Atom's velocity is generated from the parallel component distribution
  double u_par = draw_parallel(absx, a0, xcw);
  if (x < 0.)                                // Asymmetric distribution
    u_par = -u_par;                          // Correct the sign

  // Frequency in the frame of the atom (allowing for fluorescence)
  const double x_atom = (prob_F > 0. && ran() < prob_F) ? xF_div_a * a0 : x - u_par;

  // Perpendicular velocity is Maxwellian modified by core-skipping
  const double u_perp = draw_perp(absx, xcw, position, current_cell, shift_line);

  // Assemble the complete vector u_atom = v_atom / v_th
  // u = sum of the vectors u_par, u_perp_cos, and u_perp_sin
  // Idea: find two orthonormal vectors to u_par = {kx, ky, kz}
  // un-normalized u_perp vectors: {-ky, kx, 0} and {-kx kz, -ky kz, kx^2 + ky^2}
  const double phi = _2pi * ran();           // Azimuthal angle
  const double u_perp_cos = u_perp * cos(phi); // Magnitudes of perpendicular vectors
  const double u_perp_sin = u_perp * sin(phi);
  const double zS = sqrt(1. - direction.z * direction.z); // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  Vec3 u_atom {
    u_par * direction.x - (direction.y * u_perp_cos + direction.x * direction.z * u_perp_sin) / zS,
    u_par * direction.y + (direction.x * u_perp_cos - direction.y * direction.z * u_perp_sin) / zS,
    u_par * direction.z + u_perp_sin * zS,
  };

  int scatter_type = ISOTROPIC; // Either Core 2P_1/2, Core 2P_3/2, or Wing scattering
  if (anisotropic_scattering) {
    if (absx < 0.2) { // Core
      // Give 1/3 probability of an isotropic scatter - 2P_1/2 state
      if (ran() < _1_3) // Isotropic direction and LOS weight
        scatter_type = ISOTROPIC;
      else // Or 2/3 probability of anisotropic scatter - 2P_3/2 state
        scatter_type = CORE_2P32;
    } else
      scatter_type = WING_SCAT;
  }

  // Direction: Anisotropic with W(theta) propto 1 + R/Q cos(theta)^2
  const Vec3 k_out = anisotropic_direction(scatter_type, direction);

  // Camera calculations using the updated frequency (but previous direction)
  for (int camera = 0; camera < n_cameras; ++camera) {
    frequency = x_atom + camera_directions[camera].dot(u_atom);
    if (recoil)
      frequency += g_div_a * a0 * (direction.dot(camera_directions[camera]) - 1.);
    if (shift_line) {                        // Transform back to primary line frequency
      if constexpr (deuterium_fraction > 0)
        frequency = (frequency - xDB * a[current_cell]) / xDA;
      else
        frequency = (frequency - xpB * a[current_cell]) / xpA;
    }
    calculate_LOS(scatter_type, camera);     // Next event estimation
  }

  // Account for recoil and Doppler-shifting from the scattering atom
  // g = h*DnuD/2*kB*T  =>  gDa = g/a = 2*h*nu0^2/mH*c^2*DnuL = 0.54
  frequency = x_atom + u_atom.dot(k_out);
  if (recoil)
    frequency += g_div_a * a0 * (direction.dot(k_out) - 1.);
  if (shift_line) {                          // Transform back to primary line frequency
      if constexpr (deuterium_fraction > 0)
        frequency = (frequency - xDB * a[current_cell]) / xDA;
      else
        frequency = (frequency - xpB * a[current_cell]) / xpA;
  }

  // Update the photon direction (after camera calculations)
  direction = k_out;
}

/* Isolate everything that happens during the scattering event (complete redistribution). */
void Photon::crd_line_scatter() {
  // Direction: Anisotropic with W(theta) propto 1 + R/Q cos(theta)^2
  const Vec3 k_out = isotropic_direction();

  // Atom's velocity is generated from a Maxwellian distribution
  // Assemble the complete vector u_atom = v_atom / v_th
  // u = sum of the vectors u_par, u_perp_cos, and u_perp_sin
  // Idea: find two orthonormal vectors to u_par = {kx, ky, kz}
  // un-normalized u_perp vectors: {-ky, kx, 0} and {-kx kz, -ky kz, kx^2 + ky^2}
  const double u_par = sqrt(-log(ran())) * cos(_2pi * ran()); // Parallel velocity
  const double phi = _2pi * ran();           // Azimuthal angle
  const double u_perp = sqrt(-log(ran()));   // Perpendicular velocity
  const double u_perp_cos = u_perp * cos(phi); // Magnitudes of perpendicular vectors
  const double u_perp_sin = u_perp * sin(phi);
  const double zS = sqrt(1. - direction.z * direction.z); // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  Vec3 u_atom {
    u_par * direction.x - (direction.y * u_perp_cos + direction.x * direction.z * u_perp_sin) / zS,
    u_par * direction.y + (direction.x * u_perp_cos - direction.y * direction.z * u_perp_sin) / zS,
    u_par * direction.z + u_perp_sin * zS,
  };

  // Camera calculations using the updated frequency (but previous direction)
  const double x = frequency;                // Frequency before scattering
  for (int camera = 0; camera < n_cameras; ++camera) {
    frequency = x + camera_directions[camera].dot(u_atom) - u_par;
    calculate_LOS(ISOTROPIC, camera);        // Next event estimation
  }

  // Account for recoil and Doppler-shifting from the scattering atom
  // g = h*DnuD/2*kB*T  =>  gDa = g/a = 2*h*nu0^2/mH*c^2*DnuL = 0.54
  frequency = x + u_atom.dot(k_out) - u_par;

  // Update the photon direction (after camera calculations)
  direction = k_out;
}

/* Isolate everything that happens during the scattering event (coherent). */
void Photon::coherent_line_scatter() {
  // Camera calculations and outgoing direction are both isotropic
  calculate_LOS(ISOTROPIC);
  direction = isotropic_direction();
}

/* Isolate everything that happens during the dust scattering event. */
void Photon::dust_scatter() {
  // Camera calculations using the dust phase function
  calculate_LOS(DUST_SCAT);

  // Outgoing direction from the anisotropic Henyey-Greenstein phase function
  direction = anisotropic_direction_HG(direction);
}

#if coherent_electron_scattering
/* Isolate everything that happens during the coherent electron scattering event. */
void Photon::electron_scatter() {
  // Camera calculations and outgoing direction are both isotropic
  calculate_LOS(ISOTROPIC);
  direction = isotropic_direction();
}
#elif electron_scattering
static const double sqrt_mH_me = sqrt(mH / me); // Thermal velocity conversion
/* Isolate everything that happens during the electron scattering event. */
void Photon::electron_scatter() {
  // Direction: Anisotropic with W(theta) propto 1 + R/Q cos(theta)^2
  const Vec3 k_out = anisotropic_direction(WING_SCAT, direction);

  // Electron's velocity is generated from a Maxwellian distribution
  // Assemble the complete vector u_electron = v_electron / v_th
  // u = sum of the vectors u_par, u_perp_cos, and u_perp_sin
  // Idea: find two orthonormal vectors to u_par = {kx, ky, kz}
  // un-normalized u_perp vectors: {-ky, kx, 0} and {-kx kz, -ky kz, kx^2 + ky^2}
  const double u_par = sqrt_mH_me * sqrt(-log(ran())) * cos(_2pi * ran()); // Parallel velocity
  const double phi = _2pi * ran();           // Azimuthal angle
  const double u_perp = sqrt_mH_me * sqrt(-log(ran())); // Perpendicular velocity
  const double u_perp_cos = u_perp * cos(phi); // Magnitudes of perpendicular vectors
  const double u_perp_sin = u_perp * sin(phi);
  const double zS = sqrt(1. - direction.z * direction.z); // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  Vec3 u_electron {
    u_par * direction.x - (direction.y * u_perp_cos + direction.x * direction.z * u_perp_sin) / zS,
    u_par * direction.y + (direction.x * u_perp_cos - direction.y * direction.z * u_perp_sin) / zS,
    u_par * direction.z + u_perp_sin * zS,
  };

  // Camera calculations using the updated frequency (but previous direction)
  const double x = frequency;                // Frequency before scattering
  for (int camera = 0; camera < n_cameras; ++camera) {
    frequency = x + camera_directions[camera].dot(u_electron) - u_par;
    calculate_LOS(WING_SCAT, camera);        // Next event estimation
  }

  // Account for recoil and Doppler-shifting from the scattering electron
  frequency = x + u_electron.dot(k_out) - u_par;
  // TODO: Include electron recoil
  // g = h*DnuD/2*kB*T  =>  gDa = g/a = 2*h*nu0^2/mH*c^2*DnuL = 0.54
  // frequency += gDa * a0 * (direction.dot(k_out) - 1.);

  // Update the photon direction (after camera calculations)
  direction = k_out;
}
#endif
