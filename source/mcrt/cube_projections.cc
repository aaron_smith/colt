/****************************
 * mcrt/cube_projections.cc *
 ****************************

 * Driver: Assign rays for cube projections, etc.

*/

#include "proto.h"
#include "MCRT.h"
#include "../timing.h" // Timing functionality

extern Timer mcrt_proj_cube_timer; // Clock timing
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

// Quad tree data structure
struct QuadTreeBinNode {
  int children;                              // Index of the first child node
  double rtol;                               // Node relative tolerance
  double xL, xR;                             // Node x range [xmin, xmax]
  double yL, yR;                             // Node y range [ymin, ymax]
  double *fLL, *fLR, *fRL, *fRR;             // Pass pre-calculated values to children
  double *fc;                                // Cumulative integral over node region

  QuadTreeBinNode() = default;
  QuadTreeBinNode(double rtol, double xL, double xR, double yL, double yR,
                  double *fLL, double *fLR, double *fRL, double *fRR, double *fc) :
                  children(-1), rtol(rtol), xL(xL), xR(xR), yL(yL), yR(yR),
                  fLL(fLL), fLR(fLR), fRL(fRL), fRR(fRR), fc(fc) {};
};

static int n_sub_bins;                       // Number of sub-bins
static int n_fine_bins;                      // Number of fine bins
static int start_cell;                       // Starting cell for searches
#pragma omp threadprivate(start_cell)        // Each thread needs start_cell
static Cube corners;                         // Pixel corner values (cube)
static vector<vector<QuadTreeBinNode>> trees; // Quad tree arrangement of nodes
static vectors all_buffers;                  // Data buffers for each thread
static vectors all_fine_results;             // Fine bin results for each thread
static vectors all_f_traps, all_f_simps;     // Trapezoid and Simpson's rule values
static Vec3 direction;                       // Camera direction
static Vec3 xaxis;                           // Camera x-axis
static Vec3 yaxis;                           // Camera y-axis
static vector<double> proj_freqs;            // Projection frequencies

/* Update all camera specific parameters. */
static void update_camera_params(const int camera) {
  // Update camera direction and axes
  direction = camera_directions[camera];     // Camera direction
  xaxis = camera_xaxes[camera];              // Camera x-axis
  yaxis = camera_yaxes[camera];              // Camera y-axis

  // Initialize the frequency grid
  const double dfine = (cube_freq_max - cube_freq_min) / double(n_fine_bins); // Fine bin width
  #pragma omp parallel for
  for (int ibin = 0; ibin < n_fine_bins; ++ibin)
    proj_freqs[ibin] = cube_freq_min + (double(ibin) + 0.5) * dfine; // Frequency grid

  // Adjust the camera frequency the reference frame
  if (adjust_camera_frequency) {
    const double freq_offset = freq_offsets[camera]; // Camera frequency offset
    #pragma omp parallel for
    for (int ibin = 0; ibin < n_fine_bins; ++ibin)
      proj_freqs[ibin] += freq_offset;       // Un-adjust camera frequency
  }

  // Convert the frequency from output type to Doppler units
  if (!doppler_frequency) {
    #pragma omp parallel for
    for (int ibin = 0; ibin < n_fine_bins; ++ibin)
      proj_freqs[ibin] /= neg_vth_kms_exit;  // Convert: Delta_v -> x
  }

  // Doppler-shift from exit comoving frame to grid frame
  if constexpr (!SPHERICAL) {
    const double freq_motion = dot(direction, camera_motion);
    #pragma omp parallel for
    for (int ibin = 0; ibin < n_fine_bins; ++ibin)
      proj_freqs[ibin] += freq_motion;       // Doppler shift
  }
  // Frequency grid is set up for the current camera (and in exit frame Doppler units)
}

/* Intrinsic emission rule function with frequency calculations. */
static void emission_rule(double *results, double dl, int cell) {
  const double f_exit = a[cell] / a_exit;    // Correct by sqrt(T'/T) but a ∝ T^(-1/2)
  const double Du_cell = dot(direction, v[cell]);
  for (int ibin = 0; ibin < n_fine_bins; ++ibin) {
    const double Du = proj_freqs[ibin] * f_exit + Du_cell; // Doppler shift
    const double exp_Du2 = exp(-Du * Du);    // Doppler broadening (grid frame)
    results[ibin] += j_line[cell] * exp_Du2 * dl; // Volume integrated emissivity
  }
}

/* Intrinsic doublet emission rule function with frequency calculations. */
static void doublet_emission_rule(double *results, double dl, int cell) {
  const double f_exit = a[cell] / a_exit;    // Correct by sqrt(T'/T) but a ∝ T^(-1/2)
  const double Du_cell = dot(direction, v[cell]);
  for (int ibin = 0; ibin < n_fine_bins; ++ibin) {
    const double Du = proj_freqs[ibin] * f_exit + Du_cell; // Doppler shift
    const double exp_Du2 = exp(-Du * Du);    // Doppler broadening (grid frame)
    const double Dup = xpA * Du + xpB * a[cell]; // Doublet frequency shift
    const double exp_Dup2 = exp(-Dup * Dup); // Doublet broadening (grid frame)
    results[ibin] += (j_line[cell] * exp_Du2 + jp_line[cell] * exp_Dup2) * dl; // Volume integrated emissivity
  }
}

/* Attenuated emission rule function with frequency calculations. */
static void attenuation_rule(double *results, double dl, int cell) {
  const double dtau = k_dust[cell] * dl;
  const double f_exit = a[cell] / a_exit;    // Correct by sqrt(T'/T) but a ∝ T^(-1/2)
  const double Du_cell = dot(direction, v[cell]);
  for (int ibin = 0; ibin < n_fine_bins; ++ibin) {
    const double Du = proj_freqs[ibin] * f_exit + Du_cell; // Doppler shift
    const double exp_Du2 = exp(-Du * Du);    // Doppler broadening (grid frame)
    if (dtau < 1e-5) {                       // Numerical stability
      results[ibin] *= 1. - dtau;            // Attenuate incoming flux
      results[ibin] += j_line[cell] * exp_Du2 * (1. - 0.5 * dtau) * dl; // Add escaping emission
    } else {
      const double exp_tau = exp(-dtau);
      results[ibin] *= exp_tau;              // Attenuate incoming flux
      results[ibin] += j_line[cell] * exp_Du2 * (1. - exp_tau) / k_dust[cell]; // Add escaping emission
    }
  }
}

/* Attenuated doublet emission rule function with frequency calculations. */
static void doublet_attenuation_rule(double *results, double dl, int cell) {
  const double dtau = k_dust[cell] * dl;
  const double f_exit = a[cell] / a_exit;    // Correct by sqrt(T'/T) but a ∝ T^(-1/2)
  const double Du_cell = dot(direction, v[cell]);
  for (int ibin = 0; ibin < n_fine_bins; ++ibin) {
    const double Du = proj_freqs[ibin] * f_exit + Du_cell; // Doppler shift
    const double exp_Du2 = exp(-Du * Du);    // Doppler broadening (grid frame)
    const double Dup = xpA * Du + xpB * a[cell]; // Doublet frequency shift
    const double exp_Dup2 = exp(-Dup * Dup); // Doublet broadening (grid frame)
    if (dtau < 1e-5) {                       // Numerical stability
      results[ibin] *= 1. - dtau;            // Attenuate incoming flux
      results[ibin] += (j_line[cell] * exp_Du2 + jp_line[cell] * exp_Dup2) * (1. - 0.5 * dtau) * dl; // Add escaping emission
    } else {
      const double exp_tau = exp(-dtau);
      results[ibin] *= exp_tau;              // Attenuate incoming flux
      results[ibin] += (j_line[cell] * exp_Du2 + jp_line[cell] * exp_Dup2) * (1. - exp_tau) / k_dust[cell]; // Add escaping emission
    }
  }
}

/* Calculate projections for a single line of sight. */
template <void (*rule)(double*, double, int)>
void MCRT::ray_trace(double *results, const double rx, const double ry) {
  // Set the ray starting point based on (rx,ry,-proj_radius)
  Vec3 point = {camera_center.x + rx * xaxis.x
                                + ry * yaxis.x
                                - proj_radius * direction.x,
                camera_center.y + rx * xaxis.y
                                + ry * yaxis.y
                                - proj_radius * direction.y,
                camera_center.z + rx * xaxis.z
                                + ry * yaxis.z
                                - proj_radius * direction.z};
  int cell = find_cell(point, start_cell);   // Current cell index
  if (cell != start_cell)
    start_cell = cell;                       // Save the latest starting cell
  int next_cell;                             // Next cell index
  double dl, l_stop = proj_depth;            // Path lengths [cm]

  // Clear the fine results array
  double *fine_results = all_fine_results[thread].data();
  for (int ibin = 0; ibin < n_fine_bins; ++ibin)
    fine_results[ibin] = 0.;

  // Integrate through the fields for the specified distance
  while (true) {                             // Ray trace until escape
    tie(dl, next_cell) = face_distance(point, direction, cell); // Maximum propagation distance
    if (dl > l_stop) {                       // Stop before next cell
      dl = l_stop;                           // Do not go past the end
      next_cell = OUTSIDE;                   // Signal stopping criterion
    }
    rule(fine_results, dl, cell);            // Volume-weighted integration
    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        break;
    }
    if (next_cell == OUTSIDE)
      break;                                 // Finished ray tracing
    l_stop -= dl;                            // Remaining distance
    point += direction * dl;                 // Move to the new position
    cell = next_cell;                        // Update the next cell index
  }

  // Condense the fine results into the coarse results
  for (int ibin = 0, ifine = 0; ibin < n_bins; ++ibin) {
    results[ibin] = 0.;                      // Clear the results
    for (int isub = 0; isub < n_sub_bins; ++isub, ++ifine)
      results[ibin] += fine_results[ifine];  // Add the fine results
  }
}

// Adaptive quadrature 2D integrator
template <void (*rule)(double *, double, int)>
void MCRT::pixel_quad_2D(double *pixel, const int ix, const int iy) {
  // Additional quadtree variables
  double rtol, xL, xM, xR, yL, yM, yR;
  auto& tree = trees[thread];                // Reference to the local tree
  auto& buffer = all_buffers[thread];        // Reference to the local buffer
  auto& f_trap = all_f_traps[thread], f_simp = all_f_simps[thread];
  double *fLL, *fLR, *fRL, *fRR, *fc;        // Corner values
  double *fLM, *fML, *fMM, *fMR, *fRM;       // Midpoint values

  // Initialize the base node
  int n_nodes = 1;                           // Initialize the node counter
  size_t n_alloc = 0;                        // Initialize the allocation counter
  size_t n_bins4 = 4 * n_bins;               // Size of child allocations
  size_t n_bins5 = 5 * n_bins;               // Size of midpoint allocations
  tree.resize(0);                            // Clear the tree (retain capacity)
  tree.emplace_back(                         // Start with no children
    pixel_rtol,                              // Base tolerance is target tolerance
    cube_xedges[ix], cube_xedges[ix+1],      // Domain: x in (xL,xR)
    cube_yedges[iy], cube_yedges[iy+1],      // Domain: y in (yL,yR)
    corners.data(ix,iy), corners.data(ix,iy+1), // LL, LR, RL, RR corners
    corners.data(ix+1,iy), corners.data(ix+1,iy+1), pixel // Cumulative integral
  );

  // Populate the quadtree
  for (int i = 0; i < n_nodes; ++i) {        // Note: n_nodes changes size
    rtol = tree[i].rtol;                     // Relative tolerance
    xL = tree[i].xL;  xR = tree[i].xR;       // Domain x edges
    yL = tree[i].yL;  yR = tree[i].yR;       // Domain y edges
    fLL = tree[i].fLL;  fLR = tree[i].fLR;   // LL, LR corners
    fRL = tree[i].fRL;  fRR = tree[i].fRR;   // RL, RR corners
    fc = tree[i].fc;                         // Cumulative integral
    xM = 0.5 * (xL + xR);                    // Local node x midpoint
    yM = 0.5 * (yL + yR);                    // Local node y midpoint
    if (n_alloc + n_bins5 > buffer.size())   // Check for sufficient memory
      buffer.resize(2 * n_alloc);            // Allocate more memory
    fLM = &buffer[n_alloc += n_bins];        // Reserve memory for midpoints
    fML = &buffer[n_alloc += n_bins];
    fMM = &buffer[n_alloc += n_bins];
    fMR = &buffer[n_alloc += n_bins];
    fRM = &buffer[n_alloc += n_bins];
    ray_trace<rule>(fLM, xL, yM);            // Midpoint evaluations
    ray_trace<rule>(fML, xM, yL);
    ray_trace<rule>(fMM, xM, yM);
    ray_trace<rule>(fMR, xM, yR);
    ray_trace<rule>(fRM, xR, yM);

    // Note: Estimates are multiplied by 36/Area for convenience and corrected later
    double F_simp = 0.;                      // Simpson's rule estimate (integrated)
    bool converged = true;                   // Fail if one frequency is not converged
    for (int ibin = 0; ibin < n_bins; ++ibin) {
      f_trap[ibin] = fLL[ibin] + fLR[ibin] + fRL[ibin] + fRR[ibin]; // Trapezoid rule uses the corners
      f_simp[ibin] = f_trap[ibin] + 4.*(fLM[ibin] + fML[ibin] + fMR[ibin] + fRM[ibin]) + 16.*fMM[ibin]; // Simpson's rule
      F_simp += f_simp[ibin];                // Integrated value
    }
    // Check whether the node is converged
    for (int ibin = 0; ibin < n_bins; ++ibin) {
      if (fabs(9.*f_trap[ibin] - f_simp[ibin]) > rtol * F_simp ) {
        converged = false;                   // Check for convergence
        break;                               // No need to check further
      }
    }
    if (converged) {
      for (int ibin = 0; ibin < n_bins; ++ibin)
        fc[ibin] = f_simp[ibin];             // Save converged values
      continue;                              // No need to refine this node
    }

    // Refine the current node
    tree[i].children = n_nodes;              // Append children to the end
    n_nodes += 4;                            // Add four children nodes
    const double child_rtol = 2. * rtol;     // Factor of sqrt(4) per refinement level

    //  Layout of children:
    //
    // (yR) fLR-----fMR-----fRR
    //       |       |       |
    //       |   2   |   4   |
    //       |       |       |
    // (yM) fLM-----fMM-----fRM
    //       |       |       |
    //       |   1   |   3   |
    //       |       |       |
    // (yL) fLL-----fML-----fRL
    //
    //      (xL)    (xM)    (xR)

    if (n_alloc + n_bins4 > buffer.size())   // Check for sufficient memory
      buffer.resize(2 * n_alloc);            // Allocate more memory

    // Initialize 1st child
    fc = &buffer[n_alloc += n_bins];         // Reserve memory for child results
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yL, yM,                        // (xL,xR,yL,yR) = (xL,xM,yL,xM)
      fLL, fLM, fML, fMM, fc                 // (LL,LR,RL,RR) = (LL,LM,ML,MM)
    );

    // Initialize 2nd child
    fc = &buffer[n_alloc += n_bins];         // Reserve memory for child results
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xL, xM, yM, yR,                        // (xL,xR,yL,yR) = (xL,xM,yM,xR)
      fLM, fLR, fMM, fMR, fc                 // (LL,LR,RL,RR) = (LM,LR,MM,MR)
    );

    // Initialize 3rd child
    fc = &buffer[n_alloc += n_bins];         // Reserve memory for child results
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yL, yM,                        // (xL,xR,yL,yR) = (xM,xR,yL,yM)
      fML, fMM, fRL, fRM, fc                  // (LL,LR,RL,RR) = (ML,MM,RL,RM)
    );

    // Initialize 4th child
    fc = &buffer[n_alloc += n_bins];         // Reserve memory for child results
    tree.emplace_back(
      child_rtol,                            // No children, child tolerance
      xM, xR, yM, yR,                        // (xL,xR,yL,yR) = (xM,xR,yM,yR)
      fMM, fMR, fRM, fRR, fc                 // (LL,LR,RL,RR) = (MM,MR,RM,RR)
    );
  }

  // Aggregation step for converged sub-domain integrals
  for (int ci, i = n_nodes - 1; i >= 0; --i) {
    ci = tree[i].children;
    if (ci > 0) { // The node has children (i.e. not a leaf node)
      fc = tree[i].fc;                       // Cumulative integral
      double *fc1 = tree[ci].fc;             // Child 1 cumulative integral
      double *fc2 = tree[ci+1].fc;           // Child 2 cumulative integral
      double *fc3 = tree[ci+2].fc;           // Child 3 cumulative integral
      double *fc4 = tree[ci+3].fc;           // Child 4 cumulative integral
      for (int ibin = 0; ibin < n_bins; ++ibin)
        fc[ibin] = 0.25 * (fc1[ibin] + fc2[ibin] + fc3[ibin] + fc4[ibin]);
    }
  }

  // Note: Simpson estimate was multiplied by 36/Area for efficiency
  fc = tree[0].fc;                           // Cumulative integral
  double fc_norm = 1. / 36.;                 // Normalization factor
  for (int ibin = 0; ibin < n_bins; ++ibin)
    fc[ibin] *= fc_norm;                     // Correct the estimate
}

/* Ray trace for projection calculations. */
template <void (*rule)(double*, double, int)>
void MCRT::calculate_projections(Cube& proj_cube) {
  // Pre-calculate the pixel corner values
  const int Np1 = nx_cube_pixels + 1;        // Number of pixel edges
  const int Np2 = ny_cube_pixels + 1;
  const int n_corners = Np1 * Np2;           // Total number of corners
  const int corner_interval = n_corners / 100; // Interval between updates
  int n_finished = 0;                        // Progress for printing
  if (root)
    cout << "\n    Pixel corner calculations:   0%\b\b\b\b" << std::flush;
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n_corners; ++i) {
    // Ray tracing for each corner
    const int ix = i / Np1;                  // Corner ix,iy indices
    const int iy = i % Np1;
    const double rx = cube_xedges[ix];       // Corner position
    const double ry = cube_yedges[iy];
    double *corner = corners.data(ix,iy);    // Corner values
    ray_trace<rule>(corner, rx, ry);         // Ray trace for each corner

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % corner_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n_corners) << "%\b\b\b\b" << std::flush;
    }
  }

  // Perform adaptive convergence for all pixels
  if (root)
    cout << "100%\n    Pixel convergence calculations:   0%\b\b\b\b" << std::flush;
  const int n2_pixels = nx_cube_pixels * ny_cube_pixels; // Total number of pixels
  const int pixel_interval = n2_pixels / 100; // Interval between updates
  n_finished = 0;                            // Reset progress counter
  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < n2_pixels; ++i) {
    // Ray tracing for each pixel
    const int ix = i / ny_cube_pixels;       // Pixel ix,iy indices
    const int iy = i % ny_cube_pixels;
    double *pixel = proj_cube.data(ix,iy);   // Pixel values
    pixel_quad_2D<rule>(pixel, ix, iy);      // Adaptive quadrature for each pixel

    // Print completed progress
    if (root) {
      int i_finished;                        // Progress counter
      #pragma omp atomic capture
      i_finished = ++n_finished;             // Update finished counter
      if (i_finished % pixel_interval == 0)
        cout << std::setw(3) << (100 * size_t(i_finished)) / size_t(n2_pixels) << "%\b\b\b\b" << std::flush;
    }
  }
  if (root)
    cout << "100%" << endl;
}

/* Bin luminosity from unresolved HII regions. */
void MCRT::calculate_projections_HII([[maybe_unused]] Cube& proj_cube) {
  error("Unresolved HII regions not implemented for cube projections!");
}

/* Driver for projection calculations. */
void MCRT::run_cube_projections() {
  mcrt_proj_cube_timer.start();

  // Allocate pixel corner values and quad trees
  const double proj_Dv = 2e5;                // Frequency bin width [km/s]
  n_sub_bins = ceil(cube_freq_bin_width / proj_Dv); // Number of sub-bins
  n_fine_bins = n_cube_bins * n_sub_bins;    // Number of total fine bins
  corners = Cube(nx_cube_pixels + 1, ny_cube_pixels + 1, n_bins);
  trees.resize(n_threads);                   // Each thread has its own tree
  all_buffers.resize(n_threads);             // Similarly for buffers, etc.
  all_fine_results.resize(n_threads);
  all_f_traps.resize(n_threads);
  all_f_simps.resize(n_threads);
  proj_freqs.resize(n_fine_bins);            // Frequency bins
  #pragma omp parallel
  {
    start_cell = 0;                          // Reset the starting cell
    trees[thread].reserve(4097);             // Reserve reasonable tree sizes
    all_buffers[thread].resize(32768 * n_bins); // Each thread has its own data
    all_fine_results[thread].resize(n_fine_bins);
    all_f_traps[thread].resize(n_bins);
    all_f_simps[thread].resize(n_bins);
  }

  const int n_cam_rank = n_cameras / n_ranks; // Equal assignment
  const int remainder = n_cameras - n_cam_rank * n_ranks;
  int start_cam = rank * n_cam_rank;         // Camera start range
  int end_cam = (rank + 1) * n_cam_rank;     // Camera end range
  if (rank < remainder) {
    start_cam += rank;                       // Correct start range
    end_cam += (rank + 1);                   // Correct end range
  } else {
    start_cam += remainder;                  // Correct start range
    end_cam += remainder;                    // Correct end range
  }

  // Setup intrinsic emission cameras
  if (output_proj_cube_emission) {
    if (root)
      cout << "\nIntrinsic emission:";
    for (int camera = start_cam; camera < end_cam; ++camera) {
      if (root) {
        cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
        if (n_ranks > 1)
          cout << " (root)";
      }
      update_camera_params(camera);          // Update the frequency grid

      // Perform the actual projection calculations
      if (doublet_cell_based_emission)
        calculate_projections<doublet_emission_rule>(proj_cubes_int[camera]);
      else
        calculate_projections<emission_rule>(proj_cubes_int[camera]);

      // Add emission from unresolved HII regions
      if (unresolved_HII)
        calculate_projections_HII(proj_cubes_int[camera]);
    }
  }

  // Setup attenuated emission cameras
  if (output_proj_cube_attenuation) {
    if (root)
      cout << "\nAttenuated emission:";
    for (int camera = start_cam; camera < end_cam; ++camera) {
      if (root) {
        cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
        if (n_ranks > 1)
          cout << " (root)";
      }
      update_camera_params(camera);          // Update the frequency grid

      // Perform the actual projection calculations
      if (doublet_cell_based_emission)
        calculate_projections<doublet_attenuation_rule>(proj_cubes_ext[camera]);
      else
        calculate_projections<attenuation_rule>(proj_cubes_ext[camera]);
    }
  }

  // Collisional excitation cameras
  if (output_collisions) {
    // Mulitply j_line by the cell collisional excitation fraction
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      j_line[i] *= f_col_cells[i];

    // Setup intrinsic emission cameras
    if (output_proj_cube_emission) {
      if (root)
        cout << "\nCollisional excitation intrinsic emission:";
      for (int camera = start_cam; camera < end_cam; ++camera) {
        if (root) {
          cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
          if (n_ranks > 1)
            cout << " (root)";
        }
        update_camera_params(camera);        // Update the frequency grid

        // Perform the actual projection calculations
        if (doublet_cell_based_emission)
          calculate_projections<doublet_emission_rule>(proj_cubes_int_col[camera]);
        else
          calculate_projections<emission_rule>(proj_cubes_int_col[camera]);
      }
    }

    // Setup attenuated emission cameras
    if (output_proj_cube_attenuation) {
      if (root)
        cout << "\nAttenuated emission:";
      for (int camera = start_cam; camera < end_cam; ++camera) {
        if (root) {
          cout << "\n  Camera progress: " << camera+1 << " / " << end_cam;
          if (n_ranks > 1)
            cout << " (root)";
        }
        update_camera_params(camera);        // Update the frequency grid

        // Perform the actual projection calculations
        if (doublet_cell_based_emission)
          calculate_projections<doublet_attenuation_rule>(proj_cubes_ext_col[camera]);
        else
          calculate_projections<attenuation_rule>(proj_cubes_ext_col[camera]);
      }
    }
  }

  mcrt_proj_cube_timer.stop();
}
