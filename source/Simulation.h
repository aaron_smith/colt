/****************
 * Simulation.h *
 ****************

 * Base class for running simulations.

*/

#ifndef SIMULATION_INCLUDED
#define SIMULATION_INCLUDED

#include "proto.h"
#include "yaml-cpp/yaml.h"
#include "H5Cpp.h"

// Pair of (field, weight) specifiers
struct FieldWeightPair {
  int field;                                 // Field index
  int weight;                                // Weight index (or alias)
  int hist = OUTSIDE;                        // Histogram index

  FieldWeightPair() = default;
  FieldWeightPair(const int field, const int weight, const int hist = OUTSIDE) : field(field), weight(weight), hist(hist) {};

  bool operator==(const FieldWeightPair& other) const {
    return (field == other.field) && (weight == other.weight) && (hist == other.hist);
  }

  bool operator<(const FieldWeightPair& other) const {
    if (field < other.field)
      return true;
    if (weight < other.weight)
      return true;
    if (hist < other.hist)
      return true;
    return false;
  }
};

class Simulation {
protected:
  Simulation() {};                           // Do not allow object creation

  // Cosmology variables
  bool cosmological = false;                 // Flag for cosmological simulations
  double Omega0 = 0.3111;                    // Matter density [rho_crit_0]
  double OmegaB = 0.04897;                   // Baryon density [rho_crit_0]
  double h100 = 0.6766;                      // Hubble constant [100 km/s/Mpc]
  double z = 0.;                             // Simulation redshift
  double d_L = 1.;                           // Luminosity distance [cm]
  double sim_time = -1.;                     // Simulation time [s]

  double calculate_d_L(const double z);      // Luminosity distance [cm]
  void camera_config(YAML::Node& file);      // General camera info
  void after_camera_config(YAML::Node& file); // After camera setup
  void image_config(YAML::Node& file);       // General image info
  void slit_config(YAML::Node& file);        // General slit info
  void cube_config(YAML::Node& file);        // General cube info
  void radial_image_config(YAML::Node& file); // General radial info
  void radial_cube_config(YAML::Node& file); // General radial cube info
  void map_config(YAML::Node& file);         // General map info
  void group_map_config(YAML::Node& file);   // General group map info
  void subhalo_map_config(YAML::Node& file); // General subhalo map info
  void radial_map_config(YAML::Node& file);  // General radial map info
  void group_radial_map_config(YAML::Node& file); // General group radial map info
  void subhalo_radial_map_config(YAML::Node& file); // General subhalo radial map info
  void group_radial_avg_config(YAML::Node& file); // General group radial avg info
  void subhalo_radial_avg_config(YAML::Node& file); // General subhalo radial avg info
  void cube_map_config(YAML::Node& file);    // General radial spectral map info
  void projection_config(YAML::Node& file);  // General projections info
  void escape_config(YAML::Node& file);      // Escape info
  void gas_config(YAML::Node& file);         // General gas info
  void setup_cameras();                      // General setup for cameras
  void setup_projections();                  // General setup for projections
  void setup_escape();                       // General setup for escape
  void print_cameras();                      // Print camera parameters
  void print_line_info();                    // Print line constants and parameters
  void write_camera_info(const H5::H5Object& f, const bool globals, const bool locals); // Write camera parameters
  void write_dust_info(const H5::H5File& f); // Write dust parameters
  void write_sim_info(const H5::H5File& f);  // General simulation info

private:
  // Ensure implementation of required virtual functions
  void module_error(const string function) {
    error(function + "() is not implemented for the " + module + " module.");
  };
  virtual void module_config([[maybe_unused]] YAML::Node& file) { module_error("module_config"); }
  virtual void read_hdf5();                  // Read initial conditions
  virtual void setup() { module_error("setup"); } // Module specific setup
  virtual void print_info() { module_error("print_info"); } // Print module info
  virtual void write_blank();                // Write blank output file
  virtual void write_hdf5();                 // Write output file
  virtual void write_module([[maybe_unused]] const H5::H5File& f) { module_error("write_module"); } // Module output

public:
  void setup_config();                       // General configuration
  void initialize();                         // General initialization
  virtual void run() { module_error("run"); }
  void finalize();                           // General finalization

  // Calculate the minimum distance to the bbox
  inline double min_bbox_distance(const Vec3& center) {
    const Vec3 dist_L = center - bbox[0];    // Left bbox distances
    const Vec3 dist_R = bbox[1] - center;    // Right bbox distances
    const double min_L = dist_L.min();       // Min left bbox distance
    const double min_R = dist_R.min();       // Min right bbox distance
    return (min_L < min_R) ? min_L : min_R;  // Min bbox distance
  }

  // Calculate the maximum distance to the bbox
  inline double max_bbox_distance(const Vec3& center) {
    const Vec3 dist_L = center - bbox[0];    // Left bbox distances
    const Vec3 dist_R = bbox[1] - center;    // Right bbox distances
    const double max_L = dist_L.max();       // Max left bbox distance
    const double max_R = dist_R.max();       // Max right bbox distance
    return (max_L > max_R) ? max_L : max_R;  // Max bbox distance
  }
};

#endif // SIMULATION_INCLUDED
