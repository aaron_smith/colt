/***********
 * vars.cc *
 ***********

 * General global variable declarations (defaults).

*/

#include "proto.h"

// Main control variables
string executable  = "";                     // Executable name (command line)
string config_file = "";                     // Config file name (command line)
string snap_str    = "";                     // Snapshot number (option command line)
int snap_num       = -1;                     // Snapshot number (converted from string)
string halo_str    = "";                     // Group or subhalo number (option command line)
int halo_num       = -3;                     // Group or subhalo number (converted from string)
string init_dir    = "";                     // Initial conditions directory (optional)
string init_subdir = "";                     // Initial conditions subdirectory name (optional)
string init_base   = "";                     // Initial conditions file base (optional)
string init_ext    = "hdf5";                 // Initial conditions file extension
string init_file   = "";                     // Initial conditions file (required)
string output_dir  = "output";               // Output directory name
string output_subdir = "";                   // Output subdirectory name
string output_base = "colt";                 // Output file base name
string output_ext  = "hdf5";                 // Output file extension
string module      = "mcrt";                 // Module type: mcrt, projections
bool verbose       = false;                  // Verbose output for tests and debugging
bool select_subhalo = true;                  // Specified a subhalo
unsigned long long seed;                     // Seed for random number generator

// Supplementary files
bool save_connections = true;                // Save voronoi connections to a file
bool save_circulators = true;                // Save voronoi circulators to a file
string cgal_file = "";                       // CGAL connectivity file (optional)
string group_file = "";                      // Group catalog file (optional)
string subhalo_file = "";                    // Subhalo catalog file (optional)
string abundances_file = "";                 // Abundances initial conditions file (optional)
string abundances_output_file = "";          // Abundances output file (optional)

// MPI and OpenMP variables
int rank;                                    // MPI local rank
int n_ranks;                                 // Number of MPI processes
int thread;                                  // OpenMP local thread
int n_threads;                               // Number of OpenMP threads
bool root;                                   // True if rank is root
bool load_balancing = false;                 // Use MPI load balancing algorithms

// Lists of data fields
int n_active_fields = 0;                     // Number of active fields
vector<int> active_fields;                   // List of active field indices
array<Field, n_fields> fields;               // File dataset fields

// Setup helper function
static inline void setup_field(const int i, const string& name, const string& cgs = "") {
  fields[i].name = name;                     // Field name [brief]
  fields[i].cgs = cgs;                       // Field units [cgs]
}

static int setup_fields() {
  setup_field(Density, "rho", "g/cm^3");     // Gas density [g/cm^3]
  setup_field(Volume, "V", "cm^3");          // Volume [cm^3]
  setup_field(Temperature, "T", "K");        // Temperature [K]
  setup_field(DustTemperature, "T_dust", "K"); // Dust temperature [K]
  setup_field(InternalEnergy, "e_int", "cm^2/s^2"); // Internal energy [cm^2/s^2]
  setup_field(HydrogenDensity, "n_H", "cm^-3"); // Hydrogen number density [cm^-3]
  setup_field(HeliumDensity, "n_He", "cm^-3"); // Helium number density [cm^-3]
  setup_field(CarbonDensity, "n_C", "cm^-3"); // Carbon number density [cm^-3]
  setup_field(NitrogenDensity, "n_N", "cm^-3"); // Nitrogen number density [cm^-3]
  setup_field(OxygenDensity, "n_O", "cm^-3"); // Oxygen number density [cm^-3]
  setup_field(NeonDensity, "n_Ne", "cm^-3"); // Neon number density [cm^-3]
  setup_field(MagnesiumDensity, "n_Mg", "cm^-3"); // Magnesium number density [cm^-3]
  setup_field(SiliconDensity, "n_Si", "cm^-3"); // Silicon number density [cm^-3]
  setup_field(SulferDensity, "n_S", "cm^-3"); // Sulfer number density [cm^-3]
  setup_field(IronDensity, "n_Fe", "cm^-3"); // Iron number density [cm^-3]
  setup_field(LowerDensity, "n_lower", "cm^-3"); // Number density of lower transition [cm^-3]
  setup_field(UpperDensity, "n_upper", "cm^-3"); // Number density of upper transition [cm^-3]
  setup_field(HI_Fraction, "x_HI");          // x_HI = n_HI / n_H
  setup_field(HII_Fraction, "x_HII");        // x_HII = n_HII / n_H
  setup_field(H2_Fraction, "x_H2");          // x_H2 = n_H2 / n_H
  setup_field(HeI_Fraction, "x_HeI");        // x_HeI = n_HeI / n_He
  setup_field(HeII_Fraction, "x_HeII");      // x_HeII = n_HeII / n_He
  setup_field(HeIII_Fraction, "x_HeIII");    // x_HeIII = n_HeIII / n_He
  setup_field(CI_Fraction, "x_CI");          // x_CI = n_CI / n_C
  setup_field(CII_Fraction, "x_CII");        // x_CII = n_CII / n_C
  setup_field(CIII_Fraction, "x_CIII");      // x_CIII = n_CIII / n_C
  setup_field(CIV_Fraction, "x_CIV");        // x_CIV = n_CIV / n_C
  setup_field(CV_Fraction, "x_CV");          // x_CV = n_CV / n_C
  setup_field(CVI_Fraction, "x_CVI");        // x_CVI = n_CVI / n_C
  setup_field(CVII_Fraction, "x_CVII");      // x_CVII = n_CVII / n_C
  setup_field(NI_Fraction, "x_NI");          // x_NI = n_NI / n_N
  setup_field(NII_Fraction, "x_NII");        // x_NII = n_NII / n_N
  setup_field(NIII_Fraction, "x_NIII");      // x_NIII = n_NIII / n_N
  setup_field(NIV_Fraction, "x_NIV");        // x_NIV = n_NIV / n_N
  setup_field(NV_Fraction, "x_NV");          // x_NV = n_NV / n_N
  setup_field(NVI_Fraction, "x_NVI");        // x_NVI = n_NVI / n_N
  setup_field(NVII_Fraction, "x_NVII");      // x_NVII = n_NVII / n_N
  setup_field(NVIII_Fraction, "x_NVIII");    // x_NVIII = n_NVIII / n_N
  setup_field(OI_Fraction, "x_OI");          // x_OI = n_OI / n_O
  setup_field(OII_Fraction, "x_OII");        // x_OII = n_OII / n_O
  setup_field(OIII_Fraction, "x_OIII");      // x_OIII = n_OIII / n_O
  setup_field(OIV_Fraction, "x_OIV");        // x_OIV = n_OIV / n_O
  setup_field(OV_Fraction, "x_OV");          // x_OV = n_OV / n_O
  setup_field(OVI_Fraction, "x_OVI");        // x_OVI = n_OVI / n_O
  setup_field(OVII_Fraction, "x_OVII");      // x_OVII = n_OVII / n_O
  setup_field(OVIII_Fraction, "x_OVIII");    // x_OVIII = n_OVIII / n_O
  setup_field(OIX_Fraction, "x_OIX");        // x_OIX = n_OIX / n_O
  setup_field(NeI_Fraction, "x_NeI");        // x_NeI = n_NeI / n_Ne
  setup_field(NeII_Fraction, "x_NeII");      // x_NeII = n_NeII / n_Ne
  setup_field(NeIII_Fraction, "x_NeIII");    // x_NeIII = n_NeIII / n_Ne
  setup_field(NeIV_Fraction, "x_NeIV");      // x_NeIV = n_NeIV / n_Ne
  setup_field(NeV_Fraction, "x_NeV");        // x_NeV = n_NeV / n_Ne
  setup_field(NeVI_Fraction, "x_NeVI");      // x_NeVI = n_NeVI / n_Ne
  setup_field(NeVII_Fraction, "x_NeVII");    // x_NeVII = n_NeVII / n_Ne
  setup_field(NeVIII_Fraction, "x_NeVIII");  // x_NeVIII = n_NeVIII / n_Ne
  setup_field(NeIX_Fraction, "x_NeIX");      // x_NeIX = n_NeIX / n_Ne
  setup_field(MgI_Fraction, "x_MgI");        // x_MgI = n_MgI / n_Mg
  setup_field(MgII_Fraction, "x_MgII");      // x_MgII = n_MgII / n_Mg
  setup_field(MgIII_Fraction, "x_MgIII");    // x_MgIII = n_MgIII / n_Mg
  setup_field(MgIV_Fraction, "x_MgIV");      // x_MgIV = n_MgIV / n_Mg
  setup_field(MgV_Fraction, "x_MgV");        // x_MgV = n_MgV / n_Mg
  setup_field(MgVI_Fraction, "x_MgVI");      // x_MgVI = n_MgVI / n_Mg
  setup_field(MgVII_Fraction, "x_MgVII");    // x_MgVII = n_MgVII / n_Mg
  setup_field(MgVIII_Fraction, "x_MgVIII");  // x_MgVIII = n_MgVIII / n_Mg
  setup_field(MgIX_Fraction, "x_MgIX");      // x_MgIX = n_MgIX / n_Mg
  setup_field(MgX_Fraction, "x_MgX");        // x_MgX = n_MgX / n_Mg
  setup_field(MgXI_Fraction, "x_MgXI");      // x_MgXI = n_MgXI / n_Mg
  setup_field(SiI_Fraction, "x_SiI");        // x_SiI = n_SiI / n_Si
  setup_field(SiII_Fraction, "x_SiII");      // x_SiII = n_SiII / n_Si
  setup_field(SiIII_Fraction, "x_SiIII");    // x_SiIII = n_SiIII / n_Si
  setup_field(SiIV_Fraction, "x_SiIV");      // x_SiIV = n_SiIV / n_Si
  setup_field(SiV_Fraction, "x_SiV");        // x_SiV = n_SiV / n_Si
  setup_field(SiVI_Fraction, "x_SiVI");      // x_SiVI = n_SiVI / n_Si
  setup_field(SiVII_Fraction, "x_SiVII");    // x_SiVII = n_SiVII / n_Si
  setup_field(SiVIII_Fraction, "x_SiVIII");  // x_SiVIII = n_SiVIII / n_Si
  setup_field(SiIX_Fraction, "x_SiIX");      // x_SiIX = n_SiIX / n_Si
  setup_field(SiX_Fraction, "x_SiX");        // x_SiX = n_SiX / n_Si
  setup_field(SiXI_Fraction, "x_SiXI");      // x_SiXI = n_SiXI / n_Si
  setup_field(SiXII_Fraction, "x_SiXII");    // x_SiXII = n_SiXII / n_Si
  setup_field(SiXIII_Fraction, "x_SiXIII");  // x_SiXIII = n_SiXIII / n_Si
  setup_field(SI_Fraction, "x_SI");          // x_SI = n_SI / n_S
  setup_field(SII_Fraction, "x_SII");        // x_SII = n_SII / n_S
  setup_field(SIII_Fraction, "x_SIII");      // x_SIII = n_SIII / n_S
  setup_field(SIV_Fraction, "x_SIV");        // x_SIV = n_SIV / n_S
  setup_field(SV_Fraction, "x_SV");          // x_SV = n_SV / n_S
  setup_field(SVI_Fraction, "x_SVI");        // x_SVI = n_SVI / n_S
  setup_field(SVII_Fraction, "x_SVII");      // x_SVII = n_SVII / n_S
  setup_field(SVIII_Fraction, "x_SVIII");    // x_SVIII = n_SVIII / n_S
  setup_field(SIX_Fraction, "x_SIX");        // x_SIX = n_SIX / n_S
  setup_field(SX_Fraction, "x_SX");          // x_SX = n_SX / n_S
  setup_field(SXI_Fraction, "x_SXI");        // x_SXI = n_SXI / n_S
  setup_field(SXII_Fraction, "x_SXII");      // x_SXII = n_SXII / n_S
  setup_field(SXIII_Fraction, "x_SXIII");    // x_SXIII = n_SXIII / n_S
  setup_field(SXIV_Fraction, "x_SXIV");      // x_SXIV = n_SXIV / n_S
  setup_field(SXV_Fraction, "x_SXV");        // x_SXV = n_SXV / n_S
  setup_field(FeI_Fraction, "x_FeI");        // x_FeI = n_FeI / n_Fe
  setup_field(FeII_Fraction, "x_FeII");      // x_FeII = n_FeII / n_Fe
  setup_field(FeIII_Fraction, "x_FeIII");    // x_FeIII = n_FeIII / n_Fe
  setup_field(FeIV_Fraction, "x_FeIV");      // x_FeIV = n_FeIV / n_Fe
  setup_field(FeV_Fraction, "x_FeV");        // x_FeV = n_FeV / n_Fe
  setup_field(FeVI_Fraction, "x_FeVI");      // x_FeVI = n_FeVI / n_Fe
  setup_field(FeVII_Fraction, "x_FeVII");    // x_FeVII = n_FeVII / n_Fe
  setup_field(FeVIII_Fraction, "x_FeVIII");  // x_FeVIII = n_FeVIII / n_Fe
  setup_field(FeIX_Fraction, "x_FeIX");      // x_FeIX = n_FeIX / n_Fe
  setup_field(FeX_Fraction, "x_FeX");        // x_FeX = n_FeX / n_Fe
  setup_field(FeXI_Fraction, "x_FeXI");      // x_FeXI = n_FeXI / n_Fe
  setup_field(FeXII_Fraction, "x_FeXII");    // x_FeXII = n_FeXII / n_Fe
  setup_field(FeXIII_Fraction, "x_FeXIII");  // x_FeXIII = n_FeXIII / n_Fe
  setup_field(FeXIV_Fraction, "x_FeXIV");    // x_FeXIV = n_FeXIV / n_Fe
  setup_field(FeXV_Fraction, "x_FeXV");      // x_FeXV = n_FeXV / n_Fe
  setup_field(FeXVI_Fraction, "x_FeXVI");    // x_FeXVI = n_FeXVI / n_Fe
  setup_field(FeXVII_Fraction, "x_FeXVII");  // x_FeXVII = n_FeXVII / n_Fe
  setup_field(ElectronAbundance, "x_e");     // Electron fraction: x_e = n_e / n_H
  setup_field(PhotoheatingRate, "G_ion", "erg/s"); // Photoheating rate [erg/s]
  setup_field(StarFormationRate, "SFR", "Msun/yr"); // Star formation rate [Msun/yr]
  setup_field(VelocitySquared, "v2", "cm^2/s^2"); // Velocity^2 [cm^2/s^2]
  setup_field(VelocitySquaredLOS, "v2_LOS", "cm^2/s^2"); // Velocity_LOS^2 [cm^2/s^2]
  setup_field(VelocityLOS, "v_LOS", "cm/s"); // Velocity_LOS [cm/s]
  setup_field(VelocityX, "v_x", "cm/s");     // Velocity_X [cm/s]
  setup_field(VelocityY, "v_y", "cm/s");     // Velocity_Y [cm/s]
  setup_field(MagneticFieldSquared, "B2", "G^2"); // Magnetic field^2 [G^2]
  setup_field(MagneticFieldLOS, "B_LOS", "G"); // Magnetic field LOS [G]
  setup_field(MagneticFieldX, "B_x", "G");   // Magnetic field X [G]
  setup_field(MagneticFieldY, "B_y", "G");   // Magnetic field Y [G]
  setup_field(HydrogenFraction, "X");        // Mass fraction of hydrogen
  setup_field(HeliumFraction, "Y");          // Mass fraction of helium
  setup_field(Metallicity, "Z");             // Gas metallicity [mass fraction]
  setup_field(CarbonMetallicity, "Z_C");     // Carbon metallicity [mass fraction]
  setup_field(NitrogenMetallicity, "Z_N");   // Nitrogen metallicity [mass fraction]
  setup_field(OxygenMetallicity, "Z_O");     // Oxygen metallicity [mass fraction]
  setup_field(NeonMetallicity, "Z_Ne");      // Neon metallicity [mass fraction]
  setup_field(MagnesiumMetallicity, "Z_Mg"); // Magnesium metallicity [mass fraction]
  setup_field(SiliconMetallicity, "Z_Si");   // Silicon metallicity [mass fraction]
  setup_field(SulferMetallicity, "Z_S");     // Sulfer metallicity [mass fraction]
  setup_field(IronMetallicity, "Z_Fe");      // Iron metallicity [mass fraction]
  setup_field(DustMetallicity, "D");         // Dust-to-gas ratio [mass fraction]
  setup_field(SilicateMetallicity, "D_S");   // Silicate dust-to-gas ratio [mass fraction]
  setup_field(ElectronCoefficient, "k_e", "1/cm"); // Absorption coefficient of electrons [1/cm]
  setup_field(LineCoefficient, "k_0", "1/cm"); // Absorption coefficient of primary line [1/cm]
  setup_field(DoubletCoefficient, "kp_0", "1/cm"); // Absorption coefficient of doublet line [1/cm]
  setup_field(DustCoefficient, "k_dust", "1/cm"); // Absorption coefficient of dust [1/cm]
  setup_field(DustDensity, "rho_dust", "g/cm^3"); // Dust density [g/cm^3]
  setup_field(GraphiteDensity, "rho_dust_G", "g/cm^3"); // Graphite dust density [g/cm^3]
  setup_field(SilicateDensity, "rho_dust_S", "g/cm^3"); // Silicate dust density [g/cm^3]
  setup_field(DampingParameter, "a");        // "Damping parameter"
  setup_field(NonLocalATau0, "atau");        // Nonlocal estimate of a * tau for each cell
  setup_field(LineEmissivity, "j_line", "erg/s/cm^3"); // Line emissivity [erg/s/cm^3]
  setup_field(DoubletLineEmissivity, "jp_line", "erg/s/cm^3"); // Doublet line emissivity [erg/s/cm^3]
  setup_field(EnergyDensity, "u_rad", "erg/cm^3"); // Radiation energy density [erg/cm^3]
  setup_field(RadialAcceleration, "a_rad_r", "cm/s^2"); // Radial radiation acceleration [cm/s^2]
  setup_field(RadialAccelerationScat, "a_rad_r_scat", "cm/s^2"); // Radial radiation acceleration (scattering-based) [cm/s^2]
  setup_field(RadialPressure, "P_rad_r", "erg/cm^3"); // Radial radiation pressure [erg/cm^3]
  setup_field(SourceFraction, "f_src_r");    // Cumulative source fraction
  setup_field(DustEscapeFraction, "tau_dust_f_esc"); // Dust escape fraction
  setup_field(EscapeFraction, "f_esc_r");    // Cumulative escape fraction
  setup_field(RescaledEscapeFraction, "rescaled_f_esc_r"); // Cumulative rescaled escape fraction
  setup_field(TrappingRatio, "trap_r");      // Cumulative t_trap / t_light
  setup_field(ForceMultiplier, "M_F_r");     // Cumulative force multiplier
  setup_field(ForceMultiplierScat, "M_F_r_scat"); // Cumulative force multiplier (scattering-based)
  setup_field(ForceMultiplierSource, "M_F_src"); // Cumulative force multiplier (by source)
  setup_field(ForceMultiplierSourceScat, "M_F_src_scat"); // Cumulative force multiplier (by source, scattering-based)
  setup_field(PressureRatio, "P_u_r");       // Cumulative pressure-to-energy density ratio (P/u)
  setup_field(DensitySquared, "rho2", "g^2/cm^6"); // Gas density^2 [g^2/cm^6]
  setup_field(DensitySquaredHI, "rho2HI", "g^2/cm^6"); // Gas x_HI * density^2 [g^2/cm^6]
  setup_field(StellarDensity, "rho_star", "g/cm^3"); // Stellar density [g/cm^3]
  setup_field(IonizationFront, "ion_front"); // Ionization front (Gaussian centered on x_HI)
  return 0;                                  // Return success
}
static const int setup_fields_flag = setup_fields(); // Setup helper

const strings atom_names = {                 // Atom names
  "hydrogen", "helium", "carbon", "nitrogen", "oxygen", "neon", "magnesium", "silicon", "sulfer", "iron"
};
const strings atom_symbols = {               // Atom symbols
  "H", "He", "C", "N", "O", "Ne", "Mg", "Si", "S", "Fe"
};
const strings ion_names = {                  // Ionization state names
  "HI", "HeI", "HeII",                       // Species names
  "CI", "CII", "CIII", "CIV", "CV", "CVI",
  "NI", "NII", "NIII", "NIV", "NV", "NVI", "NVII",
  "OI", "OII", "OIII", "OIV", "OV", "OVI", "OVII", "OVIII",
  "NeI", "NeII", "NeIII", "NeIV", "NeV", "NeVI", "NeVII", "NeVIII",
  "MgI", "MgII", "MgIII", "MgIV", "MgV", "MgVI", "MgVII", "MgVIII", "MgIX", "MgX",
  "SiI", "SiII", "SiIII", "SiIV", "SiV", "SiVI", "SiVII", "SiVIII", "SiIX", "SiX", "SiXI", "SiXII",
  "SI", "SII", "SIII", "SIV", "SV", "SVI", "SVII", "SVIII", "SIX", "SX", "SXI", "SXII", "SXIII", "SXIV",
  "FeI", "FeII", "FeIII", "FeIV", "FeV", "FeVI", "FeVII", "FeVIII", "FeIX", "FeX", "FeXI", "FeXII", "FeXIII", "FeXIV", "FeXV", "FeXVI"
};
int n_active_atoms = 0;                      // Number of active atoms
vector<int> active_atoms;                    // Active atom indices
vector<int> ion_counts;                      // Active atom number of active ions
int n_active_ions = 0;                       // Number of active ions
int n_active_ions_atoms = 0;                 // Number of active ions and atoms
vector<int> beg_ions;                        // Active atom beginning ion indices
vector<int> end_ions;                        // Active atom ending ion indices
vector<int> active_ions;                     // Active ionization state indices
vector<int> n_indices;                       // Atom number density data indices
vector<int> x_indices;                       // Ionization fraction data indices
vector<double> ions_eV;                      // Ionization threshold energies [eV]
vector<double> ions_erg;                     // Ionization threshold energies [erg]
int n_active_ion_stats = 0;                  // Number of active ionization statistics
vector<int> active_ion_stats;                // Active ionization statistics indices
vector<double> bin_edges_eV;                 // Bin energy edges [eV]
strings bin_names;                           // Bin energy edge names

// List of possible lines (carrier, state, type)
int line_carrier = Hydrogen_Line;            // Carrier of line
int line_state = State_I;                    // State of line
int line_type = HI_Line;                     // Type of line

// Basic grid data
int n_cells = 1;                             // Number of cells
int n_stars = 0;                             // Number of stars
double n_cells_eff;                          // Effective number of cells: 1/<w>
double n_cells_effp;                         // Effective number of doublet cells: 1/<w>
double n_stars_eff;                          // Effective number of stars: 1/<w>
vector<Vec3> r;                              // Cell position [cm]
vector<double>& V = fields[Volume].data;     // Cell volume [cm^3]
array<Vec3, 2> bbox;                         // Bounding box [cm]
double max_bbox_width;                       // Max bbox width [cm]

// Halo catalog data
bool use_all_halos = false;                  // Use all halos in the catalog
int halo_index = -1;                         // Specific halo index
Vec3 halo_position;                          // Specific halo position [cm]
double halo_radius = 0.;                     // Specific halo radius [cm]

// Group catalog data
bool use_all_groups = false;                 // Use all groups in the catalog
bool filter_group_catalog = false;           // Filter the group catalog
int n_groups = 0, n_ugroups = 0;             // Number of groups (filtered and unfiltered)
vector<int> group_id, ugroup_id;             // Group ID (filtered and unfiltered)
vector<Vec3> v_grp;                          // Group velocity [km/s]
vector<Vec3> r_grp_vir, r_grp_gal;           // Group position [cm] (virial and galaxy)
vector<double> R_grp_vir, R_grp_gal;         // Group radius [cm]
vector<double> R2_grp_vir, R2_grp_gal;       // Group radius^2 [cm^2]
vector<int> group_id_cell;                   // Gas group IDs
vector<int> group_id_star;                   // Star group IDs

// Subhalo catalog data
bool use_all_subhalos = false;               // Use all subhalos in the catalog
bool filter_subhalo_catalog = false;         // Filter the subhalo catalog
int n_subhalos = 0, n_usubhalos = 0;         // Number of subhalos (filtered and unfiltered)
vector<int> subhalo_id, usubhalo_id;         // Subhalo ID (filtered and unfiltered)
vector<Vec3> v_sub;                          // Subhalo velocity [km/s]
vector<Vec3> r_sub_vir, r_sub_gal;           // Subhalo position [cm] (virial and galaxy)
vector<double> R_sub_vir, R_sub_gal;         // Subhalo radius [cm]
vector<double> R2_sub_vir, R2_sub_gal;       // Subhalo radius^2 [cm^2]
vector<int> subhalo_id_cell;                 // Gas subhalo IDs
vector<int> subhalo_id_star;                 // Star subhalo IDs

// Cartesian grid data
int nx = 0;                                  // Number of x cells
int ny = 0;                                  // Number of y cells
int nz = 0;                                  // Number of z cells
int n2 = 0;                                  // 2D hyperslab size
double wx = 0.;                              // Cell x width [cm]
double wy = 0.;                              // Cell y width [cm]
double wz = 0.;                              // Cell z width [cm]
double dV = 0.;                              // Cell volume [cm^3]

// Octree grid data (size = n_cells)
int n_leafs = 1;                             // Number of leaf cells
vector<int> parent;                          // Cell parent
vector<array<int, 8>> child;                 // Child cell indices
vector<bool> child_check;                    // True if cell has children
vector<double> w;                            // Cell width [cm]

// Voronoi tessellation data
bool avoid_edges = true;                     // Avoid ray-tracing through edge cells
bool inner_edges = false;                    // Flag to also include inner edge cells
bool output_inner_edges = true;              // Flag to output inner edge cells
int n_edges;                                 // Number of edge Voronoi cells
int n_inner_edges;                           // Number of inner edge Voronoi cells
neib_t n_neighbors_tot;                      // Total number of neighbors for all cells
circ_t n_circulators_tot;                    // Total number of circulators for all faces
vector<bool> edge_flag;                      // Flag for edge cells
vector<vector<Face>> faces;                  // Face connection data

// Additional data (size = n_cells)
bool continuous_velocity = true;             // Use a continuous velocity field
vector<Vec3> v;                              // Bulk velocity [vth]
vector<double> dvdr;                         // Bulk velocity gradient [vth/cm]
vector<double> v0;                           // Velocity extrapolation [vth/cm]
vector<double> r_c;                          // Cell center position [cm]
vector<Vec3> B;                              // Magnetic field [G]
vector<Vec3> a_rad, a_rad_scat;              // Radiation acceleration [cm/s^2]
vector<double>& rho = fields[Density].data;  // Density [g/cm^3]
vector<double>& T = fields[Temperature].data; // Temperature [K]
vector<double>& e_int = fields[InternalEnergy].data; // Internal energy [cm^2/s^2]
vector<double>& n_H = fields[HydrogenDensity].data; // Hydrogen number density [cm^-3]
vector<double>& n_He = fields[HeliumDensity].data; // Helium number density [cm^-3]
vector<double>& n_C = fields[CarbonDensity].data; // Carbon number density [cm^-3]
vector<double>& n_N = fields[NitrogenDensity].data; // Nitrogen number density [cm^-3]
vector<double>& n_O = fields[OxygenDensity].data; // Oxygen number density [cm^-3]
vector<double>& n_Ne = fields[NeonDensity].data; // Neon number density [cm^-3]
vector<double>& n_Mg = fields[MagnesiumDensity].data; // Magnesium number density [cm^-3]
vector<double>& n_Si = fields[SiliconDensity].data; // Silicon number density [cm^-3]
vector<double>& n_S = fields[SulferDensity].data; // Sulfer number density [cm^-3]
vector<double>& n_Fe = fields[IronDensity].data; // Iron number density [cm^-3]
vector<double>& n_lower = fields[LowerDensity].data; // Number density of lower transition [cm^-3]
vector<double>& n_upper = fields[UpperDensity].data; // Number density of upper transition [cm^-3]
vector<double>& x_HI = fields[HI_Fraction].data; // x_HI = n_HI / n_H
vector<double>& x_HII = fields[HII_Fraction].data; // x_HII = n_HII / n_H
vector<double>& x_H2 = fields[H2_Fraction].data; // x_H2 = n_H2 / n_H
vector<double> &x_HeI = fields[HeI_Fraction].data, &x_HeII = fields[HeII_Fraction].data, &x_HeIII = fields[HeIII_Fraction].data; // x_ion = n_ion / n_atom
vector<double>& x_e = fields[ElectronAbundance].data; // Electron fraction: x_e = n_e / n_H
vector<double>& G_ion = fields[PhotoheatingRate].data; // Photoheating rate [erg/s]
vector<double>& SFR = fields[StarFormationRate].data; // Star formation rate [Msun/yr]
vector<double>& T_dust = fields[DustTemperature].data; // Dust temperature [K]
vector<double>& v2 = fields[VelocitySquared].data; // Velocity^2 [cm^2/s^2]
vector<double>& v2_LOS = fields[VelocitySquaredLOS].data; // Velocity_LOS^2 [cm^2/s^2]
vector<double>& v_LOS = fields[VelocityLOS].data; // Velocity_LOS [cm/s]
vector<double>& v_x = fields[VelocityX].data; // Velocity_X [cm/s]
vector<double>& v_y = fields[VelocityY].data; // Velocity_Y [cm/s]
vector<double>& B2 = fields[MagneticFieldSquared].data; // Magnetic field^2 [G^2]
vector<double>& B_LOS = fields[MagneticFieldLOS].data; // Magnetic field LOS [G]
vector<double>& B_x = fields[MagneticFieldX].data; // Magnetic field X [G]
vector<double>& B_y = fields[MagneticFieldY].data; // Magnetic field Y [G]
vector<double>& X = fields[HydrogenFraction].data; // Mass fraction of hydrogen
vector<double>& Y = fields[HeliumFraction].data; // Mass fraction of helium
vector<double>& Z = fields[Metallicity].data; // Gas metallicity [mass fraction]
vector<double>& Z_C = fields[CarbonMetallicity].data; // Carbon metallicity [mass fraction]
vector<double>& Z_N = fields[NitrogenMetallicity].data; // Nitrogen metallicity [mass fraction]
vector<double>& Z_O = fields[OxygenMetallicity].data; // Oxygen metallicity [mass fraction]
vector<double>& Z_Ne = fields[NeonMetallicity].data; // Neon metallicity [mass fraction]
vector<double>& Z_Mg = fields[MagnesiumMetallicity].data; // Magnesium metallicity [mass fraction]
vector<double>& Z_Si = fields[SiliconMetallicity].data; // Silicon metallicity [mass fraction]
vector<double>& Z_S = fields[SulferMetallicity].data; // Sulfer metallicity [mass fraction]
vector<double>& Z_Fe = fields[IronMetallicity].data; // Iron metallicity [mass fraction]
vector<double>& D = fields[DustMetallicity].data; // Dust-to-gas ratio [mass fraction]
vector<double>& D_S = fields[SilicateMetallicity].data; // Silicate dust-to-gas ratio [mass fraction]
vector<double>& k_e = fields[ElectronCoefficient].data; // Absorption coefficient of electrons [1/cm]
vector<double>& k_0 = fields[LineCoefficient].data; // Absorption coefficient of primary line [1/cm]
vector<double>& kp_0 = fields[DoubletCoefficient].data; // Absorption coefficient of doublet line [1/cm]
vector<double>& k_dust = fields[DustCoefficient].data; // Absorption coefficient of dust [1/cm]
vector<double>& rho_dust = fields[DustDensity].data; // Dust density [g/cm^3]
vector<double>& rho_dust_G = fields[GraphiteDensity].data; // Graphite dust density [g/cm^3]
vector<double>& rho_dust_S = fields[SilicateDensity].data; // Silicate dust density [g/cm^3]
vector<double>& a = fields[DampingParameter].data; // "Damping parameter"
vector<double>& atau = fields[NonLocalATau0].data; // Nonlocal estimate of a * tau for each cell
vector<double>& j_line = fields[LineEmissivity].data; // Line emissivity [erg/s/cm^3]
vector<double>& jp_line = fields[DoubletLineEmissivity].data; // Doublet line emissivity [erg/s/cm^3]
vector<double>& u_rad = fields[EnergyDensity].data; // Radiation energy density [erg/cm^3]
vector<double>& a_rad_r = fields[RadialAcceleration].data; // Radial radiation acceleration [cm/s^2]
vector<double>& a_rad_r_scat = fields[RadialAccelerationScat].data; // Radial radiation acceleration (scattering-based) [cm/s^2]
vector<double>& P_rad_r = fields[RadialPressure].data; // Radial radiation pressure [erg/cm^3]
vector<double>& f_src_r = fields[SourceFraction].data; // Cumulative source fraction
vector<double>& tau_dust_f_esc = fields[DustEscapeFraction].data; // Dust escape fraction
vector<double>& f_esc_r = fields[EscapeFraction].data; // Cumulative escape fraction
vector<double>& rescaled_f_esc_r = fields[RescaledEscapeFraction].data; // Cumulative rescaled escape fraction
vector<double>& trap_r = fields[TrappingRatio].data; // Cumulative t_trap / t_light
vector<double>& M_F_r = fields[ForceMultiplier].data; // Cumulative force multiplier
vector<double>& M_F_r_scat = fields[ForceMultiplierScat].data; // Cumulative force multiplier (scattering-based)
vector<double>& M_F_src = fields[ForceMultiplierSource].data; // Cumulative force multiplier (by source)
vector<double>& M_F_src_scat = fields[ForceMultiplierSourceScat].data; // Cumulative force multiplier (by source, scattering-based)
vector<double>& P_u_r = fields[PressureRatio].data; // Cumulative pressure-to-energy density ratio (P/u)
vector<double>& rho2 = fields[DensitySquared].data; // Density^2 [g^2/cm^6]
vector<double>& rho2HI = fields[DensitySquaredHI].data; // x_HI * Density^2 [g^2/cm^6]
vector<double>& rho_star = fields[StellarDensity].data; // Stellar density [g/cm^3]
vector<double>& ion_front = fields[IonizationFront].data; // Ionization front
dust_strings dust_models;                    // Dust model: SMC, MW, etc.
double T_sputter = 1e6;                      // Thermal sputtering cutoff [K]
double f_ion = 1.;                           // HII region survival fraction
double f_dust = 0.;                          // Fraction of metals locked in dust
#if graphite_scaled_PAH
double f_PAH = 0.01, fm1_PAH;                // Fraction of carbonaceous dust in PAHs
#endif
double& hydrogen_fraction = fields[HydrogenFraction].constant; // Constant hydrogen mass fraction
double& helium_fraction = fields[HeliumFraction].constant; // Constant helium mass fraction
double& metallicity = fields[Metallicity].constant; // Constant metallicity
bool mass_weighted_metallicities = false;    // Adopt mass-weighted metallicities
double dust_to_metal = -1.;                  // Constant dust-to-metal ratio
double& dust_to_gas = fields[DustMetallicity].constant; // Constant dust-to-gas ratio
double dust_boost = 0.;                      // Optional dust boost factor
double& constant_temperature = fields[Temperature].constant; // Constant temperature: T [K]
double T_floor = -1.;                        // Apply a temperature floor: T [K]
double T_rtol = 0.1;                         // Relative tolerance for temperature changes
double& electron_fraction = fields[ElectronAbundance].constant; // Constant electron fraction: x_e
bool use_internal_energy = false;            // Set temperature from the internal energy
bool set_density_from_mass = false;          // Calculate the density as mass / volume
bool read_density_as_mass = false;           // Read the density as a mass field
// Read aliases
bool &read_helium = fields[HeliumDensity].read; // Read n_He from the input file
bool &read_HI = fields[HI_Fraction].read, &read_HII = fields[HII_Fraction].read, &read_H2 = fields[H2_Fraction].read;
bool &read_HeI = fields[HeI_Fraction].read, &read_HeII = fields[HeII_Fraction].read, &read_HeIII = fields[HeIII_Fraction].read;
bool &read_electron_fraction = fields[ElectronAbundance].read; // Read x_e from the input file
bool &read_hydrogen_fraction = fields[HydrogenFraction].read; // Read mass fraction of hydrogen
bool &read_helium_fraction = fields[HeliumFraction].read; // Read mass fraction of helium
bool read_B = false;                         // Read magnetic field
bool save_line_emissivity = false;           // Save the line emissivity

// Star data for continuum radiative transfer  (size = n_stars)
int n_photons = 1;                           // Actual number of photon packets used
int n_photons_per_star = 0;                  // Scale the number of photons with stars
int n_photons_max = 1e8;                     // Maximum number of photons
vector<Vec3> r_star;                         // Star positions [cm]
vector<Vec3> v_star;                         // Star velocities [cm/s]
vector<double> L_cont_star;                  // Stellar continuum [erg/s/angstrom]
vector<double> m_massive_star;               // Star masses [Msun]
vector<double> m_init_star;                  // Initial star masses [Msun]
vector<double> Z_star;                       // Star metallicities [mass fraction]
vector<double> age_star;                     // Star ages [Gyr]
Vec3 r_AGN;                                  // AGN position [cm]
double Lbol_AGN;                             // AGN bolometric luminosity [erg/s]
bool cell_based_emission = false;            // Include cell-based sources
bool star_based_emission = false;            // Include star-based sources
bool AGN_emission = false;                   // Include an AGN source
bool read_continuum = false;                 // Read continuum fluxes [erg/s/angstrom]
bool read_m_massive_star = false;            // Read star masses [Msun]
bool read_m_init_star = false;               // Read initial star masses [Msun]
bool read_Z_star = false;                    // Read star metallicities [mass fraction]
bool read_age_star = false;                  // Read star ages [Gyr]
bool read_v_star = false;                    // Read star velocities [cm/s]

// Information about escape criteria
#if spherical_escape
Vec3 escape_center = 0;                      // Center of the escape region [cm]
double escape_radius = 0.;                   // Radius for spherical escape [cm]
double escape_radius_bbox = 0.;              // Radius for spherical escape [min distance to bbox edge]
double escape_radius_Rvir = 0.;              // Radius for spherical escape [selected halo virial radius]
double escape_epsilon;                       // Escape uncertainty [cm]
double escape_radius2;                       // Escape radius^2 [cm^2]
double emission_radius = 0.;                 // Radius for spherical emission [cm]
double emission_radius_bbox = 0.;            // Radius for spherical emission [min distance to bbox edge]
double emission_radius_Rvir = 0.;            // Radius for spherical emission [selected halo virial radius]
double emission_radius2;                     // Emission radius^2 [cm^2]
#endif
#if box_escape
array<Vec3, 2> escape_box;                   // Box for escape [cm]
array<Vec3, 2> emission_box;                 // Box for emission [cm]
#endif
#if streaming_escape
double max_streaming = positive_infinity;    // Maximum path length without scattering [cm]
#endif

// Information about dual grids
Vec3 radial_center = 0;                      // Center of the radial grid [cm]
Vec3 radial_motion = 0;                      // Motion of the radial grid [cm/s]
bool focus_radial_on_emission = false;       // Focus radial grid on emission (position)
bool shift_radial_on_emission = false;       // Shift radial grid on emission (velocity)

// Information about the cameras
bool have_cameras = false;                   // Camera-based output
int n_cameras = 0;                           // Number of cameras
int n_side = 0;                              // Healpix parameter for cameras
int n_rot = 0;                               // Number of rotation directions
Vec3 rotation_axis = {0.,0.,1.};             // Rotation axis, e.g. (0,0,1)
int n_side_atau = 1;                         // Healpix parameter for a*tau0
vector<Vec3> camera_directions;              // Normalized camera directions
vector<Vec3> camera_xaxes;                   // Camera "x-axis" direction vectors (x,y,z)
vector<Vec3> camera_yaxes;                   // Camera "y-axis" direction vectors (x,y,z)
Vec3 camera_center = 0;                      // Camera target position [cm]
Vec3 camera_motion = 0;                      // Camera target velocity [cm/s]
Vec3 camera_north = {0., 0., 1.};            // Camera north orientation
Vec3 r_light = 0;                            // Center of luminosity position [cm]
Vec3 r2_light = 0;                           // Center of luminosity position^2 [cm^2]
Vec3 v_light = 0;                            // Center of luminosity velocity [cm/s]
Vec3 v2_light = 0;                           // Center of luminosity velocity^2 [cm^2/s^2]
Vec3 j_light = 0;                            // Center of luminosity angular momentum [cm^2/s]
bool focus_cameras_on_emission = false;      // Focus cameras on emission (position)
bool shift_cameras_on_emission = false;      // Shift cameras on emission (velocity)
bool align_cameras_on_emission = false;      // Align cameras on emission (angular momentum)

// Information about group photons (observed, virial, galaxy)
vector<double> Ndot_grp, Ndot_grp_vir, Ndot_grp_gal, Ndot_HI_grp, Ndot_HI_grp_vir, Ndot_HI_grp_gal; // Group photon rate [photons/s]
vector<double> Ndot_stars_grp, Ndot_stars_grp_vir, Ndot_stars_grp_gal, Ndot_stars_HI_grp, Ndot_stars_HI_grp_vir, Ndot_stars_HI_grp_gal; // Group stellar photon rate [photons/s]
vector<double> Ndot2_stars_grp, Ndot2_stars_grp_vir, Ndot2_stars_grp_gal, Ndot2_stars_HI_grp, Ndot2_stars_HI_grp_vir, Ndot2_stars_HI_grp_gal; // Group stellar rates^2 [photons^2/s^2]
vector<double> n_stars_eff_grp, n_stars_eff_grp_vir, n_stars_eff_grp_gal, n_stars_eff_HI_grp, n_stars_eff_HI_grp_vir, n_stars_eff_HI_grp_gal; // Effective number of stars: 1/<w>
vector<double> Ndot_gas_grp, Ndot_gas_grp_vir, Ndot_gas_grp_gal, Ndot_gas_HI_grp, Ndot_gas_HI_grp_vir, Ndot_gas_HI_grp_gal; // Group gas photon rate [photons/s]
vector<double> Ndot2_gas_grp, Ndot2_gas_grp_vir, Ndot2_gas_grp_gal, Ndot2_gas_HI_grp, Ndot2_gas_HI_grp_vir, Ndot2_gas_HI_grp_gal; // Group gas rates^2 [photons^2/s^2]
vector<double> n_cells_eff_grp, n_cells_eff_grp_vir, n_cells_eff_grp_gal, n_cells_eff_HI_grp, n_cells_eff_HI_grp_vir, n_cells_eff_HI_grp_gal; // Effective number of cells: 1/<w>
vector<double> f_src_grp, f_src_grp_vir, f_src_grp_gal, f_src_HI_grp, f_src_HI_grp_vir, f_src_HI_grp_gal; // Group source fraction: sum(w0)
vector<double> f2_src_grp, f2_src_grp_vir, f2_src_grp_gal, f2_src_HI_grp, f2_src_HI_grp_vir, f2_src_HI_grp_gal; // Group squared f_src: sum(w0^2)
vector<double> n_photons_src_grp, n_photons_src_grp_vir, n_photons_src_grp_gal, n_photons_src_HI_grp, n_photons_src_HI_grp_vir, n_photons_src_HI_grp_gal; // Effective number of emitted photons: 1/<w0>
vector<double> f_esc_grp, f_esc_grp_vir, f_esc_grp_gal, f_esc_HI_grp, f_esc_HI_grp_vir, f_esc_HI_grp_gal; // Group escape fraction: sum(w)
vector<double> f2_esc_grp, f2_esc_grp_vir, f2_esc_grp_gal, f2_esc_HI_grp, f2_esc_HI_grp_vir, f2_esc_HI_grp_gal; // Group squared f_esc: sum(w^2)
vector<double> n_photons_esc_grp, n_photons_esc_grp_vir, n_photons_esc_grp_gal, n_photons_esc_HI_grp, n_photons_esc_HI_grp_vir, n_photons_esc_HI_grp_gal; // Effective number of escaped photons: 1/<w>
vector<double> f_abs_grp, f_abs_grp_vir, f_abs_grp_gal, f_abs_HI_grp, f_abs_HI_grp_vir, f_abs_HI_grp_gal; // Group dust absorption fraction
vector<double> f2_abs_grp, f2_abs_grp_vir, f2_abs_grp_gal, f2_abs_HI_grp, f2_abs_HI_grp_vir, f2_abs_HI_grp_gal; // Group squared f_abs: sum(w^2)
vector<double> n_photons_abs_grp, n_photons_abs_grp_vir, n_photons_abs_grp_gal, n_photons_abs_HI_grp, n_photons_abs_HI_grp_vir, n_photons_abs_HI_grp_gal; // Effective number of absorbed photons: 1/<w>
bool focus_groups_on_emission = false;       // Center groups on emission (position and velocity)
vector<Vec3> r_light_grp, v_light_grp;       // Group center of luminosity position [cm] and velocity [cm/s]
Image bin_Ndot_grp, bin_Ndot_grp_vir, bin_Ndot_grp_gal; // Group bin photon rate [photons/s]
Image bin_Ndot_stars_grp, bin_Ndot_stars_grp_vir, bin_Ndot_stars_grp_gal; // Group stellar bin photon rate [photons/s]
Image bin_Ndot2_stars_grp, bin_Ndot2_stars_grp_vir, bin_Ndot2_stars_grp_gal; // Group stellar bin rates^2 [photons^2/s^2]
Image bin_n_stars_eff_grp, bin_n_stars_eff_grp_vir, bin_n_stars_eff_grp_gal; // Bin effective number of stars: 1/<w>
Image bin_Ndot_gas_grp, bin_Ndot_gas_grp_vir, bin_Ndot_gas_grp_gal; // Group gas bin photon rate [photons/s]
Image bin_Ndot2_gas_grp, bin_Ndot2_gas_grp_vir, bin_Ndot2_gas_grp_gal; // Group gas bin rates^2 [photons^2/s^2]
Image bin_n_cells_eff_grp, bin_n_cells_eff_grp_vir, bin_n_cells_eff_grp_gal; // Bin effective number of cells: 1/<w>
Image bin_f_src_grp, bin_f_src_grp_vir, bin_f_src_grp_gal; // Group bin source fraction: sum(w0)
Image bin_f2_src_grp, bin_f2_src_grp_vir, bin_f2_src_grp_gal; // Group bin squared f_src: sum(w0^2)
Image bin_n_photons_src_grp, bin_n_photons_src_grp_vir, bin_n_photons_src_grp_gal; // Effective number of emitted photons: 1/<w0>
Image bin_f_esc_grp, bin_f_esc_grp_vir, bin_f_esc_grp_gal; // Group bin escape fraction: sum(w)
Image bin_f2_esc_grp, bin_f2_esc_grp_vir, bin_f2_esc_grp_gal; // Group bin squared f_esc: sum(w^2)
Image bin_n_photons_esc_grp, bin_n_photons_esc_grp_vir, bin_n_photons_esc_grp_gal; // Effective number of escaped photons: 1/<w>
Image bin_f_abs_grp, bin_f_abs_grp_vir, bin_f_abs_grp_gal; // Group bin dust absorption fraction
Image bin_f2_abs_grp, bin_f2_abs_grp_vir, bin_f2_abs_grp_gal; // Group bin squared f_abs: sum(w^2)
Image bin_n_photons_abs_grp, bin_n_photons_abs_grp_vir, bin_n_photons_abs_grp_gal; // Effective number of absorbed photons: 1/<w>
vector<double> Ndot_ugrp, Ndot_HI_ugrp;      // Unfiltered group photon rate [photons/s]
vector<double> Ndot_stars_ugrp, Ndot_stars_HI_ugrp; // Unfiltered group stellar photon rate [photons/s]
vector<double> Ndot2_stars_ugrp, Ndot2_stars_HI_ugrp; // Unfiltered group stellar rates^2 [photons^2/s^2]
vector<double> n_stars_eff_ugrp, n_stars_eff_HI_ugrp; // Effective number of stars: 1/<w>
vector<double> Ndot_gas_ugrp, Ndot_gas_HI_ugrp; // Unfiltered group gas photon rate [photons/s]
vector<double> Ndot2_gas_ugrp, Ndot2_gas_HI_ugrp; // Unfiltered group gas rates^2 [photons^2/s^2]
vector<double> n_cells_eff_ugrp, n_cells_eff_HI_ugrp; // Effective number of cells: 1/<w>
vector<double> f_src_ugrp, f_src_HI_ugrp;    // Unfiltered group source fraction: sum(w0)
vector<double> f2_src_ugrp, f2_src_HI_ugrp;  // Unfiltered group squared f_src: sum(w0^2)
vector<double> n_photons_src_ugrp, n_photons_src_HI_ugrp; // Effective number of emitted photons: 1/<w0>
Image bin_Ndot_ugrp;                         // Unfiltered group bin photon rate [photons/s]
Image bin_Ndot_stars_ugrp;                   // Unfiltered group stellar bin photon rate [photons/s]
Image bin_Ndot2_stars_ugrp;                  // Unfiltered group stellar bin rates^2 [photons^2/s^2]
Image bin_n_stars_eff_ugrp;                  // Bin effective number of stars: 1/<w>
Image bin_Ndot_gas_ugrp;                     // Unfiltered group gas bin photon rate [photons/s]
Image bin_Ndot2_gas_ugrp;                    // Unfiltered group gas bin rates^2 [photons^2/s^2]
Image bin_n_cells_eff_ugrp;                  // Bin effective number of cells: 1/<w>
Image bin_f_src_ugrp;                        // Unfiltered group bin source fraction: sum(w0)
Image bin_f2_src_ugrp;                       // Unfiltered group bin squared f_src: sum(w0^2)
Image bin_n_photons_src_ugrp;                // Effective number of emitted photons: 1/<w0>

// Information about subhalo photons (observed, virial, galaxy)
vector<double> Ndot_sub, Ndot_sub_vir, Ndot_sub_gal, Ndot_HI_sub, Ndot_HI_sub_vir, Ndot_HI_sub_gal; // Subhalo photon rate [photons/s]
vector<double> Ndot_stars_sub, Ndot_stars_sub_vir, Ndot_stars_sub_gal, Ndot_stars_HI_sub, Ndot_stars_HI_sub_vir, Ndot_stars_HI_sub_gal; // Subhalo stellar photon rate [photons/s]
vector<double> Ndot2_stars_sub, Ndot2_stars_sub_vir, Ndot2_stars_sub_gal, Ndot2_stars_HI_sub, Ndot2_stars_HI_sub_vir, Ndot2_stars_HI_sub_gal; // Subhalo stellar rates^2 [photons^2/s^2]
vector<double> n_stars_eff_sub, n_stars_eff_sub_vir, n_stars_eff_sub_gal, n_stars_eff_HI_sub, n_stars_eff_HI_sub_vir, n_stars_eff_HI_sub_gal; // Effective number of stars: 1/<w>
vector<double> Ndot_gas_sub, Ndot_gas_sub_vir, Ndot_gas_sub_gal, Ndot_gas_HI_sub, Ndot_gas_HI_sub_vir, Ndot_gas_HI_sub_gal; // Subhalo gas photon rate [photons/s]
vector<double> Ndot2_gas_sub, Ndot2_gas_sub_vir, Ndot2_gas_sub_gal, Ndot2_gas_HI_sub, Ndot2_gas_HI_sub_vir, Ndot2_gas_HI_sub_gal; // Subhalo gas rates^2 [photons^2/s^2]
vector<double> n_cells_eff_sub, n_cells_eff_sub_vir, n_cells_eff_sub_gal, n_cells_eff_HI_sub, n_cells_eff_HI_sub_vir, n_cells_eff_HI_sub_gal; // Effective number of cells: 1/<w>
vector<double> f_src_sub, f_src_sub_vir, f_src_sub_gal, f_src_HI_sub, f_src_HI_sub_vir, f_src_HI_sub_gal; // Subhalo source fraction: sum(w0)
vector<double> f2_src_sub, f2_src_sub_vir, f2_src_sub_gal, f2_src_HI_sub, f2_src_HI_sub_vir, f2_src_HI_sub_gal; // Subhalo squared f_src: sum(w0^2)
vector<double> n_photons_src_sub, n_photons_src_sub_vir, n_photons_src_sub_gal, n_photons_src_HI_sub, n_photons_src_HI_sub_vir, n_photons_src_HI_sub_gal; // Effective number of emitted photons: 1/<w0>
vector<double> f_esc_sub, f_esc_sub_vir, f_esc_sub_gal, f_esc_HI_sub, f_esc_HI_sub_vir, f_esc_HI_sub_gal; // Subhalo escape fraction: sum(w)
vector<double> f2_esc_sub, f2_esc_sub_vir, f2_esc_sub_gal, f2_esc_HI_sub, f2_esc_HI_sub_vir, f2_esc_HI_sub_gal; // Subhalo squared f_esc: sum(w^2)
vector<double> n_photons_esc_sub, n_photons_esc_sub_vir, n_photons_esc_sub_gal, n_photons_esc_HI_sub, n_photons_esc_HI_sub_vir, n_photons_esc_HI_sub_gal; // Effective number of escaped photons: 1/<w>
vector<double> f_abs_sub, f_abs_sub_vir, f_abs_sub_gal, f_abs_HI_sub, f_abs_HI_sub_vir, f_abs_HI_sub_gal; // Subhalo dust absorption fraction
vector<double> f2_abs_sub, f2_abs_sub_vir, f2_abs_sub_gal, f2_abs_HI_sub, f2_abs_HI_sub_vir, f2_abs_HI_sub_gal; // Subhalo squared f_abs: sum(w^2)
vector<double> n_photons_abs_sub, n_photons_abs_sub_vir, n_photons_abs_sub_gal, n_photons_abs_HI_sub, n_photons_abs_HI_sub_vir, n_photons_abs_HI_sub_gal; // Effective number of absorbed photons: 1/<w>
bool focus_subhalos_on_emission = false;     // Center subhalos on emission (position and velocity)
vector<Vec3> r_light_sub, v_light_sub;       // Subhalo center of luminosity position [cm] and velocity [cm/s]
Image bin_Ndot_sub, bin_Ndot_sub_vir, bin_Ndot_sub_gal; // Subhalo bin photon rate [photons/s]
Image bin_Ndot_stars_sub, bin_Ndot_stars_sub_vir, bin_Ndot_stars_sub_gal; // Subhalo stellar bin photon rate [photons/s]
Image bin_Ndot2_stars_sub, bin_Ndot2_stars_sub_vir, bin_Ndot2_stars_sub_gal; // Subhalo stellar bin rates^2 [photons^2/s^2]
Image bin_n_stars_eff_sub, bin_n_stars_eff_sub_vir, bin_n_stars_eff_sub_gal; // Bin effective number of stars: 1/<w>
Image bin_Ndot_gas_sub, bin_Ndot_gas_sub_vir, bin_Ndot_gas_sub_gal; // Subhalo gas bin photon rate [photons/s]
Image bin_Ndot2_gas_sub, bin_Ndot2_gas_sub_vir, bin_Ndot2_gas_sub_gal; // Subhalo gas bin rates^2 [photons^2/s^2]
Image bin_n_cells_eff_sub, bin_n_cells_eff_sub_vir, bin_n_cells_eff_sub_gal; // Bin effective number of cells: 1/<w>
Image bin_f_src_sub, bin_f_src_sub_vir, bin_f_src_sub_gal; // Subhalo bin source fraction: sum(w0)
Image bin_f2_src_sub, bin_f2_src_sub_vir, bin_f2_src_sub_gal; // Subhalo bin squared f_src: sum(w0^2)
Image bin_n_photons_src_sub, bin_n_photons_src_sub_vir, bin_n_photons_src_sub_gal; // Effective number of emitted photons: 1/<w0>
Image bin_f_esc_sub, bin_f_esc_sub_vir, bin_f_esc_sub_gal; // Subhalo bin escape fraction: sum(w)
Image bin_f2_esc_sub, bin_f2_esc_sub_vir, bin_f2_esc_sub_gal; // Subhalo bin squared f_esc: sum(w^2)
Image bin_n_photons_esc_sub, bin_n_photons_esc_sub_vir, bin_n_photons_esc_sub_gal; // Effective number of escaped photons: 1/<w>
Image bin_f_abs_sub, bin_f_abs_sub_vir, bin_f_abs_sub_gal; // Subhalo bin dust absorption fraction
Image bin_f2_abs_sub, bin_f2_abs_sub_vir, bin_f2_abs_sub_gal; // Subhalo bin squared f_abs: sum(w^2)
Image bin_n_photons_abs_sub, bin_n_photons_abs_sub_vir, bin_n_photons_abs_sub_gal; // Effective number of absorbed photons: 1/<w>
vector<double> Ndot_usub, Ndot_HI_usub;      // Unfiltered subhalo photon rate [photons/s]
vector<double> Ndot_stars_usub, Ndot_stars_HI_usub; // Unfiltered subhalo stellar photon rate [photons/s]
vector<double> Ndot2_stars_usub, Ndot2_stars_HI_usub; // Unfiltered subhalo stellar rates^2 [photons^2/s^2]
vector<double> n_stars_eff_usub, n_stars_eff_HI_usub; // Effective number of stars: 1/<w>
vector<double> Ndot_gas_usub, Ndot_gas_HI_usub; // Unfiltered subhalo gas photon rate [photons/s]
vector<double> Ndot2_gas_usub, Ndot2_gas_HI_usub; // Unfiltered subhalo gas rates^2 [photons^2/s^2]
vector<double> n_cells_eff_usub, n_cells_eff_HI_usub; // Effective number of cells: 1/<w>
vector<double> f_src_usub, f_src_HI_usub;    // Unfiltered subhalo source fraction: sum(w0)
vector<double> f2_src_usub, f2_src_HI_usub;  // Unfiltered subhalo squared f_src: sum(w0^2)
vector<double> n_photons_src_usub, n_photons_src_HI_usub; // Effective number of emitted photons: 1/<w0>
Image bin_Ndot_usub;                         // Unfiltered subhalo bin photon rate [photons/s]
Image bin_Ndot_stars_usub;                   // Unfiltered subhalo stellar bin photon rate [photons/s]
Image bin_Ndot2_stars_usub;                  // Unfiltered subhalo stellar bin rates^2 [photons^2/s^2]
Image bin_n_stars_eff_usub;                  // Bin effective number of stars: 1/<w>
Image bin_Ndot_gas_usub;                     // Unfiltered subhalo gas bin photon rate [photons/s]
Image bin_Ndot2_gas_usub;                    // Unfiltered subhalo gas bin rates^2 [photons^2/s^2]
Image bin_n_cells_eff_usub;                  // Bin effective number of cells: 1/<w>
Image bin_f_src_usub;                        // Unfiltered subhalo bin source fraction: sum(w0)
Image bin_f2_src_usub;                       // Unfiltered subhalo bin squared f_src: sum(w0^2)
Image bin_n_photons_src_usub;                // Effective number of emitted photons: 1/<w0>

// Information about group line photons (observed, virial, galaxy)
vector<double> L_grp, L_grp_vir, L_grp_gal;  // Group photon rate [photons/s]
vector<double> L_stars_grp, L_stars_grp_vir, L_stars_grp_gal; // Group stellar photon rate [photons/s]
vector<double> L2_stars_grp, L2_stars_grp_vir, L2_stars_grp_gal; // Group stellar rates^2 [photons^2/s^2]
vector<double> L_gas_grp, L_gas_grp_vir, L_gas_grp_gal; // Group gas photon rate [photons/s]
vector<double> L2_gas_grp, L2_gas_grp_vir, L2_gas_grp_gal; // Group gas rates^2 [photons^2/s^2]
vector<double> L_ugrp;                       // Unfiltered group photon rate [photons/s]
vector<double> L_stars_ugrp;                 // Unfiltered group stellar photon rate [photons/s]
vector<double> L2_stars_ugrp;                // Unfiltered group stellar rates^2 [photons^2/s^2]
vector<double> L_gas_ugrp;                   // Unfiltered group gas photon rate [photons/s]
vector<double> L2_gas_ugrp;                  // Unfiltered group gas rates^2 [photons^2/s^2]

// Information about subhalo line photons (observed, virial, galaxy)
vector<double> L_sub, L_sub_vir, L_sub_gal;  // Subhalo photon rate [photons/s]
vector<double> L_stars_sub, L_stars_sub_vir, L_stars_sub_gal; // Subhalo stellar photon rate [photons/s]
vector<double> L2_stars_sub, L2_stars_sub_vir, L2_stars_sub_gal; // Subhalo stellar rates^2 [photons^2/s^2]
vector<double> L_gas_sub, L_gas_sub_vir, L_gas_sub_gal; // Subhalo gas photon rate [photons/s]
vector<double> L2_gas_sub, L2_gas_sub_vir, L2_gas_sub_gal; // Subhalo gas rates^2 [photons^2/s^2]
vector<double> L_usub;                       // Unfiltered subhalo photon rate [photons/s]
vector<double> L_stars_usub;                 // Unfiltered subhalo stellar photon rate [photons/s]
vector<double> L2_stars_usub;                // Unfiltered subhalo stellar rates^2 [photons^2/s^2]
vector<double> L_gas_usub;                   // Unfiltered subhalo gas photon rate [photons/s]
vector<double> L2_gas_usub;                  // Unfiltered subhalo gas rates^2 [photons^2/s^2]

// Information about the images
Vec2 n_pixels;                               // Number of (x,y) image pixels
int nx_pixels = 100;                         // Number of x image pixels
int ny_pixels = 100;                         // Number of y image pixels
vector<double> image_xedges;                 // Image relative x edge positions [cm]
vector<double> image_yedges;                 // Image relative y edge positions [cm]
vector<double> image_zedges;                 // Image relative z edge positions [cm]
Vec2 image_widths = 0.;                      // Image widths (x,y) [cm] (defaults to entire domain)
Vec2 image_widths_cMpc = 0.;                 // Image widths (x,y) [cMpc]
Vec2 image_radii = -1.;                      // Image radii (x,y) [cm] (image_widths / 2)
Vec2 image_radii_bbox = 0.;                  // Image radii (x,y) [min distance to bbox edge]
Vec2 image_radii_Rvir = 0.;                  // Image radii (x,y) [selected halo virial radius]
Vec2 pixel_widths = 0.;                      // Image pixel widths (x,y) [cm]
double pixel_area;                           // Image pixel area [cm^2]
Vec2 inverse_pixel_widths;                   // Inverse image pixel widths (x,y) [1/cm]
Vec2 pixel_offsets;                          // Image radii / pixel widths
Vec2 pixel_arcsecs = 0.;                     // Image pixel widths [arcsec] (optional)
double pixel_arcsec2;                        // Image pixel area [arcsec^2] (optional)
double aperture_radius = -1.;                // Aperture radius [cm]
double aperture_radius2 = 0.;                // Aperture radius^2 [cm^2]
double aperture_radius_bbox = 0.;            // Aperture radius [min distance to bbox edge]
double aperture_radius_Rvir = 0.;            // Aperture radius [selected halo virial radius]
int n_bins = 100;                            // Number of frequency bins
int n_slices = 1;                            // Number of LOS projection slices
double freq_min = -1000.;                    // Generic frequency extrema [freq units]
double freq_max = 1000.;                     // Calculated when freq is initialized
double freq_bin_width = -1.;                 // Frequency bin width [freq units]
double observed_bin_width;                   // Observed wavelength resolution [angstrom]
double inverse_freq_bin_width;               // Frequency bin width^-1 [freq units]
double rgb_freq_min = -1000.;                // RGB frequency lower limit [freq units]
double rgb_freq_max = 1000.;                 // RGB frequency upper limit [freq units]
double inverse_rgb_freq_bin_width;           // RGB frequency bin width^-1 [freq units]
double Dv_cont_min = -2000.;                 // Continuum velocity offset lower limit [km/s]
double Dv_cont_max = 2000.;                  // Continuum velocity offset upper limit [km/s]
double Dv_cont_res;                          // Continuum velocity offset resolution [km/s]
double l_cont_res;                           // Continuum wavelength resolution [angstrom]
double R_cont_res;                           // Spectral resolution: R = lambda / Delta_lambda
double proj_depth = 0.;                      // Projection depth [cm] (defaults to image_width)
double proj_depth_cMpc = 0.;                 // Projection depth [cMpc]
double proj_radius = -1.;                    // Projection radius [cm] (proj_depth / 2)
double proj_radius_bbox = 0.;                // Projection radius [min distance to bbox edge]
double proj_radius_Rvir = 0.;                // Projection radius [selected halo virial radius]
bool adaptive = false;                       // Adaptive convergence projections
double pixel_rtol = 0.01;                    // Relative tolerance per pixel
bool perspective = false;                    // Perspective camera projections
bool monoscopic = false;                     // 360-degree VR camera projections
double stepback_factor = 0.;                 // Perspective stepback factor (relative to radius)
double shape_factor = 1.;                    // Extraction shape factor (relative to radius)
bool override_volume = false;                // Override projection volume normalization
bool proj_sphere = false;                    // Project through a spherical volume
bool proj_cylinder = false;                  // Project through a cylindrical volume
bool output_flux_avg = false;                // Output the angle-averaged flux
vector<double> flux_avg;                     // Angle-averaged flux [erg/s/cm^2/angstrom]
bool output_radial_avg = false;              // Output the angle-averaged radial surface brightness
vector<double> radial_avg;                   // Angle-averaged radial profile [erg/s/cm^2/arcsec^2]
bool output_radial_cube_avg = false;         // Output the angle-averaged radial spectral data cube
Image radial_cube_avg;                       // Angle-averaged radial spectral profile [erg/s/cm^2/arcsec^2/angstrom]
bool output_escape_fractions = true;         // Output camera escape fractions
vector<double> f_escs;                       // Escape fractions [fraction]
bool output_freq_avgs = false;               // Output frequency averages
vector<double> freq_avgs;                    // Frequency averages [freq units]
bool output_freq_stds = false;               // Output frequency standard deviations
vector<double> freq_stds;                    // Standard deviations [freq units]
bool output_freq_skews = false;              // Output frequency skewnesses
vector<double> freq_skews;                   // Frequency skewnesses [normalized]
bool output_freq_kurts = false;              // Output frequency kurtoses
vector<double> freq_kurts;                   // Frequency kurtoses [normalized]
bool output_fluxes = true;                   // Output spectral fluxes
vectors fluxes;                              // Spectral fluxes [erg/s/cm^2/angstrom]
bool output_images = true;                   // Output surface brightness images
Images images;                               // Surface brightness images [erg/s/cm^2/arcsec^2]
bool output_images2 = false;                 // Output statistical moment images
Images images2;                              // Statistical moment images [erg^2/s^2/cm^4/arcsec^4]
bool output_freq_images = false;             // Output average frequency images
Images freq_images;                          // Average frequency images [freq units]
bool output_freq2_images = false;            // Output frequency^2 images
Images freq2_images;                         // Frequency^2 images [freq^2 units]
bool output_freq3_images = false;            // Output frequency^3 images
Images freq3_images;                         // Frequency^3 images [freq^3 units]
bool output_freq4_images = false;            // Output frequency^4 images
Images freq4_images;                         // Frequency^4 images [freq^4 units]
bool output_rgb_images = false;              // Output flux-weighted frequency RGB images
Image3s rgb_images;                          // Flux-weighted frequency RGB images

// Information about the spectral slits
int n_slit_pixels = 100;                     // Number of slit pixels
// vector<double> slit_edges;                   // Slit relative edge positions [cm]
double slit_width = 0.;                      // Slit width [cm]
double slit_width_cMpc = 0.;                 // Slit width [cMpc]
double slit_radius = -1.;                    // Slit radius [cm] (slit_width / 2)
double slit_radius_bbox = 0.;                // Slit radius [min distance to bbox edge]
double slit_radius_Rvir = 0.;                // Slit radius [selected halo virial radius]
double slit_pixel_width = 0.;                // Slit pixel width [cm]
double slit_pixel_area;                      // Slit pixel area [cm^2]
double inverse_slit_pixel_width;             // Inverse slit pixel width [1/cm]
double slit_pixel_offset;                    // Slit radius / pixel width
double slit_pixel_arcsec = 0.;               // Slit pixel width [arcsec] (optional)
double slit_pixel_arcsec2;                   // Slit pixel area [arcsec^2] (optional)
double slit_aperture = 0.;                   // Slit aperture [cm]
double slit_aperture_arcsec = 0.;            // Slit aperture [arcsec]
double slit_half_aperture;                   // Slit half aperture [cm]
int n_slit_bins;                             // Number of slit frequency bins
double slit_freq_min;                        // Slit frequency extrema [freq units]
double slit_freq_max;                        // Calculated when freq is initialized
double slit_freq_bin_width = -1.;            // Slit frequency bin width [freq units]
double observed_slit_bin_width;              // Observed slit wavelength resolution [angstrom]
double inverse_slit_freq_bin_width;          // Slit frequency bin width^-1 [freq units]
bool output_slits = false;                   // Output spectral slits
Images slits_x, slits_y;                     // Spectral slits [erg/s/cm^2/arcsec^2/angstrom]

// Information about the spectral data cubes
Vec2 n_cube_pixels;                          // Number of (x,y) cube pixels
int nx_cube_pixels = 20;                     // Number of x cube pixels
int ny_cube_pixels = 20;                     // Number of y cube pixels
vector<double> cube_xedges;                  // Cube relative x edge positions [cm]
vector<double> cube_yedges;                  // Cube relative y edge positions [cm]
Vec2 cube_widths = 0.;                       // Cube widths (x,y) [cm]
Vec2 cube_widths_cMpc = 0.;                  // Cube widths (x,y) [cMpc]
Vec2 cube_radii = -1.;                       // Cube radii (x,y) [cm] (cube_widths / 2)
Vec2 cube_radii_bbox = 0.;                   // Cube radii (x,y) [min distance to bbox edge]
Vec2 cube_radii_Rvir = 0.;                   // Cube radii (x,y) [selected halo virial radius]
Vec2 cube_pixel_widths = 0.;                 // Cube pixel widths (x,y) [cm]
double cube_pixel_area;                      // Cube pixel area [cm^2]
Vec2 inverse_cube_pixel_widths;              // Inverse cube pixel widths (x,y) [1/cm]
Vec2 cube_pixel_offsets;                     // Cube radii / pixel widths
Vec2 cube_pixel_arcsecs = 0.;                // Cube pixel widths [arcsec] (optional)
double cube_pixel_arcsec2;                   // Cube pixel area [arcsec^2] (optional)
int n_cube_bins;                             // Number of cube frequency bins
double cube_freq_min;                        // Cube frequency extrema [freq units]
double cube_freq_max;                        // Calculated when freq is initialized
double cube_freq_bin_width = -1.;            // Cube frequency bin width [freq units]
double observed_cube_bin_width;              // Observed cube wavelength resolution [angstrom]
double inverse_cube_freq_bin_width;          // Cube frequency bin width^-1 [freq units]
bool output_cubes = false;                   // Output spectral data cubes
Cubes cubes;                                 // Spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about the radial cameras
bool have_radial_cameras = false;            // Radial-camera-based output
int n_radial_pixels = 100;                   // Number of radial image pixels
double radial_image_radius = 0.;             // Radial image radius [cm] (defaults to half domain)
double radial_image_radius_bbox = 0.;        // Radial image radius [min distance to bbox edge]
double radial_image_radius_Rvir = 0.;        // Radial image radius [selected halo virial radius]
double radial_pixel_width = 0.;              // Radial pixel width [cm]
vector<double> radial_pixel_areas;           // Radial pixel areas [cm^2]
double inverse_radial_pixel_width;           // Inverse radial pixel width [1/cm]
double radial_pixel_arcsec = 0.;             // Radial pixel width [arcsec] (optional)
vector<double> radial_pixel_arcsec2;         // Radial pixel areas [arcsec^2] (optional)
bool output_radial_images = false;           // Output radial surface brightness images
vectors radial_images;                       // Radial surface brightness images [erg/s/cm^2/arcsec^2]
bool output_radial_images2 = false;          // Output statistical moment radial images
vectors radial_images2;                      // Statistical moment radial images [erg^2/s^2/cm^4/arcsec^4]
bool output_freq_radial_images = false;      // Output average frequency radial images
vectors freq_radial_images;                  // Average frequency radial images [freq units]
bool output_freq2_radial_images = false;     // Output frequency^2 radial images
vectors freq2_radial_images;                 // Frequency^2 radial images [freq^2 units]
bool output_freq3_radial_images = false;     // Output frequency^3 radial images
vectors freq3_radial_images;                 // Frequency^3 radial images [freq^3 units]
bool output_freq4_radial_images = false;     // Output frequency^4 radial images
vectors freq4_radial_images;                 // Frequency^4 radial images [freq^4 units]
bool output_rgb_radial_images = false;       // Output flux-weighted frequency RGB radial images
vector<vector<Vec3>> rgb_radial_images;      // Flux-weighted frequency RGB radial images

// Information about the radial spectral data cubes
int n_radial_cube_pixels = 100;              // Number of radial cube pixels
double radial_cube_radius = 0.;              // Radial cube radius [cm] (defaults to half domain)
double radial_cube_radius_bbox = 0.;         // Radial cube radius [min distance to bbox edge]
double radial_cube_radius_Rvir = 0.;         // Radial cube radius [selected halo virial radius]
double radial_cube_pixel_width = 0.;         // Radial cube pixel width [cm]
vector<double> radial_cube_pixel_areas;      // Radial cube pixel areas [cm^2]
double inverse_radial_cube_pixel_width;      // Inverse radial cube pixel width [1/cm]
double radial_cube_pixel_arcsec = 0.;        // Radial cube pixel width [arcsec] (optional)
vector<double> radial_cube_pixel_arcsec2;    // Radial cube pixel areas [arcsec^2] (optional)
int n_radial_cube_bins;                      // Number of radial cube frequency bins
double radial_cube_freq_min;                 // Radial cube frequency extrema [freq units]
double radial_cube_freq_max;                 // Calculated when freq is initialized
double radial_cube_freq_bin_width = -1.;     // Radial cube frequency bin width [freq units]
double observed_radial_cube_bin_width;       // Observed radial cube wavelength resolution [angstrom]
double inverse_radial_cube_freq_bin_width;   // Radial cube frequency bin width^-1 [freq units]
bool output_radial_cubes = false;            // Output radial spectral data cubes
Images radial_cubes;                         // Radial spectral data cubes [erg/s/cm^2/arcsec^2/angstrom]

// Information about the line-of-sight healpix maps
int n_side_map = 4;                          // Healpix parameter for maps
bool output_map = false;                     // Output line-of-sight healpix map
vector<double> map;                          // Escape fractions [fraction]
bool output_map2 = false;                    // Output healpix map^2
vector<double> map2;                         // Escape fractions^2 [statistic]
bool output_freq_map = false;                // Output average frequency map
vector<double> freq_map;                     // Average frequency map [freq units]
bool output_freq2_map = false;               // Output frequency^2 map
vector<double> freq2_map;                    // Frequency^2 map [freq^2 units]
bool output_freq3_map = false;               // Output frequency^3 map
vector<double> freq3_map;                    // Frequency^3 map [freq^3 units]
bool output_freq4_map = false;               // Output frequency^4 map
vector<double> freq4_map;                    // Frequency^4 map [freq^4 units]
bool output_rgb_map = false;                 // Output flux-weighted frequency RGB map
vector<Vec3> rgb_map;                        // Flux-weighted frequency RGB map

// Information about the line-of-sight healpix flux map
int n_side_flux = 2;                         // Healpix parameter for the flux map
int n_map_bins;                              // Number of flux map frequency bins
double map_freq_min;                         // Flux map frequency extrema [freq units]
double map_freq_max;                         // Calculated when freq is initialized
double map_freq_bin_width = -1.;             // Flux map frequency bin width [freq units]
double observed_map_bin_width;               // Observed flux map wavelength resolution [angstrom]
double inverse_map_freq_bin_width;           // Flux map frequency bin width^-1 [freq units]
bool output_flux_map = false;                // Output line-of-sight healpix flux map
Image flux_map;                              // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight healpix radial map
int n_side_radial = 2;                       // Healpix parameter for the radial map
int n_map_pixels = 100;                      // Number of radial map radial bins
double map_radius = 0.;                      // Radial map radius [cm] (defaults to half domain)
double map_radius_bbox = 0.;                 // Radial map radius [min distance to bbox edge]
double map_radius_Rvir = 0.;                 // Radial map radius [selected halo virial radius]
double map_pixel_width = 0.;                 // Radial map pixel width [cm]
vector<double> map_pixel_areas;              // Radial map pixel areas [cm^2]
double inverse_map_pixel_width;              // Inverse radial map pixel width [1/cm]
double map_pixel_arcsec = 0.;                // Radial map pixel width [arcsec] (optional)
vector<double> map_pixel_arcsec2;            // Radial map pixel areas [arcsec^2] (optional)
bool output_radial_map = false;              // Output line-of-sight healpix radial profiles
Image radial_map;                            // Line-of-sight healpix radial profiles [erg/s/cm^2/arcsec^2]

// Information about the line-of-sight healpix radial spectral map
int n_side_cube = 2;                         // Healpix parameter for the radial spectral map
int n_cube_map_pixels = 100;                 // Number of radial spectral map radial bins
double cube_map_radius = 0.;                 // Radial spectral map radius [cm] (defaults to half domain)
double cube_map_radius_bbox = 0.;            // Radial spectral map radius [min distance to bbox edge]
double cube_map_radius_Rvir = 0.;            // Radial spectral map radius [selected halo virial radius]
double cube_map_pixel_width = 0.;            // Radial spectral map pixel width [cm]
vector<double> cube_map_pixel_areas;         // Radial spectral map pixel areas [cm^2]
double inverse_cube_map_pixel_width;         // Inverse radial spectral map pixel width [1/cm]
double cube_map_pixel_arcsec = 0.;           // Radial spectral map pixel width [arcsec] (optional)
vector<double> cube_map_pixel_arcsec2;       // Radial spectral map pixel areas [arcsec^2] (optional)
int n_cube_map_bins;                         // Number of radial spectral map frequency bins
double cube_map_freq_min;                    // Radial spectral map frequency extrema [freq units]
double cube_map_freq_max;                    // Calculated when freq is initialized
double cube_map_freq_bin_width = -1.;        // Radial spectral map frequency bin width [freq units]
double observed_cube_map_bin_width;          // Observed radial spectral map wavelength resolution [angstrom]
double inverse_cube_map_freq_bin_width;      // Radial spectral map frequency bin width^-1 [freq units]
bool output_cube_map = false;                // Output line-of-sight healpix radial spectral map
Cube cube_map;                               // Line-of-sight healpix radial spectral map [erg/s/cm^2/arcsec^2/angstrom]

// Information about the line-of-sight angular cosine (μ=cosθ) map
Vec3 mu_north = {0., 0., 1.};                // Angular north orientation
int n_mu = 100;                              // Number of mu bins
double inverse_dmu;                          // Cosine bin width^-1
bool output_mu = false;                      // Output line-of-sight mu map
vector<double> mu1;                          // Escape fractions [fraction]
bool output_mu2 = false;                     // Output mu map^2
vector<double> mu2;                          // Escape fractions^2 [statistic]
bool output_freq_mu = false;                 // Output average frequency map
vector<double> freq_mu;                      // Average frequency map [freq units]
bool output_freq2_mu = false;                // Output frequency^2 map
vector<double> freq2_mu;                     // Frequency^2 map [freq^2 units]
bool output_freq3_mu = false;                // Output frequency^3 map
vector<double> freq3_mu;                     // Frequency^3 map [freq^3 units]
bool output_freq4_mu = false;                // Output frequency^4 map
vector<double> freq4_mu;                     // Frequency^4 map [freq^4 units]

// Information about the line-of-sight flux angular cosine (μ=cosθ) map
int n_mu_flux = 100;                         // Number of flux map mu bins
int n_mu_bins;                               // Number of flux map frequency bins
double inverse_dmu_flux;                     // Flux cosine bin width^-1
double mu_freq_min;                          // Flux map frequency extrema [freq units]
double mu_freq_max;                          // Calculated when freq is initialized
double mu_freq_bin_width = -1.;              // Flux map frequency bin width [freq units]
double observed_mu_bin_width;                // Observed flux map wavelength resolution [angstrom]
double inverse_mu_freq_bin_width;            // Flux map frequency bin width^-1 [freq units]
bool output_flux_mu = false;                 // Output line-of-sight flux mu map
Image flux_mu;                               // Spectral fluxes [erg/s/cm^2/angstrom]

// Informatino about the line-of-sight group healpix maps
bool output_grp_obs = false;                 // Output group observed data
bool output_grp_vir = false;                 // Output group virial radius data
bool output_grp_gal = false;                 // Output group galaxy data
int n_side_grp = 2;                          // Healpix parameter for group maps
bool output_map_grp = false;                 // Output line-of-sight group healpix map
Image map_grp;                               // Escape fractions [fraction]
bool output_map2_grp = false;                // Output healpix group maps^2
Image map2_grp;                              // Escape fractions^2 [statistic]
bool output_freq_map_grp = false;            // Output average frequency group maps
Image freq_map_grp;                          // Average frequency group maps [freq units]
bool output_freq2_map_grp = false;           // Output frequency^2 group maps
Image freq2_map_grp;                         // Frequency^2 group maps [freq^2 units]
bool output_freq3_map_grp = false;           // Output frequency^3 group maps
Image freq3_map_grp;                         // Frequency^3 group maps [freq^3 units]
bool output_freq4_map_grp = false;           // Output frequency^4 group maps
Image freq4_map_grp;                         // Frequency^4 group maps [freq^4 units]
Image map_grp_vir;                           // Group map at the halo virial radius
Image map_grp_gal;                           // Group map at galaxy radius
Image map2_grp_vir;                          // Group map^2 at the halo virial radius
Image map2_grp_gal;                          // Group map^2 at galaxy radius

// Information about the line-of-sight subhalo healpix maps
bool output_sub_obs = false;                 // Output subhalo observed data
bool output_sub_vir = false;                 // Output subhalo virial radius data
bool output_sub_gal = false;                 // Output subhalo galaxy data
int n_side_sub = 2;                          // Healpix parameter for subhalo maps
bool output_map_sub = false;                 // Output line-of-sight subhalo healpix map
Image map_sub;                               // Escape fractions [fraction]
bool output_map2_sub = false;                // Output healpix subhalo maps^2
Image map2_sub;                              // Escape fractions^2 [statistic]
bool output_freq_map_sub = false;            // Output average frequency subhalo maps
Image freq_map_sub;                          // Average frequency subhalo maps [freq units]
bool output_freq2_map_sub = false;           // Output frequency^2 subhalo maps
Image freq2_map_sub;                         // Frequency^2 subhalo maps [freq^2 units]
bool output_freq3_map_sub = false;           // Output frequency^3 subhalo maps
Image freq3_map_sub;                         // Frequency^3 subhalo maps [freq^3 units]
bool output_freq4_map_sub = false;           // Output frequency^4 subhalo maps
Image freq4_map_sub;                         // Frequency^4 subhalo maps [freq^4 units]
Image map_sub_vir;                           // Subhalo maps at the halo virial radius
Image map_sub_gal;                           // Subhalo maps at galaxy radius
Image map2_sub_vir;                          // Subhalo maps^2 at the halo virial radius
Image map2_sub_gal;                          // Subhalo maps^2 at galaxy radius

// Information about the line-of-sight healpix flux group maps
int n_side_flux_grp = 2;                     // Healpix parameter for the flux map
int n_map_bins_grp = 100;                    // Number of flux map frequency bins
double map_freq_min_grp = 0.;                // Flux map frequency extrema [freq units]
double map_freq_max_grp = 0.;                // Calculated when freq is initialized
double map_freq_bin_width_grp = 0.;          // Flux map frequency bin width [freq units]
double observed_map_bin_width_grp = 0.;      // Observed flux map wavelength resolution [angstrom]
double inverse_map_freq_bin_width_grp = 0.;  // Flux map frequency bin width^-1 [freq units]
bool output_flux_map_grp = false;            // Output line-of-sight healpix flux map
Cube flux_map_grp;                           // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight healpix flux subhalo maps
int n_side_flux_sub = 2;                     // Healpix parameter for the flux map
int n_map_bins_sub = 100;                    // Number of flux map frequency bins
double map_freq_min_sub = 0.;                // Flux map frequency extrema [freq units]
double map_freq_max_sub = 0.;                // Calculated when freq is initialized
double map_freq_bin_width_sub = 0.;          // Flux map frequency bin width [freq units]
double observed_map_bin_width_sub = 0.;      // Observed flux map wavelength resolution [angstrom]
double inverse_map_freq_bin_width_sub = 0.;  // Flux map frequency bin width^-1 [freq units]
bool output_flux_map_sub = false;            // Output line-of-sight healpix flux map
Cube flux_map_sub;                           // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the angle-averaged spectral flux for groups
int n_bins_grp = 100;                        // Number of flux avg frequency bins
double freq_min_grp = 0.;                    // Flux avg frequency extrema [freq units]
double freq_max_grp = 0.;                    // Calculated when freq is initialized
double freq_bin_width_grp = 0.;              // Flux avg frequency bin width [freq units]
double observed_bin_width_grp = 0.;          // Observed flux avg wavelength resolution [angstrom]
double inverse_freq_bin_width_grp = 0.;      // Flux avg frequency bin width^-1 [freq units]
bool output_flux_avg_grp = false;            // Output line-of-sight healpix flux avg
Image flux_avg_grp;                          // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the angle-averaged spectral flux for subhalos
int n_bins_sub = 100;                        // Number of flux avg frequency bins
double freq_min_sub = 0.;                    // Flux avg frequency extrema [freq units]
double freq_max_sub = 0.;                    // Calculated when freq is initialized
double freq_bin_width_sub = 0.;              // Flux avg frequency bin width [freq units]
double observed_bin_width_sub = 0.;          // Observed flux avg wavelength resolution [angstrom]
double inverse_freq_bin_width_sub = 0.;      // Flux avg frequency bin width^-1 [freq units]
bool output_flux_avg_sub = false;            // Output line-of-sight healpix flux avg
Image flux_avg_sub;                          // Spectral fluxes [erg/s/cm^2/angstrom]

// Information about the line-of-sight healpix group radial maps
int n_side_radial_grp = 2;                   // Healpix parameter for the radial map
int n_map_pixels_grp = 100;                  // Number of radial map radial bins
double map_radius_grp = 0.;                  // Radial map radius [cm] (defaults to half domain)
double map_radius_bbox_grp = 0.;             // Radial map radius [min distance to bbox edge]
double map_radius_Rvir_grp = 0.;             // Radial map radius [selected halo virial radius]
double map_pixel_width_grp = 0.;             // Radial map pixel width [cm]
vector<double> map_pixel_areas_grp;          // Radial map pixel areas [cm^2]
double inverse_map_pixel_width_grp;          // Inverse radial map pixel width [1/cm]
double min_map_radius_grp = 0.;              // Minimum radial map radius [cm]
double log_min_map_radius_grp = 0.;          // Logarithm of the minimum radial map radius [log10(cm)]
double map_pixel_arcsec_grp = 0.;            // Radial map pixel width [arcsec] (optional)
vector<double> map_pixel_arcsec2_grp;        // Radial map pixel areas [arcsec^2] (optional)
bool output_radial_map_grp = false;          // Output line-of-sight healpix radial profiles
Cube radial_map_grp;                         // Line-of-sight healpix radial profiles [erg/s/cm^2/arcsec^2]

// Information about the line-of-sight healpix subhalo radial maps
int n_side_radial_sub = 0;                   // Healpix parameter for the radial map
int n_map_pixels_sub = 100;                  // Number of radial map radial bins
double map_radius_sub = 0.;                  // Radial map radius [cm] (defaults to half domain)
double map_radius_bbox_sub = 0.;             // Radial map radius [min distance to bbox edge]
double map_radius_Rvir_sub = 0.;             // Radial map radius [selected halo virial radius]
double map_pixel_width_sub = 0.;             // Radial map pixel width [cm]
vector<double> map_pixel_areas_sub;          // Radial map pixel areas [cm^2]
double inverse_map_pixel_width_sub;          // Inverse radial map pixel width [1/cm]
double min_map_radius_sub = 0.;              // Minimum radial map radius [cm]
double log_min_map_radius_sub = 0.;          // Logarithm of the minimum radial map radius [log10(cm)]
double map_pixel_arcsec_sub = 0.;            // Radial map pixel width [arcsec] (optional)
vector<double> map_pixel_arcsec2_sub;        // Radial map pixel areas [arcsec^2] (optional)
bool output_radial_map_sub = false;          // Output line-of-sight healpix radial profiles
Cube radial_map_sub;                         // Line-of-sight healpix radial profiles [erg/s/cm^2/arcsec^2]

// Information about the angle-averaged radial surface brightness (groups)
int n_pixels_grp = 100;                      // Number of radial avg radial bins
double radius_grp = 0.;                      // Radial avg radius [cm] (defaults to half domain)
double radius_bbox_grp = 0.;                 // Radial avg radius [min distance to bbox edge]
double radius_Rvir_grp = 0.;                 // Radial avg radius [selected halo virial radius]
double pixel_width_grp = 0.;                 // Radial avg pixel width [cm]
vector<double> pixel_areas_grp;              // Radial avg pixel areas [cm^2]
double inverse_pixel_width_grp;              // Inverse radial avg pixel width [1/cm]
double min_radius_grp = 0.;                  // Minimum radial avg radius [cm]
double log_min_radius_grp = 0.;              // Logarithm of the minimum radial avg radius [log10(cm)]
double pixel_arcsec_grp = 0.;                // Radial avg pixel width [arcsec] (optional)
vector<double> pixel_arcsec2_grp;            // Radial avg pixel areas [arcsec^2] (optional)
bool output_radial_avg_grp = false;          // Output line-of-sight healpix radial profiles
Image radial_avg_grp;                        // Angle-averaged radial profile [erg/s/cm^2/arcsec^2]

// Information about the angle-averaged radial surface brightness (subhalos)
int n_pixels_sub = 100;                      // Number of radial avg radial bins
double radius_sub = 0.;                      // Radial avg radius [cm] (defaults to half domain)
double radius_bbox_sub = 0.;                 // Radial avg radius [min distance to bbox edge]
double radius_Rvir_sub = 0.;                 // Radial avg radius [selected halo virial radius]
double pixel_width_sub = 0.;                 // Radial avg pixel width [cm]
vector<double> pixel_areas_sub;              // Radial avg pixel areas [cm^2]
double inverse_pixel_width_sub;              // Inverse radial avg pixel width [1/cm]
double min_radius_sub = 0.;                  // Minimum radial avg radius [cm]
double log_min_radius_sub = 0.;              // Logarithm of the minimum radial avg radius [log10(cm)]
double pixel_arcsec_sub = 0.;                // Radial avg pixel width [arcsec] (optional)
vector<double> pixel_arcsec2_sub;            // Radial avg pixel areas [arcsec^2] (optional)
bool output_radial_avg_sub = false;          // Output line-of-sight healpix radial profiles
Image radial_avg_sub;                        // Angle-averaged radial profile [erg/s/cm^2/arcsec^2]

// Tabulated ionizing spectral energy distributions
IonAgeTables ion_age;                        // Tabulated ionizing SEDs
string source_file_Z_age = "";               // Spectral source file (Z,age,wavelength)
IonMetallicityAgeTables ion_Z_age;           // Tabulated ionizing SEDs
vectors log_Qion_Z_age;                      // Tabulated ionizing photon rate [photons/s/Msun]
vectors log_Lcont_Z_age;                     // Tabulated line continuum [erg/s/angstrom/Msun]
CubeMetallicityAgeTables spec_Z_age;         // Tabulated generalized SEDs
FreeFreeTemperatureTables spec_ff_Z1;        // Tabulated free-free SEDs (Z = 1)
FreeFreeTemperatureTables spec_ff_Z2;        // Tabulated free-free SEDs (Z = 2)
FreeBoundTemperatureTables spec_fb_HI;       // Tabulated free-bound SEDs (HII->HI)
FreeBoundTemperatureTables spec_fb_HeI;      // Tabulated free-bound SEDs (HeII->HeI)
FreeBoundTemperatureTables spec_fb_HeII;     // Tabulated free-bound SEDs (HeIII->HeII)
TwoPhotonWvlTables spec_2p_HI;               // Tabulated two-photon SEDs (HII->HI)
// TwoPhotonWvlTables spec_2p_HeI;              // Tabulated two-photon SEDs (HeII->HeI)
TwoPhotonWvlTables spec_2p_HeII;             // Tabulated two-photon SEDs (HeIII->HeII)

double albedo_HI = 0.;                       // Dust scattering albedos
double albedo_HeI = 0.;
double albedo_HeII = 0.;
double cosine_HI = 0.;                       // Dust scattering anisotropy parameters
double cosine_HeI = 0.;
double cosine_HeII = 0.;
double kappa_HI = 1e5;                       // Dust opacities [cm^2/g dust]
double kappa_HeI = 1e5;
double kappa_HeII = 1e5;
