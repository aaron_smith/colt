/***********
 * utils.h *
 ***********

 * General utility function definitions.

*/

#ifndef UTILS_INCLUDED
#define UTILS_INCLUDED

#include "proto.h"

/* Sum the elements of a vector using OpenMP parallelization. */
template <typename T>
T omp_sum(const vector<T>& v) {
  T result = static_cast<T>(0);
  const int size = v.size();
  #pragma omp parallel for reduction(+:result)
  for (int i = 0; i < size; ++i)
    result += v[i];
  return result;
}

/* Sum the elements of an array using OpenMP parallelization. */
template <typename T, size_t N>
T omp_sum(const array<T, N>& v) {
  T result = static_cast<T>(0);
  const int size = v.size();
  #pragma omp parallel for reduction(+:result)
  for (int i = 0; i < size; ++i)
    result += v[i];
  return result;
}

template <typename T>
inline T omp_sum(const NDArray<2,T> image, const int row) {return image.sum(row);}

/* Find the minimum value of a vector using OpenMP parallelization. */
template <typename T>
T omp_min(const vector<T>& v) {
  T result = v[0];
  const int size = v.size();
  #pragma omp parallel for reduction(min:result)
  for (int i = 1; i < size; ++i)
    if (v[i] < result)
      result = v[i];
  return result;
}

/* Find the maximum value of a vector using OpenMP parallelization. */
template <typename T>
T omp_max(const vector<T>& v) {
  T result = v[0];
  const int size = v.size();
  #pragma omp parallel for reduction(max:result)
  for (int i = 1; i < size; ++i)
    if (v[i] > result)
      result = v[i];
  return result;
}

/* Similar behavior as numpy.linspace. */
inline vector<double> linspace(const double xmin, const double xmax, const int num=50, const bool endpoint=true) {
  // Basic error checking
  if (num < 2) {
    if (num == 1)
      return {xmin};
    error("linspace called with num < 1.");
  }

  // Step size based on whether to include the last point or not
  const double dx = (xmax - xmin) / (double)(endpoint ? num - 1 : num);
  vector<double> result;
  result.resize(num);
  #pragma omp parallel for
  for (int i = 0; i < num; ++i)
    result[i] = xmin + dx * double(i);
  return result;
}

/* Clip a vector to a given range. */
inline void clip(vector<double>& vec, const double vmin, const double vmax) {
  const size_t size = vec.size();
  #pragma omp parallel for
  for (size_t i = 0; i < size; ++i) {
    if (vec[i] < vmin)
      vec[i] = vmin;
    if (vec[i] > vmax)
      vec[i] = vmax;
  }
}

/* Clip a vector to a given range. */
inline void clip(vector<double>& vec, const double vmin, const vector<double>& vmax) {
  const size_t size = vec.size();
  if (vmax.size() != size)
    error("clip: vector sizes do not match.");
  #pragma omp parallel for
  for (size_t i = 0; i < size; ++i) {
    if (vec[i] < vmin)
      vec[i] = vmin;
    if (vec[i] > vmax[i])
      vec[i] = vmax[i];
  }
}

// Helper function to handle the base case of rescaling objects
template <class C>
static inline void rescale_base(C& vec, const double scaling) {
  const size_t vec_size = vec.size();
  #pragma omp parallel for
  for (size_t i = 0; i < vec_size; ++i)
    vec[i] *= scaling;
}

// Specialization for NDArray<N,T>
template <int N, typename T>
static inline void rescale(NDArray<N,T>& vec, const double scaling) {
  rescale_base(vec, scaling);
}

// Specialization for vector<double>
static inline void rescale(vector<double>& vec, const double scaling) {
  rescale_base(vec, scaling);
}

// Main function to handle the recursive case of rescaling vectors
template <class C>
static inline void rescale(vector<C>& vecs, const double scaling) {
  const size_t vecs_size = vecs.size();
  for (size_t i = 0; i < vecs_size; ++i)
    rescale(vecs[i], scaling);
}

// Helper function to rescale objects (positive values only)
static inline void rescale_positive(vector<double>& vec, const double scaling) {
  const size_t vec_size = vec.size();
  #pragma omp parallel for
  for (size_t i = 0; i < vec_size; ++i) {
    if (vec[i] > 0.)
      vec[i] *= scaling;
  }
}

// Helper function to handle the base case of dividing vectors
template <class C1, class C2>
static inline void divide_base(C1& vec, const C2& div) {
  const size_t vec_size = vec.size();
  const size_t div_size = div.size();
  if (vec_size != div_size) // Minimal error checking
    error("Attempting to divide two vectors with different sizes.");
  #pragma omp parallel for
  for (size_t j = 0; j < div_size; ++j)
    if (div[j] > 0.)
      vec[j] /= div[j];
}

// Specialization for NDArray<N,T>
template <int N, typename T>
static inline void divide(NDArray<N,T>& vec, const NDArray<N,double>& div) {
  divide_base(vec, div);
}

// Specialization for vector<T>
template <typename T>
static inline void divide(vector<T>& vec, const vector<double>& div) {
  divide_base(vec, div);
}

// Main function to handle the recursive case of dividing vectors
template <class C1, class C2>
static inline void divide(vector<C1>& vecs, const vector<C2>& divs) {
  const size_t vecs_size = vecs.size();
  const size_t divs_size = divs.size();
  if (vecs_size != divs_size) // Minimal error checking
    error("Attempting to divide two vectors with different sizes.");
  for (size_t i = 0; i < divs_size; ++i)
    divide(vecs[i], divs[i]);
}

/*! \brief Converts a ratio to an integer, ensuring a minimum value of 1.
 *
 * \param[in] numenator The numerator of the ratio (e.g. image width).
 * \param[in] denomenator The denominator of the ratio, which may be updated (e.g. pixel width).
 *
 * \return The integer result of the ratio, guaranteed to be at least 1.
 */
static inline int sane_int(const double numenator, double& denomenator) {
  int n_sane = static_cast<int>(numenator / denomenator); // Sane integer conversion
  if (n_sane <= 0) {
    n_sane = 1; // Ensure at least one pixel
    denomenator = numenator; // Update the pixel width
  }
  return n_sane;
}

/* Safe division returns zero if the denomenator is not positive. */
static inline double safe_division(const double numenator, const double denomenator) {
  return (denomenator == 0.) ? 0. : numenator / denomenator;
}

/* Calculate the effective number of discrete contributions via <f>^2 / <f^2>. */
static inline double calc_n_eff(const double f1, const double f2) {
  return (f2 > 0.) ? f1 * f1 / f2 : 0.;
}

/* Update the first argument if the second is smaller. */
static inline void inplace_min(double& a, const double b) {
  if (b < a)
    a = b;
}

/* Dummy functions to return zero. */
template <typename T>
inline T return_zero(T) {
  return static_cast<T>(0);
}
template <typename T>
inline T return_zero(T, T) {
  return static_cast<T>(0);
}

/* Return zero if below a threshold value. */
template <typename T>
inline T chop(const T v, const T v_chop = static_cast<T>(0)) {
  // if (std::isnan(v)) { error("chop: value is NaN!"); }
  return (v < v_chop) ? static_cast<T>(0) : v;
}

/* Print vectors. */
template <typename T>
inline void print(const vector<T>& v) {
  if (v.size() > 10) {
    cout << v[0] << ", " << v[1] << ", " << v[2] << ", ... , "
         << v[v.size()-3] << ", " << v[v.size()-2] << ", " << v[v.size()-1];
  } else if (!v.empty()) {
    for (size_t i = 0; i < v.size()-1; ++i)
      cout << v[i] << ", ";
    cout << v[v.size()-1];
  }
}

/* Print vectors with unit conversion. */
template <typename T>
inline void print(const vector<T>& v, const double conv) {
  if (v.size() > 10) {
    cout << (conv == 1. ? v[0] : v[0] / conv) << ", "
         << (conv == 1. ? v[1] : v[1] / conv) << ", "
         << (conv == 1. ? v[2] : v[2] / conv) << ", ... , "
         << (conv == 1. ? v[v.size()-3] : v[v.size()-3] / conv) << ", "
         << (conv == 1. ? v[v.size()-2] : v[v.size()-2] / conv) << ", "
         << (conv == 1. ? v[v.size()-1] : v[v.size()-1] / conv);
  } else if (!v.empty()) {
    for (size_t i = 0; i < v.size()-1; ++i)
      cout << (conv == 1. ? v[i] : v[i] / conv) << ", ";
    cout << (conv == 1. ? v[v.size()-1] : v[v.size()-1] / conv);
  }
}

/* Print arrays with unit conversion. */
template <typename T, size_t N>
inline void print(const array<T, N>& v, const double conv = 1.) {
  if (N > 10) {
    cout << (conv == 1. ? v[0] : v[0] / conv) << ", "
         << (conv == 1. ? v[1] : v[1] / conv) << ", "
         << (conv == 1. ? v[2] : v[2] / conv) << ", ... , "
         << (conv == 1. ? v[N-3] : v[N-3] / conv) << ", "
         << (conv == 1. ? v[N-2] : v[N-2] / conv) << ", "
         << (conv == 1. ? v[N-1] : v[N-1] / conv);
  } else if (N > 0) {
    for (size_t i = 0; i < N-1; ++i)
      cout << (conv == 1. ? v[i] : v[i] / conv) << ", ";
    cout << (conv == 1. ? v[N-1] : v[N-1] / conv);
  }
}

template <typename T>
inline void print(string name, const vector<T>& v) {
  name.resize(10, ' ');                      // Pad the name to 10 characters
  cout << "  " << name << " = ("; print(v); cout << ")" << endl;
}

template <typename T>
inline void print(string name, const vector<T>& v, string units, const double conv = 1.) {
  name.resize(10, ' ');                      // Pad the name to 10 characters
  cout << "  " << name << " = ("; print(v, conv); cout << ") " << units << endl;
}

template <typename T, size_t N>
inline void print(string name, const array<T, N>& v, string units = "", const double conv = 1.) {
  name.resize(10, ' ');                      // Pad the name to 10 characters
  cout << "  " << name << " = ("; print(v, conv); cout << ") " << units << endl;
}

/* Allow conversion to string format. */
inline string to_string(const string& str) {
  return string(str);
}

template <typename T>
inline string to_string(const vector<T>& v, const double conv = 1.) {
  string result = "(";
  for (size_t i = 0; i < v.size()-1; ++i)
    result += to_string(conv == 1. ? v[i] : v[i] / conv) + ", ";
  if (!v.empty())
    result += to_string(conv == 1. ? v[v.size()-1] : v[v.size()-1] / conv);
  return result + ")";
}

inline string to_string(const strings& v) {
  string result = "(";
  for (size_t i = 0; i < v.size()-1; ++i)
    result += v[i] + ", ";
  if (!v.empty())
    result += v[v.size()-1];
  return result + ")";
}

template <typename T, typename S>
std::ostream& operator<<(std::ostream& os, const std::pair<T, S>& v) {
  os << "[" << v.first << ", " << v.second << "]";
  return os;
}

inline string get_filename(const string& path) {
  size_t pos = path.find_last_of("/\\");
  return (pos == string::npos) ? path : path.substr(pos + 1);
}

//  Computes the following dimensionless integral:
//   ∞  y^3 dy    ∞  y^3 dy     15   ∞           | x^3   3x^2  6 x    6  |            hν
//   ∫ -------- / ∫ --------  = ---  Σ  exp(-nx) | --- + --- + --- + --- |  where x = --
//   x exp(y)-1   0 exp(y)-1    π^4 n=1          |  n    n^2   n^3   n^4 |            kT
//
/* Compute the fraction of the planck function bolometric luminosity above a given spectral coordinate. */
inline double planck_fraction_Lbol(const double x) {
  const double x2 = x * x / 2.;              // x^2 / 2
  const double x3 = x * x2 / 3.;             // x^3 / 6
  double y, term, sum = 0.;
  for (int i = 1; i < 10000; ++i) {
    y = 1. / double(i);
    term = exp(-double(i)*x) * y * (x3 + y * (x2 + y * (x + y)));
    sum += term;
    if (term < 1e-14 * sum)
      break;                                 // Converged
  }
  return 0.9239384029215902 * sum;           // (15/pi^4) * 6 * sum
}

//  Computes the following dimensionless integral:
//   ∞  y^2 dy    ∞  y^2 dy      1   ∞           | x^2   2 x    2  |            hν
//   ∫ -------- / ∫ --------  = ---  Σ  exp(-nx) | --- + --- + --- |  where x = --
//   x exp(y)-1   0 exp(y)-1    2ζ3 n=1          |  n    n^2   n^3 |            kT
//
/* Compute the fraction of the planck function total photon rate above a given spectral coordinate. */
inline double planck_fraction_Ndot(const double x) {
  const double x2 = x * x / 2.;              // x^2 / 2
  double y, term, sum = 0.;
  for (int i = 1; i < 10000; ++i) {
    y = 1. / double(i);
    term = exp(-double(i)*x) * y * (x2 + y * (x + y));
    sum += term;
    if (term < 1e-14 * sum)
      break;                                 // Converged
  }
  return 0.8319073725807075 * sum;           // (1/2ζ3) * 2 * sum
}

#endif // UTILS_INCLUDED
