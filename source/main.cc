/*!
 * \file    main.cc
 *
 * \brief   Driver for the overall flow of the program.
 *
 * \details This file contains the main function with high level parsing of the
 *          command line arguments and setup of the parallelism. The modules
 *          are run as instances derived from a common base Simulation class.
 */

#include "proto.h"
#include <fstream> // File handling
#include <sstream> // String manipulation
#include "mcrt/MCRT.h" // Monte Carlo radiative transfer
#include "ionization/Ionization.h" // MCRT ionization equilibrium
#include "projections/Projections.h" // Ray-based projections
#include "escape/Escape.h" // Ray-based escape RT
#include "escape_cont/EscapeCont.h" // Ray-based continuum escape RT
#include "rays/Rays.h" // Ray extraction analysis
#include "reionization/ReionizationBox.h" // Reionization box analysis

void setup_parallelism(int argc, char** argv); // MPI and OpenMP setup

//! Determine which module to run.
static inline void determine_module() {
  string line, substr;
  std::ifstream file(config_file);           // Read module from header info
  while (std::getline(file, line)) {
    if (line.rfind("---", 0) == 0) {         // YAML start sequence "---"
      auto npos = line.find('!');            // YAML tag specifier '!'
      if (npos != string::npos)              // Use default if not specified
        substr = line.substr(npos + 1);
      if (!substr.empty())                   // Override default module string
        std::istringstream(substr) >> module;
    }
  }
}

//! Helper driver function to emulate a virtual constructor.
template<class C>
inline void Driver() {
  C sim{};                                   // Restrict simulation scope
  sim.setup_config();                        // Configuration
  sim.initialize();                          // Initialization
  sim.run();                                 // Calculations
  sim.finalize();                            // Finalization
}

/*! \brief Main function of the program.
 *
 *  \param[in] argc Number of arguments passed to the program.
 *  \param[in] argv Full list of arguments passed to the program.
 *
 *  \return Exit code.
 */
int main(int argc, char** argv) {
  // Turn off output buffering
  setbuf(stdout, nullptr);
  setbuf(stderr, nullptr);

  // MPI and OpenMP setup
  setup_parallelism(argc, argv);

  // Configuration and general error checking
  executable = argv[0];                      // Executable name (command line)
  if (argc < 2)
    root_error("Please specify a config file: " + executable + " config.yaml [snapshot]");
  config_file = argv[1];                     // File for simulation parameters
  if (argc > 2) {
    snap_str = argv[2];                      // Snapshot number (optional)
    snap_num = std::stoi(snap_str);          // Convert to integer
    if (snap_num < 0)
      root_error("Expected snapshot number to be >= 0, but recieved " + snap_str);
  }
  if (argc > 3) {
    halo_str = argv[3];                      // Group or subhalo number (optional)
    halo_num = std::stoi(halo_str);          // Convert to integer
    if (halo_num < -2)
      root_error("Expected group or subhalo number to be >= -2, but recieved " + halo_str);
  }

  // Run the simulation
  determine_module();                        // Determine which module to run
  if (module == "mcrt")
    Driver<MCRT>();                          // Monte Carlo radiative transfer
  else if (module == "ionization")
    Driver<Ionization>();                    // MCRT ionization equilibrium
  else if (module == "projections")
    Driver<Projections>();                   // Ray-tracing based projections
  else if (module == "escape")
    Driver<Escape>();                        // Ray-tracing based escape RT
  else if (module == "escape_cont")
    Driver<EscapeCont>();                    // Ray-tracing based continuum escape RT
  else if (module == "rays")
    Driver<Rays>();                          // Ray extraction analysis
  else if (module == "reionization")
    Driver<ReionizationBox>();               // Reionization box analysis
  else
    root_error("Unrecognized module: " + module);

  // Finalize parallelism
  MPI_Finalize();                            // Shut down MPI

  return 0;
}
