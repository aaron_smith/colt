/*!
 * \file    Vec3.h
 *
 * \brief   Lightweight 3-vector class
 *
 * \details Convenience class for handling 3-vectors in (x,y,z) order.
 */

#ifndef VEC3_INCLUDED
#define VEC3_INCLUDED

#include <cmath>
#include <ostream>

//! Generic 3-vector class with (\f$x, y, z\f$) order.
template <typename T>
struct GenericVec3 {
  T x = static_cast<T>(0);  //!< x-component
  T y = static_cast<T>(0);  //!< y-component
  T z = static_cast<T>(0);  //!< z-component

  //! Default constructor
  GenericVec3() = default;

  //! Constructor based on (x,y,z) values
  GenericVec3(T x_, T y_, T z_) : x(x_), y(y_), z(z_) {}

  //! Constructor based on a single value
  GenericVec3(T x_) : x(x_), y(x_), z(x_) {}

  //! Constructor based on a pointer to an array of 3 values
  explicit GenericVec3(T* data) {
    x = data[0];
    y = data[1];
    z = data[2];
  }

  //! Return a pointer to the first component (read-write)
  inline T* data() { return &x; }

  //! Return a const pointer to the first component (read-only)
  inline const T* data() const { return &x; }

  //! Minimum of all components
  inline T min() const { return (x < y) ? ((x < z) ? x : z) : ((y < z) ? y : z); }

  //! Maximum of all components
  inline T max() const { return (x > y) ? ((x > z) ? x : z) : ((y > z) ? y : z); }

  //! Component-wise minimum with another vector
  inline GenericVec3 min(const GenericVec3& other) const {
    return {(x < other.x) ? x : other.x, (y < other.y) ? y : other.y, (z < other.z) ? z : other.z};
  }

  //! Component-wise maximum with another vector
  inline GenericVec3 max(const GenericVec3& other) const {
    return {(x > other.x) ? x : other.x, (y > other.y) ? y : other.y, (z > other.z) ? z : other.z};
  }

  //! In-place normalization of the vector
  inline void normalize() {
    // TODO: add compile-time option to check for division by zero
    const T inv_norm = static_cast<T>(1) / sqrt(x * x + y * y + z * z);
    x *= inv_norm;
    y *= inv_norm;
    z *= inv_norm;
  }

  //! Return the unit vector of this vector (out of place)
  inline GenericVec3 unit_vector() const {
    // TODO: add compile-time option to check for division by zero
    const T inv_norm = static_cast<T>(1) / sqrt(x * x + y * y + z * z);
    return {x * inv_norm, y * inv_norm, z * inv_norm};
  }

  //! Calculate the norm of the vector
  inline T norm() const { return sqrt(x * x + y * y + z * z); }

  //! Calculate the squared norm of the vector
  inline T dot() const { return x * x + y * y + z * z; }

  //! Calculate the dot product with another vector
  inline T dot(const GenericVec3& other) const { return x * other.x + y * other.y + z * other.z; }

  //! Calculate the square of the dot product with another vector
  inline T dot2(const GenericVec3& other) const {
    const T val = x * other.x + y * other.y + z * other.z;
    return val * val;
  }

  //! Calculate the distance between two vectors: ||A-B||
  inline T dist(const GenericVec3& other) const {
    const T dx = x - other.x;
    const T dy = y - other.y;
    const T dz = z - other.z;
    return sqrt(dx * dx + dy * dy + dz * dz);
  }

  //! Calculate the cross product with another vector
  inline GenericVec3 cross(const GenericVec3& other) const {
    return {y * other.z - z * other.y, z * other.x - x * other.z, x * other.y - y * other.x};
  }

  //! Calculate the norm of the cross product with another vector: ||A x B||
  inline T cross_norm(const GenericVec3& other) const {
    const T dx = y * other.z - z * other.y;
    const T dy = z * other.x - x * other.z;
    const T dz = x * other.y - y * other.x;
    return sqrt(dx * dx + dy * dy + dz * dz);
  }

  //! Check for equality with a scalar
  inline bool operator==(const T val) const { return (x == val) && (y == val) && (z == val); }

  //! Check for equality with another vector
  inline bool operator==(const GenericVec3& other) const { return (x == other.x) && (y == other.y) && (z == other.z); }

  //! Check for inequality with a scalar
  inline bool operator!=(const T val) const { return (x != val) || (y != val) || (z != val); }

  //! Check for inequality with another vector
  inline bool operator!=(const GenericVec3& other) const { return (x != other.x) || (y != other.y) || (z != other.z); }

  //! Define addition of two vectors
  inline GenericVec3 operator+(const GenericVec3& other) const { return {x + other.x, y + other.y, z + other.z}; }

  //! Define addition with a scalar
  inline GenericVec3 operator+(const T val) const { return {x + val, y + val, z + val}; }

  //! Define in-place addition with another vector
  inline void operator+=(const GenericVec3& other) {
    x += other.x;
    y += other.y;
    z += other.z;
  }

  //! Define in-place addition with a scalar
  inline void operator+=(const T val) {
    x += val;
    y += val;
    z += val;
  }

  //! Define subtraction of two vectors
  inline GenericVec3 operator-(const GenericVec3& other) const { return {x - other.x, y - other.y, z - other.z}; }

  //! Define subtraction of a scalar
  inline GenericVec3 operator-(const T val) const { return {x - val, y - val, z - val}; }

  //! Define in-place subtraction with another vector
  inline void operator-=(const GenericVec3& other) {
    x -= other.x;
    y -= other.y;
    z -= other.z;
  }

  //! Define in-place subtraction with a scalar
  inline void operator-=(const T val) {
    x -= val;
    y -= val;
    z -= val;
  }

  //! Define multiplication of two vectors
  inline GenericVec3 operator*(const GenericVec3& other) const { return {x * other.x, y * other.y, z * other.z}; }

  //! Define multiplication with a scalar
  inline GenericVec3 operator*(const T val) const { return {x * val, y * val, z * val}; }

  //! Define in-place multiplication with another vector
  inline void operator*=(const GenericVec3& other) {
    x *= other.x;
    y *= other.y;
    z *= other.z;
  }

  //! Define in-place multiplication with a scalar
  inline void operator*=(const T val) {
    x *= val;
    y *= val;
    z *= val;
  }

  //! Define in-place division by another vector
  inline GenericVec3 operator/(const GenericVec3& other) const {
    // TODO: add compile-time option to check for division by zero
    return {x / other.x, y / other.y, z / other.z};
  }

  //! Define division by a scalar
  inline GenericVec3 operator/(const T val) const {
    // TODO: add compile-time option to check for division by zero
    return {x / val, y / val, z / val};
  }

  //! Define in-place division by another vector
  inline void operator/=(const GenericVec3& other) {
    // TODO: add compile-time option to check for division by zero
    x /= other.x;
    y /= other.y;
    z /= other.z;
  }

  //! Define in-place division by a scalar
  inline void operator/=(const T val) {
    // TODO: add compile-time option to check for division by zero
    x /= val;
    y /= val;
    z /= val;
  }

  //! Define indexing of the vector (read-only)
  inline T operator[](size_t index) const { return ((T*)this)[index]; }
  // TODO add compile-time option to check for out-of-bounds access (<0 or >2)

  //! Define indexing of the vector (read-write)
  inline T& operator[](size_t index) { return ((T*)this)[index]; }
  // TODO add compile-time option to check for out-of-bounds access (<0 or >2)
};

//! Define addition of a scalar with a vector (left-hand side)
template <typename T>
inline GenericVec3<T> operator+(const T val, const GenericVec3<T>& vec) {
  return {val + vec.x, val + vec.y, val + vec.z};
}

//! Define subtraction of a scalar from a vector (left-hand side)
template <typename T>
inline GenericVec3<T> operator-(const T val, const GenericVec3<T>& vec) {
  return {val - vec.x, val - vec.y, val - vec.z};
}

//! Define multiplication of a scalar with a vector (left-hand side)
template <typename T>
inline GenericVec3<T> operator*(const T val, const GenericVec3<T>& vec) {
  return {val * vec.x, val * vec.y, val * vec.z};
}

//! Define division of a scalar by a vector (left-hand side)
template <typename T>
inline GenericVec3<T> operator/(const T val, const GenericVec3<T>& vec) {
  // TODO: add compile-time option to check for division by zero
  return {val / vec.x, val / vec.y, val / vec.z};
}

//! Define the component-wise minimum of two vectors
template <typename T>
inline GenericVec3<T> min(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.min(other);
}

//! Define the component-wise maximum of two vectors
template <typename T>
inline GenericVec3<T> max(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.max(other);
}

//! Define the unit vector of a vector (out of place)
template <typename T>
inline GenericVec3<T> unit_vector(const GenericVec3<T>& vec) {
  return vec.unit_vector();
}

//! Define the norm of a vector
template <typename T>
inline T norm(const GenericVec3<T>& vec) {
  return vec.norm();
}

//! Define the square of the norm of a vector
template <typename T>
inline T norm2(const GenericVec3<T>& vec) {
  return vec.dot();
}

//! Define the dot product of two vectors
template <typename T>
inline T dot(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.dot(other);
}

//! Define the square of the dot product of two vectors
template <typename T>
inline T dot2(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.dot2(other);
}

//! Define the distance between two vectors
template <typename T>
inline T dist(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.dist(other);
}

//! Define the cross product of two vectors
template <typename T>
inline GenericVec3<T> cross(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.cross(other);
}

//! Define the norm of the cross product of two vectors: ||A x B||
template <typename T>
inline T cross_norm(const GenericVec3<T>& vec, const GenericVec3<T>& other) {
  return vec.cross_norm(other);
}

//! Define the square root of a vector
template <typename T>
inline GenericVec3<T> sqrt(const GenericVec3<T>& vec) {
  return {sqrt(vec.x), sqrt(vec.y), sqrt(vec.z)};
}

//! Define the output operator for a vector
template <typename T>
inline std::ostream& operator<<(std::ostream& os, const GenericVec3<T>& vec) {
  const T zero = static_cast<T>(0);
  return os << "(" << ((vec.x == -zero) ? zero : vec.x) << ", "
                   << ((vec.y == -zero) ? zero : vec.y) << ", "
                   << ((vec.z == -zero) ? zero : vec.z) << ")";
}

//! Define the to_string function for a vector
template <typename T>
inline std::string to_string(const GenericVec3<T>& vec) {
  return "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ", " + std::to_string(vec.z) + ")";
}


//! 2-vector class with (\f$x, y\f$) order.
struct Vec2 {
  double x = 0.;  //!< x-component
  double y = 0.;  //!< y-component

  //! Default constructor
  Vec2() = default;

  //! Constructor based on (x,y) values
  template <typename T>
  Vec2(T x_, T y_) : x(x_), y(y_) {}

  //! Constructor based on a single value
  template <typename T>
  Vec2(T x_) : x(x_), y(x_) {}

  //! Minimum of all components
  inline double min() const { return (x < y) ? x : y; }

  //! Maximum of all components
  inline double max() const { return (x > y) ? x : y; }

  //! Check if x and y are the same
  inline bool xy_equal() const { return x == y; }

  //! Calculate the norm of the vector
  inline double norm() const { return sqrt(x * x + y * y); }

  //! Calculate the squared norm of the vector
  inline double dot() const { return x * x + y * y; }

  //! Check for equality with a scalar
  inline bool operator==(const double val) const { return (x == val) && (y == val); }

  //! Check for equality with another vector
  inline bool operator==(const Vec2& other) const { return (x == other.x) && (y == other.y); }

  //! Check for inequality with a scalar
  inline bool operator!=(const double val) const { return (x != val) || (y != val); }

  //! Check for inequality with another vector
  inline bool operator!=(const Vec2& other) const { return (x != other.x) || (y != other.y); }

  //! Check greater than another vector
  inline bool operator>(const Vec2& other) const { return (x > other.x) && (y > other.y); }

  //! Check greater than or equal to another vector
  inline bool operator>=(const Vec2& other) const { return (x >= other.x) && (y >= other.y); }

  //! Check less than another vector
  inline bool operator<(const Vec2& other) const { return (x < other.x) && (y < other.y); }

  //! Check less than or equal to another vector
  inline bool operator<=(const Vec2& other) const { return (x <= other.x) && (y <= other.y); }

  //! Define addition of two vectors
  inline Vec2 operator+(const Vec2& other) const { return {x + other.x, y + other.y}; }

  //! Define addition with a scalar
  inline Vec2 operator+(const double val) const { return {x + val, y + val}; }

  //! Define in-place addition with another vector
  inline void operator+=(const Vec2& other) { x += other.x; y += other.y; }

  //! Define subtraction of two vectors
  inline Vec2 operator-(const Vec2& other) const { return {x - other.x, y - other.y}; }

  //! Define subtraction of a scalar
  inline Vec2 operator-(const double val) const { return {x - val, y - val}; }

  //! Define in-place subtraction with another vector
  inline void operator-=(const Vec2& other) { x -= other.x; y -= other.y; }

  //! Define multiplication of two vectors
  inline Vec2 operator*(const Vec2& other) const { return {x * other.x, y * other.y}; }

  //! Define multiplication with a scalar
  inline Vec2 operator*(const double val) const { return {x * val, y * val}; }

  //! Define in-place division by another vector
  inline Vec2 operator/(const Vec2& other) const { return {x / other.x, y / other.y}; }

  //! Define division by a scalar
  inline Vec2 operator/(const double val) const { return {x / val, y / val}; }

  //! Define in-place multiplication with a scalar
  inline void operator*=(const double val) { x *= val; y *= val; }

  //! Define in-place division by a scalar
  inline void operator/=(const double val) { x /= val; y /= val; }
};

//! Define addition of a scalar with a vector (left-hand side)
inline Vec2 operator+(const double val, const Vec2& vec) { return {val + vec.x, val + vec.y}; }

//! Define subtraction of a scalar from a vector (left-hand side)
inline Vec2 operator-(const double val, const Vec2& vec) { return {val - vec.x, val - vec.y}; }

//! Define multiplication of a scalar with a vector (left-hand side)
inline Vec2 operator*(const double val, const Vec2& vec) { return {val * vec.x, val * vec.y}; }

//! Define division of a scalar by a vector (left-hand side)
inline Vec2 operator/(const double val, const Vec2& vec) { return {val / vec.x, val / vec.y}; }

//! Define the norm of a vector
inline double norm(const Vec2& vec) { return vec.norm(); }

//! Define the square of the norm of a vector
inline double norm2(const Vec2& vec) { return vec.dot(); }

//! Define the output operator for a vector
inline std::ostream& operator<<(std::ostream& os, const Vec2& vec) {
  return os << "(" << ((vec.x == -0.) ? 0. : vec.x) << ", " << ((vec.y == -0.) ? 0. : vec.y) << ")";
}

//! Define the to_string function for a vector
inline std::string to_string(const Vec2& vec) {
  return "(" + std::to_string(vec.x) + ", " + std::to_string(vec.y) + ")";
}

using Vec3 = GenericVec3<double>;
using Vec3F = GenericVec3<float>;

#pragma omp declare reduction(+ : Vec3 : omp_out += omp_in) initializer(omp_priv = Vec3())

#endif // VEC3_INCLUDED
