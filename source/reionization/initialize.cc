/******************************
 * reionization/initialize.cc *
 ******************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "../proto.h"
#include "ReionizationBox.h"

/* Setup everything for the reionization module. */
void ReionizationBox::setup() {
  // Allocate projection data
  if (n_projections > 0)
    projections = ImageFs(n_projections, ImageF(nx_pixels, ny_pixels));
}
