/**********************************
 * reionization/ReionizationBox.h *
 **********************************

 * Module declarations.

*/

#ifndef REIONIZATION_BOX_INCLUDED
#define REIONIZATION_BOX_INCLUDED

#include "../Simulation.h" // Base simulation class

// Box data mirors the file format
using ImageF = NDArray<2, float>;
using ImageFs = std::vector<ImageF>;
using BoxField = GenericField<ImageF>;

/* Reionization box data analysis. */
class ReionizationBox : public Simulation {
public:
  void run() override;                       // Module calculations

  // List of possible fields
  enum BoxFieldIndex {
    // 3D grid
    Density = 0,                             // Density index
    DensityStars,                            // Stellar density index
    DensityDust,                             // Dust density index
    DensityMetals,                           // Metal density index
    HII_Fraction,                            // HII fraction index
    HeIII_Fraction,                          // HeIII fraction index
    Temperature,                             // Temperature index
    IonEnergy,                               // Ionizing radiation energy
    // IonFlux,                                 // Ionizing radiation flux

    // Spectral grid
    DensityHI,                               // HI density index
    DensityEOS,                              // EOS density index
    LyaLuminosityRec,                        // Lyman-alpha recombinations index
    LyaLuminosityCol,                        // Lyman-alpha collisions index
    IonLuminosityStars,                      // Ionizing luminosity index
    IonLuminosityAGN,                        // Ionizing luminosity index

    // Likely to be depricated
    NumParticles,                            // Number of gas particles index
    Volume,                                  // Volume index
    DensityAGN,                              // Black hole density index
    Metallicity,                             // Gas metallicity index
    DustMetallicity,                         // Dust-to-gas ratio index
    VelocitySquared,                         // Velocity^2 index
    // NumPhotons,                              // Number of photons index
    // Velocities,                              // Gas velocity index
    n_box_fields                             // Number of fields
  };

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void read_hdf5() override;                 // Reads data and info from files
  void write_hdf5() override;                // Writes data and info to a file

private:
  double unit_length;                        // Unit length [cm]
  double unit_mass;                          // Unit mass [g]
  double unit_velocity;                      // Unit velocity [cm/s]

  double box_size;                           // Box size [a*length/h]
  double box_area;                           // Box area [a*length/h]^2
  double box_volume;                         // Box volume [a*length/h]^3

  double cell_width;                         // Cell width [a*length/h]
  double cell_area;                          // Cell area [a*length/h]^2
  double cell_volume;                        // Cell volume [a*length/h]^3

  int n_pixels = 1;                          // Number of pixels for each axis
  int n2_pixels;                             // Number of pixels squared
  size_t n_cells;                            // Total number of cells

  int n_files_tot = 1;                       // Number of initial conditions files
  int n_files = 1;                           // Number of local files
  int first_file = 0;                        // File ownership range
  vector<int> file_sizes;                    // Dataset sizes on each file
  vector<int> n_columns;                     // Number of columns on each file
  vector<int> offsets;                       // Column offsets for each file

  array<BoxField, n_box_fields> fields;      // File dataset fields

  int n_projections = 0;                     // Number of projections
  vector<FieldWeightPair> proj_list;         // Projection (field, weight) list
  ImageFs projections;                       // Projection data

  // Generate a rendering subfile name
  string pre_filename;                       // Common part of the file names
  string sub_file(const int file) {
    string str = to_string(first_file + file); // Setup leading zeros
    return pre_filename + string(3 - str.length(), '0') + str + "." + init_ext;
  }

  void read_info();                          // Read top level information
  void read_dataset(const int field, const int file); // Read specific data
  void setup_dataspace();                    // Allocate space for each file
  void setup_fields();                       // Initialize active fields
  void read_data();                          // Read full datasets
  vector<int> calculate_rank_offsets();      // MPI convenience function
  void calculate_projections();              // Calculate all projections
};

#endif // REIONIZATION_BOX_INCLUDED
