/***************************
 * ionization/Ionization.h *
 ***************************

 * Module declarations.

*/

#ifndef IONIZATION_INCLUDED
#define IONIZATION_INCLUDED

#include "../Simulation.h" // Base simulation class

/* Ionization Monte Carlo radiative transfer module. */
class Ionization : public Simulation {
public:
  void run() override;                       // Module calculations

protected:
  void module_config(YAML::Node& file) override;
  void setup() override;                     // Setup simulation data
  void print_info() override;                // Print simulation information
  void write_module(const H5::H5File& f) override; // Module output

private:
  int max_iter = 10;                         // Maximum number of iterations
  double max_error = 0.01;                   // Relative error for convergence

  bool dialectric_recombination = true;      // Include dielectric recombination
  bool charge_exchange = true;               // Include charge exchange
  string UVB_model = "";                     // UV background model (FG11)
  bool have_UVB = false;                     // Flag for uniform ionizing background
  double UVB_rate_HI = 0.;                   // UV background ionization for HI [1/s]
  double UVB_rate_HeI = 0.;                  // UV background ionization for HeI [1/s]
  double UVB_rate_HeII = 0.;                 // UV background ionization for HeII [1/s]
  double UVB_heat_HI = 0.;                   // UV background photoheating for HI [erg/s]
  double UVB_heat_HeI = 0.;                  // UV background photoheating for HeI [erg/s]
  double UVB_heat_HeII = 0.;                 // UV background photoheating for HeII [erg/s]
  bool self_shielding = true;                // Include self-shielding
  double SS_alpha1 = 0.;                     // Self-shielding parameters
  double SS_alpha2 = 0.;                     // See Rahmati (2012)
  double SS_beta = 0.;
  double SS_xi = 0.;
  double SS_n0 = 0.;
  double SS_f = 0.;

  // Convergence history
  vectors x_V_ions;                          // Volume-weighted fractions
  vectors x_m_ions;                          // Mass-weighted fractions
  vector<double> f_col_HI;                   // Fraction of total ionizations
  vector<double> f_UVB_HI;                   // from collisions and UVB photons
  vectors rec_ions;                          // Recombinations [events/s]
  vectors col_ions;                          // Collisional ionizations [events/s]
  vectors phot_ions;                         // Photoionizations [events/s]
  vectors cxi_H_ions;                        // Charge exchange ionizations (H) [events/s]
  vectors cxi_He_ions;                       // Charge exchange ionizations (He) [events/s]
  vectors cxr_H_ions;                        // Charge exchange recombinations (H) [events/s]
  vectors cxr_He_ions;                       // Charge exchange recombinations (He) [events/s]

  void setup_UVB_FG11();                     // UV background initialization
  void setup_self_shielding();               // Self-shielding parameters
  double self_shielding_factor(const double nH); // Calculate the self-shielding factor
  void correct_units();                      // Convert to observed units
  void equilibrium_update(const bool active); // Photo-equilibrium abundances
  void print_convergence();                  // Print convergence statistics
  void write_ionization_info(const H5::H5File& f); // Write simulation information
  void iterative_setup();                    // Setup performed during each iteration
  void iterative_setup_init();               // Global initializations each iteration
  void iterative_setup_calc();               // Calculations performed each iteration
  void initial_setup_stars();                // Stellar emission
  void iterative_setup_stars();
  void initial_setup_gas();                  // Gas emission
  void iterative_setup_gas();
  void initial_setup_AGN();                  // AGN emission
  void iterative_setup_AGN();
  void iterative_setup_point();              // Point source
  void iterative_setup_plane();              // Plane source
  void calculate_refinement();               // Output a refined version of the grid
};

#endif // IONIZATION_INCLUDED
