/*************************
 * ionization/scatter.cc *
 *************************

 * Scattering Event: Scattering angle, optical depth, etc.

*/

#include "proto.h"
#include "photon.h" // Photon packets

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
#if spherical_escape
double spherical_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding sphere
#endif
#if box_escape
double box_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding box
#endif
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell

static const int ISOTROPIC = 1, DUST_SCAT = 4;
static constexpr double _2pi = 2. * M_PI;

// Constants for Henyey-Greenstein dust scattering
static double _1_2g[n_dust_species], _1pg2_2g[n_dust_species], _1mg2_2D[n_dust_species], _1mg2_2g[n_dust_species], _1mg_2g[n_dust_species];
#pragma omp threadprivate(_1_2g, _1pg2_2g, _1mg2_2D, _1mg2_2g, _1mg_2g) // Unique to each thread/photon

/* Set the derived dust parameters. */
void set_derived_dust_parameters(const double (&cosines)[n_dust_species]) {
  // Constants for the Henyey-Greenstein dust scattering phase function
  for (int i = 0; i < n_dust_species; ++i) {
    _1_2g[i] = 0.5 / cosines[i];             // = 1 / (2g)
    _1pg2_2g[i] = _1_2g[i] + 0.5 * cosines[i]; // = (1+g^2) / (2g) = 1/(2g) + g/2
    _1mg2_2g[i] = _1_2g[i] - 0.5 * cosines[i]; // = (1-g^2) / (2g) = 1/(2g) - g/2
    _1mg_2g[i]  = _1_2g[i] - 0.5;            // = (1-g) / (2g) = 1/(2g) - 1/2
    _1mg2_2D[i] = _1mg2_2g[i] * 0.5 * sqrt(_1_2g[i]); // = (1-g^2) / [2 (2g)^(3/2)]
  }
}

/* Calculate the dust absorption coefficient for LOS attenuation. */
inline double IonPhoton::k_dust_LOS(const int cell) {
  return                                     // Dust absorption coefficient
  #if multiple_dust_species
    #if graphite_dust
      #if graphite_scaled_PAH
        (fm1_PAH * kappas[GRAPHITE] + f_PAH * kappas[PAH]) * rho_dust_G[cell]
      #else
        kappas[GRAPHITE] * rho_dust_G[cell]  // Graphite
      #endif
    #endif
    #if silicate_dust
      + kappas[SILICATE] * rho_dust_S[cell]; // Silicate
    #endif
  #else
    kappas[0] * rho_dust[cell];              // Single-species dust
  #endif
}

/* Line-of-sight emission calculation. */
void IonPhoton::calculate_LOS_int() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_int(camera);
}

void IonPhoton::calculate_LOS_int(const int camera, const double phase_weight) {
  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_int = phase_weight * source_weight;

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(position, camera); // Position in camera coordinates

  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images_int[camera](ix, iy) += weight_int;
    }
  }

  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_cube_pixels) {  // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      bin_images_int[camera](ix, iy, frequency_bin) += weight_int;
    }
  }

  if (have_radial_cameras) {
    const double r_pos = norm(pos);          // Projected radius
    if (output_radial_images) {
      const double r_pix = inverse_radial_pixel_width * r_pos;
      if (r_pix < double(n_radial_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        radial_images_int[camera][ir] += weight_int;
      }
    }

    if (output_radial_cubes) {
      const double r_pix = inverse_radial_cube_pixel_width * r_pos;
      if (r_pix < double(n_radial_cube_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        bin_radial_images_int[camera](ir, frequency_bin) += weight_int;
      }
    }
  }
}

/* Line-of-sight attenuation calculation. */
void IonPhoton::calculate_LOS_ext() {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS_ext(camera);
}

void IonPhoton::calculate_LOS_ext(const int camera, const double phase_weight) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double tau_esc = -log(weight);             // Optical depth to escape
  double l;                                  // Path length [cm]
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  #if check_escape
    double l_exit = positive_infinity;       // Escape distance
  #endif
  #if spherical_escape                       // Spherical escape distance
    inplace_min(l_exit, spherical_escape_distance(point, k_cam));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(l_exit, box_escape_distance(point, k_cam));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(l_exit, max_streaming);
  #endif
  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        return;                              // No point to continue
    }

    // Check for spherical or box escape
    #if check_escape
      if (l_exit <= l) {                     // Exit region before cell
        tau_esc += k_dust_LOS(cell) * l_exit; // Cumulative LOS optical depth
        if (tau_esc > tau_discard)           // Early exit condition
          return;                            // No point to continue
        break;                               // Valid tau_esc completion
      }
      l_exit -= l;                           // Remaining distance to exit
    #endif

    // Cumulative LOS optical depth (dust)
    tau_esc += k_dust_LOS(cell) * l;

    // Discard photons from the calculation if tau_esc > tau_discard
    if (tau_esc > tau_discard)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;

    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  const double weight_ext = phase_weight * exp(-tau_esc);

  // Calculate the escape fraction for each camera
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs_ext[camera] += weight_ext;

  if (output_bin_escape_fractions)
    #pragma omp atomic
    bin_f_escs_ext[camera][frequency_bin] += weight_ext;

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(point, camera);   // Position in camera coordinates

  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images_ext[camera](ix, iy) += weight_ext;
    }
  }

  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_cube_pixels) {  // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      bin_images_ext[camera](ix, iy, frequency_bin) += weight_ext;
    }
  }

  if (have_radial_cameras) {
    const double r_pos = norm(pos);          // Projected radius
    if (output_radial_images) {
      const double r_pix = inverse_radial_pixel_width * r_pos;
      if (r_pix < double(n_radial_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        radial_images_ext[camera][ir] += weight_ext;
      }
    }

    if (output_radial_cubes) {
      const double r_pix = inverse_radial_cube_pixel_width * r_pos;
      if (r_pix < double(n_radial_cube_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        bin_radial_images_ext[camera](ir, frequency_bin) += weight_ext;
      }
    }
  }
}

/* Line-of-sight calculation via next event estimation. */
void IonPhoton::calculate_LOS(const int scatter_type) {
  for (int camera = 0; camera < n_cameras; ++camera)
    calculate_LOS(scatter_type, camera);
}

void IonPhoton::calculate_LOS(const int scatter_type, const int camera) {
  int cell = current_cell;                   // Current cell index
  int next_cell;                             // Next cell index
  double tau_esc = -log(weight);             // Optical depth to escape
  double k_eff;                              // Extinction coefficient
  double l;                                  // Path length [cm]
  Vec3 point = position;                     // Photon position
  Vec3 k_cam = camera_directions[camera];    // Camera direction
  #if check_escape
    double l_exit = positive_infinity;       // Escape distance
  #endif
  #if spherical_escape                       // Spherical escape distance
    inplace_min(l_exit, spherical_escape_distance(point, k_cam));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(l_exit, box_escape_distance(point, k_cam));
  #endif
  while (true) {                             // Ray trace until escape
    // Compute the maximum distance the photon can travel in the cell
    tie(l, next_cell) = face_distance(point, k_cam, cell);

    if constexpr (SPHERICAL) {
      if (next_cell == INSIDE)               // Check if the ray is trapped
        return;                              // No point to continue
    }

    // Check for spherical or box escape
    #if check_escape
      if (l_exit <= l) {                     // Exit region before cell
        l = l_exit;                          // Set neighbor distance to exit
        next_cell = OUTSIDE;                 // Flag for escape condition
      }
      l_exit -= l;                           // Remaining distance to exit
    #endif

    // Cumulative LOS optical depth (ionization + dust)
    k_eff = k_dust_LOS(cell);                // Dust absorption coefficient
    for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
      const double n_atom = fields[n_indices[aa]].data[cell]; // Atom number density [cm^-3]
      const int ion_count = ion_counts[aa];  // Number of active ions
      for (int j = 0; j < ion_count; ++j, ++ai) {
        const double x_ion = fields[x_indices[ai]].data[cell]; // Ionization fraction
        k_eff += x_ion * n_atom * sigma_ions[ai]; // Absorption coefficient
      }
    }
    tau_esc += k_eff * l;

    // Discard photons from the calculation if tau_esc > tau_discard
    if (tau_esc > tau_discard)
      return;                                // No point to continue

    // Calculate the new position of the photon
    point += k_cam * l;                      // Update photon position

    if (next_cell == OUTSIDE)                // Check if the photon escapes
      break;
    cell = next_cell;                        // Update the cell index
  }

  // Surface brightness LOS weight: sum( W(mu) * exp(-tau_esc) )
  double mu, weight_cam;
  switch (scatter_type) {                    // Determine the scattering weight
    case ISOTROPIC:                          // Isotropic scattering
      weight_cam = 0.5;
      break;
    case DUST_SCAT:                          // Here W = 1/2 (1 - g^2) / (1 + g^2 - 2gμ)^(3/2)
      mu = direction.dot(k_cam);
      weight_cam = _1mg2_2D[dust_species] * pow(_1pg2_2g[dust_species] - mu, -1.5);
      break;
    default:
      weight_cam = 0.;
      error("Unrecognized scatter type in camera calculation.");
  }

  // Combine the phase and optical depth weights
  weight_cam *= exp(-tau_esc);

  // Calculate the escape fraction for each camera
  if (output_escape_fractions)
    #pragma omp atomic
    f_escs[camera] += weight_cam;

  if (output_bin_escape_fractions)
    #pragma omp atomic
    bin_f_escs[camera][frequency_bin] += weight_cam;

  // Note: Convert the position to image coordinates *before* casting
  // to an int to avoid overflow and entering UB territory

  // Bin the photon's position - Only add to bins in the specified range
  const Vec2 pos = project(point, camera);   // Position in camera coordinates

  if (output_images) {
    const Vec2 pix = inverse_pixel_widths * pos + pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_pixels) {       // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      images[camera](ix, iy) += weight_cam;
    }
  }

  if (output_cubes) {
    const Vec2 pix = inverse_cube_pixel_widths * pos + cube_pixel_offsets; // Pixel coordinates
    if (pix >= 0. && pix < n_cube_pixels) {  // Range check
      const int ix = floor(pix.x), iy = floor(pix.y); // Pixel indices
      #pragma omp atomic
      bin_images[camera](ix, iy, frequency_bin) += weight_cam;
    }
  }

  if (have_radial_cameras) {
    const double r_pos = norm(pos);          // Projected radius
    if (output_radial_images) {
      const double r_pix = inverse_radial_pixel_width * r_pos;
      if (r_pix < double(n_radial_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        radial_images[camera][ir] += weight_cam;
      }
    }

    if (output_radial_cubes) {
      const double r_pix = inverse_radial_cube_pixel_width * r_pos;
      if (r_pix < double(n_radial_cube_pixels)) { // Range check
        const int ir = floor(r_pix);
        #pragma omp atomic
        bin_radial_images[camera](ir, frequency_bin) += weight_cam;
      }
    }
  }
}

/* Generate random angles from an anisotropic distribution (dust). */
static Vec3 anisotropic_direction_HG(const Vec3 k, const int dust_species) {
  // Henyey-Greenstein phase function for <μ>: ζ = random number in [0,1]
  const double xiS = _1mg2_2g[dust_species] / (_1mg_2g[dust_species] + ran()); // ξ = (1-g^2) / (1-g+2gζ)
  const double muS = _1pg2_2g[dust_species] - _1_2g[dust_species] * xiS * xiS; // μ = (1+g^2-ξ^2) / (2g)

  // Precompute sin and cos multiplied by sqrt(1-mu^2)
  const double phi = _2pi * ran();           // Arbitrary azimuthal angle
  const double sqrt_1muS2 = sqrt(1. - muS * muS);
  const double sin_phi = sqrt_1muS2 * sin(phi);
  const double cos_phi = sqrt_1muS2 * cos(phi);
  const double zS = sqrt(1. - k.z * k.z);    // sqrt(kx^2+ky^2) = sqrt(1-kz^2)

  // Outgoing direction in terms of the angles mu and phi
  Vec3 k_out {
    muS * k.x + (k.y * cos_phi + k.x * k.z * sin_phi) / zS,
    muS * k.y - (k.x * cos_phi - k.y * k.z * sin_phi) / zS,
    muS * k.z - sin_phi * zS,
  };
  k_out.normalize();                         // Normalize to avoid error
  return k_out;
}

// /* Isolate everything that happens during the ionization scattering event. */
// void IonPhoton::ionization_scatter() {
//   // Camera calculations using the Isotropic phase function
//   calculate_LOS(ISOTROPIC);

//   // Outgoing direction from the anisotropic Henyey-Greenstein phase function
//   direction = isotropic_direction();
// }

/* Isolate everything that happens during the dust scattering event. */
void IonPhoton::dust_scatter() {
  // Camera calculations using the dust phase function
  calculate_LOS(DUST_SCAT);

  // Outgoing direction from the anisotropic Henyey-Greenstein phase function
  direction = anisotropic_direction_HG(direction, dust_species);
}
