/****************************
 * ionization/ionization.cc *
 ****************************

 * Driver: Assign photon packets for radiative transfer, etc.

*/

#include "proto.h"
#include "Ionization.h"
#include "photon.h" // Photon packets
#include "../mpi_helpers.h" // MPI helper functions
#include "../timing.h" // Timing functionality
#include "../rates.h" // Thermochemistry
#include "../reduction.h" // Reduction operations
#include <chrono>

extern Timer mcrt_timer; // Clock timing
extern Timer reduce_timer; // MPI timing

void print_ionization_observables(); // Post-calculation printing
bool avoid_cell_strict(const int cell); // Avoid calculations without overrides
Vec3 cell_center(const int cell); // Center of the cell

/* Print the relative error percentage between two values with safe division. */
static inline void print_pdiff(string name, const double x_old, const double x_ref, char rep = '\0') {
  if (rep != '\0' && !name.empty()) name[0] = rep;  // Change prefix character if rep is provided
  cout << "  " << std::setw(8) << name << " | " << std::setw(11) << x_old << "  -->  "
       << std::setw(11) << x_ref << "  (" << ((x_ref > 0.) ? 100. * fabs(1. - x_old / x_ref) : 0.) << "%)" << endl;
}

/* Total recombination rate coefficients [cm^3/s] */
static const array<double (*)(const double), n_ions> all_alpha_RR_functions = {
  alpha_B_HII, alpha_B_HeII, alpha_B_HeIII,
  alpha_RR_CII, alpha_RR_CIII, alpha_RR_CIV, alpha_RR_CV, alpha_RR_CVI, alpha_RR_CVII,
  alpha_RR_NII, alpha_RR_NIII, alpha_RR_NIV, alpha_RR_NV, alpha_RR_NVI, alpha_RR_NVII, alpha_RR_NVIII,
  alpha_RR_OII, alpha_RR_OIII, alpha_RR_OIV, alpha_RR_OV, alpha_RR_OVI, alpha_RR_OVII, alpha_RR_OVIII, alpha_RR_OIX,
  alpha_RR_NeII, alpha_RR_NeIII, alpha_RR_NeIV, alpha_RR_NeV, alpha_RR_NeVI, alpha_RR_NeVII, alpha_RR_NeVIII, alpha_RR_NeIX,
  alpha_RR_MgII, alpha_RR_MgIII, alpha_RR_MgIV, alpha_RR_MgV, alpha_RR_MgVI, alpha_RR_MgVII, alpha_RR_MgVIII, alpha_RR_MgIX, alpha_RR_MgX, alpha_RR_MgXI,
  alpha_RR_SiII, alpha_RR_SiIII, alpha_RR_SiIV, alpha_RR_SiV, alpha_RR_SiVI, alpha_RR_SiVII, alpha_RR_SiVIII, alpha_RR_SiIX, alpha_RR_SiX, alpha_RR_SiXI, alpha_RR_SiXII, alpha_RR_SiXIII,
  alpha_RR_SII, alpha_RR_SIII, alpha_RR_SIV, alpha_RR_SV, alpha_RR_SVI, alpha_RR_SVII, alpha_RR_SVIII, alpha_RR_SIX, alpha_RR_SX, alpha_RR_SXI, alpha_RR_SXII, alpha_RR_SXIII, alpha_RR_SXIV, alpha_RR_SXV,
  alpha_RR_FeII, alpha_RR_FeIII, alpha_RR_FeIV, alpha_RR_FeV, alpha_RR_FeVI, alpha_RR_FeVII, alpha_RR_FeVIII, alpha_RR_FeIX, alpha_RR_FeX, alpha_RR_FeXI, alpha_RR_FeXII, alpha_RR_FeXIII, alpha_RR_FeXIV, alpha_RR_FeXV, alpha_RR_FeXVI, alpha_RR_FeXVII
};

/* Total diaelectric recombination rate coefficients [cm^3/s] */
static const array<double (*)(const double), n_ions> all_alpha_DR_functions = {
  return_zero, alpha_DR_HeII, return_zero,
  alpha_DR_CII, alpha_DR_CIII, alpha_DR_CIV, alpha_DR_CV, alpha_DR_CVI, return_zero,
  alpha_DR_NII, alpha_DR_NIII, alpha_DR_NIV, alpha_DR_NV, alpha_DR_NVI, alpha_DR_NVII, return_zero,
  alpha_DR_OII, alpha_DR_OIII, alpha_DR_OIV, alpha_DR_OV, alpha_DR_OVI, alpha_DR_OVII, alpha_DR_OVIII, return_zero,
  alpha_DR_NeII, alpha_DR_NeIII, alpha_DR_NeIV, alpha_DR_NeV, alpha_DR_NeVI, alpha_DR_NeVII, alpha_DR_NeVIII, return_zero,
  alpha_DR_MgII, alpha_DR_MgIII, alpha_DR_MgIV, alpha_DR_MgV, alpha_DR_MgVI, alpha_DR_MgVII, alpha_DR_MgVIII, alpha_DR_MgIX, alpha_DR_MgX, return_zero,
  alpha_DR_SiII, alpha_DR_SiIII, alpha_DR_SiIV, alpha_DR_SiV, alpha_DR_SiVI, alpha_DR_SiVII, alpha_DR_SiVIII, alpha_DR_SiIX, alpha_DR_SiX, alpha_DR_SiXI, alpha_DR_SiXII, return_zero,
  alpha_DR_SII, alpha_DR_SIII, alpha_DR_SIV, alpha_DR_SV, alpha_DR_SVI, alpha_DR_SVII, alpha_DR_SVIII, alpha_DR_SIX, alpha_DR_SX, alpha_DR_SXI, alpha_DR_SXII, alpha_DR_SXIII, alpha_DR_SXIV, return_zero,
  alpha_DR_FeII, alpha_DR_FeIII, alpha_DR_FeIV, alpha_DR_FeV, alpha_DR_FeVI, alpha_DR_FeVII, alpha_DR_FeVIII, alpha_DR_FeIX, alpha_DR_FeX, alpha_DR_FeXI, alpha_DR_FeXII, alpha_DR_FeXIII, alpha_DR_FeXIV, alpha_DR_FeXV, alpha_DR_FeXVI, return_zero
};

/* Collisional ionization rate coefficients [cm^3/s] (Cen 1992; Voronov 1997) */
static const array<double (*)(const double), n_ions> all_beta_functions = {
  beta_HI, beta_HeI, beta_HeII,
  beta_CI, beta_CII, beta_CIII, beta_CIV, beta_CV, beta_CVI,
  beta_NI, beta_NII, beta_NIII, beta_NIV, beta_NV, beta_NVI, beta_NVII,
  beta_OI, beta_OII, beta_OIII, beta_OIV, beta_OV, beta_OVI, beta_OVII, beta_OVIII,
  beta_NeI, beta_NeII, beta_NeIII, beta_NeIV, beta_NeV, beta_NeVI, beta_NeVII, beta_NeVIII,
  beta_MgI, beta_MgII, beta_MgIII, beta_MgIV, beta_MgV, beta_MgVI, beta_MgVII, beta_MgVIII, beta_MgIX, beta_MgX,
  beta_SiI, beta_SiII, beta_SiIII, beta_SiIV, beta_SiV, beta_SiVI, beta_SiVII, beta_SiVIII, beta_SiIX, beta_SiX, beta_SiXI, beta_SiXII,
  beta_SI, beta_SII, beta_SIII, beta_SIV, beta_SV, beta_SVI, beta_SVII, beta_SVIII, beta_SIX, beta_SX, beta_SXI, beta_SXII, beta_SXIII, beta_SXIV,
  beta_FeI, beta_FeII, beta_FeIII, beta_FeIV, beta_FeV, beta_FeVI, beta_FeVII, beta_FeVIII, beta_FeIX, beta_FeX, beta_FeXI, beta_FeXII, beta_FeXIII, beta_FeXIV, beta_FeXV, beta_FeXVI
};

/* Hydrogen charge exchange recombination rate coefficients [cm^3/s] (Kingdon & Ferland 1996) */
static const array<double (*)(const double), n_ions> all_xi_HI_functions = {
  return_zero, xi_HI_HeII, xi_HI_HeIII,
  xi_HI_CII, xi_HI_CIII, xi_HI_CIV, xi_HI_CV, return_zero, return_zero,
  xi_HI_NII, xi_HI_NIII, xi_HI_NIV, xi_HI_NV, return_zero, return_zero, return_zero,
  xi_HI_OII, xi_HI_OIII, xi_HI_OIV, xi_HI_OV, return_zero, return_zero, return_zero, return_zero,
  return_zero, xi_HI_NeIII, xi_HI_NeIV, xi_HI_NeV, return_zero, return_zero, return_zero, return_zero,
  return_zero, xi_HI_MgIII, xi_HI_MgIV, xi_HI_MgV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, xi_HI_SiIII, xi_HI_SiIV, xi_HI_SiV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  xi_HI_SII, xi_HI_SIII, xi_HI_SIV, xi_HI_SV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, xi_HI_FeIII, xi_HI_FeIV, xi_HI_FeV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero
};

/* Helium charge exchange recombination rate coefficients [cm^3/s] (Kingdon & Ferland 1996) */
static const array<double (*)(const double), n_ions> all_xi_HeI_functions = {
  return_zero, return_zero, return_zero,
  return_zero, return_zero, xi_HeI_CIV, xi_HeI_CV, return_zero, return_zero,
  return_zero, xi_HeI_NIII, xi_HeI_NIV, xi_HeI_NV, return_zero, return_zero, return_zero,
  return_zero, xi_HeI_OIII, xi_HeI_OIV, xi_HeI_OV, return_zero, return_zero, return_zero, return_zero,
  return_zero, xi_HeI_NeIII, xi_HeI_NeIV, xi_HeI_NeV, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, xi_HeI_MgIV, xi_HeI_MgV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, xi_HeI_SiIV, xi_HeI_SiV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, xi_HeI_SIV, xi_HeI_SV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, xi_HeI_FeIV, xi_HeI_FeV, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero
};

/* Hydrogen charge exchange ionization rate coefficients [cm^3/s] (Kingdon & Ferland 1996) */
static const array<double (*)(const double), n_ions> all_chi_HII_functions = {
  return_zero, return_zero, return_zero,
  chi_HII_CI, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HII_NI, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HII_OI, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HII_MgI, chi_HII_MgII, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HII_SiI, chi_HII_SiII, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HII_SI, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HII_FeI, chi_HII_FeII, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero
};

/* Helium charge exchange ionization rate coefficients [cm^3/s] */
static const array<double (*)(const double), n_ions> all_chi_HeII_functions = {
  return_zero, return_zero, return_zero,
  chi_HeII_CI, chi_HeII_CII, return_zero, return_zero, return_zero, return_zero,
  return_zero, chi_HeII_NII, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HeII_OI, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  chi_HeII_SiI, chi_HeII_SiII, chi_HeII_SiIII, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, chi_HeII_SII, chi_HeII_SIII, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero,
  return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero, return_zero
};

/*************/
/* Utilities */
/*************/

static inline void print_top() {
  cout << "\n+----------+------------+------------+------------+"
       << "\n| Progress |  Elapsed   |  Estimate  |  Remaining |"
       << "\n+----------+------------+------------+------------+";
}

static inline void print_bottom() {
  cout << "\n+----------+------------+------------+------------+" << endl;
}

/* Print the simulation progress and remaining time estimate. */
static inline void print_progress(const std::chrono::system_clock::time_point start, const int finished, const int total) {
  auto time_elapsed = std::chrono::system_clock::now() - start;
  auto time_remaining = time_elapsed * double(total - finished) / double(finished);
  long long seconds_elapsed = std::chrono::duration_cast<std::chrono::seconds>(time_elapsed).count();
  long long seconds_remaining = std::chrono::duration_cast<std::chrono::seconds>(time_remaining).count();
  long long seconds_estimate = seconds_elapsed + seconds_remaining;
  printf("\n|  %5.1f%%  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |  %02lld:%02lld:%02lld  |",
         100. * double(finished) / double(total),
         seconds_elapsed / 3600, (seconds_elapsed / 60) % 60, seconds_elapsed % 60,
         seconds_estimate / 3600, (seconds_estimate / 60) % 60, seconds_estimate % 60,
         seconds_remaining / 3600, (seconds_remaining / 60) % 60, seconds_remaining % 60);
}

/* Move vector elements within a vector over disjoint ranges. */
template <typename T>
static inline void disjoint_move(vector<T>& vec, const int last, const int move) {
  #pragma omp parallel for
  for (int i = n_escaped; i < last; ++i)
    vec[i+move] = vec[i];                    // Move non-overlapping elements
}

/* Move absorbed photons in the info vectors. */
static inline void move_absorbed_photons(const int dest) {
  const int offset = dest - n_escaped;       // Memory offset for absorbed photons
  if (offset == 0 || n_absorbed == 0)
    return;                                  // No need to move absorbed photons
  if (offset < 0 || n_absorbed < 0 || dest + n_absorbed > n_photons)
    error("Attemping to move absorbed photons out of expected bounds: n_absorbed = " +
          to_string(n_absorbed) + ", dest = " + to_string(dest) + ", n_escaped = " +
          to_string(n_escaped) + ", n_photons = " + to_string(n_photons));
  // Move range: [n_escaped, n_escaped + n_absorbed) -> [dest, dest + n_absorbed)
  // These overlap if n_absorbed > offset, so only move non-overlapping elements
  const int last = (n_absorbed > offset) ? dest : n_escaped + n_absorbed;
  const int move = (n_absorbed > offset) ? n_absorbed : offset;
  disjoint_move(source_ids, last, move);
  if (n_ion_source_types > 1) disjoint_move(source_types, last, move);
  disjoint_move(source_weights, last, move);
  if (output_n_scat) disjoint_move(n_scats, last, move);
  disjoint_move(freq_bins, last, move);
  disjoint_move(weights, last, move);
  disjoint_move(dust_weights, last, move);
  for (auto& ion_weight : ion_weights)
    disjoint_move(ion_weight, last, move);
  if (output_absorption_distance) disjoint_move(dist_abss, last, move);
  if (output_source_position) disjoint_move(source_positions, last, move);
  disjoint_move(positions, last, move);
  disjoint_move(directions, last, move);
}

/* Swap photons in the info vectors. */
static inline void swap_photons(const int i, const int j) {
  std::swap(source_ids[i], source_ids[j]);
  if (n_ion_source_types > 1) std::swap(source_types[i], source_types[j]);
  std::swap(source_weights[i], source_weights[j]);
  if (output_n_scat) std::swap(n_scats[i], n_scats[j]);
  std::swap(freq_bins[i], freq_bins[j]);
  std::swap(weights[i], weights[j]);
  std::swap(dust_weights[i], dust_weights[j]);
  for (auto& ion_weight : ion_weights)
    std::swap(ion_weight[i], ion_weight[j]);
  if (output_absorption_distance) std::swap(dist_abss[i], dist_abss[j]);
  if (output_source_position) std::swap(source_positions[i], source_positions[j]);
  std::swap(positions[i], positions[j]);
  std::swap(directions[i], directions[j]);
}

/* Add photon information to the output buffers. */
static inline void add_to_output_buffers(const struct IonPhoton& p, const int index) {
  source_ids[index] = p.source_id;
  if (n_ion_source_types > 1) source_types[index] = p.source_type;
  source_weights[index] = p.source_weight;
  if (output_n_scat) n_scats[index] = p.n_scat;
  freq_bins[index] = p.frequency_bin;
  weights[index] = p.weight;
  dust_weights[index] = p.dust_weight;
  for (int aiw = 0; aiw < n_active_ion_weights; ++aiw)
    ion_weights[aiw][index] = p.ion_weight[active_ion_weights[aiw]]; // Copy photon ionization weights
  if (output_absorption_distance) dist_abss[index] = p.dist_abs;
  if (output_source_position) source_positions[index] = p.source_position;
  positions[index] = p.position;
  directions[index] = p.direction;
}

/* Sort escaped photons to the beginning and absorbed photons to the end. */
static void sort_photons() {
  // Count the number of escaped and absorbed photons
  n_escaped = n_absorbed = 0;
  #pragma omp parallel for reduction(+:n_escaped, n_absorbed)
  for (int i = 0; i < n_finished; ++i) {
    if (weights[i] > 0.)
      ++n_escaped;                           // Escaped photon
    else
      ++n_absorbed;                          // Absorbed photon
  }

  // Sort the escaped and absorbed photons
  for (int i_esc = 0, i_abs = 0; i_esc < n_escaped; ++i_esc) {
    if (weights[i_esc] > 0.)
      continue;                              // Valid escaped photon
    do {
      ++i_abs;                               // Search from end of list
    } while (weights[n_finished-i_abs] <= 0.); // Find next escaped photon
    swap_photons(i_esc, n_finished-i_abs);   // Swap esc/abs photons
  }
}

/* All tasks have a static assignment of equal work. */
static void equal_workers() {
  if (root)
    print_top();                             // Progress top
  const auto start = std::chrono::system_clock::now(); // Reference time
  int work = n_photons / n_ranks;            // Work assignment
  if (rank < n_photons - work * n_ranks)
    ++work;                                  // Assign remaining work
  const int checks = pow(double(work+1), 0.2); // Number of time checks
  const int interval = work / checks;        // Interval between updates

  #pragma omp parallel for schedule(dynamic)
  for (int i = 0; i < work; ++i) {
    // Create photon packet and perform ray tracing
    auto p = IonPhoton();
    p.ray_trace();

    // Print completed progress and remaining time estimate
    int index;                               // Photon array index
    #pragma omp atomic capture
    index = n_finished++;                    // Update finished counter
    if (output_photons)
      add_to_output_buffers(p, index);       // Populate output buffers
    if (root && ++index % interval == 0)     // Index -> print counter
      print_progress(start, index, work);
  }
  if (root)
    print_bottom();                          // Progress bottom
}

// MPI collection helper struct
struct RecvInfo {
  void *buffer;                              // Address of receive buffer
  int count;                                 // Number of elements in buffer
  MPI_Datatype datatype;                     // Datatype of buffer elements
  int source;                                // Rank of the source
  int tag;                                   // Message tag
};

/* Convenience function to MPI_Recv data to root. */
static inline void mpi_recv(struct RecvInfo& recv_info) {
  MPI_Recv(recv_info.buffer, recv_info.count, recv_info.datatype,
           recv_info.source, recv_info.tag, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

/* Set a vector to zeros. */
static inline void clear(vector<double>& v) {
  #pragma omp parallel for
  for (size_t i = 0; i < v.size(); ++i)
    v[i] = 0.;                               // Clear values
}

/* Set a vector to zeros. */
template <class T>
static inline void clear(vector<T>& vs) {
  for (auto& v : vs) {
    #pragma omp parallel for
    for (size_t i = 0; i < v.size(); ++i)
      v[i] = 0.;                             // Clear values
  }
}

/* Set an Image to zeros. */
static inline void clear(Image& v) {
  #pragma omp parallel for
  for (size_t i = 0; i < v.size(); ++i)
    v[i] = 0.;                               // Clear values
}

/* Set a Cube to zeros. */
static inline void clear(Cube& v) {
  #pragma omp parallel for
  for (size_t i = 0; i < v.size(); ++i)
    v[i] = 0.;                               // Clear values
}

/* Ensure unique MPI message tags for safety. */
enum {
  TAG_EI0, TAG_AI0,                          // Emission cell or star index
  TAG_ET0, TAG_AT0,                          // Emission type specifier
  TAG_EW0, TAG_AW0,                          // Photon weight at emission
  TAG_ENS, TAG_ANS,                          // Number of scattering events
  TAG_EB,  TAG_AB,                           // Frequency at escape [code_freq]
  TAG_EW,  TAG_AW,                           // Photon weight at escape
  TAG_EDW, TAG_ADW,                          // Weight removed by dust absorption
  TAG_EAD, TAG_AAD,                          // Photon absorption distance [cm]
  TAG_EP0, TAG_AP0,                          // Position at emission [cm]
  TAG_EP,  TAG_AP,                           // Position at escape [cm]
  TAG_ED,  TAG_AD,                           // Direction at escape
  TAG_EIW, TAG_AIW = TAG_EIW + n_ions_atoms  // Weight removed by ionization species
};

/* Convenience function to MPI_Send photon info to root. */
static void mpi_collect_photons() {
  // Gather the number of escaped and absorbed photons to root
  vector<int> rank_esc, rank_abs;            // Number of esc/abs photons
  if (root) {
    rank_esc.resize(n_ranks);                // Allocate space for esc counts
    rank_abs.resize(n_ranks);                // Allocate space for abs counts
  }
  MPI_Gather(&n_escaped, 1, MPI_INT, rank_esc.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);
  MPI_Gather(&n_absorbed, 1, MPI_INT, rank_abs.data(), 1, MPI_INT, ROOT, MPI_COMM_WORLD);

  if (root) {
    vector<int> ind_esc, ind_abs;            // Index ranges for rank collection
    ind_esc.resize(n_ranks+1);               // Allocate space for esc indices
    ind_abs.resize(n_ranks+1);               // Allocate space for abs indices
    ind_esc[0] = 0;                          // Start root index at zero
    for (int i = 0; i < n_ranks; ++i)
      ind_esc[i+1] = ind_esc[i] + rank_esc[i];
    ind_abs[0] = ind_esc[n_ranks];           // Start root index at end of esc
    for (int i = 0; i < n_ranks; ++i)
      ind_abs[i+1] = ind_abs[i] + rank_abs[i];

    // Move root absorbed photons to correct global position
    move_absorbed_photons(ind_abs[0]);

    // Build a master list of buffers and receive counts
    vector<struct RecvInfo> buffers;
    for (int i = 1; i < n_ranks; ++i) {
      // Receive escaped photon info
      const int i_esc = ind_esc[i];
      const int n_esc = ind_esc[i+1] - ind_esc[i];
      if (n_esc > 0) {
        buffers.push_back({&source_ids[i_esc], n_esc, MPI_INT, i, TAG_EI0});
        if (n_ion_source_types > 1) buffers.push_back({&source_types[i_esc], n_esc, MPI_INT, i, TAG_ET0});
        buffers.push_back({&source_weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EW0});
        if (output_n_scat) buffers.push_back({&n_scats[i_esc], n_esc, MPI_INT, i, TAG_ENS});
        buffers.push_back({&freq_bins[i_esc], n_esc, MPI_INT, i, TAG_EB});
        buffers.push_back({&weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EW});
        buffers.push_back({&dust_weights[i_esc], n_esc, MPI_DOUBLE, i, TAG_EDW});
        for (int aiw = 0; aiw < n_active_ion_weights; ++aiw)
          buffers.push_back({&ion_weights[aiw][i_esc], n_esc, MPI_DOUBLE, i, TAG_EIW + aiw});
        if (output_absorption_distance) buffers.push_back({&dist_abss[i_esc], n_esc, MPI_DOUBLE, i, TAG_EAD});
        if (output_source_position) buffers.push_back({&source_positions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_EP0});
        buffers.push_back({&positions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_EP});
        buffers.push_back({&directions[i_esc], 3*n_esc, MPI_DOUBLE, i, TAG_ED});
      }

      // Receive absorbed photon info
      const int i_abs = ind_abs[i];
      const int n_abs = ind_abs[i+1] - ind_abs[i];
      if (n_abs > 0) {
        buffers.push_back({&source_ids[i_abs], n_abs, MPI_INT, i, TAG_AI0});
        if (n_ion_source_types > 1) buffers.push_back({&source_types[i_abs], n_abs, MPI_INT, i, TAG_AT0});
        buffers.push_back({&source_weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_AW0});
        if (output_n_scat) buffers.push_back({&n_scats[i_abs], n_abs, MPI_INT, i, TAG_ANS});
        buffers.push_back({&freq_bins[i_abs], n_abs, MPI_INT, i, TAG_AB});
        buffers.push_back({&weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_AW});
        buffers.push_back({&dust_weights[i_abs], n_abs, MPI_DOUBLE, i, TAG_ADW});
        for (int aiw = 0; aiw < n_active_ion_weights; ++aiw)
          buffers.push_back({&ion_weights[aiw][i_abs], n_abs, MPI_DOUBLE, i, TAG_AIW + aiw});
        if (output_absorption_distance) buffers.push_back({&dist_abss[i_abs], n_abs, MPI_DOUBLE, i, TAG_AAD});
        if (output_source_position) buffers.push_back({&source_positions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AP0});
        buffers.push_back({&positions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AP});
        buffers.push_back({&directions[i_abs], 3*n_abs, MPI_DOUBLE, i, TAG_AD});
      }
    }

    // Receive data from all buffers
    const int n_buffers = buffers.size();    // Number of buffers
    #pragma omp parallel for
    for (int i = 0; i < n_buffers; ++i)
      mpi_recv(buffers[i]);                  // Helper to receive data

    // Update global photon counts
    n_escaped = ind_esc[n_ranks] - ind_esc[0];
    n_absorbed = ind_abs[n_ranks] - ind_abs[0];
  } else {
    // Send escaped photon info
    if (n_escaped > 0) {
      MPI_Send(source_ids.data(), n_escaped, MPI_INT, ROOT, TAG_EI0, MPI_COMM_WORLD);
      if (n_ion_source_types > 1) MPI_Send(source_types.data(), n_escaped, MPI_INT, ROOT, TAG_ET0, MPI_COMM_WORLD);
      MPI_Send(source_weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EW0, MPI_COMM_WORLD);
      if (output_n_scat) MPI_Send(n_scats.data(), n_escaped, MPI_INT, ROOT, TAG_ENS, MPI_COMM_WORLD);
      MPI_Send(freq_bins.data(), n_escaped, MPI_INT, ROOT, TAG_EB, MPI_COMM_WORLD);
      MPI_Send(weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EW, MPI_COMM_WORLD);
      MPI_Send(dust_weights.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EDW, MPI_COMM_WORLD);
      for (int aiw = 0; aiw < n_active_ion_weights; ++aiw)
        MPI_Send(ion_weights[aiw].data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EIW + aiw, MPI_COMM_WORLD);
      if (output_absorption_distance) MPI_Send(dist_abss.data(), n_escaped, MPI_DOUBLE, ROOT, TAG_EAD, MPI_COMM_WORLD);
      if (output_source_position) MPI_Send(source_positions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_EP0, MPI_COMM_WORLD);
      MPI_Send(positions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_EP, MPI_COMM_WORLD);
      MPI_Send(directions.data(), 3*n_escaped, MPI_DOUBLE, ROOT, TAG_ED, MPI_COMM_WORLD);
    }

    // Send absorbed photon info
    if (n_absorbed > 0) {
      MPI_Send(&source_ids[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AI0, MPI_COMM_WORLD);
      if (n_ion_source_types > 1) MPI_Send(&source_types[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AT0, MPI_COMM_WORLD);
      MPI_Send(&source_weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AW0, MPI_COMM_WORLD);
      if (output_n_scat) MPI_Send(&n_scats[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_ANS, MPI_COMM_WORLD);
      MPI_Send(&freq_bins[n_escaped], n_absorbed, MPI_INT, ROOT, TAG_AB, MPI_COMM_WORLD);
      MPI_Send(&weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AW, MPI_COMM_WORLD);
      MPI_Send(&dust_weights[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_ADW, MPI_COMM_WORLD);
      for (int aiw = 0; aiw < n_active_ion_weights; ++aiw)
        MPI_Send(&ion_weights[aiw][n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AIW + aiw, MPI_COMM_WORLD);
      if (output_absorption_distance) MPI_Send(&dist_abss[n_escaped], n_absorbed, MPI_DOUBLE, ROOT, TAG_AAD, MPI_COMM_WORLD);
      if (output_source_position) MPI_Send(&source_positions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AP0, MPI_COMM_WORLD);
      MPI_Send(&positions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AP, MPI_COMM_WORLD);
      MPI_Send(&directions[n_escaped], 3*n_absorbed, MPI_DOUBLE, ROOT, TAG_AD, MPI_COMM_WORLD);
    }
  }
}

/* MPI reductions of all output data. */
static void reduce_ionization() {
  if (root)
    cout << "\nIonization: MPI Reductions ..." << endl;

  // If we do not synchronize all ranks, the reductions may time out
  MPI_Barrier(MPI_COMM_WORLD);

  reduce_timer.start();

  // Global statistics
  mpi_reduce(bin_f_src);                     // Global bin source fraction: sum(w0)
  mpi_reduce(bin_f2_src);                    // Global bin squared f_src: sum(w0^2)
  mpi_reduce(bin_f_esc);                     // Global bin escape fraction: sum(w)
  mpi_reduce(bin_f2_esc);                    // Global bin squared f_esc: sum(w^2)
  mpi_reduce(bin_f_abs);                     // Global bin dust absorption fraction
  mpi_reduce(bin_f2_abs);                    // Global bin squared f_abs: sum(w^2)
  mpi_reduce(bin_f_ions);                    // Global bin species absorption fractions
  mpi_reduce(bin_f2_HI);                     // Global bin squared absorption fractions
  mpi_reduce(bin_l_abs);                     // Global bin absorption distance [cm]

  // Angle-averaged radial surface brightness [photons/s/cm^2]
  if (output_radial_avg)
    mpi_reduce(radial_avg);                  // Apply radial_avg reductions

  // Angle-averaged bin radial surface brightness [photons/s/cm^2]
  if (output_radial_cube_avg)
    mpi_reduce(radial_cube_avg);             // Apply radial_cube_avg reductions

  // Group statistics
  if (n_groups > 0) {
    mpi_reduce(bin_f_src_grp);               // Group bin source fraction: sum(w0)
    mpi_reduce(bin_f2_src_grp);              // Group bin squared f_src: sum(w0^2)
    if (output_grp_obs) {
      mpi_reduce(bin_f_esc_grp);             // Group bin escape fraction: sum(w)
      mpi_reduce(bin_f2_esc_grp);            // Group bin squared f_esc: sum(w^2)
      mpi_reduce(bin_f_abs_grp);             // Group bin dust absorption fraction
      mpi_reduce(bin_f2_abs_grp);            // Group bin squared f_abs: sum(w^2)
    }
    if (output_grp_vir) {
      mpi_reduce(bin_f_src_grp_vir);         // Group bin source fraction: sum(w0)
      mpi_reduce(bin_f2_src_grp_vir);        // Group bin squared f_src: sum(w0^2)
      mpi_reduce(bin_f_esc_grp_vir);         // Group bin escape fraction: sum(w)
      mpi_reduce(bin_f2_esc_grp_vir);        // Group bin squared f_esc: sum(w^2)
      mpi_reduce(bin_f_abs_grp_vir);         // Group bin dust absorption fraction
      mpi_reduce(bin_f2_abs_grp_vir);        // Group bin squared f_abs: sum(w^2)
    }
    if (output_grp_gal) {
      mpi_reduce(bin_f_src_grp_gal);         // Group bin source fraction: sum(w0)
      mpi_reduce(bin_f2_src_grp_gal);        // Group bin squared f_src: sum(w0^2)
      mpi_reduce(bin_f_esc_grp_gal);         // Group bin escape fraction: sum(w)
      mpi_reduce(bin_f2_esc_grp_gal);        // Group bin squared f_esc: sum(w^2)
      mpi_reduce(bin_f_abs_grp_gal);         // Group bin dust absorption fraction
      mpi_reduce(bin_f2_abs_grp_gal);        // Group bin squared f_abs: sum(w^2)
    }
  }
  if (n_ugroups > 0) {
    mpi_reduce(bin_f_src_ugrp);              // Unfiltered group bin source fraction: sum(w0)
    mpi_reduce(bin_f2_src_ugrp);             // Unfiltered group bin squared f_src: sum(w0^2)
  }

  // Subhalo statistics
  if (n_subhalos > 0) {
    mpi_reduce(bin_f_src_sub);               // Subhalo bin source fraction: sum(w0)
    mpi_reduce(bin_f2_src_sub);              // Subhalo bin squared f_src: sum(w0^2)
    if (output_sub_obs) {
      mpi_reduce(bin_f_esc_sub);             // Subhalo bin escape fraction: sum(w)
      mpi_reduce(bin_f2_esc_sub);            // Subhalo bin squared f_esc: sum(w^2)
      mpi_reduce(bin_f_abs_sub);             // Subhalo bin dust absorption fraction
      mpi_reduce(bin_f2_abs_sub);            // Subhalo bin squared f_abs: sum(w^2)
    }
    if (output_sub_vir) {
      mpi_reduce(bin_f_src_sub_vir);         // Subhalo bin source fraction: sum(w0)
      mpi_reduce(bin_f2_src_sub_vir);        // Subhalo bin squared f_src: sum(w0^2)
      mpi_reduce(bin_f_esc_sub_vir);         // Subhalo bin escape fraction: sum(w)
      mpi_reduce(bin_f2_esc_sub_vir);        // Subhalo bin squared f_esc: sum(w^2)
      mpi_reduce(bin_f_abs_sub_vir);         // Subhalo bin dust absorption fraction
      mpi_reduce(bin_f2_abs_sub_vir);        // Subhalo bin squared f_abs: sum(w^2)
    }
    if (output_sub_gal) {
      mpi_reduce(bin_f_src_sub_gal);         // Subhalo bin source fraction: sum(w0)
      mpi_reduce(bin_f2_src_sub_gal);        // Subhalo bin squared f_src: sum(w0^2)
      mpi_reduce(bin_f_esc_sub_gal);         // Subhalo bin escape fraction: sum(w)
      mpi_reduce(bin_f2_esc_sub_gal);        // Subhalo bin squared f_esc: sum(w^2)
      mpi_reduce(bin_f_abs_sub_gal);         // Subhalo bin dust absorption fraction
      mpi_reduce(bin_f2_abs_sub_gal);        // Subhalo bin squared f_abs: sum(w^2)
    }
  }
  if (n_usubhalos > 0) {
    mpi_reduce(bin_f_src_usub);              // Unfiltered subhalo bin source fraction: sum(w0)
    mpi_reduce(bin_f2_src_usub);             // Unfiltered subhalo bin squared f_src: sum(w0^2)
  }

  // Escape fractions [fraction]
  if (output_escape_fractions)
    mpi_reduce(f_escs);                      // Apply f_esc reductions

  // Bin escape fractions [fraction]
  if (output_bin_escape_fractions)
    mpi_reduce(bin_f_escs);                  // Apply bin_f_esc reductions

  // Surface brightness images [photons/s/cm^2]
  if (output_images)
    mpi_reduce(images);                      // Apply image reductions

  // Bin SB images [photons/s/cm^2]
  if (output_cubes)
    mpi_reduce(bin_images);                  // Apply bin_image reductions

  // Radial surface brightness images [photons/s/cm^2]
  if (output_radial_images)
    mpi_reduce(radial_images);               // Apply radial image reductions

  // Bin radial SB images [photons/s/cm^2]
  if (output_radial_cubes)
    mpi_reduce(bin_radial_images);           // Apply bin_radial_image reductions

  // Line-of-sight healpix maps
  if (output_map) {
    mpi_reduce(map);                         // Apply map reductions

    if (output_map2)
      mpi_reduce(map2);                      // Apply map2 reductions
  }

  // Group healpix maps
  if (output_map_grp) {
    if (output_grp_obs)
      mpi_reduce(map_grp);                   // Apply map_grp reductions
    if (output_grp_vir)
      mpi_reduce(map_grp_vir);               // Apply map_grp_vir reductions
    if (output_grp_gal)
      mpi_reduce(map_grp_gal);               // Apply map_grp_gal reductions
    if (output_map2_grp) {
      if (output_grp_obs)
        mpi_reduce(map2_grp);                // Apply map2_grp reductions
      if (output_grp_vir)
        mpi_reduce(map2_grp_vir);            // Apply map2_grp_vir reductions
      if (output_grp_gal)
        mpi_reduce(map2_grp_gal);            // Apply map2_grp_gal reductions
    }
  }

  // Subhalo healpix maps
  if (output_map_sub) {
    if (output_sub_obs)
      mpi_reduce(map_sub);                   // Apply map_sub reductions
    if (output_sub_vir)
      mpi_reduce(map_sub_vir);               // Apply map_sub_vir reductions
    if (output_sub_gal)
      mpi_reduce(map_sub_gal);               // Apply map_sub_gal reductions
    if (output_map2_sub) {
      if (output_sub_obs)
        mpi_reduce(map2_sub);                // Apply map2_sub reductions
      if (output_sub_vir)
        mpi_reduce(map2_sub_vir);            // Apply map2_sub_vir reductions
      if (output_sub_gal)
        mpi_reduce(map2_sub_gal);            // Apply map2_sub_gal reductions
    }
  }

  // Line-of-sight healpix radial map
  if (output_radial_map)
    mpi_reduce(radial_map);                  // Apply radial_map reductions

  // Line-of-sight healpix radial cube map
  if (output_cube_map)
    mpi_reduce(cube_map);                    // Apply cube_map reductions

  // Group healpix radial map
  if (output_radial_map_grp)
    mpi_reduce(radial_map_grp);              // Apply radial_map_grp reductions

  // Subhalo healpix radial map
  if (output_radial_map_sub)
    mpi_reduce(radial_map_sub);              // Apply radial_map_sub reductions

  // Group angle-averaged radial profiles
  if (output_radial_avg_grp)
    mpi_reduce(radial_avg_grp);              // Apply radial_avg_grp reductions

  // Subhalo angle-averaged radial profiles
  if (output_radial_avg_sub)
    mpi_reduce(radial_avg_sub);              // Apply radial_avg_sub reductions

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      mpi_reduce(images_int);                // Apply image reductions

    // Bin SB images [photons/s/cm^2]
    if (output_cubes)
      mpi_reduce(bin_images_int);            // Apply bin_image reductions

    // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_images)
      mpi_reduce(radial_images_int);         // Apply radial image reductions

    // Bin radial SB images [photons/s/cm^2]
    if (output_radial_cubes)
      mpi_reduce(bin_radial_images_int);     // Apply bin_radial_image reductions
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Escape fractions [fraction]
    if (output_escape_fractions)
      mpi_reduce(f_escs_ext);                // Apply f_esc reductions

    // Bin escape fractions [fraction]
    if (output_bin_escape_fractions)
      mpi_reduce(bin_f_escs_ext);            // Apply bin_f_esc reductions

    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      mpi_reduce(images_ext);                // Apply image reductions

    // Bin SB images [photons/s/cm^2]
    if (output_cubes)
      mpi_reduce(bin_images_ext);            // Apply bin_image reductions

    // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_images)
      mpi_reduce(radial_images_ext);         // Apply radial image reductions

    // Bin radial SB images [photons/s/cm^2]
    if (output_radial_cubes)
      mpi_reduce(bin_radial_images_ext);     // Apply bin_radial_image reductions
  }

  // Absorbed and escaped photon info
  if (output_photons)
    mpi_collect_photons();                   // Collect photons to root

  // Photoionization integrals [1/s]
  for (auto& rate_ion : rate_ions)
    mpi_reduce(rate_ion);                    // All active ions

  // Photoheating rates [erg/s]
  if (output_photoheating)
    mpi_reduce(G_ion);

  // Photon density [photons/cm^3]
  if (output_photon_density)
    mpi_reduce(bin_n_gamma);

  // Number of absorptions by cell, age, and frequency [photons/s]
  if constexpr (output_ion_cell_age_freq)
    for (auto& stat : ion_cell_age_freq.stats)
      mpi_reduce(stat);

  // Number of absorptions by radial, age, and frequency [photons/s]
  if constexpr (output_ion_radial_age_freq)
    for (auto& stat : ion_radial_age_freq.stats)
      mpi_reduce(stat);

  // Number of absorptions by distance, age, and frequency [photons/s]
  if constexpr (output_ion_distance_age_freq)
    for (auto& stat : ion_distance_age_freq.stats)
      mpi_reduce(stat);

  // Radial flow statistics [photons/s]
  if constexpr (output_radial_flow) {
    mpi_reduce(radial_flow.src);
    mpi_reduce(radial_flow.esc);
    for (auto& stat : radial_flow.stats)
      mpi_reduce(stat);
    mpi_reduce(radial_flow.pass);
    mpi_reduce(radial_flow.flow);
  }

  // Group flow statistics [photons/s]
  if constexpr (output_group_flows)
    for (auto& group_flow : group_flows) {
      mpi_reduce(group_flow.src);
      mpi_reduce(group_flow.esc);
      for (auto& stat : group_flow.stats)
        mpi_reduce(stat);
      mpi_reduce(group_flow.pass);
      mpi_reduce(group_flow.flow);
    }

  // Subhalo flow statistics [photons/s]
  if constexpr (output_subhalo_flows)
    for (auto& subhalo_flow : subhalo_flows) {
      mpi_reduce(subhalo_flow.src);
      mpi_reduce(subhalo_flow.esc);
      for (auto& stat : subhalo_flow.stats)
        mpi_reduce(stat);
      mpi_reduce(subhalo_flow.pass);
      mpi_reduce(subhalo_flow.flow);
    }

  reduce_timer.stop();
}

/* Convert observables to the correct units. */
void Ionization::correct_units() {
  cout << "\nIonization: Converting to physical units ..." << endl;

  // Escape fractions [fraction]
  f_src = omp_sum(bin_f_src);                // Global source fraction: sum(w0)
  f2_src = omp_sum(bin_f2_src);              // Global squared f_src: sum(w0^2)
  n_photons_src = calc_n_eff(f_src, f2_src); // Effective number of emitted photons: 1/<w0>
  f_esc = omp_sum(bin_f_esc);                // Global escape fraction: sum(w)
  f2_esc = omp_sum(bin_f2_esc);              // Global squared f_esc: sum(w^2)
  n_photons_esc = calc_n_eff(f_esc, f2_esc); // Effective number of escaped photons: 1/<w0>
  f_abs = omp_sum(bin_f_abs);                // Global dust absorption fraction
  f2_abs = omp_sum(bin_f2_abs);              // Global squared f_abs: sum(w^2)
  n_photons_abs = calc_n_eff(f_abs, f2_abs); // Effective number of absorbed photons: 1/<w0>
  for (int ai = 0; ai < n_active_ions; ++ai)
    f_ions[ai] = omp_sum(bin_f_ions[ai]);    // Global species absorption fractions
  f2_HI = omp_sum(bin_f2_HI);                // Global squared absorption fractions
  n_photons_HI = calc_n_eff(f_ions[HI_ION], f2_HI); // Effective number of absorbed photons: 1/<w0>
  l_abs = omp_sum(bin_l_abs);                // Global absorption distance [cm]
  f_src_HI = f_esc_HI = f_abs_HI = 0.;       // Stats for (> 13.6 eV)
  f2_src_HI = f2_esc_HI = f2_abs_HI = 0.;    // Squared stats for (> 13.6 eV)
  #pragma omp parallel for reduction(+:f_src_HI, f2_src_HI, f_esc_HI, f2_esc_HI, f_abs_HI, f2_abs_HI)
  for (int i = HI_bin; i < n_bins; ++i) {
    f_src_HI += bin_f_src[i];                // Source fraction
    f2_src_HI += bin_f2_src[i];              // Squared f_src
    f_esc_HI += bin_f_esc[i];                // Escape fraction
    f2_esc_HI += bin_f2_esc[i];              // Squared f_esc
    f_abs_HI += bin_f_abs[i];                // Dust absorption fraction
    f2_abs_HI += bin_f2_abs[i];              // Squared f_abs
  }
  // Renormalization of > 13.6 eV stats
  {
    const double HI_factor = safe_division(ion_global.Ndot, ion_global.HI.Ndot);
    const double HI_factor_2 = HI_factor * HI_factor;
    f_src_HI *= HI_factor;                   // Global HI source fraction
    f2_src_HI *= HI_factor_2;                // Global squared f_src_HI
    n_photons_src_HI = calc_n_eff(f_src_HI, f2_src_HI); // Emitted HI photons
    f_esc_HI *= HI_factor;                   // Global HI escape fraction
    f2_esc_HI *= HI_factor_2;                // Global squared f_esc_HI
    n_photons_esc_HI = calc_n_eff(f_esc_HI, f2_esc_HI); // Escaped HI photons
    f_abs_HI *= HI_factor;                   // Global HI dust absorption
    f2_abs_HI *= HI_factor_2;                // Global squared f_abs_HI
    n_photons_abs_HI = calc_n_eff(f_abs_HI, f2_abs_HI); // Absorbed HI photons
  }
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    const double bin_Ndot = bin_Ndot_tot[i]; // Bin Ndot
    const double bin_factor = safe_division(ion_global.Ndot, bin_Ndot);
    const double bin_factor_2 = bin_factor * bin_factor;
    bin_f_src[i] *= bin_factor;              // Global bin source fraction
    bin_f2_src[i] *= bin_factor_2;           // Global bin squared f_src
    bin_n_photons_src[i] = calc_n_eff(bin_f_src[i], bin_f2_src[i]); // Emitted: <f_src>^2 / <f_src^2>
    bin_f_esc[i] *= bin_factor;              // Global bin escape fraction
    bin_f2_esc[i] *= bin_factor_2;           // Global bin squared f_esc
    bin_n_photons_esc[i] = calc_n_eff(bin_f_esc[i], bin_f2_esc[i]); // Escaped: <f_esc>^2 / <f_esc^2>
    bin_f_abs[i] *= bin_factor;              // Global bin dust absorption fraction
    bin_f2_abs[i] *= bin_factor_2;           // Global bin squared f_abs
    bin_n_photons_abs[i] = calc_n_eff(bin_f_abs[i], bin_f2_abs[i]); // Absorbed: <f_abs>^2 / <f_abs^2>
    for (auto& bin_f_ion : bin_f_ions)
      bin_f_ion[i] *= bin_factor;            // Global bin species absorption fractions
    bin_f2_HI[i] *= bin_factor_2;            // Global bin squared absorption fractions
    bin_n_photons_HI[i] = calc_n_eff(bin_f_ions[HI_ION][i], bin_f2_HI[i]); // <f_HI>^2 / <f_HI^2>
    bin_l_abs[i] *= bin_factor;              // Global bin absorption distance [cm]
  }
  if (output_escape_fractions)
    rescale(f_escs, 2.);                     // Apply phase function correction

  // Group statistics (source)
  if constexpr (output_groups) {
    for (int i = 0; i < n_groups; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_grp[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_grp[i] = halo_factor * omp_sum(bin_f_src_grp, i); // Group source fraction: sum(w0)
      f2_src_grp[i] = halo_factor_2 * omp_sum(bin_f2_src_grp, i); // Group squared f_src: sum(w0^2)
      n_photons_src_grp[i] = calc_n_eff(f_src_grp[i], f2_src_grp[i]); // Effective number of emitted photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf2_src_HI = 0.; // Stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_grp(i,ibin);  // Source fraction
        hf2_src_HI += bin_f2_src_grp(i,ibin); // Squared f_src
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_grp[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_grp[i] = halo_HI_factor * hf_src_HI; // Group HI source fraction
      f2_src_HI_grp[i] = halo_HI_factor_2 * hf2_src_HI; // Group squared f_src_HI
      n_photons_src_HI_grp[i] = calc_n_eff(f_src_HI_grp[i], f2_src_HI_grp[i]); // Emitted HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_grp(i,ibin); // Group bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_grp(i,ibin) *= hbin_factor; // Group bin source fraction
        bin_f2_src_grp(i,ibin) *= hbin_factor_2; // Group bin squared f_src
        bin_n_photons_src_grp(i,ibin) = calc_n_eff(bin_f_src_grp(i,ibin), bin_f2_src_grp(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
      }
    }
    // Group statistics (observed)
    if (output_grp_obs) for (int i = 0; i < n_groups; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_grp[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_esc_grp[i] = halo_factor * omp_sum(bin_f_esc_grp, i); // Group escape fraction: sum(w)
      f2_esc_grp[i] = halo_factor_2 * omp_sum(bin_f2_esc_grp, i); // Group squared f_esc: sum(w^2)
      n_photons_esc_grp[i] = calc_n_eff(f_esc_grp[i], f2_esc_grp[i]); // Effective number of escaped photons: 1/<w0>
      f_abs_grp[i] = halo_factor * omp_sum(bin_f_abs_grp, i); // Group dust absorption fraction
      f2_abs_grp[i] = halo_factor_2 * omp_sum(bin_f2_abs_grp, i); // Group squared f_abs: sum(w^2)
      n_photons_abs_grp[i] = calc_n_eff(f_abs_grp[i], f2_abs_grp[i]); // Effective number of absorbed photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_esc_HI = 0., hf2_esc_HI = 0., hf_abs_HI = 0., hf2_abs_HI = 0.; // Stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_esc_HI, hf2_esc_HI, hf_abs_HI, hf2_abs_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_esc_HI += bin_f_esc_grp(i,ibin);  // Escape fraction
        hf2_esc_HI += bin_f2_esc_grp(i,ibin); // Squared f_esc
        hf_abs_HI += bin_f_abs_grp(i,ibin);  // Dust absorption fraction
        hf2_abs_HI += bin_f2_abs_grp(i,ibin); // Squared f_abs
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_grp[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_esc_HI_grp[i] = halo_HI_factor * hf_esc_HI; // Group HI escape fraction
      f2_esc_HI_grp[i] = halo_HI_factor_2 * hf2_esc_HI; // Group squared f_esc_HI
      n_photons_esc_HI_grp[i] = calc_n_eff(f_esc_HI_grp[i], f2_esc_HI_grp[i]); // Escaped HI photons
      f_abs_HI_grp[i] = halo_HI_factor * hf_abs_HI; // Group HI dust absorption
      f2_abs_HI_grp[i] = halo_HI_factor_2 * hf2_abs_HI; // Group squared f_abs_HI
      n_photons_abs_HI_grp[i] = calc_n_eff(f_abs_HI_grp[i], f2_abs_HI_grp[i]); // Absorbed HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_grp(i,ibin); // Group bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_esc_grp(i,ibin) *= hbin_factor; // Group bin escape fraction
        bin_f2_esc_grp(i,ibin) *= hbin_factor_2; // Group bin squared f_esc
        bin_n_photons_esc_grp(i,ibin) = calc_n_eff(bin_f_esc_grp(i,ibin), bin_f2_esc_grp(i,ibin)); // Escaped: <f_esc>^2 / <f_esc^2>
        bin_f_abs_grp(i,ibin) *= hbin_factor; // Group bin dust absorption fraction
        bin_f2_abs_grp(i,ibin) *= hbin_factor_2; // Group bin squared f_abs
        bin_n_photons_abs_grp(i,ibin) = calc_n_eff(bin_f_abs_grp(i,ibin), bin_f2_abs_grp(i,ibin)); // Absorbed: <f_abs>^2 / <f_abs^2>
      }
    }
    // Group statistics (virial radius)
    if (output_grp_vir) for (int i = 0; i < n_groups; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_grp_vir[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_grp_vir[i] = halo_factor * omp_sum(bin_f_src_grp_vir, i); // Group source fraction: sum(w0)
      f2_src_grp_vir[i] = halo_factor_2 * omp_sum(bin_f2_src_grp_vir, i); // Group squared f_src: sum(w0^2)
      n_photons_src_grp_vir[i] = calc_n_eff(f_src_grp_vir[i], f2_src_grp_vir[i]); // Effective number of emitted photons: 1/<w0>
      f_esc_grp_vir[i] = halo_factor * omp_sum(bin_f_esc_grp_vir, i); // Group escape fraction: sum(w)
      f2_esc_grp_vir[i] = halo_factor_2 * omp_sum(bin_f2_esc_grp_vir, i); // Group squared f_esc: sum(w^2)
      n_photons_esc_grp_vir[i] = calc_n_eff(f_esc_grp_vir[i], f2_esc_grp_vir[i]); // Effective number of escaped photons: 1/<w0>
      f_abs_grp_vir[i] = halo_factor * omp_sum(bin_f_abs_grp_vir, i); // Group dust absorption fraction
      f2_abs_grp_vir[i] = halo_factor_2 * omp_sum(bin_f2_abs_grp_vir, i); // Group squared f_abs: sum(w^2)
      n_photons_abs_grp_vir[i] = calc_n_eff(f_abs_grp_vir[i], f2_abs_grp_vir[i]); // Effective number of absorbed photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf_esc_HI = 0., hf_abs_HI = 0.; // Stats for (> 13.6 eV)
      double hf2_src_HI = 0., hf2_esc_HI = 0., hf2_abs_HI = 0.; // Squared stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI, hf_esc_HI, hf2_esc_HI, hf_abs_HI, hf2_abs_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_grp_vir(i,ibin); // Source fraction
        hf2_src_HI += bin_f2_src_grp_vir(i,ibin); // Squared f_src
        hf_esc_HI += bin_f_esc_grp_vir(i,ibin); // Escape fraction
        hf2_esc_HI += bin_f2_esc_grp_vir(i,ibin); // Squared f_esc
        hf_abs_HI += bin_f_abs_grp_vir(i,ibin); // Dust absorption fraction
        hf2_abs_HI += bin_f2_abs_grp_vir(i,ibin); // Squared f_abs
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_grp_vir[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_grp_vir[i] = halo_HI_factor * hf_src_HI; // Group HI source fraction
      f2_src_HI_grp_vir[i] = halo_HI_factor_2 * hf2_src_HI; // Group squared f_src_HI
      n_photons_src_HI_grp_vir[i] = calc_n_eff(f_src_HI_grp_vir[i], f2_src_HI_grp_vir[i]); // Emitted HI photons
      f_esc_HI_grp_vir[i] = halo_HI_factor * hf_esc_HI; // Group HI escape fraction
      f2_esc_HI_grp_vir[i] = halo_HI_factor_2 * hf2_esc_HI; // Group squared f_esc_HI
      n_photons_esc_HI_grp_vir[i] = calc_n_eff(f_esc_HI_grp_vir[i], f2_esc_HI_grp_vir[i]); // Escaped HI photons
      f_abs_HI_grp_vir[i] = halo_HI_factor * hf_abs_HI; // Group HI dust absorption
      f2_abs_HI_grp_vir[i] = halo_HI_factor_2 * hf2_abs_HI; // Group squared f_abs_HI
      n_photons_abs_HI_grp_vir[i] = calc_n_eff(f_abs_HI_grp_vir[i], f2_abs_HI_grp_vir[i]); // Absorbed HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_grp_vir(i,ibin); // Group bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_grp_vir(i,ibin) *= hbin_factor;  // Group bin source fraction
        bin_f2_src_grp_vir(i,ibin) *= hbin_factor_2; // Group bin squared f_src
        bin_n_photons_src_grp_vir(i,ibin) = calc_n_eff(bin_f_src_grp_vir(i,ibin), bin_f2_src_grp_vir(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
        bin_f_esc_grp_vir(i,ibin) *= hbin_factor;  // Group bin escape fraction
        bin_f2_esc_grp_vir(i,ibin) *= hbin_factor_2; // Group bin squared f_esc
        bin_n_photons_esc_grp_vir(i,ibin) = calc_n_eff(bin_f_esc_grp_vir(i,ibin), bin_f2_esc_grp_vir(i,ibin)); // Escaped: <f_esc>^2 / <f_esc^2>
        bin_f_abs_grp_vir(i,ibin) *= hbin_factor;  // Group bin dust absorption fraction
        bin_f2_abs_grp_vir(i,ibin) *= hbin_factor_2; // Group bin squared f_abs
        bin_n_photons_abs_grp_vir(i,ibin) = calc_n_eff(bin_f_abs_grp_vir(i,ibin), bin_f2_abs_grp_vir(i,ibin)); // Absorbed: <f_abs>^2 / <f_abs^2>
      }
    }
    // Group escape statistics (galaxy radius)
    if (output_grp_gal) for (int i = 0; i < n_groups; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_grp_gal[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_grp_gal[i] = halo_factor * omp_sum(bin_f_src_grp_gal, i); // Group source fraction: sum(w0)
      f2_src_grp_gal[i] = halo_factor_2 * omp_sum(bin_f2_src_grp_gal, i); // Group squared f_src: sum(w0^2)
      n_photons_src_grp_gal[i] = calc_n_eff(f_src_grp_gal[i], f2_src_grp_gal[i]); // Effective number of emitted photons: 1/<w0>
      f_esc_grp_gal[i] = halo_factor * omp_sum(bin_f_esc_grp_gal, i); // Group escape fraction: sum(w)
      f2_esc_grp_gal[i] = halo_factor_2 * omp_sum(bin_f2_esc_grp_gal, i); // Group squared f_esc: sum(w^2)
      n_photons_esc_grp_gal[i] = calc_n_eff(f_esc_grp_gal[i], f2_esc_grp_gal[i]); // Effective number of escaped photons: 1/<w0>
      f_abs_grp_gal[i] = halo_factor * omp_sum(bin_f_abs_grp_gal, i); // Group dust absorption fraction
      f2_abs_grp_gal[i] = halo_factor_2 * omp_sum(bin_f2_abs_grp_gal, i); // Group squared f_abs: sum(w^2)
      n_photons_abs_grp_gal[i] = calc_n_eff(f_abs_grp_gal[i], f2_abs_grp_gal[i]); // Effective number of absorbed photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf_esc_HI = 0., hf_abs_HI = 0.; // Stats for (> 13.6 eV)
      double hf2_src_HI = 0., hf2_esc_HI = 0., hf2_abs_HI = 0.; // Squared stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI, hf_esc_HI, hf2_esc_HI, hf_abs_HI, hf2_abs_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_grp_gal(i,ibin); // Source fraction
        hf2_src_HI += bin_f2_src_grp_gal(i,ibin); // Squared f_src
        hf_esc_HI += bin_f_esc_grp_gal(i,ibin); // Escape fraction
        hf2_esc_HI += bin_f2_esc_grp_gal(i,ibin); // Squared f_esc
        hf_abs_HI += bin_f_abs_grp_gal(i,ibin); // Dust absorption fraction
        hf2_abs_HI += bin_f2_abs_grp_gal(i,ibin); // Squared f_abs
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_grp_gal[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_grp_gal[i] = halo_HI_factor * hf_src_HI; // Group HI source fraction
      f2_src_HI_grp_gal[i] = halo_HI_factor_2 * hf2_src_HI; // Group squared f_src_HI
      n_photons_src_HI_grp_gal[i] = calc_n_eff(f_src_HI_grp_gal[i], f2_src_HI_grp_gal[i]); // Emitted HI photons
      f_esc_HI_grp_gal[i] = halo_HI_factor * hf_esc_HI; // Group HI escape fraction
      f2_esc_HI_grp_gal[i] = halo_HI_factor_2 * hf2_esc_HI; // Group squared f_esc_HI
      n_photons_esc_HI_grp_gal[i] = calc_n_eff(f_esc_HI_grp_gal[i], f2_esc_HI_grp_gal[i]); // Escaped HI photons
      f_abs_HI_grp_gal[i] = halo_HI_factor * hf_abs_HI; // Group HI dust absorption
      f2_abs_HI_grp_gal[i] = halo_HI_factor_2 * hf2_abs_HI; // Group squared f_abs_HI
      n_photons_abs_HI_grp_gal[i] = calc_n_eff(f_abs_HI_grp_gal[i], f2_abs_HI_grp_gal[i]); // Absorbed HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_grp_gal(i,ibin); // Group bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_grp_gal(i,ibin) *= hbin_factor;  // Group bin source fraction
        bin_f2_src_grp_gal(i,ibin) *= hbin_factor_2; // Group bin squared f_src
        bin_n_photons_src_grp_gal(i,ibin) = calc_n_eff(bin_f_src_grp_gal(i,ibin), bin_f2_src_grp_gal(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
        bin_f_esc_grp_gal(i,ibin) *= hbin_factor;  // Group bin escape fraction
        bin_f2_esc_grp_gal(i,ibin) *= hbin_factor_2; // Group bin squared f_esc
        bin_n_photons_esc_grp_gal(i,ibin) = calc_n_eff(bin_f_esc_grp_gal(i,ibin), bin_f2_esc_grp_gal(i,ibin)); // Escaped: <f_esc>^2 / <f_esc^2>
        bin_f_abs_grp_gal(i,ibin) *= hbin_factor;  // Group bin dust absorption fraction
        bin_f2_abs_grp_gal(i,ibin) *= hbin_factor_2; // Group bin squared f_abs
        bin_n_photons_abs_grp_gal(i,ibin) = calc_n_eff(bin_f_abs_grp_gal(i,ibin), bin_f2_abs_grp_gal(i,ibin)); // Absorbed: <f_abs>^2 / <f_abs^2>
      }
    }
    // Unfiltered group statistics
    for (int i = 0; i < n_ugroups; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_ugrp[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_ugrp[i] = halo_factor * omp_sum(bin_f_src_ugrp, i); // Unfiltered group source fraction: sum(w0)
      f2_src_ugrp[i] = halo_factor_2 * omp_sum(bin_f2_src_ugrp, i); // Unfiltered group squared f_src: sum(w0^2)
      n_photons_src_ugrp[i] = calc_n_eff(f_src_ugrp[i], f2_src_ugrp[i]); // Effective number of emitted photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf2_src_HI = 0.; // Stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_ugrp(i,ibin); // Source fraction
        hf2_src_HI += bin_f2_src_ugrp(i,ibin); // Squared f_src
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_ugrp[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_ugrp[i] = halo_HI_factor * hf_src_HI; // Unfiltered group HI source fraction
      f2_src_HI_ugrp[i] = halo_HI_factor_2 * hf2_src_HI; // Unfiltered group squared f_src_HI
      n_photons_src_HI_ugrp[i] = calc_n_eff(f_src_HI_ugrp[i], f2_src_HI_ugrp[i]); // Emitted HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_ugrp(i,ibin); // Unfiltered group bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_ugrp(i,ibin) *= hbin_factor; // Unfiltered group bin source fraction
        bin_f2_src_ugrp(i,ibin) *= hbin_factor_2; // Unfiltered group bin squared f_src
        bin_n_photons_src_ugrp(i,ibin) = calc_n_eff(bin_f_src_ugrp(i,ibin), bin_f2_src_ugrp(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
      }
    }
  }

  // Subhalo statistics (source)
  if constexpr (output_subhalos) {
    for (int i = 0; i < n_subhalos; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_sub[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_sub[i] = halo_factor * omp_sum(bin_f_src_sub, i); // Subhalo source fraction: sum(w0)
      f2_src_sub[i] = halo_factor_2 * omp_sum(bin_f2_src_sub, i); // Subhalo squared f_src: sum(w0^2)
      n_photons_src_sub[i] = calc_n_eff(f_src_sub[i], f2_src_sub[i]); // Effective number of emitted photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf2_src_HI = 0.; // Stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_sub(i,ibin);  // Source fraction
        hf2_src_HI += bin_f2_src_sub(i,ibin); // Squared f_src
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_sub[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_sub[i] = halo_HI_factor * hf_src_HI; // Subhalo HI source fraction
      f2_src_HI_sub[i] = halo_HI_factor_2 * hf2_src_HI; // Subhalo squared f_src_HI
      n_photons_src_HI_sub[i] = calc_n_eff(f_src_HI_sub[i], f2_src_HI_sub[i]); // Emitted HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_sub(i,ibin); // Subhalo bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_sub(i,ibin) *= hbin_factor; // Subhalo bin source fraction
        bin_f2_src_sub(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_src
        bin_n_photons_src_sub(i,ibin) = calc_n_eff(bin_f_src_sub(i,ibin), bin_f2_src_sub(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
      }
    }
    // Subhalo statistics (observed)
    if (output_sub_obs) for (int i = 0; i < n_subhalos; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_sub[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_esc_sub[i] = halo_factor * omp_sum(bin_f_esc_sub, i); // Subhalo escape fraction: sum(w)
      f2_esc_sub[i] = halo_factor_2 * omp_sum(bin_f2_esc_sub, i); // Subhalo squared f_esc: sum(w^2)
      n_photons_esc_sub[i] = calc_n_eff(f_esc_sub[i], f2_esc_sub[i]); // Effective number of escaped photons: 1/<w0>
      f_abs_sub[i] = halo_factor * omp_sum(bin_f_abs_sub, i); // Subhalo dust absorption fraction
      f2_abs_sub[i] = halo_factor_2 * omp_sum(bin_f2_abs_sub, i); // Subhalo squared f_abs: sum(w^2)
      n_photons_abs_sub[i] = calc_n_eff(f_abs_sub[i], f2_abs_sub[i]); // Effective number of absorbed photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_esc_HI = 0., hf2_esc_HI = 0., hf_abs_HI = 0., hf2_abs_HI = 0.; // Stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_esc_HI, hf2_esc_HI, hf_abs_HI, hf2_abs_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_esc_HI += bin_f_esc_sub(i,ibin);  // Escape fraction
        hf2_esc_HI += bin_f2_esc_sub(i,ibin); // Squared f_esc
        hf_abs_HI += bin_f_abs_sub(i,ibin);  // Dust absorption fraction
        hf2_abs_HI += bin_f2_abs_sub(i,ibin); // Squared f_abs
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_sub[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_esc_HI_sub[i] = halo_HI_factor * hf_esc_HI; // Subhalo HI escape fraction
      f2_esc_HI_sub[i] = halo_HI_factor_2 * hf2_esc_HI; // Subhalo squared f_esc_HI
      n_photons_esc_HI_sub[i] = calc_n_eff(f_esc_HI_sub[i], f2_esc_HI_sub[i]); // Escaped HI photons
      f_abs_HI_sub[i] = halo_HI_factor * hf_abs_HI; // Subhalo HI dust absorption
      f2_abs_HI_sub[i] = halo_HI_factor_2 * hf2_abs_HI; // Subhalo squared f_abs_HI
      n_photons_abs_HI_sub[i] = calc_n_eff(f_abs_HI_sub[i], f2_abs_HI_sub[i]); // Absorbed HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_sub(i,ibin); // Subhalo bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_esc_sub(i,ibin) *= hbin_factor; // Subhalo bin escape fraction
        bin_f2_esc_sub(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_esc
        bin_n_photons_esc_sub(i,ibin) = calc_n_eff(bin_f_esc_sub(i,ibin), bin_f2_esc_sub(i,ibin)); // Escaped: <f_esc>^2 / <f_esc^2>
        bin_f_abs_sub(i,ibin) *= hbin_factor; // Subhalo bin dust absorption fraction
        bin_f2_abs_sub(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_abs
        bin_n_photons_abs_sub(i,ibin) = calc_n_eff(bin_f_abs_sub(i,ibin), bin_f2_abs_sub(i,ibin)); // Absorbed: <f_abs>^2 / <f_abs^2>
      }
    }
    // Subhalo statistics (virial radius)
    if (output_sub_vir) for (int i = 0; i < n_subhalos; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_sub_vir[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_sub_vir[i] = halo_factor * omp_sum(bin_f_src_sub_vir, i); // Subhalo source fraction: sum(w0)
      f2_src_sub_vir[i] = halo_factor_2 * omp_sum(bin_f2_src_sub_vir, i); // Subhalo squared f_src: sum(w0^2)
      n_photons_src_sub_vir[i] = calc_n_eff(f_src_sub_vir[i], f2_src_sub_vir[i]); // Effective number of emitted photons: 1/<w0>
      f_esc_sub_vir[i] = halo_factor * omp_sum(bin_f_esc_sub_vir, i); // Subhalo escape fraction: sum(w)
      f2_esc_sub_vir[i] = halo_factor_2 * omp_sum(bin_f2_esc_sub_vir, i); // Subhalo squared f_esc: sum(w^2)
      n_photons_esc_sub_vir[i] = calc_n_eff(f_esc_sub_vir[i], f2_esc_sub_vir[i]); // Effective number of escaped photons: 1/<w0>
      f_abs_sub_vir[i] = halo_factor * omp_sum(bin_f_abs_sub_vir, i); // Subhalo dust absorption fraction
      f2_abs_sub_vir[i] = halo_factor_2 * omp_sum(bin_f2_abs_sub_vir, i); // Subhalo squared f_abs: sum(w^2)
      n_photons_abs_sub_vir[i] = calc_n_eff(f_abs_sub_vir[i], f2_abs_sub_vir[i]); // Effective number of absorbed photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf_esc_HI = 0., hf_abs_HI = 0.; // Stats for (> 13.6 eV)
      double hf2_src_HI = 0., hf2_esc_HI = 0., hf2_abs_HI = 0.; // Squared stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI, hf_esc_HI, hf2_esc_HI, hf_abs_HI, hf2_abs_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_sub_vir(i,ibin); // Source fraction
        hf2_src_HI += bin_f2_src_sub_vir(i,ibin); // Squared f_src
        hf_esc_HI += bin_f_esc_sub_vir(i,ibin); // Escape fraction
        hf2_esc_HI += bin_f2_esc_sub_vir(i,ibin); // Squared f_esc
        hf_abs_HI += bin_f_abs_sub_vir(i,ibin); // Dust absorption fraction
        hf2_abs_HI += bin_f2_abs_sub_vir(i,ibin); // Squared f_abs
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_sub_vir[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_sub_vir[i] = halo_HI_factor * hf_src_HI; // Subhalo HI source fraction
      f2_src_HI_sub_vir[i] = halo_HI_factor_2 * hf2_src_HI; // Subhalo squared f_src_HI
      n_photons_src_HI_sub_vir[i] = calc_n_eff(f_src_HI_sub_vir[i], f2_src_HI_sub_vir[i]); // Emitted HI photons
      f_esc_HI_sub_vir[i] = halo_HI_factor * hf_esc_HI; // Subhalo HI escape fraction
      f2_esc_HI_sub_vir[i] = halo_HI_factor_2 * hf2_esc_HI; // Subhalo squared f_esc_HI
      n_photons_esc_HI_sub_vir[i] = calc_n_eff(f_esc_HI_sub_vir[i], f2_esc_HI_sub_vir[i]); // Escaped HI photons
      f_abs_HI_sub_vir[i] = halo_HI_factor * hf_abs_HI; // Subhalo HI dust absorption
      f2_abs_HI_sub_vir[i] = halo_HI_factor_2 * hf2_abs_HI; // Subhalo squared f_abs_HI
      n_photons_abs_HI_sub_vir[i] = calc_n_eff(f_abs_HI_sub_vir[i], f2_abs_HI_sub_vir[i]); // Absorbed HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_sub_vir(i,ibin); // Subhalo bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_sub_vir(i,ibin) *= hbin_factor;  // Subhalo bin source fraction
        bin_f2_src_sub_vir(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_src
        bin_n_photons_src_sub_vir(i,ibin) = calc_n_eff(bin_f_src_sub_vir(i,ibin), bin_f2_src_sub_vir(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
        bin_f_esc_sub_vir(i,ibin) *= hbin_factor;  // Subhalo bin escape fraction
        bin_f2_esc_sub_vir(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_esc
        bin_n_photons_esc_sub_vir(i,ibin) = calc_n_eff(bin_f_esc_sub_vir(i,ibin), bin_f2_esc_sub_vir(i,ibin)); // Escaped: <f_esc>^2 / <f_esc^2>
        bin_f_abs_sub_vir(i,ibin) *= hbin_factor;  // Subhalo bin dust absorption fraction
        bin_f2_abs_sub_vir(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_abs
        bin_n_photons_abs_sub_vir(i,ibin) = calc_n_eff(bin_f_abs_sub_vir(i,ibin), bin_f2_abs_sub_vir(i,ibin)); // Absorbed: <f_abs>^2 / <f_abs^2>
      }
    }
    // Subhalo escape statistics (galaxy radius)
    if (output_sub_gal) for (int i = 0; i < n_subhalos; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_sub_gal[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_sub_gal[i] = halo_factor * omp_sum(bin_f_src_sub_gal, i); // Subhalo source fraction: sum(w0)
      f2_src_sub_gal[i] = halo_factor_2 * omp_sum(bin_f2_src_sub_gal, i); // Subhalo squared f_src: sum(w0^2)
      n_photons_src_sub_gal[i] = calc_n_eff(f_src_sub_gal[i], f2_src_sub_gal[i]); // Effective number of emitted photons: 1/<w0>
      f_esc_sub_gal[i] = halo_factor * omp_sum(bin_f_esc_sub_gal, i); // Subhalo escape fraction: sum(w)
      f2_esc_sub_gal[i] = halo_factor_2 * omp_sum(bin_f2_esc_sub_gal, i); // Subhalo squared f_esc: sum(w^2)
      n_photons_esc_sub_gal[i] = calc_n_eff(f_esc_sub_gal[i], f2_esc_sub_gal[i]); // Effective number of escaped photons: 1/<w0>
      f_abs_sub_gal[i] = halo_factor * omp_sum(bin_f_abs_sub_gal, i); // Subhalo dust absorption fraction
      f2_abs_sub_gal[i] = halo_factor_2 * omp_sum(bin_f2_abs_sub_gal, i); // Subhalo squared f_abs: sum(w^2)
      n_photons_abs_sub_gal[i] = calc_n_eff(f_abs_sub_gal[i], f2_abs_sub_gal[i]); // Effective number of absorbed photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf_esc_HI = 0., hf_abs_HI = 0.; // Stats for (> 13.6 eV)
      double hf2_src_HI = 0., hf2_esc_HI = 0., hf2_abs_HI = 0.; // Squared stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI, hf_esc_HI, hf2_esc_HI, hf_abs_HI, hf2_abs_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_sub_gal(i,ibin); // Source fraction
        hf2_src_HI += bin_f2_src_sub_gal(i,ibin); // Squared f_src
        hf_esc_HI += bin_f_esc_sub_gal(i,ibin); // Escape fraction
        hf2_esc_HI += bin_f2_esc_sub_gal(i,ibin); // Squared f_esc
        hf_abs_HI += bin_f_abs_sub_gal(i,ibin); // Dust absorption fraction
        hf2_abs_HI += bin_f2_abs_sub_gal(i,ibin); // Squared f_abs
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_sub_gal[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_sub_gal[i] = halo_HI_factor * hf_src_HI; // Subhalo HI source fraction
      f2_src_HI_sub_gal[i] = halo_HI_factor_2 * hf2_src_HI; // Subhalo squared f_src_HI
      n_photons_src_HI_sub_gal[i] = calc_n_eff(f_src_HI_sub_gal[i], f2_src_HI_sub_gal[i]); // Emitted HI photons
      f_esc_HI_sub_gal[i] = halo_HI_factor * hf_esc_HI; // Subhalo HI escape fraction
      f2_esc_HI_sub_gal[i] = halo_HI_factor_2 * hf2_esc_HI; // Subhalo squared f_esc_HI
      n_photons_esc_HI_sub_gal[i] = calc_n_eff(f_esc_HI_sub_gal[i], f2_esc_HI_sub_gal[i]); // Escaped HI photons
      f_abs_HI_sub_gal[i] = halo_HI_factor * hf_abs_HI; // Subhalo HI dust absorption
      f2_abs_HI_sub_gal[i] = halo_HI_factor_2 * hf2_abs_HI; // Subhalo squared f_abs_HI
      n_photons_abs_HI_sub_gal[i] = calc_n_eff(f_abs_HI_sub_gal[i], f2_abs_HI_sub_gal[i]); // Absorbed HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_sub_gal(i,ibin); // Subhalo bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_sub_gal(i,ibin) *= hbin_factor; // Subhalo bin source fraction
        bin_f2_src_sub_gal(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_src
        bin_n_photons_src_sub_gal(i,ibin) = calc_n_eff(bin_f_src_sub_gal(i,ibin), bin_f2_src_sub_gal(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
        bin_f_esc_sub_gal(i,ibin) *= hbin_factor; // Subhalo bin escape fraction
        bin_f2_esc_sub_gal(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_esc
        bin_n_photons_esc_sub_gal(i,ibin) = calc_n_eff(bin_f_esc_sub_gal(i,ibin), bin_f2_esc_sub_gal(i,ibin)); // Escaped: <f_esc>^2 / <f_esc^2>
        bin_f_abs_sub_gal(i,ibin) *= hbin_factor; // Subhalo bin dust absorption fraction
        bin_f2_abs_sub_gal(i,ibin) *= hbin_factor_2; // Subhalo bin squared f_abs
        bin_n_photons_abs_sub_gal(i,ibin) = calc_n_eff(bin_f_abs_sub_gal(i,ibin), bin_f2_abs_sub_gal(i,ibin)); // Absorbed: <f_abs>^2 / <f_abs^2>
      }
    }
    // Unfiltered subhalo statistics
    for (int i = 0; i < n_usubhalos; ++i) {
      const double halo_factor = safe_division(ion_global.Ndot, Ndot_usub[i]);
      const double halo_factor_2 = halo_factor * halo_factor;
      f_src_usub[i] = halo_factor * omp_sum(bin_f_src_usub, i); // Unfiltered subhalo source fraction: sum(w0)
      f2_src_usub[i] = halo_factor_2 * omp_sum(bin_f2_src_usub, i); // Unfiltered subhalo squared f_src: sum(w0^2)
      n_photons_src_usub[i] = calc_n_eff(f_src_usub[i], f2_src_usub[i]); // Effective number of emitted photons: 1/<w0>
      // Renormalization of > 13.6 eV stats
      double hf_src_HI = 0., hf2_src_HI = 0.; // Stats for (> 13.6 eV)
      #pragma omp parallel for reduction(+:hf_src_HI, hf2_src_HI)
      for (int ibin = HI_bin; ibin < n_bins; ++ibin) {
        hf_src_HI += bin_f_src_usub(i,ibin); // Source fraction
        hf2_src_HI += bin_f2_src_usub(i,ibin); // Squared f_src
      }
      const double halo_HI_factor = safe_division(ion_global.Ndot, Ndot_HI_usub[i]);
      const double halo_HI_factor_2 = halo_HI_factor * halo_HI_factor;
      f_src_HI_usub[i] = halo_HI_factor * hf_src_HI; // Unfiltered subhalo HI source fraction
      f2_src_HI_usub[i] = halo_HI_factor_2 * hf2_src_HI; // Unfiltered subhalo squared f_src_HI
      n_photons_src_HI_usub[i] = calc_n_eff(f_src_HI_usub[i], f2_src_HI_usub[i]); // Emitted HI photons
      #pragma omp parallel for
      for (int ibin = 0; ibin < n_bins; ++ibin) {
        const double hbin_Ndot = bin_Ndot_usub(i,ibin); // Unfiltered subhalo bin Ndot
        const double hbin_factor = safe_division(ion_global.Ndot, hbin_Ndot);
        const double hbin_factor_2 = hbin_factor * hbin_factor;
        bin_f_src_usub(i,ibin) *= hbin_factor; // Unfiltered subhalo bin source fraction
        bin_f2_src_usub(i,ibin) *= hbin_factor_2; // Unfiltered subhalo bin squared f_src
        bin_n_photons_src_usub(i,ibin) = calc_n_eff(bin_f_src_usub(i,ibin), bin_f2_src_usub(i,ibin)); // Emitted: <f_src>^2 / <f_src^2>
      }
    }
  }

  // Bin escape fractions [fraction]
  if (output_bin_escape_fractions) {
    #pragma omp parallel for
    for (int camera = 0; camera < n_cameras; ++camera)
      for (int i = 0; i < n_bins; ++i)
        bin_f_escs[camera][i] *= 2. * safe_division(ion_global.Ndot, bin_Ndot_tot[i]);
  }

  // Surface brightness images [photons/s/cm^2]
  const double to_image_units = 2. * ion_global.Ndot / pixel_area; // 2 Ndot / dA
  const double to_cube_units = 2. * ion_global.Ndot / cube_pixel_area; // 2 Ndot / dA
  if (output_images)
    rescale(images, to_image_units);         // Apply image correction

  // Bin SB images [photons/s/cm^2]
  if (output_cubes)
    rescale(bin_images, to_cube_units);      // Apply bin_image correction

  // Set up radial image units
  vector<double> to_radial_image_units, to_radial_cube_units; // 2 Ndot / dA
  vector<double> to_radial_map_units, to_cube_map_units, to_radial_map_grp_units, to_radial_map_sub_units; // n_pix Ndot / dA
  if (output_radial_images || output_radial_avg) {
    to_radial_image_units = vector<double>(n_radial_pixels);
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_pixels; ++ir)
      to_radial_image_units[ir] = 2. * ion_global.Ndot / radial_pixel_areas[ir];
  }
  if (output_radial_cubes || output_radial_cube_avg) {
    to_radial_cube_units = vector<double>(n_radial_cube_pixels);
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_cube_pixels; ++ir)
      to_radial_cube_units[ir] = 2. * ion_global.Ndot / radial_cube_pixel_areas[ir];
  }
  if (output_radial_map) {
    to_radial_map_units = vector<double>(n_map_pixels);
    const double n_pix_radial = 12 * n_side_radial * n_side_radial; // Number of pixels
    const double to_radial_map_units_area = n_pix_radial * ion_global.Ndot;
    #pragma omp parallel for
    for (int ir = 0; ir < n_map_pixels; ++ir)
      to_radial_map_units[ir] = to_radial_map_units_area / map_pixel_areas[ir];
  }
  if (output_cube_map) {
    to_cube_map_units = vector<double>(n_cube_map_pixels);
    const double n_pix_cube = 12 * n_side_cube * n_side_cube; // Number of pixels
    const double to_cube_map_units_area = n_pix_cube * ion_global.Ndot;
    #pragma omp parallel for
    for (int ir = 0; ir < n_cube_map_pixels; ++ir)
      to_cube_map_units[ir] = to_cube_map_units_area / cube_map_pixel_areas[ir];
  }
  if (output_radial_map_grp) {
    to_radial_map_grp_units = vector<double>(n_map_pixels_grp);
    const double n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // Number of pixels
    const double to_radial_map_grp_units_area = n_pix_radial_grp * ion_global.Ndot;
    #pragma omp parallel for
    for (int ir = 0; ir < n_map_pixels_grp; ++ir)
      to_radial_map_grp_units[ir] = to_radial_map_grp_units_area / map_pixel_areas_grp[ir];
  }
  if (output_radial_map_sub) {
    to_radial_map_sub_units = vector<double>(n_map_pixels_sub);
    const double n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // Number of pixels
    const double to_radial_map_sub_units_area = n_pix_radial_sub * ion_global.Ndot;
    #pragma omp parallel for
    for (int ir = 0; ir < n_map_pixels_sub; ++ir)
      to_radial_map_sub_units[ir] = to_radial_map_sub_units_area / map_pixel_areas_sub[ir];
  }
  if (output_radial_avg_grp) {
    #pragma omp parallel for
    for (int ir = 0; ir < n_pixels_grp; ++ir)
      radial_avg_grp[ir] *= ion_global.Ndot / pixel_areas_grp[ir]; // Apply radial_avg_grp correction
  }
  if (output_radial_avg_sub) {
    #pragma omp parallel for
    for (int ir = 0; ir < n_pixels_sub; ++ir)
      radial_avg_sub[ir] *= ion_global.Ndot / pixel_areas_sub[ir]; // Apply radial_avg_sub correction
  }

  // Angle-averaged radial surface brightness [photons/s/cm^2]
  if (output_radial_avg) {
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_pixels; ++ir)
      radial_avg[ir] *= 0.5 * to_radial_image_units[ir];
  }

  // Angle-averaged bin radial SB [photons/s/cm^2]
  if (output_radial_cube_avg) {
    #pragma omp parallel for
    for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
      for (int bin = 0; bin < n_bins; ++bin)
        radial_cube_avg(ir, bin) *= 0.5 * to_radial_cube_units[ir];
    }
  }

  // Radial surface brightness images [photons/s/cm^2]
  if (output_radial_images) {
    for (auto& radial_image : radial_images) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_pixels; ++ir)
        radial_image[ir] *= to_radial_image_units[ir];
    }
  }

  // Bin radial SB images [photons/s/cm^2]
  if (output_radial_cubes) {
    for (auto& bin_radial_image : bin_radial_images) {
      #pragma omp parallel for
      for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
        for (int bin = 0; bin < n_bins; ++bin)
          bin_radial_image(ir, bin) *= to_radial_cube_units[ir];
      }
    }
  }

  // Line-of-sight healpix maps
  if (output_map) {
    // Escape fractions [fraction]           // Note: After previous divisions
    const double n_pix_map = 12 * n_side_map * n_side_map; // Number of pixels
    rescale(map, n_pix_map);                 // Apply solid angle correction

    // Statistical moment maps
    if (output_map2)
      rescale(map2, n_pix_map * n_pix_map);  // Apply solid angle^2 correction
  }

  // Group healpix maps
  if (output_map_grp) {
    // Escape fractions [fraction]           // Note: After previous divisions
    const int n_pix_grp = 12 * n_side_grp * n_side_grp; // Number of pixels
    const double Ndot_pix = ion_global.Ndot * static_cast<double>(n_pix_grp); // Pixel factor
    if (output_grp_obs) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(Ndot_pix, Ndot_grp[i]);
        for (int j = 0; j < n_pix_grp; ++j)
          map_grp(i, j) *= halo_factor;      // Apply halo solid angle correction
      }
    }
    if (output_grp_vir) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(Ndot_pix, Ndot_grp_vir[i]);
        for (int j = 0; j < n_pix_grp; ++j)
          map_grp_vir(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }
    if (output_grp_gal) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(Ndot_pix, Ndot_grp_gal[i]);
        for (int j = 0; j < n_pix_grp; ++j)
          map_grp_gal(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }

    // Statistical moment maps
    if (output_map2) {
      const double L2_pix = Ndot_pix * Ndot_pix; // Pixel factor^2
      if (output_grp_obs) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, Ndot_grp[i] * Ndot_grp[i]);
          for (int j = 0; j < n_pix_grp; ++j)
            map2_grp(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_grp_vir) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, Ndot_grp_vir[i] * Ndot_grp_vir[i]);
          for (int j = 0; j < n_pix_grp; ++j)
            map2_grp_vir(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_grp_gal) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, Ndot_grp_gal[i] * Ndot_grp_gal[i]);
          for (int j = 0; j < n_pix_grp; ++j)
            map2_grp_gal(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
    }
  }

  // Subhalo healpix maps
  if (output_map_sub) {
    // Escape fractions [fraction]           // Note: After previous divisions
    const int n_pix_sub = 12 * n_side_sub * n_side_sub; // Number of pixels
    const double Ndot_pix = ion_global.Ndot * static_cast<double>(n_pix_sub); // Pixel factor
    if (output_sub_obs) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(Ndot_pix, Ndot_sub[i]);
        for (int j = 0; j < n_pix_sub; ++j)
          map_sub(i, j) *= halo_factor;      // Apply halo solid angle correction
      }
    }
    if (output_sub_vir) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(Ndot_pix, Ndot_sub_vir[i]);
        for (int j = 0; j < n_pix_sub; ++j)
          map_sub_vir(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }
    if (output_sub_gal) {
      #pragma omp parallel for
      for (int i = 0; i < n_groups; ++i) {
        const double halo_factor = safe_division(Ndot_pix, Ndot_sub_gal[i]);
        for (int j = 0; j < n_pix_sub; ++j)
          map_sub_gal(i, j) *= halo_factor;  // Apply halo solid angle correction
      }
    }

    // Statistical moment maps
    if (output_map2) {
      const double L2_pix = Ndot_pix * Ndot_pix; // Pixel factor^2
      if (output_sub_obs) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, Ndot_sub[i] * Ndot_sub[i]);
          for (int j = 0; j < n_pix_sub; ++j)
            map2_sub(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_sub_vir) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, Ndot_sub_vir[i] * Ndot_sub_vir[i]);
          for (int j = 0; j < n_pix_sub; ++j)
            map2_sub_vir(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
      if (output_sub_gal) {
        #pragma omp parallel for
        for (int i = 0; i < n_groups; ++i) {
          const double halo_factor_2 = safe_division(L2_pix, Ndot_sub_gal[i] * Ndot_sub_gal[i]);
          for (int j = 0; j < n_pix_sub; ++j)
            map2_sub_gal(i, j) *= halo_factor_2; // Apply halo solid angle^2 correction
        }
      }
    }
  }

  // Line-of-sight healpix radial map
  if (output_radial_map) {
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // Number of pixels
    const int n_pix2 = n_pix_radial * n_map_pixels; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      radial_map[i] *= to_radial_map_units[i % n_map_pixels]; // Apply radial_map correction
  }
  // Line-of-sight healpix radial cube map
  if (output_cube_map) {
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // Number of pixels
    const int n_pix2 = n_pix_cube * n_cube_map_pixels; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      cube_map[i] *= to_cube_map_units[i % n_cube_map_pixels]; // Apply cube_map correction
  }
  // Group healpix radial map
  if (output_radial_map_grp) {
    const int n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // Number of pixels
    const int n_pix2 = n_pix_radial_grp * n_map_pixels_grp; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      radial_map_grp[i] *= to_radial_map_grp_units[i % n_map_pixels_grp]; // Apply radial_map_grp correction
  }
  // Subhalo healpix radial map
  if (output_radial_map_sub) {
    const int n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // Number of pixels
    const int n_pix2 = n_pix_radial_sub * n_map_pixels_sub; // Number of flat elements
    #pragma omp parallel for
    for (int i = 0; i < n_pix2; ++i)
      radial_map_sub[i] *= to_radial_map_sub_units[i % n_map_pixels_sub]; // Apply radial_map_sub correction
  }

  // Intrinsic cameras (mcrt)
  if (output_mcrt_emission) {
    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      rescale(images_int, to_image_units);   // Apply image correction

    // Bin SB images [photons/s/cm^2]
    if (output_cubes)
      rescale(bin_images_int, to_cube_units); // Apply bin_image correction

    // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_images) {
      for (auto& radial_image_int : radial_images_int) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_pixels; ++ir)
          radial_image_int[ir] *= to_radial_image_units[ir];
      }
    }

    // Bin radial SB images [photons/s/cm^2]
    if (output_radial_cubes) {
      for (auto& bin_radial_image_int : bin_radial_images_int) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
          for (int bin = 0; bin < n_bins; ++bin)
            bin_radial_image_int(ir, bin) *= to_radial_cube_units[ir];
        }
      }
    }
  }

  // Attenuation cameras (mcrt)
  if (output_mcrt_attenuation) {
    // Escape fractions [fraction]
    if (output_escape_fractions)
      rescale(f_escs_ext, 2.);               // Apply phase function correction

    // Bin escape fractions [fraction]
    if (output_bin_escape_fractions) {
      #pragma omp parallel for
      for (int camera = 0; camera < n_cameras; ++camera)
        for (int i = 0; i < n_bins; ++i)
          bin_f_escs_ext[camera][i] *= 2. * safe_division(ion_global.Ndot, bin_Ndot_tot[i]);
    }

    // Surface brightness images [photons/s/cm^2]
    if (output_images)
      rescale(images_ext, to_image_units);   // Apply image correction

    // Bin SB images [photons/s/cm^2]
    if (output_cubes)
      rescale(bin_images_ext, to_cube_units); // Apply bin_image correction

    // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_images) {
      for (auto& radial_image_ext : radial_images_ext) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_pixels; ++ir)
          radial_image_ext[ir] *= to_radial_image_units[ir];
      }
    }

    // Bin radial SB images [photons/s/cm^2]
    if (output_radial_cubes) {
      for (auto& bin_radial_image_ext : bin_radial_images_ext) {
        #pragma omp parallel for
        for (int ir = 0; ir < n_radial_cube_pixels; ++ir) {
          for (int bin = 0; bin < n_bins; ++bin)
            bin_radial_image_ext(ir, bin) *= to_radial_cube_units[ir];
        }
      }
    }
  }

  // Photoionization integrals (convert to photons/s)
  #pragma omp parallel for
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell_strict(i)) {
      for (auto& rate_ion : rate_ions)
        rate_ion[i] = 0.;                    // Avoid cell
      if (output_photoheating)
        G_ion[i] = 0.;                       // Avoid cell
      if (output_photon_density) {
        for (int bin = 0; bin < n_bins; ++bin)
          bin_n_gamma(i,bin) = 0.;           // Avoid cell
      }
      continue;
    }
    const double Ndot_V = ion_global.Ndot / VOLUME(i); // Normalizations
    for (auto& rate_ion : rate_ions)
      rate_ion[i] *= Ndot_V;                 // Photoionization (convert to photons/s/H)
    if (output_photoheating)
      G_ion[i] *= ion_global.Ndot;           // Photoheating (convert to erg/s)
    if (output_photon_density) {
      const double Ndot_cV = Ndot_V / c;     // Normalization
      for (int bin = 0; bin < n_bins; ++bin)
        bin_n_gamma(i,bin) *= Ndot_cV;       // Photon density (convert to photons/cm^3)
    }
  }

  // Absorption statistics (convert to photons/s)
  if constexpr (output_ion_cell_age_freq)    // Cell, age, and frequency
    for (auto& stat : ion_cell_age_freq.stats) {
      const size_t n_vals = stat.size();     // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_vals; ++i)
        stat[i] *= ion_global.Ndot;          // Convert to photons/s
    }
  if constexpr (output_ion_radial_age_freq)  // Radial, age, and frequency
    for (auto& stat : ion_radial_age_freq.stats) {
      const size_t n_vals = stat.size();     // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_vals; ++i)
        stat[i] *= ion_global.Ndot;          // Convert to photons/s
    }
  if constexpr (output_ion_distance_age_freq) // Distance, age, and frequency
    for (auto& stat : ion_distance_age_freq.stats) {
      const size_t n_vals = stat.size();     // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_vals; ++i)
        stat[i] *= ion_global.Ndot;          // Convert to photons/s
    }
  if constexpr (output_radial_flow) {        // Radial flow statistics
    const size_t n_src_vals = radial_flow.src.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_src_vals; ++i)
      radial_flow.src[i] *= ion_global.Ndot; // Convert to photons/s
    const size_t n_esc_vals = radial_flow.esc.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_esc_vals; ++i)
      radial_flow.esc[i] *= ion_global.Ndot; // Convert to photons/s
    for (auto& stat : radial_flow.stats) {
      const size_t n_vals = stat.size();     // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_vals; ++i)
        stat[i] *= ion_global.Ndot;          // Convert to photons/s
    }
    const size_t n_pass_vals = radial_flow.pass.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_pass_vals; ++i)
      radial_flow.pass[i] *= ion_global.Ndot; // Convert to photons/s
    const size_t n_flow_vals = radial_flow.flow.size(); // Number of values
    #pragma omp parallel for
    for (size_t i = 0; i < n_flow_vals; ++i)
      radial_flow.flow[i] *= ion_global.Ndot; // Convert to photons/s
  }
  if constexpr (output_group_flows)          // Group flows
    for (auto& group_flow : group_flows) {
      const size_t n_src_vals = group_flow.src.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_src_vals; ++i)
        group_flow.src[i] *= ion_global.Ndot; // Convert to photons/s
      const size_t n_esc_vals = group_flow.esc.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_esc_vals; ++i)
        group_flow.esc[i] *= ion_global.Ndot; // Convert to photons/s
      for (auto& stat : group_flow.stats) {
        const size_t n_vals = stat.size();   // Number of values
        #pragma omp parallel for
        for (size_t i = 0; i < n_vals; ++i)
          stat[i] *= ion_global.Ndot;        // Convert to photons/s
      }
      const size_t n_pass_vals = group_flow.pass.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_pass_vals; ++i)
        group_flow.pass[i] *= ion_global.Ndot; // Convert to photons/s
      const size_t n_flow_vals = group_flow.flow.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_flow_vals; ++i)
        group_flow.flow[i] *= ion_global.Ndot; // Convert to photons/s
    }
  if constexpr (output_subhalo_flows)        // Subhalo flows
    for (auto& subhalo_flow : subhalo_flows) {
      const size_t n_src_vals = subhalo_flow.src.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_src_vals; ++i)
        subhalo_flow.src[i] *= ion_global.Ndot; // Convert to photons/s
      const size_t n_esc_vals = subhalo_flow.esc.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_esc_vals; ++i)
        subhalo_flow.esc[i] *= ion_global.Ndot; // Convert to photons/s
      for (auto& stat : subhalo_flow.stats) {
        const size_t n_vals = stat.size();   // Number of values
        #pragma omp parallel for
        for (size_t i = 0; i < n_vals; ++i)
          stat[i] *= ion_global.Ndot;        // Convert to photons/s
      }
      const size_t n_pass_vals = subhalo_flow.pass.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_pass_vals; ++i)
        subhalo_flow.pass[i] *= ion_global.Ndot; // Convert to photons/s
      const size_t n_flow_vals = subhalo_flow.flow.size(); // Number of values
      #pragma omp parallel for
      for (size_t i = 0; i < n_flow_vals; ++i)
        subhalo_flow.flow[i] *= ion_global.Ndot; // Convert to photons/s
    }
}

/* Calculate the self-shielding factor based on the hydrogen number density [cm^-3]. */
inline double Ionization::self_shielding_factor(const double nH) {
  if (!self_shielding)
    return 1.;                               // No self-shielding

  double SS_factor = SS_f * pow(1. + nH / pow(10., SS_n0), SS_alpha2)
    + (1. - SS_f) * pow(1. + pow(nH / pow(10., SS_n0), SS_beta), SS_alpha1);

  if (SS_factor > 1.)
    error("UV background self-shielding factor > 1!");

  return 1. - SS_xi * (1. - SS_factor);      // Allow smooth rescaling
}

/* Photoionization equilibrium calculation to update abundances. */
void Ionization::equilibrium_update(const bool active = true) {
  // Setup active functions
  vector<double (*)(const double)> alpha_RR_functions(n_active_ions, nullptr);
  vector<double (*)(const double)> alpha_DR_functions(n_active_ions, nullptr);
  vector<double (*)(const double)> beta_functions(n_active_ions, nullptr);
  vector<double (*)(const double)> xi_HI_functions(n_active_ions, nullptr);
  vector<double (*)(const double)> xi_HeI_functions(n_active_ions, nullptr);
  vector<double (*)(const double)> chi_HII_functions(n_active_ions, nullptr);
  vector<double (*)(const double)> chi_HeII_functions(n_active_ions, nullptr);
  for (int ai = 0; ai < n_active_ions; ++ai) {
    const int ion = active_ions[ai];         // Active ion index
    alpha_RR_functions[ai] = all_alpha_RR_functions[ion]; // Copy active ions
    if (dialectric_recombination)
      alpha_DR_functions[ai] = all_alpha_DR_functions[ion];
    else
      alpha_DR_functions[ai] = return_zero;
    beta_functions[ai] = all_beta_functions[ion];
    if (charge_exchange) {
      xi_HI_functions[ai] = all_xi_HI_functions[ion];
      chi_HII_functions[ai] = all_chi_HII_functions[ion];
    } else {
      xi_HI_functions[ai] = return_zero;
      chi_HII_functions[ai] = return_zero;
    }
    if (read_HeI && charge_exchange) {
      xi_HeI_functions[ai] = all_xi_HeI_functions[ion];
      chi_HeII_functions[ai] = all_chi_HeII_functions[ion];
    } else {
      xi_HeI_functions[ai] = return_zero;
      chi_HeII_functions[ai] = return_zero;
    }
  }
  constexpr double gamma = 5. / 3.;          // Adiabatic index
  constexpr double T_div_emu = (gamma - 1.) * mH / kB; // T / (e * mu)
  const double T_inc_factor = 1. - T_rtol;   // Temperature increase factor
  const double T_dec_factor = 1. + T_rtol;   // Temperature decrease factor
  const int first_metal_atom = read_helium ? 2 : 1; // First metal atom index
  const int first_metal_ion = read_HeII ? 3 : (read_HeI ? 2 : 1); // First metal ion index
  double V_tot = 0.;                         // Total volume [cm^3]
  double m_tot = 0.;                         // Total mass [g]
  double phot_HI = 0.;                       // Photoionizations [events/s]
  double UVB_HI = 0.;                        // UVB ionizations [events/s]
  // Note: The highest implicit state for each atom is stored after all normal ions
  vector<double> V_ions(n_active_ions_atoms); // Volume-weighted values
  vector<double> m_ions(n_active_ions_atoms); // Mass-weighted values
  vector<double> x_ions(n_active_ions_atoms); // Ionization fractions
  vector<double> c_ions(n_active_ions_atoms); // Solver constants
  vector<double> y_atoms(n_active_atoms);    // Number density ratios
  vector<double> alpha_ions(n_active_ions);  // Recombination rate coefficients [cm^3/s]
  vector<double> beta_ions(n_active_ions);   // Collisional ionization rate coefficients [cm^3/s]
  vector<double> xi_HI_ions(n_active_ions);  // Charge exchange recombination rate coefficients (HI) [cm^3/s]
  vector<double> xi_HeI_ions(n_active_ions); // Charge exchange recombination rate coefficients (HeI) [cm^3/s]
  vector<double> chi_HII_ions(n_active_ions); // Charge exchange ionization rate coefficients (HII) [cm^3/s]
  vector<double> chi_HeII_ions(n_active_ions); // Charge exchange ionization rate coefficients (HeII) [cm^3/s]
  vector<double> gamma_ions(n_active_ions);  // Photoionization rate coefficients [cm^3/s]
  vector<double> rec_sums(n_active_ions);    // Recombinations [events/s]
  vector<double> col_sums(n_active_ions);    // Collisional ionizations [events/s]
  vector<double> phot_sums(n_active_ions);   // Photoionizations [events/s]
  vector<double> cxi_H_sums(n_active_ions);  // Charge exchange ionizations (H) [events/s]
  vector<double> cxi_He_sums(n_active_ions); // Charge exchange ionizations (He) [events/s]
  vector<double> cxr_H_sums(n_active_ions);  // Charge exchange recombinations (H) [events/s]
  vector<double> cxr_He_sums(n_active_ions); // Charge exchange recombinations (He) [events/s]
  #pragma omp parallel for firstprivate(x_ions, c_ions, y_atoms, alpha_ions, beta_ions, xi_HI_ions, xi_HeI_ions, chi_HII_ions, chi_HeII_ions, gamma_ions) \
    reduction(+:V_tot, m_tot, phot_HI, UVB_HI) reduction(vec_plus:V_ions, m_ions, rec_sums, col_sums, phot_sums, cxi_H_sums, cxi_He_sums, cxr_H_sums, cxr_He_sums)
  for (int i = 0; i < n_cells; ++i) {
    if (avoid_cell_strict(i) ||
        #if spherical_escape
          (cell_center(i) - escape_center).dot() >= escape_radius2 ||
        #endif
        #if box_escape
          (r[i].x < escape_box[0].x || r[i].x > escape_box[1].x ||
           r[i].y < escape_box[0].y || r[i].y > escape_box[1].y ||
           r[i].z < escape_box[0].z || r[i].z > escape_box[1].z) ||
        #endif
        (avoid_SFR && SFR[i] > 0.)) {
      x_HII[i] = 0.; x_e[i] = 0.;            // HII and electrons are not in x_indices
      for (int ai = 0; ai < n_active_ions; ++ai)
        fields[x_indices[ai]].data[i] = 0.;  // Set cell to be empty
      continue;                              // Cell is not relevant
    }

    const double V_cell = VOLUME(i);         // Cell volume [cm^3]
    const double m_cell = rho[i] * V_cell;   // Cell mass [g]
    const double X_cell = read_hydrogen_fraction ? X[i] : hydrogen_fraction * (1. - Z[i]);
    const double nH = n_H[i];                // Hydrogen number density [cm^-3]
    const double SS_factor = self_shielding_factor(nH);
    y_atoms[0] = 1.;                         // Hydrogen number density ratio
    for (int aa = 1; aa < n_active_atoms; ++aa)
      y_atoms[aa] = fields[n_indices[aa]].data[i] / nH; // Atom number density ratios
    const double fac_xH2 = read_H2 ? 1. - 2. * x_H2[i] : 1.; // H2 correction factor
    if (fac_xH2 < 0. || fac_xH2 > 1.)
      error("Molecular fraction is out of range: 1 - 2 x_H2 = " + to_string(fac_xH2));
    double xe_prev = 1., xe = 1., ne = nH;   // Track electron values
    double &xHI = x_ions[0], &xHII = x_ions[n_active_ions]; // HI references
    double &bHI = beta_ions[0], &gHI = gamma_ions[0], &aHII = alpha_ions[0];
    double fake_xHeI = 0., fake_bHeI = 0., fake_gHeI = 0., fake_aHeII = 0., fake_eHeII = 0.;
    double &xHeI = read_HeI ? x_ions[1] : fake_xHeI; // HeI references
    double &bHeI = read_HeI ? beta_ions[1] : fake_bHeI, &gHeI = read_HeI ? gamma_ions[1] : fake_gHeI;
    double &aHeII = read_HeI ? alpha_ions[1] : fake_aHeII, &eHeII = read_HeI ? xi_HI_ions[1] : fake_eHeII;
    double fake_xHeII = 0., fake_bHeII = 0., fake_gHeII = 0., fake_aHeIII = 0., fake_eHeIII = 0.;
    double &xHeII = read_HeII ? x_ions[2] : fake_xHeII; // HeII references
    double &bHeII = read_HeII ? beta_ions[2] : fake_bHeII, &gHeII = read_HeII ? gamma_ions[2] : fake_gHeII;
    double &aHeIII = read_HeII ? alpha_ions[2] : fake_aHeIII, &eHeIII = read_HeII ? xi_HI_ions[2] : fake_eHeIII;
    double fake_xHeIII = 0., fake_yHe = 0.;  // HeIII and yHe
    double &xHeIII = read_HeII ? x_ions[n_active_ions+1] : fake_xHeIII;
    double &yHe = read_HeI ? y_atoms[1] : fake_yHe;
    int count = 0;                           // Number of iterations
    double T_i = T[i];                       // Temperature placeholder
    for (int ai = 0; ai < n_active_ions; ++ai) {
      // Total recombination rate coefficients (Case B; radiative + dielectronic) [cm^3/s] (Badnell 2006)
      alpha_ions[ai] = alpha_RR_functions[ai](T_i) + alpha_DR_functions[ai](T_i); // ion+ + e-  ->  ion + γ
      // Collisional ionization rate coefficients [cm^3/s] (Cen 1992; Voronov 1997)
      beta_ions[ai] = beta_functions[ai](T_i); // ion + e-  ->  ion+ + 2e-
      // Hydrogen charge exchange recombination rate coefficients [cm^3/s] (Kingdon & Ferland 1996)
      xi_HI_ions[ai] = xi_HI_functions[ai](T_i); // ion+ + HI  ->  ion + HII
      // Helium charge exchange recombination rate coefficients [cm^3/s]
      xi_HeI_ions[ai] = xi_HeI_functions[ai](T_i); // ion+ + HeI  ->  ion + HeII
      // Hydrogen charge exchange ionization rate coefficients [cm^3/s]
      chi_HII_ions[ai] = chi_HII_functions[ai](T_i); // ion + HII  ->  ion+ + HI
      // Helium charge exchange ionization rate coefficients [cm^3/s]
      chi_HeII_ions[ai] = chi_HeII_functions[ai](T_i); // ion + HeII  ->  ion+ + HeI
      // Photoionization integrals
      gamma_ions[ai] = rate_ions[ai][i];     // Copy active ions
    }

    // Uniform UVB photoionization rate (including self-shielding)
    double gHI_UVB = 0., gHeI_UVB = 0., gHeII_UVB = 0.;
    if (have_UVB && SS_factor > 0.) {
      gHI_UVB = SS_factor * UVB_rate_HI;     // HI
      if (read_HeI) gHeI_UVB = SS_factor * UVB_rate_HeI; // HeI
      if (read_HeII) gHeII_UVB = SS_factor * UVB_rate_HeII; // HeII
      if (output_photoheating) {
        double UVB_heat_eff = UVB_heat_HI * x_HI[i] * n_H[i];
        if (read_HeI) UVB_heat_eff += UVB_heat_HeI * x_HeI[i] * n_He[i];
        if (read_HeII) UVB_heat_eff += UVB_heat_HeII * x_HeII[i] * n_He[i];
        G_ion[i] += SS_factor * UVB_heat_eff * V_cell; // Total rate [erg/s]
      }
    }

    // Evaluate states iteratively, eq. (33-38) of Katz, Weinberg, Hernquist (1996)
    if (active) {                            // Photoionization equilibrium
      while (true) {
        // Hydrogen
        const double cHI = (bHI + (gHI + gHI_UVB)/ne) / aHII;
        xHI = fac_xH2 / (1. + cHI);          // Simplified format
        xHII = fac_xH2 - xHI;                // 1 - x_HI - 2 x_H2
        const double HIe = xHI / xe;         // n_HI / n_e for charge exchange recombination
        const double HIIe = xHII / xe;       // n_HII / n_e for charge exchange ionization
        // Helium
        const double cHeI = safe_division(bHeI + (gHeI + gHeI_UVB)/ne, aHeII + eHeII*HIe);
        const double cHeII = safe_division(bHeII + (gHeII + gHeII_UVB)/ne, aHeIII + eHeIII*HIe);
        xHeI = 1. / (1. + cHeI * (1. + cHeII));
        xHeII = xHeI * cHeI;                 // States are linked
        xHeIII = xHeII * cHeII;
        const double HeIe = xHeI * yHe / xe; // n_HeI / n_e for charge exchange recombination
        const double HeIIe = xHeII * yHe / xe; // n_HeII / n_e for charge exchange ionization
        // Metals and electrons
        xe = xHII + yHe * (xHeII + 2. * xHeIII); // Hydrogen + Helium
        for (int aa = first_metal_atom, ai = first_metal_ion - 1; aa < n_active_atoms; ++aa) {
          const int ion_count = ion_counts[aa]; // Number of active ions
          double denom = 1.;                 // Denominator: xI = 1 / (1 + cI * (1 + cII * (1 + ...)))
          ai += ion_count;                   // Last ion index (explicitly tracked)
          for (int j = 0; j < ion_count; ++j, --ai) {
            c_ions[ai] = safe_division(beta_ions[ai] + chi_HII_ions[ai]*HIIe + chi_HeII_ions[ai]*HeIIe + gamma_ions[ai]/ne,
                                        alpha_ions[ai] + xi_HI_ions[ai]*HIe + xi_HeI_ions[ai]*HeIe);
            denom = 1. + c_ions[ai] * denom; // Recurence relation
          }
          double xe_sum = 0., x_ion_next;    // Sum for counting free electrons
          x_ions[++ai] = 1. / denom;         // First ionization state (ground state)
          for (int j = 1; j < ion_count; ++j) {
            x_ion_next = x_ions[ai] * c_ions[ai]; // Next ionization state (explicitly tracked)
            xe_sum += double(j) * x_ion_next; // Add free electrons
            x_ions[++ai] = x_ion_next;       // States are linked
          }
          x_ion_next = x_ions[ai] * c_ions[ai]; // Last ionization state (implicitly tracked)
          xe_sum += double(ion_count) * x_ion_next; // Add final free electrons
          x_ions[n_active_ions+aa] = x_ion_next; // Store after all normal ions
          xe += y_atoms[aa] * xe_sum;        // Electrons (using number density ratios)
        }
        xe = 0.5 * (xe + xe_prev);           // Stability
        ne = xe * nH;                        // Update electron number density

        // Update the temperature in case it is affected by ionization
        bool T_conv_flag = true;             // Temperature convergence flag
        if (use_internal_energy) {
          const double T_i_prev = T_i;       // Track previous value
          const double mu = 4. / (1. + X_cell * (3. + 4. * xe)); // Mean molecular mass [mH]
          T_i = T_div_emu * e_int[i] * mu;   // Gas temperature [K]
          if (T_floor >= 0. && T_i < T_floor)
            T_i = T_floor;                   // Apply temperature floor [K]
          if (T_inc_factor * T_i > T_i_prev) {
            T_i = T_i_prev / T_inc_factor;   // Stability (limit increase)
            T_conv_flag = false;             // Temperature not converged
          }
          if (T_dec_factor * T_i < T_i_prev) {
            T_i = T_i_prev / T_dec_factor;   // Stability (limit decrease)
            T_conv_flag = false;             // Temperature not converged
          }
          T[i] = T_i;                        // Update temperature
          for (int ai = 0; ai < n_active_ions; ++ai) {
            // Total recombination rate coefficients (Case B; radiative + dielectronic) [cm^3/s] (Badnell 2006)
            alpha_ions[ai] = alpha_RR_functions[ai](T_i) + alpha_DR_functions[ai](T_i); // ion+ + e-  ->  ion + γ
            // Collisional ionization rate coefficients [cm^3/s] (Cen 1992; Voronov 1997)
            beta_ions[ai] = beta_functions[ai](T_i); // ion + e-  ->  ion+ + 2e-
            // Hydrogen charge exchange recombination rate coefficients [cm^3/s] (Kingdon & Ferland 1996)
            xi_HI_ions[ai] = xi_HI_functions[ai](T_i); // ion+ + HI  ->  ion + HII
            // Helium charge exchange recombination rate coefficients [cm^3/s]
            xi_HeI_ions[ai] = xi_HeI_functions[ai](T_i); // ion+ + HeI  ->  ion + HeII
            // Hydrogen charge exchange ionization rate coefficients [cm^3/s]
            chi_HII_ions[ai] = chi_HII_functions[ai](T_i); // ion + HII  ->  ion+ + HI
            // Helium charge exchange ionization rate coefficients [cm^3/s]
            chi_HeII_ions[ai] = chi_HeII_functions[ai](T_i); // ion + HeII  ->  ion+ + HeI
          }
        }

        if (T_conv_flag && fabs(xe - xe_prev) < 1e-9)
          break;                             // Converged
        xe_prev = xe;                        // Track previous value

        if (++count > 100) {
          #pragma omp critical
          cout << "\nT = " << T_i << " K, n_e = " << ne << " cm^-3, n_H = " << nH << " cm^-3"
               << "\nx_e = " << xe << ", x_e_prev = " << xe_prev
               << "\nx_HI = " << xHI << ", x_HII = " << xHII
               << "\nx_HeI = " << xHeI << ", x_HeII = " << xHeII << ", x_HeIII = " << xHeIII << endl;
          error("The photoionization equilibrium calculation did not converge within 100 iterations!");
        }
      };
    } else {                                 // Passive calculations
      xHI = x_HI[i]; xHII = x_HII[i];        // Already initialized
      for (int aa = He_ATOM, ai = HeI_ION; aa < n_active_atoms; ++aa) {
        double x_sum = 0.;                   // Cumulative fraction sum
        const int ion_count = ion_counts[aa]; // Number of active ions
        for (int j = 0; j < ion_count; ++j, ++ai)
          x_sum += x_ions[ai] = fields[x_indices[ai]].data[i]; // Ionization fraction
        x_ions[n_active_ions+aa] = fmax(1. - x_sum, 0.); // Store after all normal ions
      }
      xe = x_e[i];                           // Electrons
      ne = xe * nH;                          // Update electron number density
    }

    // Global statistics
    const double ne_V = ne * V_cell;         // Integration factors
    const double nH_V = nH * V_cell;
    const double nHe_V = yHe * nH_V;
    const double nHI_V = xHI * nH_V, nHII_V = xHII * nH_V;
    const double nHeI_V = xHeI * nHe_V, nHeII_V = xHeII * nHe_V;
    V_tot += V_cell;                         // Total volume
    m_tot += m_cell;                         // Total mass
    for (int aia = 0; aia < n_active_ions_atoms; ++aia) {
      V_ions[aia] += x_ions[aia] * V_cell;   // Volume-weighted values
      m_ions[aia] += x_ions[aia] * m_cell;   // Mass-weighted values
    }
    // Hydrogen
    double rec_ion = aHII * ne * nHII_V;     // Recombinations [events/s]
    double col_ion = bHI * ne * nHI_V;       // Collisional ionizations [events/s]
    double phot_ion = gHI * nHI_V;           // Photoionizations [events/s]
    double UVB_ion = SS_factor * UVB_rate_HI * nHI_V; // UVB ionizations [events/s]
    double cxi_H_ion = 0., cxi_He_ion = 0.;  // Charge exchange ionizations (H, He) [events/s]
    double cxr_H_ion = 0., cxr_He_ion = 0.;  // Charge exchange recombinations (H, He) [events/s]
    rec_sums[0] += rec_ion;                  // Recombinations [events/s]
    col_sums[0] += col_ion;                  // Collisional ionizations [events/s]
    phot_sums[0] += phot_ion;                // Photoionizations [events/s]
    UVB_HI += UVB_ion;                       // UVB ionizations [events/s]
    int i_rec = 0, i_col = 0, i_phot = 0;    // Cell stats counters
    int i_cxi_H = 0, i_cxi_He = 0, i_cxr_H = 0, i_cxr_He = 0;
    if (output_cells_rec_ions[i_rec])
      Ndot_rec_ions_cells[i_rec++][i] = rec_ion; // Output cell recombinations [events/s]
    if (output_cells_col_ions[i_col])
      Ndot_col_ions_cells[i_col++][i] = col_ion; // Output cell collisional ionizations [events/s]
    if (output_cells_phot_ions[i_phot])
      Ndot_phot_ions_cells[i_phot++][i] = phot_ion; // Output cell photoionizations [events/s]
    if (output_cells_UVB_HI)
      Ndot_UVB_HI_cells[i] = UVB_ion;        // Output cell UVB ionizations [events/s]
    // Note: charge exchange cannot be output for hydrogen and helium
    // Helium and metals
    for (int aa = He_ATOM, ai = HeI_ION; aa < n_active_atoms; ++aa) {
      const double natom = y_atoms[aa] * nH; // Atom number density [cm^-3]
      const double natom_V = natom * V_cell, natom_ne_V = natom * ne_V; // Integration factors
      const double natom_nHI_V = natom * nHI_V, natom_nHII_V = natom * nHII_V;
      const double natom_nHeI_V = natom * nHeI_V, natom_nHeII_V = natom * nHeII_V;
      const int ion_count = ion_counts[aa];  // Number of active ions
      // First ion
      col_ion = beta_ions[ai] * x_ions[ai] * natom_ne_V;
      phot_ion = gamma_ions[ai] * x_ions[ai] * natom_V;
      cxi_H_ion = chi_HII_ions[ai] * x_ions[ai] * natom_nHII_V;
      cxi_He_ion = chi_HeII_ions[ai] * x_ions[ai] * natom_nHeII_V;
      col_sums[ai] += col_ion;               // Collisional ionizations [events/s]
      phot_sums[ai] += phot_ion;             // Photoionizations [events/s]
      cxi_H_sums[ai] += cxi_H_ion;           // Charge exchange ionizations (H) [events/s]
      cxi_He_sums[ai] += cxi_He_ion;         // Charge exchange ionizations (He) [events/s]
      if (output_cells_col_ions[ai]) Ndot_col_ions_cells[i_col++][i] = col_ion; // Output cell values
      if (output_cells_phot_ions[ai]) Ndot_phot_ions_cells[i_phot++][i] = phot_ion;
      if (output_cells_cxi_H_ions[ai]) Ndot_cxi_H_ions_cells[i_cxi_H++][i] = cxi_H_ion;
      if (output_cells_cxi_He_ions[ai]) Ndot_cxi_He_ions_cells[i_cxi_He++][i] = cxi_He_ion;
      int aim1 = ai++;                       // Save previous ion index before incrementing
      // Middle ions
      for (int j = 1; j < ion_count; ++j, ++ai, ++aim1) {
        rec_ion = alpha_ions[aim1] * x_ions[ai] * natom_ne_V;
        col_ion = beta_ions[ai] * x_ions[ai] * natom_ne_V;
        phot_ion = gamma_ions[ai] * x_ions[ai] * natom_V;
        cxi_H_ion = chi_HII_ions[ai] * x_ions[ai] * natom_nHII_V;
        cxi_He_ion = chi_HeII_ions[ai] * x_ions[ai] * natom_nHeII_V;
        cxr_H_ion = xi_HI_ions[aim1] * x_ions[ai] * natom_nHI_V;
        cxr_He_ion = xi_HeI_ions[aim1] * x_ions[ai] * natom_nHeI_V;
        rec_sums[aim1] += rec_ion;           // Recombinations [events/s]
        col_sums[ai] += col_ion;             // Collisional ionizations [events/s]
        phot_sums[ai] += phot_ion;           // Photoionizations [events/s]
        cxi_H_sums[ai] += cxi_H_ion;         // Charge exchange ionizations (H) [events/s]
        cxi_He_sums[ai] += cxi_He_ion;       // Charge exchange ionizations (He) [events/s]
        cxr_H_sums[aim1] += cxr_H_ion;       // Charge exchange recombinations (H) [events/s]
        cxr_He_sums[aim1] += cxr_He_ion;     // Charge exchange recombinations (He) [events/s]
        if (output_cells_rec_ions[aim1]) Ndot_rec_ions_cells[i_rec++][i] = rec_ion; // Output cell values
        if (output_cells_col_ions[ai]) Ndot_col_ions_cells[i_col++][i] = col_ion;
        if (output_cells_phot_ions[ai]) Ndot_phot_ions_cells[i_phot++][i] = phot_ion;
        if (output_cells_cxi_H_ions[ai]) Ndot_cxi_H_ions_cells[i_cxi_H++][i] = cxi_H_ion;
        if (output_cells_cxi_He_ions[ai]) Ndot_cxi_He_ions_cells[i_cxi_He++][i] = cxi_He_ion;
        if (output_cells_cxr_H_ions[aim1]) Ndot_cxr_H_ions_cells[i_cxr_H++][i] = cxr_H_ion;
        if (output_cells_cxr_He_ions[aim1]) Ndot_cxr_He_ions_cells[i_cxr_He++][i] = cxr_He_ion;
      }
      // Last ion
      const int last_ion = n_active_ions + aa; // Last ion index (implicitly tracked)
      rec_ion = alpha_ions[aim1] * x_ions[last_ion] * natom_ne_V;
      cxr_H_ion = xi_HI_ions[aim1] * x_ions[ai] * natom_nHI_V;
      cxr_He_ion = xi_HeI_ions[aim1] * x_ions[ai] * natom_nHeI_V;
      rec_sums[aim1] += rec_ion;             // Recombinations [events/s]
      cxr_H_sums[aim1] += cxr_H_ion;         // Charge exchange recombinations (H) [events/s]
      cxr_He_sums[aim1] += cxr_He_ion;       // Charge exchange recombinations (He) [events/s]
      if (output_cells_rec_ions[aim1]) Ndot_rec_ions_cells[i_rec++][i] = rec_ion; // Output cell values
      if (output_cells_cxr_H_ions[aim1]) Ndot_cxr_H_ions_cells[i_cxr_H++][i] = cxr_H_ion;
      if (output_cells_cxr_He_ions[aim1]) Ndot_cxr_He_ions_cells[i_cxr_He++][i] = cxr_He_ion;
    }

    // Update cell abundances
    if (active) {
      x_e[i] = xe; x_HII[i] = xHII;          // Electrons and HII
      for (int ai = 0; ai < n_active_ions; ++ai)
        fields[x_indices[ai]].data[i] = x_ions[ai]; // Ionization fraction
    }
  }

  // Global summary statistics
  const double tot_HI = col_sums[0] + phot_HI + UVB_HI; // Total ionizations [events/s]
  f_col_HI.push_back(col_sums[0] / tot_HI);  // Fraction of total ionizations
  f_UVB_HI.push_back(UVB_HI / tot_HI);
  for (int aia = 0; aia < n_active_ions_atoms; ++aia) {
    x_V_ions[aia].push_back(V_ions[aia] / V_tot); // Volume-weighted fractions
    x_m_ions[aia].push_back(m_ions[aia] / m_tot); // Mass-weighted fractions
  }
  for (int ai = 0; ai < n_active_ions; ++ai) {
    rec_ions[ai].push_back(rec_sums[ai]);    // Recombinations [events/s]
    col_ions[ai].push_back(col_sums[ai]);    // Collisional ionizations [events/s]
    phot_ions[ai].push_back(phot_sums[ai]);  // Photoionizations [events/s]
    cxi_H_ions[ai].push_back(cxi_H_sums[ai]); // Charge exchange ionizations (H) [events/s]
    cxi_He_ions[ai].push_back(cxi_He_sums[ai]); // Charge exchange ionizations (He) [events/s]
    cxr_H_ions[ai].push_back(cxr_H_sums[ai]); // Charge exchange recombinations (H) [events/s]
    cxr_He_ions[ai].push_back(cxr_He_sums[ai]); // Charge exchange recombinations (He) [events/s]
  }
}

/* Print latest convergence statistics. */
void Ionization::print_convergence() {
  // Print summary of updates
  const int i = x_V_ions[0].size() - 1;      // Last index
  const int im1 = i - 1;
  const int n_print_atoms = verbose ? n_active_atoms : 1;
  const int n_print_ions = verbose ? n_active_ions : 1;
  cout << "\nGlobal convergence statistics and relative differences:\n"
       << " Volume-weighted averages:\n";
  for (int aa = 0, ai = 0; aa < n_print_atoms; ++aa) {
    const int ion_count = ion_counts[aa];    // Number of active ions
    for (int j = 0; j < ion_count; ++j, ++ai)
      print_pdiff(fields[x_indices[ai]].name, x_V_ions[ai][im1], x_V_ions[ai][i]);
    const int last_ion = n_active_ions + aa; // Last ion index (implicitly tracked)
    print_pdiff(fields[x_indices[ai-1]+1].name, x_V_ions[last_ion][im1], x_V_ions[last_ion][i]);
  }
  cout << " Mass-weighted averages:\n";
  for (int aa = 0, ai = 0; aa < n_print_atoms; ++aa) {
    const int ion_count = ion_counts[aa];    // Number of active ions
    for (int j = 0; j < ion_count; ++j, ++ai)
      print_pdiff(fields[x_indices[ai]].name, x_m_ions[ai][im1], x_m_ions[ai][i]);
    const int last_ion = n_active_ions + aa; // Last ion index (implicitly tracked)
    print_pdiff(fields[x_indices[ai-1]+1].name, x_m_ions[last_ion][im1], x_m_ions[last_ion][i]);
  }
  cout << " Recombination events per second:\n";
  for (int ion = 0; ion < n_print_ions; ++ion)
    print_pdiff(fields[x_indices[ion]+1].name, rec_ions[ion][im1], rec_ions[ion][i], 'R');
  cout << " Collisional ionizations per second:\n";
  for (int ion = 0; ion < n_print_ions; ++ion)
    print_pdiff(fields[x_indices[ion]].name, col_ions[ion][im1], col_ions[ion][i], 'C');
}

/* Driver for ionization calculations. */
void Ionization::run() {
  mcrt_timer.start();

  // Perform MCRT iterations until convergence
  double ion_error = 1.;                     // Recombination relative error
  equilibrium_update(false);                 // Base convergence calculations
  if (output_photoheating)
    clear(G_ion);                            // UVB can corrupt photoheating
  for (int iter = 1; iter <= max_iter; ++iter) {
    // Perform the actual ionization calculations
    n_finished = 0;                          // Reset finished progress
    equal_workers();                         // Assign equal work across ranks

    // Sort escaped and absorbed photons
    if (output_photons)
      sort_photons();

    // Add all independent photons from different processors
    if (n_ranks > 1)
      reduce_ionization();

    if (root) {
      // Convert observables to the correct units
      correct_units();
      print_ionization_observables();
      equilibrium_update();                  // Update abundances
      print_convergence();                   // Print convergence statistics
      ion_error = fabs(1. - rec_ions[0][iter-1] / rec_ions[0][iter]); // Error estimate
      cout << "\nIonization: Finished iteration " << iter << endl;
    }

    // Early completion if convergence is reached
    mpi_bcast(ion_error);                    // Report convergence statistics
    if (ion_error < max_error)
      break;

    // Remainder of loop is not needed on the final iteration
    if (iter == max_iter) {
      if (root)
        cout << "\nWarning: Reached max number of iterations before convergence!" << endl;
      break;
    }

    // Write intermediate output files if requested
    // if (output_iter)
    //   write_hdf5();                          // Simulation info and data

    // Broadcast abundances to other ranks
    if (n_ranks > 1) {
      mpi_bcast(x_HII);                      // Synchronize fractions
      for (int x_index : x_indices)
        mpi_bcast(fields[x_index].data);     // Loop over x_ion data
      mpi_bcast(x_e);
    }

    // Update the dust density in case survival is affected
    if (f_ion != 1.) {
      #pragma omp parallel for
      for (int i = 0; i < n_cells; ++i) {
        if (avoid_cell_strict(i)) {          // Ignore certain cells
          if constexpr (graphite_dust)
            rho_dust_G[i] = 0.;
          if constexpr (silicate_dust)
            rho_dust_S[i] = 0.;
          if constexpr (n_dust_species == 1)
            rho_dust[i] = 0.;
          continue;
        }
        const double f_surv = (T[i] > T_sputter) ? 0. : (f_ion == 1. ? 1. : 1. + (f_ion - 1.) * x_HII[i]);
        if constexpr (n_dust_species == 1) {
          rho_dust[i] = f_surv * rho[i] * D[i]; // Dust density [g/cm^3]
        } else {                             // Multi-species dust
          if constexpr (graphite_dust) {
            double D_G = D[i];               // Graphite dust-to-gas ratio
            if constexpr (silicate_dust)
              D_G -= D_S[i];                 // Subtract silicate dust-to-gas ratio
            rho_dust_G[i] = f_surv * rho[i] * D_G; // Graphite dust density [g/cm^3]
          }
          if constexpr (silicate_dust)
            rho_dust_S[i] = f_surv * rho[i] * D_S[i]; // Silicate dust density [g/cm^3]
        }
      }
    }
    if (cell_based_emission) {
      iterative_setup();                     // Setup that is performed during each iteration
      if (root)
        cout << "\nUpdated gas emission: Ndot_gas = " << ion_gas.Ndot <<
                " photons/s  (Ndot_gas/Ndot_tot = " << ion_gas.Ndot / ion_global.Ndot << ")" << endl;
    }

    // Clear cameras and path estimators
    clear(bin_f_src);                        // Global bin source fraction
    clear(bin_f2_src);                       // Global bin squared f_src
    clear(bin_f_esc);                        // Global bin escape fraction
    clear(bin_f2_esc);                       // Global bin squared f_esc
    clear(bin_f_abs);                        // Global bin dust absorption fraction
    clear(bin_f2_abs);                       // Global bin squared f_abs
    clear(bin_f_ions);                       // Global bin species absorption fractions
    clear(bin_f2_HI);                        // Global bin squared f_HI
    for (auto& rate_ion : rate_ions)
      clear(rate_ion);                       // Photoionization integrals [1/s]
    clear(bin_l_abs);                        // Global bin absorption distance [cm]
    if (output_radial_avg)
      clear(radial_avg);                     // Angle-averaged radial surface brightness
    if (output_radial_cube_avg)
      clear(radial_cube_avg);                // Angle-averaged radial spectral data cube
    if (output_radial_map)
      clear(radial_map);                     // Angle-averaged radial surface brightness map
    if (output_cube_map)
      clear(cube_map);                       // Angle-averaged radial spectral data cube map
    if constexpr (output_groups) {           // Group statistics
      if (n_groups > 0) {
        clear(bin_f_src_grp);                // Group bin source fraction
        clear(bin_f2_src_grp);               // Group bin squared f_src
        if (output_grp_obs) {
          clear(bin_f_esc_grp);              // Group bin escape fraction
          clear(bin_f2_esc_grp);             // Group bin squared f_esc
          clear(bin_f_abs_grp);              // Group bin absorption fraction
          clear(bin_f2_abs_grp);             // Group bin squared f_abs
          if (output_map_grp) {
            clear(map_grp);                  // Group map fraction
            if (output_map2_grp)
              clear(map2_grp);               // Group squared map fraction
          }
          if (output_radial_map_grp)
            clear(radial_map_grp);           // Group radial map
          if (output_radial_avg_grp)
            clear(radial_avg_grp);           // Group average radial profile
        }
        if (output_grp_vir) {                // Virial radius statistics
          clear(bin_f_src_grp_vir);          // Group bin source fraction
          clear(bin_f2_src_grp_vir);         // Group bin squared f_src
          clear(bin_f_esc_grp_vir);          // Group bin escape fraction
          clear(bin_f2_esc_grp_vir);         // Group bin squared f_esc
          clear(bin_f_abs_grp_vir);          // Group bin absorption fraction
          clear(bin_f2_abs_grp_vir);         // Group bin squared f_abs
          if (output_map_grp) {
            clear(map_grp_vir);              // Group map fraction
            if (output_map2_grp)
              clear(map2_grp_vir);           // Group squared map fraction
          }
        }
        if (output_grp_gal) {                // Galaxy statistics
          clear(bin_f_src_grp_gal);          // Group bin source fraction
          clear(bin_f2_src_grp_gal);         // Group bin squared f_src
          clear(bin_f_esc_grp_gal);          // Group bin escape fraction
          clear(bin_f2_esc_grp_gal);         // Group bin squared f_esc
          clear(bin_f_abs_grp_gal);          // Group bin absorption fraction
          clear(bin_f2_abs_grp_gal);         // Group bin squared f_abs
          if (output_map_grp) {
            clear(map_grp_gal);              // Group map fraction
            if (output_map2_grp)
              clear(map2_grp_gal);           // Group squared map fraction
          }
        }
      }
      if (n_ugroups > 0) {
        clear(bin_f_src_ugrp);               // Unfiltered group bin source fraction
        clear(bin_f2_src_ugrp);              // Unfiltered group bin squared f_src
      }
    }
    if constexpr (output_subhalos) {         // Subhalo statistics
      if (n_subhalos > 0) {
        clear(bin_f_src_sub);                // Subhalo bin source fraction
        clear(bin_f2_src_sub);               // Subhalo bin squared f_src
        if (output_sub_obs) {
          clear(bin_f_esc_sub);              // Subhalo bin escape fraction
          clear(bin_f2_esc_sub);             // Subhalo bin squared f_esc
          clear(bin_f_abs_sub);              // Subhalo bin absorption fraction
          clear(bin_f2_abs_sub);             // Subhalo bin squared f_abs
          if (output_map_sub) {
            clear(map_sub);                  // Subhalo map fraction
            if (output_map2_sub)
              clear(map2_sub);               // Subhalo squared map fraction
          }
          if (output_radial_map_sub)
            clear(radial_map_sub);           // Subhalo radial map
          if (output_radial_avg_sub)
            clear(radial_avg_sub);           // Subhalo average radial profile
        }
        if (output_sub_vir) {                // Virial radius statistics
          clear(bin_f_src_sub_vir);          // Subhalo bin source fraction
          clear(bin_f2_src_sub_vir);         // Subhalo bin squared f_src
          clear(bin_f_esc_sub_vir);          // Subhalo bin escape fraction
          clear(bin_f2_esc_sub_vir);         // Subhalo bin squared f_esc
          clear(bin_f_abs_sub_vir);          // Subhalo bin absorption fraction
          clear(bin_f2_abs_sub_vir);         // Subhalo bin squared f_abs
          if (output_map_sub) {
            clear(map_sub_vir);              // Subhalo map fraction
            if (output_map2_sub)
              clear(map2_sub_vir);           // Subhalo squared map fraction
          }
        }
        if (output_sub_gal) {                // Galaxy statistics
          clear(bin_f_src_sub_gal);          // Subhalo bin source fraction
          clear(bin_f2_src_sub_gal);         // Subhalo bin squared f_src
          clear(bin_f_esc_sub_gal);          // Subhalo bin escape fraction
          clear(bin_f2_esc_sub_gal);         // Subhalo bin squared f_esc
          clear(bin_f_abs_sub_gal);          // Subhalo bin absorption fraction
          clear(bin_f2_abs_sub_gal);         // Subhalo bin squared f_abs
          if (output_map_sub) {
            clear(map_sub_gal);              // Subhalo map fraction
            if (output_map2_sub)
              clear(map2_sub_gal);           // Subhalo squared map fraction
          }
        }
      }
      if (n_usubhalos > 0) {
        clear(bin_f_src_usub);               // Unfiltered subhalo bin source fraction
        clear(bin_f2_src_usub);              // Unfiltered subhalo bin squared f_src
      }
    }
    if (output_photoheating)
      clear(G_ion);                          // Photoheating rates [erg/s]
    if (output_photon_density)
      clear(bin_n_gamma);                    // Photon density [photons/cm^3]
    if constexpr (output_ion_cell_age_freq)  // Absorptions [photons/s]
      for (auto& stat : ion_cell_age_freq.stats)
        clear(stat);                         // Ion specific stats
    if constexpr (output_ion_radial_age_freq) // Absorptions [photons/s]
      for (auto& stat : ion_radial_age_freq.stats)
        clear(stat);                         // Ion specific stats
    if constexpr (output_ion_distance_age_freq) // Absorptions [photons/s]
      for (auto& stat : ion_distance_age_freq.stats)
        clear(stat);                         // Ion specific stats
    if constexpr (output_radial_flow) {      // Radial flow statistics [photons/s]
      clear(radial_flow.src);                // Emission
      clear(radial_flow.esc);                // Escape
      for (auto& stat : radial_flow.stats)   // Ion specific
        clear(stat);                         // Absorption
      clear(radial_flow.pass);               // First passage
      clear(radial_flow.flow);               // Flow
    }
    if constexpr (output_group_flows) {      // Group flow statistics [photons/s]
      for (auto& group_flow : group_flows) {
        clear(group_flow.src);               // Emission
        clear(group_flow.esc);               // Escape
        for (auto& stat : group_flow.stats)  // Ion specific
          clear(stat);                       // Absorption
        clear(group_flow.pass);              // First passage
        clear(group_flow.flow);              // Flow
      }
    }
    if constexpr (output_subhalo_flows) {    // Subhalo flow statistics [photons/s]
      for (auto& subhalo_flow : subhalo_flows) {
        clear(subhalo_flow.src);             // Emission
        clear(subhalo_flow.esc);             // Escape
        for (auto& stat : subhalo_flow.stats) // Ion specific
          clear(stat);                       // Absorption
        clear(subhalo_flow.pass);            // First passage
        clear(subhalo_flow.flow);            // Flow
      }
    }
    if (have_cameras) {
      // Primary cameras (mcrt)
      if (output_escape_fractions)
        clear(f_escs);                       // Escape fractions
      if (output_bin_escape_fractions)
        clear(bin_f_escs);                   // Bin escape fractions
      if (output_images)
        clear(images);                       // Surface brightness images
      if (output_cubes)
        clear(bin_images);                   // Bin surface brightness images
      if (output_radial_images)
        clear(radial_images);                // Radial surface brightness images
      if (output_radial_cubes)
        clear(bin_radial_images);            // Bin radial surface brightness images

      // Intrinsic cameras (mcrt)
      if (output_mcrt_emission) {
        if (output_images)
          clear(images_int);                 // Surface brightness images
        if (output_cubes)
          clear(bin_images_int);             // Bin surface brightness images
        if (output_radial_images)
          clear(radial_images_int);          // Radial surface brightness images
        if (output_radial_cubes)
          clear(bin_radial_images_int);      // Bin radial surface brightness images
      }

      // Attenuation cameras (mcrt)
      if (output_mcrt_attenuation) {
        if (output_escape_fractions)
          clear(f_escs_ext);                 // Escape fractions
        if (output_images)
          clear(images_ext);                 // Surface brightness images
        if (output_cubes)
          clear(bin_images_ext);             // Bin surface brightness images
        if (output_radial_images)
          clear(radial_images_ext);          // Radial surface brightness images
        if (output_radial_cubes)
          clear(bin_radial_images_ext);      // Bin radial surface brightness images
      }
    }
  }

  mcrt_timer.stop();

#if VORONOI && HAVE_CGAL
  if (output_refinement) {
    if (root)
      calculate_refinement();                // Output a refined version of the grid
    MPI_Barrier(MPI_COMM_WORLD);             // Avoid read/write race condition
  }
#endif
}
