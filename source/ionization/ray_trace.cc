/***************************
 * ionization/ray_trace.cc *
 ***************************

 * Ray tracer: Follow individual photon packets.

*/

#include "proto.h"
#include "photon.h" // Photon packets

double ran(); // Random number (from global seed)
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
int get_index_sorted(const int val, const vector<int> vec); // Get index of a value in a sorted vector
double escape_distance(const Vec3& dr, const Vec3& direction, const double radius); // Generalized spherical escape
#if spherical_escape
double spherical_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding sphere
double spherical_emission_distance(const Vec3& point, const Vec3& direction); // Distance to escape the emission sphere
#endif
#if box_escape
double box_escape_distance(const Vec3& point, const Vec3& direction); // Distance to escape the bounding box
double box_emission_distance(const Vec3& point, const Vec3& direction); // Distance to escape the emission box
#endif
bool avoid_cell(const int cell); // Avoid calculations for certain cells
Vec3 cell_center(const int cell); // Center of the cell
Vec3 random_point_in_cell(const int cell); // Uniform position in cell volume
int find_cell(const Vec3 point, int cell); // Cell index of a point
tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell); // Distance to leave the cell
void set_derived_dust_parameters(const double (&cosines)[n_dust_species]); // Set the derived dust parameters
int vec2pix_ring(const int nside, const Vec3& vec); // Healpix pixel index
double HI_2p_rate(const double T, const double ne, const double nHII); // HI two-photon emission rate (without g_nu)
// double HeI_2p_rate(const double T, const double ne, const double nHII); // HeI two-photon emission rate (without g_nu)
double HeII_2p_rate(const double T, const double ne, const double nHII); // HeII two-photon emission rate (without g_nu)

static const int ISOTROPIC = 1;

#if output_ion_radial_age_freq || output_ion_distance_age_freq
/* Update in-range flag. */
static inline void update_in_range(bool& in_range, const int neib) {
  if (!in_range)                             // Photon re-enters the grid
    in_range = true;                         // Flag as in-range
  else if (neib < 0)                         // Photon escapes the grid
    in_range = false;                        // Flag as out-of-range
}
#endif

static void (*setup_spectra)(IonPhoton *, const int); // Set the bin and spectral properties

#define EVAL_ZA(tab) mass * pow(10., (spec_Z_age.log_##tab(i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                                   + (spec_Z_age.log_##tab(i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)
#define MEAN_ZA(tab) mass_Ndot * pow(10., (spec_Z_age.log_##tab##s_Ndot[i](i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab##s_Ndot[i](i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                                        + (spec_Z_age.log_##tab##s_Ndot[i](i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab##s_Ndot[i](i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)
#define LIN_NUM_ZA(tab) ((spec_Z_age.tab##s_Ndot[i](i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.tab##s_Ndot[i](i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                       + (spec_Z_age.tab##s_Ndot[i](i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.tab##s_Ndot[i](i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)
#define LIN_DEN_ZA ((pow(10., spec_Z_age.log_Ndot(i_L_Z,i_L_age,i_bin))*frac_L_Z + pow(10., spec_Z_age.log_Ndot(i_R_Z,i_L_age,i_bin))*frac_R_Z) * frac_L_age \
                  + (pow(10., spec_Z_age.log_Ndot(i_L_Z,i_R_age,i_bin))*frac_L_Z + pow(10., spec_Z_age.log_Ndot(i_R_Z,i_R_age,i_bin))*frac_R_Z) * frac_R_age)

#define EVAL_FF(tab,sub) chop(n2V_##sub * pow(10., spec_ff_##sub.log_##tab[i_bin][i_L_T_ff]*frac_L_T_ff + spec_ff_##sub.log_##tab[i_bin][i_R_T_ff]*frac_R_T_ff))
#define MEAN_FF(tab,sub) chop(n2V_##sub##_Ndot * pow(10., spec_ff_##sub.log_##tab##s_Ndot[i][i_bin][i_L_T_ff]*frac_L_T_ff + spec_ff_##sub.log_##tab##s_Ndot[i][i_bin][i_R_T_ff]*frac_R_T_ff))
#define COS_NDOT_FF(sub) n2V_##sub * (spec_ff_##sub.cosines_Ndot[i][i_bin][i_L_T_ff]*frac_L_T_ff + spec_ff_##sub.cosines_Ndot[i][i_bin][i_R_T_ff]*frac_R_T_ff)
#define LIN_NDOT_FF(sub) n2V_##sub * (pow(10., spec_ff_##sub.log_Ndot[i_bin][i_L_T_ff])*frac_L_T_ff + pow(10., spec_ff_##sub.log_Ndot[i_bin][i_R_T_ff])*frac_R_T_ff)

#define EVAL_FB(tab,sub) chop(n2V_##sub##I * pow(10., spec_fb_##sub.log_##tab[i_bin][i_L_T_fb]*frac_L_T_fb + spec_fb_##sub.log_##tab[i_bin][i_R_T_fb]*frac_R_T_fb))
#define MEAN_FB(tab,sub) chop(n2V_##sub##I_Ndot * pow(10., spec_fb_##sub.log_##tab##s_Ndot[i][i_bin][i_L_T_fb]*frac_L_T_fb + spec_fb_##sub.log_##tab##s_Ndot[i][i_bin][i_R_T_fb]*frac_R_T_fb))
#define COS_NDOT_FB(sub) n2V_##sub##I * (spec_fb_##sub.cosines_Ndot[i][i_bin][i_L_T_fb]*frac_L_T_fb + spec_fb_##sub.cosines_Ndot[i][i_bin][i_R_T_fb]*frac_R_T_fb)
#define LIN_NDOT_FB(sub) n2V_##sub##I * (pow(10., spec_fb_##sub.log_Ndot[i_bin][i_L_T_fb])*frac_L_T_fb + pow(10., spec_fb_##sub.log_Ndot[i_bin][i_R_T_fb])*frac_R_T_fb)

#define EVAL_2P(tab,sub) chop(sub##_2p_n2V * spec_2p_##sub.tab[i_bin])
#define MEAN_2P(tab,sub) chop(sub##_2p_n2V_Ndot * spec_2p_##sub.tab##s_Ndot[i][i_bin])

/* Set the spectral properties of each bin based on the BC03-IMF-SOLAR and BC03-IMF-HALF-SOLAR models. */
static void setup_spectra_imf_solar(IonPhoton *p, const int star) {
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double age = age_star[star];         // Age of the star [Gyr]
  int edge = -1;
  if (age <= 1.00000001e-3)                  // Age minimum = 1 Myr
    edge = 0;
  else if (age >= 99.999999)                 // Age maximum = 100 Gyr
    edge = 50;
  else {
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    const double frac_R = d_age - f_age;     // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_age;                   // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    const double Ndot_int[3] = {             // Photon rate [photons/s]
      mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R), // HI
      mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R), // HeI
      mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R)}; // HeII
    const double bin_cdf_0 = Ndot_int[0];    // Frequency boosted cdf
    const double bin_cdf_1 = Ndot_int[1] * ion_stars.bin_boost[1] + bin_cdf_0;
    const double bin_cdf_tot = Ndot_int[2] * ion_stars.bin_boost[2] + bin_cdf_1;
    const double bin_comp = ran() * bin_cdf_tot;
    if (bin_comp < bin_cdf_0) {
      p->frequency_bin = 0;                  // Photon frequency bin
      p->Ndot = Ndot_int[0];                 // Source rate [photons/s]
      p->luminosity = mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // Luminosity [erg/s]
      const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
      p->sigma_HI = mass_Ndot * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Cross-sections [cm^2]
      p->sigma_HeI = 0.;                     // Always zero
      p->sigma_HeII = 0.;                    // Always zero
      if (output_photoheating) {             // Photoheating [erg cm^2]
        p->sigma_epsilon_HI = mass_Ndot * (ion_age.epsilon_HI_1[i_L]*ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                         + ion_age.epsilon_HI_1[i_R]*ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
        p->sigma_epsilon_HeI = 0.;           // Always zero
        p->sigma_epsilon_HeII = 0.;          // Always zero
      }
      p->kappas[0] = mass_Ndot * (ion_age.kappa_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.kappa_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Dust opacity [cm^2/g dust]
      p->albedos[0] = mass_Ndot * (ion_age.albedo_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.albedo_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering albedo
      p->cosines[0] = mass_Ndot * (ion_age.cosine_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.cosine_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering anisotropy
    } else if (bin_comp < bin_cdf_1) {
      p->frequency_bin = 1;                  // Photon frequency bin
      p->Ndot = Ndot_int[1];                 // Source rate [photons/s]
      p->luminosity = mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R); // Luminosity [erg/s]
      const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
      p->sigma_HI = mass_Ndot * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Cross-sections [cm^2]
      p->sigma_HeI = mass_Ndot * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
      p->sigma_HeII = 0.;                    // Always zero
      if (output_photoheating) {             // Photoheating [erg cm^2]
        p->sigma_epsilon_HI = mass_Ndot * (ion_age.epsilon_HI_2[i_L]*ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                         + ion_age.epsilon_HI_2[i_R]*ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
        p->sigma_epsilon_HeI = mass_Ndot * (ion_age.epsilon_HeI_2[i_L]*ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                          + ion_age.epsilon_HeI_2[i_R]*ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
        p->sigma_epsilon_HeII = 0.;          // Always zero
      }
      p->kappas[0] = mass_Ndot * (ion_age.kappa_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.kappa_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Dust opacity [cm^2/g dust]
      p->albedos[0] = mass_Ndot * (ion_age.albedo_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.albedo_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Scattering albedo
      p->cosines[0] = mass_Ndot * (ion_age.cosine_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.cosine_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R); // Scattering anisotropy
    } else {
      p->frequency_bin = 2;                  // Photon frequency bin
      p->Ndot = Ndot_int[2];                 // Source rate [photons/s]
      p->luminosity = mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R); // Luminosity [erg/s]
      const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
      p->sigma_HI = mass_Ndot * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Cross-sections [cm^2]
      p->sigma_HeI = mass_Ndot * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
      p->sigma_HeII = mass_Ndot * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
      if (output_photoheating) {             // Photoheating [erg cm^2]
        p->sigma_epsilon_HI = mass_Ndot * (ion_age.epsilon_HI_3[i_L]*ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                         + ion_age.epsilon_HI_3[i_R]*ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
        p->sigma_epsilon_HeI = mass_Ndot * (ion_age.epsilon_HeI_3[i_L]*ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                          + ion_age.epsilon_HeI_3[i_R]*ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
        p->sigma_epsilon_HeII = mass_Ndot * (ion_age.epsilon_HeII_3[i_L]*ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                           + ion_age.epsilon_HeII_3[i_R]*ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
      }
      p->kappas[0] = mass_Ndot * (ion_age.kappa_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.kappa_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Dust opacity [cm^2/g dust]
      p->albedos[0] = mass_Ndot * (ion_age.albedo_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.albedo_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Scattering albedo
      p->cosines[0] = mass_Ndot * (ion_age.cosine_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.cosine_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R); // Scattering anisotropy
    }
    return;                                  // Finished setup
  }

  // Age was out of range so use the bounding value
  const double Ndot_int[3] = {mass * ion_age.Ndot_HI[edge], mass * ion_age.Ndot_HeI[edge], mass * ion_age.Ndot_HeII[edge]};
  const double bin_cdf_0 = Ndot_int[0];      // Frequency boosted cdf
  const double bin_cdf_1 = Ndot_int[1] * ion_stars.bin_boost[1] + bin_cdf_0;
  const double bin_cdf_tot = Ndot_int[2] * ion_stars.bin_boost[2] + bin_cdf_1;
  const double bin_comp = ran() * bin_cdf_tot;
  if (bin_comp < bin_cdf_0) {
    p->frequency_bin = 0;
    p->Ndot = Ndot_int[0];                   // Source rate [photons/s]
    p->luminosity = mass * ion_age.L_HI[edge]; // Source luminosity [erg/s]
    p->sigma_HI = ion_age.sigma_HI_1[edge];  // Cross-sections [cm^2]
    p->sigma_HeI = 0.;                       // Always zero
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = ion_age.epsilon_HI_1[edge] * ion_age.sigma_HI_1[edge];
      p->sigma_epsilon_HeI = 0.;             // Always zero
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappas[0] = ion_age.kappa_HI[edge];   // Dust opacity [cm^2/g dust]
    p->albedos[0] = ion_age.albedo_HI[edge]; // Scattering albedo
    p->cosines[0] = ion_age.cosine_HI[edge]; // Scattering anisotropy
  } else if (bin_comp < bin_cdf_1) {
    p->frequency_bin = 1;
    p->Ndot = Ndot_int[1];                   // Source rate [photons/s]
    p->luminosity = mass * ion_age.L_HeI[edge]; // Source luminosity [erg/s]
    p->sigma_HI = ion_age.sigma_HI_2[edge];  // Cross-sections [cm^2]
    p->sigma_HeI = ion_age.sigma_HeI_2[edge];
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = ion_age.epsilon_HI_2[edge] * ion_age.sigma_HI_2[edge];
      p->sigma_epsilon_HeI = ion_age.epsilon_HeI_2[edge] * ion_age.sigma_HeI_2[edge];
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappas[0] = ion_age.kappa_HeI[edge];  // Dust opacity [cm^2/g dust]
    p->albedos[0] = ion_age.albedo_HeI[edge]; // Scattering albedo
    p->cosines[0] = ion_age.cosine_HeI[edge]; // Scattering anisotropy
  } else {
    p->frequency_bin = 2;
    p->Ndot = Ndot_int[2];                   // Source rate [photons/s]
    p->luminosity = mass * ion_age.L_HeII[edge]; // Source luminosity [erg/s]
    p->sigma_HI = ion_age.sigma_HI_3[edge];  // Cross-sections [cm^2]
    p->sigma_HeI = ion_age.sigma_HeI_3[edge];
    p->sigma_HeII = ion_age.sigma_HeII_3[edge];
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = ion_age.epsilon_HI_3[edge] * ion_age.sigma_HI_3[edge];
      p->sigma_epsilon_HeI = ion_age.epsilon_HeI_3[edge] * ion_age.sigma_HeI_3[edge];
      p->sigma_epsilon_HeII = ion_age.epsilon_HeII_3[edge] * ion_age.sigma_HeII_3[edge];
    }
    p->kappas[0] = ion_age.kappa_HeII[edge]; // Dust opacity [cm^2/g dust]
    p->albedos[0] = ion_age.albedo_HeII[edge]; // Scattering albedo
    p->cosines[0] = ion_age.cosine_HeII[edge]; // Scattering anisotropy
  }
}

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

/* Set the spectral properties of each bin based on the BC03-IMF, BPASS-IMF-135-100, or BPASS-CHAB-100 models. */
static void setup_spectra_imf(IonPhoton *p, const int star) {
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double Z = Z_star[star];             // Metallicity of the star
  const double age = age_star[star];         // Age of the star [Gyr]

  int i_L_Z = 0, i_L_age = 0;                // Lower interpolation index
  double frac_R_Z = 0., frac_R_age = 0.;     // Upper interpolation fraction
  if (Z >= 4e-2) {                           // Metallicity maximum = 0.04
    i_L_Z = 11;                              // 13 metallicity bins
    frac_R_Z = 1.;
  } else if (Z > 1e-5) {                     // Metallicity minimum = 10^-5
    const double logZ = log10(Z);            // Interpolate in log space
    while (logZ > logZ_BP[i_L_Z+1])
      i_L_Z++;                               // Search metallicity indices
    frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
  }
  if (age >= 100.) {                         // Age maximum = 100 Gyr
    i_L_age = 49;                            // 51 age bins
    frac_R_age = 1.;
  } else if (age > 1e-3) {                   // Age minimum = 1 Myr
    const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
    const double f_age = floor(d_age);       // Floor coordinate
    frac_R_age = d_age - f_age;              // Interpolation fraction (right)
    i_L_age = int(f_age);                    // Table age index (left)
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
  const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
  // Photon rates [photons/s]
  const double Ndot_int[3] = {
    mass * pow(10., (ion_Z_age.log_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (ion_Z_age.log_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age),
    mass * pow(10., (ion_Z_age.log_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (ion_Z_age.log_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age),
    mass * pow(10., (ion_Z_age.log_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                  + (ion_Z_age.log_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age)};
  // Luminosities [erg/s]
  const double bin_cdf_0 = Ndot_int[0];      // Frequency boosted cdf
  const double bin_cdf_1 = Ndot_int[1] * ion_stars.bin_boost[1] + bin_cdf_0;
  const double bin_cdf_tot = Ndot_int[2] * ion_stars.bin_boost[2] + bin_cdf_1;
  const double bin_comp = ran() * bin_cdf_tot;
  if (bin_comp < bin_cdf_0) {
    p->frequency_bin = 0;                    // Photon frequency bin
    p->Ndot = Ndot_int[0];                   // Source rate [photons/s]
    p->luminosity = mass * pow(10., (ion_Z_age.log_L_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Luminosity [erg/s]
    const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
    p->sigma_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                     + (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Cross-sections [cm^2]
    p->sigma_HeI = 0.;                       // Always zero
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeI = 0.;             // Always zero
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappas[0] = mass_Ndot * pow(10., (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
    p->albedos[0] = mass_Ndot * pow(10., (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering albedo
    p->cosines[0] = mass_Ndot * pow(10., (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering anisotropy
  } else if (bin_comp < bin_cdf_1) {
    p->frequency_bin = 1;                    // Photon frequency bin
    p->Ndot = Ndot_int[1];                   // Source rate [photons/s]
    p->luminosity = mass * pow(10., (ion_Z_age.log_L_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Luminosity [erg/s]
    const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
    p->sigma_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                     + (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Cross-sections [cm^2]
    p->sigma_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    p->sigma_HeII = 0.;                      // Always zero
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                + (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeII = 0.;            // Always zero
    }
    p->kappas[0] = mass_Ndot * pow(10., (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
    p->albedos[0] = mass_Ndot * pow(10., (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering albedo
    p->cosines[0] = mass_Ndot * pow(10., (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering anisotropy
  } else {
    p->frequency_bin = 2;                    // Photon frequency bin
    p->Ndot = Ndot_int[2];                   // Source rate [photons/s]
    p->luminosity = mass * pow(10., (ion_Z_age.log_L_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                  + (ion_Z_age.log_L_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Luminosity [erg/s]
    const double mass_Ndot = mass / p->Ndot; // Interpolation normalization
    p->sigma_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                     + (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Cross-sections [cm^2]
    p->sigma_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    p->sigma_HeII = mass_Ndot * pow(10., (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    if (output_photoheating) {               // Photoheating [erg cm^2]
      p->sigma_epsilon_HI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                               + (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeI = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                + (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      p->sigma_epsilon_HeII = mass_Ndot * pow(10., (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                 + (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
    }
    p->kappas[0] = mass_Ndot * pow(10., (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Dust opacity [cm^2/g dust]
    p->albedos[0] = mass_Ndot * pow(10., (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering albedo
    p->cosines[0] = mass_Ndot * pow(10., (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age); // Scattering anisotropy
  }
}

/* Set the spectral properties based on general (Z,age,bin) tables. */
static void setup_spectra_Z_age(IonPhoton *p, const int star) {
  const double mass = m_init_star[star];     // Initial mass of the star [Msun]
  const double Z = Z_star[star];             // Metallicity of the star
  const double age = age_star[star];         // Age of the star [Gyr]

  int i_L_Z = 0, i_L_age = 0;                // Lower interpolation index
  double frac_R_Z = 0., frac_R_age = 0.;     // Upper interpolation fraction
  if (Z >= spec_Z_age.max_Z) {               // Metallicity maximum
    i_L_Z = spec_Z_age.n_Zs - 2;             // Metallicity bins - 2
    frac_R_Z = 1.;
  } else if (Z > spec_Z_age.min_Z) {         // Metallicity minimum
    const double logZ = log10(Z);            // Interpolate in log space
    while (logZ > spec_Z_age.log_Z[i_L_Z+1])
      i_L_Z++;                               // Search metallicity indices
    frac_R_Z = (logZ - spec_Z_age.log_Z[i_L_Z]) / (spec_Z_age.log_Z[i_L_Z+1] - spec_Z_age.log_Z[i_L_Z]);
  }
  if (age >= spec_Z_age.max_age) {           // Age maximum
    i_L_age = spec_Z_age.n_ages - 2;         // Age bins - 2
    frac_R_age = 1.;
  } else if (age > spec_Z_age.min_age) {     // Age minimum
    const double logA = log10(age);          // Interpolate in log space
    while (logA > spec_Z_age.log_age[i_L_age+1])
      i_L_age++;                             // Search age indices
    frac_R_age = (logA - spec_Z_age.log_age[i_L_age]) / (spec_Z_age.log_age[i_L_age+1] - spec_Z_age.log_age[i_L_age]);
  }

  // Bilinear interpolation (based on left and right fractions)
  const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
  const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
  vector<double> Ndot_int, bin_cdf;          // Photon rates and boosted cdf [photons/s]
  Ndot_int.resize(n_bins); bin_cdf.resize(n_bins); // Allocate memory
  int i_bin = 0;                             // Frequency bin index
  bin_cdf[0] = Ndot_int[0] = EVAL_ZA(Ndot);  // First bin
  for (i_bin = 1; i_bin < n_bins; ++i_bin) {
    Ndot_int[i_bin] = EVAL_ZA(Ndot);         // Photon rate [photons/s]
    bin_cdf[i_bin] = bin_cdf[i_bin-1] + Ndot_int[i_bin] * ion_stars.bin_boost[i_bin]; // Cumulative distribution
  }
  const double bin_comp = ran() * bin_cdf[n_bins-1];
  i_bin = 0;                                 // Reset index
  while (bin_cdf[i_bin] <= bin_comp && i_bin < n_bins - 1)
    ++i_bin;                                 // Move "outward" ( --> )
  p->frequency_bin = i_bin;                  // Photon frequency bin
  p->Ndot = Ndot_int[i_bin];                 // Source rate [photons/s]
  p->luminosity = EVAL_ZA(L);                // Source luminosity [erg/s]
  const double mass_Ndot = mass / p->Ndot;   // Interpolation normalization
  // Cross-sections [cm^2]
  for (int ai = 0; ai < n_active_ions; ++ai) {
    auto& log_sigma_Ndot_ion = spec_Z_age.log_sigma_Ndot_ions[ai];
    double log_sigma_Ndot_val = (log_sigma_Ndot_ion(i_L_Z,i_L_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age
                              + (log_sigma_Ndot_ion(i_L_Z,i_R_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age;
    p->sigma_ions[ai] = chop(mass_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
  }
  // Photoheating [erg cm^2]
  if (output_photoheating) {
    for (int ai = 0; ai < n_active_ions; ++ai) {
      auto& log_sigma_L_ion = spec_Z_age.log_sigma_L_ions[ai];
      double log_sigma_L_val = (log_sigma_L_ion(i_L_Z,i_L_age,i_bin)*frac_L_Z + log_sigma_L_ion(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age
                             + (log_sigma_L_ion(i_L_Z,i_R_age,i_bin)*frac_L_Z + log_sigma_L_ion(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age;
      p->sigma_epsilon_ions[ai] = chop(mass_Ndot * pow(10., log_sigma_L_val) - p->sigma_ions[ai] * ions_erg[ai]);
    }
  }
  const double lin_norm = 1. / LIN_DEN_ZA;   // Linear normalization
  for (int i = 0; i < n_dust_species; ++i) {
    p->kappas[i] = MEAN_ZA(kappa);           // Dust opacity [cm^2/g dust]
    p->albedos[i] = MEAN_ZA(albedo);         // Scattering albedo
    p->cosines[i] = lin_norm * LIN_NUM_ZA(cosine); // Scattering anisotropy
  }
}

/* Set the spectral properties based on general temperature tables. */
static void setup_spectra_gas(IonPhoton *p, const int cell) {
  // Local cell properties
  const double T_i = T[cell];                // Temperature [K]
  const double logT = (free_free || free_bound) ? log10(T_i) : 0.; // Log(T)
  const double ne = x_e[cell] * n_H[cell];   // Electron density (n_e)
  const double ne_V = ne * VOLUME(cell);     // n_e V
  const double nHII = x_HII[cell] * n_H[cell]; // n_HII
  const double n2V_HII = nHII * ne_V;        // n_e n_HII V
  double n2V_HeII = 0., n2V_HeIII = 0.;      // n_e n_{HeII,HeIII} V
  if (read_HeI) {
    const double n2V_He = n_He[cell] * ne_V; // n_e n_He V
    if (read_HeII) {
      n2V_HeII = x_HeII[cell] * n2V_He;      // n_e n_HeII V
      n2V_HeIII = (1. - x_HeI[cell] - x_HeII[cell]) * n2V_He; // n_e n_HeIII V
    } else {
      n2V_HeII = (1. - x_HeI[cell]) * n2V_He; // n_e n_HeIII V
    }
  }

  vector<double> Ndot_int, bin_cdf;          // Photon rates and boosted cdf [photons/s]
  Ndot_int.resize(n_bins); bin_cdf.resize(n_bins); // Allocate memory
  for (int i_bin = 0; i_bin < n_bins; ++i_bin)
    Ndot_int[i_bin] = 0.;

  // Interpolation indices and fractions
  int i_L_T_ff = 0, i_L_T_fb = 0;            // Lower indices
  int i_R_T_ff = 1, i_R_T_fb = 1;            // Upper indices
  double frac_L_T_ff = 1., frac_L_T_fb = 1.; // Lower fractions
  double frac_R_T_ff = 0., frac_R_T_fb = 0.; // Upper fractions
  double n2V_Z1 = 0., n2V_Z2 = 0.;           // n_e n_ions V (Z = 1,2)
  double HI_2p_n2V = 0., HeII_2p_n2V = 0.;   // Two-photon rates
  double cosines_Ndot[n_dust_species];       // Scattering anisotropy
  double linear_Ndot = 0.;                   // Non-log normalizations

  // Free-free continuum emission (HII, HeII, HeIII)
  if (free_free) {
    // Free-free emission (Z = 1)
    if (T_i >= spec_ff_Z1.max_T) {           // Temperature maximum
      i_L_T_ff = spec_ff_Z1.n_Ts - 2;        // Temperature bins - 2
      frac_R_T_ff = 1.;
    } else if (T_i > spec_ff_Z1.min_T) {     // Temperature minimum
      i_L_T_ff = floor(spec_ff_Z1.inv_dlogT * (logT - spec_ff_Z1.min_logT));
      frac_R_T_ff = spec_ff_Z1.inv_dlogT * (logT - spec_ff_Z1.min_logT) - double(i_L_T_ff);
    }

    // Linear interpolation (based on left and right fractions)
    i_R_T_ff = i_L_T_ff + 1;
    frac_L_T_ff = 1. - frac_R_T_ff;
    n2V_Z1 = n2V_HII + n2V_HeII;             // n_e n_ions V (Z = 1)
    // Free-free emission (Z = 1)
    for (int i_bin = 0; i_bin < n_bins; ++i_bin)
      Ndot_int[i_bin] += EVAL_FF(Ndot,Z1);   // Interpolation (Z = 1) [photons/s]
    // Free-free emission (Z = 2)
    if (read_HeII) {
      n2V_Z2 = n2V_HeIII;                    // n_e n_ions V (Z = 2)
      for (int i_bin = 0; i_bin < n_bins; ++i_bin)
        Ndot_int[i_bin] += EVAL_FF(Ndot,Z2); // Interpolation (Z = 2) [photons/s]
    }
  }

  // Free-bound continuum emission (HII->HI, HeII->HeI, HeIII->HeII)
  if (free_bound) {
    if (T_i >= spec_fb_HI.max_T) {           // Temperature maximum
      i_L_T_fb = spec_fb_HI.n_Ts - 2;        // Temperature bins - 2
      frac_R_T_fb = 1.;
    } else if (T_i > spec_fb_HI.min_T) {     // Temperature minimum
      i_L_T_fb = floor(spec_fb_HI.inv_dlogT * (logT - spec_fb_HI.min_logT));
      frac_R_T_fb = spec_fb_HI.inv_dlogT * (logT - spec_fb_HI.min_logT) - double(i_L_T_fb);
    }

    // Linear interpolation (based on left and right fractions)
    i_R_T_fb = i_L_T_fb + 1;
    frac_L_T_fb = 1. - frac_R_T_fb;
    // Free-bound emission (HII->HI)
    for (int i_bin = 0; i_bin < n_bins; ++i_bin)
      Ndot_int[i_bin] += EVAL_FB(Ndot,HI);   // Interpolation (HI) [photons/s]
    // Free-bound emission (HeII->HeI)
    if (read_HeI) {
      for (int i_bin = 0; i_bin < n_bins; ++i_bin)
        Ndot_int[i_bin] += EVAL_FB(Ndot,HeI); // Interpolation (Z = 2) [photons/s]
    }
    // Free-bound emission (HeIII->HeII)
    if (read_HeII) {
      for (int i_bin = 0; i_bin < n_bins; ++i_bin)
        Ndot_int[i_bin] += EVAL_FB(Ndot,HeII); // Interpolation (Z = 2) [photons/s]
    }
  }

  // Two-photon continuum emission (HI, HeI, HeII)
  if (two_photon) {
    // Two-photon emission (HII->HI)
    HI_2p_n2V = n2V_HII * HI_2p_rate(T_i, ne, nHII);
    for (int i_bin = 0; i_bin < n_bins; ++i_bin)
      Ndot_int[i_bin] += EVAL_2P(Ndot,HI);   // Evaluation (HI) [photons/s]
    // Two-photon emission (HeII->HeI)
    // if (read_HeI) {
    //   HeI_2p_n2V = n2V_HeII * HeI_2p_rate(T_i, ne, nHII);
    //   for (int i_bin = 0; i_bin < n_bins; ++i_bin)
    //     Ndot_int[i_bin] += EVAL_2P(Ndot,HeI); // Evaluation (HeI) [photons/s]
    // }
    // Two-photon emission (HeIII->HeII)
    if (read_HeII) {
      HeII_2p_n2V = n2V_HeIII * HeII_2p_rate(T_i, ne, nHII);
      for (int i_bin = 0; i_bin < n_bins; ++i_bin)
        Ndot_int[i_bin] += EVAL_2P(Ndot,HeII); // Evaluation (HeII) [photons/s]
    }
  }

  // Determine the photon frequency bin and set the spectral properties
  bin_cdf[0] = Ndot_int[0];                  // First bin
  for (int i_bin = 1; i_bin < n_bins; ++i_bin)
    bin_cdf[i_bin] = bin_cdf[i_bin-1] + Ndot_int[i_bin] * ion_gas.bin_boost[i_bin]; // Cumulative distribution
  const double bin_comp = ran() * bin_cdf[n_bins-1];
  int i_bin = 0;
  while (bin_cdf[i_bin] <= bin_comp && i_bin < n_bins - 1)
    ++i_bin;                                 // Move "outward" ( --> )
  p->frequency_bin = i_bin;                  // Photon frequency bin
  p->Ndot = Ndot_int[i_bin];                 // Source rate [photons/s]
  p->luminosity = 0.;                        // Luminosity [erg/s]
  for (int ai = 0; ai < n_active_ions; ++ai) {
    p->sigma_ions[ai] = 0.;                  // Cross-sections [cm^2]
    p->sigma_epsilon_ions[ai] = 0.;          // Photoheating [erg cm^2]
  }
  for (int i = 0; i < n_dust_species; ++i) {
    p->kappas[i] = 0.;                       // Dust opacity [cm^2/g dust]
    p->albedos[i] = 0.;                      // Dust scattering albedo
    p->cosines[i] = 0.;                      // Dust scattering anisotropy
  }
  const double inv_Ndot = 1. / p->Ndot;      // Normalization factor

  // Free-free continuum emission (HII, HeII, HeIII)
  if (free_free) {
    const double n2V_Z1_Ndot = n2V_Z1 * inv_Ndot; // Normalized coefficient
    // Free-free emission (Z = 1)
    p->luminosity += EVAL_FF(L,Z1);          // Source luminosity [erg/s]
    // Cross-sections [cm^2]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      auto& log_sigma_Ndot_ion = spec_ff_Z1.log_sigma_Ndot_ions[ai];
      double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T_ff]*frac_L_T_ff + log_sigma_Ndot_ion[i_bin][i_R_T_ff]*frac_R_T_ff;
      p->sigma_ions[ai] += chop(n2V_Z1_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
    }
    // Photoheating [erg cm^2]
    if (output_photoheating) {
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& log_sigma_L_ion = spec_ff_Z1.log_sigma_L_ions[ai];
        double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T_ff]*frac_L_T_ff + log_sigma_L_ion[i_bin][i_R_T_ff]*frac_R_T_ff;
        p->sigma_epsilon_ions[ai] += n2V_Z1_Ndot * pow(10., log_sigma_L_val); // Subtract ionization energy later
      }
    }
    for (int i = 0; i < n_dust_species; ++i) {
      p->kappas[i] += MEAN_FF(kappa,Z1);     // Dust opacity [cm^2/g dust]
      p->albedos[i] += MEAN_FF(albedo,Z1);   // Dust scattering albedo
      cosines_Ndot[i] += COS_NDOT_FF(Z1);    // Dust scattering anisotropy
    }
    linear_Ndot += LIN_NDOT_FF(Z1);          // Linear photon rate [photons/s]

    // Free-free emission (Z = 2)
    if (read_HeII) {
      const double n2V_Z2_Ndot = n2V_Z2 * inv_Ndot; // Normalized coefficient
      p->luminosity += EVAL_FF(L,Z2);        // Luminosities [erg/s]
      // Cross-sections [cm^2]
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& log_sigma_Ndot_ion = spec_ff_Z2.log_sigma_Ndot_ions[ai];
        double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T_ff]*frac_L_T_ff + log_sigma_Ndot_ion[i_bin][i_R_T_ff]*frac_R_T_ff;
        p->sigma_ions[ai] += chop(n2V_Z2_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
      }
      // Photoheating [erg cm^2]
      if (output_photoheating) {
        for (int ai = 0; ai < n_active_ions; ++ai) {
          auto& log_sigma_L_ion = spec_ff_Z2.log_sigma_L_ions[ai];
          double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T_ff]*frac_L_T_ff + log_sigma_L_ion[i_bin][i_R_T_ff]*frac_R_T_ff;
          p->sigma_epsilon_ions[ai] += n2V_Z2_Ndot * pow(10., log_sigma_L_val); // Subtract ionization energy later
        }
      }
      for (int i = 0; i < n_dust_species; ++i) {
        p->kappas[i] += MEAN_FF(kappa,Z2);   // Dust opacity [cm^2/g dust]
        p->albedos[i] += MEAN_FF(albedo,Z2); // Dust scattering albedo
        cosines_Ndot[i] += COS_NDOT_FF(Z2);  // Dust scattering anisotropy
      }
      linear_Ndot += LIN_NDOT_FF(Z2);        // Linear photon rate [photons/s]
    }
  }

  // Free-bound continuum emission (HII->HI, HeII->HeI, HeIII->HeII)
  if (free_bound) {
    const double n2V_HII_Ndot = n2V_HII * inv_Ndot; // Normalized coefficient
    // Free-bound emission (HII->HI)
    p->luminosity += EVAL_FB(L,HI);          // Source luminosity [erg/s]
    // Cross-sections [cm^2]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      auto& log_sigma_Ndot_ion = spec_fb_HI.log_sigma_Ndot_ions[ai];
      double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T_fb]*frac_L_T_fb + log_sigma_Ndot_ion[i_bin][i_R_T_fb]*frac_R_T_fb;
      p->sigma_ions[ai] += chop(n2V_HII_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
    }
    // Photoheating [erg cm^2]
    if (output_photoheating) {
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& log_sigma_L_ion = spec_fb_HI.log_sigma_L_ions[ai];
        double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T_fb]*frac_L_T_fb + log_sigma_L_ion[i_bin][i_R_T_fb]*frac_R_T_fb;
        p->sigma_epsilon_ions[ai] += n2V_HII_Ndot * pow(10., log_sigma_L_val); // Subtract ionization energy later
      }
    }
    for (int i = 0; i < n_dust_species; ++i) {
      p->kappas[i] += MEAN_FB(kappa,HI);     // Dust opacity [cm^2/g dust]
      p->albedos[i] += MEAN_FB(albedo,HI);   // Dust scattering albedo
      cosines_Ndot[i] += COS_NDOT_FB(HI);    // Dust scattering anisotropy
    }
    linear_Ndot += LIN_NDOT_FB(HI);          // Linear photon rate [photons/s]

    // Free-bound emission (HeII->HeI)
    if (read_HeI) {
      const double n2V_HeII_Ndot = n2V_HeII * inv_Ndot; // Normalized coefficient
      p->luminosity += EVAL_FB(L,HeI);       // Luminosities [erg/s]
      // Cross-sections [cm^2]
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& log_sigma_Ndot_ion = spec_fb_HeI.log_sigma_Ndot_ions[ai];
        double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T_fb]*frac_L_T_fb + log_sigma_Ndot_ion[i_bin][i_R_T_fb]*frac_R_T_fb;
        p->sigma_ions[ai] += chop(n2V_HeII_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
      }
      // Photoheating [erg cm^2]
      if (output_photoheating) {
        for (int ai = 0; ai < n_active_ions; ++ai) {
          auto& log_sigma_L_ion = spec_fb_HeI.log_sigma_L_ions[ai];
          double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T_fb]*frac_L_T_fb + log_sigma_L_ion[i_bin][i_R_T_fb]*frac_R_T_fb;
          p->sigma_epsilon_ions[ai] += n2V_HeII_Ndot * pow(10., log_sigma_L_val); // Subtract ionization energy later
        }
      }
      for (int i = 0; i < n_dust_species; ++i) {
        p->kappas[i] += MEAN_FB(kappa,HeI);  // Dust opacity [cm^2/g dust]
        p->albedos[i] += MEAN_FB(albedo,HeI); // Dust scattering albedo
        cosines_Ndot[i] += COS_NDOT_FB(HeI); // Dust scattering anisotropy
      }
      linear_Ndot += LIN_NDOT_FB(HeI);       // Linear photon rate [photons/s]
    }

    // Free-bound emission (HeIII->HeII)
    if (read_HeII) {
      const double n2V_HeIII_Ndot = n2V_HeIII * inv_Ndot; // Normalized coefficient
      p->luminosity += EVAL_FB(L,HeII);      // Luminosities [erg/s]
      // Cross-sections [cm^2]
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& log_sigma_Ndot_ion = spec_fb_HeII.log_sigma_Ndot_ions[ai];
        double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T_fb]*frac_L_T_fb + log_sigma_Ndot_ion[i_bin][i_R_T_fb]*frac_R_T_fb;
        p->sigma_ions[ai] += chop(n2V_HeIII_Ndot * pow(10., log_sigma_Ndot_val), sigma_chop);
      }
      // Photoheating [erg cm^2]
      if (output_photoheating) {
        for (int ai = 0; ai < n_active_ions; ++ai) {
          auto& log_sigma_L_ion = spec_fb_HeII.log_sigma_L_ions[ai];
          double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T_fb]*frac_L_T_fb + log_sigma_L_ion[i_bin][i_R_T_fb]*frac_R_T_fb;
          p->sigma_epsilon_ions[ai] += n2V_HeIII_Ndot * pow(10., log_sigma_L_val); // Subtract ionization energy later
        }
      }
      for (int i = 0; i < n_dust_species; ++i) {
        p->kappas[i] += MEAN_FB(kappa,HeII); // Dust opacity [cm^2/g dust]
        p->albedos[i] += MEAN_FB(albedo,HeII); // Dust scattering albedo
        cosines_Ndot[i] += COS_NDOT_FB(HeII); // Dust scattering anisotropy
      }
      linear_Ndot += LIN_NDOT_FB(HeII);      // Linear photon rate [photons/s]
    }
  }

  // Two-photon continuum emission (HII->HI, HeII->HeI, HeIII->HeII)
  if (two_photon) {
    const double HI_2p_n2V_Ndot = HI_2p_n2V * inv_Ndot; // Normalized coefficient
    // Two-photon emission (HII->HI)
    p->luminosity += EVAL_2P(L,HI);          // Source luminosity [erg/s]
    // Cross-sections [cm^2]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      auto& sigma_Ndot_ion = spec_2p_HI.sigma_Ndot_ions[ai];
      p->sigma_ions[ai] += chop(HI_2p_n2V_Ndot * sigma_Ndot_ion[i_bin], sigma_chop);
    }
    // Photoheating [erg cm^2]
    if (output_photoheating) {
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& sigma_L_ion = spec_2p_HI.sigma_L_ions[ai];
        p->sigma_epsilon_ions[ai] += HI_2p_n2V_Ndot * sigma_L_ion[i_bin]; // Subtract ionization energy later
      }
    }
    for (int i = 0; i < n_dust_species; ++i) {
      p->kappas[i] += MEAN_2P(kappa,HI);     // Dust opacity [cm^2/g dust]
      p->albedos[i] += MEAN_2P(albedo,HI);   // Dust scattering albedo
      cosines_Ndot[i] += EVAL_2P(cosines_Ndot[i],HI); // Dust scattering anisotropy
    }
    linear_Ndot += EVAL_2P(Ndot,HI);         // Linear photon rate [photons/s]

    // Two-photon emission (HeII->HeI)
    // if (read_HeI) {
    //   const double HeI_2p_n2V_Ndot = HeI_2p_n2V * inv_Ndot; // Normalized coefficient
    //   p->luminosity += EVAL_2P(L,HeI);       // Luminosities [erg/s]
    //   // Cross-sections [cm^2]
    //   for (int ai = 0; ai < n_active_ions; ++ai) {
    //     auto& sigma_Ndot_ion = spec_2p_HeI.sigma_Ndot_ions[ai];
    //     p->sigma_ions[ai] += chop(HeI_2p_n2V_Ndot * sigma_Ndot_ion[i_bin], sigma_chop);
    //   }
    //   // Photoheating [erg cm^2]
    //   if (output_photoheating) {
    //     for (int ai = 0; ai < n_active_ions; ++ai) {
    //       auto& sigma_L_ion = spec_2p_HeI.sigma_L_ions[ai];
    //       p->sigma_epsilon_ions[ai] += HeI_2p_n2V_Ndot * sigma_L_ion[i_bin]; // Subtract ionization energy later
    //     }
    //   }
    //   for (int i = 0; i < n_dust_species; ++i) {
    //     p->kappas[i] += MEAN_2P(kappa,HeI);        // Dust opacity [cm^2/g dust]
    //     p->albedos[i] += MEAN_2P(albedo,HeI);      // Dust scattering albedo
    //     cosines_Ndot[i] += EVAL_2P(cosines_Ndot[i],HeI); // Dust scattering anisotropy rate [photons/s]
    //   }
    //   linear_Ndot += EVAL_2P(Ndot,HeI);      // Linear photon rate [photons/s]
    // }

    // Two-photon emission (HeIII->HeII)
    if (read_HeII) {
      const double HeII_2p_n2V_Ndot = HeII_2p_n2V * inv_Ndot; // Normalized coefficient
      p->luminosity += EVAL_2P(L,HeII);      // Luminosities [erg/s]
      // Cross-sections [cm^2]
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& sigma_Ndot_ion = spec_2p_HeII.sigma_Ndot_ions[ai];
        p->sigma_ions[ai] += chop(HeII_2p_n2V_Ndot * sigma_Ndot_ion[i_bin], sigma_chop);
      }
      // Photoheating [erg cm^2]
      if (output_photoheating) {
        for (int ai = 0; ai < n_active_ions; ++ai) {
          auto& sigma_L_ion = spec_2p_HeII.sigma_L_ions[ai];
          p->sigma_epsilon_ions[ai] += HeII_2p_n2V_Ndot * sigma_L_ion[i_bin]; // Subtract ionization energy later
        }
      }
      for (int i = 0; i < n_dust_species; ++i) {
        p->kappas[i] += MEAN_2P(kappa,HeII); // Dust opacity [cm^2/g dust]
        p->albedos[i] += MEAN_2P(albedo,HeII); // Dust scattering albedo
        cosines_Ndot[i] += EVAL_2P(cosines_Ndot[i],HeII); // Dust scattering anisotropy rate [photons/s]
      }
      linear_Ndot += EVAL_2P(Ndot,HeII);     // Linear photon rate [photons/s]
    }
  }
  for (int i = 0; i < n_dust_species; ++i)
    p->cosines[i] = safe_division(cosines_Ndot[i], linear_Ndot); // Dust scattering anisotropy

  // Subtract ionization energy to photoheating cross-sections
  if (output_photoheating) {
    for (int ai = 0; ai < n_active_ions; ++ai)
      p->sigma_epsilon_ions[ai] = chop(p->sigma_epsilon_ions[ai] - p->sigma_ions[ai] * ions_erg[ai]);
  }
}

/* Set the AGN spectral properties. */
static void setup_AGN_spectra(IonPhoton *p) {
  p->source_type = AGN;                      // AGN source
  int i_bin = 0;                             // Photon frequency bin
  const double bin_comp = ran();             // Draw a random bin
  while (bin_comp > bin_cdf_AGN[i_bin])
    i_bin++;                                 // Search frequency bins
  p->frequency_bin = i_bin;                  // Photon frequency bin
  p->source_weight = mixed_ion_weights[AGN] * bin_weight_AGN[i_bin] / double(n_photons); // Weight boosting
  p->Ndot = bin_Ndot_AGN[i_bin];             // Source rate [photons/s]
  p->luminosity = bin_L_AGN[i_bin];          // Source luminosity [erg/s]
  p->sigma_HI = sigma_HI_AGN[i_bin];         // Cross-sections [cm^2]
  p->sigma_HeI = sigma_HeI_AGN[i_bin];
  p->sigma_HeII = sigma_HeII_AGN[i_bin];
  p->sigma_epsilon_HI = sigma_L_HI_AGN[i_bin]; // Photoheating [erg cm^2]
  p->sigma_epsilon_HeI = sigma_L_HeI_AGN[i_bin];
  p->sigma_epsilon_HeII = sigma_L_HeII_AGN[i_bin];
  p->kappas[0] = kappa_AGN[i_bin];           // Dust opacity [cm^2/g dust]
  p->albedos[0] = albedo_AGN[i_bin];         // Scattering albedo
  p->cosines[0] = cosine_AGN[i_bin];         // Scattering anisotropy
}

/* Set generic copy spectral properties. */
static inline void setup_copy_spectra(IonPhoton *p, IonBinData& ion, const int type) {
  p->source_type = type;                     // Source type
  int i_bin = 0;                             // Photon frequency bin
  const double bin_comp = ran();             // Draw a random bin
  while (bin_comp > ion.bin_cdf[i_bin])
    i_bin++;                                 // Search frequency bins
  p->frequency_bin = i_bin;                  // Photon frequency bin
  p->source_weight = mixed_ion_weights[type] * ion.bin_weight[i_bin] / double(n_photons); // Weight boosting
  p->Ndot = ion.bin_Ndot[i_bin];             // Source rate [photons/s]
  p->luminosity = ion.bin_L[i_bin];          // Source luminosity [erg/s]
  for (int ai = 0; ai < n_active_ions; ++ai)
    p->sigma_ions[ai] = ion.sigma_ions[ai][i_bin]; // Cross-sections [cm^2]
  if (output_photoheating) {
    for (int ai = 0; ai < n_active_ions; ++ai)
      p->sigma_epsilon_ions[ai] = ion.sigma_epsilon_ions[ai][i_bin]; // Photoheating [erg cm^2]
  }
  for (int i = 0; i < n_dust_species; ++i) {
    p->kappas[i] = ion.kappas[i][i_bin];     // Dust opacity [cm^2/g dust]
    p->albedos[i] = ion.albedos[i][i_bin];   // Scattering albedo
    p->cosines[i] = ion.cosines[i][i_bin];   // Scattering anisotropy
  }
}

/* Set the point spectral properties. */
static void setup_point_spectra(IonPhoton *p) {
  setup_copy_spectra(p, ion_point, POINT);   // Copy point ionization data
}

/* Set the plane spectral properties. */
static void setup_plane_spectra(IonPhoton *p) {
  setup_copy_spectra(p, ion_plane, PLANE);   // Copy plane ionization data
}

/* Assign the setup_spectra function based on the source model. */
void assign_spectra() {
  // Assign luminosity and opacity functions
  if (source_file_Z_age != "") {             // General (Z,age,bin) tables
    setup_spectra = setup_spectra_Z_age;
  } else if (source_model == "GMC") {        // RT is based on the GMC model
    error("GMC not implemented.");
  } else if (source_model == "MRT") {        // RT is based on the MRT model
    error("MRT not implemented.");
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR") {
    setup_spectra = setup_spectra_imf_solar;
  } else if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    setup_spectra = setup_spectra_imf;
  }
}

/* Assign the group ID based on the source type and index. */
static inline int get_group_id(const int source_type, const int source_id) {
  if (source_type == STARS) {
    return group_id_star[source_id];         // Star group ID
  } else if (source_type == CELLS) {
    return group_id_cell[source_id];         // Cell group ID
  } else {
    return -3;                               // Not assigned
  }
}

/* Assign the subhalo ID based on the source type and index. */
static inline int get_subhalo_id(const int source_type, const int source_id) {
  if (source_type == STARS) {
    return subhalo_id_star[source_id];       // Star subhalo ID
  } else if (source_type == CELLS) {
    return subhalo_id_cell[source_id];       // Cell subhalo ID
  } else {
    return -3;                               // Not assigned
  }
}

/* Assign the group ID index based on the source type and index. */
static inline int get_group_index(const int source_type, const int source_id) {
  const int gid = get_group_id(source_type, source_id); // Group ID
  return get_index_sorted(gid, group_id);    // Group ID index
}

/* Assign the subhalo ID index based on the source type and index. */
static inline int get_subhalo_index(const int source_type, const int source_id) {
  const int sid = get_subhalo_id(source_type, source_id); // Subhalo ID
  return get_index_sorted(sid, subhalo_id);  // Subhalo ID index
}

/* Assign the unfiltered group ID index based on the source type and index. */
static inline int get_ugroup_index(const int source_type, const int source_id) {
  const int gid = get_group_id(source_type, source_id); // Unfiltered group ID
  return get_index_sorted(gid, ugroup_id);   // Unfiltered group ID index
}

/* Assign the unfiltered subhalo ID index based on the source type and index. */
static inline int get_usubhalo_index(const int source_type, const int source_id) {
  const int sid = get_subhalo_id(source_type, source_id); // Unfiltered subhalo ID
  return get_index_sorted(sid, usubhalo_id); // Unfiltered subhalo ID index
}

IonPhoton::IonPhoton() {
  // Draw the source that the photon comes from (stars, cells, AGN, point, plane)
  const double mixed_comp = ran();
  if (mixed_comp < mixed_ion_cdf[STARS]) {   // Star-based emission
    const double ran_comp = ran();           // Draw the star that the photon comes from
    int i = j_map_stars[int(ran_comp * double(n_cdf_stars))]; // Start search from lookup table
    while (j_cdf_stars[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf_stars[i] <= ran_comp && i < n_stars - 1)
      ++i;                                   // Move "outward" ( --> )
    setup_spectra(this, i);                  // Set the bin and spectral properties
    source_id = i;                           // Star index
    source_type = STARS;                     // Star source
    source_weight = mixed_ion_weights[STARS] * j_weights_stars[i] * ion_stars.bin_weight[frequency_bin]; // Star-based luminosity boosting
    current_cell = find_cell(r_star[i], 0);  // Cell index hosting the star (cell = 0)
    position = r_star[i];                    // Set the photon position to the star position
    if (output_stars) {
      #pragma omp atomic
      source_weight_stars[i] += source_weight; // Add photon weight at emission to the star
    }
  } else if (mixed_comp < mixed_ion_cdf[CELLS]) { // Cell-based emission (free_free)
    const double ran_comp = ran();           // Draw the cell that the photon comes from
    int i = j_map_cells[int(ran_comp * double(n_cdf_cells))]; // Start search from lookup table
    while (j_cdf_cells[i] > ran_comp && i > 0)
      --i;                                   // Move "inward"  ( <-- )
    while (j_cdf_cells[i] <= ran_comp && i < n_cells - 1)
      ++i;                                   // Move "outward" ( --> )
    setup_spectra_gas(this, i);              // Set the bin and spectral properties
    source_id = i;                           // Cell index
    source_type = CELLS;                     // Cell source
    source_weight = mixed_ion_weights[CELLS] * j_weights_cells[i] * ion_gas.bin_weight[frequency_bin]; // Cell-based luminosity boosting
    current_cell = i;                        // Cell index assigned directly
    position = random_point_in_cell(current_cell); // Uniform distribution within the cell volume
    #if spherical_escape                     // Ensure emission is within spherical region
      if (norm2(position - escape_center) > emission_radius2) { // Point is outside emission sphere
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 sphere_direction = position - point; // Outward facing
        sphere_direction.normalize();        // Normalize the direction
        const double dl = spherical_emission_distance(point, sphere_direction);
        position = point + sphere_direction * dl; // Move photon to a valid position on the sphere
      }
    #endif
    #if box_escape
      if (position.x < emission_box[0].x || position.x > emission_box[1].x ||
          position.y < emission_box[0].y || position.y > emission_box[1].y ||
          position.z < emission_box[0].z || position.z > emission_box[1].z) {
        const Vec3 point = cell_center(i);   // Cell center position
        Vec3 box_direction = position - point; // Outward facing
        box_direction.normalize();           // Normalize the direction
        const double dl = box_emission_distance(point, box_direction);
        position = point + box_direction * dl; // Move photon to a valid position on the sphere
      }
    #endif
    if (output_cells) {
      #pragma omp atomic
      source_weight_cells[i] += source_weight; // Add photon weight at emission to the cell
    }
  } else if (mixed_comp < mixed_ion_cdf[AGN]) { // AGN emission
    current_cell = source_id = i_AGN;        // Known cell index (only one AGN id)
    position = r_AGN;                        // Known position
    setup_AGN_spectra(this);                 // Set the AGN bin and spectral properties
  } else if (mixed_comp < mixed_ion_cdf[POINT]) { // Point source emission
    current_cell = source_id = i_point;      // Known cell index (only one point id)
    position = r_point;                      // Known position
    setup_point_spectra(this);               // Set the point bin and spectral properties
  } else if (mixed_comp < mixed_ion_cdf[PLANE]) { // Plane source emission
    double x1, x2;
    do {
      x1 = 2. * ran() - 1.;                  // Random draws in [-1,1]
      x2 = 2. * ran() - 1.;
    } while (plane_beam && x1*x1 + x2*x2 >= 1.);
    const double p1 = plane_center_1 + x1 * plane_radius_1; // Positions
    const double p2 = plane_center_2 + x2 * plane_radius_2;
    switch (plane_type) {                    // Assign plane properties
      case PosX:                             // +x direction
        position = {plane_position, p1, p2};
        direction = {1., 0., 0.};
        break;
      case NegX:                             // -x direction
        position = {plane_position, p1, p2};
        direction = {-1., 0., 0.};
        break;
      case PosY:                             // +y direction
        position = {p1, plane_position, p2};
        direction = {0., 1., 0.};
        break;
      case NegY:                             // -y direction
        position = {p1, plane_position, p2};
        direction = {0., -1., 0.};
        break;
      case PosZ:                             // +z direction
        position = {p1, p2, plane_position};
        direction = {0., 0., 1.};
        break;
      case NegZ:                             // -z direction
        position = {p1, p2, plane_position};
        direction = {0., 0., -1.};
        break;
      default:
        error("Unrecognized plane direction type.");
    }
    current_cell = source_id = find_cell(position, 0); // Cell index hosting the photon (cell = 0)
    setup_plane_spectra(this);               // Set the plane bin and spectral properties
  } else {
    error("Unexpected source probability behavior");
  }
  if (avoid_cell(current_cell))
    error("Attempting to insert photon in an invalid cell");
  for (int i = 0; i < n_dust_species; ++i) {
    if (kappas[i] < 0.)                      // Validate kappa_dust range
      error("kappa must be >= 0 but got " + to_string(kappas[i]));
    if (albedos[i] < 0. || albedos[i] > 1.)  // Validate albedo range
      error("albedo must be in the range [0,1] but got " + to_string(albedos[i]));
    if (cosines[i] < -1. || cosines[i] > 1.) // Validate cosine range
      error("cosine must be in the range [-1,1] but got " + to_string(cosines[i]));
    if (fabs(cosines[i]) < 1e-6)
      cosines[i] = (cosines[i] < 0.) ? -1e-6 : 1e-6; // Avoid division by zero
  }

  weight = source_weight;                    // Set the initial photon weight
  n_scat = 0;                                // Number of scattering events
  source_position = position;                // Remember position at emission
  dust_weight = 0.;                          // Set the initial dust weight
  for (int ai = 0; ai < n_active_ions; ++ai)
    ion_weight[ai] = 0.;                     // Initial species weights
  dist_abs = 0.;                             // Set the initial photon absorption distance [cm]
  l_tot = 0.;                                // Set the initial total path length [cm]
  set_derived_dust_parameters(cosines);      // Set the derived dust parameters

  // Set up ionization statistics histogram bins
  #if output_ion_cell_age_freq
    if (source_type == STARS) {
      // Calculate age bin from stellar age [Gyr]
      caf_age_bin = ion_cell_age_freq.find_age_bin(age_star[source_id]);
      // Calculate frequency bin
      caf_freq_bin = ion_cell_age_freq.find_freq_bin(frequency_bin);
    } else {
      caf_age_bin = caf_freq_bin = OUTSIDE;  // Default is out of range
    }
    caf_in_range = (caf_age_bin >= 0 && caf_freq_bin >= 0);
  #endif
  #if output_ion_radial_age_freq
    if (source_type == STARS) {
      // Calculate radial bin from radial position [cm]
      const double raf_radius = (position - radial_center).norm();
      raf_radial_bin = ion_radial_age_freq.find_radial_bin(raf_radius);
      // Calculate age bin from stellar age [Gyr]
      raf_age_bin = ion_radial_age_freq.find_age_bin(age_star[source_id]);
      // Calculate frequency bin
      raf_freq_bin = ion_radial_age_freq.find_freq_bin(frequency_bin);
    } else {
      raf_radial_bin = raf_age_bin = raf_freq_bin = OUTSIDE; // Default is out of range
    }
    raf_in_range = (raf_radial_bin >= 0 && raf_age_bin >= 0 && raf_freq_bin >= 0);
  #endif
  #if output_ion_distance_age_freq
    if (source_type == STARS) {
      // Calculate distance bin from source position [cm]
      daf_distance_bin = 0;                  // Photon starts at the source
      // Calculate age bin from stellar age [Gyr]
      daf_age_bin = ion_distance_age_freq.find_age_bin(age_star[source_id]);
      // Calculate frequency bin
      daf_freq_bin = ion_distance_age_freq.find_freq_bin(frequency_bin);
    } else {
      daf_distance_bin = daf_age_bin = daf_freq_bin = OUTSIDE; // Default is out of range
    }
    daf_in_range = (daf_distance_bin >= 0 && daf_age_bin >= 0 && daf_freq_bin >= 0);
  #endif
  #if output_radial_flow
    // Calculate radial bin from radial position [cm]
    const double rf_radius = (position - radial_center).norm();
    rf_bin_max = rf_bin_src = rf_bin = radial_flow.find_radial_bin(rf_radius);
    // Calculate frequency bin
    rf_freq_bin = radial_flow.find_freq_bin(frequency_bin);
    rf_in_range = (rf_bin >= 0 && rf_freq_bin >= 0);
    if (rf_in_range) {
      #pragma omp atomic
      radial_flow.src(rf_bin_src, rf_freq_bin) += source_weight;
    }
  #endif

  // Group statistics histogram bins
  #if output_groups || output_group_flows
    igrp = get_group_index(source_type, source_id); // Group ID index
    valid_igrp = (igrp >= 0 && igrp < n_groups); // Valid index
  #endif
  #if output_group_flows
    if (valid_igrp) {
      auto& group_flow = group_flows[igrp];
      vector<Vec3>& r_grp_flows = focus_groups_on_emission ? r_light_grp : (output_grp_gal ? r_grp_gal : r_grp_vir); // Group flow positions [cm]
      // Calculate radial bin from radial position [cm]
      const double gf_radius = (position - r_grp_flows[igrp]).norm();
      gf_bin_max = gf_bin_src = gf_bin = group_flow.find_radial_bin(gf_radius);
      // Calculate frequency bin
      gf_freq_bin = group_flow.find_freq_bin(frequency_bin);
      gf_in_range = (gf_bin >= 0 && gf_freq_bin >= 0);
      if (gf_in_range) {
        #pragma omp atomic
        group_flow.src(gf_bin_src, gf_freq_bin) += source_weight;
      }
    } else {
      gf_in_range = false;                   // Group ID is invalid
    }
  #endif

  // Subhalo statistics histogram bins
  #if output_subhalos || output_subhalo_flows
    isub = get_subhalo_index(source_type, source_id); // Subhalo ID index
    valid_isub = (isub >= 0 && isub < n_subhalos); // Valid index
  #endif
  #if output_subhalo_flows
    if (valid_isub) {
      auto& subhalo_flow = subhalo_flows[isub];
      vector<Vec3>& r_sub_flows = focus_subhalos_on_emission ? r_light_sub : (output_sub_gal ? r_sub_gal : r_sub_vir); // Subhalo flow positions [cm]
      // Calculate radial bin from radial position [cm]
      const double sf_radius = (position - r_sub_flows[isub]).norm();
      sf_bin_max = sf_bin_src = sf_bin = subhalo_flow.find_radial_bin(sf_radius);
      // Calculate frequency bin
      sf_freq_bin = subhalo_flow.find_freq_bin(frequency_bin);
      sf_in_range = (sf_bin >= 0 && sf_freq_bin >= 0);
      if (sf_in_range) {
        #pragma omp atomic
        subhalo_flow.src(sf_bin_src, sf_freq_bin) += source_weight;
      }
    } else {
      sf_in_range = false;
    }
  #endif

  // Group statistics
  #if output_groups
    if (valid_igrp) {
      #pragma omp atomic
      bin_f_src_grp(igrp, frequency_bin) += source_weight; // Bin source fraction
      #pragma omp atomic
      bin_f2_src_grp(igrp, frequency_bin) += source_weight * source_weight; // Bin squared f_src

      // Check whether the photon is inside the group virial radius
      if (output_grp_vir && norm2(position - r_grp_vir[igrp]) < R2_grp_vir[igrp]) {
        in_grp_vir = true;                   // Flag for virial statistics
        #pragma omp atomic
        bin_f_src_grp_vir(igrp, frequency_bin) += source_weight; // Bin source fraction
        #pragma omp atomic
        bin_f2_src_grp_vir(igrp, frequency_bin) += source_weight * source_weight; // Bin squared f_src
      }
      // Check whether the photon is inside the group galaxy radius
      if (output_grp_gal && norm2(position - r_grp_gal[igrp]) < R2_grp_gal[igrp]) {
        in_grp_gal = true;                   // Flag for galaxy statistics
        #pragma omp atomic
        bin_f_src_grp_gal(igrp, frequency_bin) += source_weight; // Bin source fraction
        #pragma omp atomic
        bin_f2_src_grp_gal(igrp, frequency_bin) += source_weight * source_weight; // Bin squared f_src
      }
    } else {
      const int uigrp = get_ugroup_index(source_type, source_id); // Unfiltered group ID index
      if (uigrp < 0 || uigrp >= n_ugroups) {
        print(); error("Invalid unfiltered group ID: " + to_string(uigrp));
      }
      #pragma omp atomic
      bin_f_src_ugrp(uigrp, frequency_bin) += source_weight; // Bin source fraction
      #pragma omp atomic
      bin_f2_src_ugrp(uigrp, frequency_bin) += source_weight * source_weight; // Bin squared f_src
    }
  #endif

  // Subhalo statistics
  #if output_subhalos
    if (valid_isub) {
      #pragma omp atomic
      bin_f_src_sub(isub, frequency_bin) += source_weight; // Bin source fraction
      #pragma omp atomic
      bin_f2_src_sub(isub, frequency_bin) += source_weight * source_weight; // Bin squared f_src

      // Check whether the photon is inside the subhalo virial radius
      if (output_sub_vir && norm2(position - r_sub_vir[isub]) < R2_sub_vir[isub]) {
        in_sub_vir = true;                   // Flag for virial statistics
        #pragma omp atomic
        bin_f_src_sub_vir(isub, frequency_bin) += source_weight; // Bin source fraction
        #pragma omp atomic
        bin_f2_src_sub_vir(isub, frequency_bin) += source_weight * source_weight; // Bin squared f_src
      }
      // Check whether the photon is inside the subhalo galaxy radius
      if (output_sub_gal && norm2(position - r_sub_gal[isub]) < R2_sub_gal[isub]) {
        in_sub_gal = true;                   // Flag for galaxy statistics
        #pragma omp atomic
        bin_f_src_sub_gal(isub, frequency_bin) += source_weight; // Bin source fraction
        #pragma omp atomic
        bin_f2_src_sub_gal(isub, frequency_bin) += source_weight * source_weight; // Bin squared f_src
      }
    } else {
      const int uisub = get_usubhalo_index(source_type, source_id); // Unfiltered subhalo ID index
      if (uisub < 0 || uisub >= n_usubhalos) {
        print(); error("Invalid unfiltered subhalo ID: " + to_string(uisub));
      }
      #pragma omp atomic
      bin_f_src_usub(uisub, frequency_bin) += source_weight; // Bin source fraction
      #pragma omp atomic
      bin_f2_src_usub(uisub, frequency_bin) += source_weight * source_weight; // Bin squared f_src
    }
  #endif

  // Note: calculate_LOS requires an initialized photon so should be done last
  if (source_type == PLANE) {
    for (int camera = 0; camera < n_cameras; ++camera) {
      if (direction.dot(camera_directions[camera]) > 0.999) {
        calculate_LOS(ISOTROPIC, camera);
        if (output_mcrt_emission)
          calculate_LOS_int(camera);         // Intrinsic emission cameras
        if (output_mcrt_attenuation)
          calculate_LOS_ext(camera);         // Attenuated emission cameras
      }
    }
  } else {
    direction = isotropic_direction();       // Initial direction (isotropic)
    calculate_LOS(ISOTROPIC);                // Flux for optically thin cases
    if (output_mcrt_emission)
      calculate_LOS_int();                   // Intrinsic emission cameras
    if (output_mcrt_attenuation)
      calculate_LOS_ext();                   // Attenuated emission cameras
  }
}

/* Print photon data. */
void IonPhoton::print() {
  cout << "\nIonPhoton {"
       << "\n  source_id       = " << source_id
       << "\n  source_type     = " << source_type << " (" << ion_source_names_mask[source_type] << ")"
       << "\n  source_weight   = " << source_weight
       << "\n  current_cell    = " << current_cell
       << "\n  n_scat          = " << n_scat
       << "\n  source_position = " << source_position / kpc << " kpc"
       << "\n  position        = " << position / kpc << " kpc"
       << "\n  direction       = " << direction
       << "\n  frequency_bin   = " << frequency_bin << "  [" << bin_names[frequency_bin]
       << ": " << bin_edges_eV[frequency_bin] << " - " << bin_edges_eV[frequency_bin+1] << " eV]"
       << "\n  weight          = " << weight
       << "\n  dust_weight     = " << dust_weight;
  for (int ai = 0; ai < n_active_ions; ++ai)
    cout << "\n  " << std::setw(15) << std::left << ion_names[active_ions[ai]] + "_weight" << " = " << ion_weight[ai];
  cout << "\n  dist_abs        = " << dist_abs
       << "\n  l_tot           = " << l_tot
       << "\n  luminosity      = " << luminosity << " erg/s"
       << "\n  Ndot            = " << Ndot << " photons/s"
       << "\n  energy (L/Ndot) = " << luminosity / (Ndot * eV) << " eV";
  for (int ai = 0; ai < n_active_ions; ++ai)
    cout << "\n  sigma_" << std::setw(9) << std::left << ion_names[active_ions[ai]] << " = " << sigma_ions[ai] << " cm^2";
  if (output_photoheating)
    for (int ai = 0; ai < n_active_ions; ++ai)
      cout << "\n  sigma_epsilon_" << ion_names[active_ions[ai]] << " = " << sigma_epsilon_ions[ai] << " erg cm^2";
  auto cout_dust = [](const double (&arr)[n_dust_species]) -> void {
    if constexpr (multiple_dust_species)
      cout << "[";
    cout << arr[0];
    for (int i = 1; i < n_dust_species; ++i)
      cout << ", " << arr[i];
    if constexpr (multiple_dust_species)
      cout << "]";
  };
  cout << "\n  kappa           = "; cout_dust(kappas); cout << " cm^2/g dust"
       << "\n  albedo          = "; cout_dust(albedos);
  cout << "\n  cosine          = "; cout_dust(cosines);
  cout << "\n}" << endl;
  if (source_type == STARS) {
    cout << "\nEmitting star data:"
         << "\n  position        = " << r_star[source_id] / kpc << " kpc";
    if (read_v_star)
      cout << "\n  velocity        = " << v_star[source_id] / km << " km/s";
    if (read_continuum)
      cout << "\n  L_cont          = " << L_cont_star[source_id] << " erg/s/angstrom";
    if (read_m_massive_star)
      cout << "\n  m_star(OB)      = " << m_massive_star[source_id] << " Msun";
    if (read_m_init_star)
      cout << "\n  m_star(0)       = " << m_init_star[source_id] << " Msun";
    if (read_Z_star)
      cout << "\n  Z_star          = " << Z_star[source_id];
    if (read_age_star)
      cout << "\n  age_star        = " << age_star[source_id] << " Gyr";
    if (!group_id_star.empty())
      cout << "\n  group_id        = " << get_group_id(source_type, source_id);
    if (!subhalo_id_star.empty())
      cout << "\n  subhalo_id      = " << get_subhalo_id(source_type, source_id);
    cout << endl;
  }
  #if output_groups || output_group_flows
    cout << "\nEmitting group " << get_group_id(source_type, source_id) << " data:";
    if (valid_igrp) {
      cout << "\n  index            = " << igrp
           << "\n  n_groups         = " << n_groups
           << "\n  Ndot_grp         = " << Ndot_grp[igrp] << " photons/s"
           << "\n  bin_Ndot_grp     = " << bin_Ndot_grp(igrp, frequency_bin) << " photons/s";
      if (output_grp_vir)
        cout << "\n   in_grp_vir      = " << (in_grp_vir ? "true" : "false")
             << "\n    Ndot_grp_vir   = " << Ndot_grp_vir[igrp] << " photons/s"
             << "\n    bin_Ndot_vir   = " << bin_Ndot_grp_vir(igrp, frequency_bin) << " photons/s"
             << "\n    r_grp_vir      = " << r_grp_vir[igrp] / kpc << " kpc"
             << "\n    R_grp_vir      = " << R_grp_vir[igrp] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_grp_vir[igrp]) / kpc << " kpc";
      if (output_grp_gal)
        cout << "\n   in_grp_gal      = " << (in_grp_gal ? "true" : "false")
             << "\n    Ndot_grp_gal   = " << Ndot_grp_gal[igrp] << " photons/s"
             << "\n    bin_Ndot_gal   = " << bin_Ndot_grp_gal(igrp, frequency_bin) << " photons/s"
             << "\n    r_grp_gal      = " << r_grp_gal[igrp] / kpc << " kpc"
             << "\n    R_grp_gal      = " << R_grp_gal[igrp] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_grp_gal[igrp]) / kpc << " kpc";
    } else {
      cout << "\n  index            = " << get_ugroup_index(source_type, source_id)
           << "\n  n_ugroups        = " << n_ugroups
           << "\n  Ndot_ugrp        = " << Ndot_ugrp[igrp] << " photons/s"
           << "\n  bin_Ndot_ugrp    = " << bin_Ndot_ugrp(igrp, frequency_bin) << " photons/s";
      if (output_grp_vir)
        cout << "\n   in_grp_vir      = " << (in_grp_vir ? "true" : "false");
      if (output_grp_gal)
        cout << "\n   in_grp_gal      = " << (in_grp_gal ? "true" : "false");
    }
    cout << endl;
  #endif
  #if output_subhalos || output_subhalo_flows
    cout << "\nEmitting subhalo " << get_subhalo_id(source_type, source_id) << " data:";
    if (valid_isub) {
      cout << "\n  index            = " << isub
           << "\n  n_subhalos       = " << n_subhalos
           << "\n  Ndot_sub         = " << Ndot_sub[isub] << " photons/s"
           << "\n  bin_Ndot_sub     = " << bin_Ndot_sub(isub, frequency_bin) << " photons/s";
      if (output_sub_vir)
        cout << "\n   in_sub_vir      = " << (in_sub_vir ? "true" : "false")
             << "\n    Ndot_sub_vir   = " << Ndot_sub_vir[isub] << " photons/s"
             << "\n    bin_Ndot_vir   = " << bin_Ndot_sub_vir(isub, frequency_bin) << " photons/s"
             << "\n    r_sub_vir      = " << r_sub_vir[isub] / kpc << " kpc"
             << "\n    R_sub_vir      = " << R_sub_vir[isub] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_sub_vir[isub]) / kpc << " kpc";
      if (output_sub_gal)
        cout << "\n   in_sub_gal      = " << (in_sub_gal ? "true" : "false")
             << "\n    Ndot_sub_gal   = " << Ndot_sub_gal[isub] << " photons/s"
             << "\n    bin_Ndot_gal   = " << bin_Ndot_sub_gal(isub, frequency_bin) << " photons/s"
             << "\n    r_sub_gal      = " << r_sub_gal[isub] / kpc << " kpc"
             << "\n    R_sub_gal      = " << R_sub_gal[isub] / kpc << " kpc"
             << "\n    photon radius  = " << norm(position - r_sub_gal[isub]) / kpc << " kpc";
    } else {
      cout << "\n  index            = " << get_usubhalo_index(source_type, source_id)
           << "\n  n_usubhalos      = " << n_usubhalos
           << "\n  Ndot_usub        = " << Ndot_usub[isub] << " photons/s"
           << "\n  bin_Ndot_usub    = " << bin_Ndot_usub(isub, frequency_bin) << " photons/s";
      if (output_sub_vir)
        cout << "\n   in_sub_vir      = " << (in_sub_vir ? "true" : "false");
      if (output_sub_gal)
        cout << "\n   in_sub_gal      = " << (in_sub_gal ? "true" : "false");
    }
    cout << endl;
  #endif
}

/* Updates the position and weight of the photon */
void IonPhoton::move_photon(const double distance) {
  const double dtau_abs = k_abs * distance;  // Absorption optical depth
  const double exp_dtau_abs = exp(-dtau_abs);
  const double rate_weight = weight * ((dtau_abs < 1e-5) ? // Numerical stability
    (1. - 0.5 * dtau_abs) * distance :       // Series approx (dtau_abs << 1)
    (1. - exp_dtau_abs) / k_abs);            // Radiative transfer solution [cm]
  const double tau_abs = k_abs * l_tot;      // Absorption optical depth
  const double abs_dl = weight * ((dtau_abs < 1e-5) ? // Numerical stability
    distance * (l_tot + 0.5 * distance - dtau_abs * (0.5 * l_tot + distance / 3.)) : // Series approx (dtau_abs << 1)
    (1. + tau_abs - (1. + tau_abs + dtau_abs) * exp_dtau_abs) / (k_abs * k_abs)); // Analytic solution [cm^2]

  // Update ionization rates (Note: x_HI etc. are in the equilibrium solver)
  for (int ai = 0; ai < n_active_ions; ++ai) {
    #pragma omp atomic
    rate_ions[ai][current_cell] += sigma_ions[ai] * rate_weight;
  }
  if (output_photoheating) {                 // Combined photoheating [erg]
    double k_epsilon_eff = 0.;               // Effective absorption coefficient
    for (int ai = 0; ai < n_active_ions; ++ai)
      k_epsilon_eff += n_ions_abs[ai] * sigma_epsilon_ions[ai];
    #pragma omp atomic
    G_ion[current_cell] += k_epsilon_eff * rate_weight; // Photoheating [erg/photons]
  }
  if (output_photon_density)
    #pragma omp atomic
    bin_n_gamma(current_cell, frequency_bin) += rate_weight; // Photon density [photons/cm^3]

  // Number of absorptions by cell, age, and frequency [photons/s]
  #if output_ion_cell_age_freq
    if (caf_in_range) {
      for (int ais = 0; ais < n_active_ion_stats; ++ais) {
        const int ai = active_ion_stats[ais];
        auto& stat = ion_cell_age_freq.stats[ais];
        #pragma omp atomic
        stat(current_cell, caf_age_bin, caf_freq_bin) += k_abss[ai] * rate_weight;
      }
    }
  #endif

  // Number of absorptions by radial, age, and frequency [photons/s]
  #if output_ion_radial_age_freq
    if (raf_in_range) {
      for (int ais = 0; ais < n_active_ion_stats; ++ais) {
        const int ai = active_ion_stats[ais];
        auto& stat = ion_radial_age_freq.stats[ais];
        #pragma omp atomic
        stat(raf_radial_bin, raf_age_bin, raf_freq_bin) += k_abss[ai] * rate_weight;
      }
    }
  #endif

  // Number of absorptions by distance, age, and frequency [photons/s]
  #if output_ion_distance_age_freq
    if (daf_in_range) {
      for (int ais = 0; ais < n_active_ion_stats; ++ais) {
        const int ai = active_ion_stats[ais];
        auto& stat = ion_distance_age_freq.stats[ais];
        #pragma omp atomic
        stat(daf_distance_bin, daf_age_bin, daf_freq_bin) += k_abss[ai] * rate_weight;
      }
    }
  #endif

  // Number of absorptions by radial and frequency bin [photons/s]
  #if output_radial_flow
    if (rf_in_range) {
      for (int ais = 0; ais < n_active_ion_stats; ++ais) {
        const int ai = active_ion_stats[ais];
        auto& stat = radial_flow.stats[ais];
        #pragma omp atomic
        stat(rf_bin, rf_freq_bin) += k_abss[ai] * rate_weight;
      }
    }
  #endif

  // Group number of absorptions by radial and frequency bin [photons/s]
  #if output_group_flows
    if (gf_in_range) {
      for (int ais = 0; ais < n_active_ion_stats; ++ais) {
        const int ai = active_ion_stats[ais];
        auto& stat = group_flows[igrp].stats[ais];
        #pragma omp atomic
        stat(gf_bin, gf_freq_bin) += k_abss[ai] * rate_weight;
      }
    }
  #endif

  // Subhalo number of absorptions by radial and frequency bin [photons/s]
  #if output_subhalo_flows
    if (sf_in_range) {
      for (int ais = 0; ais < n_active_ion_stats; ++ais) {
        const int ai = active_ion_stats[ais];
        auto& stat = subhalo_flows[isub].stats[ais];
        #pragma omp atomic
        stat(sf_bin, sf_freq_bin) += k_abss[ai] * rate_weight;
      }
    }
  #endif

  // Update the position and weight
  position += direction * distance;          // Update photon position
  weight *= exp_dtau_abs;                    // Reduce weight by exp(-dtau_abs)
  dust_weight += k_dust_abs * rate_weight;   // Weight removed by dust absorption
  for (int ai = 0; ai < n_active_ions; ++ai)
    ion_weight[ai] += k_abss[ai] * rate_weight; // Weight removed by species absorption
  dist_abs += abs_dl;                        // Photon absorption distance [cm]
  l_tot += distance;                         // Update the total path length [cm]
}

/* Update the cell absorption coefficient properties. */
void IonPhoton::update_k_abs() {
  #if multiple_dust_species
    #if graphite_dust
      #if graphite_scaled_PAH
        k_dusts[GRAPHITE] = fm1_PAH * kappas[GRAPHITE] * rho_dust_G[current_cell];
        k_dusts[PAH] = f_PAH * kappas[PAH] * rho_dust_G[current_cell];
      #else
        k_dusts[GRAPHITE] = kappas[GRAPHITE] * rho_dust_G[current_cell];
      #endif
    #endif
    #if silicate_dust
      k_dusts[SILICATE] = kappas[SILICATE] * rho_dust_S[current_cell];
    #endif
    k_dust = k_dusts[0];                     // Dust absorption coefficient
    k_scat = k_scats[0] = albedos[0] * k_dusts[0]; // Dust scattering coefficient
    for (int i = 1; i < n_dust_species; ++i) {
      k_dust += k_dusts[i];                  // Dust absorption coefficient
      k_scat += k_scats[i] = albedos[i] * k_dusts[i]; // Dust scattering coefficient
    }
  #else
    k_dust = kappas[0] * rho_dust[current_cell]; // Dust absorption coefficient
    k_scat = albedos[0] * k_dust;            // Dust scattering coefficient
  #endif
  k_dust_abs = k_dust - k_scat;              // Dust extinction coefficient
  k_gas_abs = 0.;                            // Total (without dust) absorption
  for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
    const double n_atom = fields[n_indices[aa]].data[current_cell]; // Atom number density [cm^-3]
    double& k_sum = k_abss[n_active_ions + aa]; // Atom absorption coefficient [1/cm]
    k_sum = 0.;                              // Reset sum
    const int ion_count = ion_counts[aa];    // Number of active ions
    for (int j = 0; j < ion_count; ++j, ++ai) {
      const double x_ion = fields[x_indices[ai]].data[current_cell]; // Ionization fraction
      n_ions_abs[ai] = x_ion * n_atom;       // Save species number densities
      k_sum += k_abss[ai] = n_ions_abs[ai] * sigma_ions[ai]; // Ionization contribution
    }
    k_gas_abs += k_sum;                      // Add to gas absorption coefficient
  }
  k_abs = k_gas_abs + k_dust_abs;            // Total (gas + dust) absorption
}

/* Ray tracer (ionization) */
void IonPhoton::ray_trace() {
  // Initialize the grid traversal variables
  #if have_multiple_grids
    int event;                               // Event flag
  #endif
  int neib_cell;                             // Neighbor index
  double dl_next, dl_neib;                   // Event distances
  double dtau_scat = -log(ran());            // Optical depth to next scatter
  #if check_escape
    double dl_exit = positive_infinity;      // Exit distance
  #endif
  #if spherical_escape                       // Spherical escape distance
    inplace_min(dl_exit, spherical_escape_distance(position, direction));
  #endif
  #if box_escape                             // Box escape distance
    inplace_min(dl_exit, box_escape_distance(position, direction));
  #endif
  #if streaming_escape                       // Streaming escape distance
    inplace_min(dl_exit, max_streaming);
  #endif
  #if age_escape
    const bool age_escape_active = (source_type == STARS); // Age escape flag
    double dl_age = (age_escape_active) ? age_star[source_id] * cGyr : positive_infinity;
  #endif
  #if output_radial_age_freq                 // Initialize radial age frequency
    int neib_raf;                            // Radial age frequency neighbor index
    double dl_raf = 0.;                      // Radial age frequency distance
    tie(dl_raf, neib_raf) = ion_radial_age_freq.face_distance(position - radial_center, direction, raf_radial_bin);
  #endif
  #if output_distance_age_freq               // Initialize distance age frequency
    int neib_daf;                            // Distance age frequency neighbor index
    double dl_daf = 0.;                      // Distance age frequency distance
    tie(dl_daf, neib_daf) = ion_distance_age_freq.face_distance(position - source_position, direction, daf_distance_bin);
  #endif
  #if output_radial_flow                     // Initialize radial flow
    int neib_rf;                             // Radial flow neighbor index
    double dl_rf = 0.;                       // Radial flow distance
    tie(dl_rf, neib_rf) = radial_flow.face_distance(position - radial_center, direction, rf_bin);
  #endif
  #if output_groups || output_group_flows    // Initialize group flow
    vector<Vec3>& r_grp_flows = focus_groups_on_emission ? r_light_grp : (output_grp_gal ? r_grp_gal : r_grp_vir); // Group flow positions [cm]
  #endif
  #if output_subhalos || output_subhalo_flows // Initialize subhalo flow
    vector<Vec3>& r_sub_flows = focus_subhalos_on_emission ? r_light_sub : (output_sub_gal ? r_sub_gal : r_sub_vir); // Subhalo flow positions [cm]
  #endif
  #if output_group_flows                     // Group flow requires a valid group ID index
    int neib_gf;                             // Group flow neighbor index
    double dl_gf = 0.;                       // Group flow distance
    if (valid_igrp) tie(dl_gf, neib_gf) = group_flows[igrp].face_distance(position - r_grp_flows[igrp], direction, gf_bin);
  #endif
  #if output_subhalo_flows                   // Subhalo flow requires a valid subhalo ID index
    int neib_sf;                             // Subhalo flow neighbor index
    double dl_sf = 0.;                       // Subhalo flow distance
    if (valid_isub) tie(dl_sf, neib_sf) = subhalo_flows[isub].face_distance(position - r_sub_flows[isub], direction, sf_bin);
  #endif
  #if output_groups                          // Initialize group escape distance (virial, galaxy radius)
    double dl_grp_vir = 0., dl_grp_gal = 0.; // Escape distances
    if (in_grp_vir) dl_grp_vir = escape_distance(position - r_grp_vir[igrp], direction, R2_grp_vir[igrp]);
    if (in_grp_gal) dl_grp_gal = escape_distance(position - r_grp_gal[igrp], direction, R2_grp_gal[igrp]);
  #endif
  #if output_subhalos                        // Initialize subhalo escape distance (virial, galaxy radius)
    double dl_sub_vir = 0., dl_sub_gal = 0.; // Escape distances
    if (in_sub_vir) dl_sub_vir = escape_distance(position - r_sub_vir[isub], direction, R2_sub_vir[isub]);
    if (in_sub_gal) dl_sub_gal = escape_distance(position - r_sub_gal[isub], direction, R2_sub_gal[isub]);
  #endif
  tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);
  update_k_abs();                            // Update absorption coefficients

  while (true) {
    // Determine the next event
    dl_next = dl_neib;                       // Reset comparison distance
    #if have_multiple_grids
      event = GRID;                          // Flag for grid traversal
    #endif
    #if check_escape
      if (dl_exit <= dl_next) {              // Exit region before cell
        dl_next = dl_exit;                   // Set neighbor distance to exit
        neib_cell = OUTSIDE;                 // Flag for escape condition
      }
    #endif
    #if age_escape
      if (dl_age <= dl_next) {               // Exit due to stellar age
        dl_next = dl_age;                    // Set neighbor distance to age limit
        neib_cell = OUTSIDE;                 // Flag for escape condition
      }
    #endif
    #if output_radial_age_freq
      if (dl_raf <= dl_next) {
        dl_next = dl_raf;                    // Update to raf distance
        event = RADIAL;                      // Flag for raf traversal
      }
    #endif
    #if output_distance_age_freq
      if (dl_daf <= dl_next) {
        dl_next = dl_daf;                    // Update to daf distance
        event = DISTANCE;                    // Flag for daf traversal
      }
    #endif
    #if output_radial_flow
      if (dl_rf <= dl_next) {
        dl_next = dl_rf;                     // Update to rf distance
        event = RADIAL_FLOW;                 // Flag for rf traversal
      }
    #endif
    #if output_group_flows
      if (valid_igrp && dl_gf <= dl_next) {
        dl_next = dl_gf;                     // Update to gf distance
        event = GROUP_FLOW;                  // Flag for gf traversal
      }
    #endif
    #if output_subhalo_flows
      if (valid_isub && dl_sf <= dl_next) {
        dl_next = dl_sf;                     // Update to sf distance
        event = SUBHALO_FLOW;                // Flag for sf traversal
      }
    #endif
    #if output_groups
      if (in_grp_vir && dl_grp_vir <= dl_next) {
        dl_next = dl_grp_vir;                // Update to group escape distance
        event = GROUP_VIR_ESCAPE;            // Flag for group escape
      }
      if (in_grp_gal && dl_grp_gal <= dl_next) {
        dl_next = dl_grp_gal;                // Update to group escape distance
        event = GROUP_GAL_ESCAPE;            // Flag for group escape
      }
    #endif
    #if output_subhalos
      if (in_sub_vir && dl_sub_vir <= dl_next) {
        dl_next = dl_sub_vir;                // Update to subhalo escape distance
        event = SUBHALO_VIR_ESCAPE;          // Flag for subhalo escape
      }
      if (in_sub_gal && dl_sub_gal <= dl_next) {
        dl_next = dl_sub_gal;                // Update to subhalo escape distance
        event = SUBHALO_GAL_ESCAPE;          // Flag for subhalo escape
      }
    #endif

    // Check whether the photon is scattered
    const double dtau_next = k_scat * dl_next; // Optical depth to neighboring cell
    if (dtau_scat <= dtau_next) {            // A scattering event takes place
      // Calculate the new position from the remaining optical depth
      const double dl_scat = dtau_scat / k_scat; // Actual length traveled
      move_photon(dl_scat);                  // Update position and weight
      n_scat++;                              // Update number of scattering events
      if (weight < weight_discard) {         // Consider the photon packet absorbed
        if (k_dust_abs > 0.)
          dust_weight += weight * k_dust_abs / k_abs; // Assign remaining weight
        weight = 0.;                         // Flag the photon as absorbed
        break;                               // Stop ray-tracing procedure
      }

      // Scatter: Update direction, cameras, etc.
      #if multiple_dust_species
        const double k_dust_species = ran() * k_dust; // Dust species selection
        double k_dust_cdf = k_dusts[0];      // Initialize cumulative distribution
        dust_species = 0;                    // Initialize dust species index
        while (k_dust_species > k_dust_cdf && dust_species < n_dust_species - 1) {
          k_dust_cdf += k_dusts[++dust_species]; // Cumulative distribution
        }
      #endif
      dust_scatter();                        // Interaction is with dust

      // Reset traversal distances (new direction)
      tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);
      #if check_escape
        dl_exit = positive_infinity;         // Reset escape distance
      #endif
      #if spherical_escape                   // Spherical escape distance
        inplace_min(dl_exit, spherical_escape_distance(position, direction));
      #endif
      #if box_escape                         // Box escape distance
        inplace_min(dl_exit, box_escape_distance(position, direction));
      #endif
      #if streaming_escape                   // Streaming escape distance
        inplace_min(dl_exit, max_streaming);
      #endif
      #if output_radial_age_freq
        tie(dl_raf, neib_raf) = ion_radial_age_freq.face_distance(position - radial_center, direction, raf_radial_bin);
      #endif
      #if output_distance_age_freq
        tie(dl_daf, neib_daf) = ion_distance_age_freq.face_distance(position - source_position, direction, daf_distance_bin);
      #endif
      #if output_radial_flow
        tie(dl_rf, neib_rf) = radial_flow.face_distance(position - radial_center, direction, rf_bin);
      #endif
      #if output_group_flows
        if (valid_igrp) tie(dl_gf, neib_gf) = group_flows[igrp].face_distance(position - r_grp_flows[igrp], direction, gf_bin);
      #endif
      #if output_subhalo_flows
        if (valid_isub) tie(dl_sf, neib_sf) = subhalo_flows[isub].face_distance(position - r_sub_flows[isub], direction, sf_bin);
      #endif
      #if output_groups
        if (in_grp_vir) dl_grp_vir = escape_distance(position - r_grp_vir[igrp], direction, R2_grp_vir[igrp]);
        if (in_grp_gal) dl_grp_gal = escape_distance(position - r_grp_gal[igrp], direction, R2_grp_gal[igrp]);
      #endif
      #if output_subhalos
        if (in_sub_vir) dl_sub_vir = escape_distance(position - r_sub_vir[isub], direction, R2_sub_vir[isub]);
        if (in_sub_gal) dl_sub_gal = escape_distance(position - r_sub_gal[isub], direction, R2_sub_gal[isub]);
      #endif

      // Generate a new random optical depth
      dtau_scat = -log(ran());
      continue;                              // Continue ray-tracing procedure
    }

    // No scattering event
    dtau_scat -= dtau_next;                  // Subtract traversed optical depth
    if constexpr (have_multiple_grids)
      dl_neib -= dl_next;                    // Subtract traversed distance (grid)
    #if check_escape
      dl_exit -= dl_next;                    // Subtract traversed distance (escape)
    #endif
    #if age_escape
      if (age_escape_active) dl_age -= dl_next; // Subtract traversed distance (stellar age)
    #endif
    #if output_radial_age_freq
      dl_raf -= dl_next;                     // Subtract traversed distance (raf)
    #endif
    #if output_distance_age_freq
      dl_daf -= dl_next;                     // Subtract traversed distance (daf)
    #endif
    #if output_radial_flow
      dl_rf -= dl_next;                      // Subtract traversed distance (rf)
    #endif
    #if output_group_flows
      dl_gf -= dl_next;                      // Subtract traversed distance (gf)
    #endif
    #if output_subhalo_flows
      dl_sf -= dl_next;                      // Subtract traversed distance (sf)
    #endif
    #if output_groups
      if (in_grp_vir) dl_grp_vir -= dl_next; // Subtract traversed distance (group virial)
      if (in_grp_gal) dl_grp_gal -= dl_next; // Subtract traversed distance (group galaxy)
    #endif
    #if output_subhalos
      if (in_sub_vir) dl_sub_vir -= dl_next; // Subtract traversed distance (subhalo virial)
      if (in_sub_gal) dl_sub_gal -= dl_next; // Subtract traversed distance (subhalo galaxy)
    #endif

    // Calculate the new position of the photon
    move_photon(dl_next);                    // Update position and weight
    if (weight < weight_discard) {           // Consider the photon packet absorbed
      if (k_dust_abs > 0.)
        dust_weight += weight * k_dust_abs / k_abs; // Assign remaining weight
      weight = 0.;                           // Flag the photon as absorbed
      break;                                 // Stop ray-tracing procedure
    }
    #if output_ion_radial_age_freq
      if (event == RADIAL) {                 // Check for raf traversal
        if (raf_age_bin >= 0 && raf_freq_bin >= 0) // Valid age and frequency bins
          update_in_range(raf_in_range, neib_raf); // Update in-range flag
        raf_radial_bin = neib_raf;           // Update radial bin index
        tie(dl_raf, neib_raf) = ion_radial_age_freq.face_distance(position - radial_center, direction, raf_radial_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_ion_distance_age_freq
      if (event == DISTANCE) {               // Check for daf traversal
        if (daf_age_bin >= 0 && daf_freq_bin >= 0) // Valid age and frequency bins
          update_in_range(daf_in_range, neib_daf); // Update in-range flag
        daf_distance_bin = neib_daf;         // Update distance bin index
        tie(dl_daf, neib_daf) = ion_distance_age_freq.face_distance(position - source_position, direction, daf_distance_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_radial_flow
      if (event == RADIAL_FLOW) {            // Check for rf traversal
        if (rf_freq_bin >= 0) {              // Check for valid frequency bin
          if (!rf_in_range) {                // Photon re-enters the grid
            rf_in_range = true;              // Flag as in-range
            #pragma omp atomic               // Inward flow from outside
            radial_flow.flow(rf_bin_src, neib_rf, rf_freq_bin) -= weight;
          } else if (neib_rf < 0) {          // Photon escapes the grid
            rf_in_range = false;             // Flag as out-of-range
            #pragma omp atomic               // Outward flow to outside
            radial_flow.flow(rf_bin_src, rf_bin, rf_freq_bin) += weight;
            if (rf_bin == rf_bin_max) {      // Check for first passage
              #pragma omp atomic
              radial_flow.pass(rf_bin_src, rf_bin, rf_freq_bin) += weight; // First passage
              rf_bin_max++;                  // Update the maximum radial bin
            }
          } else if (neib_rf > rf_bin) {     // Photon moves outward in the grid
            #pragma omp atomic
            radial_flow.flow(rf_bin_src, rf_bin, rf_freq_bin) += weight;
            if (rf_bin == rf_bin_max) {      // Check for first passage
              #pragma omp atomic
              radial_flow.pass(rf_bin_src, rf_bin, rf_freq_bin) += weight; // First passage
              rf_bin_max++;                  // Update the maximum radial bin
            }
          } else {                           // Photon moves inward in the grid
            #pragma omp atomic
            radial_flow.flow(rf_bin_src, neib_rf, rf_freq_bin) -= weight;
          }
        }
        rf_bin = neib_rf;                    // Update radial bin index
        tie(dl_rf, neib_rf) = radial_flow.face_distance(position - radial_center, direction, rf_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_group_flows
      if (event == GROUP_FLOW) {             // Check for gf traversal
        auto& group_flow = group_flows[igrp]; // Group flow object
        if (gf_freq_bin >= 0) {              // Check for valid frequency bin
          if (!gf_in_range) {                // Photon re-enters the grid
            gf_in_range = true;              // Flag as in-range
            #pragma omp atomic               // Inward flow from outside
            group_flow.flow(gf_bin_src, neib_gf, gf_freq_bin) -= weight;
          } else if (neib_gf < 0) {          // Photon escapes the grid
            gf_in_range = false;             // Flag as out-of-range
            #pragma omp atomic               // Outward flow to outside
            group_flow.flow(gf_bin_src, gf_bin, gf_freq_bin) += weight;
            if (gf_bin == gf_bin_max) {      // Check for first passage
              #pragma omp atomic
              group_flow.pass(gf_bin_src, gf_bin, gf_freq_bin) += weight; // First passage
              gf_bin_max++;                  // Update the maximum radial bin
            }
          } else if (neib_gf > gf_bin) {     // Photon moves outward in the grid
            #pragma omp atomic
            group_flow.flow(gf_bin_src, gf_bin, gf_freq_bin) += weight;
            if (gf_bin == gf_bin_max) {      // Check for first passage
              #pragma omp atomic
              group_flow.pass(gf_bin_src, gf_bin, gf_freq_bin) += weight; // First passage
              gf_bin_max++;                  // Update the maximum radial bin
            }
          } else {                           // Photon moves inward in the grid
            #pragma omp atomic
            group_flow.flow(gf_bin_src, neib_gf, gf_freq_bin) -= weight;
          }
        }
        gf_bin = neib_gf;                    // Update radial bin index
        tie(dl_gf, neib_gf) = group_flow.face_distance(position - r_grp_flows[igrp], direction, gf_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_subhalo_flows
      if (event == SUBHALO_FLOW) {           // Check for sf traversal
        auto& subhalo_flow = subhalo_flows[isub]; // Subhalo flow object
        if (sf_freq_bin >= 0) {              // Check for valid frequency bin
          if (!sf_in_range) {                // Photon re-enters the grid
            sf_in_range = true;              // Flag as in-range
            #pragma omp atomic               // Inward flow from outside
            subhalo_flow.flow(sf_bin_src, neib_sf, sf_freq_bin) -= weight;
          } else if (neib_sf < 0) {          // Photon escapes the grid
            sf_in_range = false;             // Flag as out-of-range
            #pragma omp atomic               // Outward flow to outside
            subhalo_flow.flow(sf_bin_src, sf_bin, sf_freq_bin) += weight;
            if (sf_bin == sf_bin_max) {      // Check for first passage
              #pragma omp atomic
              subhalo_flow.pass(sf_bin_src, sf_bin, sf_freq_bin) += weight; // First passage
              sf_bin_max++;                  // Update the maximum radial bin
            }
          } else if (neib_sf > sf_bin) {     // Photon moves outward in the grid
            #pragma omp atomic
            subhalo_flow.flow(sf_bin_src, sf_bin, sf_freq_bin) += weight;
            if (sf_bin == sf_bin_max) {      // Check for first passage
              #pragma omp atomic
              subhalo_flow.pass(sf_bin_src, sf_bin, sf_freq_bin) += weight; // First passage
              sf_bin_max++;                  // Update the maximum radial bin
            }
          } else {                           // Photon moves inward in the grid
            #pragma omp atomic
            subhalo_flow.flow(sf_bin_src, neib_sf, sf_freq_bin) -= weight;
          }
        }
        sf_bin = neib_sf;                    // Update radial bin index
        tie(dl_sf, neib_sf) = subhalo_flow.face_distance(position - r_sub_flows[isub], direction, sf_bin);
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_groups
      if (event == GROUP_VIR_ESCAPE) {       // Check for group escape (virial radius)
        if (weight > 0.) {
          #pragma omp atomic
          bin_f_esc_grp_vir(igrp, frequency_bin) += weight; // Bin escape fraction
          #pragma omp atomic
          bin_f2_esc_grp_vir(igrp, frequency_bin) += weight * weight; // Bin squared f_esc
          if (output_map_grp) {
            const int imap = vec2pix_ring(n_side_grp, direction); // Healpix index
            #pragma omp atomic
            map_grp_vir(igrp, imap) += weight;
            if (output_map2_grp)
              #pragma omp atomic
              map2_grp_vir(igrp, imap) += weight * weight; // Statistical convergence
          }
          if (output_stars && source_type == STARS) {
            #pragma omp atomic
            weight_stars_grp_vir[source_id] += weight; // Add photon weight at escape to the star (group virial)
          }
        }
        if (dust_weight > 0.) {
          #pragma omp atomic
          bin_f_abs_grp_vir(igrp, frequency_bin) += dust_weight; // Bin absorption fraction
          #pragma omp atomic
          bin_f2_abs_grp_vir(igrp, frequency_bin) += dust_weight * dust_weight; // Bin squared f_abs
        }
        in_grp_vir = false;                  // Finished group escape
        continue;                            // Continue ray-tracing procedure
      }
      if (event == GROUP_GAL_ESCAPE) {       // Check for group escape (galaxy radius)
        if (weight > 0.) {
          #pragma omp atomic
          bin_f_esc_grp_gal(igrp, frequency_bin) += weight; // Bin escape fraction
          #pragma omp atomic
          bin_f2_esc_grp_gal(igrp, frequency_bin) += weight * weight; // Bin squared f_esc
          if (output_map_grp) {
            const int imap = vec2pix_ring(n_side_grp, direction); // Healpix index
            #pragma omp atomic
            map_grp_gal(igrp, imap) += weight;
            if (output_map2_grp)
              #pragma omp atomic
              map2_grp_gal(igrp, imap) += weight * weight; // Statistical convergence
          }
          if (output_stars && source_type == STARS) {
            #pragma omp atomic
            weight_stars_grp_gal[source_id] += weight; // Add photon weight at escape to the star (group galaxy)
          }
        }
        if (dust_weight > 0.) {
          #pragma omp atomic
          bin_f_abs_grp_gal(igrp, frequency_bin) += dust_weight; // Bin absorption fraction
          #pragma omp atomic
          bin_f2_abs_grp_gal(igrp, frequency_bin) += dust_weight * dust_weight; // Bin squared f_abs
        }
        in_grp_gal = false;                  // Finished group escape
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    #if output_subhalos
      if (event == SUBHALO_VIR_ESCAPE) {     // Check for subhalo escape (virial radius)
        if (weight > 0.) {
          #pragma omp atomic
          bin_f_esc_sub_vir(isub, frequency_bin) += weight; // Bin escape fraction
          #pragma omp atomic
          bin_f2_esc_sub_vir(isub, frequency_bin) += weight * weight; // Bin squared f_esc
          if (output_map_sub) {
            const int imap = vec2pix_ring(n_side_sub, direction); // Healpix index
            #pragma omp atomic
            map_sub_vir(isub, imap) += weight;
            if (output_map2_sub)
              #pragma omp atomic
              map2_sub_vir(isub, imap) += weight * weight; // Statistical convergence
          }
          if (output_stars && source_type == STARS) {
            #pragma omp atomic
            weight_stars_sub_vir[source_id] += weight; // Add photon weight at escape to the star (subhalo virial)
          }
        }
        if (dust_weight > 0.) {
          #pragma omp atomic
          bin_f_abs_sub_vir(isub, frequency_bin) += dust_weight; // Bin absorption fraction
          #pragma omp atomic
          bin_f2_abs_sub_vir(isub, frequency_bin) += dust_weight * dust_weight; // Bin squared f_abs
        }
        in_sub_vir = false;                  // Finished subhalo escape
        continue;                            // Continue ray-tracing procedure
      }
      if (event == SUBHALO_GAL_ESCAPE) {     // Check for subhalo escape (galaxy radius)
        if (weight > 0.) {
          #pragma omp atomic
          bin_f_esc_sub_gal(isub, frequency_bin) += weight; // Bin escape fraction
          #pragma omp atomic
          bin_f2_esc_sub_gal(isub, frequency_bin) += weight * weight; // Bin squared f_esc
          if (output_map_sub) {
            const int imap = vec2pix_ring(n_side_sub, direction); // Healpix index
            #pragma omp atomic
            map_sub_gal(isub, imap) += weight;
            if (output_map2_sub)
              #pragma omp atomic
              map2_sub_gal(isub, imap) += weight * weight; // Statistical convergence
          }
          if (output_stars && source_type == STARS) {
            #pragma omp atomic
            weight_stars_sub_gal[source_id] += weight; // Add photon weight at escape to the star (subhalo galaxy)
          }
        }
        if (dust_weight > 0.) {
          #pragma omp atomic
          bin_f_abs_sub_gal(isub, frequency_bin) += dust_weight; // Bin absorption fraction
          #pragma omp atomic
          bin_f2_abs_sub_gal(isub, frequency_bin) += dust_weight * dust_weight; // Bin squared f_abs
        }
        in_sub_gal = false;                  // Finished subhalo escape
        continue;                            // Continue ray-tracing procedure
      }
    #endif
    if constexpr (SPHERICAL) {
      if (neib_cell == INSIDE)               // Check if the photon is trapped
        break;                               // Stop ray-tracing procedure
    }
    if (neib_cell == OUTSIDE)                // Check if the photon escapes
      break;                                 // Stop ray-tracing procedure
    current_cell = neib_cell;                // Update the cell index
    update_k_abs();                          // Update absorption coefficients
    tie(dl_neib, neib_cell) = face_distance(position, direction, current_cell);
  }

  // Calculate global statistics
  #pragma omp atomic
  bin_f_src[frequency_bin] += source_weight; // Global source fraction: sum(w0)
  #pragma omp atomic
  bin_f2_src[frequency_bin] += source_weight * source_weight; // Squared f_src: sum(w0^2)
  if (weight > 0.) {
    #pragma omp atomic
    bin_f_esc[frequency_bin] += weight;      // Global escape fraction: sum(w)
    #pragma omp atomic
    bin_f2_esc[frequency_bin] += weight * weight; // Squared f_esc: sum(w^2)
    if (output_stars && source_type == STARS) {
      #pragma omp atomic
      weight_stars[source_id] += weight;     // Add photon weight at escape to the star
    }
    if (output_cells && source_type == CELLS) {
      #pragma omp atomic
      weight_cells[source_id] += weight;     // Add photon weight at escape to the cell
    }

    // Line-of-sight healpix maps
    if (output_map) {
      const int imap = vec2pix_ring(n_side_map, direction); // Healpix index
      #pragma omp atomic
      map[imap] += weight;
      if (output_map2)
        #pragma omp atomic
        map2[imap] += weight * weight;       // Statistical convergence
    }

    // Line-of-sight radial map
    if (output_radial_map || output_cube_map || output_radial_avg || output_radial_cube_avg) {
      const Vec3 r_cam = position - camera_center; // Position relative to camera center
      const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance

      // Angle-averaged radial surface brightness
      if (output_radial_avg) {
        const double r_pix = inverse_radial_pixel_width * r_perp;
        if (r_pix < double(n_radial_pixels)) {
          const int ir = floor(r_pix);
          #pragma omp atomic
          radial_avg[ir] += weight;
        }
      }

      // Angle-averaged bin radial surface brightness
      if (output_radial_cube_avg) {
        const double r_pix = inverse_radial_cube_pixel_width * r_perp;
        if (r_pix < double(n_radial_cube_pixels)) {
          const int ir = floor(r_pix);
          #pragma omp atomic
          radial_cube_avg(ir, frequency_bin) += weight;
        }
      }

      // Line-of-sight radial map
      if (output_radial_map) {
        const double r_pix = inverse_map_pixel_width * r_perp;
        if (r_pix < double(n_map_pixels)) {
          const int imap = vec2pix_ring(n_side_radial, direction); // Healpix index
          const int ir = floor(r_pix);
          #pragma omp atomic
          radial_map(imap, ir) += weight;
        }
      }

      // Line-of-sight bin radial map
      if (output_cube_map) {
        const double r_pix = inverse_cube_map_pixel_width * r_perp;
        if (r_pix < double(n_cube_map_pixels)) {
          const int imap = vec2pix_ring(n_side_cube, direction); // Healpix index
          const int ir = floor(r_pix);
          #pragma omp atomic
          cube_map(imap, ir, frequency_bin) += weight;
        }
      }
    }

    // Group statistics
    #if output_groups
      if (output_grp_obs && valid_igrp) {
        // Global bin escape fraction for groups
        #pragma omp atomic
        bin_f_esc_grp(igrp, frequency_bin) += weight; // Bin escape fraction
        #pragma omp atomic
        bin_f2_esc_grp(igrp, frequency_bin) += weight * weight; // Bin squared f_esc

        // Line-of-sight healpix maps for groups
        if (output_map_grp) {
          const int imap = vec2pix_ring(n_side_grp, direction); // Healpix index
          #pragma omp atomic
          map_grp(igrp, imap) += weight;
          if (output_map2_grp)
            #pragma omp atomic
            map2_grp(igrp, imap) += weight * weight; // Statistical convergence
        }

        // Line-of-sight radial map for groups
        if (output_radial_map_grp) {
          const Vec3 r_cam = position - r_grp_flows[igrp]; // Position relative to group center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_map_radius_grp > 0.) {     // Logarithmically spaced radial map
            r_pix = (r_perp < min_map_radius_grp) ? 0. : 1. + (log10(r_perp) - log_min_map_radius_grp) * inverse_map_pixel_width_grp;
          } else {                           // Linearly spaced radial map
            r_pix = inverse_map_pixel_width_grp * r_perp;
          }
          if (r_pix < double(n_map_pixels_grp)) {
            const int imap = vec2pix_ring(n_side_radial_grp, direction); // Healpix index
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_map_grp(igrp, imap, ir) += weight;
          }
        }

        // Angle-averaged radial surface brightness for groups
        if (output_radial_avg_grp) {
          const Vec3 r_cam = position - r_grp_flows[igrp]; // Position relative to group center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_radius_grp > 0.) {         // Logarithmically spaced radial avg
            r_pix = (r_perp < min_radius_grp) ? 0. : 1. + (log10(r_perp) - log_min_radius_grp) * inverse_pixel_width_grp;
          } else {                           // Linearly spaced radial avg
            r_pix = inverse_pixel_width_grp * r_perp;
          }
          if (r_pix < double(n_pixels_grp)) {
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_avg_grp(igrp, ir) += weight;
          }
        }
      }
    #endif

    // Subhalo statistics
    #if output_subhalos
      if (output_sub_obs && valid_isub) {
        // Global bin escape fraction for subhalos
        #pragma omp atomic
        bin_f_esc_sub(isub, frequency_bin) += weight; // Bin escape fraction
        #pragma omp atomic
        bin_f2_esc_sub(isub, frequency_bin) += weight * weight; // Bin squared f_esc

        // Line-of-sight healpix maps for subhalos
        if (output_map_sub) {
          const int imap = vec2pix_ring(n_side_sub, direction); // Healpix index
          #pragma omp atomic
          map_sub(isub, imap) += weight;
          if (output_map2_sub)
            #pragma omp atomic
            map2_sub(isub, imap) += weight * weight; // Statistical convergence
        }

        // Line-of-sight radial map for subhalos
        if (output_radial_map_sub) {
          const Vec3 r_cam = position - r_sub_flows[isub]; // Position relative to subhalo center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_map_radius_sub > 0.) {     // Logarithmically spaced radial map
            r_pix = (r_perp < min_map_radius_sub) ? 0. : 1. + (log10(r_perp) - log_min_map_radius_sub) * inverse_map_pixel_width_sub;
          } else {                           // Linearly spaced radial map
            r_pix = inverse_map_pixel_width_sub * r_perp;
          }
          if (r_pix < double(n_map_pixels_sub)) {
            const int imap = vec2pix_ring(n_side_radial_sub, direction); // Healpix index
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_map_sub(isub, imap, ir) += weight;
          }
        }

        // Angle-averaged radial surface brightness for subhalos
        if (output_radial_avg_sub) {
          const Vec3 r_cam = position - r_sub_flows[isub]; // Position relative to subhalo center
          const double r_perp = cross_norm(r_cam, direction); // Perpendicular distance
          double r_pix;
          if (min_radius_sub > 0.) {         // Logarithmically spaced radial avg
            r_pix = (r_perp < min_radius_sub) ? 0. : 1. + (log10(r_perp) - log_min_radius_sub) * inverse_pixel_width_sub;
          } else {                           // Linearly spaced radial avg
            r_pix = inverse_pixel_width_sub * r_perp;
          }
          if (r_pix < double(n_pixels_sub)) {
            const int ir = floor(r_pix);
            #pragma omp atomic
            radial_avg_sub(isub, ir) += weight;
          }
        }
      }
    #endif

    // Radial flow statistics
    #if output_radial_flow
      #pragma omp atomic
      radial_flow.esc(rf_bin_src, rf_freq_bin) += weight;
    #endif

    // Group flow statistics
    #if output_group_flows
      if (valid_igrp)
        #pragma omp atomic
        group_flows[igrp].esc(gf_bin_src, gf_freq_bin) += weight;
    #endif

    // Subhalo flow statistics
    #if output_subhalo_flows
      if (valid_isub)
        #pragma omp atomic
        subhalo_flows[isub].esc(sf_bin_src, sf_freq_bin) += weight;
    #endif
  }
  if (dust_weight > 0.) {
    #pragma omp atomic
    bin_f_abs[frequency_bin] += dust_weight; // Global dust absorption fraction
    #pragma omp atomic
    bin_f2_abs[frequency_bin] += dust_weight * dust_weight; // Squared f_abs: sum(w^2)

    // Group statistics
    #if output_groups
      if (output_grp_obs && valid_igrp) {
        #pragma omp atomic
        bin_f_abs_grp(igrp, frequency_bin) += dust_weight; // Bin absorption fraction
        #pragma omp atomic
        bin_f2_abs_grp(igrp, frequency_bin) += dust_weight * dust_weight; // Bin squared f_abs
      }
    #endif

    // Subhalo statistics
    #if output_subhalos
      if (output_sub_obs && valid_isub) {
        #pragma omp atomic
        bin_f_abs_sub(isub, frequency_bin) += dust_weight; // Bin absorption fraction
        #pragma omp atomic
        bin_f2_abs_sub(isub, frequency_bin) += dust_weight * dust_weight; // Bin squared f_abs
      }
    #endif
  }
  if (ion_weight[HI_ION] > 0.) {
    #pragma omp atomic
    bin_f2_HI[frequency_bin] += ion_weight[HI_ION] * ion_weight[HI_ION]; // Squared f_HI: sum(w^2)
  }
  for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
    double sum_weight = 0.;                  // Sum of weights for this atom
    const int ion_count = ion_counts[aa];    // Number of active ions
    for (int j = 0; j < ion_count; ++j, ++ai) {
      if (ion_weight[ai] > 0.) {
        sum_weight += ion_weight[ai];
        #pragma omp atomic
        bin_f_ions[ai][frequency_bin] += ion_weight[ai]; // Global absorption fraction
      }
    }
    ion_weight[n_active_ions + aa] = sum_weight; // Save sum of weights for this atom
  }
  dist_abs /= (l_tot * source_weight);       // Absorption distance correction
  if (dist_abs > 0.) {
    #pragma omp atomic
    bin_l_abs[frequency_bin] += dist_abs * source_weight; // Weighted sum
  }
}
