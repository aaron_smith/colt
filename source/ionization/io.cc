/********************
 * ionization/io.cc *
 ********************

 * All I/O operations related to ionization radiative transfer.

*/

#include "proto.h"
#include "Ionization.h"
#include "../io_hdf5.h" // HDF5 read/write functions
#include <cctype>       // std::toupper

Vec3 cell_center(const int cell); // Center of the cell
void write_sim_info(const H5File& f); // Write general simulation information

/* Helper function to return the dust species trailing tags. */
dust_strings get_dust_tags() {
  dust_strings tags;
  #if graphite_dust
    tags[GRAPHITE] = "_graphite";
  #endif
  #if silicate_dust
    tags[SILICATE] = "_silicate";
  #endif
  #if graphite_scaled_PAH
    tags[PAH] = "_PAH";
  #endif
  #if !graphite_dust && !silicate_dust && !graphite_scaled_PAH
    tags[0] = "";
  #endif
  return tags;
}

static void print_f_sum_ions(string name, vector<double>& f_sum_ions) {
  name[0] = std::toupper(name[0]);
  cout << "\n" << std::setw(11) << name << " ions = " << omp_sum(f_sum_ions);
  if (f_sum_ions.size() > 1) { cout << "  ("; print(f_sum_ions, 1.); cout << ")"; }
}

/* Print ionization spectral data. */
static void print_ion_bin(IonBinData& ion) {
  // Ionizing cross-sections [cm^2]
  for (int ai = 0; ai < n_active_ions; ++ai)
    print("sigma_" + ion_names[active_ions[ai]], ion.sigma_ions[ai], "cm^2");
  // Photoheating [erg]
  for (int ai = 0; ai < n_active_ions; ++ai)
    print("epsilon_" + ion_names[active_ions[ai]], ion.epsilon_ions[ai], "eV", eV);
  print("avg energy", ion.mean_energy, "eV", eV);
}

/* Write ionization spectral data to a specified group or file. */
template <typename File_or_Group>
static void write_ion_bin(const File_or_Group& f, IonBinData& ion) {
  write(f, "L", ion.L);                      // Luminosity [erg/s]
  write(f, "Ndot", ion.Ndot);                // Photon rate [photons/s]
  write(f, "L_HI", ion.HI.L);                // Luminosity (> 13.6 eV) [erg/s]
  write(f, "Ndot_HI", ion.HI.Ndot);          // Photon rate (> 13.6 eV) [photons/s]
  write(f, "L", ion.bin_L, "erg/s");         // Bin luminosities [erg/s]
  write(f, "Ndot", ion.bin_Ndot, "photons/s"); // Bin rates [photons/s]
  for (int ai = 0; ai < n_active_ions; ++ai) {
    write(f, "sigma_" + ion_names[active_ions[ai]], ion.sigma_ions[ai], "cm^2"); // Ionizing cross-sections [cm^2]
    write(f, "epsilon_" + ion_names[active_ions[ai]], ion.epsilon_ions[ai], "erg"); // Photoheating [erg]
  }
  write(f, "mean_energy", ion.mean_energy, "erg");
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    const string tag = tags[i];
    write(f, "kappa" + tag, ion.kappas[i], "cm^2/g dust"); // Dust opacity [cm^2/g dust]
    write(f, "albedo" + tag, ion.albedos[i]); // Dust albedo for scattering vs. absorption
    write(f, "cosine" + tag, ion.cosines[i]); // Anisotropy parameter: <μ> for dust scattering
  }
}

/* Helper function to print extended ion stat/weight names. */
static inline string get_ion_name(const int ais) {
  const int ai = active_ion_stats[ais];      // Active ion index
  if (ai < 0 || ai >= n_active_ions_atoms) { // Bounds check
    if (ai == STATS_tot) return "tot";       // Total
    if (ai == STATS_gas) return "gas";       // Gas
    if (ai == STATS_dust) return "dust";     // Dust
    error("Invalid value: active_ion_{stats,weight}[" + to_string(ais) + "] = " + to_string(ai));
  }
  if (ai < n_active_ions)
    return ion_names[active_ions[ai]];       // Valid ion name
  const int aa = ai - n_active_ions;         // Active atom index
  return atom_symbols[active_atoms[aa]];     // Valid atom name
}

/* Print additional ionization radiative transfer information. */
void Ionization::print_info() {
  // Print active atoms and ions
  if (true) { // verbose) {
    cout << "\nAtoms: (" << atom_names[active_atoms[0]];
    for (int aa = 1; aa < n_active_atoms; ++aa)
      cout << ", " << atom_names[active_atoms[aa]];
    cout << ")  [" << n_active_atoms << "]\nIons: (" << ion_names[active_ions[0]];
    for (int ai = 1; ai < n_active_ions; ++ai)
      cout << ", " << ion_names[active_ions[ai]];
    cout << ")  [" << n_active_ions << "]" << endl;
    if (n_active_ion_stats > 0) {
      cout << "Ion statistics: (" << get_ion_name(0);
      for (int ais = 1; ais < n_active_ion_stats; ++ais)
        cout << ", " << get_ion_name(ais);
      cout << ")  [" << n_active_ion_stats << "]" << endl;
    }
    if (n_active_ion_weights > 0) {
      cout << "Ion weights: (" << get_ion_name(0);
      for (int aiw = 1; aiw < n_active_ion_weights; ++aiw)
        cout << ", " << get_ion_name(aiw);
      cout << ")  [" << n_active_ion_weights << "]" << endl;
    }
  }

  // Print photon parameters
  cout << "\nPhoton parameters: [" << source_model << "]"
       << "\n  n_bins     = " << n_bins
       << "\n  n_photons  = " << n_photons << endl;
  #if spherical_escape
    cout << "  r_escape   = " << escape_radius / kpc << " kpc\n"
         << "  r_emission = " << emission_radius / kpc << " kpc\n"
         << "  esc center = " << escape_center / kpc << " kpc" << endl;
  #endif
  #if box_escape
    cout << "  box emission = [" << emission_box[0] / kpc << ", " << emission_box[1] / kpc << "] kpc\n"
         << "  box escape = [" << escape_box[0] / kpc << ", " << escape_box[1] / kpc << "] kpc" << endl;
  #endif
  #if streaming_escape
    cout << "  streaming  = " << max_streaming / kpc << " kpc" << endl;
  #endif

  // Ionizing photon models
  if (!bin_names.empty())
    print("bin_names", bin_names);
  if (!bin_edges_eV.empty())
    print("bin_edges", bin_edges_eV, "eV");
  if (verbose)
    print_ion_bin(ion_global);               // Global ionization spectral data

  if (verbose && n_ion_source_types > 1) {
    if (star_based_emission ) {
      cout << "\nStar spectral properties:" << endl;
      print_ion_bin(ion_stars);              // Stellar ionization spectral data
    }

    if (cell_based_emission) {
      cout << "\nGas spectral properties:" << endl;
      print_ion_bin(ion_gas);                // Gas ionization spectral data
    }

    if (AGN_emission) {
      cout << "\nAGN spectral properties: [" << AGN_model << "]" << endl;
      print("sigma_HI", sigma_HI_AGN, "cm^2");
      print("sigma_HeI", sigma_HeI_AGN, "cm^2");
      print("sigma_HeII", sigma_HeII_AGN, "cm^2");
      print("epsilon_HI", epsilon_HI_AGN, "eV", eV);
      print("epsilon_HeI", epsilon_HeI_AGN, "eV", eV);
      print("epsilon_HeII", epsilon_HeII_AGN, "eV", eV);
      print("avg energy", mean_energy_AGN, "eV", eV);
    }
  }

  // UV background
  if (have_UVB)
    cout << "\nUV background parameters: [" << UVB_model << "]"
         << "\n  Gamma_HI   = " << UVB_rate_HI << " 1/s"
         << "\n  Gamma_HeI  = " << UVB_rate_HeI << " 1/s"
         << "\n  Gamma_HeII = " << UVB_rate_HeII << " 1/s"
         << "\n  Heat_HI    = " << UVB_heat_HI << " erg/s"
         << "\n  Heat_HeI   = " << UVB_heat_HeI << " erg/s"
         << "\n  Heat_HeII  = " << UVB_heat_HeII << " erg/s" << endl;

  cout << "\nDust properties:" << (multiple_dust_species ? " [" : " ");
  for (int i = 0; i < n_dust_species; ++i) {
    if (i > 0) cout << ", ";
    if (dust_models[i] != "")
      cout << get_filename(dust_models[i]);
  }
  cout << (multiple_dust_species ? "] models" : " model") << endl;
  if (f_ion < 1.) cout << "  f_ion      = " << f_ion << endl;
  if (f_dust > 0.) cout << "  f_dust     = " << f_dust << endl;
  #if graphite_scaled_PAH
    cout << "  f_PAH      = " << f_PAH << endl;
  #endif
  if (T_sputter < 1e6) cout << "  T_sputter  = " << T_sputter << " K" << endl;
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    if (!tags[i].empty())
      cout << "\nMulti-species dust: " << tags[i].substr(1) << endl;
    print("kappas", ion_global.kappas[i], "cm^2/g dust");
    print("albedos", ion_global.albedos[i]);
    print("cosines", ion_global.cosines[i]);
    if (verbose && n_ion_source_types > 1) {
      if (star_based_emission) {
        print("kappas_stars", ion_stars.kappas[i], "cm^2/g dust");
        print("albedos_stars", ion_stars.albedos[i]);
        print("cosines_stars", ion_stars.cosines[i]);
      }
      if (cell_based_emission) {
        print("kappas_gas", ion_gas.kappas[i], "cm^2/g dust");
        print("albedos_gas", ion_gas.albedos[i]);
        print("cosines_gas", ion_gas.cosines[i]);
      }
      if (AGN_emission && i == 0) {
        print("kappas_AGN", kappa_AGN, "cm^2/g dust");
        print("albedos_AGN", albedo_AGN);
        print("cosines_AGN", cosine_AGN);
      }
    }
  }

  if (star_based_emission) {
    cout << "\nEmission includes stellar sources:  (Ndot_stars/Ndot_tot = "
         << ion_stars.Ndot / ion_global.Ndot << ")\n  <n_stars>  = " << n_stars_eff;
    if (HI_bin > 0) cout << " | [>13.6 eV] = " << n_stars_eff_HI;
    if (verbose && n_bins > 1) print(" |   bins", bin_n_stars_eff); else cout << endl;
    if (n_ion_source_types > 1) {
      cout << "\n  Luminosity = " << ion_stars.L << " erg/s = " << ion_stars.L / Lsun << " Lsun"
           << "\n  Prod. Rate = " << ion_stars.Ndot << " photons/s" << endl;
      if (HI_bin > 0) {
        cout << "  [>13.6 eV] = " << ion_stars.HI.Ndot << " photons/s  (" << 100.*ion_stars.HI.Ndot/ion_stars.Ndot << "%)";
        if (j_exp != 1. || nu_exp != 0. || min_HI_bin_cdf > 0.)
          cout << " (Boosted -> " << 100.*(1. - ion_stars.bin_cdf[HI_bin-1]) << "%)";
        cout << endl;
      }
      if (verbose) {
        if (n_bins > 1) {
          print("Lum (bins)", bin_L_stars, "erg/s");
          print("          ", bin_L_stars, "Lsun", Lsun);
          print("Prod rates", bin_Ndot_stars, "photons/s");
        }
        print("j_cdf", j_cdf_stars);
        print("j_weights", j_weights_stars);
      }
    }
  }

  if (cell_based_emission) {
    cout << "\nEmission includes gas sources:  (Ndot_gas/Ndot_tot = "
         << ion_gas.Ndot / ion_global.Ndot << ")\n  <n_cells>  = " << n_cells_eff;
    if (HI_bin > 0) cout << " | [>13.6 eV] = " << n_cells_eff_HI;
    if (verbose && n_bins > 1) print(" |   bins", bin_n_cells_eff); else cout << endl;
    if (n_ion_source_types > 1) {
      cout << "\n  Luminosity = " << ion_gas.L << " erg/s = " << ion_gas.L / Lsun << " Lsun"
           << "\n  Prod. Rate = " << ion_gas.Ndot << " photons/s" << endl;
      if (HI_bin > 0) {
        cout << "  [>13.6 eV] = " << ion_gas.HI.Ndot << " photons/s  (" << 100.*ion_gas.HI.Ndot/ion_gas.Ndot << "%)";
        if (j_exp != 1. || nu_exp != 0. || min_HI_bin_cdf > 0. || V_exp != 1.)
          cout << " (Boosted -> " << 100.*(1. - ion_gas.bin_cdf[HI_bin-1]) << "%)";
        cout << endl;
      }
      if (verbose) {
        if (n_bins > 1) {
          print("Lum (bins)", bin_L_gas, "erg/s");
          print("          ", bin_L_gas, "Lsun", Lsun);
          print("Prod rates", bin_Ndot_gas, "photons/s");
        }
        print("j_cdf", j_cdf_cells);
        print("j_weights", j_weights_cells);
      }
    }
  }

  if (AGN_emission) {
    cout << "\nEmission includes an AGN source: (Ndot_AGN/Ndot_tot = " << Ndot_AGN / ion_global.Ndot << ")"
         << "\n    Position = " << to_string(r_AGN / kpc) << " kpc";
    if (AGN_model == "Stalevski12" || AGN_model == "Lusso15")
      cout << "\n  Bolometric = " << Lbol_AGN << " erg/s = " << Lbol_AGN / Lsun << " Lsun";
    if (n_ion_source_types > 1) {
      cout << "\n  Luminosity = " << L_AGN << " erg/s = " << L_AGN / Lsun << " Lsun"
           << "\n  Prod. Rate = " << Ndot_AGN << " photons/s" << endl;
      // if (HI_bin > 0) cout << "\n  [>13.6 eV] = " << Ndot_AGN_HI << " photons/s  (" << 100.*Ndot_AGN_HI/Ndot_AGN << "%)" << endl;
      if (verbose) {
        print("Lum (bins)", bin_L_AGN, "erg/s");
        print("          ", bin_L_AGN, "Lsun", Lsun);
        print("Prod rates", bin_Ndot_AGN, "photons/s");
      }
    }
  }

  if (point_source) {
    cout << "\nEmission includes a point source: (Ndot_point/Ndot_tot = " << ion_point.Ndot / ion_global.Ndot << ")"
         << "\n  r_point    = " << r_point / pc << " pc"
         << "\n  T_point    = " << T_point << " K"
         << "\n  Lbol_point = " << Lbol_point << " erg/s"
         << "\n  L_point    = " << ion_point.L << " erg/s"
         << "\n  Ndot_point = " << ion_point.Ndot << " photons/s";
    if (n_ion_source_types > 1 && HI_bin > 0) {
      cout << "  [>13.6 eV] = " << ion_point.HI.Ndot << " photons/s  (" << 100.*ion_point.HI.Ndot/ion_point.Ndot << "%)";
      if (j_exp != 1. || nu_exp != 0. || min_HI_bin_cdf > 0.)
        cout << " (Boosted -> " << 100.*(1. - ion_point.bin_cdf[HI_bin-1]) << "%)";
      cout << endl;
    }
    if (verbose)
      cout << "\n  i_point    = " << i_point
           << "\nConsistency check:"
           << "\n  r[i_point] = " << cell_center(i_point) / pc << " pc";
    cout << endl;
    if (verbose && n_ion_source_types > 1) {
      print("Lum (bins)", ion_point.bin_L, "erg/s");
      print("          ", ion_point.bin_L, "Lsun", Lsun);
      print("Prod rates", ion_point.bin_Ndot, "photons/s");
    }
  }

  if (plane_source) {
    cout << "\nEmission includes a plane source: (Ndot_plane/Ndot_tot = " << ion_plane.Ndot / ion_global.Ndot << ")"
         << "\n  direction  = " << plane_direction
         << "\n  position   = " << plane_position / kpc << " kpc"
         << "\n  center     = (" << plane_center_1 / kpc << ", " << plane_center_2 / kpc << ") kpc"
         << "\n  radii      = (" << plane_radius_1 / kpc << ", " << plane_radius_2 / kpc << ") kpc"
         << "\n  plane area = " << plane_area / (kpc * kpc) << " kpc^2";
    if (source_file_Z_age != "")
      cout << "\n  Z_age_file = " << source_file_Z_age
           << "\n  single_Z   = " << single_Z
           << "\n  single_age = " << single_age << " Gyr";
    else
      cout << "\n  T_plane    = " << T_plane << " K";
    cout << "\n  Sbol_plane = " << Sbol_plane << " erg/s/cm^2"
         << "\n  Lbol_plane = " << Lbol_plane << " erg/s"
         << "\n  L_plane    = " << ion_plane.L << " erg/s"
         << "\n  Ndot_plane = " << ion_plane.Ndot << " photons/s" << endl;
    if (n_ion_source_types > 1 && HI_bin > 0) {
      cout << "  [>13.6 eV] = " << ion_plane.HI.Ndot << " photons/s  (" << 100.*ion_plane.HI.Ndot/ion_plane.Ndot << "%)";
      if (j_exp != 1. || nu_exp != 0. || min_HI_bin_cdf > 0.)
        cout << " (Boosted -> " << 100.*(1. - ion_plane.bin_cdf[HI_bin-1]) << "%)";
      cout << endl;
    }
    if (plane_beam)
      cout << "  Using a beam instead of a rectangle." << endl;
    if (verbose && n_ion_source_types > 1) {
      print("Lum (bins)", ion_plane.bin_L, "erg/s");
      print("          ", ion_plane.bin_L, "Lsun", Lsun);
      print("Prod rates", ion_plane.bin_Ndot, "photons/s");
    }
  }

  if (n_ion_source_types > 1)
    cout << "\nMixed source calculation uses: " << to_string(ion_source_names_mask)
         << "\n  mixed pdf  = " << to_string(mixed_ion_pdf_mask)
         << "\n  boost cdf  = " << to_string(mixed_ion_cdf_mask)
         << "\n   weights   = " << to_string(mixed_ion_weights_mask) << endl;

  cout << "\nStrength of all sources:"
       << "\n  Luminosity = " << ion_global.L << " erg/s = " << ion_global.L / Lsun << " Lsun"
       << "\n  Prod. Rate = " << ion_global.Ndot << " photons/s" << endl;
  if (HI_bin > 0) {
    cout << "  [>13.6 eV] = " << ion_global.HI.Ndot << " photons/s  (" << 100.*ion_global.HI.Ndot/ion_global.HI.Ndot << "%)";
    if (j_exp != 1. || nu_exp != 0. || min_HI_bin_cdf > 0. || (cell_based_emission && V_exp != 1.))
      cout << " (Boosted -> " << 100.*(1. - ion_global.bin_cdf[HI_bin-1]) << "%)";
    cout << endl;
  }
  if (nu_exp != 0. || min_HI_bin_cdf > 0.)
    cout << "  [nu_boost] = " << ion_global.boosted_Ndot / ion_global.Ndot << "  (nu_exp = " << nu_exp << ")" << endl;
  if (verbose && n_bins > 1) {
    print("Lum (bins)", bin_L_tot, "erg/s");
    print("          ", bin_L_tot, "Lsun", Lsun);
    print("Prod rates", bin_Ndot_tot, "photons/s");
  }

  // Ionization statistics: cell, age, and frequency
  if constexpr (output_ion_cell_age_freq) {
    cout << "\nSetup for ion(cell, age, freq):"
         << "\n  n_age_bins = " << ion_cell_age_freq.n_age_bins
         << "\n n_freq_bins = " << ion_cell_age_freq.n_freq_bins << endl;
    print("age_edges", ion_cell_age_freq.age_edges, "Myr", 1e-3);
    print("bin_index", ion_cell_age_freq.freq_edges);
    print("bin_names", ion_cell_age_freq.freq_names);
    print("bin_edges", ion_cell_age_freq.freq_edges_eV, "eV");
  }

  // Ionization statistics: radial, age, and frequency
  if constexpr (output_ion_radial_age_freq) {
    cout << "\nSetup for ion(radial, age, freq):"
         << "\n  n_rad_bins = " << ion_radial_age_freq.n_radial_bins
         << "\n  n_age_bins = " << ion_radial_age_freq.n_age_bins
         << "\n n_freq_bins = " << ion_radial_age_freq.n_freq_bins << endl;
    print("rad_edges", ion_radial_age_freq.radial_edges, "pc", pc);
    print("age_edges", ion_radial_age_freq.age_edges, "Myr", 1e-3);
    print("bin_index", ion_radial_age_freq.freq_edges);
    print("bin_names", ion_radial_age_freq.freq_names);
    print("bin_edges", ion_radial_age_freq.freq_edges_eV, "eV");
  }

  // Ionization statistics: distance, age, and frequency
  if constexpr (output_ion_distance_age_freq) {
    cout << "\nSetup for ion(distance, age, freq):"
         << "\n n_dist_bins = " << ion_distance_age_freq.n_distance_bins
         << "\n  n_age_bins = " << ion_distance_age_freq.n_age_bins
         << "\n n_freq_bins = " << ion_distance_age_freq.n_freq_bins << endl;
    print("dist_edges", ion_distance_age_freq.distance_edges, "pc", pc);
    print("age_edges", ion_distance_age_freq.age_edges, "Myr", 1e-3);
    print("bin_index", ion_distance_age_freq.freq_edges);
    print("bin_names", ion_distance_age_freq.freq_names);
    print("bin_edges", ion_distance_age_freq.freq_edges_eV, "eV");
  }

  // Radial flow statistics
  if constexpr (output_radial_flow) {
    cout << "\nSetup for radial flow:"
         << "\n  n_rad_bins = " << radial_flow.n_radial_bins
         << "\n n_freq_bins = " << radial_flow.n_freq_bins << endl;
    print("rad_edges", radial_flow.radial_edges, "pc", pc);
    print("bin_index", radial_flow.freq_edges);
    print("bin_names", radial_flow.freq_names);
    print("bin_edges", radial_flow.freq_edges_eV, "eV");
  }

  // Group flow statistics
  if constexpr (output_group_flows) {
    if (n_groups > 0) {
      auto& g0 = group_flows[0];             // Group 0 flow
      cout << "\nSetup for group flows: [n_radial_bins = " << g0.n_radial_bins
          << ", n_freq_bins = " << g0.n_freq_bins << "]" << endl;
      print("bin_index", g0.freq_edges);
      print("bin_names", g0.freq_names);
      print("bin_edges", g0.freq_edges_eV, "eV");
      const int n_groups_print = (n_groups > 5) ? 5 : n_groups;
      for (int i = 0; i < n_groups_print; ++i) {
        string name = "g" + to_string(group_id[i]) + " radii";
        print(name, group_flows[i].radial_edges, "kpc", kpc);
      }
      if (n_groups > 5)
        cout << "  ... and " << n_groups - 5 << " more groups." << endl;
    }
  }

  // Subhalo flow statistics
  if constexpr (output_subhalo_flows) {
    if (n_subhalos > 0) {
      auto& s0 = subhalo_flows[0];           // Subhalo 0 flow
      cout << "\nSetup for subhalo flows: [n_radial_bins = " << s0.n_radial_bins
          << ", n_freq_bins = " << s0.n_freq_bins << "]" << endl;
      print("bin_index", s0.freq_edges);
      print("bin_names", s0.freq_names);
      print("bin_edges", s0.freq_edges_eV, "eV");
      const int n_subhalos_print = (n_subhalos > 5) ? 5 : n_subhalos;
      for (int i = 0; i < n_subhalos_print; ++i) {
        string name = "s" + to_string(subhalo_id[i]) + " radii";
        print(name, subhalo_flows[i].radial_edges, "kpc", kpc);
      }
      if (n_subhalos > 5)
        cout << "  ... and " << n_subhalos - 5 << " more subhalos." << endl;
    }
  }

  // Print camera parameters
  if (have_cameras)
    print_cameras();                         // General camera parameters

  // Print map parameters
  if (output_map) {
    const int n_pix_map = 12 * n_side_map * n_side_map; // Number of pixels
    cout << "\nHealpix map parameters:"
         << "\n  n_side     = " << n_side_map
         << "\n  n_pix_map  = " << n_pix_map;
  }

  // Print radial map parameters
  if (output_radial_map) {
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // Number of pixels
    cout << "\nHealpix radial map parameters:"
         << "\n  n_side     = " << n_side_radial
         << "\n  n_pix_map  = " << n_pix_radial
         << "\n  n_pixels   = " << n_map_pixels
         << "\n  radius     = " << map_radius / kpc << " kpc"
         << "\n  pixel dr   = " << map_pixel_width / pc << " pc" << endl;
    if (cosmological)
      cout << "  pixel dl   = " << map_pixel_arcsec << " arcsec" << endl;
  }

  // Print radial cube map parameters
  if (output_cube_map) {
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // Number of pixels
    cout << "\nHealpix radial cube map parameters:"
         << "\n  n_side     = " << n_side_cube
         << "\n  n_pix_map  = " << n_pix_cube
         << "\n  n_pixels   = " << n_cube_map_pixels
         << "\n  radius     = " << cube_map_radius / kpc << " kpc"
         << "\n  pixel dr   = " << cube_map_pixel_width / pc << " pc" << endl;
    if (cosmological)
      cout << "  pixel dl   = " << cube_map_pixel_arcsec << " arcsec" << endl;
  }
}

/* Function to print additional data. */
void print_ionization_observables() {
  cout << "\nGlobal statistics:"
       << "\n Escape fraction = " << f_esc;
  if (HI_bin > 0) cout << "  (>13.6 eV = " << f_esc_HI << ")";
  cout << "\n Source fraction = " << f_src;
  if (HI_bin > 0) cout << "  (>13.6 eV = " << f_src_HI << ")";
  cout << "\n Eff. n_phot_src = " << n_photons_src;
  if (HI_bin > 0) cout << "  (>13.6 eV = " << n_photons_src_HI << ")";
  cout << "\n Eff. n_phot_esc = " << n_photons_src;
  if (HI_bin > 0) cout << "  (>13.6 eV = " << n_photons_src_HI << ")";
  cout << "\n Dust absorption = " << f_abs;
  if (HI_bin > 0) cout << "  (>13.6 eV = " << f_abs_HI << ")";
  for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
    const int ion_count = ion_counts[aa]; // Number of active ions
    vector<double> f_sum_ions; f_sum_ions.reserve(ion_count);
    for (int j = 0; j < ion_count; ++j, ++ai)
      f_sum_ions.push_back(f_ions[ai]);
    print_f_sum_ions(atom_names[active_atoms[aa]], f_sum_ions);
  }
  cout << "\n Dist absorption = " << l_abs / pc << " pc" << endl;
  if (verbose && n_bins > 1) {
    print("Bin f_esc", bin_f_esc);
    print("Bin f_abs", bin_f_abs);
    for (int ai = 0; ai < n_active_ions; ++ai)
      print("Bin f_" + ion_names[active_ions[ai]], bin_f_ions[ai]);
    print("Bin l_abs", bin_l_abs, "pc", pc);
  }

  if (output_escape_fractions) {
    cout << "\nCamera escape fractions:" << endl;
    print("f_esc (LOS)", f_escs);
    if (output_mcrt_attenuation)
      print("f_esc (ext)", f_escs_ext);
  }
}

/* Write general simulation information. */
void Ionization::write_ionization_info(const H5File& f) {
  // Simulation information
  write(f, "n_bins", n_bins);                // Number of frequency bins
  write(f, "HI_bin", HI_bin);                // HI bin index
  write(f, "n_photons", n_photons);          // Actual number of photon packets used
  const int n_iter = x_V_ions[0].size() - 1;
  write(f, "n_iter", n_iter);                // Number of iterations performed
  write(f, "max_iter", max_iter);            // Maximum number of iterations
  write(f, "max_error", max_error);          // Relative error for convergence
  write(f, "Ndot_tot", ion_global.Ndot);     // Total emission rate [photons/s]
  write(f, "L_tot", ion_global.L);           // Total luminosity [erg/s]
  write(f, "f_src", f_src);                  // Global source fraction: sum(w0)
  write(f, "n_photons_src", n_photons_src);  // Effective number of emitted photons: 1/<w0>
  write(f, "f_esc", f_esc);                  // Global escape fraction: sum(w)
  write(f, "n_photons_esc", n_photons_esc);  // Effective number of escaped photons: 1/<w>
  write(f, "f_abs", f_abs);                  // Global dust absorption fraction
  write(f, "n_photons_abs", n_photons_abs);  // Effective number of absorbed photons: 1/<w>
  write(f, "Ndot_tot_HI", ion_global.HI.Ndot); // HI stats (> 13.6 eV)
  write(f, "L_tot_HI", ion_global.HI.L);
  write(f, "f_src_HI", f_src_HI);            // Emitted
  write(f, "n_photons_src_HI", n_photons_src_HI);
  write(f, "f_esc_HI", f_esc_HI);            // Escaped
  write(f, "n_photons_esc_HI", n_photons_esc_HI);
  write(f, "f_abs_HI", f_abs_HI);            // Absorbed
  write(f, "n_photons_abs_HI", n_photons_abs_HI);
  for (int ai = 0; ai < n_active_ions; ++ai)
    write(f, "f_" + ion_names[active_ions[ai]], f_ions[ai]); // Global species absorption fractions
  write(f, "n_photons_HI", n_photons_HI);    // Effective number of photons: 1/<w>
  write(f, "l_abs", l_abs);                  // Global absorption distance [cm]

  // Global bin information
  {
    Group g = f.createGroup("/bin");
    write(g, "edges_eV", bin_edges_eV);      // Bin energy edges [eV]
    if (n_bins < 300)
      write(g, "names", bin_names);          // Bin energy edge names
    write(g, "f_src", bin_f_src);            // Source fractions: sum(w0)
    write(g, "n_photons_src", bin_n_photons_src); // Emitted photons: 1/<w0>
    write(g, "f_esc", bin_f_esc);            // Escape fractions: sum(w)
    write(g, "n_photons_esc", bin_n_photons_esc); // Escaped photons: 1/<w>
    write(g, "f_abs", bin_f_abs);            // Dust absorption fraction
    write(g, "n_photons_abs", bin_n_photons_abs); // Absorbed photons: 1/<w>
    for (int ai = 0; ai < n_active_ions; ++ai)
      write(g, "f_" + ion_names[active_ions[ai]], bin_f_ions[ai]); // Species absorption fractions
    write(g, "n_photons_HI", bin_n_photons_HI); // Ionization photons: 1/<w>
    write(g, "l_abs", bin_l_abs, "cm");      // Absorption distance [cm]
    write_ion_bin(g, ion_global);            // Write ionization spectral data
    g = exists(f, "/dust") ? f.openGroup("/dust") : f.createGroup("/dust");
    dust_strings tags = get_dust_tags();     // Dust species trailing tags
    for (int i = 0; i < n_dust_species; ++i) {
      string tag = tags[i];
      write(g, "kappa" + tag, ion_global.kappas[i], "cm^2/g dust"); // Dust opacity [cm^2/g dust]
      write(g, "albedo" + tag, ion_global.albedos[i]); // Dust albedo for scattering vs. absorption
      write(g, "cosine" + tag, ion_global.cosines[i]); // Anisotropy parameter: <μ> for dust scattering
    }
  }

  // Information about convergence history
  {
    Group g = f.createGroup("/conv");
    write(g, "f_col_HI", f_col_HI);          // Fraction of total ionizations
    write(g, "f_UVB_HI", f_UVB_HI);
    for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
      const int ion_count = ion_counts[aa];  // Number of active ions
      for (int j = 0; j < ion_count; ++j, ++ai) {
        string name = "x_" + fields[x_indices[ai]].name;
        name[2] = 'V';                       // Replace 'x_x' with 'x_V'
        write(g, name, x_V_ions[ai]);        // Volume-weighted fractions
        name[2] = 'm';                       // Replace 'x_V' with 'x_m'
        write(g, name, x_m_ions[ai]);        // Mass-weighted fractions
      }
      const int last_ion = n_active_ions + aa; // Last ion index (implicitly tracked)
      string name = "x_" + fields[x_indices[ai-1]+1].name;
      name[2] = 'V';                         // Replace 'x_x' with 'x_V'
      write(g, name, x_V_ions[last_ion]);    // Volume-weighted fractions
      name[2] = 'm';                         // Replace 'x_V' with 'x_m'
      write(g, name, x_m_ions[last_ion]);    // Mass-weighted fractions
    }

    for (int ai = 0; ai < n_active_ions; ++ai) {
      const string ion_name = fields[x_indices[ai]].name.substr(2); // Lower state
      const string rec_name = fields[x_indices[ai]+1].name.substr(2); // Upper state
      write(g, "rec_" + rec_name, rec_ions[ai]); // Recombinations [events/s]
      write(g, "col_" + ion_name, col_ions[ai]); // Collisional ionizations [events/s]
      write(g, "phot_" + ion_name, phot_ions[ai]); // Photoionizations [events/s]
      if (cxi_H_ions[ai][n_iter] > 0.)       // Charge exchange ionizations (H) [events/s]
        write(g, "cxi_H_" + ion_name, cxi_H_ions[ai]);
      if (cxi_He_ions[ai][n_iter] > 0.)      // Charge exchange ionizations (He) [events/s]
        write(g, "cxi_He_" + ion_name, cxi_He_ions[ai]);
      if (cxr_H_ions[ai][n_iter] > 0.)       // Charge exchange recombinations (H) [events/s]
        write(g, "cxr_H_" + rec_name, cxr_H_ions[ai]);
      if (cxr_He_ions[ai][n_iter] > 0.)      // Charge exchange recombinations (He) [events/s]
        write(g, "cxr_He_" + rec_name, cxr_He_ions[ai]);
    }
  }

  // Information about stellar sources
  if (star_based_emission && n_ion_source_types > 1) {
    Group g = f.createGroup("/stars");
    write_ion_bin(g, ion_stars);             // Write ionization spectral data
  }

  // Information about gas sources
  if (cell_based_emission) {
    Group g = f.createGroup("/gas");
    write(g, "free_free", free_free);        // Free-free emission flag
    write(g, "free_bound", free_bound);      // Free-bound emission flag
    write(g, "two_photon", two_photon);      // Two-photon emission flag
    if (n_ion_source_types > 1)
      write_ion_bin(g, ion_gas);             // Write ionization spectral data
  }

  // Information about the AGN source
  if (AGN_emission && n_ion_source_types > 1) {
    Group g = f.createGroup("/AGN");
    write(g, "AGN_model", AGN_model);        // AGN model
    write(g, "r", r_AGN, "cm");              // AGN source position [cm]
    write(g, "Lbol", Lbol_AGN);              // AGN bolometric luminosity [erg/s]
    write(g, "L", L_AGN);                    // AGN luminosity [erg/s]
    write(g, "Ndot", Ndot_AGN);              // AGN photon rate [photons/s]
    // write(g, "L_HI", L_AGN_HI);              // AGN luminosity (> 13.6 eV) [erg/s]
    // write(g, "Ndot_HI", Ndot_AGN_HI);        // AGN photon rate (> 13.6 eV) [photons/s]
    write(g, "L", bin_L_AGN, "erg/s");       // Bin luminosities [erg/s]
    write(g, "Ndot", bin_Ndot_AGN, "photons/s"); // Bin rates [photons/s]
    write(g, "sigma_HI", sigma_HI_AGN, "cm^2"); // Ionizing cross-sections [cm^2]
    write(g, "sigma_HeI", sigma_HeI_AGN, "cm^2");
    write(g, "sigma_HeII", sigma_HeII_AGN, "cm^2");
    write(g, "epsilon_HI", epsilon_HI_AGN, "erg"); // Photoheating [erg]
    write(g, "epsilon_HeI", epsilon_HeI_AGN, "erg");
    write(g, "epsilon_HeII", epsilon_HeII_AGN, "erg");
    write(g, "mean_energy", mean_energy_AGN, "erg");
    write(g, "kappa", kappa_AGN, "cm^2/g dust"); // Dust opacity [cm^2/g dust]
    write(g, "albedo", albedo_AGN);          // Dust albedo for scattering vs. absorption
    write(g, "cosine", cosine_AGN);          // Anisotropy parameter: <μ> for dust scattering
  }

  if (point_source) {
    Group g = f.createGroup("/point");
    write(g, "r", r_point, "cm");            // Point source position [cm]
    write(g, "T", T_point);                  // Point source temperature [K]
    write(g, "Lbol", Lbol_point);            // Point bolometric luminosity [erg/s]
    if (n_ion_source_types > 1)
      write_ion_bin(g, ion_point);           // Write ionization spectral data
  }

  if (plane_source) {
    Group g = f.createGroup("/plane");
    write(g, "direction", plane_direction);  // Plane direction name
    write(g, "position", plane_position);    // Plane position [cm]
    write(g, "center_1", plane_center_1);    // Plane center positions
    write(g, "center_2", plane_center_2);
    write(g, "radius_1", plane_radius_1);    // Plane box/disk radii
    write(g, "radius_2", plane_radius_2);
    write(g, "area", plane_area);            // Plane area [cm^2]
    write(g, "beam", plane_beam);            // Use an ellipsoidal beam instead of a rectangle
    write(g, "T", T_plane);                  // Plane source temperature [K]
    write(g, "Sbol", Sbol_plane);            // Plane bolometric surface density [erg/s/cm^2]
    write(g, "Lbol", Lbol_plane);            // Plane bolometric luminosity [erg/s]
    if (n_ion_source_types > 1)
      write_ion_bin(g, ion_plane);           // Write ionization spectral data
  }

  // Information about emission sources
  {
    Group g = f.createGroup("/sources");
    if (star_based_emission) {
      write(g, "n_stars", n_stars);          // Number of stars
      write(g, "n_stars_eff", n_stars_eff);  // Effective number of stars: 1/<w>
      write(g, "n_stars_eff_HI", n_stars_eff_HI); // Effective number of stars (> 13.6 eV): 1/<w>
      write(g, "bin_n_stars_eff", bin_n_stars_eff); // Bin effective number of stars: 1/<w>
    }
    if (cell_based_emission) {
      write(g, "n_cells", n_cells);          // Number of cells
      write(g, "n_cells_eff", n_cells_eff);  // Effective number of cells: 1/<w>
      write(g, "n_cells_eff_HI", n_cells_eff_HI); // Effective number of cells (> 13.6 eV): 1/<w>
      write(g, "bin_n_cells_eff", bin_n_cells_eff); // Effective number of cells: 1/<w>
    }

    write(g, "n_source_types", n_ion_source_types); // Number of source types used
    write_attr(g, "source_types", ion_source_types_mask); // Source type mask indices
    write_attr(g, "source_names", ion_source_names_mask); // Source type names (masked)
    write_attr(g, "mixed_pdf", mixed_ion_pdf_mask); // Mixed source pdf (masked)
    write_attr(g, "mixed_cdf", mixed_ion_cdf_mask); // Mixed source cdf (masked)
    write_attr(g, "mixed_weights", mixed_ion_weights_mask); // Mixed source weights (masked)

    write(g, "spherical_escape", spherical_escape); // Photons escape from a sphere
    #if spherical_escape
      write(g, "escape_radius", escape_radius); // Radius for spherical escape [cm]
      write(g, "emission_radius", emission_radius); // Radius for spherical emission [cm]
      write(g, "escape_center", escape_center, "cm"); // Center of the escape region [cm]
    #endif
    #if box_escape
      write(g, "escape_box_min", escape_box[0], "cm"); // Escape bounding box [cm]
      write(g, "escape_box_max", escape_box[1], "cm");
      write(g, "emission_box_min", emission_box[0], "cm"); // Emission bounding box [cm]
      write(g, "emission_box_max", emission_box[1], "cm");
    #endif
    #if streaming_escape
      write(g, "max_streaming", max_streaming); // Maximum streaming distance [cm]
    #endif
  }
}

/* Write photon data to a specified group or file. */
template <typename File_or_Group>
static void write_photons(const File_or_Group& f) {
  write(f, "n_escaped", n_escaped);          // Number of escaped photons
  write(f, "n_absorbed", n_absorbed);        // Number of photons absorbed
  write(f, "source_id", source_ids);         // Emission cell or star index
  if (n_ion_source_types > 1)
    write(f, "source_type", source_types);   // Emission type specifier
  write(f, "source_weight", source_weights); // Photon weight at emission
  if (output_n_scat)
    write(f, "n_scat", n_scats);             // Number of scattering events
  write(f, "frequency_bin", freq_bins);      // Frequency at escape [bin]
  write(f, "weight", weights);               // Photon weight at escape
  write(f, "dust_weight", dust_weights);     // Weight removed by dust absorption
  // Weight removed by the ionization of each species
  for (int aiw = 0; aiw < n_active_ion_weights; ++aiw)
    write(f, get_ion_name(aiw) + "_weight", ion_weights[aiw]);
  if (output_absorption_distance) write(f, "dist_abs", dist_abss); // Photon absorption distance [cm]
  if (output_source_position) write(f, "source_position", source_positions, "cm"); // Position at emission [cm]
  write(f, "position", positions, "cm");     // Position at escape [cm]
  write(f, "direction", directions);         // Direction at escape
}

/* Write abundances data to a specified group or file. */
template <typename File_or_Group>
static void write_abundances(const File_or_Group& f) {
  for (int ai = 0; ai < n_active_ions; ++ai) {
    Field& field = fields[x_indices[ai]];
    overwrite(f, field.name, field.data);    // x_ion = n_ion / n_atom
  }
  overwrite(f, "x_HII", x_HII);              // x_HII = n_HII / n_H
  overwrite(f, "x_e", x_e);                  // Electron fraction: x_e = n_e / n_H
}

/* Write photoionization rate data to a specified group or file. */
template <typename File_or_Group>
static void write_photoionization(const File_or_Group& f) {
  // Species photoionization integrals [photons/s/carrier]
  for (int ai = 0; ai < n_active_ions; ++ai)
    overwrite(f, "rate_" + ion_names[active_ions[ai]], rate_ions[ai], "1/s");
}

/* Write number of absorptions by cell, age, and frequency to a specified group or file. */
template <typename File_or_Group>
static void write_ion_cell_age_freq(const File_or_Group& f) {
  // Histogram data
  if (exists(f, "ion_cell_age_freq")) {
    H5Ldelete(f.getId(), "ion_cell_age_freq", H5P_DEFAULT);
    cout << "\nWarning: Replacing ion_cell_age_freq group." << endl;
  }
  Group g = f.createGroup("/ion_cell_age_freq"); // Write to a separate group
  write(g, "n_age_bins", ion_cell_age_freq.n_age_bins);
  write(g, "n_freq_bins", ion_cell_age_freq.n_freq_bins);
  write(g, "age_edges", ion_cell_age_freq.age_edges, "Gyr");
  // write(g, "freq_names", ion_cell_age_freq.freq_names);
  write(g, "freq_indices", ion_cell_age_freq.freq_edges);
  write(g, "freq_edges_eV", ion_cell_age_freq.freq_edges_eV, "eV");
  // Write all active ionization statistics
  for (int ais = 0; ais < n_active_ion_stats; ++ais)
    write(g, get_ion_name(ais), ion_cell_age_freq.stats[ais], "photons/s");
}

/* Write number of absorptions by radial, age, and frequency to a specified group or file. */
template <typename File_or_Group>
static void write_ion_radial_age_freq(const File_or_Group& f) {
  // Histogram data
  if (exists(f, "ion_radial_age_freq")) {
    H5Ldelete(f.getId(), "ion_radial_age_freq", H5P_DEFAULT);
    cout << "\nWarning: Replacing ion_radial_age_freq group." << endl;
  }
  Group g = f.createGroup("/ion_radial_age_freq"); // Write to a separate group
  write(g, "n_radial_bins", ion_radial_age_freq.n_radial_bins);
  write(g, "n_age_bins", ion_radial_age_freq.n_age_bins);
  write(g, "n_freq_bins", ion_radial_age_freq.n_freq_bins);
  write(g, "radial_edges", ion_radial_age_freq.radial_edges, "cm");
  write(g, "age_edges", ion_radial_age_freq.age_edges, "Gyr");
  // write(g, "freq_names", ion_radial_age_freq.freq_names);
  write(g, "freq_indices", ion_radial_age_freq.freq_edges);
  write(g, "freq_edges_eV", ion_radial_age_freq.freq_edges_eV, "eV");
  // Write all active ionization statistics
  for (int ais = 0; ais < n_active_ion_stats; ++ais)
    write(g, get_ion_name(ais), ion_radial_age_freq.stats[ais], "photons/s");
}

/* Write number of absorptions by distance, age, and frequency to a specified group or file. */
template <typename File_or_Group>
static void write_ion_distance_age_freq(const File_or_Group& f) {
  // Histogram data
  if (exists(f, "ion_distance_age_freq")) {
    H5Ldelete(f.getId(), "ion_distance_age_freq", H5P_DEFAULT);
    cout << "\nWarning: Replacing ion_distance_age_freq group." << endl;
  }
  Group g = f.createGroup("/ion_distance_age_freq"); // Write to a separate group
  write(g, "n_distance_bins", ion_distance_age_freq.n_distance_bins);
  write(g, "n_age_bins", ion_distance_age_freq.n_age_bins);
  write(g, "n_freq_bins", ion_distance_age_freq.n_freq_bins);
  write(g, "distance_edges", ion_distance_age_freq.distance_edges, "cm");
  write(g, "age_edges", ion_distance_age_freq.age_edges, "Gyr");
  // write(g, "freq_names", ion_distance_age_freq.freq_names);
  write(g, "freq_indices", ion_distance_age_freq.freq_edges);
  write(g, "freq_edges_eV", ion_distance_age_freq.freq_edges_eV, "eV");
  // Write all active ionization statistics
  for (int ais = 0; ais < n_active_ion_stats; ++ais)
    write(g, get_ion_name(ais), ion_distance_age_freq.stats[ais], "photons/s");
}

/* Write radial flow statistics to a specified group or file. */
template <typename File_or_Group>
static void write_radial_flow(const File_or_Group& f) {
  // Histogram data
  if (exists(f, "radial_flow")) {
    H5Ldelete(f.getId(), "radial_flow", H5P_DEFAULT);
    cout << "\nWarning: Replacing radial_flow group." << endl;
  }
  Group g = f.createGroup("/radial_flow");   // Write to a separate group
  write(g, "n_radial_bins", radial_flow.n_radial_bins);
  write(g, "n_freq_bins", radial_flow.n_freq_bins);
  // write(g, "freq_names", radial_flow.freq_names);
  write(g, "freq_indices", radial_flow.freq_edges);
  write(g, "freq_edges_eV", radial_flow.freq_edges_eV, "eV");
  write(g, "radial_edges", radial_flow.radial_edges, "cm");
  write(g, "Ndot_src", radial_flow.src, "photons/s");
  write(g, "Ndot_esc", radial_flow.esc, "photons/s");
  for (int ais = 0; ais < n_active_ion_stats; ++ais)
    write(g, "Ndot_"+get_ion_name(ais), radial_flow.stats[ais], "photons/s");
  write(g, "Ndot_pass", radial_flow.pass, "photons/s");
  write(g, "Ndot_flow", radial_flow.flow, "photons/s");
}

/* Write group and subhalo flows statistics to a specified group or file. */
template <typename File_or_Group>
static void write_halo_flows(const File_or_Group& f, const bool is_group) {
  // Histogram data
  string name = is_group ? "group_flows" : "subhalo_flows";
  vector<int>& halo_id = is_group ? group_id : subhalo_id;
  vector<RadialFlow>& halo_flows = is_group ? group_flows : subhalo_flows;
  const int n_data = halo_flows.size();
  if (n_data == 0) return;                   // No data to write
  if (exists(f, name)) {
    H5Ldelete(f.getId(), name.c_str(), H5P_DEFAULT);
    cout << "\nWarning: Replacing " << name << " group." << endl;
  }
  Group g = f.createGroup(name);             // Write to a separate group
  write(g, "n_radial_bins", halo_flows[0].n_radial_bins);
  write(g, "n_freq_bins", halo_flows[0].n_freq_bins);
  // write(g, "freq_names", halo_flows[0].freq_names);
  write(g, "freq_indices", halo_flows[0].freq_edges);
  write(g, "freq_edges_eV", halo_flows[0].freq_edges_eV, "eV");
  if (!output_groups )
    write(g, "id", halo_id);                 // Halo IDs
  for (int i = 0; i < n_data; ++i) {
    Group g_i = g.createGroup(to_string(halo_id[i]));
    write(g_i, "radial_edges", halo_flows[i].radial_edges, "cm");
    write(g_i, "Ndot_src", halo_flows[i].src, "photons/s");
    write(g_i, "Ndot_esc", halo_flows[i].esc, "photons/s");
    for (int ais = 0; ais < n_active_ion_stats; ++ais)
      write(g_i, "Ndot_"+get_ion_name(ais), halo_flows[i].stats[ais], "photons/s");
    write(g_i, "Ndot_pass", halo_flows[i].pass, "photons/s");
    write(g_i, "Ndot_flow", halo_flows[i].flow, "photons/s");
  }
}

/* Writes ionization data and info to the specified hdf5 filename */
void Ionization::write_module(const H5File& f) {
  // Write general ionization info
  write_ionization_info(f);

  // Non-camera data
  if (output_radial_avg)
    write(f, "radial_avg", radial_avg, "photons/s/cm^2"); // Angle-averaged radial surface brightness [photons/s/cm^2]
  if (output_radial_cube_avg)
    write(f, "bin_radial_avg", radial_cube_avg, "photons/s/cm^2"); // Angle-averaged bin radial SB [photons/s/cm^2]

  // Additional camera info
  if (have_cameras) {
    if (output_escape_fractions)
      write(f, "f_escs", f_escs);            // Escape fractions [fraction]
    if (output_bin_escape_fractions)
      write(f, "bin_f_escs", bin_f_escs);    // Bin escape fractions [fraction]
    if (output_images)
      write(f, "images", images, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
    if (output_cubes)
      write(f, "bin_images", bin_images, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
    if (output_radial_images)
      write(f, "radial_images", radial_images, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
    if (output_radial_cubes)
      write(f, "bin_radial_images", bin_radial_images, "photons/s/cm^2"); // Bin radial SB images [photons/s/cm^2]

    // Intrinsic emission (mcrt)
    if (output_mcrt_emission) {
      if (output_images)
        write(f, "images_int", images_int, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
      if (output_cubes)
        write(f, "bin_images_int", bin_images_int, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
      if (output_radial_images)
        write(f, "radial_images_int", radial_images_int, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
      if (output_radial_cubes)
        write(f, "bin_radial_images_int", bin_radial_images_int, "photons/s/cm^2"); // Bin radial SB images [photons/s/cm^2]
    }

    // Attenuated emission  (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        write(f, "f_escs_ext", f_escs_ext);  // Escape fractions [fraction]
      if (output_bin_escape_fractions)
        write(f, "bin_f_escs_ext", bin_f_escs_ext); // Bin escape fractions [fraction]
      if (output_images)
        write(f, "images_ext", images_ext, "photons/s/cm^2"); // Surface brightness images [photons/s/cm^2]
      if (output_cubes)
        write(f, "bin_images_ext", bin_images_ext, "photons/s/cm^2"); // Bin SB images [photons/s/cm^2]
      if (output_radial_images)
        write(f, "radial_images_ext", radial_images_ext, "photons/s/cm^2"); // Radial surface brightness images [photons/s/cm^2]
      if (output_radial_cubes)
        write(f, "bin_radial_images_ext", bin_radial_images_ext, "photons/s/cm^2"); // Bin radial SB images [photons/s/cm^2]
    }
  }

  // Line-of-sight healpix maps
  if (output_map) {
    const int n_pix_map = 12 * n_side_map * n_side_map; // 12 n_side^2
    write(f, "n_side_map", n_side_map);      // Healpix n_side parameter
    write(f, "n_pix_map", n_pix_map);        // Number of map pixels
    write(f, "map", map);                    // Escape fraction map [fraction]
    if (output_map2)
      write(f, "map2", map2);                // Escape fraction^2 map [statistic]
  }

  // Line-of-sight healpix radial map
  if (output_radial_map) {
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // 12 n_side^2
    write(f, "n_side_radial", n_side_radial); // Healpix n_side parameter
    write(f, "n_pix_radial", n_pix_radial);  // Number of radial map pixels
    write(f, "radial_map", radial_map);      // Radial surface brightness map [erg/s/cm^2/arcsec^2]
    if (!have_cameras || n_map_pixels != n_radial_pixels || map_radius != radial_image_radius) {
      write(f, "n_map_pixels", n_map_pixels); // Number of radial map pixels
      write(f, "map_radius", map_radius);    // Radial map radius [cm] (defaults to half domain)
      write(f, "map_pixel_width", map_pixel_width); // Radial map pixel width [cm]
      write(f, "map_pixel_areas", map_pixel_areas, "cm^2"); // Radial map pixel areas [cm^2]
      if (cosmological) {
        write(f, "map_pixel_arcsec", map_pixel_arcsec); // Radial map pixel width [arcsec] (optional)
        write(f, "map_pixel_arcsec2", map_pixel_arcsec2, "arcsec^2"); // Radial map pixel areas [arcsec^2] (optional)
      }
    }
  }

  // Line-of-sight healpix radial cube map
  if (output_cube_map) {
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // 12 n_side^2
    write(f, "n_side_cube", n_side_cube);    // Healpix n_side parameter
    write(f, "n_pix_cube", n_pix_cube);      // Number of radial cube map pixels
    write(f, "bin_radial_map", cube_map);    // Bin radial surface brightness map [erg/s/cm^2/arcsec^2]
    if (!have_cameras || n_cube_map_pixels != n_radial_cube_pixels || cube_map_radius != radial_cube_radius) {
      write(f, "n_cube_map_pixels", n_cube_map_pixels); // Number of radial cube map pixels
      write(f, "cube_map_radius", cube_map_radius); // Radial cube map radius [cm] (defaults to half domain)
      write(f, "cube_map_pixel_width", cube_map_pixel_width); // Radial map pixel width [cm]
      write(f, "cube_map_pixel_areas", cube_map_pixel_areas, "cm^2"); // Radial cube map pixel areas [cm^2]
      if (cosmological) {
        write(f, "cube_map_pixel_arcsec", cube_map_pixel_arcsec); // Radial cube map pixel width [arcsec] (optional)
        write(f, "cube_map_pixel_arcsec2", cube_map_pixel_arcsec2, "arcsec^2"); // Radial cube map pixel areas [arcsec^2] (optional)
      }
    }
  }

  if (output_photons) {
    if (photon_file.empty()) {               // Output photons to the same file
      Group g = f.createGroup("/photons");
      write_photons(g);                      // Write to the photons group
    } else {                                 // Output a separate photon file
      H5File f_photons(output_dir + "/" + output_base + "_" + photon_file +
                       snap_str + halo_str + "." + output_ext, H5F_ACC_TRUNC);
      write_sim_info(f_photons);             // Write general simulation info
      write_ionization_info(f_photons);      // Write general ionization info
      write_photons(f_photons);              // Write to the main group
    }
  }

  if (output_stars) {
    Group g = exists(f, "/stars") ? f.openGroup("/stars") : f.createGroup("/stars"); // Stars group

    // Count the number of invalid stars
    int n_invalid_stars = 0;                 // Number of invalid stars
    #pragma omp parallel for reduction(+:n_invalid_stars)
    for (int i = 0; i < n_stars; ++i) {
      if (weight_stars[i] < -1.5)            // Set to -2 for invalid stars
        ++n_invalid_stars;
    }
    const int n_valid_stars = n_stars - n_invalid_stars; // Number of valid stars
    write(g, "n_valid_stars", n_valid_stars); // Number of valid stars
    write(g, "n_invalid_stars", n_invalid_stars); // Number of invalid stars
    if (n_invalid_stars > 0) {
      // Create valid and invalid star index arrays
      vector<int> valid_indices(n_valid_stars);
      vector<int> invalid_indices(n_invalid_stars);
      for (int i = 0, i_valid = 0, i_invalid = 0; i < n_stars; ++i) {
        if (weight_stars[i] < -1.5) {        // Invalid stars were set to -2
          invalid_indices[i_invalid++] = i;  // Store the invalid star index
        } else {
          // Move valid stars to the front
          source_weight_stars[i_valid] = source_weight_stars[i];
          weight_stars[i_valid] = weight_stars[i];
          Ndot_int_stars[i_valid] = Ndot_int_stars[i];
          if (output_grp_vir)
            weight_stars_grp_vir[i_valid] = weight_stars_grp_vir[i];
          if (output_grp_gal)
            weight_stars_grp_gal[i_valid] = weight_stars_grp_gal[i];
          if (output_sub_vir)
            weight_stars_sub_vir[i_valid] = weight_stars_sub_vir[i];
          if (output_sub_gal)
            weight_stars_sub_gal[i_valid] = weight_stars_sub_gal[i];
          valid_indices[i_valid++] = i;      // Store the valid star index
        }
      }
      // Resize the arrays to the valid size
      Ndot_int_stars.resize(n_valid_stars);
      source_weight_stars.resize(n_valid_stars);
      weight_stars.resize(n_valid_stars);
      rescale(source_weight_stars, ion_global.Ndot); // Convert to photons/s
      rescale(weight_stars, ion_global.Ndot);
      if (output_grp_vir) {
        weight_stars_grp_vir.resize(n_valid_stars);
        rescale_positive(weight_stars_grp_vir, ion_global.Ndot);
      }
      if (output_grp_gal) {
        weight_stars_grp_gal.resize(n_valid_stars);
        rescale_positive(weight_stars_grp_gal, ion_global.Ndot);
      }
      if (output_sub_vir) {
        weight_stars_sub_vir.resize(n_valid_stars);
        rescale_positive(weight_stars_sub_vir, ion_global.Ndot);
      }
      if (output_sub_gal) {
        weight_stars_sub_gal.resize(n_valid_stars);
        rescale_positive(weight_stars_sub_gal, ion_global.Ndot);
      }
      // Write the lesser of the two arrays
      if (n_invalid_stars < n_valid_stars) {
        if (n_invalid_stars > 0)
          write(g, "invalid_indices", invalid_indices); // Invalid star indices
      } else {
        if (n_valid_stars > 0)
          write(g, "valid_indices", valid_indices); // Valid star indices
      }
    }

    // Write star data
    write(g, "Ndot_int", Ndot_int_stars, "photons/s"); // Intrinsic photon rate for each star [photons/s]
    write(g, "Ndot_src", source_weight_stars, "photons/s"); // Photon rate at emission for each star [photons/s]
    write(g, "Ndot_esc", weight_stars, "photons/s"); // Photon rate at escape for each star [photons/s]
    if (output_grp_vir)
      write(g, "Ndot_esc_group_vir", weight_stars_grp_vir, "photons/s"); // Photon rate at escape for each star (group virial) [photons/s]
    if (output_grp_gal)
      write(g, "Ndot_esc_group_gal", weight_stars_grp_gal, "photons/s"); // Photon rate at escape for each star (group galaxy) [photons/s]
    if (output_sub_vir)
      write(g, "Ndot_esc_subhalo_vir", weight_stars_sub_vir, "photons/s"); // Photon rate at escape for each star (subhalo virial) [photons/s]
    if (output_sub_gal)
      write(g, "Ndot_esc_subhalo_gal", weight_stars_sub_gal, "photons/s"); // Photon rate at escape for each star (subhalo galaxy) [photons/s]
  }

  const int n_cx_ions_cells = n_cxi_H_ions_cells + n_cxi_He_ions_cells + n_cxr_H_ions_cells + n_cxr_He_ions_cells;
  if (output_cells || output_cells_UVB_HI || n_col_ions_cells + n_cx_ions_cells + n_rec_ions_cells + n_phot_ions_cells > 0) {
    Group g = f.createGroup("/cells");       // Write to the cells group
    if (output_cells) {
      rescale(source_weight_cells, ion_global.Ndot); // Convert to photons/s
      rescale(weight_cells, ion_global.Ndot);
      write(g, "Ndot_int", Ndot_int_cells, "photons/s"); // Intrinsic photon rate for each cell [photons/s]
      write(g, "Ndot_src", source_weight_cells, "photons/s"); // Photon rate at emission for each cell [photons/s]
      write(g, "Ndot_esc", weight_cells, "photons/s"); // Photon rate at escape for each cell [photons/s]
    }
    // UV background ionizations for each cell [events/s]
    if (output_cells_UVB_HI)
      write(g, "Ndot_UVB_HI", Ndot_UVB_HI_cells, "events/s");
    // Recombinations for each cell [events/s]
    if (n_rec_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_rec_ions[ai]) {
          const string name = "Ndot_rec_" + fields[x_indices[ai]+1].name.substr(2);
          write(g, name, Ndot_rec_ions_cells[j++], "events/s");
        }
      }
    }
    // Collisional ionizations for each cell [events/s]
    if (n_col_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_col_ions[ai])
          write(g, "Ndot_col_" + ion_names[active_ions[ai]], Ndot_col_ions_cells[j++], "events/s");
      }
    }
    // Photoionizations for each cell [events/s]
    if (n_phot_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_phot_ions[ai])
          write(g, "Ndot_phot_" + ion_names[active_ions[ai]], Ndot_phot_ions_cells[j++], "events/s");
      }
    }
    // Charge exchange ionizations (H) for each cell [events/s]
    if (n_cxi_H_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_cxi_H_ions[ai])
          write(g, "Ndot_cxi_H_" + ion_names[active_ions[ai]], Ndot_cxi_H_ions_cells[j++], "events/s");
      }
    }
    // Charge exchange ionizations (He) for each cell [events/s]
    if (n_cxi_He_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_cxi_He_ions[ai])
          write(g, "Ndot_cxi_He_" + ion_names[active_ions[ai]], Ndot_cxi_He_ions_cells[j++], "events/s");
      }
    }
    // Charge exchange recombinations (H) for each cell [events/s]
    if (n_cxr_H_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_cxr_H_ions[ai]) {
          const string name = "Ndot_cxr_H_" + fields[x_indices[ai]+1].name.substr(2);
          write(g, name, Ndot_cxr_H_ions_cells[j++], "events/s");
        }
      }
    }
    // Charge exchange recombinations (He) for each cell [events/s]
    if (n_cxr_He_ions_cells > 0) {
      for (int ai = 0, j = 0; ai < n_active_ions; ++ai) {
        if (output_cells_cxr_He_ions[ai]) {
          const string name = "Ndot_cxr_He_" + fields[x_indices[ai]+1].name.substr(2);
          write(g, name, Ndot_cxr_He_ions_cells[j++], "events/s");
        }
      }
    }
  }

  // Dual grid data
  if constexpr (output_ion_cell_age_freq)
    write_ion_cell_age_freq(f);              // Absorptions by cell, age, and frequency [photons/s]
  if constexpr (output_ion_radial_age_freq)
    write_ion_radial_age_freq(f);            // Absorptions by radial, age, and frequency [photons/s]
  if constexpr (output_ion_distance_age_freq)
    write_ion_distance_age_freq(f);          // Absorptions by distance, age, and frequency [photons/s]
  if constexpr (output_radial_flow)
    write_radial_flow(f);                    // Radial flow statistics [photons/s]
  if constexpr (output_group_flows)
    write_halo_flows(f, true);               // Group flow statistics [photons/s]
  if constexpr (output_subhalo_flows)
    write_halo_flows(f, false);              // Subhalo flow statistics [photons/s]

  // Group information
  if constexpr (output_groups) {
    if (n_groups > 0) {                      // Have groups
      Group g = f.createGroup("/group"), g_HI, g_bin;
      write(g, "id", group_id);              // Group IDs
      write(g, "Ndot", Ndot_grp, "photons/s"); // Group photon rates [photons/s]
      write(g, "f_src", f_src_grp);          // Group source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_grp); // Effective number of emitted photons: 1/<w0>
      if (output_grp_obs) {                  // Observed properties
        write(g, "f_esc", f_esc_grp);        // Group escape fractions: sum(w)
        write(g, "n_photons_esc", n_photons_esc_grp); // Effective number of escaped photons: 1/<w>
        write(g, "f_abs", f_abs_grp);        // Group dust absorption fractions
        write(g, "n_photons_abs", n_photons_abs_grp); // Effective number of absorbed photons: 1/<w>
      }
      if (output_grp_vir) {                  // Virial radius properties
        write(g, "Ndot_vir", Ndot_grp_vir, "photons/s"); // Group photon rates [photons/s]
        write(g, "f_src_vir", f_src_grp_vir); // Group source fractions: sum(w0)
        write(g, "n_photons_src_vir", n_photons_src_grp_vir); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_vir", f_esc_grp_vir); // Group escape fractions: sum(w)
        write(g, "n_photons_esc_vir", n_photons_esc_grp_vir); // Effective number of escaped photons: 1/<w>
        write(g, "f_abs_vir", f_abs_grp_vir); // Group dust absorption fractions
        write(g, "n_photons_abs_vir", n_photons_abs_grp_vir); // Effective number of absorbed photons: 1/<w>
      }
      if (output_grp_gal) {                  // Galaxy radius properties
        write(g, "Ndot_gal", Ndot_grp_gal, "photons/s"); // Group photon rates [photons/s]
        write(g, "f_src_gal", f_src_grp_gal); // Group source fractions: sum(w0)
        write(g, "n_photons_src_gal", n_photons_src_grp_gal); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_gal", f_esc_grp_gal); // Group escape fractions: sum(w)
        write(g, "n_photons_esc_gal", n_photons_esc_grp_gal); // Effective number of escaped photons: 1/<w>
        write(g, "f_abs_gal", f_abs_grp_gal); // Group dust absorption fractions
        write(g, "n_photons_abs_gal", n_photons_abs_grp_gal); // Effective number of absorbed photons: 1/<w>
      }
      if (focus_groups_on_emission) {
        write(g, "r_light", r_light_grp, "cm"); // Center of luminosity position [cm]
        write(g, "v_light", v_light_grp, "cm/s"); // Center of luminosity velocity [cm/s]
      }
      // HI properties
      if (HI_bin > 0) {
        g_HI = g.createGroup("HI");
        write(g_HI, "Ndot", Ndot_HI_grp, "photons/s"); // Group HI photon rates [photons/s]
        write(g_HI, "f_src", f_src_HI_grp);  // Group HI source fractions: sum(w0)
        write(g_HI, "n_photons_src", n_photons_src_HI_grp); // Effective number of emitted photons: 1/<w0>
        if (output_grp_obs) {                // Observed properties
          write(g_HI, "f_esc", f_esc_HI_grp); // Group HI escape fractions: sum(w)
          write(g_HI, "n_photons_esc", n_photons_esc_HI_grp); // Effective number of escaped photons: 1/<w>
          write(g_HI, "f_abs", f_abs_HI_grp); // Group HI dust absorption fractions
          write(g_HI, "n_photons_abs", n_photons_abs_HI_grp); // Effective number of absorbed photons: 1/<w>
        }
        if (output_grp_vir) {                // Virial radius properties
          write(g_HI, "Ndot_vir", Ndot_HI_grp_vir, "photons/s"); // Group HI photon rates [photons/s]
          write(g_HI, "f_src_vir", f_src_HI_grp_vir); // Group HI source fractions: sum(w0)
          write(g_HI, "n_photons_src_vir", n_photons_src_HI_grp_vir); // Effective number of emitted photons: 1/<w0>
          write(g_HI, "f_esc_vir", f_esc_HI_grp_vir); // Group HI escape fractions: sum(w)
          write(g_HI, "n_photons_esc_vir", n_photons_esc_HI_grp_vir); // Effective number of escaped photons: 1/<w>
          write(g_HI, "f_abs_vir", f_abs_HI_grp_vir); // Group HI dust absorption fractions
          write(g_HI, "n_photons_abs_vir", n_photons_abs_HI_grp_vir); // Effective number of absorbed photons: 1/<w>
        }
        if (output_grp_gal) {                // Galaxy radius properties
          write(g_HI, "Ndot_gal", Ndot_HI_grp_gal, "photons/s"); // Group HI photon rates [photons/s]
          write(g_HI, "f_src_gal", f_src_HI_grp_gal); // Group HI source fractions: sum(w0)
          write(g_HI, "n_photons_src_gal", n_photons_src_HI_grp_gal); // Effective number of emitted photons: 1/<w0>
          write(g_HI, "f_esc_gal", f_esc_HI_grp_gal); // Group HI escape fractions: sum(w)
          write(g_HI, "n_photons_esc_gal", n_photons_esc_HI_grp_gal); // Effective number of escaped photons: 1/<w>
          write(g_HI, "f_abs_gal", f_abs_HI_grp_gal); // Group HI dust absorption fractions
          write(g_HI, "n_photons_abs_gal", n_photons_abs_HI_grp_gal); // Effective number of absorbed photons: 1/<w>
        }
      }
      // Line-of-sight healpix maps
      if (output_map_grp) {
        const int n_pix_grp = 12 * n_side_grp * n_side_grp; // 12 n_side^2
        write(g, "n_side", n_side_grp);      // Healpix n_side parameter
        write(g, "n_pix_map", n_pix_grp);    // Number of group maps pixels
        if (output_grp_obs)
          write(g, "map", map_grp);          // Escape fraction maps [fraction]
        if (output_grp_vir)
          write(g, "map_vir", map_grp_vir);  // Escape fraction maps [fraction]
        if (output_grp_gal)
          write(g, "map_gal", map_grp_gal);  // Escape fraction maps [fraction]
        if (output_map2_grp) {
          if (output_grp_obs)
            write(g, "map2", map2_grp);      // Escape fraction^2 maps [statistic]
          if (output_grp_vir)
            write(g, "map2_vir", map2_grp_vir); // Escape fraction^2 maps [statistic]
          if (output_grp_gal)
            write(g, "map2_gal", map2_grp_gal); // Escape fraction^2 maps [statistic]
        }
      }
      // Line-of-sight healpix radial group maps
      if (output_radial_map_grp) {
        const int n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // 12 n_side^2
        write(g, "n_side_radial", n_side_radial_grp); // Healpix n_side parameter
        write(g, "n_pix_radial", n_pix_radial_grp); // Number of radial map directions
        write(g, "n_map_pixels", n_map_pixels_grp); // Number of radial map pixels
        write(g, "map_radius", map_radius_grp); // Radial map radius [cm] (defaults to half domain)
        if (min_map_radius_grp > 0.)
          write(g, "min_map_radius", min_map_radius_grp); // Logarithmic radial map minimum radius [cm]
        else
          write(g, "map_pixel_width", map_pixel_width_grp); // Radial map pixel width [cm]
        write(g, "map_pixel_areas", map_pixel_areas_grp, "cm^2"); // Radial map pixel areas [cm^2]
        if (cosmological) {
          if (min_map_radius_grp <= 0.)
            write(g, "map_pixel_arcsec", map_pixel_arcsec_grp); // Radial map pixel width [arcsec] (optional)
          write(g, "map_pixel_arcsec2", map_pixel_arcsec2_grp, "arcsec^2"); // Radial map pixel areas [arcsec^2] (optional)
        }
        if (output_grp_obs)
          write(g, "radial_map", radial_map_grp); // Radial surface brightness group maps [erg/s/cm^2/arcsec^2]
      }
      // Angle-averaged radial group profiles
      if (output_radial_avg_grp) {
        write(g, "n_pixels", n_pixels_grp);  // Number of radial avg pixels
        write(g, "radius", radius_grp);      // Radial avg radius [cm] (defaults to half domain)
        if (min_radius_grp > 0.)
          write(g, "min_radius", min_radius_grp); // Logarithmic radial avg minimum radius [cm]
        else
          write(g, "pixel_width", pixel_width_grp); // Radial avg pixel width [cm]
        write(g, "pixel_areas", pixel_areas_grp, "cm^2"); // Radial avg pixel areas [cm^2]
        if (cosmological) {
          if (min_radius_grp <= 0.)
            write(g, "pixel_arcsec", pixel_arcsec_grp); // Radial avg pixel width [arcsec] (optional)
          write(g, "pixel_arcsec2", pixel_arcsec2_grp, "arcsec^2"); // Radial avg pixel areas [arcsec^2] (optional)
        }
        if (output_grp_obs)
          write(g, "radial_avg", radial_avg_grp); // Radial surface brightness group avg [erg/s/cm^2/arcsec^2]
      }
      // Bin properties
      if (n_bins > 1) {
        g_bin = g.createGroup("bin");
        write(g_bin, "Ndot", bin_Ndot_grp, "photons/s"); // Group bin photon rates [photons/s]
        write(g_bin, "f_src", bin_f_src_grp); // Group bin source fractions: sum(w0)
        write(g_bin, "n_photons_src", bin_n_photons_src_grp); // Effective number of emitted photons: 1/<w0>
        if (output_grp_obs) {                // Observed properties
          write(g_bin, "f_esc", bin_f_esc_grp); // Group bin escape fractions
          write(g_bin, "n_photons_esc", bin_n_photons_esc_grp); // Effective number of escaped photons: 1/<w>
          write(g_bin, "f_abs", bin_f_abs_grp); // Group bin dust absorption fractions
          write(g_bin, "n_photons_abs", bin_n_photons_abs_grp); // Effective number of absorbed photons: 1/<w>
        }
        if (output_grp_vir) {                // Virial radius properties
          write(g_bin, "Ndot_vir", bin_Ndot_grp_vir, "photons/s"); // Group bin photon rates [photons/s]
          write(g_bin, "f_src_vir", bin_f_src_grp_vir); // Group bin source fractions: sum(w0)
          write(g_bin, "n_photons_src_vir", bin_n_photons_src_grp_vir); // Effective number of emitted photons: 1/<w0>
          write(g_bin, "f_esc_vir", bin_f_esc_grp_vir); // Group bin escape fractions
          write(g_bin, "n_photons_esc_vir", bin_n_photons_esc_grp_vir); // Effective number of escaped photons: 1/<w>
          write(g_bin, "f_abs_vir", bin_f_abs_grp_vir); // Group bin dust absorption fractions
          write(g_bin, "n_photons_abs_vir", bin_n_photons_abs_grp_vir); // Effective number of absorbed photons: 1/<w>
        }
        if (output_grp_gal) {                // Galaxy radius properties
          write(g_bin, "Ndot_gal", bin_Ndot_grp_gal, "photons/s"); // Group bin photon rates [photons/s]
          write(g_bin, "f_src_gal", bin_f_src_grp_gal); // Group bin source fractions: sum(w0)
          write(g_bin, "n_photons_src_gal", bin_n_photons_src_grp_gal); // Effective number of emitted photons: 1/<w0>
          write(g_bin, "f_esc_gal", bin_f_esc_grp_gal); // Group bin escape fractions
          write(g_bin, "n_photons_esc_gal", bin_n_photons_esc_grp_gal); // Effective number of escaped photons: 1/<w>
          write(g_bin, "f_abs_gal", bin_f_abs_grp_gal); // Group bin dust absorption fractions
          write(g_bin, "n_photons_abs_gal", bin_n_photons_abs_grp_gal); // Effective number of absorbed photons: 1/<w>
        }
      }
      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission) {
          write(g, "Ndot_stars", Ndot_stars_grp, "photons/s"); // Group stellar photon rates [photons/s]
          if (output_grp_vir)
            write(g, "Ndot_stars_vir", Ndot_stars_grp_vir, "photons/s"); // Group stellar photon rates [photons/s]
          if (output_grp_gal)
            write(g, "Ndot_stars_gal", Ndot_stars_grp_gal, "photons/s"); // Group stellar photon rates [photons/s]
          if (HI_bin > 0) {
            write(g_HI, "Ndot_stars", Ndot_stars_HI_grp, "photons/s"); // Group stellar HI photon rates [photons/s]
            if (output_grp_vir)
              write(g_HI, "Ndot_stars_vir", Ndot_stars_HI_grp_vir, "photons/s"); // Group stellar HI photon rates [photons/s]
            if (output_grp_gal)
              write(g_HI, "Ndot_stars_gal", Ndot_stars_HI_grp_gal, "photons/s"); // Group stellar HI photon rates [photons/s]
          }
          if (n_bins > 1) {
            write(g_bin, "Ndot_stars", bin_Ndot_stars_grp, "photons/s"); // Group bin stellar photon rates [photons/s]
            if (output_grp_vir)
              write(g_bin, "Ndot_stars_vir", bin_Ndot_stars_grp_vir, "photons/s"); // Group bin stellar photon rates [photons/s]
            if (output_grp_gal)
              write(g_bin, "Ndot_stars_gal", bin_Ndot_stars_grp_gal, "photons/s"); // Group bin stellar photon rates [photons/s]
          }
        }
        write(g, "n_stars_eff", n_stars_eff_grp); // Effective number of stars: 1/<w>
        if (output_grp_vir)
          write(g, "n_stars_eff_vir", n_stars_eff_grp_vir); // Effective number of stars: 1/<w>
        if (output_grp_gal)
          write(g, "n_stars_eff_gal", n_stars_eff_grp_gal); // Effective number of stars: 1/<w>
        if (HI_bin > 0) {
          write(g_HI, "n_stars_eff", n_stars_eff_HI_grp); // Effective number of stars: 1/<w>
          if (output_grp_vir)
            write(g_HI, "n_stars_eff_vir", n_stars_eff_HI_grp_vir); // Effective number of stars: 1/<w>
          if (output_grp_gal)
            write(g_HI, "n_stars_eff_gal", n_stars_eff_HI_grp_gal); // Effective number of stars: 1/<w>
        }
        if (n_bins > 1) {
          write(g_bin, "n_stars_eff", bin_n_stars_eff_grp); // Bin effective number of stars: 1/<w>
          if (output_grp_vir)
            write(g_bin, "n_stars_eff_vir", bin_n_stars_eff_grp_vir); // Bin effective number of stars: 1/<w>
          if (output_grp_gal)
            write(g_bin, "n_stars_eff_gal", bin_n_stars_eff_grp_gal); // Bin effective number of stars: 1/<w>
        }
      }
      if (cell_based_emission) {
        if (star_based_emission) {
          write(g, "Ndot_gas", Ndot_gas_grp, "photons/s"); // Group gas photon rates [photons/s]
          if (output_grp_vir)
            write(g, "Ndot_gas_vir", Ndot_gas_grp_vir, "photons/s"); // Group gas photon rates [photons/s]
          if (output_grp_gal)
            write(g, "Ndot_gas_gal", Ndot_gas_grp_gal, "photons/s"); // Group gas photon rates [photons/s]
          if (HI_bin > 0) {
            write(g_HI, "Ndot_gas", Ndot_gas_HI_grp, "photons/s"); // Group gas HI photon rates [photons/s]
            if (output_grp_vir)
              write(g_HI, "Ndot_gas_vir", Ndot_gas_HI_grp_vir, "photons/s"); // Group gas HI photon rates [photons/s]
            if (output_grp_gal)
              write(g_HI, "Ndot_gas_gal", Ndot_gas_HI_grp_gal, "photons/s"); // Group gas HI photon rates [photons/s]
          }
          if (n_bins > 1) {
            write(g_bin, "Ndot_gas", bin_Ndot_gas_grp, "photons/s"); // Group bin gas photon rates [photons/s]
            if (output_grp_vir)
              write(g_bin, "Ndot_gas_vir", bin_Ndot_gas_grp_vir, "photons/s"); // Group bin gas photon rates [photons/s]
            if (output_grp_gal)
              write(g_bin, "Ndot_gas_gal", bin_Ndot_gas_grp_gal, "photons/s"); // Group bin gas photon rates [photons/s]
          }
        }
        write(g, "n_cells_eff", n_cells_eff_grp); // Effective number of cells: 1/<w>
        if (output_grp_vir)
          write(g, "n_cells_eff_vir", n_cells_eff_grp_vir); // Effective number of cells: 1/<w>
        if (output_grp_gal)
          write(g, "n_cells_eff_gal", n_cells_eff_grp_gal); // Effective number of cells: 1/<w>
        if (HI_bin > 0) {
          write(g_HI, "n_cells_eff", n_cells_eff_HI_grp); // Effective number of cells: 1/<w>
          if (output_grp_vir)
            write(g_HI, "n_cells_eff_vir", n_cells_eff_HI_grp_vir); // Effective number of cells: 1/<w>
          if (output_grp_gal)
            write(g_HI, "n_cells_eff_gal", n_cells_eff_HI_grp_gal); // Effective number of cells: 1/<w>
        }
        if (n_bins > 1) {
          write(g_bin, "n_cells_eff", bin_n_cells_eff_grp); // Bin effective number of cells: 1/<w>
          if (output_grp_vir)
            write(g_bin, "n_cells_eff_vir", bin_n_cells_eff_grp_vir); // Bin effective number of cells: 1/<w>
          if (output_grp_gal)
            write(g_bin, "n_cells_eff_gal", bin_n_cells_eff_grp_gal); // Bin effective number of cells: 1/<w>
        }
      }
    }
    if (n_ugroups > 0) {                     // Have unfiltered groups
      Group g = f.createGroup("/group_unfiltered"), g_HI, g_bin;
      write(g, "id", ugroup_id);             // Unfiltered group IDs
      write(g, "Ndot", Ndot_ugrp, "photons/s"); // Unfiltered group photon rates [photons/s]
      write(g, "f_src", f_src_ugrp);         // Unfiltered group source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_ugrp); // Effective number of emitted photons: 1/<w0>
      // HI properties
      if (HI_bin > 0) {
        g_HI = g.createGroup("HI");
        write(g_HI, "Ndot", Ndot_HI_ugrp, "photons/s"); // Unfiltered group HI photon rates [photons/s]
        write(g_HI, "f_src", f_src_HI_ugrp); // Unfiltered group HI source fractions: sum(w0)
        write(g_HI, "n_photons_src", n_photons_src_HI_ugrp); // Effective number of emitted photons: 1/<w0>
      }
      // Bin properties
      if (n_bins > 1) {
        g_bin = g.createGroup("bin");
        write(g_bin, "Ndot", bin_Ndot_ugrp, "photons/s"); // Unfiltered group bin photon rates [photons/s]
        write(g_bin, "f_src", bin_f_src_ugrp); // Unfiltered group bin source fractions: sum(w0)
        write(g_bin, "n_photons_src", bin_n_photons_src_ugrp); // Effective number of emitted photons: 1/<w0>
      }
      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission) {
          write(g, "Ndot_stars", Ndot_stars_ugrp, "photons/s"); // Unfiltered group stellar photon rates [photons/s]
          if (HI_bin > 0)
            write(g_HI, "Ndot_stars", Ndot_stars_HI_ugrp, "photons/s"); // Unfiltered group stellar HI photon rates [photons/s]
          if (n_bins > 1)
            write(g_bin, "Ndot_stars", bin_Ndot_stars_ugrp, "photons/s"); // Unfiltered group bin stellar photon rates [photons/s]
        }
        write(g, "n_stars_eff", n_stars_eff_ugrp); // Effective number of stars: 1/<w>
        if (HI_bin > 0)
          write(g_HI, "n_stars_eff", n_stars_eff_HI_ugrp); // Effective number of stars: 1/<w>
        if (n_bins > 1)
          write(g_bin, "n_stars_eff", bin_n_stars_eff_ugrp); // Bin effective number of stars: 1/<w>
      }
      if (cell_based_emission) {
        if (star_based_emission) {
          write(g, "Ndot_gas", Ndot_gas_ugrp, "photons/s"); // Unfiltered group gas photon rates [photons/s]
          if (HI_bin > 0)
            write(g_HI, "Ndot_gas", Ndot_gas_HI_ugrp, "photons/s"); // Unfiltered group gas HI photon rates [photons/s]
          if (n_bins > 1)
            write(g_bin, "Ndot_gas", bin_Ndot_gas_ugrp, "photons/s"); // Unfiltered group bin gas photon rates [photons/s]
        }
        write(g, "n_cells_eff", n_cells_eff_ugrp); // Effective number of cells: 1/<w>
        if (HI_bin > 0)
          write(g_HI, "n_cells_eff", n_cells_eff_HI_ugrp); // Effective number of cells: 1/<w>
        if (n_bins > 1)
          write(g_bin, "n_cells_eff", bin_n_cells_eff_ugrp); // Bin effective number of cells: 1/<w>
      }
    }
  }

  // Subhalo information
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {                    // Have subhalos
      Group g = f.createGroup("/subhalo"), g_HI, g_bin;
      write(g, "id", subhalo_id);            // Subhalo IDs
      write(g, "Ndot", Ndot_sub, "photons/s"); // Subhalo photon rates [photons/s]
      write(g, "f_src", f_src_sub);          // Subhalo source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_sub); // Effective number of emitted photons: 1/<w0>
      if (output_sub_obs) {                  // Observed properties
        write(g, "f_esc", f_esc_sub);        // Subhalo escape fractions: sum(w)
        write(g, "n_photons_esc", n_photons_esc_sub); // Effective number of escaped photons: 1/<w>
        write(g, "f_abs", f_abs_sub);        // Subhalo dust absorption fractions
        write(g, "n_photons_abs", n_photons_abs_sub); // Effective number of absorbed photons: 1/<w>
      }
      if (output_sub_vir) {                  // Virial radius properties
        write(g, "Ndot_vir", Ndot_sub_vir, "photons/s"); // Subhalo photon rates [photons/s]
        write(g, "f_src_vir", f_src_sub_vir); // Subhalo source fractions: sum(w0)
        write(g, "n_photons_src_vir", n_photons_src_sub_vir); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_vir", f_esc_sub_vir); // Subhalo escape fractions: sum(w)
        write(g, "n_photons_esc_vir", n_photons_esc_sub_vir); // Effective number of escaped photons: 1/<w>
        write(g, "f_abs_vir", f_abs_sub_vir); // Subhalo dust absorption fractions
        write(g, "n_photons_abs_vir", n_photons_abs_sub_vir); // Effective number of absorbed photons: 1/<w>
      }
      if (output_sub_gal) {                  // Galaxy radius properties
        write(g, "Ndot_gal", Ndot_sub_gal, "photons/s"); // Subhalo photon rates [photons/s]
        write(g, "f_src_gal", f_src_sub_gal); // Subhalo source fractions: sum(w0)
        write(g, "n_photons_src_gal", n_photons_src_sub_gal); // Effective number of emitted photons: 1/<w0>
        write(g, "f_esc_gal", f_esc_sub_gal); // Subhalo escape fractions: sum(w)
        write(g, "n_photons_esc_gal", n_photons_esc_sub_gal); // Effective number of escaped photons: 1/<w>
        write(g, "f_abs_gal", f_abs_sub_gal); // Subhalo dust absorption fractions
        write(g, "n_photons_abs_gal", n_photons_abs_sub_gal); // Effective number of absorbed photons: 1/<w>
      }
      if (focus_subhalos_on_emission) {
        write(g, "r_light", r_light_sub, "cm"); // Center of luminosity position [cm]
        write(g, "v_light", v_light_sub, "cm/s"); // Center of luminosity velocity [cm/s]
      }
      // HI properties
      if (HI_bin > 0) {
        g_HI = g.createGroup("HI");
        write(g_HI, "Ndot", Ndot_HI_sub, "photons/s"); // Subhalo HI photon rates [photons/s]
        write(g_HI, "f_src", f_src_HI_sub);  // Subhalo HI source fractions: sum(w0)
        write(g_HI, "n_photons_src", n_photons_src_HI_sub); // Effective number of emitted photons: 1/<w0>
        if (output_sub_obs) {                // Observed properties
          write(g_HI, "f_esc", f_esc_HI_sub); // Subhalo HI escape fractions: sum(w)
          write(g_HI, "n_photons_esc", n_photons_esc_HI_sub); // Effective number of escaped photons: 1/<w>
          write(g_HI, "f_abs", f_abs_HI_sub); // Subhalo HI dust absorption fractions
          write(g_HI, "n_photons_abs", n_photons_abs_HI_sub); // Effective number of absorbed photons: 1/<w>
        }
        if (output_sub_vir) {                // Virial radius properties
          write(g_HI, "Ndot_vir", Ndot_HI_sub_vir, "photons/s"); // Subhalo HI photon rates [photons/s]
          write(g_HI, "f_src_vir", f_src_HI_sub_vir); // Subhalo HI source fractions: sum(w0)
          write(g_HI, "n_photons_src_vir", n_photons_src_HI_sub_vir); // Effective number of emitted photons: 1/<w0>
          write(g_HI, "f_esc_vir", f_esc_HI_sub_vir); // Subhalo HI escape fractions: sum(w)
          write(g_HI, "n_photons_esc_vir", n_photons_esc_HI_sub_vir); // Effective number of escaped photons: 1/<w>
          write(g_HI, "f_abs_vir", f_abs_HI_sub_vir); // Subhalo HI dust absorption fractions
          write(g_HI, "n_photons_abs_vir", n_photons_abs_HI_sub_vir); // Effective number of absorbed photons: 1/<w>
        }
        if (output_sub_gal) {                // Galaxy radius properties
          write(g_HI, "Ndot_gal", Ndot_HI_sub_gal, "photons/s"); // Subhalo HI photon rates [photons/s]
          write(g_HI, "f_src_gal", f_src_HI_sub_gal); // Subhalo HI source fractions: sum(w0)
          write(g_HI, "n_photons_src_gal", n_photons_src_HI_sub_gal); // Effective number of emitted photons: 1/<w0>
          write(g_HI, "f_esc_gal", f_esc_HI_sub_gal); // Subhalo HI escape fractions: sum(w)
          write(g_HI, "n_photons_esc_gal", n_photons_esc_HI_sub_gal); // Effective number of escaped photons: 1/<w>
          write(g_HI, "f_abs_gal", f_abs_HI_sub_gal); // Subhalo HI dust absorption fractions
          write(g_HI, "n_photons_abs_gal", n_photons_abs_HI_sub_gal); // Effective number of absorbed photons: 1/<w>
        }
      }
      // Line-of-sight healpix maps
      if (output_map_sub) {
        const int n_pix_sub = 12 * n_side_sub * n_side_sub; // 12 n_side^2
        write(g, "n_side", n_side_sub);      // Healpix n_side parameter
        write(g, "n_pix_map", n_pix_sub);    // Number of subhalo maps pixels
        if (output_sub_obs)
          write(g, "map", map_sub);          // Escape fraction maps [fraction]
        if (output_sub_vir)
          write(g, "map_vir", map_sub_vir);  // Escape fraction maps [fraction]
        if (output_sub_gal)
          write(g, "map_gal", map_sub_gal);  // Escape fraction maps [fraction]
        if (output_map2_sub) {
          if (output_sub_obs)
            write(g, "map2", map2_sub);      // Escape fraction^2 maps [statistic]
          if (output_sub_vir)
            write(g, "map2_vir", map2_sub_vir); // Escape fraction^2 maps [statistic]
          if (output_sub_gal)
            write(g, "map2_gal", map2_sub_gal); // Escape fraction^2 maps [statistic]
        }
      }
      // Line-of-sight healpix radial subhalo maps
      if (output_radial_map_sub) {
        const int n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // 12 n_side^2
        write(g, "n_side_radial", n_side_radial_sub); // Healpix n_side parameter
        write(g, "n_pix_radial", n_pix_radial_sub); // Number of radial map directions
        write(g, "n_map_pixels", n_map_pixels_sub); // Number of radial map pixels
        write(g, "map_radius", map_radius_sub); // Radial map radius [cm] (defaults to half domain)
        if (min_map_radius_sub > 0)
          write(g, "min_map_radius", min_map_radius_sub); // Logarithmic radial map minimum radius [cm]
        else
          write(g, "map_pixel_width", map_pixel_width_sub); // Radial map pixel width [cm]
        write(g, "map_pixel_areas", map_pixel_areas_sub, "cm^2"); // Radial map pixel areas [cm^2]
        if (cosmological) {
          if (min_map_radius_sub <= 0.)
            write(g, "map_pixel_arcsec", map_pixel_arcsec_sub); // Radial map pixel width [arcsec] (optional)
          write(g, "map_pixel_arcsec2", map_pixel_arcsec2_sub, "arcsec^2"); // Radial map pixel areas [arcsec^2] (optional)
        }
        if (output_sub_obs)
          write(g, "radial_map", radial_map_sub); // Radial surface brightness subhalo maps [erg/s/cm^2/arcsec^2]
      }
      // Angle-averaged radial subhalo profiles
      if (output_radial_avg_sub) {
        write(g, "n_pixels", n_pixels_sub);  // Number of radial avg pixels
        write(g, "radius", radius_sub);      // Radial avg radius [cm] (defaults to half domain)
        if (min_radius_sub > 0.)
          write(g, "min_radius", min_radius_sub); // Logarithmic radial avg minimum radius [cm]
        else
          write(g, "pixel_width", pixel_width_sub); // Radial avg pixel width [cm]
        write(g, "pixel_areas", pixel_areas_sub, "cm^2"); // Radial avg pixel areas [cm^2]
        if (cosmological) {
          if (min_radius_sub <= 0.)
            write(g, "pixel_arcsec", pixel_arcsec_sub); // Radial avg pixel width [arcsec] (optional)
          write(g, "pixel_arcsec2", pixel_arcsec2_sub, "arcsec^2"); // Radial avg pixel areas [arcsec^2] (optional)
        }
        if (output_sub_obs)
          write(g, "radial_avg", radial_avg_sub); // Radial surface brightness subhalo avg [erg/s/cm^2/arcsec^2]
      }
      // Bin properties
      if (n_bins > 1) {
        g_bin = g.createGroup("bin");
        write(g_bin, "Ndot", bin_Ndot_sub, "photons/s"); // Subhalo bin photon rates [photons/s]
        write(g_bin, "f_src", bin_f_src_sub); // Subhalo bin source fractions: sum(w0)
        write(g_bin, "n_photons_src", bin_n_photons_src_sub); // Effective number of emitted photons: 1/<w0>
        if (output_sub_obs) {                // Observed properties
          write(g_bin, "f_esc", bin_f_esc_sub); // Subhalo bin escape fractions
          write(g_bin, "n_photons_esc", bin_n_photons_esc_sub); // Effective number of escaped photons: 1/<w>
          write(g_bin, "f_abs", bin_f_abs_sub); // Subhalo bin dust absorption fractions
          write(g_bin, "n_photons_abs", bin_n_photons_abs_sub); // Effective number of absorbed photons: 1/<w>
        }
        if (output_sub_vir) {                // Virial radius properties
          write(g_bin, "Ndot_vir", bin_Ndot_sub_vir, "photons/s"); // Subhalo bin photon rates [photons/s]
          write(g_bin, "f_src_vir", bin_f_src_sub_vir); // Subhalo bin source fractions: sum(w0)
          write(g_bin, "n_photons_src_vir", bin_n_photons_src_sub_vir); // Effective number of emitted photons: 1/<w0>
          write(g_bin, "f_esc_vir", bin_f_esc_sub_vir); // Subhalo bin escape fractions
          write(g_bin, "n_photons_esc_vir", bin_n_photons_esc_sub_vir); // Effective number of escaped photons: 1/<w>
          write(g_bin, "f_abs_vir", bin_f_abs_sub_vir); // Subhalo bin dust absorption fractions
          write(g_bin, "n_photons_abs_vir", bin_n_photons_abs_sub_vir); // Effective number of absorbed photons: 1/<w>
        }
        if (output_sub_gal) {                // Galaxy radius properties
          write(g_bin, "Ndot_gal", bin_Ndot_sub_gal, "photons/s"); // Subhalo bin photon rates [photons/s]
          write(g_bin, "f_src_gal", bin_f_src_sub_gal); // Subhalo bin source fractions: sum(w0)
          write(g_bin, "n_photons_src_gal", bin_n_photons_src_sub_gal); // Effective number of emitted photons: 1/<w0>
          write(g_bin, "f_esc_gal", bin_f_esc_sub_gal); // Subhalo bin escape fractions
          write(g_bin, "n_photons_esc_gal", bin_n_photons_esc_sub_gal); // Effective number of escaped photons: 1/<w>
          write(g_bin, "f_abs_gal", bin_f_abs_sub_gal); // Subhalo bin dust absorption fractions
          write(g_bin, "n_photons_abs_gal", bin_n_photons_abs_sub_gal); // Effective number of absorbed photons: 1/<w>
        }
      }
      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission) {
          write(g, "Ndot_stars", Ndot_stars_sub, "photons/s"); // Subhalo stellar photon rates [photons/s]
          if (output_sub_vir)
            write(g, "Ndot_stars_vir", Ndot_stars_sub_vir, "photons/s"); // Subhalo stellar photon rates [photons/s]
          if (output_sub_gal)
            write(g, "Ndot_stars_gal", Ndot_stars_sub_gal, "photons/s"); // Subhalo stellar photon rates [photons/s]
          if (HI_bin > 0) {
            write(g_HI, "Ndot_stars", Ndot_stars_HI_sub, "photons/s"); // Subhalo stellar HI photon rates [photons/s]
            if (output_sub_vir)
              write(g_HI, "Ndot_stars_vir", Ndot_stars_HI_sub_vir, "photons/s"); // Subhalo stellar HI photon rates [photons/s]
            if (output_sub_gal)
              write(g_HI, "Ndot_stars_gal", Ndot_stars_HI_sub_gal, "photons/s"); // Subhalo stellar HI photon rates [photons/s]
          }
          if (n_bins > 1) {
            write(g_bin, "Ndot_stars", bin_Ndot_stars_sub, "photons/s"); // Subhalo bin stellar photon rates [photons/s]
            if (output_sub_vir)
              write(g_bin, "Ndot_stars_vir", bin_Ndot_stars_sub_vir, "photons/s"); // Subhalo bin stellar photon rates [photons/s]
            if (output_sub_gal)
              write(g_bin, "Ndot_stars_gal", bin_Ndot_stars_sub_gal, "photons/s"); // Subhalo bin stellar photon rates [photons/s]
          }
        }
        write(g, "n_stars_eff", n_stars_eff_sub); // Effective number of stars: 1/<w>
        if (output_sub_vir)
          write(g, "n_stars_eff_vir", n_stars_eff_sub_vir); // Effective number of stars: 1/<w>
        if (output_sub_gal)
          write(g, "n_stars_eff_gal", n_stars_eff_sub_gal); // Effective number of stars: 1/<w>
        if (HI_bin > 0) {
          write(g_HI, "n_stars_eff", n_stars_eff_HI_sub); // Effective number of stars: 1/<w>
          if (output_sub_vir)
            write(g_HI, "n_stars_eff_vir", n_stars_eff_HI_sub_vir); // Effective number of stars: 1/<w>
          if (output_sub_gal)
            write(g_HI, "n_stars_eff_gal", n_stars_eff_HI_sub_gal); // Effective number of stars: 1/<w>
        }
        if (n_bins > 1) {
          write(g_bin, "n_stars_eff", bin_n_stars_eff_sub); // Bin effective number of stars: 1/<w>
          if (output_sub_vir)
            write(g_bin, "n_stars_eff_vir", bin_n_stars_eff_sub_vir); // Bin effective number of stars: 1/<w>
          if (output_sub_gal)
            write(g_bin, "n_stars_eff_gal", bin_n_stars_eff_sub_gal); // Bin effective number of stars: 1/<w>
        }
      }
      if (cell_based_emission) {
        if (star_based_emission) {
          write(g, "Ndot_gas", Ndot_gas_sub, "photons/s"); // Subhalo gas photon rates [photons/s]
          if (output_sub_vir)
            write(g, "Ndot_gas_vir", Ndot_gas_sub_vir, "photons/s"); // Subhalo gas photon rates [photons/s]
          if (output_sub_gal)
            write(g, "Ndot_gas_gal", Ndot_gas_sub_gal, "photons/s"); // Subhalo gas photon rates [photons/s]
          if (HI_bin > 0) {
            write(g_HI, "Ndot_gas", Ndot_gas_HI_sub, "photons/s"); // Subhalo gas HI photon rates [photons/s]
            if (output_sub_vir)
              write(g_HI, "Ndot_gas_vir", Ndot_gas_HI_sub_vir, "photons/s"); // Subhalo gas HI photon rates [photons/s]
            if (output_sub_gal)
              write(g_HI, "Ndot_gas_gal", Ndot_gas_HI_sub_gal, "photons/s"); // Subhalo gas HI photon rates [photons/s]
          }
          if (n_bins > 1) {
            write(g_bin, "Ndot_gas", bin_Ndot_gas_sub, "photons/s"); // Subhalo bin gas photon rates [photons/s]
            if (output_sub_vir)
              write(g_bin, "Ndot_gas_vir", bin_Ndot_gas_sub_vir, "photons/s"); // Subhalo bin gas photon rates [photons/s]
            if (output_sub_gal)
              write(g_bin, "Ndot_gas_gal", bin_Ndot_gas_sub_gal, "photons/s"); // Subhalo bin gas photon rates [photons/s]
          }
        }
        write(g, "n_cells_eff", n_cells_eff_sub); // Effective number of cells: 1/<w>
        if (output_sub_vir)
          write(g, "n_cells_eff_vir", n_cells_eff_sub_vir); // Effective number of cells: 1/<w>
        if (output_sub_gal)
          write(g, "n_cells_eff_gal", n_cells_eff_sub_gal); // Effective number of cells: 1/<w>
        if (HI_bin > 0) {
          write(g_HI, "n_cells_eff", n_cells_eff_HI_sub); // Effective number of cells: 1/<w>
          if (output_sub_vir)
            write(g_HI, "n_cells_eff_vir", n_cells_eff_HI_sub_vir); // Effective number of cells: 1/<w>
          if (output_sub_gal)
            write(g_HI, "n_cells_eff_gal", n_cells_eff_HI_sub_gal); // Effective number of cells: 1/<w>
        }
        if (n_bins > 1) {
          write(g_bin, "n_cells_eff", bin_n_cells_eff_sub); // Bin effective number of cells: 1/<w>
          if (output_sub_vir)
            write(g_bin, "n_cells_eff_vir", bin_n_cells_eff_sub_vir); // Bin effective number of cells: 1/<w>
          if (output_sub_gal)
            write(g_bin, "n_cells_eff_gal", bin_n_cells_eff_sub_gal); // Bin effective number of cells: 1/<w>
        }
      }
    }
    if (n_usubhalos > 0) {                   // Have unfiltered subhalos
      Group g = f.createGroup("/subhalo_unfiltered"), g_HI, g_bin;
      write(g, "id", usubhalo_id);           // Unfiltered subhalo IDs
      write(g, "Ndot", Ndot_usub, "photons/s"); // Unfiltered subhalo photon rates [photons/s]
      write(g, "f_src", f_src_usub);         // Unfiltered subhalo source fractions: sum(w0)
      write(g, "n_photons_src", n_photons_src_usub); // Effective number of emitted photons: 1/<w0>
      // HI properties
      if (HI_bin > 0) {
        g_HI = g.createGroup("HI");
        write(g_HI, "Ndot", Ndot_HI_usub, "photons/s"); // Unfiltered subhalo HI photon rates [photons/s]
        write(g_HI, "f_src", f_src_HI_usub); // Unfiltered subhalo HI source fractions: sum(w0)
        write(g_HI, "n_photons_src", n_photons_src_HI_usub); // Effective number of emitted photons: 1/<w0>
      }
      // Bin properties
      if (n_bins > 1) {
        g_bin = g.createGroup("bin");
        write(g_bin, "Ndot", bin_Ndot_usub, "photons/s"); // Unfiltered subhalo bin photon rates [photons/s]
        write(g_bin, "f_src", bin_f_src_usub); // Unfiltered subhalo bin source fractions: sum(w0)
        write(g_bin, "n_photons_src", bin_n_photons_src_usub); // Effective number of emitted photons: 1/<w0>
      }
      // Star and gas properties
      if (star_based_emission) {
        if (cell_based_emission) {
          write(g, "Ndot_stars", Ndot_stars_usub, "photons/s"); // Unfiltered subhalo stellar photon rates [photons/s]
          if (HI_bin > 0)
            write(g_HI, "Ndot_stars", Ndot_stars_HI_usub, "photons/s"); // Unfiltered subhalo stellar HI photon rates [photons/s]
          if (n_bins > 1)
            write(g_bin, "Ndot_stars", bin_Ndot_stars_usub, "photons/s"); // Unfiltered subhalo bin stellar photon rates [photons/s]
        }
        write(g, "n_stars_eff", n_stars_eff_usub); // Effective number of stars: 1/<w>
        if (HI_bin > 0)
          write(g_HI, "n_stars_eff", n_stars_eff_HI_usub); // Effective number of stars: 1/<w>
        if (n_bins > 1)
          write(g_bin, "n_stars_eff", bin_n_stars_eff_usub); // Bin effective number of stars: 1/<w>
      }
      if (cell_based_emission) {
        if (star_based_emission) {
          write(g, "Ndot_gas", Ndot_gas_usub, "photons/s"); // Unfiltered subhalo gas photon rates [photons/s]
          if (HI_bin > 0)
            write(g_HI, "Ndot_gas", Ndot_gas_HI_usub, "photons/s"); // Unfiltered subhalo gas HI photon rates [photons/s]
          if (n_bins > 1)
            write(g_bin, "Ndot_gas", bin_Ndot_gas_usub, "photons/s"); // Unfiltered subhalo bin gas photon rates [photons/s]
        }
        write(g, "n_cells_eff", n_cells_eff_usub); // Effective number of cells: 1/<w>
        if (HI_bin > 0)
          write(g_HI, "n_cells_eff", n_cells_eff_HI_usub); // Effective number of cells: 1/<w>
        if (n_bins > 1)
          write(g_bin, "n_cells_eff", bin_n_cells_eff_usub); // Bin effective number of cells: 1/<w>
      }
    }
  }

  if (output_abundances || output_photoionization || output_photoheating || output_photon_density) {
    // Open file and (over)write new data
    bool new_file = (abundances_output_file != init_file); // New file or overwrite?
    if (FILE *file = fopen(abundances_output_file.c_str(), "r")) {
      fclose(file);                          // First check if the file exists
      if (!output_abundances)                // If not outputting abundances
        new_file = false;                    // Then append to existing file
    }
    H5File f_x(abundances_output_file, new_file ? H5F_ACC_TRUNC : H5F_ACC_RDWR);
    if (new_file) {                          // Output a separate abundances file
      write_sim_info(f_x);                   // Write general simulation info
      write_ionization_info(f_x);            // Write general ionization info
    }
    if (output_abundances)
      write_abundances(f_x);                 // Write abundances to main group
    if (output_photoionization)
      write_photoionization(f_x);            // Write photoionization rates [1/s]
    if (output_photoheating)
      overwrite(f_x, "G_ion", G_ion, "erg/s"); // Photoheating rate [erg/s]
    if (output_photon_density)
      overwrite(f_x, "bin_n_gamma", bin_n_gamma, "photons/cm^3"); // Photon density [photons/cm^3]
  }
}
