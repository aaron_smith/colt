/****************************
 * ionization/refinement.cc *
 ****************************

 * Output a refined version of the grid to better resolve photoionization.

*/

#include "proto.h"
#include "Ionization.h"
#include "../io_hdf5.h" // HDF5 read/write functions
#include "../timing.h" // Timing functionality
#include "../rates.h" // Thermochemistry
#include <unordered_map> // Dictionaries
#include <unordered_set> // Unique sets
#include <set> // Ordered sets

extern Timer refine_timer; // Refinement timing

#if VORONOI && HAVE_CGAL
Vec3 isotropic_direction(); // Generate direction from an isotropic distribution
Vec3 circumcenter(const Vec3& A, const Vec3& B, const Vec3& C, const Vec3& D); // Tetrahedron circumcenter
void calculate_connectivity(const bool verbose_cgal = true); // Calculate Delaunay connections
void write_cell_connectivity(); // Write connections to a file

constexpr bool verbose_refinement = false;   // Verbose refinement output

//! Tetrahedron with vertices represented by four cell indices.
struct Tetra {
  int indices[4];                            // Four integer representation

  //! Default tetrahedron constructor.
  Tetra() = default;

  //! Tetrahedron constructor based on (\f$a, b, c, d\f$) cell indices.
  Tetra(const int a_val, const int b_val, const int c_val, const int d_val) {
    indices[0] = a_val;
    indices[1] = b_val;
    indices[2] = c_val;
    indices[3] = d_val;
  }

  //! Overload the indexing operator.
  int operator[](const int index) const {
    return indices[index];
  }
  int& operator[](const int index) {
    return indices[index];
  }
  Tetra operator[](const Tetra& isort) const {
    return Tetra(indices[isort[0]], indices[isort[1]], indices[isort[2]], indices[isort[3]]);
  }

  //! Tetrahedron constructor based on another Tetra object.
  Tetra(const Tetra& other) {
    indices[0] = other[0];
    indices[1] = other[1];
    indices[2] = other[2];
    indices[3] = other[3];
  }

  //! Overload the assignment operator.
  Tetra& operator=(const Tetra& other) {
    if (this != &other) {
      indices[0] = other[0];
      indices[1] = other[1];
      indices[2] = other[2];
      indices[3] = other[3];
    }
    return *this;
  }

  //! Define the equality check operator.
  bool operator==(const Tetra& other) const {
    return ((indices[0] == other[0]) &&
            (indices[1] == other[1]) &&
            (indices[2] == other[2]) &&
            (indices[3] == other[3]));
  }

  //! Define the less than comparison operator.
  bool operator<(const Tetra& other) const {
    for (int i = 0; i < 4; i++) {
      if (indices[i] < other[i])
        return true;
      else if (indices[i] > other[i])
        return false;
    }
    return false;
  }

  //! Sort the tetrahedron indices.
  inline void sort() {
    std::sort(indices, indices + 4);
  }
};

inline std::ostream& operator<<(std::ostream& os, const Tetra& tetra) {
  return os << "(" << tetra[0] << ", " << tetra[1] << ", " << tetra[2] << ", " << tetra[3] << ")";
}

namespace std {
  //! Implement Jenkins hash function to efficiently organize and access elements based on their keys.
  template <>
  struct hash<Tetra> {
    size_t operator()(const Tetra& tetra) const {
      size_t seed = 0;
      for (int i = 0; i < 4; ++i)
        seed ^= tetra[i] + 0x9e3779b9 + (seed << 6) + (seed >> 2);
      return seed;
    }
  };

  //! Implement hash function for pairs of tetrahedra.
  template <>
  struct hash<pair<Tetra, Tetra>> {
    size_t operator()(const pair<Tetra, Tetra>& p) const {
      size_t hash_value = 0;
      // Calculate hash value based on the Tetra objects in the pair
      hash<Tetra> tetra_hash;
      hash_value ^= tetra_hash(p.first);
      hash_value ^= tetra_hash(p.second);
      return hash_value;
    }
  };
}

//! Comparison function for sorting tetrahedron indices.
inline bool compare(const Tetra& tetra, const int i, const int j) {
  return tetra[i] < tetra[j];
}

//! Construct a mapping for sorting tetrahedron indices.
inline Tetra argsort(const Tetra& tetra) {
  Tetra isort(0, 1, 2, 3);
  std::sort(isort.indices, isort.indices + 4, [&](int i, int j) {
    return compare(tetra, i, j);
  });
  return isort;
}

//! Construct the inverse mapping from a sorted tetrahedron map.
inline Tetra invsort(const Tetra& isort) {
  Tetra iflip;
  iflip[isort[0]] = 0;
  iflip[isort[1]] = 1;
  iflip[isort[2]] = 2;
  iflip[isort[3]] = 3;
  return iflip;
}

//! Tetrahedron information with circumcenter, distance, and flips.
struct TetraInfo {
  Vec3 c0;                                   // Circumcenter
  double dr;                                 // Circumcenter distance
  int flips[4];                              // Tetra circulator information

  //! Default tetrahedron information constructor.
  TetraInfo() = default;

  //! Tetrahedron information constructor based on member data.
  TetraInfo(const Vec3& c0_val, const double dr_val) {
    c0 = c0_val;
    dr = dr_val;
    flips[0] = -1;
    flips[1] = -1;
    flips[2] = -1;
    flips[3] = -1;
  }

  //! Tetrahedron information constructor based on points and flips.
  TetraInfo(const Vec3& rc, const Vec3& rn, const Vec3& rp, const Vec3& ro,
            const int i_prev, const int prev, const int i_circ, const int circ) {
    c0 = circumcenter(rc, rn, rp, ro);       // Calculate circumcenter
    dr = c0.dist(rc);                        // Circumcenter distance
    flips[0] = -1;                           // Uninitialized flips
    flips[1] = -1;
    flips[2] = -1;
    flips[3] = -1;
    flips[i_prev] = prev;                    // Populate with circulator info
    flips[i_circ] = circ;
  }
};

using std::set;
using std::unordered_set;
using std::unordered_map;

/* Driver for ionization calculations. */
void Ionization::calculate_refinement() {
  refine_timer.start();

  // Print refinement information
  const double Ndot2_stars_HI = safe_division(ion_stars.HI.Ndot * ion_stars.HI.Ndot, n_stars_eff_HI);
  const double Ndot2_gas_HI = safe_division(ion_gas.HI.Ndot * ion_gas.HI.Ndot, n_cells_eff_HI);
  const double Ndot2_AGN = Ndot_AGN * Ndot_AGN;
  const double Ndot2_point_HI = ion_point.HI.Ndot * ion_point.HI.Ndot;
  const double Ndot2_plane_HI = ion_plane.HI.Ndot * ion_plane.HI.Ndot;
  const double Ndot2_tot_HI = Ndot2_stars_HI + Ndot2_gas_HI + Ndot2_AGN + Ndot2_point_HI + Ndot2_plane_HI; // TODO: + Ndot_AGN_HI
  const double n_tot_eff_HI = calc_n_eff(ion_global.HI.Ndot, Ndot2_tot_HI); // Effective number of sources: <Ndot>^2/<Ndot^2>
  Ndot_HI_threshold = refinement_rtol * ion_global.HI.Ndot / n_tot_eff_HI; // = threshold * Ndot2_HI / Ndot_HI // [photons/s]
  const double half_ratio = 0.5 * refinement_mdeg * M_PI / 180.; // Merge half ratio (circ distance / cell radius / 2)
  cout << "\nRefinement parameters and statistics:"
       << "\n  Tolerance  = " << refinement_rtol
       << "\n  Min x_HI   = " << refinement_x_HI;
  if (refinement_Rmin < positive_infinity)
    cout << "\n  Resolution = " << refinement_Rmin;
  cout << "\n Merge angle = " << refinement_mdeg << " degrees"
       << "\n Merge ratio = " << 2.*half_ratio << " (circ distance / cell radius)"
       << "\n Pertubation = " << refinement_pert << " (cell radius)"
       << "\n Ndot_tot_HI = " << ion_global.HI.Ndot << " photons/s"
       << "\nn_tot_eff_HI = " << n_tot_eff_HI
       << "\n  Ndot_HI_th = " << Ndot_HI_threshold << " photons/s = "
       << Ndot_HI_threshold / ion_global.HI.Ndot << " Ndot_tot_HI" << endl;

  // Avoid inner edge cells too
  if (output_inner_edges) {
    // Open file in read only mode
    H5File f(cgal_file, H5F_ACC_RDONLY);
    vector<int> inner_edge_indices;          // List of inner edge indices
    n_inner_edges = read(f, "inner_edges", inner_edge_indices); // Inner edge cell indices
    for (int i = 0; i < n_inner_edges; ++i)
      edge_flag[inner_edge_indices[i]] = true; // Treat inner edges as edges
    n_edges += n_inner_edges;                // Update number of edges
  }

  // Establish refinement flagging
  int n_cells_flagged = 0;                   // Number of cells flagged for refinement
  double M_H_ion = 0.;                       // Total ionized hydrogen mass [g]
  double ion_HI_tot = 0.;                    // Number of HI photoionizations [1/s]
  vector<int> ref_flag(n_cells);             // Refinement flags
  #pragma omp parallel for reduction(+:M_H_ion, ion_HI_tot) reduction(+:n_cells_flagged)
  for (int cell = 0; cell < n_cells; ++cell) {
    if (edge_flag[cell])
      continue;
    const double nH_V = n_H[cell] * VOLUME(cell);
    const double rate_HI = rate_ions[HI_ION][cell];
    const double ion_HI = rate_HI * x_HI[cell] * nH_V;
    const double R_eff = rate_HI / (alpha_B_HII(T[cell]) * n_H[cell]); // Effective resolution
    ion_HI_tot += ion_HI;
    M_H_ion += mH * x_HII[cell] * nH_V;
    if (ion_HI > Ndot_HI_threshold && x_HI[cell] > refinement_x_HI && R_eff < refinement_Rmin) {
      ref_flag[cell] = 1;                    // Selection criteria
      n_cells_flagged++;                     // Number of selected cells
    }
  }
  cout << "  M_H_ion    = " << M_H_ion / Msun << " Msun = " << M_H_ion / mH
       << " hydrogen atoms\n  ion_HI_tot = " << ion_HI_tot << " s^-1 = "
       << ion_HI_tot / ion_global.HI.Ndot << " Ndot_HI\n\nFlagged " << n_cells_flagged
       << " out of " << n_cells << " cells for refinement ("
       << 100.*double(n_cells_flagged)/double(n_cells) << "%)" << endl;
  if (n_cells_flagged <= 0) {
    cout << "\nNo cells were selected for refinement!" << endl;
    refine_timer.stop();
    return;                                  // Early return
  }
  vector<int> flagged_cells(n_cells_flagged); // Flagged cell indices
  for (int count = 0, cell = 0; cell < n_cells; ++cell)
    if (ref_flag[cell])
      flagged_cells[count++] = cell;         // Next valid flagged cell
  ref_flag = vector<int>();                  // Free memory

  // Add unique circumcenters to a set
  const int PREV = 2, CIRC = 3;              // Index ordering
  unordered_map<Tetra, TetraInfo> new_points; // Unordered map of points
  cout << "\nProcessing flagged cell circumcenters:" << endl;
  #pragma omp parallel for // No merge reduction because we update flips in TetraInfo
  for (int fc_index = 0; fc_index < n_cells_flagged; ++fc_index) {
    const int cell = flagged_cells[fc_index];
    const Vec3& rc = r[cell];                // Host cell position [cm]
    const auto& cell_faces = faces[cell];    // Neighbor information
    const int n_faces = cell_faces.size();   // Face neighbor count
    if (n_faces < 1)
      error("Expecting at least 1 neighbor!");
    // Loop over all neighbors
    for (int i_face = 0; i_face < n_faces; ++i_face) {
      const auto& face = cell_faces[i_face];
      const int neib = face.neighbor;        // Neighbor cell index
      const Vec3& rn = r[neib];              // Neighbor cell position [cm]
      // Delaunay tetrahedron circumcenters define the Voronoi face region
      const auto& face_circ = face.circulator; // Face circulator
      const int n_circs = face_circ.size();  // Circulator count
      if (n_circs < 3)
        error("Expecting at least 4 circulators! n_circs = "+to_string(n_circs));
      // Loop over all circulators
      int circ_flip = face_circ[n_circs - 2]; // Flip circulator cell index
      int prev = face_circ[n_circs - 1];     // Previous circulator cell index
      for (int i_circ = 0; i_circ < n_circs; ++i_circ) {
        const int circ = face_circ[i_circ];  // Circulator cell index
        const Tetra tetra(cell, neib, prev, circ); // Initialize the tetrahedron
        const Tetra isort = argsort(tetra);  // Sort mapping
        const Tetra iflip = invsort(isort);  // Flip mapping
        const Tetra tsort = tetra[isort];    // Unique point identifier
        const int iflip_prev = iflip[PREV];  // Previous inverse mapping
        const int iflip_circ = iflip[CIRC];  // Circulator inverse mapping
        int ip1 = i_circ + 1; if (ip1 == n_circs) ip1 = 0; // Next circulator
        const int prev_flip = face_circ[ip1]; // Flip circulator cell index
        // Check if tsort exists in new_points using the count function
        #pragma omp critical(NEW_POINTS)
        {
          if (new_points.count(tsort) > 0) {
            // Check whether flips needs to be updated
            auto& flips = new_points[tsort].flips;
            if (flips[iflip_prev] < 0)
              flips[iflip_prev] = prev_flip;
            if (flips[iflip_circ] < 0)
              flips[iflip_circ] = circ_flip;
          } else {
            // Add new point and initial flips
            new_points[tsort] = TetraInfo(rc, rn, r[prev], r[circ], iflip_prev, prev_flip, iflip_circ, circ_flip);
          }
        }
        // Update pre-initialized quantities
        circ_flip = prev;
        prev = circ;
      }
    }
  }
  const int n_candidate_points = new_points.size();
  cout << "  n_candidates   = " << n_candidate_points << endl;
  if constexpr (verbose_refinement)
    for (auto& pair : new_points) {
      const Tetra& point = pair.first;
      const TetraInfo& point_info = pair.second;
      const Vec3& c0 = point_info.c0;
      cout << "|c0| = " << c0.norm()/pc << " pc, c0 = " << c0/pc << " pc, tetra = " << point << endl;
    }
  flagged_cells = vector<int>();             // Free memory
  faces = vector<vector<Face>>();            // Clear all face data
  unordered_set<std::pair<Tetra, Tetra>> merge_pairs;
  #pragma omp parallel
  {
    unordered_set<std::pair<Tetra, Tetra>> local_merge_pairs;
    #pragma omp for schedule(dynamic)
    for (size_t bucket = 0; bucket < new_points.bucket_count(); ++bucket) {
      for (auto pair = new_points.begin(bucket); pair != new_points.end(bucket); pair++) {
        // Unpack new_points data
        const Tetra& point = pair->first;
        const TetraInfo& point_info = pair->second;
        const Vec3& c0 = point_info.c0;
        const double& dr = point_info.dr;
        const int (&flips)[4] = point_info.flips;
        int n_valid = 0;
        double dcs[4], drs[4];
        Vec3 c0s[4];
        Tetra p0s[4];
        for (int i_flip = 0; i_flip < 4; ++i_flip) {
          const int flip = flips[i_flip];
          if (flip >= 0) {
            Tetra tetra = point;             // Copy the point
            tetra[i_flip] = flip;            // Access flipped tetra
            tetra.sort();                    // Sorted indices are unique
            const TetraInfo& tinfo = new_points[tetra]; // Unpack data
            dcs[n_valid] = c0.dist(tinfo.c0); // Distance between circumcenters
            drs[n_valid] = tinfo.dr;         // Circumcenter radius
            c0s[n_valid] = tinfo.c0;         // Circumcenter position
            p0s[n_valid] = tetra;            // Tetrahedron indices
            n_valid++;                       // Valid connectivity counter
          }
        }
        // Collect merge candidates
        if (n_valid > 0) {
          int n_mask = 0;                    // Number of candidates
          int i_mask[n_valid];               // Candidate mask indices
          for (int i = 0; i < n_valid; ++i) {
            if (dcs[i] < half_ratio * (dr + drs[i])) // Small separation relative to radii
              i_mask[n_mask++] = i;          // Merge candidate index and counter
          }
          for (int i = 0; i < n_mask; ++i) {
            const Tetra& p0 = p0s[i_mask[i]]; // Merging tetrahedron
            if (point < p0)                  // Add the ordered pair to the set
              local_merge_pairs.insert(std::pair<Tetra, Tetra>(point, p0));
            else
              local_merge_pairs.insert(std::pair<Tetra, Tetra>(p0, point));
          }
        }
      }
    }
    #pragma omp critical(MERGE_PAIRS)
    merge_pairs.insert(local_merge_pairs.begin(), local_merge_pairs.end());
  }
  const int n_merge_pairs = merge_pairs.size(); // Number of merge pair candidates
  cout << "  n_merge_pairs  = " << n_merge_pairs << endl;
  if constexpr (verbose_refinement)
    for (auto& pair : merge_pairs)
    cout << "p1, p2 = " << pair.first << ", " << pair.second << endl;
  constexpr double pfac = 1. / 3.;           // Fraction to existing points
  constexpr double cfac = 1. - pfac;         // Fraction to circumcenters
  vector<Vec3> r_merged;                     // Positions of added merge points
  vector<int> i_merged;                      // Parent indices of added merge points
  if (n_merge_pairs > 0) {
    if constexpr (verbose_refinement)
      cout << "Merging points!" << endl;
    // Merge groups are disjoint sets of tetrahedra
    vector<set<Tetra>> merge_groups;
    unordered_map<Tetra, int> tetra_to_group;
    for (const auto& pair : merge_pairs) {   // Unclear how to parallelize this
      const Tetra& p1 = pair.first;
      const Tetra& p2 = pair.second;
      // Find the groups that p1 and p2 belong to
      int i1 = -1, i2 = -1;
      auto it0 = tetra_to_group.find(p1);    // Find first index
      if (it0 != tetra_to_group.end())
        i1 = it0->second;
      auto it1 = tetra_to_group.find(p2);    // Find second index
      if (it1 != tetra_to_group.end())
        i2 = it1->second;
      if constexpr (verbose_refinement)
        cout << "p1, p2 = " << p1 << ", " << p2 << "  i1, i2 = " << i1 << ", " << i2 << " -> ";
      // If both groups are found then merge them
      if (i1 != -1 && i2 != -1) {
        if (i1 != i2) {                      // Only merge if groups are different
          merge_groups[i1].insert(merge_groups[i2].begin(), merge_groups[i2].end());
          for (const auto& tetra : merge_groups[i2])
            tetra_to_group[tetra] = i1;      // Reassign group indices
          merge_groups[i2].clear();          // Clear the merged group
        }
        if constexpr (verbose_refinement)
          cout << i1 << endl;
      } else if (i1 != -1) {
        merge_groups[i1].insert(p2);         // Add p2 to first group
        tetra_to_group[p2] = i1;
        if constexpr (verbose_refinement)
          cout << i1 << endl;
      } else if (i2 != -1) {
        merge_groups[i2].insert(p1);         // Add p1 to second group
        tetra_to_group[p1] = i2;
        if constexpr (verbose_refinement)
          cout << i2 << endl;
      } else {
        const int i_end = merge_groups.size(); // Index of the new group
        merge_groups.push_back({p1, p2});    // Create the new group
        tetra_to_group[p1] = tetra_to_group[p2] = i_end; // Save indices
        if constexpr (verbose_refinement)
          cout << i_end << endl;
      }
    }
    // Remove empty sets from merge_groups
    merge_groups.erase(std::remove_if(merge_groups.begin(), merge_groups.end(),
      [](const set<Tetra>& group) { return group.empty(); }), merge_groups.end());
    const int n_merge_groups = merge_groups.size();
    cout << "  n_merge_groups = " << n_merge_groups << endl;
    if constexpr (verbose_refinement)
      for (int i = 0; i < n_merge_groups; ++i) {
        auto& group = merge_groups[i];
        cout << "group " << i << ": ";
        for (const auto& tetra : group)
          cout << tetra << " ";
        cout << endl;
      }

    // Merged points are the average of the circumcenters
    int n_added = 0;                         // Number of points added so far
    auto merged_points = vector<Vec3>(n_merge_groups); // Reserve space
    auto merged_radii = vector<double>(n_merge_groups); // For merged info
    #pragma omp parallel for reduction(+:n_added) schedule(dynamic)
    for (int i_group = 0; i_group < n_merge_groups; ++i_group) {
      const set<Tetra>& group = merge_groups[i_group];
      Vec3 c0_sum = 0.;                      // Sum of circumcenter positions
      double dr_sum = 0.;                    // Sum of circumcenter radii
      for (const auto& tetra : group) {
        c0_sum += new_points[tetra].c0;      // Sum positions
        dr_sum += new_points[tetra].dr;      // Sum radii
      }
      const double norm = 1. / double(group.size());
      merged_points[i_group] = c0_sum * norm; // Average position
      merged_radii[i_group] = dr_sum * norm; // Average radius
      // Find the unique indices from the group tetrahedra
      unordered_set<int> points;
      for (const auto& tetra : group)
        for (int i = 0; i < 4; ++i)
          points.insert(tetra[i]);
      n_added += points.size();              // Track how many points will be added
    }
    // Find the unique tetra from the set of merge pairs
    unordered_set<Tetra> merge_points;
    for (const auto& pair : merge_pairs) {   // Unclear how to parallelize this
      merge_points.insert(pair.first);
      merge_points.insert(pair.second);
    }
    const int n_merge_points = merge_points.size();
    cout << "  n_merge_points = " << n_merge_points << endl;
    if constexpr (verbose_refinement)
      for (const auto& c0 : merged_points)
        cout << "|c0| = " << c0.norm()/pc << " pc, c0 = " << c0 << " pc" << endl;
    merge_pairs = unordered_set<std::pair<Tetra, Tetra>>(); // Free memory
    // Remove merge points from new_points
    for (const auto& point : merge_points)   // Unclear how to parallelize this
      new_points.erase(point);
    // Calculate the points that will be added
    r_merged = vector<Vec3>(n_added);        // Positions of added points
    i_merged = vector<int>(n_added);         // Parent indices of added points
    n_added = 0;                             // Reset the number of points added
    #pragma omp parallel for schedule(dynamic)
    for (int i_group = 0; i_group < n_merge_groups; ++i_group) {
      const set<Tetra>& group = merge_groups[i_group];
      const Vec3& c0_cfac = merged_points[i_group] * cfac; // Position with cfac weight
      const double dp = refinement_pert * merged_radii[i_group]; // Position perturbation
      // Find the unique indices from the group tetrahedra
      unordered_set<int> points;
      for (const auto& tetra : group)
        for (int i = 0; i < 4; ++i)
          points.insert(tetra[i]);
      // Add points to the r_merged vector
      int i_added;                           // Index into r_merged
      for (const int index : points) {
        #pragma omp atomic capture
        i_added = n_added++;                 // Capture number of added points
        r_merged[i_added] = r[index] * pfac + c0_cfac + isotropic_direction() * dp;
        i_merged[i_added] = index;           // Parent index
      }
    }
  }
  const int n_merged = r_merged.size();
  cout << "  n_merged       = " << n_merged << endl;
  if constexpr (verbose_refinement)
    for (int i = 0; i < n_merged; ++i)
      cout << "|r_merged| = " << r_merged[i].norm()/pc << " pc, r_merged = " << r_merged[i] << " pc" << endl;

  // Positions and parents for the unmerged new points
  const int n_new_points = new_points.size();
  const int n_split = 4;                     // Aggressive refinement scheme
  cout << "  n_new_points   = " << n_new_points << " (n_split = " << n_split << ")" << endl;
  const int n_new = n_new_points * n_split;  // Number of new points
  vector<Vec3> r_new = vector<Vec3>(n_new);  // Positions of added new points
  vector<int> i_new = vector<int>(n_new);    // Parent indices of added new points
  // Refine around isolated circumcenters
  int n_added = 0;                           // Number of points added so far
  #pragma omp parallel for schedule(dynamic)
  for (size_t bucket = 0; bucket < new_points.bucket_count(); ++bucket) {
    for (auto pair = new_points.begin(bucket); pair != new_points.end(bucket); pair++) {
      // Unpack new_points data
      const Tetra& tetra = pair->first;
      const TetraInfo& tetra_info = pair->second;
      const Vec3& c0_cfac = tetra_info.c0 * cfac; // Position with cfac weight
      const double dp = refinement_pert * tetra_info.dr; // Position perturbation
      // Add points to the r_new vector
      int i_added;                           // Index into r_new
      for (int i = 0; i < n_split; ++i) {
        #pragma omp atomic capture
        i_added = n_added++;                 // Capture number of added points
        const int index = tetra[i];          // Cell index
        r_new[i_added] = r[index] * pfac + c0_cfac + isotropic_direction() * dp;
        i_new[i_added] = index;              // Parent index
      }
    }
  }
  new_points = unordered_map<Tetra, TetraInfo>(); // Clear the new_points map
  if constexpr (verbose_refinement)
    for (int i = 0; i < n_new; ++i)
      cout << "|r_new| = " << r_new[i].norm()/pc << " pc, r_new = " << r_new[i] << " pc" << endl;

  // Combine all the old, new, and merged points
  const int n_old = r.size();
  n_cells = n_old + n_new + n_merged;        // Total number of cells
  cout << "  *** n_cells ---> " << n_cells << endl;
  if (n_new + n_merged <= 0) {
    cout << "\nNo new cells were added after refinement!" << endl;
    refine_timer.stop();
    return;                                  // Early return
  }
  vector<Vec3> r_old = std::move(r);         // Move the data from r to r_old
  vector<double> V_old = std::move(V);       // Move the data from V to V_old
  r.resize(n_cells);                         // Resize the r vector
  vector<int> n_children(n_old, 1);          // Number of children with the same parent
  for (const int index : i_new)              // Count new children
    ++n_children[index];
  for (const int index : i_merged)           // Count merged children
    ++n_children[index];
  vector<int> current_child(n_old);          // Current child index for each parent
  current_child[0] = 0;                      // Initialize the first parent
  for (int i = 1; i < n_old; ++i)            // Cumulative sum (no parallelism)
    current_child[i] = current_child[i - 1] + n_children[i - 1];
  #pragma omp parallel for
  for (int i = 0; i < n_old; ++i)            // Place the old points
    r[current_child[i]++] = r_old[i];        // Original parent positions
  r_old = vector<Vec3>();                    // Clear the r_old vector
  int i_r;                                   // Index into r
  #pragma omp parallel for private(i_r)
  for (int i = 0; i < n_new; ++i) {          // Place the new points
    #pragma omp atomic capture
    i_r = current_child[i_new[i]]++;         // Capture the current child index
    r[i_r] = r_new[i];                       // Unmerged new points
  }
  r_new = vector<Vec3>();                    // Clear the r_new vector
  #pragma omp parallel for private(i_r)
  for (int i = 0; i < n_merged; ++i) {       // Place the merged points
    #pragma omp atomic capture
    i_r = current_child[i_merged[i]]++;      // Capture the current child index
    r[i_r] = r_merged[i];                    // Merged points
  }
  r_merged = vector<Vec3>();                 // Clear the r_merged vector
  // Check that all the children have been placed correctly
  if (current_child[0] != n_children[0])
    error("Refinement bookkeeping failed for index 0");
  #pragma omp parallel for
  for (int i = 1; i < n_old; ++i)
    if (current_child[i] != current_child[i - 1] + n_children[i])
      error("Refinement bookkeeping failed for index "+to_string(i));
  int n_parents = 0;                         // Number of parents with multiple children
  #pragma omp parallel for reduction(+:n_parents)
  for (int i = 0; i < n_old; ++i) {
    if (n_children[i] > 1) ++n_parents;      // Count parents with multiple children
    current_child[i] -= n_children[i];       // Reset the current child index
  }
  vector<int> i_collapsed(n_parents);        // Parent indices (collapsed)
  vector<int> n_collapsed(n_parents);        // Number of children (collapsed)
  for (int i = 0, j = 0; i < n_old; ++i) {   // Loop over all parents (no parallelism)
    if (n_children[i] > 1) {
      i_collapsed[j] = i;                    // Parent index
      n_collapsed[j] = n_children[i];        // Number of children
      ++j;                                   // Increment collapsed index
    }
  }

  // Calculate the new volumes and neighbors
  Timer cgal_timer = Timer("cgal", true);    // Start connectivity timer
  calculate_connectivity(root);              // Calculate Delaunay connections
  cgal_timer.stop();

  // Write cell connectivity
  Timer cgal_io_timer = Timer("cgal_io", true); // Start io timer
  size_t i_s = cgal_file.find_last_of('/');  // Find the last slash
  cgal_file = cgal_file.substr(0, i_s) + "/ref_" + cgal_file.substr(i_s + 1);
  cout << "\nCGAL: Writing connectivity to file ..." << endl;
  write_cell_connectivity();
  cgal_io_timer.stop();

  // Write the remaining refinement data
  {
    cout << "\nCGAL: Writing refinement data to file ..." << endl;
    H5File f(cgal_file, H5F_ACC_RDWR);
    if (cosmological) {
      write(f, "redshift", z);               // Current simulation redshift
      write(f, "Omega0", Omega0);            // Matter density [rho_crit_0]
      write(f, "OmegaB", OmegaB);            // Baryon density [rho_crit_0]
      write(f, "h100", h100);                // Hubble constant [100 km/s/Mpc]
    } else if (sim_time >= 0.)
      write(f, "time", sim_time);            // Simulation time [s]
    write(f, "n_cells", n_cells);            // Number of cells
    write(f, "n_cells_old", n_old);          // Number of cells before refinement
    write(f, "n_parents", n_parents);        // Number of parents with multiple children
    write(f, "parent_indices", i_collapsed); // Parent indices (collapsed)
    write(f, "n_children", n_collapsed);     // Number of children (collapsed)
    if ((bbox[0] + bbox[1]).norm() < 1e-10 * (bbox[1] - bbox[0]).norm()
        && bbox[1].x == bbox[1].y && bbox[1].x == bbox[1].z)
      write(f, "r_box", bbox[1].x);          // Box radius [cm]
    write(f, "r", r, "cm");                  // Cell position [cm]
  }

  // Print connectivity data
  cout << "\n  n_parents  = " << n_parents
       << "\n  n_edges    = " << n_edges;
  if (inner_edges)
    cout << "\n  n_inner_edges = " << n_inner_edges;
  cout << "\n  n_neib_tot = " << n_neighbors_tot
       << "\n  n_circ_tot = " << n_circulators_tot
       << "\n  sum(V)     = " << omp_sum(V) / (kpc*kpc*kpc) << " kpc^3" << endl;

  // Print exit messages
  cgal_timer.set_wall(cgal_timer.get_wall() / double(n_threads));
  cgal_io_timer.set_wall(cgal_io_timer.get_wall() / double(n_threads));
  cout << "\nCGAL timings:  [w/r = wall/real scaled by thread count]"
       << "\n  Calculation = " << cgal_timer.str() << "  [w/r: " << cgal_timer.efficiency() << "]"
       << "\n  Output file = " << cgal_io_timer.str() << "  [w/r: " << cgal_io_timer.efficiency() << "]" << endl;

  refine_timer.stop();
}

#endif // VORONOI
