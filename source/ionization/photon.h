/************
 * photon.h *
 ************

 * Ionization photon packets.

*/

#ifndef ION_PHOTON_INCLUDED
#define ION_PHOTON_INCLUDED

#include "../proto.h"                        // Function prototypes and data structures

struct IonPhoton {
  IonPhoton();                               // Initialization constructor
  void print();                              // Print photon data
  void ray_trace();                          // Perform ionization calculations

  // Photon properties
  int source_id;                             // Index of emission source
  int source_type;                           // Type of emission source
  double source_weight;                      // Photon weight at emission
  int current_cell;                          // Index of the current cell
  int n_scat;                                // Number of scattering events
  Vec3 source_position;                      // Photon position at emission [cm]
  Vec3 position;                             // Photon position [cm]
  Vec3 direction;                            // Photon direction (normalized)
  int frequency_bin;                         // Photon frequency bin
#if output_ion_cell_age_freq
  int caf_age_bin, caf_freq_bin;             // Photon caf (stellar age, frequency) bins
  bool caf_in_range;                         // Age, frequency in range flags
#endif
#if output_ion_radial_age_freq
  int raf_radial_bin, raf_age_bin, raf_freq_bin; // Photon raf (radial, stellar age, frequency) bins
  bool raf_in_range;                         // Age, frequency in range flags
#endif
#if output_ion_distance_age_freq
  int daf_distance_bin, daf_age_bin, daf_freq_bin; // Photon daf (distance, stellar age, frequency) bins
  bool daf_in_range;                         // Age, frequency in range flags
#endif
#if output_radial_flow
  int rf_bin, rf_bin_src, rf_bin_max, rf_freq_bin; // Radial flow radial and frequency bins
  bool rf_in_range;                          // Radial flow in range flags
#endif
#if output_groups || output_group_flows
  int igrp;                                  // Group ID index
  bool valid_igrp;                           // Valid group ID index
#endif
#if output_groups
  bool in_grp_vir = false, in_grp_gal = false; // Group flags
#endif
#if output_group_flows
  int gf_bin, gf_bin_src, gf_bin_max, gf_freq_bin; // Group flow radial and frequency bins
  bool gf_in_range;                          // Group flow in range flags
#endif
#if output_subhalos || output_subhalo_flows
  int isub;                                  // Subhalo ID index
  bool valid_isub;                           // Valid subhalo ID index
#endif
#if output_subhalos
  bool in_sub_vir = false, in_sub_gal = false; // Subhalo flags
#endif
#if output_subhalo_flows
  int sf_bin, sf_bin_src, sf_bin_max, sf_freq_bin; // Subhalo flow radial and frequency bins
  bool sf_in_range;                          // Subhalo flow in range flags
#endif
  double weight;                             // Photon weight (normalized)
  double dust_weight;                        // Weight removed by dust absorption
  double ion_weight[n_ions_atoms];           // Weight removed by species absorption
  double dist_abs;                           // Photon absorption distance [cm]
  double l_tot;                              // Total path length [cm]

  // Spectral and radiative transfer properties
  double luminosity;                         // Source luminosity [erg/s]
  double Ndot;                               // Source rate [photons/s]
  double sigma_ions[n_ions];                 // Species cross-sections [cm^2]
  double &sigma_HI = sigma_ions[HI_ION], &sigma_HeI = sigma_ions[HeI_ION], &sigma_HeII = sigma_ions[HeII_ION]; // Relic references
  double sigma_epsilon_ions[n_ions];         // Species photoheating cross-sections [erg cm^2]
  double &sigma_epsilon_HI = sigma_epsilon_ions[HI_ION], &sigma_epsilon_HeI = sigma_epsilon_ions[HeI_ION], &sigma_epsilon_HeII = sigma_epsilon_ions[HeII_ION]; // Relic references
  double kappas[n_dust_species];             // Dust opacity [cm^2/g dust]
  double albedos[n_dust_species];            // Scattering albedo
  double cosines[n_dust_species];            // Scattering anisotropy
  double k_dust, k_scat;                     // Dust abs/scat coefficients
#if multiple_dust_species
  double k_dusts[n_dust_species];            // Dust absorption coefficients
  double k_scats[n_dust_species];            // Dust scattering coefficients
#else
  static const                               // Fixed dust species index
#endif
  int dust_species = 0;                      // Dust species index
  double n_ions_abs[n_ions];                 // Species number densities [cm^-3]
  double k_abss[n_ion_stats];                // Absorption coefficients [1/cm]
  double &k_abs = k_abss[STATS_tot], &k_gas_abs = k_abss[STATS_gas], &k_dust_abs = k_abss[STATS_dust]; // Utility references

private:
  void update_k_abs();                       // Updates absorption coefficients
  void move_photon(const double distance);   // Updates the position and weight

  // void ionization_scatter();
  void dust_scatter();

  double k_dust_LOS(const int cell);         // Dust absorption coefficient
  void calculate_LOS_int();                  // Intrinsic emission calculation
  void calculate_LOS_int(const int camera, const double phase_weight = 0.5);

  void calculate_LOS_ext();                  // Attenuated emission calculation
  void calculate_LOS_ext(const int camera, const double phase_weight = 0.5);

  void calculate_LOS(const int scatter_type); // Line-of-sight calculation
  void calculate_LOS(const int scatter_type, const int camera);
};

#endif // ION_PHOTON_INCLUDED
