/**********************
 * ionization/vars.cc *
 **********************

 * Module global variable declarations (defaults).

*/

#include "proto.h"

/* Information about the source */
int HI_bin = 0;                              // HI bin index
string source_model = "";                    // Spectral source model
string AGN_model = "";                       // AGN spectral source model
double f_esc_stars = 0.;                     // Escape fraction from stellar birth clouds
bool free_free = false;                      // Free-free continuum emission
bool free_bound = false;                     // Free-bound continuum emission
bool two_photon = false;                     // Two-photon continuum emission
double L_AGN = 0.;                           // AGN luminosity [erg/s]
double Ndot_AGN = 0.;                        // AGN rate [photons/s]
double T_point = 1e5;                        // Point source temperature [K]
double Lbol_point = 0.;                      // Point bolometric luminosity [erg/s]
bool plane_source = false;                   // Insert photons at a specified plane
string plane_direction = "";                 // Plane direction name
int plane_type = OUTSIDE;                    // Plane direction type
double plane_position = 0.;                  // Plane position [cm]
double plane_center_1, plane_center_2;       // Plane center positions
double plane_radius_1, plane_radius_2;       // Plane box/beam radii
double plane_center_x_bbox = 0.5, plane_center_y_bbox = 0.5, plane_center_z_bbox = 0.5; // Center [bbox]
double plane_radius_x_bbox = 0.5, plane_radius_y_bbox = 0.5, plane_radius_z_bbox = 0.5; // Radius [bbox]
double plane_area = 0.;                      // Plane surface area [cm^2]
bool plane_beam = false;                     // Use an ellipsoidal beam instead of a rectangle
double T_plane = 1e5;                        // Plane source temperature [K]
double Sbol_plane = 0.;                      // Plane bolometric surface density [erg/s/cm^2]
double Lbol_plane = 0.;                      // Plane bolometric luminosity [erg/s]
double single_Z = 0.02;                      // Single SED metallicity [mass fraction]
double single_age = 1.;                      // Single SED stellar age [Gyr]
int i_AGN = 0;                               // Cell index of the AGN source

/* Distinguish between source types */
int n_ion_source_types;                      // Number of source types used
double nu_exp = 0.;                          // Frequency boosting exponent (nu_exp > 0 favors higher frequency photons)
double min_HI_bin_cdf = 0.;                  // Minimum CDF value for >= 13.6 eV bins (step function correction)
array<double, MAX_ION_SOURCE_TYPES> mixed_ion_pdf; // Mixed source probability distribution function
array<double, MAX_ION_SOURCE_TYPES> mixed_ion_cdf; // Mixed source cumulative distribution function
array<double, MAX_ION_SOURCE_TYPES> mixed_ion_weights; // Mixed source weights (stars, cells, AGN, point)
vector<int> ion_source_types_mask;           // Source type mask indices
strings ion_source_names_mask;               // Source type names (masked)
vector<double> mixed_ion_pdf_mask;           // Mixed source pdf (masked)
vector<double> mixed_ion_cdf_mask;           // Mixed source cdf (masked)
vector<double> mixed_ion_weights_mask;       // Mixed source weights (masked)

/* Global spectral properties */
IonBinData ion_global;                       // Global spectral properties
IonBinData ion_stars;                        // Stellar spectral properties
IonBinData ion_gas;                          // Gas spectral properties
IonBinData ion_point;                        // Point spectral properties
IonBinData ion_plane;                        // Plane spectral properties
vector<double>& bin_L_tot = ion_global.bin_L; // Total bin luminosities [erg/s]
vector<double>& bin_Ndot_tot = ion_global.bin_Ndot; // Total bin rates [photons/s]
vector<double>& bin_L_stars = ion_stars.bin_L; // Stellar bin luminosities [erg/s]
vector<double>& bin_Ndot_stars = ion_stars.bin_Ndot; // Stellar bin rates [photons/s]
vector<double>& bin_L_gas = ion_gas.bin_L;   // Gas bin luminosities [erg/s]
vector<double>& bin_Ndot_gas = ion_gas.bin_Ndot; // Gas bin rates [photons/s]
vector<double> bin_Ndot2_stars;              // Stellar bin rates^2 [photons^2/s^2]
vector<double> bin_n_stars_eff;              // Bin effective number of stars: 1/<w>
vector<double> bin_Ndot2_gas;                // Gas bin rates^2 [photons^2/s^2]
vector<double> bin_n_cells_eff;              // Bin effective number of cells: 1/<w>
double n_stars_eff_HI = 0.;                  // Effective number of stars (> 13.6 eV): 1/<w>
double n_cells_eff_HI = 0.;                  // Effective number of cells (> 13.6 eV): 1/<w>

/* AGN spectral properties */
vector<double> bin_L_AGN;                    // Total bin luminosities [erg/s]
vector<double> bin_Ndot_AGN;                 // Total bin rates [photons/s]
vector<double> bin_cdf_AGN;                  // Bin cumulative distribution function
vector<double> bin_weight_AGN;               // Bin weights (for frequency boosting)
vector<double> sigma_Ndot_HI_AGN;            // HI rate cross-section [cm^2 photons/s]
vector<double> sigma_Ndot_HeI_AGN;           // HeI rate cross-section [cm^2 photons/s]
vector<double> sigma_Ndot_HeII_AGN;          // HeII rate cross-section [cm^2 photons/s]
vector<double> sigma_HI_AGN;                 // HI ionization cross-section [cm^2]
vector<double> sigma_HeI_AGN;                // HeI ionization cross-section [cm^2]
vector<double> sigma_HeII_AGN;               // HeII ionization cross-section [cm^2]
vector<double> sigma_L_HI_AGN;               // HI heating cross-section [cm^2 erg/s]
vector<double> sigma_L_HeI_AGN;              // HeI heating cross-section [cm^2 erg/s]
vector<double> sigma_L_HeII_AGN;             // HeII heating cross-section [cm^2 erg/s]
vector<double> epsilon_HI_AGN;               // HI ionization heating [erg]
vector<double> epsilon_HeI_AGN;              // HeI ionization heating [erg]
vector<double> epsilon_HeII_AGN;             // HeII ionization heating [erg]
vector<double> mean_energy_AGN;              // Mean energy of each frequency bin [erg]
vector<double> kappa_Ndot_AGN;               // Dust opacity rate [cm^2/g dust photons/s]
vector<double> albedo_Ndot_AGN;              // Dust albedo rate for scattering vs. absorption [photons/s]
vector<double> cosine_Ndot_AGN;              // Anisotropy parameter rate: <μ> for dust scattering [photons/s]
vector<double> kappa_AGN;                    // Dust opacity [cm^2/g dust]
vector<double> albedo_AGN;                   // Dust albedo for scattering vs. absorption
vector<double> cosine_AGN;                   // Anisotropy parameter: <μ> for dust scattering

/* Information about local ionization coupling */
Image bin_n_gamma;                           // Photon density [photons/cm^3]
vectors rate_ions;                           // Species photoionization integrals [1/s]
IonCellAgeFreq ion_cell_age_freq;            // Number of absorptions by cell, age, and frequency [photons/s]
IonRadialAgeFreq ion_radial_age_freq;        // Number of absorptions by radial, age, and frequency [photons/s]
IonDistanceAgeFreq ion_distance_age_freq;    // Number of absorptions by distance, age, and frequency [photons/s]
RadialFlow radial_flow;                      // Number of events by radial flow [photons/s]
vector<RadialFlow> group_flows;              // Number of events by radial flow for each group [photons/s]
vector<RadialFlow> subhalo_flows;            // Number of events by radial flow for each subhalo [photons/s]

/* Information about escaped photons */
double f_src_HI = 0., f2_src_HI = 0., n_photons_src_HI = 0.; // Source fraction for HI
double f_esc_HI = 0., f2_esc_HI = 0., n_photons_esc_HI = 0.; // Escape fraction for HI
double f_abs = 0., f_abs_HI = 0.;            // Global dust absorption fraction: sum(w)
double f2_abs = 0., f2_abs_HI = 0.;          // Global squared f_abs: sum(w^2)
double n_photons_abs = 0., n_photons_abs_HI = 0.; // Effective number of absorbed photons: 1/<w>
vector<double> f_ions;                       // Global species absorption fractions
double f2_HI, n_photons_HI;                  // Global species statistics
double l_abs = 0.;                           // Global absorption distance [cm]
vector<double> bin_f_src;                    // Global bin source fraction: sum(w0)
vector<double> bin_f2_src;                   // Global bin squared f_src: sum(w0^2)
vector<double> bin_n_photons_src;            // Effective number of emitted photons: 1/<w0>
vector<double> bin_f_esc;                    // Global bin escape fraction: sum(w)
vector<double> bin_f2_esc;                   // Global bin squared f_esc: sum(w^2)
vector<double> bin_n_photons_esc;            // Effective number of escaped photons: 1/<w>
vector<double> bin_f_abs;                    // Global bin dust absorption fraction
vector<double> bin_f2_abs;                   // Global bin squared f_abs: sum(w^2)
vector<double> bin_n_photons_abs;            // Effective number of absorbed photons: 1/<w>
vectors bin_f_ions;                          // Global bin species absorption fractions
vector<double> bin_f2_HI;                    // Global bin squared absorption fractions
vector<double> bin_n_photons_HI;             // Effective number of photons: 1/<w>
vector<double> bin_l_abs;                    // Global bin absorption distance [cm]

/* Information about output statistics */
bool output_abundances = true;               // Output abundances (x_HI etc)
bool output_photoionization = false;         // Output photoionization rates [1/s]
bool output_photoheating = false;            // Output photoheating rates [erg/s]
bool output_photon_density = false;          // Output photon density [photons/cm^3]
bool output_refinement = false;              // Output a refined version of the grid
double refinement_rtol = 1e-2;               // Relative photon rate tolerence
double refinement_x_HI = 1e-2;               // Neutral hydrogen fraction threshold
double refinement_Rmin = positive_infinity;  // Effective resolution threshold (R = Γ_HI / α_B n_H)
double refinement_mdeg = 5.;                 // Merging opening angle [degrees]
double refinement_pert = 1e-6;               // Perturbation factor for refinement
double Ndot_HI_threshold = 0.;               // Refinement threshold HI rate [photons/s]
vector<int> freq_bins;                       // Frequency bin at escape [bin]
array<bool, n_ions_atoms> output_ion_absorptions; // Output weight removed by species absorption
vectors ion_weights;                         // Weight removed by species absorption
int n_active_ion_weights = 0;                // Number of active ionization weight statistics
vector<int> active_ion_weights;              // Active ionization weight statistics indices
bool output_absorption_distance = false;     // Output photon absorption distance [cm]
vector<double> dust_weights;                 // Weight removed by dust absorption
vector<double> dist_abss;                    // Photon absorption distance [cm]

/* Information about cell rates */
vector<double> Ndot_UVB_HI_cells;            // UV background ionizations for each cell
vectors Ndot_rec_ions_cells;                 // Recombinations for each cell
vectors Ndot_col_ions_cells;                 // Collisional ionizations for each cell
vectors Ndot_phot_ions_cells;                // Photoionizations for each cell
vectors Ndot_cxi_H_ions_cells;               // Charge exchange ionizations (H) for each cell
vectors Ndot_cxi_He_ions_cells;              // Charge exchange ionizations (He) for each cell
vectors Ndot_cxr_H_ions_cells;               // Charge exchange recombinations (H) for each cell
vectors Ndot_cxr_He_ions_cells;              // Charge exchange recombinations (He) for each cell
int n_rec_ions_cells = 0, n_col_ions_cells = 0, n_phot_ions_cells = 0; // Number of active stats
int n_cxi_H_ions_cells = 0, n_cxi_He_ions_cells = 0, n_cxr_H_ions_cells = 0, n_cxr_He_ions_cells = 0;
bool output_cells_UVB_HI = false;            // Output cell UV background ionizations
vector<bool> output_cells_rec_ions;          // Output cell recombinations
vector<bool> output_cells_col_ions;          // Output cell collisional ionizations
vector<bool> output_cells_phot_ions;         // Output cell photoionizations
vector<bool> output_cells_cxi_H_ions;        // Output cell charge ionizations (H)
vector<bool> output_cells_cxi_He_ions;       // Output cell charge ionizations (He)
vector<bool> output_cells_cxr_H_ions;        // Output cell charge recombinations (H)
vector<bool> output_cells_cxr_He_ions;       // Output cell charge recombinations (He)

/* Information about secondary cameras (mcrt) */
bool output_bin_escape_fractions = true;     // Output bin escape fractions
vectors bin_f_escs;                          // Bin escape fractions [fraction]
vectors bin_f_escs_ext;                      // Attenuated bin escape fractions [fraction]
Cubes bin_images;                            // Escaped bin SB images [photons/s/cm^2]
Cubes bin_images_int;                        // Intrinsic bin SB images [photons/s/cm^2]
Cubes bin_images_ext;                        // Attenuated bin SB images [photons/s/cm^2]
Images bin_radial_images;                    // Escaped bin radial SB images [photons/s/cm^2]
Images bin_radial_images_int;                // Intrinsic bin radial SB images [photons/s/cm^2]
Images bin_radial_images_ext;                // Attenuated bin radial SB images [photons/s/cm^2]

static int setup_ion_output() {
  output_ion_absorptions.fill(false);
  return 0;
}
static const int setup_ion_output_flag = setup_ion_output(); // Setup helper
