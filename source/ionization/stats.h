/**********************
 * ionization/stats.h *
 **********************

 * Module stats utilities.

*/

#ifndef IONIZATION_STATS_INCLUDED
#define IONIZATION_STATS_INCLUDED

// Calculate radial bin from radial position [cm]
static inline int find_radial_bin(const double radius, const int n_radial_bins, const vector<double>& radial_edges) {
  if (radius < radial_edges[0])
    error("Photon cannot have radius < 0: radius = "+to_string(radius));
  if (radius >= radial_edges[n_radial_bins])
    return OUTSIDE_DUAL;                     // Outside radial range
  int radial_bin = 0;                        // Initial radial bin
  while (radius >= radial_edges[radial_bin+1])
    radial_bin++;                            // Search radial bins
  return radial_bin;
}

// Calculate age bin from stellar age [Gyr]
static inline int find_age_bin(const double age, const int n_age_bins, const vector<double>& age_edges) {
  int age_bin = -1;                          // Initial age bin
  if (age < age_edges[n_age_bins])
    while (age >= age_edges[age_bin+1])
      age_bin++;                             // Search age bins
  return age_bin;
}

// Calculate frequency range bin from frequency bin
static inline int find_freq_bin(const int frequency_bin, const int n_freq_bins, const vector<int>& freq_edges) {
  int freq_bin = -1;                         // Initial frequency bin
  if (frequency_bin < freq_edges[n_freq_bins])
    while (frequency_bin >= freq_edges[freq_bin+1])
      freq_bin++;                            // Search frequency bins
  return freq_bin;
}

// Compute the maximum distance the photon can travel in the radial cell.
static inline tuple<double, int> radial_face_distance(const Vec3& point, const Vec3& direction,
  const int cell, const int n_radial_bins, const vector<double>& radial_edges) {
  const double mu_r = direction.dot(point);  // Unnormalized radial cosine
  const double r2 = point.dot();             // Radius^2
  const double r_diff = mu_r*mu_r - r2;      // (k*r)^2 - r^2
  if (cell == 0) {                           // No inner radius
    const double rp = radial_edges[1];       // First outer radius
    return make_tuple(sqrt(r_diff + rp*rp) - mu_r, 1);
  }
  if (cell == OUTSIDE_DUAL) {                // Outside radial range
    if (mu_r >= 0.)                          // No intersection
      return make_tuple(positive_infinity, OUTSIDE_DUAL);
    const double rm = radial_edges[n_radial_bins]; // Inner radius
    const double disc_inner = r_diff + rm*rm; // Inner discriminant
    if (disc_inner <= 0.)                    // No intersection
      return make_tuple(positive_infinity, OUTSIDE_DUAL);
    return make_tuple(-sqrt(disc_inner) - mu_r, n_radial_bins-1);
  }
  const double rm = radial_edges[cell];      // Inner radius
  const double disc_inner = r_diff + rm*rm;  // Inner discriminant
  // Outward radial propagation: l = -k*r + sqrt(|k*r|^2 - r^2 + r_outer^2)
  // Case: k*r >= 0 or k*r can be < 0 but not cross inner radius
  if (mu_r >= 0. || disc_inner <= 0.) {
    const int next = cell + 1;               // Outer face values
    const double rp = radial_edges[next];    // Outer radius
    return make_tuple(sqrt(r_diff + rp*rp) - mu_r, (next < n_radial_bins) ? next : OUTSIDE_DUAL);
  }

  // Inward radial propagation: l = -k*r - sqrt(|k*r|^2 - r^2 + r_inner^2)
  // Case: k*r < 0, cell > 0, and disc_inner > 0
  return make_tuple(-sqrt(disc_inner) - mu_r, cell - 1);
}

// Number of absorptions by cell, age, and frequency [photons/s]
struct IonCellAgeFreq {
  int n_age_bins = 0;                        // Number of stellar age bins
  int n_freq_bins = 0;                       // Number of frequency bins
  vector<double> age_edges;                  // Stellar age bin edges [Gyr]
  vector<int> freq_edges;                    // Frequency bin edge indices
  strings freq_names;                        // Frequency bin edge names
  vector<double> freq_edges_eV;              // Frequency bin edges [eV]
  Cubes stats;                               // Number of absorptions [photons/s]

  // Initialize data structures
  void setup() {
    stats = Cubes(n_active_ion_stats, Cube(n_cells, n_age_bins, n_freq_bins)); // Allocate stat memory
  }

  // Calculate age bin from stellar age [Gyr]
  inline int find_age_bin(const double age) {
    return ::find_age_bin(age, n_age_bins, age_edges);
  }

  // Calculate frequency range bin from frequency bin
  inline int find_freq_bin(const int frequency_bin) {
    return ::find_freq_bin(frequency_bin, n_freq_bins, freq_edges);
  }
};

// Number of absorptions by radial, age, and frequency [photons/s]
struct IonRadialAgeFreq {
  int n_radial_bins = 0;                     // Number of radial bins
  int n_age_bins = 0;                        // Number of stellar age bins
  int n_freq_bins = 0;                       // Number of frequency bins
  vector<double> radial_edges;               // Radial bin edges [cm]
  vector<double> age_edges;                  // Stellar age bin edges [Gyr]
  vector<int> freq_edges;                    // Frequency bin edge indices
  strings freq_names;                        // Frequency bin edge names
  vector<double> freq_edges_eV;              // Frequency bin edges [eV]
  Cubes stats;                               // Number of absorptions [photons/s]

  // Initialize data structures
  void setup() {
    stats = Cubes(n_active_ion_stats, Cube(n_radial_bins, n_age_bins, n_freq_bins)); // Allocate stat memory
  }

  // Calculate radial bin from radial position [cm]
  inline int find_radial_bin(const double radius) {
    return ::find_radial_bin(radius, n_radial_bins, radial_edges);
  }

  // Calculate age bin from stellar age [Gyr]
  inline int find_age_bin(const double age) {
    return ::find_age_bin(age, n_age_bins, age_edges);
  }

  // Calculate frequency range bin from frequency bin
  inline int find_freq_bin(const int frequency_bin) {
    return ::find_freq_bin(frequency_bin, n_freq_bins, freq_edges);
  }

  // Compute the maximum distance the photon can travel in the radial cell.
  tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
    if (cell == OUTSIDE)                     // Source is not a star
      return make_tuple(positive_infinity, OUTSIDE);
    return radial_face_distance(point, direction, cell, n_radial_bins, radial_edges);
  }
};

struct RadialFlow {
  int n_radial_bins = 0;                     // Number of radial bins
  int n_freq_bins = 0;                       // Number of frequency bins
  vector<double> radial_edges;               // Radial bin edges [cm]
  vector<int> freq_edges;                    // Frequency bin edge indices
  strings freq_names;                        // Frequency bin edge names
  vector<double> freq_edges_eV;              // Frequency bin edges [eV]
  Image src;                                 // Emission (source radial and frequency bin)
  Image esc;                                 // Escape (source radial and frequency bin)
  Images stats;                              // Absorption statistics (event radial and frequency bin)
  Cube pass;                                 // First passage (source radial and shell boundaries and frequency bin)
  Cube flow;                                 // Flow (source radial and shell boundaries and frequency bin)

  // Initialize data structures
  void setup() {
    src = Image(n_radial_bins, n_freq_bins); // Allocate emission
    esc = Image(n_radial_bins, n_freq_bins); // Allocate escape
    stats = Images(n_active_ion_stats, Image(n_radial_bins, n_freq_bins)); // Allocate absorption statistics
    pass = Cube(n_radial_bins, n_radial_bins, n_freq_bins); // Allocate first passage
    flow = Cube(n_radial_bins, n_radial_bins, n_freq_bins); // Allocate flow
  }

  // Calculate radial bin from radial position [cm]
  inline int find_radial_bin(const double radius) {
    return ::find_radial_bin(radius, n_radial_bins, radial_edges);
  }

  // Calculate frequency range bin from frequency bin
  inline int find_freq_bin(const int frequency_bin) {
    return ::find_freq_bin(frequency_bin, n_freq_bins, freq_edges);
  }

  // Compute the maximum distance the photon can travel in the radial cell.
  tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
    return radial_face_distance(point, direction, cell, n_radial_bins, radial_edges);
  }
};

// Number of absorptions by distance, age, and frequency [photons/s]
struct IonDistanceAgeFreq {
  int n_distance_bins = 0;                   // Number of distance bins
  int n_age_bins = 0;                        // Number of stellar age bins
  int n_freq_bins = 0;                       // Number of frequency bins
  vector<double> distance_edges;             // Distance bin edges [cm]
  vector<double> age_edges;                  // Stellar age bin edges [Gyr]
  vector<int> freq_edges;                    // Frequency bin edge indices
  strings freq_names;                        // Frequency bin edge names
  vector<double> freq_edges_eV;              // Frequency bin edges [eV]
  Cubes stats;                               // Number of absorptions [photons/s]

  // Initialize data structures
  void setup() {
    stats = Cubes(n_active_ion_stats, Cube(n_distance_bins, n_age_bins, n_freq_bins)); // Allocate stat memory
  }

  // Calculate age bin from stellar age [Gyr]
  inline int find_age_bin(const double age) {
    return ::find_age_bin(age, n_age_bins, age_edges);
  }

  // Calculate frequency range bin from frequency bin
  inline int find_freq_bin(const int frequency_bin) {
    return ::find_freq_bin(frequency_bin, n_freq_bins, freq_edges);
  }

  // Compute the maximum distance the photon can travel in the distance cell.
  tuple<double, int> face_distance(const Vec3& point, const Vec3& direction, const int cell) {
    if (cell == OUTSIDE)                     // Source is not a star
      return make_tuple(positive_infinity, OUTSIDE);
    return radial_face_distance(point, direction, cell, n_distance_bins, distance_edges);
  }
};

// Global declarations
extern IonCellAgeFreq ion_cell_age_freq;     // Number of absorptions by cell, age, and frequency [photons/s]
extern IonRadialAgeFreq ion_radial_age_freq; // Number of absorptions by radial, age, and frequency [photons/s]
extern IonDistanceAgeFreq ion_distance_age_freq; // Number of absorptions by distance, age, and frequency [photons/s]
extern RadialFlow radial_flow;               // Number of events by radial flow [photons/s]
extern vector<RadialFlow> group_flows;       // Number of events by radial flow for each group [photons/s]
extern vector<RadialFlow> subhalo_flows;     // Number of events by radial flow for each subhalo [photons/s]

#endif // IONIZATION_STATS_INCLUDED
