/************************
 * ionization/config.cc *
 ************************

 * Module configuration for the simulation.

*/

#include "proto.h"
#include "Ionization.h"
#include "../config.h" // Configuration functions
#include "../io_hdf5.h" // HDF5 read/write functions

dust_strings get_dust_tags(); // Dust species trailing tags
void setup_Z_age_table(const string& source_file_Z_age, vector<double>& bin_edges_eV, const dust_strings& dust_models);
void setup_ff_table(vector<double>& bin_edges_eV, const dust_strings& dust_models); // Tabulated free-free spectra
void setup_fb_table(vector<double>& bin_edges_eV, const dust_strings& dust_models); // Tabulated free-bound spectra
void setup_2p_table(vector<double>& bin_edges_eV, const dust_strings& dust_models); // Tabulated two-photon spectra
void setup_blackbody(IonBinData& ion, vector<double>& bin_edges_eV, const dust_strings& dust_models, const double Lbol_black, const double T_black);
void setup_single_SED(IonBinData& ion, const string& source_file_Z_age, vector<double>& bin_edges_eV, const dust_strings& dust_models, const double single_Z, const double single_age, const double Lbol);
void setup_bc03_imf_solar_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bc03_imf_half_solar_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bc03_imf_table(const string& dust_model); // Tabulated BC03 spectral properties
void setup_bpass_imf135_100_table(const string& dust_model); // Tabulated BPASS (135_100) spectral properties
void setup_bpass_chab_100_table(const string& dust_model); // Tabulated BPASS (chab_100) spectral properties

constexpr array<int, n_ions> all_x_indices = {
  HI_Fraction, HeI_Fraction, HeII_Fraction,
  CI_Fraction, CII_Fraction, CIII_Fraction, CIV_Fraction, CV_Fraction, CVI_Fraction,
  NI_Fraction, NII_Fraction, NIII_Fraction, NIV_Fraction, NV_Fraction, NVI_Fraction, NVII_Fraction,
  OI_Fraction, OII_Fraction, OIII_Fraction, OIV_Fraction, OV_Fraction, OVI_Fraction, OVII_Fraction, OVIII_Fraction,
  NeI_Fraction, NeII_Fraction, NeIII_Fraction, NeIV_Fraction, NeV_Fraction, NeVI_Fraction, NeVII_Fraction, NeVIII_Fraction,
  MgI_Fraction, MgII_Fraction, MgIII_Fraction, MgIV_Fraction, MgV_Fraction, MgVI_Fraction, MgVII_Fraction, MgVIII_Fraction, MgIX_Fraction, MgX_Fraction,
  SiI_Fraction, SiII_Fraction, SiIII_Fraction, SiIV_Fraction, SiV_Fraction, SiVI_Fraction, SiVII_Fraction, SiVIII_Fraction, SiIX_Fraction, SiX_Fraction, SiXI_Fraction, SiXII_Fraction,
  SI_Fraction, SII_Fraction, SIII_Fraction, SIV_Fraction, SV_Fraction, SVI_Fraction, SVII_Fraction, SVIII_Fraction, SIX_Fraction, SX_Fraction, SXI_Fraction, SXII_Fraction, SXIII_Fraction, SXIV_Fraction,
  FeI_Fraction, FeII_Fraction, FeIII_Fraction, FeIV_Fraction, FeV_Fraction, FeVI_Fraction, FeVII_Fraction, FeVIII_Fraction, FeIX_Fraction, FeX_Fraction, FeXI_Fraction, FeXII_Fraction, FeXIII_Fraction, FeXIV_Fraction, FeXV_Fraction, FeXVI_Fraction
};
constexpr array<double, n_ions> all_ions_eV = {
  HI_eV, HeI_eV, HeII_eV,
  CI_eV, CII_eV, CIII_eV, CIV_eV, CV_eV, CVI_eV,
  NI_eV, NII_eV, NIII_eV, NIV_eV, NV_eV, NVI_eV, NVII_eV,
  OI_eV, OII_eV, OIII_eV, OIV_eV, OV_eV, OVI_eV, OVII_eV, OVIII_eV,
  NeI_eV, NeII_eV, NeIII_eV, NeIV_eV, NeV_eV, NeVI_eV, NeVII_eV, NeVIII_eV,
  MgI_eV, MgII_eV, MgIII_eV, MgIV_eV, MgV_eV, MgVI_eV, MgVII_eV, MgVIII_eV, MgIX_eV, MgX_eV,
  SiI_eV, SiII_eV, SiIII_eV, SiIV_eV, SiV_eV, SiVI_eV, SiVII_eV, SiVIII_eV, SiIX_eV, SiX_eV, SiXI_eV, SiXII_eV,
  SI_eV, SII_eV, SIII_eV, SIV_eV, SV_eV, SVI_eV, SVII_eV, SVIII_eV, SIX_eV, SX_eV, SXI_eV, SXII_eV, SXIII_eV, SXIV_eV,
  FeI_eV, FeII_eV, FeIII_eV, FeIV_eV, FeV_eV, FeVI_eV, FeVII_eV, FeVIII_eV, FeIX_eV, FeX_eV, FeXI_eV, FeXII_eV, FeXIII_eV, FeXIV_eV, FeXV_eV, FeXVI_eV
};

/* Setup active atom and ionization state information. */
void setup_active_ions(const bool require_contiguous = true) {
  active_atoms.reserve(n_atoms);             // Reserve memory for active atoms
  beg_ions.reserve(n_atoms);                 // Active atom beginning ion indices
  end_ions.reserve(n_atoms);                 // Active atom ending ion indices
  ion_counts.reserve(n_atoms);               // Active atom number of active ions
  active_ions.reserve(n_ions);               // Reserve memory for active ions
  for (int atom = 0; atom < n_atoms; ++atom) { // Loop over atoms
    int ion_count = 0, beg_ion = -1;         // Initialize ion counter and first ion
    const int offset = atom_offsets[atom];   // Atom offset
    for (int ion = atom_ranges[atom]; ion < atom_ranges[atom+1]; ++ion) {
      if (fields[ion+offset].read) {         // Ion read flag
        active_ions.push_back(ion);          // Save active ion indices
        if (beg_ion < 0) {
          beg_ion = ion;                     // This is the first ion
        } else if (require_contiguous && ion != beg_ion + ion_count) {
          error("Ion indices are not contiguous for " + atom_names[atom]);
        }
        ++ion_count;                         // Increment ion counter
      }
    }
    if (ion_count > 0 || fields[HydrogenDensity+atom].read) { // Atom is active
      active_atoms.push_back(atom);          // Save active atom indices
      beg_ions.push_back(beg_ion);           // Save first ion
      end_ions.push_back(beg_ion + ion_count); // Ion end range
      ion_counts.push_back(ion_count);       // Save number of active ions
    }
  }
}

/* Setup ionization fraction indices. */
void setup_x_indices() {
  n_active_atoms = active_atoms.size();      // Number of active atoms
  n_active_ions = active_ions.size();        // Number of active ions
  n_active_ions_atoms = n_active_ions + n_active_atoms; // Number of active ions and atoms
  x_indices.resize(n_active_ions);           // Ionization fraction data indices
  ions_eV.resize(n_active_ions);             // Ionization threshold energies [eV]
  ions_erg.resize(n_active_ions);            // Ionization threshold energies [erg]
  for (int ai = 0; ai < n_active_ions; ++ai) {
    const int ion = active_ions[ai];         // Active ion index
    x_indices[ai] = all_x_indices[ion];      // Copy active fraction indices
    ions_eV[ai] = all_ions_eV[ion];          // Copy active threshold energies
    ions_erg[ai] = ions_eV[ai] * eV;         // Convert to erg
  }
}

/* Determine which ionization states to include. */
void ions_config(YAML::Node& file) {
  double ion_max_eV = 100.;                  // Maximum ionization threshold to include [eV]
  load("ion_max_eV", ion_max_eV);

  // Hydrogen and Helium
  if (file["HeI_ion"] || file["HeII_ion"])
    read_helium = false;                     // Implicitly reset helium default
  load("helium_ions", read_helium);
  bool HeII_ion = read_helium;               // Default to helium_ions
  if (!HeII_ion) load("HeII_ion", HeII_ion);
  read_HeII = (HeII_ion && HeII_eV <= ion_max_eV);
  bool HeI_ion = HeII_ion;                   // Default to HeII_ion
  if (!HeI_ion) load("HeI_ion", HeI_ion);
  read_HeI = (HeI_ion && HeI_eV <= ion_max_eV);
  if (read_HeI) read_helium = true;

  // Metals
  bool read_metals = false;
  load("metal_ions", read_metals);
  for (int atom = 2; atom < n_atoms; ++atom) { // Loop over atoms
    bool read_atom = read_metals;            // Default to metals
    load(atom_names[atom] + "_ions", read_atom); // Override {atom}_ions
    bool prev_read_ion = read_atom;          // Default to atom
    for (int ion = atom_ranges[atom+1] - 1; ion >= atom_ranges[atom]; --ion) {
      bool read_ion = prev_read_ion;         // Default to previous read flag
      if (!read_ion)                         // Skip if previous is true
        load(ion_names[ion] + "_ion", read_ion); // Override {ion}_ion
      fields[all_x_indices[ion]].read = (read_ion && all_ions_eV[ion] <= ion_max_eV); // Save ion read flag
      prev_read_ion = read_ion;              // Save previous read flag
    }
    const int x_index = all_x_indices[atom_ranges[atom]]; // First ion index
    const int n_index = HydrogenDensity + atom; // Number density index
    fields[n_index].read = fields[x_index].read; // Save atom read flag
  }
}

/* Determine bin energy edges. */
void setup_bin_edges(YAML::Node& file) {
  array<bool, n_ions> ions_bin = {false};    // Ion bin flags
  ions_bin[HI_ION] = true;                   // Hydrogen is always included
  ions_bin[HeI_ION] = read_HeI; ions_bin[HeII_ION] = read_HeII; // Helium defaults
  bool ion_bins = false, metal_bins = false;
  load("ion_bins", ion_bins);
  if (ion_bins) {
    for (int atom = 2; atom < n_atoms; ++atom) {
      const int offset = atom_offsets[atom]; // Atom offset
      for (int ion = atom_ranges[atom]; ion < atom_ranges[atom+1]; ++ion)
        ions_bin[ion] = fields[ion+offset].read; // Override ion bin flags
    }
  }

  // Atom bin flags
  load("metal_bins", metal_bins);            // Control all metal bins
  for (int atom = 1; atom < n_atoms; ++atom) {
    const string name = atom_names[atom] + "_bins";
    bool atom_bins = metal_bins;             // Default to metal_bins
    if (file[name]) {
      load(name, atom_bins);                 // Control all atom bins
      for (int ion = atom_ranges[atom]; ion < atom_ranges[atom+1]; ++ion)
        ions_bin[ion] = atom_bins;           // Set all ions
    }
  }

  // Ion bin flags
  for (int ion = 0; ion < n_ions; ++ion) {
    const string name = ion_names[ion] + "_bin";
    load(name, ions_bin[ion]);               // Override ion bin flags
  }
  if constexpr (SVIII_eV == MgIX_eV) {
    if (ions_bin[SVIII_ION] && ions_bin[MgIX_ION])
      ions_bin[SVIII_ION] = false;           // Avoid duplicate bin
  }

  // Save bin options
  for (int ion = 0; ion < n_ions; ++ion) {
    if (ions_bin[ion]) {                     // Ion bin flag
      bin_names.push_back(ion_names[ion]);   // Save ion name
      bin_edges_eV.push_back(all_ions_eV[ion]); // Save ionization energy [eV]
    }
  }

  // Add the lowest bin edge (optional)
  double min_bin_eV = -1., min_bin_angstrom = -1.; // Lower limit of highest bin [eV]
  load("min_bin_angstrom", min_bin_angstrom); // Allow setting defalt in angstroms
  if (min_bin_angstrom > 0.)
    min_bin_eV = h * c / (min_bin_angstrom * angstrom * eV); // Convert to eV
  load("min_bin_eV", min_bin_eV);            // Default value is no lower limit
  if (min_bin_eV > 0.) {
    if (bin_edges_eV.size() > 0) {
      const double min_eV = omp_min(bin_edges_eV); // Minimum bin edge [eV]
      if (min_eV <= min_bin_eV)
        min_bin_eV = min_eV / 1.1;           // Ensure min_bin_eV bounds all bin edges
    }
    bin_edges_eV.push_back(min_bin_eV);      // Add the first edge
    bin_names.push_back("min");              // Name the first edge
  }

  // Add the highest bin edge
  {
    double max_bin_eV = h * c / (angstrom * eV); // Upper limit of highest bin [eV]
    double max_bin_angstrom = -1.;           // Upper limit of highest bin [angstrom]
    load("max_bin_angstrom", max_bin_angstrom); // Allow setting defalt in angstroms
    if (max_bin_angstrom > 0.)
      max_bin_eV = h * c / (max_bin_angstrom * angstrom * eV); // Convert to eV
    load("max_bin_eV", max_bin_eV);          // Default value is 1 angstrom
    if (bin_edges_eV.size() == 0)
      root_error("Ionization module requires at least one energy bin!");
    const double max_eV = omp_max(bin_edges_eV); // Maximum bin edge [eV]
    if (max_eV >= max_bin_eV)
      max_bin_eV = 1.1 * max_eV;             // Ensure max_bin_eV bounds all bin edges
    bin_edges_eV.push_back(max_bin_eV);      // Last edge is already sorted
    bin_names.push_back("max");              // Name the last edge
  }

  // Sort all bin edges
  n_bins = bin_edges_eV.size() - 1;          // Number of energy bins
  for (int i_bin = 0; i_bin < n_bins - 1; ++i_bin) {
    int i_min = i_bin;                       // Initialize min index
    for (int i_bin_p = i_bin + 1; i_bin_p < n_bins; ++i_bin_p) {
      if (bin_edges_eV[i_bin_p] < bin_edges_eV[i_min])
        i_min = i_bin_p;                     // Update min index
    }
    if (i_min > i_bin) {                     // Requires sorting
      std::swap(bin_edges_eV[i_bin], bin_edges_eV[i_min]);
      std::swap(bin_names[i_bin], bin_names[i_min]);
    }
  }

  // Remove duplicate bin edges
  for (int i_bin = 0; i_bin < n_bins - 1; ++i_bin) {
    if (bin_edges_eV[i_bin] == bin_edges_eV[i_bin + 1]) {
      bin_edges_eV.erase(bin_edges_eV.begin() + i_bin);
      bin_names.erase(bin_names.begin() + i_bin);
      --n_bins; --i_bin;                     // Update bin count and index
    }
  }

  // Loop over bin edges and insert new edges if requested
  if (file["max_bin_dex"]) {
    double max_bin_dex = -1.;                // Minimum frequency bin width in dex
    load("max_bin_dex", max_bin_dex, ">0");
    double log_edge = log10(bin_edges_eV[0]), next_log_edge; // Initialize log edge
    for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
      next_log_edge = log10(bin_edges_eV[i_bin + 1]);
      const double bin_dex = next_log_edge - log_edge;
      if (bin_dex > max_bin_dex) {
        const int n_add = floor(bin_dex / max_bin_dex); // Number of new edges to insert
        const double log_step = bin_dex / double(n_add + 1); // Equal logspaced intervals
        for (int j = 1; j <= n_add; ++j) {   // Insert new bin edges and names
          const double new_edge = pow(10., log_edge + double(j) * log_step);
          const string new_name = bin_names[i_bin] + "." + to_string(j);
          bin_edges_eV.insert(bin_edges_eV.begin() + i_bin + j, new_edge);
          bin_names.insert(bin_names.begin() + i_bin + j, new_name);
        }
        n_bins += n_add; i_bin += n_add;     // Update bin count and index
      }
      log_edge = next_log_edge;              // Update log edge
    }
  }

  // Find the HI bin (after all sorting and inserting)
  for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
    if (bin_names[i_bin] == "HI") {
      HI_bin = i_bin;                        // Found HI bin
      i_bin += n_bins;                       // Stop search
    }
  }
}

/* Utility function for loading distance edges. */
template <typename T>
void load_distance_edges(YAML::Node& subfile, T& data) {
  // Distance bin information
  if (subfile["distance_linspace_pc"]) {     // Linearly spaced distance bins [pc]
    Vec3 rad_lin; subload("distance_linspace_pc", rad_lin);
    const int n_lin = int(rad_lin.z);
    if (n_lin <= 1)
      root_error("distance_linspace_pc must have n > 1");
    const double dx_lin = (rad_lin.y - rad_lin.x) / (n_lin - 1);
    data.distance_edges.resize(n_lin);
    #pragma omp parallel for
    for (int i = 0; i < n_lin; ++i)
      data.distance_edges[i] = pc * (rad_lin.x + double(i) * dx_lin); // Distance edges [cm]
  } else if (subfile["distance_logspace_pc"]) { // Logarithmically spaced distance bins [pc]
    Vec3 rad_log; subload("distance_logspace_pc", rad_log);
    const int n_log = int(rad_log.z);
    if (n_log <= 1)
      root_error("distance_logspace_pc must have n > 1");
    const double dx_log = (rad_log.y - rad_log.x) / (n_log - 1);
    data.distance_edges.resize(n_log + 1);
    #pragma omp parallel for
    for (int i = 0; i < n_log; ++i)
      data.distance_edges[i+1] = pc * pow(10., rad_log.x + double(i) * dx_log); // Distance edges [cm]
  } else if (subfile["distance_edges_pc"]) {
    subload("distance_edges_pc", data.distance_edges);
    const int n_distance_edges = data.distance_edges.size();
    for (int i = 0; i < n_distance_edges; ++i)
      data.distance_edges[i] *= pc;          // Convert to cm
  } else if (subfile["distance_edges_cm"]) {
    subload("distance_edges_cm", data.distance_edges); // Already in cm
  } else {
    data.distance_edges = {0., pc, 10.*pc, 1e2*pc, kpc, 10.*kpc, 100.*kpc, Mpc}; // Distance edges [cm]
  }
  data.n_distance_bins = data.distance_edges.size() - 1; // Number of distance bins
  data.distance_edges[0] = 0.;               // Extend to the origin
  if (data.n_distance_bins <= 0)
    root_error("Must have at least one distance bin!");
  if (data.distance_edges[data.n_distance_bins] < max_bbox_width)
    data.distance_edges[data.n_distance_bins] = max_bbox_width; // Extend to the box size
  for (int distance_bin = 0; distance_bin < data.n_distance_bins; ++distance_bin) {
    if (data.distance_edges[distance_bin+1] <= data.distance_edges[distance_bin])
      root_error("Distance bin edges must be monotonically increasing!");
  }
}

/* Utility function for loading age edges. */
template <typename T>
void load_age_edges(YAML::Node& subfile, T& data) {
  // Age bin information
  if (subfile["age_edges_Myr"]) {
    subload("age_edges_Myr", data.age_edges);
    const int n_age_edges = data.age_edges.size();
    for (int i = 0; i < n_age_edges; ++i)
      data.age_edges[i] *= 1e-3;             // Convert to Gyr
  } else if (subfile["age_edges_Gyr"]) {
    subload("age_edges_Gyr", data.age_edges); // Already in Gyr
  } else {
    data.age_edges = {0., 1e-2, 1e-1, 1e2};  // Stellar ages [Gyr]
  }
  data.n_age_bins = data.age_edges.size() - 1; // Number of age bins
  for (int age_bin = 0; age_bin < data.n_age_bins; ++age_bin) {
    if (data.age_edges[age_bin+1] <= data.age_edges[age_bin])
      root_error("Age bin edges should be increasing!");
  }
}

/* Utility function for loading frequency bin edges. */
template <typename T>
void load_freq_edges(YAML::Node& subfile, T& data) {
  // Frequency bin information
  strings temp_names;                        // Temporary names
  if (subfile["freq_edge_names"]) {
    subload("freq_edge_names", temp_names);  // Frequency bin edge names
  } else {
    temp_names = {"HI", "HeI", "max"};       // Default values
  }
  const int n_temp = temp_names.size();      // Number of temporary elements
  data.freq_edges.reserve(n_temp);           // Allocate space
  data.freq_names.reserve(n_temp);
  data.freq_edges_eV.reserve(n_temp);
  for (int i_bin = 0; i_bin <= n_bins; ++i_bin) { // Already sorted by frequency
    for (int i_temp = 0; i_temp < n_temp; ++i_temp) { // Linear search
      if (bin_names[i_bin] == temp_names[i_temp]) { // Skip missing bins
        data.freq_edges.push_back(i_bin);    // Save frequency bin index
        data.freq_names.push_back(bin_names[i_bin]); // Save frequency name
        data.freq_edges_eV.push_back(bin_edges_eV[i_bin]); // Save frequency [eV]
        i_temp += n_temp;                    // Avoid duplicates
      }
    }
  }
  data.n_freq_bins = data.freq_names.size() - 1; // Number of frequency bins
  if (data.n_freq_bins <= 0)
    root_error("freq_edge_names requires at least two valid names! e.g. [HI, ..., max]");
}

/* Utility function for loading frequency bin edges. */
template <typename T>
void load_freq_edges(YAML::Node& subfile, vector<T>& data) {
  // Frequency bin information
  const int n_data = data.size();            // Size of data vector
  strings temp_names;                        // Temporary names
  if (subfile["freq_edge_names"]) {
    subload("freq_edge_names", temp_names);  // Frequency bin edge names
  } else {
    temp_names = {"HI", "HeI", "max"};       // Default values
  }
  const int n_temp = temp_names.size();      // Number of temporary elements
  vector<int> freq_edges;                    // Frequency bin edge indices
  strings freq_names;                        // Frequency bin edge names
  vector<double> freq_edges_eV;              // Frequency bin edges [eV]
  freq_edges.reserve(n_temp);                // Allocate space
  freq_names.reserve(n_temp);
  freq_edges_eV.reserve(n_temp);
  for (int i_bin = 0; i_bin <= n_bins; ++i_bin) { // Already sorted by frequency
    for (int i_temp = 0; i_temp < n_temp; ++i_temp) { // Linear search
      if (bin_names[i_bin] == temp_names[i_temp]) { // Skip missing bins
        freq_edges.push_back(i_bin);         // Save frequency bin index
        freq_names.push_back(bin_names[i_bin]); // Save frequency name
        freq_edges_eV.push_back(bin_edges_eV[i_bin]); // Save frequency [eV]
        i_temp += n_temp;                    // Avoid duplicates
      }
    }
  }
  const int n_freq_bins = freq_names.size() - 1; // Number of frequency bins
  if (n_freq_bins <= 0)
    root_error("freq_edge_names requires at least two valid names! e.g. [HI, ..., max]");
  #pragma omp parallel for
  for (int i = 0; i < n_data; ++i) {
    auto& data_i = data[i];                  // Reference to data element
    data_i.freq_edges = freq_edges;          // Copy frequency bin edge indices
    data_i.freq_names = freq_names;          // Copy frequency bin edge names
    data_i.freq_edges_eV = freq_edges_eV;    // Copy frequency bin edges [eV]
    data_i.n_freq_bins = n_freq_bins;        // Copy number of frequency bins
  }
}

/* Module configuration for the simulation. */
void Ionization::module_config(YAML::Node& file) {
  // Set the dust properties: Opacity, albedo, scattering anisotropy
  dust_strings tags = get_dust_tags();       // Dust species trailing tags
  for (int i = 0; i < n_dust_species; ++i) {
    const string dust_str = "dust_model" + tags[i];
    if (file[dust_str])
      load(dust_str, dust_models[i]);        // Dust model: SMC, MW, etc.
  }
  if constexpr (!multiple_dust_species) if (dust_models[0] == "") {
    load("albedo_HI", albedo_HI);            // Dust scattering albedos
    load("albedo_HeI", albedo_HeI);
    load("albedo_HeII", albedo_HeII);
    load("cosine_HI", cosine_HI);            // Dust scattering anisotropy parameters
    load("cosine_HeI", cosine_HeI);
    load("cosine_HeII", cosine_HeII);
    load("kappa_HI", kappa_HI);              // Dust opacities [cm^2/g dust]
    load("kappa_HeI", kappa_HeI);
    load("kappa_HeII", kappa_HeII);
  }
  gas_config(file);                          // General gas configuration

  // Star parameters
  load("f_esc_stars", f_esc_stars);          // Escape fraction from stellar birth clouds

  // Gas source detection
  load("free_free", free_free);              // Free-free continuum emission
  load("free_bound", free_bound);            // Free-bound continuum emission
  load("two_photon", two_photon);            // Two-photon continuum emission
  if (free_free || free_bound || two_photon)
    cell_based_emission = true;              // Include cell-based sources

  // AGN source detection
  if (file["AGN_model"]) {
    AGN_emission = true;                     // AGN emission
    load("AGN_model", AGN_model);            // Model for the AGN source
    if (AGN_model != "Stalevski12" && AGN_model != "Lusso15")
      root_error("Unrecognized AGN source model: " + AGN_model);
  }

  // Point source detection
  if (file["x_point"]) {
    load("x_point", x_point);                // Point source x position [cm]
    r_point.x = x_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["y_point"]) {
    load("y_point", y_point);                // Point source y position [cm]
    r_point.y = y_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["z_point"]) {
    load("z_point", z_point);                // Point source z position [cm]
    r_point.z = z_point;                     // (x, y, z) [cm]
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (file["point"]) {
    load("point", r_point);                  // Specified as sequence
    x_point = r_point.x;                     // Update (x,y,z) for consistency
    y_point = r_point.y;
    z_point = r_point.z;
    if (!point_source)
      point_source = true;                   // Update point source flag
  }
  if (!point_source)
    load("point_source", point_source);      // Allow flag activation

  // Plane source detection
  if (file["plane_direction"]) {
    load("plane_direction", plane_direction); // Plane source direction
    if (plane_direction == "+x")
      plane_type = PosX;
    else if (plane_direction == "-x")
      plane_type = NegX;
    else if (plane_direction == "+y")
      plane_type = PosY;
    else if (plane_direction == "-y")
      plane_type = NegY;
    else if (plane_direction == "+z")
      plane_type = PosZ;
    else if (plane_direction == "-z")
      plane_type = NegZ;
    else
      root_error("plane_direction must be {+x, -x, +y, -y, +z, -z} but got "+plane_direction);
    plane_source = true;                     // Update plane source flag
  }

  // Internal simulation flags
  if (file["source_file_Z_age"]) {
    load("source_file_Z_age", source_file_Z_age); // File for the sources (Z,age,wavelength)
  } else {
    load("source_model", source_model);      // Model for the sources
  }
  read_electron_fraction = true;             // Generally requires electrons
  read_HI = read_HII = read_HeI = read_HeII = true; // Default H/He states
  read_helium = true;                        // Generally requires helium states
  bool variable_bins = (source_file_Z_age != "" || cell_based_emission || point_source || plane_source);
  if (variable_bins) {
    ions_config(file);                       // Determine which ions to include
    setup_active_ions();                     // Setup active ionization state information
    setup_bin_edges(file);                   // Determine bin energy edges
  } else {
    active_atoms = {H_ATOM, He_ATOM};        // Otherwise assume H/He atoms
    beg_ions = {HI_ION, HeI_ION};            // Begin with HI, HeI ions
    end_ions = {HI_ION + 1, HeI_ION + 2};    // End with HII, HeIII ions
    ion_counts = {1, 2};                     // Number of active ions
    active_ions = {HI_ION, HeI_ION, HeII_ION}; // Tracking HI, HeI, HeII ions
  }
  setup_x_indices();                         // Setup ionization fraction indices

  if (source_file_Z_age != "") {
    source_model = "file";                   // Source model comes from a file
    if (!plane_source) {
      star_based_emission = true;            // Star-based emission
      read_m_init_star = true;               // Requires initial stellar masses
      read_Z_star = true;                    // Requires stellar metallicities
      read_age_star = true;                  // Requires stellar ages
      if (focus_groups_on_emission || focus_subhalos_on_emission)
        read_v_star = true;                  // Requires stellar velocities
      setup_Z_age_table(source_file_Z_age, bin_edges_eV, dust_models); // Tabulated spectral properties
    }
    // Allow a birth cloud escape fraction (modify ionization tables)
    if (f_esc_stars > 0.) {
      const int n_spec = spec_Z_age.log_L.size();
      const double log_f_esc_stars = log10(f_esc_stars);
      #pragma omp parallel for
      for (int i = 0; i < n_spec; ++i) {
        spec_Z_age.log_L[i] += log_f_esc_stars; // Luminosities [erg/s/Msun]
        spec_Z_age.log_Ndot[i] += log_f_esc_stars; // Photon rates [photons/s/Msun]
        for (auto& log_sigma_Ndot_ion : spec_Z_age.log_sigma_Ndot_ions)
          log_sigma_Ndot_ion[i] += log_f_esc_stars; // Cross-sections [cm^2]
        for (auto& log_sigma_L_ion : spec_Z_age.log_sigma_L_ions)
          log_sigma_L_ion[i] += log_f_esc_stars; // Photoheating [erg]
        for (int j = 0; j < n_dust_species; ++j) {
          spec_Z_age.log_albedos_Ndot[j][i] += log_f_esc_stars; // Dust scattering albedos
          spec_Z_age.cosines_Ndot[j][i] *= f_esc_stars; // Dust scattering anisotropy parameters
          spec_Z_age.log_kappas_Ndot[j][i] += log_f_esc_stars; // Dust opacities [cm^2/g dust]
        }
      }
    }
  } else if (source_model == "GMC") {
    star_based_emission = true;              // Star-based emission
    read_m_massive_star = true;              // Requires (massive) stellar masses
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    bin_names = {"HI", "HeI", "HeII", "max"}; // Bin energy edge names
    ion_stars.sigma_ions[HI_ION] = {2.99466e-18, 5.68179e-19, 7.97294e-20}; // HI ionizing cross-sections
    ion_stars.sigma_ions[HeI_ION] = {0., 3.71495e-18, 5.70978e-19}; // HeI ionizing cross-sections
    ion_stars.sigma_ions[HeII_ION] = {0., 0., 1.06525e-18}; // HeII ionizing cross-sections
    ion_stars.epsilon_ions[HI_ION] = {0., 0., 0.}; // HI photoheating
    ion_stars.epsilon_ions[HeI_ION] = {0., 0., 0.}; // HeI photoheating
    ion_stars.epsilon_ions[HeII_ION] = {0., 0., 0.}; // HeII photoheating
    ion_stars.mean_energy = {18.8567*eV, 35.0815*eV, 64.9768*eV}; // Mean energy of each bin [erg]
    if (f_esc_stars > 0.)
      root_error("f_esc_stars is not implemented for source_model = GMC");
    if constexpr (multiple_dust_species)
      root_error("GMC source model only supports one dust species!");
  } else if (source_model == "MRT") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    read_Z_star = true;                      // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    bin_names = {"HI", "HeI", "HeII", "max"}; // Bin energy edge names
    ion_stars.sigma_ions[HI_ION] = {3.3718e-18, 7.89957e-19, 1.09584e-19}; // HI ionizing cross-sections
    ion_stars.sigma_ions[HeI_ION] = {0., 5.10279e-18, 7.76916e-19}; // HeI ionizing cross-sections
    ion_stars.sigma_ions[HeII_ION] = {0., 0., 1.42413e-18}; // HeII ionizing cross-sections
    ion_stars.epsilon_ions[HI_ION] = {0., 0., 0.}; // HI photoheating
    ion_stars.epsilon_ions[HeI_ION] = {0., 0., 0.}; // HeI photoheating
    ion_stars.epsilon_ions[HeII_ION] = {0., 0., 0.}; // HeII photoheating
    ion_stars.mean_energy = {18.0058*eV, 29.8868*eV, 56.8456*eV}; // Mean energy of each bin [erg]
    if (f_esc_stars > 0.)
      root_error("f_esc_stars is not implemented for source_model = MRT");
    if constexpr (multiple_dust_species)
      root_error("MRT source model only supports one dust species!");
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR" || source_model == "BC03-IMF" ||
             source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    star_based_emission = true;              // Star-based emission
    read_m_init_star = true;                 // Requires initial stellar masses
    if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100")
      read_Z_star = true;                    // Requires stellar metallicities
    read_age_star = true;                    // Requires stellar ages
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    bin_names = {"HI", "HeI", "HeII", "max"}; // Bin energy edge names
    if (source_model == "BC03-IMF-SOLAR")
      setup_bc03_imf_solar_table(dust_models[0]); // Tabulated BC03 spectral properties
    else if (source_model == "BC03-IMF-HALF-SOLAR")
      setup_bc03_imf_half_solar_table(dust_models[0]); // Tabulated BC03 spectral properties
    else if (source_model == "BC03-IMF")
      setup_bc03_imf_table(dust_models[0]);  // Tabulated BC03 spectral properties
    else if (source_model == "BPASS-IMF-135-100")
      setup_bpass_imf135_100_table(dust_models[0]); // Tabulated BPASS (135_100) spectral properties
    else // (source_model == "BPASS-CHAB-100")
      setup_bpass_chab_100_table(dust_models[0]); // Tabulated BPASS (chab_100) spectral properties
    // Allow a birth cloud escape fraction (modify ionization tables)
    if (f_esc_stars > 0.) {
      if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR") {
        const int n_ages = ion_age.Ndot_HI.size();
        auto& L_HI = ion_age.L_HI;
        auto& L_HeI = ion_age.L_HeI;
        auto& L_HeII = ion_age.L_HeII;
        auto& Ndot_HI = ion_age.Ndot_HI;
        auto& Ndot_HeI = ion_age.Ndot_HeI;
        auto& Ndot_HeII = ion_age.Ndot_HeII;
        for (int i_age = 0; i_age < n_ages; ++i_age) {
          L_HI[i_age] *= f_esc_stars;        // Luminosities [erg/s/Msun]
          L_HeI[i_age] *= f_esc_stars;
          L_HeII[i_age] *= f_esc_stars;
          Ndot_HI[i_age] *= f_esc_stars;     // Photon rates [photons/s/Msun]
          Ndot_HeI[i_age] *= f_esc_stars;
          Ndot_HeII[i_age] *= f_esc_stars;
        }
      } else if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
        const int n_Zs = ion_Z_age.log_Ndot_HI.size();
        const int n_ages = ion_Z_age.log_Ndot_HI[0].size();
        const double log_f_esc_stars = log10(f_esc_stars);
        #pragma omp parallel for
        for (int i_Z = 0; i_Z < n_Zs; ++i_Z) {
          auto& log_L_HI_Z = ion_Z_age.log_L_HI[i_Z];
          auto& log_L_HeI_Z = ion_Z_age.log_L_HeI[i_Z];
          auto& log_L_HeII_Z = ion_Z_age.log_L_HeII[i_Z];
          auto& log_Ndot_HI_Z = ion_Z_age.log_Ndot_HI[i_Z];
          auto& log_Ndot_HeI_Z = ion_Z_age.log_Ndot_HeI[i_Z];
          auto& log_Ndot_HeII_Z = ion_Z_age.log_Ndot_HeII[i_Z];
          auto& log_sigma_Ndot_HI_1_Z = ion_Z_age.log_sigma_Ndot_HI_1[i_Z];
          auto& log_sigma_Ndot_HI_2_Z = ion_Z_age.log_sigma_Ndot_HI_2[i_Z];
          auto& log_sigma_Ndot_HI_3_Z = ion_Z_age.log_sigma_Ndot_HI_3[i_Z];
          auto& log_sigma_Ndot_HeI_2_Z = ion_Z_age.log_sigma_Ndot_HeI_2[i_Z];
          auto& log_sigma_Ndot_HeI_3_Z = ion_Z_age.log_sigma_Ndot_HeI_3[i_Z];
          auto& log_sigma_Ndot_HeII_3_Z = ion_Z_age.log_sigma_Ndot_HeII_3[i_Z];
          auto& log_sigma_L_HI_1_Z = ion_Z_age.log_sigma_L_HI_1[i_Z];
          auto& log_sigma_L_HI_2_Z = ion_Z_age.log_sigma_L_HI_2[i_Z];
          auto& log_sigma_L_HI_3_Z = ion_Z_age.log_sigma_L_HI_3[i_Z];
          auto& log_sigma_L_HeI_2_Z = ion_Z_age.log_sigma_L_HeI_2[i_Z];
          auto& log_sigma_L_HeI_3_Z = ion_Z_age.log_sigma_L_HeI_3[i_Z];
          auto& log_sigma_L_HeII_3_Z = ion_Z_age.log_sigma_L_HeII_3[i_Z];
          auto& log_albedo_Ndot_HI_Z = ion_Z_age.log_albedo_Ndot_HI[i_Z];
          auto& log_albedo_Ndot_HeI_Z = ion_Z_age.log_albedo_Ndot_HeI[i_Z];
          auto& log_albedo_Ndot_HeII_Z = ion_Z_age.log_albedo_Ndot_HeII[i_Z];
          auto& log_cosine_Ndot_HI_Z = ion_Z_age.log_cosine_Ndot_HI[i_Z];
          auto& log_cosine_Ndot_HeI_Z = ion_Z_age.log_cosine_Ndot_HeI[i_Z];
          auto& log_cosine_Ndot_HeII_Z = ion_Z_age.log_cosine_Ndot_HeII[i_Z];
          auto& log_kappa_Ndot_HI_Z = ion_Z_age.log_kappa_Ndot_HI[i_Z];
          auto& log_kappa_Ndot_HeI_Z = ion_Z_age.log_kappa_Ndot_HeI[i_Z];
          auto& log_kappa_Ndot_HeII_Z = ion_Z_age.log_kappa_Ndot_HeII[i_Z];
          for (int i_age = 0; i_age < n_ages; ++i_age) {
            log_L_HI_Z[i_age] += log_f_esc_stars; // Luminosities [erg/s/Msun]
            log_L_HeI_Z[i_age] += log_f_esc_stars;
            log_L_HeII_Z[i_age] += log_f_esc_stars;
            log_Ndot_HI_Z[i_age] += log_f_esc_stars; // Photon rates [photons/s/Msun]
            log_Ndot_HeI_Z[i_age] += log_f_esc_stars;
            log_Ndot_HeII_Z[i_age] += log_f_esc_stars;
            log_sigma_Ndot_HI_1_Z[i_age] += log_f_esc_stars; // Cross-sections [cm^2]
            log_sigma_Ndot_HI_2_Z[i_age] += log_f_esc_stars;
            log_sigma_Ndot_HI_3_Z[i_age] += log_f_esc_stars;
            log_sigma_Ndot_HeI_2_Z[i_age] += log_f_esc_stars;
            log_sigma_Ndot_HeI_3_Z[i_age] += log_f_esc_stars;
            log_sigma_Ndot_HeII_3_Z[i_age] += log_f_esc_stars;
            log_sigma_L_HI_1_Z[i_age] += log_f_esc_stars; // Photoheating [erg]
            log_sigma_L_HI_2_Z[i_age] += log_f_esc_stars;
            log_sigma_L_HI_3_Z[i_age] += log_f_esc_stars;
            log_sigma_L_HeI_2_Z[i_age] += log_f_esc_stars;
            log_sigma_L_HeI_3_Z[i_age] += log_f_esc_stars;
            log_sigma_L_HeII_3_Z[i_age] += log_f_esc_stars;
            log_albedo_Ndot_HI_Z[i_age] += log_f_esc_stars; // Dust scattering albedos
            log_albedo_Ndot_HeI_Z[i_age] += log_f_esc_stars;
            log_albedo_Ndot_HeII_Z[i_age] += log_f_esc_stars;
            log_cosine_Ndot_HI_Z[i_age] += log_f_esc_stars; // Dust scattering anisotropy parameters
            log_cosine_Ndot_HeI_Z[i_age] += log_f_esc_stars;
            log_cosine_Ndot_HeII_Z[i_age] += log_f_esc_stars;
            log_kappa_Ndot_HI_Z[i_age] += log_f_esc_stars; // Dust opacities [cm^2/g dust]
            log_kappa_Ndot_HeI_Z[i_age] += log_f_esc_stars;
            log_kappa_Ndot_HeII_Z[i_age] += log_f_esc_stars;
          }
        }
      }
    }
    if constexpr (multiple_dust_species)
      root_error("Pre-tabulated source models only supports one dust species!");
  } else if (source_model == "custom") {
    load("n_bins", n_bins, ">0");            // Number of frequency bins
    root_error("The custom source_model is not implemented yet!");
    // load("kappa_dust", kappa_dust, ">=0");   // Dust opacity [cm^2/g dust]
    // load("albedo", albedo, "[0,1]");         // Dust albedo for scattering vs. absorption
    // load("cosine", cosine, "[-1,1]");        // Anisotropy parameter: <μ> for dust scattering
    // if (fabs(cosine) < 1e-6)
    //   cosine = (cosine < 0.) ? -1e-6 : 1e-6; // Avoid division by zero
    // if (f_esc_stars > 0.)
    //   root_error("f_esc_stars is not implemented for source_model = custom");
  } else if (source_model != "")
    root_error("Requested stellar source model is not implemented: " + source_model);

  if (star_based_emission || cell_based_emission)
    load("j_exp", j_exp, "(0,1]");           // Luminosity boosting exponent
  if (cell_based_emission)
    load("V_exp", V_exp);                    // Volume boosting exponent
  load("nu_exp", nu_exp);                    // Frequency boosting exponent
  load("min_HI_bin_cdf", min_HI_bin_cdf);    // Minimum CDF value for >= 13.6 eV bins

  // Gas sources
  if (free_free)                             // Free-free continuum emission
    setup_ff_table(bin_edges_eV, dust_models); // Tabulated spectral properties
  if (free_bound)                            // Free-bound continuum emission
    setup_fb_table(bin_edges_eV, dust_models); // Tabulated spectral properties
  if (two_photon)                            // Two-photon continuum emission
    setup_2p_table(bin_edges_eV, dust_models); // Tabulated spectral properties

  // AGN source
  if (AGN_emission) {
    if (variable_bins && n_bins != 3)        // Requires 3 bins
      root_error("AGN is only implemented for n_bins = 3");
    n_bins = 3;                              // Ionizing photons [HI,HeI,HeII]
    incompatible("Lbol", "Lbol_Lsun");       // Cannot specify both Lbol and Lbol_Lsun
    if (file["Lbol"]) {
      load("Lbol", Lbol_AGN, ">0");          // Bolometric luminosity [erg/s]
    } else {
      double Lbol_Lsun = 1e11;               // Standard normalization [Lsun]
      load("Lbol_Lsun", Lbol_Lsun, ">0");
      Lbol_AGN = Lbol_Lsun * Lsun;           // Convert to cgs [erg/s]
    }
  }

  // UV background
  if (file["UVB_model"]) {
    load("UVB_model", UVB_model);            // UV background model: FG11, etc.
    if (UVB_model != "")
      have_UVB = true;                       // Activate ionizing background
  }
  load("self_shielding", self_shielding);    // Include UVB self-shielding

  // Point source variables
  if (point_source) {
    load("T_point", T_point, ">0");          // Point source temperature [K]
    // Fraction of luminosity and photon rate in range [xmin,xmax]
    const double xmin = bin_edges_eV[0] * eV / (kB * T_point); // x = hν / kT
    const double xmax = bin_edges_eV[n_bins] * eV / (kB * T_point); // x = hν / kT
    const double frac_Lbol = planck_fraction_Lbol(xmin) - planck_fraction_Lbol(xmax);
    const double frac_Ndot = planck_fraction_Ndot(xmin) - planck_fraction_Ndot(xmax);
    const double conv_bol = 2.701178032919064 * kB * T_point; // Lbol / Ndotbol = (π^4 kB T) / (30 zeta(3))
    if (file["Lbol_point"]) {
      load("Lbol_point", Lbol_point, ">0");  // Point source bolometric luminosity [erg/s]
    } else if (file["L_point"]) {
      load("L_point", ion_point.L, ">0");    // Point source luminosity [erg/s]
      Lbol_point = ion_point.L / frac_Lbol;  // Point source bolometric luminosity [erg/s]
    } else {
      ion_point.Ndot = 1.;                   // Change the default from zero
      load("Ndot_point", ion_point.Ndot, ">0"); // Point source photon rate [photons/s]
      Lbol_point = ion_point.Ndot * conv_bol / frac_Ndot; // Point source luminosity [erg/s]
    }
    ion_point.L = frac_Lbol * Lbol_point;    // Point source luminosity [erg/s]
    const double Ndot_bol = Lbol_point / conv_bol; // Point source bolometric photon rate [photons/s]
    ion_point.Ndot = frac_Ndot * Ndot_bol;   // Point source photon rate [photons/s]
    dust_strings tags = get_dust_tags();     // Dust species trailing tags
    for (int i = 0; i < n_dust_species; ++i) {
      const string& tag = tags[i];
      load("kappa_point"+tag, ion_point.constant_kappas[i], ">0"); // Dust opacity [cm^2/g dust]
      load("albedo_point"+tag, ion_point.constant_albedos[i], "[0,1]"); // Dust albedo for scattering vs. absorption
      load("cosine_point"+tag, ion_point.constant_cosines[i], "[-1,1]"); // Anisotropy parameter: <μ> for dust scattering
    }
    setup_blackbody(ion_point, bin_edges_eV, dust_models, Lbol_point, T_point); // Spectral properties
    // Overwrite point source cross-sections [cm^2]
    if (file["sigma_boost"]) {
      double sigma_boost = 0.;               // Boost factor for cross-sections
      load("sigma_boost", sigma_boost, ">0");
      for (int ai = 0; ai < n_active_ions; ++ai) {
        auto& sigma_ion = ion_point.sigma_ions[ai];
        auto& sigma_Ndot_ion = ion_point.sigma_Ndot_ions[ai];
        auto& sigma_L_ion = ion_point.sigma_L_ions[ai];
        auto& sigma_epsilon_ion = ion_point.sigma_epsilon_ions[ai];
        auto& epsilon_ion = ion_point.epsilon_ions[ai];
        const double ion_erg = ions_erg[ai];
        for (int i = 0; i < n_bins; i++) {
          sigma_ion[i] *= sigma_boost;
          sigma_Ndot_ion[i] *= sigma_boost;
          sigma_L_ion[i] *= sigma_boost;
          sigma_epsilon_ion[i] = chop(sigma_L_ion[i] - sigma_Ndot_ion[i] * ion_erg);
          epsilon_ion[i] = sigma_epsilon_ion[i] / sigma_Ndot_ion[i];
        }
      }
    }
  }

  // Plane source variables
  if (plane_source) {
    load("plane_beam", plane_beam);          // Use an ellipsoidal beam instead of a rectangle
    if (plane_beam) {
      load("plane_center_x_bbox", plane_center_x_bbox, "(0,1)"); // Center [bbox]
      load("plane_center_y_bbox", plane_center_y_bbox, "(0,1)");
      load("plane_center_z_bbox", plane_center_z_bbox, "(0,1)");
      load("plane_radius_x_bbox", plane_radius_x_bbox, ">0"); // Radius [bbox]
      load("plane_radius_y_bbox", plane_radius_y_bbox, ">0");
      load("plane_radius_z_bbox", plane_radius_z_bbox, ">0");
      if (plane_center_x_bbox < plane_radius_x_bbox || plane_center_x_bbox + plane_radius_x_bbox > 1.)
        root_error("plane_radius_x_bbox must be < plane_center_x_bbox and < 1 - plane_center_x_bbox");
      if (plane_center_y_bbox < plane_radius_y_bbox || plane_center_y_bbox + plane_radius_y_bbox > 1.)
        root_error("plane_radius_y_bbox must be < plane_center_y_bbox and < 1 - plane_center_y_bbox");
      if (plane_center_z_bbox < plane_radius_z_bbox || plane_center_z_bbox + plane_radius_z_bbox > 1.)
        root_error("plane_radius_z_bbox must be < plane_center_z_bbox and < 1 - plane_center_z_bbox");
    }
    if (source_file_Z_age != "") {
      load("single_Z", single_Z, ">0");      // Metallicity for single SED source
      load("single_age", single_age, ">0");  // Age for single SED source
      if (file["Sbol_plane"]) {
        load("Sbol_plane", Sbol_plane, ">0"); // Plane bolometric surface density [erg/s/cm^2]
        Lbol_plane = Sbol_plane;             // Fill Lbol and multiply by area later
      } else {
        load("Lbol_plane", Lbol_plane, ">0"); // Plane source bolometric luminosity [erg/s]
      }
      setup_single_SED(ion_plane, source_file_Z_age, bin_edges_eV, dust_models, single_Z, single_age, Lbol_plane); // Spectral properties
    } else {
      load("T_plane", T_plane, ">0");        // Plane source temperature [K]
      // Fraction of luminosity and photon rate in range [xmin,xmax]
      const double xmin = bin_edges_eV[0] * eV / (kB * T_plane); // x = hν / kT
      const double xmax = bin_edges_eV[n_bins] * eV / (kB * T_point); // x = hν / kT
      const double frac_Lbol = planck_fraction_Lbol(xmin) - planck_fraction_Lbol(xmax);
      const double frac_Ndot = planck_fraction_Ndot(xmin) - planck_fraction_Ndot(xmax);
      const double conv_bol = 2.701178032919064 * kB * T_plane; // Lbol / Ndotbol = (π^4 kB T) / (30 zeta(3))
      if (file["Sbol_plane"]) {
        load("Sbol_plane", Sbol_plane, ">0"); // Plane bolometric surface density [erg/s/cm^2]
        Lbol_plane = Sbol_plane;             // Fill Lbol and multiply by area later
      } else if (file["Lbol_plane"]) {
        load("Lbol_plane", Lbol_plane, ">0"); // Plane source bolometric luminosity [erg/s]
      } else if (file["L_plane"]) {
        load("L_plane", ion_plane.L, ">0");  // Plane source luminosity [erg/s]
        Lbol_plane = ion_plane.L / frac_Lbol; // Plane source bolometric luminosity [erg/s]
      } else {
        ion_plane.Ndot = 1.;                 // Change the default from zero
        load("Ndot_plane", ion_plane.Ndot, ">0"); // Plane source photon rate [photons/s]
        Lbol_plane = ion_plane.Ndot * conv_bol / frac_Ndot; // Plane source luminosity [erg/s]
      }
      ion_plane.L = frac_Lbol * Lbol_plane;  // Plane source luminosity [erg/s]
      const double Ndot_bol = Lbol_plane / conv_bol; // Plane source bolometric photon rate [photons/s]
      ion_plane.Ndot = frac_Ndot * Ndot_bol; // Plane source photon rate [photons/s]
      dust_strings tags = get_dust_tags();   // Dust species trailing tags
      for (int i = 0; i < n_dust_species; ++i) {
        const string& tag = tags[i];
        load("kappa_plane"+tag, ion_plane.constant_kappas[i], ">0"); // Dust opacity [cm^2/g dust]
        load("albedo_plane"+tag, ion_plane.constant_albedos[i], "[0,1]"); // Dust albedo for scattering vs. absorption
        load("cosine_plane"+tag, ion_plane.constant_cosines[i], "[-1,1]"); // Anisotropy parameter: <μ> for dust scattering
      }
      setup_blackbody(ion_plane, bin_edges_eV, dust_models, Lbol_plane, T_plane); // Spectral properties
    }
  }

  if (!star_based_emission && !cell_based_emission && !AGN_emission && !point_source && !plane_source)
    root_error("Ionization module requires either source_file_Z_age, source_model, free_free, AGN_model, point_source, or plane_source.");

  // Escape variables
  escape_config(file);                       // General escape setup
  load("output_photons", output_photons);    // Output escaped photon packets
  load("output_stars", output_stars);        // Output star emission and escape
  load("output_cells", output_cells);        // Output cell emission and escape
  load("output_cells_UVB_HI", output_cells_UVB_HI); // Output cell UV background ionization
  // Output cell collisional ionization, charge exchange, recombination, and photoionization
  n_rec_ions_cells = 0; n_col_ions_cells = 0; n_phot_ions_cells = 0; // Initialize counters
  n_cxi_H_ions_cells = 0; n_cxi_He_ions_cells = 0; n_cxr_H_ions_cells = 0; n_cxr_He_ions_cells = 0;
  output_cells_rec_ions.resize(n_active_ions);
  output_cells_col_ions.resize(n_active_ions);
  output_cells_phot_ions.resize(n_active_ions);
  output_cells_cxi_H_ions.resize(n_active_ions);
  output_cells_cxi_He_ions.resize(n_active_ions);
  output_cells_cxr_H_ions.resize(n_active_ions);
  output_cells_cxr_He_ions.resize(n_active_ions);
  const vector<int> valid_cxi_H = {FeII_ION, FeI_ION, SI_ION, SiII_ION, SiI_ION, MgII_ION, MgI_ION, OI_ION, NI_ION, CI_ION};
  const vector<int> valid_cxi_He = {SIII_ION, SII_ION, SiIII_ION, SiII_ION, SiI_ION, OI_ION, NII_ION, CII_ION, CI_ION};
  const vector<int> valid_cxr_H = {FeIV_ION, FeIII_ION, FeII_ION, SIV_ION, SIII_ION, SII_ION, SI_ION, SiIV_ION, SiIII_ION, SiII_ION, MgIV_ION, MgIII_ION, MgII_ION, NeIV_ION, NeIII_ION, NeII_ION, OIV_ION, OIII_ION, OII_ION, OI_ION, NIV_ION, NIII_ION, NII_ION, NI_ION, CIV_ION, CIII_ION, CII_ION, CI_ION, HeII_ION, HeI_ION};
  const vector<int> valid_cxr_He = {FeIV_ION, FeIII_ION, SIV_ION, SIII_ION, SiIV_ION, SiIII_ION, MgIV_ION, MgIII_ION, NeIV_ION, NeIII_ION, NeII_ION, OIV_ION, OIII_ION, OII_ION, NIV_ION, NIII_ION, NII_ION, CIV_ION, CIII_ION};
  int i_cxi_H = valid_cxi_H.size() - 1, i_cxi_He = valid_cxi_He.size() - 1; // Last index
  int i_cxr_H = valid_cxr_H.size() - 1, i_cxr_He = valid_cxr_He.size() - 1;
  int ion_cxi_H = valid_cxi_H[i_cxi_H], ion_cxi_He = valid_cxi_He[i_cxi_He]; // Last ion
  int ion_cxr_H = valid_cxr_H[i_cxr_H], ion_cxr_He = valid_cxr_He[i_cxr_He];
  bool oc_ions = false;                      // Default to false
  load("charge_exchange", charge_exchange);  // Include charge exchange
  load("dialectric_recombination", dialectric_recombination); // Include dialectric recombination
  load("output_cells_ions", oc_ions);
  for (int ai = 0; ai < n_active_ions; ++ai) {
    const int ion = active_ions[ai];         // Active ion index
    const string ion_name = ion_names[ion];  // Ion name
    const string rec_name = fields[x_indices[ai]+1].name.substr(2); // Upper ion name
    bool oc_ion = oc_ions;                   // Default to false
    load("output_cells_" + ion_name, oc_ion);
    bool oc_rec = oc_ion, oc_col = oc_ion, oc_phot = oc_ion, oc_cx = oc_ion; // Default to oc_ion
    load("output_cells_rec_" + rec_name, oc_rec);
    load("output_cells_col_" + ion_name, oc_col);
    load("output_cells_phot_" + ion_name, oc_phot);
    load("output_cells_cx_" + ion_name, oc_cx);
    bool oc_cxi_H = oc_cx, oc_cxi_He = oc_cx, oc_cxr_H = oc_cx, oc_cxr_He = oc_cx; // Default to oc_cx
    load("output_cells_cxi_H_" + ion_name, oc_cxi_H);
    load("output_cells_cxi_He_" + ion_name, oc_cxi_He);
    load("output_cells_cxr_H_" + rec_name, oc_cxr_H);
    load("output_cells_cxr_He_" + rec_name, oc_cxr_He);
    if (oc_cxi_H) {
      while (ion_cxi_H < ion && i_cxi_H > 0)
        ion_cxi_H = valid_cxi_H[--i_cxi_H];  // Sorted search
      if (ion_cxi_H != ion)
        oc_cxi_H = false;                    // Not valid for this ion
    }
    if (oc_cxi_He) {
      while (ion_cxi_He < ion && i_cxi_He > 0)
        ion_cxi_He = valid_cxi_He[--i_cxi_He]; // Sorted search
      if (ion_cxi_He != ion)
        oc_cxi_He = false;                   // Not valid for this ion
    }
    if (oc_cxr_H) {
      while (ion_cxr_H < ion && i_cxr_H > 0)
        ion_cxr_H = valid_cxr_H[--i_cxr_H];  // Sorted search
      if (ion_cxr_H != ion)
        oc_cxr_H = false;                    // Not valid for this ion
    }
    if (oc_cxr_He) {
      while (ion_cxr_He < ion && i_cxr_He > 0)
        ion_cxr_He = valid_cxr_He[--i_cxr_He]; // Sorted search
      if (ion_cxr_He != ion)
        oc_cxr_He = false;                   // Not valid for this ion
    }
    output_cells_rec_ions[ai] = oc_rec;      // Save output preferences
    output_cells_col_ions[ai] = oc_col;
    output_cells_phot_ions[ai] = oc_phot;
    output_cells_cxi_H_ions[ai] = oc_cxi_H;
    output_cells_cxi_He_ions[ai] = oc_cxi_He;
    output_cells_cxr_H_ions[ai] = oc_cxr_H;
    output_cells_cxr_He_ions[ai] = oc_cxr_He;
    if (oc_rec) ++n_rec_ions_cells;          // Increment counters
    if (oc_col) ++n_col_ions_cells;
    if (oc_phot) ++n_phot_ions_cells;
    if (oc_cxi_H) ++n_cxi_H_ions_cells;
    if (oc_cxi_He) ++n_cxi_He_ions_cells;
    if (oc_cxr_H) ++n_cxr_H_ions_cells;
    if (oc_cxr_He) ++n_cxr_He_ions_cells;
  }
  if (output_stars && !star_based_emission)
    root_error("output_stars requires star_based_emission");
  if (output_cells && !cell_based_emission)
    root_error("output_cells requires cell_based_emission");
  if (output_photons) {
    load("photon_file", photon_file);        // Output a separate photon file
    load("output_n_scat", output_n_scat);    // Output number of scattering events
    load("output_absorption_distance", output_absorption_distance); // Output photon absorption distance [cm]
    load("output_source_position", output_source_position); // Output position at emission [cm]
    // Output weight removed by species absorption
    active_ion_weights.reserve(n_active_ions_atoms); // Reserve memory
    for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
      bool oia_atom = false;                 // Default to false
      load("output_" + atom_names[active_atoms[aa]] + "_absorption", oia_atom);
      const int beg_ion = beg_ions[aa], end_ion = end_ions[aa];
      for (int ion = beg_ion; ion < end_ion; ++ion, ++ai) {
        bool oia_ion = oia_atom;             // Default to output_{atom}_absorption
        load("output_" + ion_names[ion] + "_absorption", oia_ion);
        if (oia_ion) active_ion_weights.push_back(ai);
      }
    }
    for (int aa = 0; aa < n_active_atoms; ++aa) { // Loop for sum statistics
      bool oia_sum = false;                  // Default to false
      load("output_" + atom_symbols[active_atoms[aa]] + "_absorption", oia_sum);
      if (oia_sum) active_ion_weights.push_back(n_active_ions + aa);
    }
    n_active_ion_weights = active_ion_weights.size(); // Number of active ionization statistics
  }
  load("output_abundances", output_abundances); // Output abundances (x_HI etc)
  load("output_photoionization", output_photoionization); // Output photoionization rates [1/s]
  load("output_photoheating", output_photoheating); // Output photoheating rates [erg/s]
  load("output_photon_density", output_photon_density); // Output photon density [photons/cm^3]
#if VORONOI
  load("output_refinement", output_refinement); // Output a refined version of the grid
  if (output_refinement) {
    if constexpr (!HAVE_CGAL)
      root_error("Voronoi mesh connectivity is missing: output_refinement requires HAVE_CGAL");
    load("refinement_rtol", refinement_rtol, ">0"); // Relative photon rate tolerence
    load("refinement_x_HI", refinement_x_HI, "[0,1]"); // Neutral hydrogen fraction threshold
    load("refinement_Rmin", refinement_Rmin, ">0"); // Effective resolution threshold (R = Γ_HI / α_B n_H)
    load("refinement_mdeg", refinement_mdeg, "[0,30]"); // Merging opening angle [degrees]
    load("refinement_pert", refinement_pert, "[0,0.01]"); // Perturbation factor for refinement
  }
#endif
  // General ionization statistics output flags
  active_ion_stats.reserve(n_ion_stats);     // Reserve memory for active statistics
  bool ois = false, ois_tot = false, ois_gas = false, ois_dust = false;
  load("output_ion_stats", ois);             // Check compile/runtime consistency
  // Consider all active atoms and ions
  for (int aa = 0, ai = 0; aa < n_active_atoms; ++aa) {
    bool ois_atom = ois;                     // Default to output_ion_stats
    load("output_ion_stats_" + atom_names[active_atoms[aa]], ois_atom);
    const int beg_ion = beg_ions[aa], end_ion = end_ions[aa];
    for (int ion = beg_ion; ion < end_ion; ++ion, ++ai) {
      bool ois_ion = ois_atom;               // Default to output_ion_stats_{atom}
      load("output_ion_stats_" + ion_names[ion], ois_ion);
      if (ois_ion) active_ion_stats.push_back(ai);
    }
  }
  for (int aa = 0; aa < n_active_atoms; ++aa) { // Loop for sum statistics
    bool ois_sum = false;                    // Default to false
    load("output_ion_stats_" + atom_symbols[active_atoms[aa]], ois_sum);
    if (ois_sum) active_ion_stats.push_back(n_active_ions + aa);
  }
  load("output_ion_stats_tot", ois_tot);     // Total (gas + dust)
  if (ois_tot) active_ion_stats.push_back(STATS_tot);
  load("output_ion_stats_gas", ois_gas);     // Total (without dust)
  if (ois_gas) active_ion_stats.push_back(STATS_gas);
  load("output_ion_stats_dust", ois_dust);   // Dust
  if (ois_dust) active_ion_stats.push_back(STATS_dust);
  // Check whether any flags were set to true
  n_active_ion_stats = active_ion_stats.size(); // Number of active ionization statistics
  // Check runtime consistency against compile-time parameters
  runtime("output_radial_flow", output_radial_flow); // Radial flow [photons/s]
  runtime("output_group_flows", output_group_flows); // Group flows [photons/s]
  runtime("output_subhalo_flows", output_subhalo_flows); // Subhalo flows [photons/s]
  runtime("output_groups", output_groups);   // Group properties
  runtime("output_subhalos", output_subhalos); // Subhalo properties
  if (n_active_ion_stats > 0) {
    // Check runtime consistency
    runtime("output_ion_cell_age_freq", output_ion_cell_age_freq); // Number of absorptions by cell, age, and frequency [photons/s]
    runtime("output_ion_radial_age_freq", output_ion_radial_age_freq); // Number of absorptions by radial, age, and frequency [photons/s]
    runtime("output_ion_distance_age_freq", output_ion_distance_age_freq); // Number of absorptions by distance, age, and frequency [photons/s]
    if (!output_ion_cell_age_freq && !output_ion_radial_age_freq && !output_ion_distance_age_freq && !output_radial_flow && !output_group_flows && !output_subhalo_flows)
      root_error("Requested output_ion_stats but no output options: [output_ion_cell_age_freq, output_ion_radial_age_freq, output_ion_distance_age_freq, output_radial_flow, output_group_flows, output_subhalo_flows]");
  }
  if constexpr (output_ion_cell_age_freq) {
    YAML::Node subfile; load("cell_age_freq", subfile); // Load subfile
    load_age_edges(subfile, ion_cell_age_freq); // Age bin information
    load_freq_edges(subfile, ion_cell_age_freq); // Frequency bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized cell_age_freq options:\n" << subfile << endl;
      root_error("Unrecognized cell_age_freq options!");
    }
  }
  if constexpr (output_ion_radial_age_freq) {
    YAML::Node subfile; load("radial_age_freq", subfile); // Load subfile
    load_radial_edges(subfile, ion_radial_age_freq); // Radial bin information
    load_age_edges(subfile, ion_radial_age_freq); // Age bin information
    load_freq_edges(subfile, ion_radial_age_freq); // Frequency bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized radial_age_freq options:\n" << subfile << endl;
      root_error("Unrecognized radial_age_freq options!");
    }
  }
  if constexpr (output_ion_distance_age_freq) {
    YAML::Node subfile; load("distance_age_freq", subfile); // Load subfile
    load_distance_edges(subfile, ion_distance_age_freq); // Distance bin information
    load_age_edges(subfile, ion_distance_age_freq); // Age bin information
    load_freq_edges(subfile, ion_distance_age_freq); // Frequency bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized distance_age_freq options:\n" << subfile << endl;
      root_error("Unrecognized distance_age_freq options!");
    }
  }
  if constexpr (output_radial_flow) {
    YAML::Node subfile; load("radial_flow", subfile); // Load subfile
    load_radial_edges(subfile, radial_flow); // Radial bin information
    load_freq_edges(subfile, radial_flow); // Frequency bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized radial_flow options:\n" << subfile << endl;
      root_error("Unrecognized radial_flow options!");
    }
  }
  if constexpr (output_group_flows) {
    group_flows = vector<RadialFlow>(n_groups); // Group flows
    YAML::Node subfile; load("group_flows", subfile); // Load subfile
    load_radial_edges(subfile, group_flows, true); // Group bin information
    load_freq_edges(subfile, group_flows); // Frequency bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized group_flows options:\n" << subfile << endl;
      root_error("Unrecognized group_flows options!");
    }
  }
  if constexpr (output_subhalo_flows) {
    subhalo_flows = vector<RadialFlow>(n_subhalos); // Subhalo flows
    YAML::Node subfile; load("subhalo_flows", subfile); // Load subfile
    load_radial_edges(subfile, subhalo_flows, false); // Subhalo bin information
    load_freq_edges(subfile, subhalo_flows); // Frequency bin information
    if (subfile.size() > 0) {
      cout << "\nUnrecognized subhalo_flows options:\n" << subfile << endl;
      root_error("Unrecognized subhalo_flows options!");
    }
  }

  // Information about the simulation
  load("n_photons", n_photons, ">0");        // Number of photon packets
  if (file["n_photons_per_star"]) {
    load("n_photons_per_star", n_photons_per_star, ">0"); // Number of photon packets
    load("n_photons_max", n_photons_max);    // Maximum number of photons
    if (n_photons_max < n_photons)
      root_error("n_photons_max must be >= n_photons"); // Validate range
  }
  load("max_iter", max_iter, "[1,100]");     // Maximum number of iterations
  load("max_error", max_error, "[0,1]");     // Relative error for convergence

  // Angle-averaged parameters
  load("output_radial_avg", output_radial_avg); // Output angle-averaged radial surface brightness
  load("output_bin_radial_avg", output_radial_cube_avg); // Output angle-averaged bin radial surface brightness

  // Information about the cameras
  camera_config(file);                       // General camera setup
  if (have_cameras) {
    if (n_bins == 1) output_bin_escape_fractions = false; // Reset default
    load("output_escape_fractions", output_escape_fractions); // Output camera escape fractions
    load("output_bin_escape_fractions", output_bin_escape_fractions); // Output bin escape fractions
    load("output_images", output_images);    // Output surface brightness images
    load("output_bin_images", output_cubes); // Output bin SB images
    load("output_radial_images", output_radial_images); // Output radial surface brightness images
    load("output_bin_radial_images", output_radial_cubes); // Output bin radial SB images
    load("output_mcrt_emission", output_mcrt_emission); // Output intrinsic emission without transport (mcrt)
    load("output_mcrt_attenuation", output_mcrt_attenuation); // Output attenuated emission without scattering (mcrt)
    // Check camera output compatibility
    if (output_mcrt_emission || output_mcrt_attenuation) {
      if (output_images || output_cubes || output_radial_images || output_radial_cubes)
        root_error("output_mcrt_emission and output_mcrt_attenuation require output_images, output_bin_images, output_radial_images, or output_bin_radial_images");
    }
    if (!(output_escape_fractions || output_bin_escape_fractions || output_images || output_cubes || output_radial_images || output_radial_cubes))
      root_error("Cameras were requested without any camera output types.");
    have_radial_cameras = (output_radial_images || output_radial_cubes);
    after_camera_config(file);               // After camera setup
  } else {
    output_escape_fractions = output_images = false; // Reset defaults
  }

  // Healpix map configuration
  load("output_map2", output_map2);          // Output statistical moment map
  load_if_false(output_map2, "output_map", output_map);
  load("output_radial_map", output_radial_map); // Output radial surface brightness map
  load("output_bin_radial_map", output_cube_map); // Output bin surface brightness map

  // Specific healpix map parameters
  if (output_map)
    map_config(file);                        // Healpix map configuration
  if (output_radial_map)
    radial_map_config(file);                 // Radial map configuration
  if (output_cube_map)
    cube_map_config(file);                   // Bin map configuration

  // Group healpix map configuration and parameters
  if constexpr (output_groups) {
    load("output_map2_groups", output_map2_grp); // Output statistical moment group maps
    load_if_false(output_map2_grp, "output_map_groups", output_map_grp);
    load("output_radial_map_groups", output_radial_map_grp); // Output radial surface brightness group maps
    load("output_radial_avg_groups", output_radial_avg_grp); // Output angle-averaged radial surface brightness
    if (!output_grp_obs) {
      if (output_radial_map_grp)
        root_error("output_radial_map_groups requires output_groups_obs");
      if (output_radial_avg_grp)
        root_error("output_radial_avg_groups requires output_groups_obs");
    }

    // Specific healpix map parameters
    if (output_map_grp)
      group_map_config(file);                // Healpix map configuration
    if (output_radial_map_grp)
      group_radial_map_config(file);         // Radial map configuration
    if (output_radial_avg_grp)
      group_radial_avg_config(file);         // Radial average configuration
  }

  // Subhalo healpix map configuration and parameters
  if constexpr (output_subhalos) {
    load("output_map2_subhalos", output_map2_sub); // Output statistical moment subhalo maps
    load_if_false(output_map2_sub, "output_map_subhalos", output_map_sub);
    load("output_radial_map_subhalos", output_radial_map_sub); // Output radial surface brightness subhalo maps
    load("output_radial_avg_subhalos", output_radial_avg_sub); // Output angle-averaged radial surface brightness
    if (!output_sub_obs) {
      if (output_radial_map_sub)
        root_error("output_radial_map_subhalos requires output_subhalos_obs");
      if (output_radial_avg_sub)
        root_error("output_radial_avg_subhalos requires output_subhalos_obs");
    }

    // Specific healpix map parameters
    if (output_map_sub)
      subhalo_map_config(file);              // Healpix map configuration
    if (output_radial_map_sub)
      subhalo_radial_map_config(file);       // Radial map configuration
    if (output_radial_avg_sub)
      subhalo_radial_avg_config(file);       // Radial average configuration
  }
}
