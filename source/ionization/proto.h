/**********************
 * ionization/proto.h *
 **********************

 * Module function declarations.

*/

#ifndef IONIZATION_PROTO_INCLUDED
#define IONIZATION_PROTO_INCLUDED

// Inherited headers
#include "../proto.h"

// Information about the source
extern int n_absorbed;                       // Number of absorbed photons
extern int n_escaped;                        // Number of escaped photons
extern int n_finished;                       // Track finished progress
extern int HI_bin;                           // HI bin index
extern string source_model;                  // Spectral source model
extern string AGN_model;                     // AGN spectral source model
extern double f_esc_stars;                   // Escape fraction from stellar birth clouds
extern bool free_free;                       // Free-free continuum emission
extern bool free_bound;                      // Free-bound continuum emission
extern bool two_photon;                      // Two-photon continuum emission
extern double L_AGN;                         // AGN luminosity [erg/s]
extern double Ndot_AGN;                      // AGN rate [photons/s]

// Spatial emission from stars, cells, AGN, point, plane
extern bool avoid_SFR;                       // Ignore cells with SFR > 0
extern double j_exp;                         // Luminosity boosting exponent (j_exp < 1 favors lower emissivity cells)
extern double V_exp;                         // Volume boosting exponent (V_exp < 1 favors smaller cells)
extern double nu_exp;                        // Frequency boosting exponent (nu_exp > 0 favors higher frequency photons)
extern double min_HI_bin_cdf;                // Minimum CDF value for >= 13.6 eV bins (step function correction)
constexpr int n_cdf_cells = 10000;           // Number of entrees in the cdf lookup table (cells)
extern array<int, n_cdf_cells> j_map_cells;  // Lookup table for photon insertion (cells)
extern vector<double> j_cdf_cells;           // Cumulative distribution function for emission
extern vector<double> j_weights_cells;       // Initial photon weights for each cell
extern vector<double> Ndot_int_cells;        // Intrinsic photon rates for each cell
extern vector<double> Ndot_UVB_HI_cells;     // UV background ionizations for each cell
extern vectors Ndot_rec_ions_cells;          // Recombinations for each cell
extern vectors Ndot_col_ions_cells;          // Collisional ionizations for each cell
extern vectors Ndot_phot_ions_cells;         // Photoionizations for each cell
extern vectors Ndot_cxi_H_ions_cells;        // Charge exchange ionizations (H) for each cell
extern vectors Ndot_cxi_He_ions_cells;       // Charge exchange ionizations (He) for each cell
extern vectors Ndot_cxr_H_ions_cells;        // Charge exchange recombinations (H) for each cell
extern vectors Ndot_cxr_He_ions_cells;       // Charge exchange recombinations (He) for each cell
extern vector<double> source_weight_cells;   // Photon weight at emission for each cell
extern vector<double> weight_cells;          // Photon weight at escape for each cell
constexpr int n_cdf_stars = 10000;           // Number of entrees in the cdf lookup table (stars)
extern array<int, n_cdf_stars> j_map_stars;  // Lookup table for photon insertion (stars)
extern vector<double> j_cdf_stars;           // Cumulative distribution function for emission
extern vector<double> j_weights_stars;       // Initial photon weights for each star
extern vector<double> Ndot_int_stars;        // Intrinsic photon rates for each star
extern vector<double> source_weight_stars;   // Photon weight at emission for each star
extern vector<double> weight_stars;          // Photon weight at escape for each star
extern vector<double> weight_stars_grp_vir;  // Photon weight at escape for each star (group virial)
extern vector<double> weight_stars_grp_gal;  // Photon weight at escape for each star (group galaxy)
extern vector<double> weight_stars_sub_vir;  // Photon weight at escape for each star (subhalo virial)
extern vector<double> weight_stars_sub_gal;  // Photon weight at escape for each star (subhalo galaxy)
extern int i_AGN;                            // Cell index of the AGN source

// Point source variables
extern bool point_source;                    // Insert photons at a specified point (default)
extern double T_point;                       // Point source temperature [K]
extern double Lbol_point;                    // Point source luminosity [erg/s]
extern int i_point;                          // Cell index to insert photons
extern double x_point;                       // Point source position [cm]
extern double y_point;
extern double z_point;
extern Vec3 r_point;                         // (x, y, z) [cm]

// Plane source variables
extern bool plane_source;                    // Insert photons at a specified plane
extern string plane_direction;               // Plane direction name
extern int plane_type;                       // Plane direction type
extern double T_plane;                       // Plane source temperature [K]
extern double Sbol_plane;                    // Plane bolometric surface density [erg/s/cm^2]
extern double Lbol_plane;                    // Plane source luminosity [erg/s]
extern double single_Z;                      // Single SED metallicity [mass fraction]
extern double single_age;                    // Single SED stellar age [Gyr]
extern double plane_position;                // Plane position [cm]
extern double plane_center_1, plane_center_2; // Plane center positions
extern double plane_radius_1, plane_radius_2; // Plane box/beam radii
extern double plane_center_x_bbox, plane_center_y_bbox, plane_center_z_bbox; // Center [bbox]
extern double plane_radius_x_bbox, plane_radius_y_bbox, plane_radius_z_bbox; // Radius [bbox]
extern double plane_area;                    // Plane surface area [cm^2]
extern bool plane_beam;                      // Use an ellipsoidal beam instead of a rectangle
enum PlaneDirectionTypes { PosX = 0, NegX, PosY, NegY, PosZ, NegZ };

// Distinguish between source types
enum IonSourceTypes {
  STARS = 0,                                 // Star-based emission
  CELLS,                                     // Cell-based emission (free_free)
  AGN,                                       // AGN emission
  POINT,                                     // Point source emission
  PLANE,                                     // Plane source emission
  MAX_ION_SOURCE_TYPES                       // Max number of source types
};
extern int n_ion_source_types;               // Number of source types used
extern array<double, MAX_ION_SOURCE_TYPES> mixed_ion_pdf; // Mixed source probability distribution function
extern array<double, MAX_ION_SOURCE_TYPES> mixed_ion_cdf; // Mixed source cumulative distribution function
extern array<double, MAX_ION_SOURCE_TYPES> mixed_ion_weights; // Mixed source weights (stars, cells, AGN, point, plane)
extern vector<int> ion_source_types_mask;    // Source type mask indices
extern strings ion_source_names_mask;        // Source type names (masked)
extern vector<double> mixed_ion_pdf_mask;    // Mixed source pdf (masked)
extern vector<double> mixed_ion_cdf_mask;    // Mixed source cdf (masked)
extern vector<double> mixed_ion_weights_mask; // Mixed source weights (masked)

// Summary ionization spectral properties
struct IonData {
  double L = 0.;                             // Luminosity [erg/s]
  double Ndot = 0.;                          // Photon rate [photons/s]
  // double sigma_Ndot = 0.;                    // Rate cross-section [cm^2 photons/s]
  // double sigma = 0.;                         // Ionization cross-section [cm^2]
  // double sigma_L = 0.;                       // Heating cross-section [cm^2 erg/s]
  // double sigma_epsilon = 0.;                 // Excess heating cross-section [cm^2 erg/s]
  // double epsilon = 0.;                       // Ionization heating [erg]
  // double mean_energy = 0.;                   // Mean energy [erg]
  // double kappa_Ndot = 0.;                    // Dust opacity rate [cm^2/g dust photons/s]
  // double albedo_Ndot = 0.;                   // Dust albedo rate for scattering vs. absorption [photons/s]
  // double cosine_Ndot = 0.;                   // Anisotropy parameter rate: <μ> for dust scattering [photons/s]
  // double kappa = 0.;                         // Dust opacity [cm^2/g dust]
  // double albedo = 0.;                        // Dust albedo for scattering vs. absorption
  // double cosine = 0.;                        // Anisotropy parameter: <μ> for dust scattering
};

// Ionization bin spectral properties
struct IonBinData {
  double L = 0.;                             // Luminosity [erg/s]
  double Ndot = 0.;                          // Photon rate [photons/s]
  double boosted_Ndot = 0.;                  // Frequency boosted rate [photons/s]
  vector<double> bin_L;                      // Total bin luminosities [erg/s]
  vector<double> bin_Ndot;                   // Total bin rates [photons/s]
  vector<double> bin_cdf;                    // Bin cumulative distribution function
  vector<double> bin_weight;                 // Bin weights (for frequency boosting)
  vector<double> bin_boost;                  // Bin frequency boost factors
  vectors sigma_Ndot_ions;                   // Rate cross-sections [cm^2 photons/s]
  vectors sigma_ions;                        // Ionization cross-sections [cm^2]
  vectors sigma_L_ions;                      // Heating cross-sections [cm^2 erg/s]
  vectors sigma_epsilon_ions;                // Excess heating cross-sections [cm^2 erg/s]
  vectors epsilon_ions;                      // Ionization heatings [erg]
  vector<double> mean_energy;                // Mean energy of each frequency bin [erg]
  dust_vectors kappas_Ndot;                  // Dust opacity rates [cm^2/g dust photons/s]
  dust_vectors albedos_Ndot;                 // Dust albedo rates for scattering vs. absorption [photons/s]
  dust_vectors cosines_Ndot;                 // Anisotropy parameter rates: <μ> for dust scattering [photons/s]
  dust_vectors kappas;                       // Dust opacities [cm^2/g dust]
  dust_vectors albedos;                      // Dust albedos for scattering vs. absorption
  dust_vectors cosines;                      // Anisotropy parameters: <μ> for dust scattering
  double constant_kappas[n_dust_species];    // Constant dust opacities [cm^2/g dust]
  double constant_albedos[n_dust_species];   // Constant dust albedos for scattering vs. absorption
  double constant_cosines[n_dust_species];   // Constant anisotropy parameters: <μ> for dust scattering
  IonData HI;                                // Summary statistics (> 13.6 eV)
};

extern IonBinData ion_global;                // Global spectral properties
extern IonBinData ion_stars;                 // Stellar spectral properties
extern IonBinData ion_gas;                   // Gas spectral properties
extern IonBinData ion_point;                 // Point spectral properties
extern IonBinData ion_plane;                 // Plane spectral properties
extern vector<double>& bin_L_tot;            // Total bin luminosities [erg/s]
extern vector<double>& bin_Ndot_tot;         // Total bin rates [photons/s]
extern vector<double>& bin_L_stars;          // Stellar bin luminosities [erg/s]
extern vector<double>& bin_Ndot_stars;       // Stellar bin rates [photons/s]
extern vector<double>& bin_L_gas;            // Gas bin luminosities [erg/s]
extern vector<double>& bin_Ndot_gas;         // Gas bin rates [photons/s]
extern vector<double> bin_Ndot2_stars;       // Stellar bin rates^2 [photons^2/s^2]
extern vector<double> bin_n_stars_eff;       // Bin effective number of stars: 1/<w>
extern vector<double> bin_Ndot2_gas;         // Gas bin rates^2 [photons^2/s^2]
extern vector<double> bin_n_cells_eff;       // Bin effective number of cells: 1/<w>
extern double n_stars_eff_HI;                // Effective number of stars (> 13.6 eV): 1/<w>
extern double n_cells_eff_HI;                // Effective number of cells (> 13.6 eV): 1/<w>

// AGN spectral properties
extern vector<double> bin_L_AGN;             // Total bin luminosities [erg/s]
extern vector<double> bin_Ndot_AGN;          // Total bin rates [photons/s]
extern vector<double> bin_cdf_AGN;           // Bin cumulative distribution function
extern vector<double> bin_weight_AGN;        // Bin weights (for frequency boosting)
extern vector<double> sigma_Ndot_HI_AGN;     // HI rate cross-section [cm^2 photons/s]
extern vector<double> sigma_Ndot_HeI_AGN;    // HeI rate cross-section [cm^2 photons/s]
extern vector<double> sigma_Ndot_HeII_AGN;   // HeII rate cross-section [cm^2 photons/s]
extern vector<double> sigma_HI_AGN;          // HI ionization cross-section [cm^2]
extern vector<double> sigma_HeI_AGN;         // HeI ionization cross-section [cm^2]
extern vector<double> sigma_HeII_AGN;        // HeII ionization cross-section [cm^2]
extern vector<double> sigma_L_HI_AGN;        // HI heating cross-section [cm^2 erg/s]
extern vector<double> sigma_L_HeI_AGN;       // HeI heating cross-section [cm^2 erg/s]
extern vector<double> sigma_L_HeII_AGN;      // HeII heating cross-section [cm^2 erg/s]
extern vector<double> epsilon_HI_AGN;        // HI ionization heating [erg]
extern vector<double> epsilon_HeI_AGN;       // HeI ionization heating [erg]
extern vector<double> epsilon_HeII_AGN;      // HeII ionization heating [erg]
extern vector<double> mean_energy_AGN;       // Mean energy of each frequency bin [erg]
extern vector<double> kappa_Ndot_AGN;        // Dust opacity rate [cm^2/g dust photons/s]
extern vector<double> albedo_Ndot_AGN;       // Dust albedo rate for scattering vs. absorption [photons/s]
extern vector<double> cosine_Ndot_AGN;       // Anisotropy parameter rate: <μ> for dust scattering [photons/s]
extern vector<double> kappa_AGN;             // Dust opacity [cm^2/g dust]
extern vector<double> albedo_AGN;            // Dust albedo for scattering vs. absorption
extern vector<double> cosine_AGN;            // Anisotropy parameter: <μ> for dust scattering

// Information about local ionization coupling
extern Image bin_n_gamma;                    // Photon density [photons/cm^3]
extern vectors rate_ions;                    // Species photoionization integrals [1/s]

// Information about escaped photons
extern double f_src, f_src_HI;               // Global source fraction: sum(w0)
extern double f2_src, f2_src_HI;             // Global squared f_src: sum(w0^2)
extern double n_photons_src, n_photons_src_HI; // Effective number of emitted photons: 1/<w0>
extern double f_esc, f_esc_HI;               // Global escape fraction: sum(w)
extern double f2_esc, f2_esc_HI;             // Global squared f_esc: sum(w^2)
extern double n_photons_esc, n_photons_esc_HI; // Effective number of escaped photons: 1/<w>
extern double f_abs, f_abs_HI;               // Global dust absorption fraction
extern double f2_abs, f2_abs_HI;             // Global squared f_abs: sum(w^2)
extern double n_photons_abs, n_photons_abs_HI; // Effective number of absorbed photons: 1/<w>
extern vector<double> f_ions;                // Global species absorption fractions
extern double f2_HI, n_photons_HI;           // Global species absorption fractions
extern double l_abs;                         // Global absorption distance [cm]
extern vector<double> bin_f_src;             // Global bin source fraction: sum(w0)
extern vector<double> bin_f2_src;            // Global bin squared f_src: sum(w0^2)
extern vector<double> bin_n_photons_src;     // Effective number of emitted photons: 1/<w0>
extern vector<double> bin_f_esc;             // Global bin escape fraction: sum(w)
extern vector<double> bin_f2_esc;            // Global bin squared f_esc: sum(w^2)
extern vector<double> bin_n_photons_esc;     // Effective number of escaped photons: 1/<w>
extern vector<double> bin_f_abs;             // Global bin dust absorption fraction
extern vector<double> bin_f2_abs;            // Global bin squared f_abs: sum(w^2)
extern vector<double> bin_n_photons_abs;     // Effective number of absorbed photons: 1/<w>
extern vectors bin_f_ions;                   // Global bin species absorption fractions
extern vector<double> bin_f2_HI;             // Global bin squared absorption fractions
extern vector<double> bin_n_photons_HI;      // Effective number of photons: 1/<w>
extern vector<double> bin_l_abs;             // Global bin absorption distance [cm]

// Information about output statistics
extern bool output_photons;                  // Output photon packets (esc/abs)
extern bool output_n_scat;                   // Output number of scattering events
extern array<bool, n_ions_atoms> output_ion_absorptions; // Output weight removed by species absorption
extern vectors ion_weights;                  // Weight removed by species absorption
extern int n_active_ion_weights;             // Number of active ionization weight statistics
extern vector<int> active_ion_weights;       // Active ionization weight statistics indices
extern bool output_absorption_distance;      // Output photon absorption distance [cm]
extern bool output_source_position;          // Output position at emission [cm]
extern bool output_stars;                    // Output star emission and escape
extern bool output_cells;                    // Output cell emission and escape
extern int n_rec_ions_cells, n_col_ions_cells, n_phot_ions_cells; // Number of active stats
extern int n_cxi_H_ions_cells, n_cxi_He_ions_cells, n_cxr_H_ions_cells, n_cxr_He_ions_cells;
extern bool output_cells_UVB_HI;             // Output cell UV background ionizations
extern vector<bool> output_cells_rec_ions;   // Output cell recombinations
extern vector<bool> output_cells_col_ions;   // Output cell collisional ionizations
extern vector<bool> output_cells_phot_ions;  // Output cell photoionizations
extern vector<bool> output_cells_cxi_H_ions; // Output cell charge ionizations (H)
extern vector<bool> output_cells_cxi_He_ions; // Output cell charge ionizations (He)
extern vector<bool> output_cells_cxr_H_ions; // Output cell charge recombinations (H)
extern vector<bool> output_cells_cxr_He_ions; // Output cell charge recombinations (He)
extern string photon_file;                   // Output a separate photon file
extern bool output_abundances;               // Output abundances (x_HI etc)
extern bool output_photoionization;          // Output photoionization rates [1/s]
extern bool output_photoheating;             // Output photoheating rates [erg/s]
extern bool output_photon_density;           // Output photon density [photons/cm^3]
extern bool output_refinement;               // Output a refined version of the grid
extern double refinement_rtol;               // Relative photon rate tolerence
extern double refinement_x_HI;               // Neutral hydrogen fraction threshold
extern double refinement_Rmin;               // Effective resolution threshold (R = Γ_HI / α_B n_H)
extern double refinement_mdeg;               // Merging opening angle [degrees]
extern double refinement_pert;               // Perturbation factor for refinement
extern double Ndot_HI_threshold;             // Refinement threshold HI rate [photons/s]
#define have_multiple_grids (output_ion_radial_age_freq || output_ion_distance_age_freq || output_radial_flow || output_group_flows || output_subhalo_flows)
extern vector<int> source_ids;               // Emission cell or star index
extern vector<int> source_types;             // Emission type specifier
extern vector<int> n_scats;                  // Number of scattering events
extern vector<double> source_weights;        // Photon weight at emission
extern vector<int> freq_bins;                // Frequency bin at escape [bin]
extern vector<double> weights;               // Photon weight at escape
extern vector<double> dust_weights;          // Weight removed by dust absorption
extern vector<double> dist_abss;             // Photon absorption distance [cm]
extern vector<Vec3> source_positions;        // Position at emission [cm]
extern vector<Vec3> positions;               // Position at escape [cm]
extern vector<Vec3> directions;              // Direction at escape

// Information about secondary cameras (mcrt)
extern bool output_mcrt_emission;            // Output intrinsic emission without transport
extern bool output_mcrt_attenuation;         // Output attenuated emission without scattering
extern vector<double> f_escs_ext;            // Attenuated escape fractions [fraction]
extern bool output_bin_escape_fractions;     // Output bin escape fractions
extern vectors bin_f_escs;                   // Bin escape fractions [fraction]
extern vectors bin_f_escs_ext;               // Attenuated bin escape fractions [fraction]
extern Images images_int;                    // Intrinsic surface brightness images [photons/s/cm^2]
extern Images images_ext;                    // Attenuated surface brightness images [photons/s/cm^2]
extern vectors radial_images_int;            // Intrinsic radial surface brightness images [photons/s/cm^2]
extern vectors radial_images_ext;            // Attenuated radial surface brightness images [photons/s/cm^2]
extern Cubes bin_images;                     // Escaped bin SB images [photons/s/cm^2]
extern Cubes bin_images_int;                 // Intrinsic bin SB images [photons/s/cm^2]
extern Cubes bin_images_ext;                 // Attenuated bin SB images [photons/s/cm^2]
extern Images bin_radial_images;             // Escaped bin radial SB images [photons/s/cm^2]
extern Images bin_radial_images_int;         // Intrinsic bin radial SB images [photons/s/cm^2]
extern Images bin_radial_images_ext;         // Attenuated bin radial SB images [photons/s/cm^2]

// Information about ionization statistics
#include "stats.h"                           // Stats definitions

#endif // IONIZATION_PROTO_INCLUDED
