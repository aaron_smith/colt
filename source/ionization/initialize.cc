/****************************
 * ionization/initialize.cc *
 ****************************

 * Initialization: Simulation setup, allocation, opening messages, etc.

*/

#include "proto.h"
#include "Ionization.h"
#include "../reduction.h" // Reduction operations

#define EVAL_ZA(tab) mass * pow(10., (spec_Z_age.log_##tab(i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                                   + (spec_Z_age.log_##tab(i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.log_##tab(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)
#define COS_NDOT_ZA mass * ((spec_Z_age.cosines_Ndot[j](i_L_Z,i_L_age,i_bin)*frac_L_Z + spec_Z_age.cosines_Ndot[j](i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age \
                          + (spec_Z_age.cosines_Ndot[j](i_L_Z,i_R_age,i_bin)*frac_L_Z + spec_Z_age.cosines_Ndot[j](i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age)
#define LIN_NDOT_ZA mass * ((pow(10., spec_Z_age.log_Ndot(i_L_Z,i_L_age,i_bin))*frac_L_Z + pow(10., spec_Z_age.log_Ndot(i_R_Z,i_L_age,i_bin))*frac_R_Z) * frac_L_age \
                          + (pow(10., spec_Z_age.log_Ndot(i_L_Z,i_R_age,i_bin))*frac_L_Z + pow(10., spec_Z_age.log_Ndot(i_R_Z,i_R_age,i_bin))*frac_R_Z) * frac_R_age)

#define EVAL_FF_Z1(tab) chop(n2V_Z1 * pow(10., spec_ff_Z1.log_##tab[i_bin][i_L_T]*frac_L_T + spec_ff_Z1.log_##tab[i_bin][i_R_T]*frac_R_T))
#define EVAL_FF_Z2(tab) chop(n2V_Z2 * pow(10., spec_ff_Z2.log_##tab[i_bin][i_L_T]*frac_L_T + spec_ff_Z2.log_##tab[i_bin][i_R_T]*frac_R_T))
#define COS_NDOT_FF(sub) n2V_##sub * (spec_ff_##sub.cosines_Ndot[j][i_bin][i_L_T]*frac_L_T + spec_ff_##sub.cosines_Ndot[j][i_bin][i_R_T]*frac_R_T)
#define LIN_NDOT_FF(sub) n2V_##sub * (pow(10., spec_ff_##sub.log_Ndot[i_bin][i_L_T])*frac_L_T + pow(10., spec_ff_##sub.log_Ndot[i_bin][i_R_T])*frac_R_T)

#define EVAL_FB_HI(tab) chop(n2V_HII * pow(10., spec_fb_HI.log_##tab[i_bin][i_L_T]*frac_L_T + spec_fb_HI.log_##tab[i_bin][i_R_T]*frac_R_T))
#define EVAL_FB_HeI(tab) chop(n2V_HeII * pow(10., spec_fb_HeI.log_##tab[i_bin][i_L_T]*frac_L_T + spec_fb_HeI.log_##tab[i_bin][i_R_T]*frac_R_T))
#define EVAL_FB_HeII(tab) chop(n2V_HeIII * pow(10., spec_fb_HeII.log_##tab[i_bin][i_L_T]*frac_L_T + spec_fb_HeII.log_##tab[i_bin][i_R_T]*frac_R_T))
#define COS_NDOT_FB(sub) n2V_##sub##I * (spec_fb_##sub.cosines_Ndot[j][i_bin][i_L_T]*frac_L_T + spec_fb_##sub.cosines_Ndot[j][i_bin][i_R_T]*frac_R_T)
#define LIN_NDOT_FB(sub) n2V_##sub##I * (pow(10., spec_fb_##sub.log_Ndot[i_bin][i_L_T])*frac_L_T + pow(10., spec_fb_##sub.log_Ndot[i_bin][i_R_T])*frac_R_T)

#define EVAL_2P_HI(tab) chop(HI_2p_n2V * spec_2p_HI.tab[i_bin])
// #define EVAL_2P_HeI(tab) chop(HeI_2p_n2V * spec_2p_HeI.tab[i_bin])
#define EVAL_2P_HeII(tab) chop(HeII_2p_n2V * spec_2p_HeII.tab[i_bin])

vector<Vec3> healpix_vectors(const int nside); // Healpix directions
bool find_sorted(const int val, const vector<int> vec); // Check if a value is in a sorted vector
int get_index_sorted(const int val, const vector<int> vec); // Get index of a value in a sorted vector
bool avoid_cell(const int cell); // Avoid calculations for certain cells
bool avoid_cell_strict(const int cell); // Avoid calculations without overrides
Vec3 cell_center(const int cell); // Center of the cell
int find_cell(const Vec3 point, int cell); // Cell index of a point
void assign_spectra(); // Assign luminosity and opacity functions
double HI_2p_rate(const double T, const double ne, const double nHII); // HI two-photon emission rate (without g_nu)
// double HeI_2p_rate(const double T, const double ne, const double nHII); // HeI two-photon emission rate (without g_nu)
double HeII_2p_rate(const double T, const double ne, const double nHII); // HeII two-photon emission rate (without g_nu)

// BPASS tables use 13 metallicities and 51 ages
static const double logZ_BP[13] = {-5., -4., -3., log10(2e-3), log10(3e-3), log10(4e-3), log10(6e-3), log10(8e-3), -2., log10(1.4e-2), log10(2e-2), log10(3e-2), log10(4e-2)};

/* Setup frequency boosting for a given IonBinData object. */
void setup_bin_cdf(IonBinData& ion) {
  ion.bin_cdf.resize(n_bins);                // Bin cumulative distribution function
  ion.bin_boost.resize(n_bins);              // Bin frequency boosting factor
  ion.bin_weight.resize(n_bins);             // Bin weights (for frequency boosting)
  if (nu_exp == 0.) {
    #pragma omp parallel for
    for (int i = 0; i < n_bins; ++i)
      ion.bin_boost[i] = 1.;                 // No frequency boost
  } else {
    const double e_0 = ion.mean_energy[0];   // Mean energy of first bin [erg]
    ion.bin_boost[0] = 1.;                   // Reference bin has no boost
    #pragma omp parallel for
    for (int i = 1; i < n_bins; ++i)
      ion.bin_boost[i] = pow(ion.mean_energy[i] / e_0, nu_exp); // Frequency boost
  }
  ion.bin_cdf[0] = ion.bin_Ndot[0];          // Copy unboosted rate
  for (int i = 1; i < n_bins; ++i)
    ion.bin_cdf[i] = ion.bin_cdf[i-1] + ion.bin_Ndot[i] * ion.bin_boost[i]; // Boosted pdf (unnormalized)
  if (HI_bin > 0) {                          // Enforce minimum fraction of cdf >13.6 eV
    double bin_HI_cdf = 1. - ion.bin_cdf[HI_bin-1] / ion.bin_cdf[n_bins-1];
    if (bin_HI_cdf < min_HI_bin_cdf) {
      const double boost_correction = min_HI_bin_cdf * (1. - bin_HI_cdf) / (bin_HI_cdf * (1. - min_HI_bin_cdf));
      for (int i = HI_bin; i < n_bins; ++i) {
        ion.bin_boost[i] *= boost_correction;
        ion.bin_cdf[i] = ion.bin_cdf[i-1] + ion.bin_Ndot[i] * ion.bin_boost[i]; // Boosted pdf (unnormalized)
      }
    }
  }
  ion.boosted_Ndot = ion.bin_cdf[n_bins-1];  // Total frequency boosted rate [photons/s]
  const double boosted_Ndot_norm = 1. / ion.boosted_Ndot; // Normalization (1 / boosted_Ndot)
  const double avg_boost = ion.boosted_Ndot / ion.Ndot; // Average frequency boost
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    ion.bin_cdf[i] *= boosted_Ndot_norm;     // Boosted cdf (normalized)
    ion.bin_weight[i] = avg_boost / ion.bin_boost[i]; // Boosted weight (normalized)
  }
  ion.bin_cdf[n_bins-1] = 1.;                // Ensure last value is 1
}

/* Return the ID of the host halo for a given cell or star. */
template <bool is_star>
static inline int get_halo_id(const int i) {
  if constexpr (is_star)
    return select_subhalo ? subhalo_id_star[i] : group_id_star[i];
  else
    return select_subhalo ? subhalo_id_cell[i] : group_id_cell[i];
}

/* Check whether a cell or star is not in a valid halo. */
template <bool is_star>
static inline bool avoid_host_halo(const int i) {
  if (halo_num < -2)
    return false;                            // No halo restrictions
  if (use_all_halos) {                       // All halos are valid
    if (filter_group_catalog) {              // Check the filtered group list
      if constexpr (is_star)
        return !find_sorted(group_id_star[i], group_id); // Avoid unfiltered groups (stars)
      else
        return !find_sorted(group_id_cell[i], group_id); // Avoid unfiltered groups (cells)
    }
    if (filter_subhalo_catalog) {            // Check the filtered subhalo list
      if constexpr (is_star)
        return !find_sorted(subhalo_id_star[i], subhalo_id); // Avoid unfiltered subhalos (stars)
      else
        return !find_sorted(subhalo_id_cell[i], subhalo_id); // Avoid unfiltered subhalos (cells)
    }
    return false;                            // No halo restrictions
  }
  const int id = get_halo_id<is_star>(i);    // Get the halo ID
  return halo_num != id;                     // Compare the halo IDs
}

/* Check whether a cell is not in a valid group. */
bool avoid_cell_host_halo(const int cell) {
  return avoid_host_halo<false>(cell);
}

/* Check whether a star is not in a valid group. */
bool avoid_star_host_halo(const int star) {
  return avoid_host_halo<true>(star);
}

// Avoid stars that are outside the emission region
static inline bool invalid_star(const int i) {
  return avoid_cell_strict(find_cell(r_star[i], 0)) ||
    #if spherical_escape
      (r_star[i] - escape_center).dot() >= emission_radius2 ||
    #endif
    #if box_escape
      (r_star[i].x < emission_box[0].x || r_star[i].x > emission_box[1].x ||
       r_star[i].y < emission_box[0].y || r_star[i].y > emission_box[1].y ||
       r_star[i].z < emission_box[0].z || r_star[i].z > emission_box[1].z) ||
    #endif
    avoid_star_host_halo(i);
};

// Avoid cells that are outside the emission region
static inline bool invalid_cell(const int i) {
  return avoid_cell_strict(i) ||
    #if spherical_escape
      (cell_center(i) - escape_center).dot() >= emission_radius2 ||
    #endif
    #if box_escape
      (r[i].x < emission_box[0].x || r[i].x > emission_box[1].x ||
       r[i].y < emission_box[0].y || r[i].y > emission_box[1].y ||
       r[i].z < emission_box[0].z || r[i].z > emission_box[1].z) ||
    #endif
    avoid_cell_host_halo(i);
};

// Invalidate a star particle due to filtering
static inline void invalidate_star(const int i) {
  j_cdf_stars[i] = 0.;                       // No emission (filtered out)

  if (output_stars) {
    weight_stars[i] = -2.;                   // Invalid weight (star)
    if (output_grp_vir)
      weight_stars_grp_vir[i] = -2.;         // Invalid weight (group virial)
    if (output_grp_gal)
      weight_stars_grp_gal[i] = -2.;         // Invalid weight (group galaxy)
    if (output_sub_vir)
      weight_stars_sub_vir[i] = -2.;         // Invalid weight (subhalo virial)
    if (output_sub_gal)
      weight_stars_sub_gal[i] = -2.;         // Invalid weight (subhalo galaxy)
  }
}

/* Set a vector to zeros. */
static inline void clear(vector<double>& v) {
  #pragma omp parallel for
  for (size_t i = 0; i < v.size(); ++i)
    v[i] = 0.;                               // Clear values
}

/* Group and subhalo statistics. */
template <bool is_gas>
static inline void add_Ndot_halo(vector<double>& Ndot_halo, vector<double>& Ndot2_halo, vector<double>& n_eff_halo,
                                 vector<double>& Ndot_HI_halo, vector<double>& Ndot2_HI_halo, vector<double>& n_eff_HI_halo,
                                 Image& bin_Ndot_halo, Image& bin_Ndot2_halo, Image& bin_n_eff_halo, Image& bin_Ndot_tot_halo) {
  const int n_halos = Ndot_halo.size();      // Number of halos
  if constexpr (is_gas) {                    // Reset values
    clear(Ndot_halo);
    clear(Ndot2_halo);
    clear(Ndot_HI_halo);
    clear(Ndot2_HI_halo);
  }
  #pragma omp parallel for
  for (int i_halo = 0; i_halo < n_halos; ++i_halo) {
    double Ndot_halo_i = 0., Ndot2_halo_i = 0.; // Halo Ndot and Ndot^2
    for (int i_bin = HI_bin; i_bin < n_bins; ++i_bin) {
      const double bin_Ndot = bin_Ndot_halo(i_halo, i_bin); // Halo bin Ndot
      const double bin_Ndot2 = bin_Ndot2_halo(i_halo, i_bin); // Halo bin Ndot^2
      Ndot_halo_i += bin_Ndot;               // Total rate [photons/s]
      Ndot2_halo_i += bin_Ndot2;             // Total rate^2 [photons^2/s^2]
      bin_n_eff_halo(i_halo, i_bin) = calc_n_eff(bin_Ndot, bin_Ndot2);
      if constexpr (is_gas)
        bin_Ndot_tot_halo(i_halo, i_bin) += bin_Ndot; // Global rate
    }
    Ndot_HI_halo[i_halo] = Ndot_halo_i;      // Total rate [photons/s]
    Ndot2_HI_halo[i_halo] = Ndot2_halo_i;    // Total rate^2 [photons^2/s^2]
    n_eff_HI_halo[i_halo] = calc_n_eff(Ndot_halo_i, Ndot2_halo_i); // Effective number of cells: <Ndot>^2/<Ndot^2>
    for (int i_bin = 0; i_bin < HI_bin; ++i_bin) {
      const double bin_Ndot = bin_Ndot_halo(i_halo, i_bin); // Halo bin Ndot
      const double bin_Ndot2 = bin_Ndot2_halo(i_halo, i_bin); // Halo bin Ndot^2
      Ndot_halo_i += bin_Ndot;               // Total rate [photons/s]
      Ndot2_halo_i += bin_Ndot2;             // Total rate^2 [photons^2/s^2]
      bin_n_eff_halo(i_halo, i_bin) = calc_n_eff(bin_Ndot, bin_Ndot2);
      if constexpr (is_gas)
        bin_Ndot_tot_halo(i_halo, i_bin) += bin_Ndot; // Global rate
    }
    Ndot_halo[i_halo] = Ndot_halo_i;         // Total rate [photons/s]
    Ndot2_halo[i_halo] = Ndot2_halo_i;       // Total rate^2 [photons^2/s^2]
    n_eff_halo[i_halo] = calc_n_eff(Ndot_halo_i, Ndot2_halo_i); // Effective number of cells: <Ndot>^2/<Ndot^2>
  }
}

/* Group and subhalo statistics for stars. */
static inline void add_Ndot_stars_halo(vector<double>& Ndot_halo, vector<double>& Ndot2_halo, vector<double>& n_eff_halo,
                                       vector<double>& Ndot_HI_halo, vector<double>& Ndot2_HI_halo, vector<double>& n_eff_HI_halo,
                                       Image& bin_Ndot_halo, Image& bin_Ndot2_halo, Image& bin_n_eff_halo, Image& bin_Ndot_tot_halo) {
  add_Ndot_halo<false>(Ndot_halo, Ndot2_halo, n_eff_halo, Ndot_HI_halo, Ndot2_HI_halo, n_eff_HI_halo, bin_Ndot_halo, bin_Ndot2_halo, bin_n_eff_halo, bin_Ndot_tot_halo);
}

/* Group and subhalo statistics for gas. */
static inline void add_Ndot_gas_halo(vector<double>& Ndot_halo, vector<double>& Ndot2_halo, vector<double>& n_eff_halo,
                                     vector<double>& Ndot_HI_halo, vector<double>& Ndot2_HI_halo, vector<double>& n_eff_HI_halo,
                                     Image& bin_Ndot_halo, Image& bin_Ndot2_halo, Image& bin_n_eff_halo, Image& bin_Ndot_tot_halo) {
  add_Ndot_halo<true>(Ndot_halo, Ndot2_halo, n_eff_halo, Ndot_HI_halo, Ndot2_HI_halo, n_eff_HI_halo, bin_Ndot_halo, bin_Ndot2_halo, bin_n_eff_halo, bin_Ndot_tot_halo);
}

/* Group and subhalo statistics. */
static inline void update_Ndot_halo(Image& bin_Ndot_halo, Image& bin_Ndot_tot_halo) {
  const int n_halos_bins = bin_Ndot_halo.size(); // Number of halos * bins
  #pragma omp parallel for
  for (int i = 0; i < n_halos_bins; ++i)
    bin_Ndot_tot_halo[i] += bin_Ndot_halo[i]; // Global rate
}

/* Group and subhalo statistics. */
static inline void global_Ndot_halo(vector<double>& Ndot_halo, const vector<double>& Ndot_stars_halo, const vector<double>& Ndot_gas_halo,
                                    vector<double>& Ndot_HI_halo, const vector<double>& Ndot_stars_HI_halo, const vector<double>& Ndot_gas_HI_halo) {
  const int n_halos = Ndot_halo.size();      // Number of halos
  #pragma omp parallel for
  for (int i_halo = 0; i_halo < n_halos; ++i_halo) {
    Ndot_halo[i_halo] = 0.;
    Ndot_HI_halo[i_halo] = 0.;
  }
  if (star_based_emission) {
    #pragma omp parallel for
    for (int i_halo = 0; i_halo < n_halos; ++i_halo) {
      Ndot_halo[i_halo] += Ndot_stars_halo[i_halo];
      Ndot_HI_halo[i_halo] += Ndot_stars_HI_halo[i_halo];
    }
  }
  if (cell_based_emission) {
    #pragma omp parallel for
    for (int i_halo = 0; i_halo < n_halos; ++i_halo) {
      Ndot_halo[i_halo] += Ndot_gas_halo[i_halo];
      Ndot_HI_halo[i_halo] += Ndot_gas_HI_halo[i_halo];
    }
  }
}

/* Setup that is performed during each iteration. */
void Ionization::iterative_setup() {
  iterative_setup_init();                    // Global initializations
  if (star_based_emission)
    iterative_setup_stars();                 // Stellar emission
  if (cell_based_emission)
    iterative_setup_gas();                   // Gas emission
  if (AGN_emission)
    iterative_setup_AGN();                   // AGN emission
  if (point_source)
    iterative_setup_point();                 // Point source
  if (plane_source)
    iterative_setup_plane();                 // Plane source
  iterative_setup_calc();                    // Global calculations
}

/* Clear the ionization spectral data. */
static inline void clear(IonBinData& ion) {
  // Weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    ion.bin_L[i] = 0.;                       // Bin luminosities [erg/s]
    ion.bin_Ndot[i] = 0.;                    // Bin rates [photons/s]
    for (auto& sigma_ion : ion.sigma_ions)
      sigma_ion[i] = 0.;                     // Species ionization cross-sections [cm^2]
    for (auto& epsilon_ion : ion.epsilon_ions)
      epsilon_ion[i] = 0.;                   // Species ionization heating [erg]
    ion.mean_energy[i] = 0.;                 // Mean energy of each frequency bin [erg]
    for (int i_dust = 0; i_dust < n_dust_species; ++i_dust) {
      ion.kappas[i_dust][i] = 0.;            // Dust opacity [cm^2/g dust]
      ion.albedos[i_dust][i] = 0.;           // Dust albedo for scattering vs. absorption
      ion.cosines[i_dust][i] = 0.;           // Anisotropy parameter: <μ> for dust scattering
    }
  }
}

/* Global initializations that are performed during each iteration. */
void Ionization::iterative_setup_init() {
  clear(ion_global);                         // Clear ionization spectral data
}

/* Calculations that are performed during each iteration. */
void Ionization::iterative_setup_calc() {
  // Global weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
  auto& ig = ion_global;                     // Global ionization data
  #pragma omp parallel for
  for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
    const double bin_Ndot = bin_Ndot_tot[i_bin]; // Bin Ndot
    const double inv_Ndot = safe_division(1., bin_Ndot); // 1 / Ndot
    // Note: Convert global spectral properties (sigma_L -> epsilon) and (sigma_Ndot -> sigma)
    for (int ai = 0; ai < n_active_ions; ++ai) {
      ig.epsilon_ions[ai][i_bin] = (ig.sigma_ions[ai][i_bin] > 0.) ? chop(ig.epsilon_ions[ai][i_bin] / ig.sigma_ions[ai][i_bin] - ions_erg[ai]) : 0.;
      // Only update the sigmas after the epsilon calculations
      ig.sigma_ions[ai][i_bin] = chop(inv_Ndot * ig.sigma_ions[ai][i_bin], sigma_chop);
    }
    ig.mean_energy[i_bin] = inv_Ndot * bin_L_tot[i_bin];
    for (int i_dust = 0; i_dust < n_dust_species; ++i_dust) {
      ig.kappas[i_dust][i_bin] = inv_Ndot * ig.kappas[i_dust][i_bin];
      ig.albedos[i_dust][i_bin] = inv_Ndot * ig.albedos[i_dust][i_bin];
      ig.cosines[i_dust][i_bin] = inv_Ndot * ig.cosines[i_dust][i_bin];
    }
  }

  // Calculate the mixed source pdf based on luminosities
  ig.L = ion_stars.L + ion_gas.L + L_AGN + ion_point.L + ion_plane.L;
  ig.Ndot = ion_stars.Ndot + ion_gas.Ndot + Ndot_AGN + ion_point.Ndot + ion_plane.Ndot;
  ig.boosted_Ndot = ion_stars.boosted_Ndot + ion_gas.boosted_Ndot + Ndot_AGN + ion_point.boosted_Ndot + ion_plane.boosted_Ndot; // TODO: + boosted_Ndot_AGN
  ig.HI.L = ion_stars.HI.L + ion_gas.HI.L + L_AGN + ion_point.HI.L + ion_plane.HI.L; // TODO: + L_AGN_HI
  ig.HI.Ndot = ion_stars.HI.Ndot + ion_gas.HI.Ndot + Ndot_AGN + ion_point.HI.Ndot + ion_plane.HI.Ndot; // TODO: + Ndot_AGN_HI
  mixed_ion_cdf = mixed_ion_pdf = {ion_stars.Ndot, ion_gas.Ndot, Ndot_AGN, ion_point.Ndot, ion_plane.Ndot}; // Unnormalized pdf
  mixed_ion_weights = {ion_stars.boosted_Ndot, ion_gas.boosted_Ndot, Ndot_AGN, ion_point.boosted_Ndot, ion_plane.boosted_Ndot}; // Unnormalized pdf

  // Group and subhalo statistics
  if constexpr (output_groups) {
    if (n_groups > 0) {
      global_Ndot_halo(Ndot_grp, Ndot_stars_grp, Ndot_gas_grp, Ndot_HI_grp, Ndot_stars_HI_grp, Ndot_gas_HI_grp);
      if (output_grp_vir)
        global_Ndot_halo(Ndot_grp_vir, Ndot_stars_grp_vir, Ndot_gas_grp_vir, Ndot_HI_grp_vir, Ndot_stars_HI_grp_vir, Ndot_gas_HI_grp_vir);
      if (output_grp_gal)
        global_Ndot_halo(Ndot_grp_gal, Ndot_stars_grp_gal, Ndot_gas_grp_gal, Ndot_HI_grp_gal, Ndot_stars_HI_grp_gal, Ndot_gas_HI_grp_gal);
    }
    if (n_ugroups > 0)
      global_Ndot_halo(Ndot_ugrp, Ndot_stars_ugrp, Ndot_gas_ugrp, Ndot_HI_ugrp, Ndot_stars_HI_ugrp, Ndot_gas_HI_ugrp);
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {
      global_Ndot_halo(Ndot_sub, Ndot_stars_sub, Ndot_gas_sub, Ndot_HI_sub, Ndot_stars_HI_sub, Ndot_gas_HI_sub);
      if (output_sub_vir)
        global_Ndot_halo(Ndot_sub_vir, Ndot_stars_sub_vir, Ndot_gas_sub_vir, Ndot_HI_sub_vir, Ndot_stars_HI_sub_vir, Ndot_gas_HI_sub_vir);
      if (output_sub_gal)
        global_Ndot_halo(Ndot_sub_gal, Ndot_stars_sub_gal, Ndot_gas_sub_gal, Ndot_HI_sub_gal, Ndot_stars_HI_sub_gal, Ndot_gas_HI_sub_gal);
    }
    if (n_usubhalos > 0)
      global_Ndot_halo(Ndot_usub, Ndot_stars_usub, Ndot_gas_usub, Ndot_HI_usub, Ndot_stars_HI_usub, Ndot_gas_HI_usub);
  }

  // Apply luminosity and frequency boosting
  const double Ndot_norm = 1. / ig.Ndot;
  for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i) {
    const double nu_pdf = mixed_ion_weights[i] * Ndot_norm; // Frequency boost
    mixed_ion_weights[i] = mixed_ion_pdf[i] *= Ndot_norm; // Copy the normalized pdf
    if (mixed_ion_pdf[i] > 0.)               // Apply power law boost
      mixed_ion_cdf[i] = pow(nu_pdf, j_exp); // nu_pdf^j_exp (unnormalized)
    else
      mixed_ion_cdf[i] = 0.;                 // No probability
  }
  const double cdf_norm = 1. / omp_sum(mixed_ion_cdf);
  for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i) {
    mixed_ion_cdf[i] *= cdf_norm;            // nu_pdf^j_exp (normalized)
    if (mixed_ion_cdf[i] > 0.)
      mixed_ion_weights[i] /= mixed_ion_cdf[i]; // weight = pdf / nu_pdf^j_exp
  }
  // Calculate the global bin cdf
  for (int i = 0; i < n_bins; ++i)
    ig.bin_cdf[i] = 0.;                      // Reset global cdf
  if (star_based_emission) {
    #pragma omp parallel for
    for (int i = 0; i < n_bins; ++i)
      ig.bin_cdf[i] += mixed_ion_cdf[STARS] * ion_stars.bin_cdf[i];
  }
  if (cell_based_emission) {
    #pragma omp parallel for
    for (int i = 0; i < n_bins; ++i)
      ig.bin_cdf[i] += mixed_ion_cdf[CELLS] * ion_gas.bin_cdf[i];
  }
  // TODO: AGN
  if (point_source) {
    #pragma omp parallel for
    for (int i = 0; i < n_bins; ++i)
      ig.bin_cdf[i] += mixed_ion_cdf[POINT] * ion_point.bin_cdf[i];
  }
  if (plane_source) {
    #pragma omp parallel for
    for (int i = 0; i < n_bins; ++i)
      ig.bin_cdf[i] += mixed_ion_cdf[PLANE] * ion_plane.bin_cdf[i];
  }
  // Turn the pdf into a cdf
  for (int i = 1; i < MAX_ION_SOURCE_TYPES; ++i)
    mixed_ion_cdf[i] += mixed_ion_cdf[i - 1];
  const double boosted_Ndot_norm = 1. / ig.bin_cdf[n_bins-1]; // Normalization (1 / boosted_Ndot)
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i)
    ig.bin_cdf[i] *= boosted_Ndot_norm;      // Boosted cdf (normalized)
  ig.bin_cdf[n_bins-1] = 1.;                 // Ensure last value is 1

  // Populate masked versions
  for (int i = 0; i < n_ion_source_types; ++i) {
    const int i_full = ion_source_types_mask[i];
    mixed_ion_pdf_mask[i] = mixed_ion_pdf[i_full];
    mixed_ion_cdf_mask[i] = mixed_ion_cdf[i_full];
    mixed_ion_weights_mask[i] = mixed_ion_weights[i_full];
  }
}

/* Star setup that is performed once. */
void Ionization::initial_setup_stars() {
  // Allocate the pdf based on star photon rates
  j_cdf_stars.resize(n_stars);               // Cumulative distribution function
  j_weights_stars.resize(n_stars);           // Star luminosity boosting weights

  // Allocate group and subhalo properties
  if constexpr (output_groups) {
    if (n_groups > 0) {                      // Have groups
      Ndot_stars_grp = vector<double>(n_groups); // Group stellar photon rates [photons/s]
      Ndot2_stars_grp = vector<double>(n_groups); // Group stellar rates^2 [photons^2/s^2]
      n_stars_eff_grp = vector<double>(n_groups); // Effective number of stars: 1/<w>
      Ndot_stars_HI_grp = vector<double>(n_groups); // Group HI stellar photon rates [photons/s]
      Ndot2_stars_HI_grp = vector<double>(n_groups); // Group HI stellar rates^2 [photons^2/s^2]
      n_stars_eff_HI_grp = vector<double>(n_groups); // Effective number of stars: 1/<w>
      bin_Ndot_stars_grp = Image(n_groups, n_bins); // Group bin stellar photon rates [photons/s]
      bin_Ndot2_stars_grp = Image(n_groups, n_bins); // Group bin stellar rates^2 [photons^2/s^2]
      bin_n_stars_eff_grp = Image(n_groups, n_bins); // Effective number of stars: 1/<w>
      if (output_grp_vir) {
        Ndot_stars_grp_vir = vector<double>(n_groups); // Group stellar photon rates [photons/s]
        Ndot2_stars_grp_vir = vector<double>(n_groups); // Group stellar rates^2 [photons^2/s^2]
        n_stars_eff_grp_vir = vector<double>(n_groups); // Effective number of stars: 1/<w>
        Ndot_stars_HI_grp_vir = vector<double>(n_groups); // Group HI stellar photon rates [photons/s]
        Ndot2_stars_HI_grp_vir = vector<double>(n_groups); // Group HI stellar rates^2 [photons^2/s^2]
        n_stars_eff_HI_grp_vir = vector<double>(n_groups); // Effective number of stars: 1/<w>
        bin_Ndot_stars_grp_vir = Image(n_groups, n_bins); // Group bin stellar photon rates [photons/s]
        bin_Ndot2_stars_grp_vir = Image(n_groups, n_bins); // Group bin stellar rates^2 [photons^2/s^2]
        bin_n_stars_eff_grp_vir = Image(n_groups, n_bins); // Effective number of stars: 1/<w>
      }
      if (output_grp_gal) {
        Ndot_stars_grp_gal = vector<double>(n_groups); // Group stellar photon rates [photons/s]
        Ndot2_stars_grp_gal = vector<double>(n_groups); // Group stellar rates^2 [photons^2/s^2]
        n_stars_eff_grp_gal = vector<double>(n_groups); // Effective number of stars: 1/<w>
        Ndot_stars_HI_grp_gal = vector<double>(n_groups); // Group HI stellar photon rates [photons/s]
        Ndot2_stars_HI_grp_gal = vector<double>(n_groups); // Group HI stellar rates^2 [photons^2/s^2]
        n_stars_eff_HI_grp_gal = vector<double>(n_groups); // Effective number of stars: 1/<w>
        bin_Ndot_stars_grp_gal = Image(n_groups, n_bins); // Group bin stellar photon rates [photons/s]
        bin_Ndot2_stars_grp_gal = Image(n_groups, n_bins); // Group bin stellar rates^2 [photons^2/s^2]
        bin_n_stars_eff_grp_gal = Image(n_groups, n_bins); // Effective number of stars: 1/<w>
      }
    }
    if (n_ugroups > 0) {                     // Have unfiltered groups
      Ndot_stars_ugrp = vector<double>(n_ugroups); // Unfiltered group stellar photon rates [photons/s]
      Ndot2_stars_ugrp = vector<double>(n_ugroups); // Unfiltered group stellar rates^2 [photons^2/s^2]
      n_stars_eff_ugrp = vector<double>(n_ugroups); // Effective number of stars: 1/<w>
      Ndot_stars_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI stellar photon rates [photons/s]
      Ndot2_stars_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI stellar rates^2 [photons^2/s^2]
      n_stars_eff_HI_ugrp = vector<double>(n_ugroups); // Effective number of stars: 1/<w>
      bin_Ndot_stars_ugrp = Image(n_ugroups, n_bins); // Unfiltered group bin stellar photon rates [photons/s]
      bin_Ndot2_stars_ugrp = Image(n_ugroups, n_bins); // Unfiltered group bin stellar rates^2 [photons^2/s^2]
      bin_n_stars_eff_ugrp = Image(n_ugroups, n_bins); // Effective number of stars: 1/<w>
    }
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {                    // Have subhalos
      Ndot_stars_sub = vector<double>(n_subhalos); // Subhalo stellar photon rates [photons/s]
      Ndot2_stars_sub = vector<double>(n_subhalos); // Subhalo stellar rates^2 [photons^2/s^2]
      n_stars_eff_sub = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
      Ndot_stars_HI_sub = vector<double>(n_subhalos); // Subhalo HI stellar photon rates [photons/s]
      Ndot2_stars_HI_sub = vector<double>(n_subhalos); // Subhalo HI stellar rates^2 [photons^2/s^2]
      n_stars_eff_HI_sub = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
      bin_Ndot_stars_sub = Image(n_subhalos, n_bins); // Subhalo bin stellar photon rates [photons/s]
      bin_Ndot2_stars_sub = Image(n_subhalos, n_bins); // Subhalo bin stellar rates^2 [photons^2/s^2]
      bin_n_stars_eff_sub = Image(n_subhalos, n_bins); // Effective number of stars: 1/<w>
      if (output_sub_vir) {
        Ndot_stars_sub_vir = vector<double>(n_subhalos); // Subhalo stellar photon rates [photons/s]
        Ndot2_stars_sub_vir = vector<double>(n_subhalos); // Subhalo stellar rates^2 [photons^2/s^2]
        n_stars_eff_sub_vir = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
        Ndot_stars_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI stellar photon rates [photons/s]
        Ndot2_stars_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI stellar rates^2 [photons^2/s^2]
        n_stars_eff_HI_sub_vir = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
        bin_Ndot_stars_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin stellar photon rates [photons/s]
        bin_Ndot2_stars_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin stellar rates^2 [photons^2/s^2]
        bin_n_stars_eff_sub_vir = Image(n_subhalos, n_bins); // Effective number of stars: 1/<w>
      }
      if (output_sub_gal) {
        Ndot_stars_sub_gal = vector<double>(n_subhalos); // Subhalo stellar photon rates [photons/s]
        Ndot2_stars_sub_gal = vector<double>(n_subhalos); // Subhalo stellar rates^2 [photons^2/s^2]
        n_stars_eff_sub_gal = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
        Ndot_stars_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI stellar photon rates [photons/s]
        Ndot2_stars_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI stellar rates^2 [photons^2/s^2]
        n_stars_eff_HI_sub_gal = vector<double>(n_subhalos); // Effective number of stars: 1/<w>
        bin_Ndot_stars_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin stellar photon rates [photons/s]
        bin_Ndot2_stars_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin stellar rates^2 [photons^2/s^2]
        bin_n_stars_eff_sub_gal = Image(n_subhalos, n_bins); // Effective number of stars: 1/<w>
      }
    }
    if (n_usubhalos > 0) {                   // Have unfiltered subhalos
      Ndot_stars_usub = vector<double>(n_usubhalos); // Unfiltered subhalo stellar photon rates [photons/s]
      Ndot2_stars_usub = vector<double>(n_usubhalos); // Unfiltered subhalo stellar rates^2 [photons^2/s^2]
      n_stars_eff_usub = vector<double>(n_usubhalos); // Effective number of stars: 1/<w>
      Ndot_stars_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI stellar photon rates [photons/s]
      Ndot2_stars_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI stellar rates^2 [photons^2/s^2]
      n_stars_eff_HI_usub = vector<double>(n_usubhalos); // Effective number of stars: 1/<w>
      bin_Ndot_stars_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo bin stellar photon rates [photons/s]
      bin_Ndot2_stars_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo bin stellar rates^2 [photons^2/s^2]
      bin_n_stars_eff_usub = Image(n_usubhalos, n_bins); // Effective number of stars: 1/<w>
    }
  }

  // Group and subhalo flags
  const bool save_groups = output_groups && n_groups > 0;
  const bool save_ugroups = output_groups && n_ugroups > 0;
  const bool save_subhalos = output_subhalos && n_subhalos > 0;
  const bool save_usubhalos = output_subhalos && n_usubhalos > 0;
  const bool need_group_indices = save_groups || save_ugroups;
  const bool need_subhalo_indices = save_subhalos || save_usubhalos;

  if (source_model == "GMC") {
    root_error("GMC source model is not implemented.");
  } else if (source_model == "MRT") {
    root_error("MRT source model is not implemented.");
  }
  if (source_file_Z_age != "") {
    if (focus_groups_on_emission) {
      r_light_grp = vector<Vec3>(n_groups);  // Group center of luminosity position [cm]
      v_light_grp = vector<Vec3>(n_groups);  // Group center of luminosity velocity [cm/s]
    }
    if (focus_subhalos_on_emission) {
      r_light_sub = vector<Vec3>(n_subhalos); // Subhalo center of luminosity position [cm]
      v_light_sub = vector<Vec3>(n_subhalos); // Subhalo center of luminosity velocity [cm/s]
    }
    // Allocate the stellar luminosities and spectral properties
    auto& is = ion_stars;                    // Stellar ionization data
    bin_L_stars = vector<double>(n_bins);    // Bin luminosities [erg/s]
    bin_Ndot_stars = vector<double>(n_bins); // Bin rates [photons/s]
    bin_Ndot2_stars = vector<double>(n_bins); // Bin rates^2 [photons^2/s^2]
    bin_n_stars_eff = vector<double>(n_bins); // Bin effective number of stars: 1/<w>
    is.sigma_ions.resize(n_active_ions);     // Species ionization cross-sections [cm^2]
    for (auto& sigma_ion : is.sigma_ions)
      sigma_ion = vector<double>(n_bins);
    is.epsilon_ions.resize(n_active_ions);   // Species ionization heating [erg]
    for (auto& epsilon_ion : is.epsilon_ions)
      epsilon_ion = vector<double>(n_bins);
    is.mean_energy = vector<double>(n_bins); // Mean energy of each frequency bin [erg]
    for (int j = 0; j < n_dust_species; ++j) {
      is.kappas[j] = vector<double>(n_bins); // Dust opacity [cm^2/g dust]
      is.albedos[j] = vector<double>(n_bins); // Dust albedo for scattering vs. absorption
      is.cosines[j] = vector<double>(n_bins); // Anisotropy parameter: <μ> for dust scattering
    }
    // Rate weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
    Image sigma_Ndot_ions_stars(n_active_ions, n_bins);
    Image sigma_L_ions_stars(n_active_ions, n_bins);
    vector<double> linear_Ndot_stars(n_bins);
    dust_vectors kappas_Ndot_stars, albedos_Ndot_stars, cosines_Ndot_stars;
    for (int j = 0; j < n_dust_species; ++j) {
      kappas_Ndot_stars[j] = vector<double>(n_bins);
      albedos_Ndot_stars[j] = vector<double>(n_bins);
      cosines_Ndot_stars[j] = vector<double>(n_bins);
    }
    #pragma omp parallel for reduction(vec_plus:bin_L_stars, bin_Ndot_stars, bin_Ndot2_stars, linear_Ndot_stars) \
                             reduction(image_plus:sigma_Ndot_ions_stars, sigma_L_ions_stars) \
                             reduction(dust_plus:kappas_Ndot_stars, albedos_Ndot_stars, cosines_Ndot_stars) \
                             reduction(vec3_plus:r_light_grp, v_light_grp, r_light_sub, v_light_sub)
    for (int i = 0; i < n_stars; ++i) {
      // Avoid stars that are in special cells or outside the emission region
      if (invalid_star(i)) {
        invalidate_star(i);                  // No emission (filtered out)
        continue;
      }

      // Local group and subhalo indices
      int i_grp = -1, i_ugrp = -1, i_sub = -1, i_usub = -1;
      bool in_grp_vir = false, in_grp_gal = false, in_sub_vir = false, in_sub_gal = false;
      if (need_group_indices) {
        const int grp_id = group_id_star[i]; // IDs
        if (save_groups)
          i_grp = get_index_sorted(grp_id, group_id); // Group ID index
        if (save_ugroups)
          i_ugrp = get_index_sorted(grp_id, ugroup_id); // Unfiltered group ID index
        if (i_grp >= 0) {
          if (output_grp_vir)
            in_grp_vir = norm2(r_star[i] - r_grp_vir[i_grp]) < R2_grp_vir[i_grp];
          if (output_grp_gal)
            in_grp_gal = norm2(r_star[i] - r_grp_gal[i_grp]) < R2_grp_gal[i_grp];
        }
      }
      if (need_subhalo_indices) {
        const int sub_id = subhalo_id_star[i]; // IDs
        if (save_subhalos)
          i_sub = get_index_sorted(sub_id, subhalo_id); // Subhalo ID index
        if (save_usubhalos)
          i_usub = get_index_sorted(sub_id, usubhalo_id); // Unfiltered subhalo ID index
        if (i_sub >= 0) {
          if (output_sub_vir)
            in_sub_vir = norm2(r_star[i] - r_sub_vir[i_sub]) < R2_sub_vir[i_sub];
          if (output_sub_gal)
            in_sub_gal = norm2(r_star[i] - r_sub_gal[i_sub]) < R2_sub_gal[i_sub];
        }
      }
      if (output_stars) {                    // Reset the weight or invalidate the star
        if (output_grp_vir && !in_grp_vir)
          weight_stars_grp_vir[i] = -1.;     // Not in group virial region
        if (output_grp_gal && !in_grp_gal)
          weight_stars_grp_gal[i] = -1.;     // Not in group galaxy region
        if (output_sub_vir && !in_sub_vir)
          weight_stars_sub_vir[i] = -1.;     // Not in subhalo virial region
        if (output_sub_gal && !in_sub_gal)
          weight_stars_sub_gal[i] = -1.;     // Not in subhalo galaxy region
      }

      const double mass = m_init_star[i];    // Initial mass of the star [Msun]
      const double Z = Z_star[i];            // Metallicity of the star
      const double age = age_star[i];        // Age of the star [Gyr]

      int i_L_Z = 0, i_L_age = 0;            // Lower interpolation index
      double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
      if (Z >= spec_Z_age.max_Z) {           // Metallicity maximum
        i_L_Z = spec_Z_age.n_Zs - 2;         // Metallicity bins - 2
        frac_R_Z = 1.;
      } else if (Z > spec_Z_age.min_Z) {     // Metallicity minimum
        const double logZ = log10(Z);        // Interpolate in log space
        while (logZ > spec_Z_age.log_Z[i_L_Z+1])
          i_L_Z++;                           // Search metallicity indices
        frac_R_Z = (logZ - spec_Z_age.log_Z[i_L_Z]) / (spec_Z_age.log_Z[i_L_Z+1] - spec_Z_age.log_Z[i_L_Z]);
      }
      if (age >= spec_Z_age.max_age) {       // Age maximum
        i_L_age = spec_Z_age.n_ages - 2;     // Age bins - 2
        frac_R_age = 1.;
      } else if (age > spec_Z_age.min_age) { // Age minimum
        const double logA = log10(age);      // Interpolate in log space
        while (logA > spec_Z_age.log_age[i_L_age+1])
          i_L_age++;                         // Search age indices
        frac_R_age = (logA - spec_Z_age.log_age[i_L_age]) / (spec_Z_age.log_age[i_L_age+1] - spec_Z_age.log_age[i_L_age]);
      }

      // Bilinear interpolation (based on left and right fractions)
      const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
      const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
      double local_Ndot_stars = 0.;          // Needed for j_cdf_stars
      for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
        const double local_bin_Ndot_stars = EVAL_ZA(Ndot); // Photon rates [photons/s]
        const double local_bin_Ndot2_stars = local_bin_Ndot_stars * local_bin_Ndot_stars; // Photon rates^2 [photons^2/s^2]
        const double local_bin_L_stars = EVAL_ZA(L); // Luminosities [erg/s]
        local_Ndot_stars += local_bin_Ndot_stars;
        bin_Ndot_stars[i_bin] += local_bin_Ndot_stars;
        bin_Ndot2_stars[i_bin] += local_bin_Ndot2_stars;
        bin_L_stars[i_bin] += local_bin_L_stars; // Luminosities [erg/s]
        if (i_grp >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_grp(i_grp, i_bin) += local_bin_Ndot_stars;
          #pragma omp atomic
          bin_Ndot2_stars_grp(i_grp, i_bin) += local_bin_Ndot2_stars;
          if (in_grp_vir) {
            #pragma omp atomic
            bin_Ndot_stars_grp_vir(i_grp, i_bin) += local_bin_Ndot_stars;
            #pragma omp atomic
            bin_Ndot2_stars_grp_vir(i_grp, i_bin) += local_bin_Ndot2_stars;
          }
          if (in_grp_gal) {
            #pragma omp atomic
            bin_Ndot_stars_grp_gal(i_grp, i_bin) += local_bin_Ndot_stars;
            #pragma omp atomic
            bin_Ndot2_stars_grp_gal(i_grp, i_bin) += local_bin_Ndot2_stars;
          }
          if (focus_groups_on_emission) {
            r_light_grp[i_grp] += local_bin_Ndot_stars * r_star[i]; // Center of luminosity position [cm erg/s]
            v_light_grp[i_grp] += local_bin_Ndot_stars * v_star[i]; // Center of luminosity velocity [cm/s erg/s]
          }
        } else if (i_ugrp >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_ugrp(i_ugrp, i_bin) += local_bin_Ndot_stars;
          #pragma omp atomic
          bin_Ndot2_stars_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_stars;
        }
        if (i_sub >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_sub(i_sub, i_bin) += local_bin_Ndot_stars;
          #pragma omp atomic
          bin_Ndot2_stars_sub(i_sub, i_bin) += local_bin_Ndot2_stars;
          if (in_sub_vir) {
            #pragma omp atomic
            bin_Ndot_stars_sub_vir(i_sub, i_bin) += local_bin_Ndot_stars;
            #pragma omp atomic
            bin_Ndot2_stars_sub_vir(i_sub, i_bin) += local_bin_Ndot2_stars;
          }
          if (in_sub_gal) {
            #pragma omp atomic
            bin_Ndot_stars_sub_gal(i_sub, i_bin) += local_bin_Ndot_stars;
            #pragma omp atomic
            bin_Ndot2_stars_sub_gal(i_sub, i_bin) += local_bin_Ndot2_stars;
          }
          if (focus_subhalos_on_emission) {
            r_light_sub[i_sub] += local_bin_Ndot_stars * r_star[i]; // Center of luminosity position [cm erg/s]
            v_light_sub[i_sub] += local_bin_Ndot_stars * v_star[i]; // Center of luminosity velocity [cm/s erg/s]
          }
        } else if (i_usub >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_usub(i_usub, i_bin) += local_bin_Ndot_stars;
          #pragma omp atomic
          bin_Ndot2_stars_usub(i_usub, i_bin) += local_bin_Ndot2_stars;
        }
        for (int ai = 0; ai < n_active_ions; ++ai) {
          // Cross-section rates [cm^2 photons/s]
          auto& log_sigma_Ndot_ion = spec_Z_age.log_sigma_Ndot_ions[ai];
          double log_sigma_Ndot_val = (log_sigma_Ndot_ion(i_L_Z,i_L_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age
                                    + (log_sigma_Ndot_ion(i_L_Z,i_R_age,i_bin)*frac_L_Z + log_sigma_Ndot_ion(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age;
          sigma_Ndot_ions_stars(ai, i_bin) += mass * pow(10., log_sigma_Ndot_val);
          // Photoheating rates [cm^2 erg/s]
          auto& log_sigma_L_ion = spec_Z_age.log_sigma_L_ions[ai];
          double log_sigma_L_val = (log_sigma_L_ion(i_L_Z,i_L_age,i_bin)*frac_L_Z + log_sigma_L_ion(i_R_Z,i_L_age,i_bin)*frac_R_Z) * frac_L_age
                                 + (log_sigma_L_ion(i_L_Z,i_R_age,i_bin)*frac_L_Z + log_sigma_L_ion(i_R_Z,i_R_age,i_bin)*frac_R_Z) * frac_R_age;
          sigma_L_ions_stars(ai, i_bin) += mass * pow(10., log_sigma_L_val);
        }
        for (int j = 0; j < n_dust_species; ++j) {
          kappas_Ndot_stars[j][i_bin] += EVAL_ZA(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
          albedos_Ndot_stars[j][i_bin] += EVAL_ZA(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
          cosines_Ndot_stars[j][i_bin] += COS_NDOT_ZA; // Dust scattering anisotropy rates [photons/s]
        }
        linear_Ndot_stars[i_bin] += LIN_NDOT_ZA; // Normalization for cosine rates [photons/s]
      }
      j_cdf_stars[i] = local_Ndot_stars;     // Photon rate pdf
    }

    // Weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
    #pragma omp parallel for
    for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
      if (bin_Ndot_stars[i_bin] <= 0.)
        continue;                            // Avoid division by zero
      const double bin_Ndot = bin_Ndot_stars[i_bin]; // Bin Ndot
      const double inv_Ndot = safe_division(1., bin_Ndot); // 1 / Ndot
      for (int ai = 0; ai < n_active_ions; ++ai) {
        is.sigma_ions[ai][i_bin] = chop(inv_Ndot * sigma_Ndot_ions_stars(ai, i_bin), sigma_chop); // Cross-section [cm^2]
        is.epsilon_ions[ai][i_bin] = chop(sigma_L_ions_stars(ai, i_bin) / sigma_Ndot_ions_stars(ai, i_bin) - ions_erg[ai]); // Photoheating [erg]
      }
      is.mean_energy[i_bin] = inv_Ndot * bin_L_stars[i_bin]; // Mean energy of each bin [erg]
      for (int j = 0; j < n_dust_species; ++j) {
        is.kappas[j][i_bin] = inv_Ndot * kappas_Ndot_stars[j][i_bin]; // Dust opacity [cm^2/g dust]
        is.albedos[j][i_bin] = inv_Ndot * albedos_Ndot_stars[j][i_bin]; // Dust albedo for scattering vs. absorption
        is.cosines[j][i_bin] = safe_division(cosines_Ndot_stars[j][i_bin], linear_Ndot_stars[i_bin]); // Anisotropy parameter: <μ> for dust scattering
      }
    }
  } else if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR" || source_model == "BC03-IMF" ||
      source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
    // Calculate the star luminosities
    double L_HI = 0.;                        // Total ionizing luminosity [erg/s]
    double L_HeI = 0.;
    double L_HeII = 0.;
    double Ndot_HI = 0.;                     // Total ionizing rate [photons/s]
    double Ndot_HeI = 0.;
    double Ndot_HeII = 0.;
    double Ndot2_HI = 0.;                    // Total ionizing rate^2 [photons^2/s^2]
    double Ndot2_HeI = 0.;
    double Ndot2_HeII = 0.;
    double sigma_Ndot_HI_1 = 0.;             // Cross-section rates [cm^2 photons/s]
    double sigma_Ndot_HI_2 = 0.;
    double sigma_Ndot_HI_3 = 0.;
    double sigma_Ndot_HeI_2 = 0.;
    double sigma_Ndot_HeI_3 = 0.;
    double sigma_Ndot_HeII_3 = 0.;
    double sigma_L_HI_1 = 0.;                // Photoheating rates [cm^2 erg/s]
    double sigma_L_HI_2 = 0.;
    double sigma_L_HI_3 = 0.;
    double sigma_L_HeI_2 = 0.;
    double sigma_L_HeI_3 = 0.;
    double sigma_L_HeII_3 = 0.;
    double kappa_Ndot_HI = 0.;               // Dust opacity rates [photons/s cm^2/g dust]
    double kappa_Ndot_HeI = 0.;
    double kappa_Ndot_HeII = 0.;
    double albedo_Ndot_HI = 0.;              // Dust scattering albedo rates [photons/s]
    double albedo_Ndot_HeI = 0.;
    double albedo_Ndot_HeII = 0.;
    double cosine_Ndot_HI = 0.;              // Dust scattering anisotropy rates [photons/s]
    double cosine_Ndot_HeI = 0.;
    double cosine_Ndot_HeII = 0.;
    if (source_model == "BC03-IMF-SOLAR" || source_model == "BC03-IMF-HALF-SOLAR") {
      #pragma omp parallel for reduction(+:L_HI, L_HeI, L_HeII, Ndot_HI, Ndot_HeI, Ndot_HeII, Ndot2_HI, Ndot2_HeI, Ndot2_HeII) \
                               reduction(+:sigma_Ndot_HI_1, sigma_Ndot_HI_2, sigma_Ndot_HI_3, sigma_Ndot_HeI_2, sigma_Ndot_HeI_3, sigma_Ndot_HeII_3) \
                               reduction(+:sigma_L_HI_1, sigma_L_HI_2, sigma_L_HI_3, sigma_L_HeI_2, sigma_L_HeI_3, sigma_L_HeII_3) \
                               reduction(+:kappa_Ndot_HI, kappa_Ndot_HeI, kappa_Ndot_HeII) \
                               reduction(+:albedo_Ndot_HI, albedo_Ndot_HeI, albedo_Ndot_HeII) \
                               reduction(+:cosine_Ndot_HI, cosine_Ndot_HeI, cosine_Ndot_HeII)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are in special cells or outside the emission region
        if (invalid_star(i)) {
          invalidate_star(i);                // No emission (filtered out)
          continue;
        }

        // Local group and subhalo indices
        int i_grp = -1, i_ugrp = -1, i_sub = -1, i_usub = -1;
        bool in_grp_vir = false, in_grp_gal = false, in_sub_vir = false, in_sub_gal = false;
        if (need_group_indices) {
          const int grp_id = group_id_star[i]; // IDs
          if (save_groups)
            i_grp = get_index_sorted(grp_id, group_id); // Group ID index
          if (save_ugroups)
            i_ugrp = get_index_sorted(grp_id, ugroup_id); // Unfiltered group ID index
          if (i_grp >= 0) {
            if (output_grp_vir)
              in_grp_vir = norm2(r_star[i] - r_grp_vir[i_grp]) < R2_grp_vir[i_grp];
            if (output_grp_gal)
              in_grp_gal = norm2(r_star[i] - r_grp_gal[i_grp]) < R2_grp_gal[i_grp];
          }
        }
        if (need_subhalo_indices) {
          const int sub_id = subhalo_id_star[i]; // IDs
          if (save_subhalos)
            i_sub = get_index_sorted(sub_id, subhalo_id); // Subhalo ID index
          if (save_usubhalos)
            i_usub = get_index_sorted(sub_id, usubhalo_id); // Unfiltered subhalo ID index
          if (i_sub >= 0) {
            if (output_sub_vir)
              in_sub_vir = norm2(r_star[i] - r_sub_vir[i_sub]) < R2_sub_vir[i_sub];
            if (output_sub_gal)
              in_sub_gal = norm2(r_star[i] - r_sub_gal[i_sub]) < R2_sub_gal[i_sub];
          }
        }
        if (output_stars) {                  // Reset the weight or invalidate the star
          if (output_grp_vir && !in_grp_vir)
            weight_stars_grp_vir[i] = -1.;   // Not in group virial region
          if (output_grp_gal && !in_grp_gal)
            weight_stars_grp_gal[i] = -1.;   // Not in group galaxy region
          if (output_sub_vir && !in_sub_vir)
            weight_stars_sub_vir[i] = -1.;   // Not in subhalo virial region
          if (output_sub_gal && !in_sub_gal)
            weight_stars_sub_gal[i] = -1.;   // Not in subhalo galaxy region
        }

        const double mass = m_init_star[i];  // Initial mass of the star [Msun]
        const double age = age_star[i];      // Age of the star [Gyr]
        int edge = -1;
        if (age <= 1.00000001e-3)            // Age minimum = 1 Myr
          edge = 0;
        else if (age >= 99.999999)           // Age maximum = 100 Gyr
          edge = 50;
        else {
          const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
          const double f_age = floor(d_age); // Floor coordinate
          const double frac_R = d_age - f_age; // Interpolation fraction (right)
          const double frac_L = 1. - frac_R; // Interpolation fraction (left)
          const int i_L = f_age;             // Table age index (left)
          const int i_R = i_L + 1;           // Table age index (right)
          const double local_Ndot_HI = mass * (ion_age.Ndot_HI[i_L]*frac_L + ion_age.Ndot_HI[i_R]*frac_R);
          const double local_Ndot_HeI = mass * (ion_age.Ndot_HeI[i_L]*frac_L + ion_age.Ndot_HeI[i_R]*frac_R);
          const double local_Ndot_HeII = mass * (ion_age.Ndot_HeII[i_L]*frac_L + ion_age.Ndot_HeII[i_R]*frac_R);
          const double local_Ndot2_HI = local_Ndot_HI * local_Ndot_HI; // Photon rate^2 [photons^2/s^2]
          const double local_Ndot2_HeI = local_Ndot_HeI * local_Ndot_HeI;
          const double local_Ndot2_HeII = local_Ndot_HeII * local_Ndot_HeII;
          j_cdf_stars[i] = local_Ndot_HI + local_Ndot_HeI + local_Ndot_HeII; // Photon rate pdf [photons/s]
          Ndot_HI += local_Ndot_HI;          // Rates [photons/s]
          Ndot_HeI += local_Ndot_HeI;
          Ndot_HeII += local_Ndot_HeII;
          Ndot2_HI += local_Ndot2_HI;        // Effective number of stars statistic
          Ndot2_HeI += local_Ndot2_HeI;
          Ndot2_HeII += local_Ndot2_HeII;
          L_HI += mass * (ion_age.L_HI[i_L]*frac_L + ion_age.L_HI[i_R]*frac_R); // Luminosities [erg/s]
          L_HeI += mass * (ion_age.L_HeI[i_L]*frac_L + ion_age.L_HeI[i_R]*frac_R);
          L_HeII += mass * (ion_age.L_HeII[i_L]*frac_L + ion_age.L_HeII[i_R]*frac_R);
          if (i_grp >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_grp(i_grp, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_grp(i_grp, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_grp(i_grp, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_grp(i_grp, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_grp(i_grp, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_grp(i_grp, 2) += local_Ndot2_HeII;
            if (in_grp_vir) {
              #pragma omp atomic
              bin_Ndot_stars_grp_vir(i_grp, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_grp_vir(i_grp, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_grp_vir(i_grp, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_grp_vir(i_grp, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_vir(i_grp, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_vir(i_grp, 2) += local_Ndot2_HeII;
            }
            if (in_grp_gal) {
              #pragma omp atomic
              bin_Ndot_stars_grp_gal(i_grp, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_grp_gal(i_grp, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_grp_gal(i_grp, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_grp_gal(i_grp, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_gal(i_grp, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_gal(i_grp, 2) += local_Ndot2_HeII;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_ugrp(i_ugrp, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_ugrp(i_ugrp, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_ugrp(i_ugrp, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_ugrp(i_ugrp, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_ugrp(i_ugrp, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_ugrp(i_ugrp, 2) += local_Ndot2_HeII;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_sub(i_sub, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_sub(i_sub, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_sub(i_sub, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_sub(i_sub, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_sub(i_sub, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_sub(i_sub, 2) += local_Ndot2_HeII;
            if (in_sub_vir) {
              #pragma omp atomic
              bin_Ndot_stars_sub_vir(i_sub, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_sub_vir(i_sub, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_sub_vir(i_sub, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_sub_vir(i_sub, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_vir(i_sub, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_vir(i_sub, 2) += local_Ndot2_HeII;
            }
            if (in_sub_gal) {
              #pragma omp atomic
              bin_Ndot_stars_sub_gal(i_sub, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_sub_gal(i_sub, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_sub_gal(i_sub, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_sub_gal(i_sub, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_gal(i_sub, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_gal(i_sub, 2) += local_Ndot2_HeII;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_usub(i_usub, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_usub(i_usub, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_usub(i_usub, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_usub(i_usub, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_usub(i_usub, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_usub(i_usub, 2) += local_Ndot2_HeII;
          }
          // Cross-section rates [cm^2 photons/s]
          sigma_Ndot_HI_1 += mass * (ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                   + ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
          sigma_Ndot_HI_2 += mass * (ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                   + ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          sigma_Ndot_HI_3 += mass * (ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                   + ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          sigma_Ndot_HeI_2 += mass * (ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                    + ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          sigma_Ndot_HeI_3 += mass * (ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                    + ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          sigma_Ndot_HeII_3 += mass * (ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                     + ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          // Photoheating rates [cm^2 erg/s]
          sigma_L_HI_1 += mass * (ion_age.epsilon_HI_1[i_L]*ion_age.sigma_HI_1[i_L]*ion_age.Ndot_HI[i_L]*frac_L
                                + ion_age.epsilon_HI_1[i_R]*ion_age.sigma_HI_1[i_R]*ion_age.Ndot_HI[i_R]*frac_R);
          sigma_L_HI_2 += mass * (ion_age.epsilon_HI_2[i_L]*ion_age.sigma_HI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                + ion_age.epsilon_HI_2[i_R]*ion_age.sigma_HI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          sigma_L_HI_3 += mass * (ion_age.epsilon_HI_3[i_L]*ion_age.sigma_HI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                + ion_age.epsilon_HI_3[i_R]*ion_age.sigma_HI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          sigma_L_HeI_2 += mass * (ion_age.epsilon_HeI_2[i_L]*ion_age.sigma_HeI_2[i_L]*ion_age.Ndot_HeI[i_L]*frac_L
                                 + ion_age.epsilon_HeI_2[i_R]*ion_age.sigma_HeI_2[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          sigma_L_HeI_3 += mass * (ion_age.epsilon_HeI_3[i_L]*ion_age.sigma_HeI_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                 + ion_age.epsilon_HeI_3[i_R]*ion_age.sigma_HeI_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          sigma_L_HeII_3 += mass * (ion_age.epsilon_HeII_3[i_L]*ion_age.sigma_HeII_3[i_L]*ion_age.Ndot_HeII[i_L]*frac_L
                                  + ion_age.epsilon_HeII_3[i_R]*ion_age.sigma_HeII_3[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          kappa_Ndot_HI += mass * (ion_age.kappa_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.kappa_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Opacity rates [photons/s cm^2/g dust]
          kappa_Ndot_HeI += mass * (ion_age.kappa_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.kappa_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          kappa_Ndot_HeII += mass * (ion_age.kappa_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.kappa_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          albedo_Ndot_HI += mass * (ion_age.albedo_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.albedo_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering albedo rates [photons/s]
          albedo_Ndot_HeI += mass * (ion_age.albedo_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.albedo_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          albedo_Ndot_HeII += mass * (ion_age.albedo_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.albedo_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
          cosine_Ndot_HI += mass * (ion_age.cosine_HI[i_L]*ion_age.Ndot_HI[i_L]*frac_L + ion_age.cosine_HI[i_R]*ion_age.Ndot_HI[i_R]*frac_R); // Scattering anisotropy rates [photons/s]
          cosine_Ndot_HeI += mass * (ion_age.cosine_HeI[i_L]*ion_age.Ndot_HeI[i_L]*frac_L + ion_age.cosine_HeI[i_R]*ion_age.Ndot_HeI[i_R]*frac_R);
          cosine_Ndot_HeII += mass * (ion_age.cosine_HeII[i_L]*ion_age.Ndot_HeII[i_L]*frac_L + ion_age.cosine_HeII[i_R]*ion_age.Ndot_HeII[i_R]*frac_R);
        }

        // Age was out of range so use the bounding value
        if (edge != -1) {
          const double local_Ndot_HI = mass * ion_age.Ndot_HI[edge]; // Photon rates [photons/s]
          const double local_Ndot_HeI = mass * ion_age.Ndot_HeI[edge];
          const double local_Ndot_HeII = mass * ion_age.Ndot_HeII[edge];
          const double local_Ndot2_HI = local_Ndot_HI * local_Ndot_HI; // Photon rate^2 [photons^2/s^2]
          const double local_Ndot2_HeI = local_Ndot_HeI * local_Ndot_HeI;
          const double local_Ndot2_HeII = local_Ndot_HeII * local_Ndot_HeII;
          j_cdf_stars[i] = local_Ndot_HI + local_Ndot_HeI + local_Ndot_HeII; // Photon rate pdf
          Ndot_HI += local_Ndot_HI;          // Rates [photons/s]
          Ndot_HeI += local_Ndot_HeI;
          Ndot_HeII += local_Ndot_HeII;
          Ndot2_HI += local_Ndot2_HI;        // Effective number of stars statistic
          Ndot2_HeI += local_Ndot2_HeI;
          Ndot2_HeII += local_Ndot2_HeII;
          L_HI += mass * ion_age.L_HI[edge]; // Luminosities [erg/s]
          L_HeI += mass * ion_age.L_HeI[edge];
          L_HeII += mass * ion_age.L_HeII[edge];
          if (i_grp >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_grp(i_grp, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_grp(i_grp, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_grp(i_grp, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_grp(i_grp, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_grp(i_grp, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_grp(i_grp, 2) += local_Ndot2_HeII;
            if (in_grp_vir) {
              #pragma omp atomic
              bin_Ndot_stars_grp_vir(i_grp, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_grp_vir(i_grp, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_grp_vir(i_grp, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_grp_vir(i_grp, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_vir(i_grp, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_vir(i_grp, 2) += local_Ndot2_HeII;
            }
            if (in_grp_gal) {
              #pragma omp atomic
              bin_Ndot_stars_grp_gal(i_grp, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_grp_gal(i_grp, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_grp_gal(i_grp, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_grp_gal(i_grp, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_gal(i_grp, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_grp_gal(i_grp, 2) += local_Ndot2_HeII;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_ugrp(i_ugrp, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_ugrp(i_ugrp, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_ugrp(i_ugrp, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_ugrp(i_ugrp, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_ugrp(i_ugrp, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_ugrp(i_ugrp, 2) += local_Ndot2_HeII;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_sub(i_sub, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_sub(i_sub, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_sub(i_sub, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_sub(i_sub, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_sub(i_sub, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_sub(i_sub, 2) += local_Ndot2_HeII;
            if (in_sub_vir) {
              #pragma omp atomic
              bin_Ndot_stars_sub_vir(i_sub, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_sub_vir(i_sub, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_sub_vir(i_sub, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_sub_vir(i_sub, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_vir(i_sub, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_vir(i_sub, 2) += local_Ndot2_HeII;
            }
            if (in_sub_gal) {
              #pragma omp atomic
              bin_Ndot_stars_sub_gal(i_sub, 0) += local_Ndot_HI;
              #pragma omp atomic
              bin_Ndot_stars_sub_gal(i_sub, 1) += local_Ndot_HeI;
              #pragma omp atomic
              bin_Ndot_stars_sub_gal(i_sub, 2) += local_Ndot_HeII;
              #pragma omp atomic
              bin_Ndot2_stars_sub_gal(i_sub, 0) += local_Ndot2_HI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_gal(i_sub, 1) += local_Ndot2_HeI;
              #pragma omp atomic
              bin_Ndot2_stars_sub_gal(i_sub, 2) += local_Ndot2_HeII;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            bin_Ndot_stars_usub(i_usub, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_usub(i_usub, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_usub(i_usub, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_usub(i_usub, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_usub(i_usub, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_usub(i_usub, 2) += local_Ndot2_HeII;
          }
          sigma_Ndot_HI_1 += local_Ndot_HI * ion_age.sigma_HI_1[edge]; // Cross-section rates [cm^2 photons/s]
          sigma_Ndot_HI_2 += local_Ndot_HeI * ion_age.sigma_HI_2[edge];
          sigma_Ndot_HI_3 += local_Ndot_HeII * ion_age.sigma_HI_3[edge];
          sigma_Ndot_HeI_2 += local_Ndot_HeI * ion_age.sigma_HeI_2[edge];
          sigma_Ndot_HeI_3 += local_Ndot_HeII * ion_age.sigma_HeI_3[edge];
          sigma_Ndot_HeII_3 += local_Ndot_HeII * ion_age.sigma_HeII_3[edge];
          sigma_L_HI_1 += local_Ndot_HI * ion_age.sigma_HI_1[edge] * ion_age.epsilon_HI_1[edge]; // Photoheating rates [cm^2 erg/s]
          sigma_L_HI_2 += local_Ndot_HeI * ion_age.sigma_HI_2[edge] * ion_age.epsilon_HI_2[edge];
          sigma_L_HI_3 += local_Ndot_HeII * ion_age.sigma_HI_3[edge] * ion_age.epsilon_HI_3[edge];
          sigma_L_HeI_2 += local_Ndot_HeI * ion_age.sigma_HeI_2[edge] * ion_age.epsilon_HeI_2[edge];
          sigma_L_HeI_3 += local_Ndot_HeII * ion_age.sigma_HeI_3[edge] * ion_age.epsilon_HeI_3[edge];
          sigma_L_HeII_3 += local_Ndot_HeII * ion_age.sigma_HeII_3[edge] * ion_age.epsilon_HeII_3[edge];
          kappa_Ndot_HI += local_Ndot_HI * ion_age.kappa_HI[edge]; // Opacity rates [photons/s cm^2/g dust]
          kappa_Ndot_HeI += local_Ndot_HeI * ion_age.kappa_HeI[edge];
          kappa_Ndot_HeII += local_Ndot_HeII * ion_age.kappa_HeII[edge];
          albedo_Ndot_HI += local_Ndot_HI * ion_age.albedo_HI[edge]; // Scattering albedo rates [photons/s]
          albedo_Ndot_HeI += local_Ndot_HeI * ion_age.albedo_HeI[edge];
          albedo_Ndot_HeII += local_Ndot_HeII * ion_age.albedo_HeII[edge];
          cosine_Ndot_HI += local_Ndot_HI * ion_age.cosine_HI[edge]; // Scattering anisotropy rates [photons/s]
          cosine_Ndot_HeI += local_Ndot_HeI * ion_age.cosine_HeI[edge];
          cosine_Ndot_HeII += local_Ndot_HeII * ion_age.cosine_HeII[edge];
        }
      }
    } else if (source_model == "BC03-IMF" || source_model == "BPASS-IMF-135-100" || source_model == "BPASS-CHAB-100") {
      #pragma omp parallel for reduction(+:L_HI, L_HeI, L_HeII, Ndot_HI, Ndot_HeI, Ndot_HeII, Ndot2_HI, Ndot2_HeI, Ndot2_HeII) \
                               reduction(+:sigma_Ndot_HI_1, sigma_Ndot_HI_2, sigma_Ndot_HI_3, sigma_Ndot_HeI_2, sigma_Ndot_HeI_3, sigma_Ndot_HeII_3) \
                               reduction(+:sigma_L_HI_1, sigma_L_HI_2, sigma_L_HI_3, sigma_L_HeI_2, sigma_L_HeI_3, sigma_L_HeII_3) \
                               reduction(+:kappa_Ndot_HI, kappa_Ndot_HeI, kappa_Ndot_HeII) \
                               reduction(+:albedo_Ndot_HI, albedo_Ndot_HeI, albedo_Ndot_HeII) \
                               reduction(+:cosine_Ndot_HI, cosine_Ndot_HeI, cosine_Ndot_HeII)
      for (int i = 0; i < n_stars; ++i) {
        // Avoid stars that are in special cells or outside the emission region
        if (invalid_star(i)) {
          invalidate_star(i);                // No emission (filtered out)
          continue;
        }

        // Local group and subhalo indices
        int i_grp = -1, i_ugrp = -1, i_sub = -1, i_usub = -1;
        bool in_grp_vir = false, in_grp_gal = false, in_sub_vir = false, in_sub_gal = false;
        if (need_group_indices) {
          const int grp_id = group_id_star[i]; // IDs
          if (save_groups)
            i_grp = get_index_sorted(grp_id, group_id); // Group ID index
          if (save_ugroups)
            i_ugrp = get_index_sorted(grp_id, ugroup_id); // Unfiltered group ID index
          if (i_grp >= 0) {
            if (output_grp_vir)
              in_grp_vir = norm2(r_star[i] - r_grp_vir[i_grp]) < R2_grp_vir[i_grp];
            if (output_grp_gal)
              in_grp_gal = norm2(r_star[i] - r_grp_gal[i_grp]) < R2_grp_gal[i_grp];
          }
        }
        if (need_subhalo_indices) {
          const int sub_id = subhalo_id_star[i]; // IDs
          if (save_subhalos)
            i_sub = get_index_sorted(sub_id, subhalo_id); // Subhalo ID index
          if (save_usubhalos)
            i_usub = get_index_sorted(sub_id, usubhalo_id); // Unfiltered subhalo ID index
          if (i_sub >= 0) {
            if (output_sub_vir)
              in_sub_vir = norm2(r_star[i] - r_sub_vir[i_sub]) < R2_sub_vir[i_sub];
            if (output_sub_gal)
              in_sub_gal = norm2(r_star[i] - r_sub_gal[i_sub]) < R2_sub_gal[i_sub];
          }
        }
        if (output_stars) {                  // Reset the weight or invalidate the star
          if (output_grp_vir && !in_grp_vir)
            weight_stars_grp_vir[i] = -1.;   // Not in group virial region
          if (output_grp_gal && !in_grp_gal)
            weight_stars_grp_gal[i] = -1.;   // Not in group galaxy region
          if (output_sub_vir && !in_sub_vir)
            weight_stars_sub_vir[i] = -1.;   // Not in subhalo virial region
          if (output_sub_gal && !in_sub_gal)
            weight_stars_sub_gal[i] = -1.;   // Not in subhalo galaxy region
        }

        const double mass = m_init_star[i];  // Initial mass of the star [Msun]
        const double Z = Z_star[i];          // Metallicity of the star
        const double age = age_star[i];      // Age of the star [Gyr]

        int i_L_Z = 0, i_L_age = 0;          // Lower interpolation index
        double frac_R_Z = 0., frac_R_age = 0.; // Upper interpolation fraction
        if (Z >= 4e-2) {                     // Metallicity maximum = 0.04
          i_L_Z = 11;                        // 13 metallicity bins
          frac_R_Z = 1.;
        } else if (Z > 1e-5) {               // Metallicity minimum = 10^-5
          const double logZ = log10(Z);      // Interpolate in log space
          while (logZ > logZ_BP[i_L_Z+1])
            i_L_Z++;                         // Search metallicity indices
          frac_R_Z = (logZ - logZ_BP[i_L_Z]) / (logZ_BP[i_L_Z+1] - logZ_BP[i_L_Z]);
        }
        if (age >= 100.) {                   // Age maximum = 100 Gyr
          i_L_age = 49;                      // 51 age bins
          frac_R_age = 1.;
        } else if (age > 1e-3) {             // Age minimum = 1 Myr
          const double d_age = 10. * (log10(age) + 3.); // Table coordinates (log age)
          const double f_age = floor(d_age); // Floor coordinate
          frac_R_age = d_age - f_age;        // Interpolation fraction (right)
          i_L_age = int(f_age);              // Table age index (left)
        }

        // Bilinear interpolation (based on left and right fractions)
        const int i_R_Z = i_L_Z + 1, i_R_age = i_L_age + 1;
        const double frac_L_Z = 1. - frac_R_Z, frac_L_age = 1. - frac_R_age;
        // Photon rates [photons/s]
        const double local_Ndot_HI = mass * pow(10., (ion_Z_age.log_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                   + (ion_Z_age.log_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        const double local_Ndot_HeI = mass * pow(10., (ion_Z_age.log_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                    + (ion_Z_age.log_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        const double local_Ndot_HeII = mass * pow(10., (ion_Z_age.log_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                                     + (ion_Z_age.log_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        const double local_Ndot2_HI = local_Ndot_HI * local_Ndot_HI; // Photon rate^2 [photons^2/s^2]
        const double local_Ndot2_HeI = local_Ndot_HeI * local_Ndot_HeI;
        const double local_Ndot2_HeII = local_Ndot_HeII * local_Ndot_HeII;
        j_cdf_stars[i] = local_Ndot_HI + local_Ndot_HeI + local_Ndot_HeII; // Photon rate pdf
        Ndot_HI += local_Ndot_HI;            // Photon rates [photons/s]
        Ndot_HeI += local_Ndot_HeI;
        Ndot_HeII += local_Ndot_HeII;
        Ndot2_HI += local_Ndot2_HI;          // Effective number of stars statistic
        Ndot2_HeI += local_Ndot2_HeI;
        Ndot2_HeII += local_Ndot2_HeII;
        if (i_grp >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_grp(i_grp, 0) += local_Ndot_HI;
          #pragma omp atomic
          bin_Ndot_stars_grp(i_grp, 1) += local_Ndot_HeI;
          #pragma omp atomic
          bin_Ndot_stars_grp(i_grp, 2) += local_Ndot_HeII;
          #pragma omp atomic
          bin_Ndot2_stars_grp(i_grp, 0) += local_Ndot2_HI;
          #pragma omp atomic
          bin_Ndot2_stars_grp(i_grp, 1) += local_Ndot2_HeI;
          #pragma omp atomic
          bin_Ndot2_stars_grp(i_grp, 2) += local_Ndot2_HeII;
          if (in_grp_vir) {
            #pragma omp atomic
            bin_Ndot_stars_grp_vir(i_grp, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_grp_vir(i_grp, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_grp_vir(i_grp, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_grp_vir(i_grp, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_grp_vir(i_grp, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_grp_vir(i_grp, 2) += local_Ndot2_HeII;
          }
          if (in_grp_gal) {
            #pragma omp atomic
            bin_Ndot_stars_grp_gal(i_grp, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_grp_gal(i_grp, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_grp_gal(i_grp, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_grp_gal(i_grp, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_grp_gal(i_grp, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_grp_gal(i_grp, 2) += local_Ndot2_HeII;
          }
        } else if (i_ugrp >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_ugrp(i_ugrp, 0) += local_Ndot_HI;
          #pragma omp atomic
          bin_Ndot_stars_ugrp(i_ugrp, 1) += local_Ndot_HeI;
          #pragma omp atomic
          bin_Ndot_stars_ugrp(i_ugrp, 2) += local_Ndot_HeII;
          #pragma omp atomic
          bin_Ndot2_stars_ugrp(i_ugrp, 0) += local_Ndot2_HI;
          #pragma omp atomic
          bin_Ndot2_stars_ugrp(i_ugrp, 1) += local_Ndot2_HeI;
          #pragma omp atomic
          bin_Ndot2_stars_ugrp(i_ugrp, 2) += local_Ndot2_HeII;
        }
        if (i_sub >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_sub(i_sub, 0) += local_Ndot_HI;
          #pragma omp atomic
          bin_Ndot_stars_sub(i_sub, 1) += local_Ndot_HeI;
          #pragma omp atomic
          bin_Ndot_stars_sub(i_sub, 2) += local_Ndot_HeII;
          #pragma omp atomic
          bin_Ndot2_stars_sub(i_sub, 0) += local_Ndot2_HI;
          #pragma omp atomic
          bin_Ndot2_stars_sub(i_sub, 1) += local_Ndot2_HeI;
          #pragma omp atomic
          bin_Ndot2_stars_sub(i_sub, 2) += local_Ndot2_HeII;
          if (in_sub_vir) {
            #pragma omp atomic
            bin_Ndot_stars_sub_vir(i_sub, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_sub_vir(i_sub, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_sub_vir(i_sub, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_sub_vir(i_sub, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_sub_vir(i_sub, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_sub_vir(i_sub, 2) += local_Ndot2_HeII;
          }
          if (in_sub_gal) {
            #pragma omp atomic
            bin_Ndot_stars_sub_gal(i_sub, 0) += local_Ndot_HI;
            #pragma omp atomic
            bin_Ndot_stars_sub_gal(i_sub, 1) += local_Ndot_HeI;
            #pragma omp atomic
            bin_Ndot_stars_sub_gal(i_sub, 2) += local_Ndot_HeII;
            #pragma omp atomic
            bin_Ndot2_stars_sub_gal(i_sub, 0) += local_Ndot2_HI;
            #pragma omp atomic
            bin_Ndot2_stars_sub_gal(i_sub, 1) += local_Ndot2_HeI;
            #pragma omp atomic
            bin_Ndot2_stars_sub_gal(i_sub, 2) += local_Ndot2_HeII;
          }
        } else if (i_usub >= 0) {
          #pragma omp atomic
          bin_Ndot_stars_usub(i_usub, 0) += local_Ndot_HI;
          #pragma omp atomic
          bin_Ndot_stars_usub(i_usub, 1) += local_Ndot_HeI;
          #pragma omp atomic
          bin_Ndot_stars_usub(i_usub, 2) += local_Ndot_HeII;
          #pragma omp atomic
          bin_Ndot2_stars_usub(i_usub, 0) += local_Ndot2_HI;
          #pragma omp atomic
          bin_Ndot2_stars_usub(i_usub, 1) += local_Ndot2_HeI;
          #pragma omp atomic
          bin_Ndot2_stars_usub(i_usub, 2) += local_Ndot2_HeII;
        }
        // Luminosities [erg/s]
        L_HI += mass * pow(10., (ion_Z_age.log_L_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                              + (ion_Z_age.log_L_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        L_HeI += mass * pow(10., (ion_Z_age.log_L_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                               + (ion_Z_age.log_L_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        L_HeII += mass * pow(10., (ion_Z_age.log_L_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                + (ion_Z_age.log_L_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_L_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        // Cross-section rates [cm^2 photons/s]
        sigma_Ndot_HI_1 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_sigma_Ndot_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_Ndot_HI_2 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_sigma_Ndot_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_Ndot_HI_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_sigma_Ndot_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_Ndot_HeI_2 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_sigma_Ndot_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_Ndot_HeI_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_sigma_Ndot_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_Ndot_HeII_3 += mass * pow(10., (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                           + (ion_Z_age.log_sigma_Ndot_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_Ndot_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        // Photoheating rates [cm^2 erg/s]
        sigma_L_HI_1 += mass * pow(10., (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_L_HI_1[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_1[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_L_HI_2 += mass * pow(10., (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_L_HI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_L_HI_3 += mass * pow(10., (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                      + (ion_Z_age.log_sigma_L_HI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_L_HeI_2 += mass * pow(10., (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_sigma_L_HeI_2[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_2[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_L_HeI_3 += mass * pow(10., (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_sigma_L_HeI_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeI_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        sigma_L_HeII_3 += mass * pow(10., (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                        + (ion_Z_age.log_sigma_L_HeII_3[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_sigma_L_HeII_3[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        // Opacity rates [photons/s cm^2/g dust]
        kappa_Ndot_HI += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                       + (ion_Z_age.log_kappa_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        kappa_Ndot_HeI += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                        + (ion_Z_age.log_kappa_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        kappa_Ndot_HeII += mass * pow(10., (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_kappa_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_kappa_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        // Scattering albedo rates [photons/s]
        albedo_Ndot_HI += mass * pow(10., (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                        + (ion_Z_age.log_albedo_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        albedo_Ndot_HeI += mass * pow(10., (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_albedo_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        albedo_Ndot_HeII += mass * pow(10., (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_albedo_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_albedo_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        // Scattering anisotropy rates [photons/s]
        cosine_Ndot_HI += mass * pow(10., (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                        + (ion_Z_age.log_cosine_Ndot_HI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        cosine_Ndot_HeI += mass * pow(10., (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                         + (ion_Z_age.log_cosine_Ndot_HeI[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeI[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
        cosine_Ndot_HeII += mass * pow(10., (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_L_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_L_age]*frac_R_Z) * frac_L_age
                                          + (ion_Z_age.log_cosine_Ndot_HeII[i_L_Z][i_R_age]*frac_L_Z + ion_Z_age.log_cosine_Ndot_HeII[i_R_Z][i_R_age]*frac_R_Z) * frac_R_age);
      }
    }

    bin_L_stars = {L_HI, L_HeI, L_HeII};     // Total bin luminosities [erg/s]
    bin_Ndot_stars = {Ndot_HI, Ndot_HeI, Ndot_HeII}; // Total bin rates [photons/s]
    bin_Ndot2_stars = {Ndot2_HI, Ndot2_HeI, Ndot2_HeII}; // Total bin rates^2 [photons^2/s^2]
    bin_n_stars_eff = vector<double>(n_bins); // Bin effective number of stars: 1/<w>
    // Weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
    ion_stars.sigma_ions.resize(n_bins);
    ion_stars.sigma_ions[HI_ION] = {sigma_Ndot_HI_1 / Ndot_HI, sigma_Ndot_HI_2 / Ndot_HeI, sigma_Ndot_HI_3 / Ndot_HeII};
    ion_stars.sigma_ions[HeI_ION] = {0., sigma_Ndot_HeI_2 / Ndot_HeI, sigma_Ndot_HeI_3 / Ndot_HeII};
    ion_stars.sigma_ions[HeII_ION] = {0., 0., sigma_Ndot_HeII_3 / Ndot_HeII};
    ion_stars.epsilon_ions.resize(n_bins);
    ion_stars.epsilon_ions[HI_ION] = {sigma_L_HI_1 / sigma_Ndot_HI_1, sigma_L_HI_2 / sigma_Ndot_HI_2, sigma_L_HI_3 / sigma_Ndot_HI_3};
    ion_stars.epsilon_ions[HeI_ION] = {0., sigma_L_HeI_2 / sigma_Ndot_HeI_2, sigma_L_HeI_3 / sigma_Ndot_HeI_3};
    ion_stars.epsilon_ions[HeII_ION] = {0., 0., sigma_L_HeII_3 / sigma_Ndot_HeII_3};
    ion_stars.mean_energy = {L_HI / Ndot_HI, L_HeI / Ndot_HeI, L_HeII / Ndot_HeII};
    ion_stars.kappas[0] = {kappa_Ndot_HI / Ndot_HI, kappa_Ndot_HeI / Ndot_HeI, kappa_Ndot_HeII / Ndot_HeII};
    ion_stars.albedos[0] = {albedo_Ndot_HI / Ndot_HI, albedo_Ndot_HeI / Ndot_HeI, albedo_Ndot_HeII / Ndot_HeII};
    ion_stars.cosines[0] = {cosine_Ndot_HI / Ndot_HI, cosine_Ndot_HeI / Ndot_HeI, cosine_Ndot_HeII / Ndot_HeII};
  }

  // Calculate HI luminosity and photon rate
  double L_stars_HI = 0., Ndot_stars_HI = 0., Ndot2_stars_HI = 0.;
  #pragma omp parallel for reduction(+:L_stars_HI, Ndot_stars_HI, Ndot2_stars_HI)
  for (int i_bin = HI_bin; i_bin < n_bins; ++i_bin) {
    L_stars_HI += bin_L_stars[i_bin];        // Total luminosity [erg/s]
    Ndot_stars_HI += bin_Ndot_stars[i_bin];  // Total rate [photons/s]
    Ndot2_stars_HI += bin_Ndot2_stars[i_bin]; // Total rate^2 [photons^2/s^2]
    bin_n_stars_eff[i_bin] = calc_n_eff(bin_Ndot_stars[i_bin], bin_Ndot2_stars[i_bin]);
  }
  ion_stars.HI.L = L_stars_HI;               // Total luminosity [erg/s]
  ion_stars.HI.Ndot = Ndot_stars_HI;         // Total rate [photons/s]
  n_stars_eff_HI = calc_n_eff(Ndot_stars_HI, Ndot2_stars_HI); // Effective number of stars: <Ndot>^2/<Ndot^2>

  // Calculate total luminosity and photon rate
  double L_stars = 0., Ndot_stars = 0., Ndot2_stars = 0.;
  #pragma omp parallel for reduction(+:L_stars, Ndot_stars, Ndot2_stars)
  for (int i_bin = 0; i_bin < HI_bin; ++i_bin) {
    L_stars += bin_L_stars[i_bin];           // Total luminosity [erg/s]
    Ndot_stars += bin_Ndot_stars[i_bin];     // Total rate [photons/s]
    Ndot2_stars += bin_Ndot2_stars[i_bin];   // Total rate^2 [photons^2/s^2]
    bin_n_stars_eff[i_bin] = calc_n_eff(bin_Ndot_stars[i_bin], bin_Ndot2_stars[i_bin]);
  }
  L_stars += L_stars_HI;                     // Combine partial sums (HI)
  Ndot_stars += Ndot_stars_HI;
  Ndot2_stars += Ndot2_stars_HI;
  ion_stars.L = L_stars;                     // Total luminosity [erg/s]
  ion_stars.Ndot = Ndot_stars;               // Total rate [photons/s]
  n_stars_eff = calc_n_eff(Ndot_stars, Ndot2_stars); // Effective number of stars: <Ndot>^2/<Ndot^2>

  // Calculate group and subhalo properties
  if constexpr (output_groups) {
    if (n_groups > 0) {
      add_Ndot_stars_halo(Ndot_stars_grp, Ndot2_stars_grp, n_stars_eff_grp, Ndot_stars_HI_grp, Ndot2_stars_HI_grp,
                          n_stars_eff_HI_grp, bin_Ndot_stars_grp, bin_Ndot2_stars_grp, bin_n_stars_eff_grp, bin_Ndot_grp);
      if (output_grp_vir)
        add_Ndot_stars_halo(Ndot_stars_grp_vir, Ndot2_stars_grp_vir, n_stars_eff_grp_vir, Ndot_stars_HI_grp_vir, Ndot2_stars_HI_grp_vir,
                            n_stars_eff_HI_grp_vir, bin_Ndot_stars_grp_vir, bin_Ndot2_stars_grp_vir, bin_n_stars_eff_grp_vir, bin_Ndot_grp_vir);
      if (output_grp_gal)
        add_Ndot_stars_halo(Ndot_stars_grp_gal, Ndot2_stars_grp_gal, n_stars_eff_grp_gal, Ndot_stars_HI_grp_gal, Ndot2_stars_HI_grp_gal,
                            n_stars_eff_HI_grp_gal, bin_Ndot_stars_grp_gal, bin_Ndot2_stars_grp_gal, bin_n_stars_eff_grp_gal, bin_Ndot_grp_gal);
      if (focus_groups_on_emission) {
        for (int i = 0; i < n_groups; ++i) {
          const double _1_Ndot_grp = safe_division(1., Ndot_stars_grp[i]); // Group normalization factor
          r_light_grp[i] *= _1_Ndot_grp;     // Center of light position [cm]
          v_light_grp[i] *= _1_Ndot_grp;     // Center of light velocity [cm/s]
        }
      }
    }
    if (n_ugroups > 0)
      add_Ndot_stars_halo(Ndot_stars_ugrp, Ndot2_stars_ugrp, n_stars_eff_ugrp, Ndot_stars_HI_ugrp, Ndot2_stars_HI_ugrp,
                          n_stars_eff_HI_ugrp, bin_Ndot_stars_ugrp, bin_Ndot2_stars_ugrp, bin_n_stars_eff_ugrp, bin_Ndot_ugrp);
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {
      add_Ndot_stars_halo(Ndot_stars_sub, Ndot2_stars_sub, n_stars_eff_sub, Ndot_stars_HI_sub, Ndot2_stars_HI_sub,
                          n_stars_eff_HI_sub, bin_Ndot_stars_sub, bin_Ndot2_stars_sub, bin_n_stars_eff_sub, bin_Ndot_sub);
      if (output_sub_vir)
        add_Ndot_stars_halo(Ndot_stars_sub_vir, Ndot2_stars_sub_vir, n_stars_eff_sub_vir, Ndot_stars_HI_sub_vir, Ndot2_stars_HI_sub_vir,
                            n_stars_eff_HI_sub_vir, bin_Ndot_stars_sub_vir, bin_Ndot2_stars_sub_vir, bin_n_stars_eff_sub_vir, bin_Ndot_sub_vir);
      if (output_sub_gal)
        add_Ndot_stars_halo(Ndot_stars_sub_gal, Ndot2_stars_sub_gal, n_stars_eff_sub_gal, Ndot_stars_HI_sub_gal, Ndot2_stars_HI_sub_gal,
                            n_stars_eff_HI_sub_gal, bin_Ndot_stars_sub_gal, bin_Ndot2_stars_sub_gal, bin_n_stars_eff_sub_gal, bin_Ndot_sub_gal);
      if (focus_subhalos_on_emission) {
        for (int i = 0; i < n_subhalos; ++i) {
          const double _1_Ndot_sub = safe_division(1., Ndot_stars_sub[i]); // Subhalo normalization factor
          r_light_sub[i] *= _1_Ndot_sub;     // Center of light position [cm]
          v_light_sub[i] *= _1_Ndot_sub;     // Center of light velocity [cm/s]
        }
      }
    }
    if (n_usubhalos > 0)
      add_Ndot_stars_halo(Ndot_stars_usub, Ndot2_stars_usub, n_stars_eff_usub, Ndot_stars_HI_usub, Ndot2_stars_HI_usub,
                          n_stars_eff_HI_usub, bin_Ndot_stars_usub, bin_Ndot2_stars_usub, bin_n_stars_eff_usub, bin_Ndot_usub);
  }

  if (output_stars) {
    Ndot_int_stars.resize(n_stars);          // Intrinsic emission [photons/s]
    #pragma omp parallel for
    for (int i = 0; i < n_stars; ++i)
      Ndot_int_stars[i] = j_cdf_stars[i];    // Copy before cdf conversion
  }

  if (Ndot_stars <= 0.)
    root_error("Total stellar photon rate must be positive");
  setup_bin_cdf(ion_stars);                  // Set up frequency cdf
  const double Ndot_norm = 1. / Ndot_stars;
  const double n_photons_norm = 1. / double(n_photons);
  if (j_exp == 1.) {                         // No luminosity boosting
    for (int i = 1; i < n_stars; ++i)
      j_cdf_stars[i] += j_cdf_stars[i - 1];  // Turn the pdf into a cdf
    #pragma omp parallel for
    for (int i = 0; i < n_stars; ++i) {
      j_cdf_stars[i] *= Ndot_norm;           // Normalize cdf
      j_weights_stars[i] = n_photons_norm;   // Equal weights
    }
  } else {                                   // Apply luminosity boosting
    #pragma omp parallel for
    for (int i = 0; i < n_stars; ++i) {
      j_weights_stars[i] = j_cdf_stars[i] * Ndot_norm; // Copy the normalized pdf
      if (j_weights_stars[i] > 0.)           // Apply power law boost
        j_cdf_stars[i] = pow(j_weights_stars[i], j_exp); // pdf^j_exp (unnormalized)
      else
        j_cdf_stars[i] = 0.;                 // No probability
    }
    const double cdf_norm = 1. / omp_sum(j_cdf_stars);
    const double dn_photons = double(n_photons);
    #pragma omp parallel for
    for (int i = 0; i < n_stars; ++i) {
      j_cdf_stars[i] *= cdf_norm;            // pdf^j_exp (normalized)
      if (j_cdf_stars[i] > 0.)
        j_weights_stars[i] /= j_cdf_stars[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
    }
    for (int i = 1; i < n_stars; ++i)
      j_cdf_stars[i] += j_cdf_stars[i - 1];  // Turn the pdf into a cdf
  }

  // Populate a lookup table to quickly find cdf indices (stars)
  const double half_cdf = 0.5 / double(n_cdf_stars); // Half interval width
  for (int i = 0, star = 0; i < n_cdf_stars; ++i) {
    const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
    while (j_cdf_stars[star] <= frac_cdf && star < n_stars - 1)
      star++;                                // Move to the right bin ( --> )
    j_map_stars[i] = star;                   // Lookup table for insertion
  }
}

/* Star setup that is performed during each iteration. */
void Ionization::iterative_setup_stars() {
  // Weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
  auto& ig = ion_global;                     // Global ionization data
  auto& is = ion_stars;                      // Stellar ionization data
  #pragma omp parallel for
  for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
    const double bin_Ndot = bin_Ndot_stars[i_bin]; // Bin Ndot
    if (bin_Ndot <= 0.)
      continue;                              // Skip addition by zero
    bin_L_tot[i_bin] += bin_L_stars[i_bin];  // Add to the total bin values
    bin_Ndot_tot[i_bin] += bin_Ndot;         // Photon rate [photons/s]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      ig.sigma_ions[ai][i_bin] += bin_Ndot * is.sigma_ions[ai][i_bin]; // Cross-section [cm^2]
      ig.epsilon_ions[ai][i_bin] += (is.epsilon_ions[ai][i_bin] + ions_erg[ai]) * bin_Ndot * is.sigma_ions[ai][i_bin]; // Photoheating [erg]
    }
    for (int i = 0; i < n_dust_species; ++i) {
      ig.kappas[i][i_bin] += bin_Ndot * is.kappas[i][i_bin]; // Dust opacity [cm^2/g dust]
      ig.albedos[i][i_bin] += bin_Ndot * is.albedos[i][i_bin]; // Dust albedo for scattering vs. absorption
      ig.cosines[i][i_bin] += bin_Ndot * is.cosines[i][i_bin]; // Anisotropy parameter: <μ> for dust scattering
    }
  }

  // Group and subhalo properties
  if constexpr (output_groups) {
    if (n_groups > 0) {
      update_Ndot_halo(bin_Ndot_stars_grp, bin_Ndot_grp);
      if (output_grp_vir)
        update_Ndot_halo(bin_Ndot_stars_grp_vir, bin_Ndot_grp_vir);
      if (output_grp_gal)
        update_Ndot_halo(bin_Ndot_stars_grp_gal, bin_Ndot_grp_gal);
    }
    if (n_ugroups > 0)
      update_Ndot_halo(bin_Ndot_stars_ugrp, bin_Ndot_ugrp);
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {
      update_Ndot_halo(bin_Ndot_stars_sub, bin_Ndot_sub);
      if (output_sub_vir)
        update_Ndot_halo(bin_Ndot_stars_sub_vir, bin_Ndot_sub_vir);
      if (output_sub_gal)
        update_Ndot_halo(bin_Ndot_stars_sub_gal, bin_Ndot_sub_gal);
    }
    if (n_usubhalos > 0)
      update_Ndot_halo(bin_Ndot_stars_usub, bin_Ndot_usub);
  }
}

/* Gas setup that is performed once. */
void Ionization::initial_setup_gas() {
  // Allocate the pdf based on cell photon rates
  j_cdf_cells.resize(n_cells);               // Cumulative distribution function
  j_weights_cells.resize(n_cells);           // Cell luminosity boosting weights
  // Allocate the gas luminosities and spectral properties
  auto& ic = ion_gas;                        // Gas ionization data
  bin_L_gas = vector<double>(n_bins);        // Gas bin luminosities [erg/s]
  bin_Ndot_gas = vector<double>(n_bins);     // Gas bin rates [photons/s]
  bin_Ndot2_gas = vector<double>(n_bins);    // Gas bin rates^2 [photons^2/s^2]
  bin_n_cells_eff = vector<double>(n_bins);  // Bin effective number of cells: 1/<w>
  ic.sigma_ions.resize(n_active_ions);       // Species ionization cross-sections [cm^2]
  for (auto& sigma_ion : ic.sigma_ions)      // Species allocations
    sigma_ion = vector<double>(n_bins);
  ic.epsilon_ions.resize(n_active_ions);     // Species ionization heating [erg]
  for (auto& epsilon_ion : ic.epsilon_ions)  // Species allocations
    epsilon_ion = vector<double>(n_bins);
  ic.mean_energy = vector<double>(n_bins);   // Mean energy of each frequency bin [erg]
  for (int i = 0; i < n_dust_species; ++i) { // Dust species
    ic.kappas[i] = vector<double>(n_bins);   // Dust opacity [cm^2/g dust]
    ic.albedos[i] = vector<double>(n_bins);  // Dust albedo for scattering vs. absorption
    ic.cosines[i] = vector<double>(n_bins);  // Anisotropy parameter: <μ> for dust scattering
  }

  // Allocate group and subhalo properties
  if constexpr (output_groups) {
    if (n_groups > 0) {                      // Have groups
      Ndot_gas_grp = vector<double>(n_groups); // Group gas photon rates [photons/s]
      Ndot2_gas_grp = vector<double>(n_groups); // Group gas rates^2 [photons^2/s^2]
      n_cells_eff_grp = vector<double>(n_groups); // Effective number of cells: 1/<w>
      Ndot_gas_HI_grp = vector<double>(n_groups); // Group HI gas photon rates [photons/s]
      Ndot2_gas_HI_grp = vector<double>(n_groups); // Group HI gas rates^2 [photons^2/s^2]
      n_cells_eff_HI_grp = vector<double>(n_groups); // Effective number of cells: 1/<w>
      bin_Ndot_gas_grp = Image(n_groups, n_bins); // Group bin gas photon rates [photons/s]
      bin_Ndot2_gas_grp = Image(n_groups, n_bins); // Group bin gas rates^2 [photons^2/s^2]
      bin_n_cells_eff_grp = Image(n_groups, n_bins); // Effective number of cells: 1/<w>
      if (output_grp_vir) {
        Ndot_gas_grp_vir = vector<double>(n_groups); // Group virial gas photon rates [photons/s]
        Ndot2_gas_grp_vir = vector<double>(n_groups); // Group virial gas rates^2 [photons^2/s^2]
        n_cells_eff_grp_vir = vector<double>(n_groups); // Effective number of cells: 1/<w>
        Ndot_gas_HI_grp_vir = vector<double>(n_groups); // Group virial HI gas photon rates [photons/s]
        Ndot2_gas_HI_grp_vir = vector<double>(n_groups); // Group virial HI gas rates^2 [photons^2/s^2]
        n_cells_eff_HI_grp_vir = vector<double>(n_groups); // Effective number of cells: 1/<w>
        bin_Ndot_gas_grp_vir = Image(n_groups, n_bins); // Group virial bin gas photon rates [photons/s]
        bin_Ndot2_gas_grp_vir = Image(n_groups, n_bins); // Group virial bin gas rates^2 [photons^2/s^2]
        bin_n_cells_eff_grp_vir = Image(n_groups, n_bins); // Effective number of cells: 1/<w>
      }
      if (output_grp_gal) {
        Ndot_gas_grp_gal = vector<double>(n_groups); // Group galaxy gas photon rates [photons/s]
        Ndot2_gas_grp_gal = vector<double>(n_groups); // Group galaxy gas rates^2 [photons^2/s^2]
        n_cells_eff_grp_gal = vector<double>(n_groups); // Effective number of cells: 1/<w>
        Ndot_gas_HI_grp_gal = vector<double>(n_groups); // Group galaxy HI gas photon rates [photons/s]
        Ndot2_gas_HI_grp_gal = vector<double>(n_groups); // Group galaxy HI gas rates^2 [photons^2/s^2]
        n_cells_eff_HI_grp_gal = vector<double>(n_groups); // Effective number of cells: 1/<w>
        bin_Ndot_gas_grp_gal = Image(n_groups, n_bins); // Group galaxy bin gas photon rates [photons/s]
        bin_Ndot2_gas_grp_gal = Image(n_groups, n_bins); // Group galaxy bin gas rates^2 [photons^2/s^2]
        bin_n_cells_eff_grp_gal = Image(n_groups, n_bins); // Effective number of cells: 1/<w>
      }
    }
    if (n_ugroups > 0) {                     // Have unfiltered groups
      Ndot_gas_ugrp = vector<double>(n_ugroups); // Unfiltered group gas photon rates [photons/s]
      Ndot2_gas_ugrp = vector<double>(n_ugroups); // Unfiltered group gas rates^2 [photons^2/s^2]
      n_cells_eff_ugrp = vector<double>(n_ugroups); // Effective number of cells: 1/<w>
      Ndot_gas_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI gas photon rates [photons/s]
      Ndot2_gas_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI gas rates^2 [photons^2/s^2]
      n_cells_eff_HI_ugrp = vector<double>(n_ugroups); // Effective number of cells: 1/<w>
      bin_Ndot_gas_ugrp = Image(n_ugroups, n_bins); // Unfiltered group bin gas photon rates [photons/s]
      bin_Ndot2_gas_ugrp = Image(n_ugroups, n_bins); // Unfiltered group bin gas rates^2 [photons^2/s^2]
      bin_n_cells_eff_ugrp = Image(n_ugroups, n_bins); // Effective number of cells: 1/<w>
    }
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {                    // Have subhalos
      Ndot_gas_sub = vector<double>(n_subhalos); // Subhalo gas photon rates [photons/s]
      Ndot2_gas_sub = vector<double>(n_subhalos); // Subhalo gas rates^2 [photons^2/s^2]
      n_cells_eff_sub = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
      Ndot_gas_HI_sub = vector<double>(n_subhalos); // Subhalo HI gas photon rates [photons/s]
      Ndot2_gas_HI_sub = vector<double>(n_subhalos); // Subhalo HI gas rates^2 [photons^2/s^2]
      n_cells_eff_HI_sub = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
      bin_Ndot_gas_sub = Image(n_subhalos, n_bins); // Subhalo bin gas photon rates [photons/s]
      bin_Ndot2_gas_sub = Image(n_subhalos, n_bins); // Subhalo bin gas rates^2 [photons^2/s^2]
      bin_n_cells_eff_sub = Image(n_subhalos, n_bins); // Effective number of cells: 1/<w>
      if (output_sub_vir) {
        Ndot_gas_sub_vir = vector<double>(n_subhalos); // Subhalo virial gas photon rates [photons/s]
        Ndot2_gas_sub_vir = vector<double>(n_subhalos); // Subhalo virial gas rates^2 [photons^2/s^2]
        n_cells_eff_sub_vir = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
        Ndot_gas_HI_sub_vir = vector<double>(n_subhalos); // Subhalo virial HI gas photon rates [photons/s]
        Ndot2_gas_HI_sub_vir = vector<double>(n_subhalos); // Subhalo virial HI gas rates^2 [photons^2/s^2]
        n_cells_eff_HI_sub_vir = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
        bin_Ndot_gas_sub_vir = Image(n_subhalos, n_bins); // Subhalo virial bin gas photon rates [photons/s]
        bin_Ndot2_gas_sub_vir = Image(n_subhalos, n_bins); // Subhalo virial bin gas rates^2 [photons^2/s^2]
        bin_n_cells_eff_sub_vir = Image(n_subhalos, n_bins); // Effective number of cells: 1/<w>
      }
      if (output_sub_gal) {
        Ndot_gas_sub_gal = vector<double>(n_subhalos); // Subhalo galaxy gas photon rates [photons/s]
        Ndot2_gas_sub_gal = vector<double>(n_subhalos); // Subhalo galaxy gas rates^2 [photons^2/s^2]
        n_cells_eff_sub_gal = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
        Ndot_gas_HI_sub_gal = vector<double>(n_subhalos); // Subhalo galaxy HI gas photon rates [photons/s]
        Ndot2_gas_HI_sub_gal = vector<double>(n_subhalos); // Subhalo galaxy HI gas rates^2 [photons^2/s^2]
        n_cells_eff_HI_sub_gal = vector<double>(n_subhalos); // Effective number of cells: 1/<w>
        bin_Ndot_gas_sub_gal = Image(n_subhalos, n_bins); // Subhalo galaxy bin gas photon rates [photons/s]
        bin_Ndot2_gas_sub_gal = Image(n_subhalos, n_bins); // Subhalo galaxy bin gas rates^2 [photons^2/s^2]
        bin_n_cells_eff_sub_gal = Image(n_subhalos, n_bins); // Effective number of cells: 1/<w>
      }
    }
    if (n_usubhalos > 0) {                   // Have unfiltered subhalos
      Ndot_gas_usub = vector<double>(n_usubhalos); // Unfiltered subhalo gas photon rates [photons/s]
      Ndot2_gas_usub = vector<double>(n_usubhalos); // Unfiltered subhalo gas rates^2 [photons^2/s^2]
      n_cells_eff_usub = vector<double>(n_usubhalos); // Effective number of cells: 1/<w>
      Ndot_gas_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI gas photon rates [photons/s]
      Ndot2_gas_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI gas rates^2 [photons^2/s^2]
      n_cells_eff_HI_usub = vector<double>(n_usubhalos); // Effective number of cells: 1/<w>
      bin_Ndot_gas_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo bin gas photon rates [photons/s]
      bin_Ndot2_gas_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo bin gas rates^2 [photons^2/s^2]
      bin_n_cells_eff_usub = Image(n_usubhalos, n_bins); // Effective number of cells: 1/<w>
    }
  }
}

/* Gas setup that is performed during each iteration. */
void Ionization::iterative_setup_gas() {
  // Reset certain gas quantities
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    bin_L_gas[i] = 0.;                       // Gas bin luminosities [erg/s]
    bin_Ndot_gas[i] = 0.;                    // Gas bin rates [photons/s]
    bin_Ndot2_gas[i] = 0.;                   // Gas bin rates^2 [photons^2/s^2]
  }

  // Reset group and subhalo properties
  const bool save_groups = output_groups && n_groups > 0;
  const bool save_ugroups = output_groups && n_ugroups > 0;
  const bool save_subhalos = output_subhalos && n_subhalos > 0;
  const bool save_usubhalos = output_subhalos && n_usubhalos > 0;
  const bool need_group_indices = save_groups || save_ugroups;
  const bool need_subhalo_indices = save_subhalos || save_usubhalos;
  if (save_groups) {                         // Have groups
    const int n_groups_bins = n_groups * n_bins;
    #pragma omp parallel for
    for (int i = 0; i < n_groups_bins; ++i) {
      bin_Ndot_gas_grp[i] = 0.;              // Group bin gas photon rates [photons/s]
      bin_Ndot2_gas_grp[i] = 0.;             // Group bin gas rates^2 [photons^2/s^2]
    }
  }
  if (save_ugroups) {                        // Have unfiltered groups
    const int n_ugroups_bins = n_ugroups * n_bins;
    #pragma omp parallel for
    for (int i = 0; i < n_ugroups_bins; ++i) {
      bin_Ndot_gas_ugrp[i] = 0.;             // Unfiltered group bin gas photon rates [photons/s]
      bin_Ndot2_gas_ugrp[i] = 0.;            // Unfiltered group bin gas rates^2 [photons^2/s^2]
    }
  }
  if (save_subhalos) {                       // Have subhalos
    const int n_subhalos_bins = n_subhalos * n_bins;
    #pragma omp parallel for
    for (int i = 0; i < n_subhalos_bins; ++i) {
      bin_Ndot_gas_sub[i] = 0.;              // Subhalo bin gas photon rates [photons/s]
      bin_Ndot2_gas_sub[i] = 0.;             // Subhalo bin gas rates^2 [photons^2/s^2]
    }
  }
  if (save_usubhalos) {                      // Have unfiltered subhalos
    const int n_usubhalos_bins = n_usubhalos * n_bins;
    #pragma omp parallel for
    for (int i = 0; i < n_usubhalos_bins; ++i) {
      bin_Ndot_gas_usub[i] = 0.;             // Unfiltered subhalo bin gas photon rates [photons/s]
      bin_Ndot2_gas_usub[i] = 0.;            // Unfiltered subhalo bin gas rates^2 [photons^2/s^2]
    }
  }

  // Rate weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
  Image sigma_Ndot_ions_gas(n_active_ions, n_bins);
  Image sigma_L_ions_gas(n_active_ions, n_bins);
  vector<double> linear_Ndot_gas(n_bins);
  dust_vectors kappas_Ndot_gas, albedos_Ndot_gas, cosines_Ndot_gas;
  for (int j = 0; j < n_dust_species; ++j) {
    kappas_Ndot_gas[j] = vector<double>(n_bins);
    albedos_Ndot_gas[j] = vector<double>(n_bins);
    cosines_Ndot_gas[j] = vector<double>(n_bins);
  }
  #pragma omp parallel for reduction(vec_plus:bin_L_gas, bin_Ndot_gas, bin_Ndot2_gas, linear_Ndot_gas) \
                           reduction(image_plus:sigma_Ndot_ions_gas, sigma_L_ions_gas) \
                           reduction(dust_plus:kappas_Ndot_gas, albedos_Ndot_gas, cosines_Ndot_gas)
  for (int i = 0; i < n_cells; ++i) {
    // Avoid cells that are special or outside the emission region
    if (invalid_cell(i)) {
      j_cdf_cells[i] = 0.;                   // No emission
      continue;
    }

    // Local cell properties
    double local_Ndot = 0.;                  // Needed for j_cdf_cells
    const Vec3 r_i = cell_center(i);         // Position [cm]
    const double T_i = T[i];                 // Temperature [K]
    const double ne = x_e[i] * n_H[i];       // Electron density (n_e)
    const double ne_V = ne * VOLUME(i);      // n_e V
    const double nHII = x_HII[i] * n_H[i];   // n_HII
    const double n2V_HII = nHII * ne_V;      // n_e n_HII V
    double n2V_HeII = 0., n2V_HeIII = 0.;    // n_e n_{HeII,HeIII} V
    if (read_HeI) {
      const double n2V_He = n_He[i] * ne_V;  // n_e n_He V
      if (read_HeII) {
        n2V_HeII = x_HeII[i] * n2V_He;       // n_e n_HeII V
        n2V_HeIII = (1. - x_HeI[i] - x_HeII[i]) * n2V_He; // n_e n_HeIII V
      } else {
        n2V_HeII = (1. - x_HeI[i]) * n2V_He; // n_e n_HeIII V
      }
    }

    // Local group and subhalo indices
    int i_grp = -1, i_ugrp = -1, i_sub = -1, i_usub = -1;
    bool in_grp_vir = false, in_grp_gal = false, in_sub_vir = false, in_sub_gal = false;
    if (need_group_indices) {
      const int grp_id = group_id_cell[i]; // IDs
      if (save_groups)
        i_grp = get_index_sorted(grp_id, group_id); // Group ID index
      if (save_ugroups)
        i_ugrp = get_index_sorted(grp_id, ugroup_id); // Unfiltered group ID index
      if (i_grp >= 0) {
        if (output_grp_vir)
          in_grp_vir = norm2(r_i - r_grp_vir[i_grp]) < R2_grp_vir[i_grp];
        if (output_grp_gal)
          in_grp_gal = norm2(r_i - r_grp_gal[i_grp]) < R2_grp_gal[i_grp];
      }
    }
    if (need_subhalo_indices) {
      const int sub_id = subhalo_id_cell[i]; // IDs
      if (save_subhalos)
        i_sub = get_index_sorted(sub_id, subhalo_id); // Subhalo ID index
      if (save_usubhalos)
        i_usub = get_index_sorted(sub_id, usubhalo_id); // Unfiltered subhalo ID index
      if (i_sub >= 0) {
        if (output_sub_vir)
          in_sub_vir = norm2(r_i - r_sub_vir[i_sub]) < R2_sub_vir[i_sub];
        if (output_sub_gal)
          in_sub_gal = norm2(r_i - r_sub_gal[i_sub]) < R2_sub_gal[i_sub];
      }
    }

    // Free-free continuum emission (HII, HeII, HeIII)
    if (free_free) {
      // Free-free emission (Z = 1)
      int i_L_T = 0;                         // Lower interpolation index
      double frac_R_T = 0.;                  // Upper interpolation fraction
      if (T_i >= spec_ff_Z1.max_T) {         // Temperature maximum
        i_L_T = spec_ff_Z1.n_Ts - 2;         // Temperature bins - 2
        frac_R_T = 1.;
      } else if (T_i > spec_ff_Z1.min_T) {   // Temperature minimum
        const double logT = log10(T_i);      // Interpolate in log space
        i_L_T = floor(spec_ff_Z1.inv_dlogT * (logT - spec_ff_Z1.min_logT));
        frac_R_T = spec_ff_Z1.inv_dlogT * (logT - spec_ff_Z1.min_logT) - double(i_L_T);
      }

      // Linear interpolation (based on left and right fractions)
      const int i_R_T = i_L_T + 1;
      const double frac_L_T = 1. - frac_R_T;
      const double n2V_Z1 = n2V_HII + n2V_HeII; // n_e n_ions V (Z = 1)
      double local_Ndot_ff = 0.;             // Needed for j_cdf_cells
      for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
        const double local_bin_Ndot_ff = EVAL_FF_Z1(Ndot);
        const double local_bin_Ndot2_ff = local_bin_Ndot_ff * local_bin_Ndot_ff;
        const double local_bin_L_ff = EVAL_FF_Z1(L);
        local_Ndot_ff += local_bin_Ndot_ff;  // Photon rates [photons/s]
        bin_Ndot_gas[i_bin] += local_bin_Ndot_ff;
        bin_Ndot2_gas[i_bin] += local_bin_Ndot2_ff;
        bin_L_gas[i_bin] += local_bin_L_ff;  // Luminosities [erg/s]
        if (i_grp >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_ff;
          #pragma omp atomic
          bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_ff;
          if (in_grp_vir) {
            #pragma omp atomic
            bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_ff;
          }
          if (in_grp_gal) {
            #pragma omp atomic
            bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_ff;
          }
        } else if (i_ugrp >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_ff;
          #pragma omp atomic
          bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_ff;
        }
        if (i_sub >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_ff;
          #pragma omp atomic
          bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_ff;
          if (in_sub_vir) {
            #pragma omp atomic
            bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_ff;
          }
          if (in_sub_gal) {
            #pragma omp atomic
            bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_ff;
          }
        } else if (i_usub >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_ff;
          #pragma omp atomic
          bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_ff;
        }
        for (int ai = 0; ai < n_active_ions; ++ai) {
          // Cross-section rates [cm^2 photons/s]
          auto& log_sigma_Ndot_ion = spec_ff_Z1.log_sigma_Ndot_ions[ai];
          double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T]*frac_L_T
                                    + log_sigma_Ndot_ion[i_bin][i_R_T]*frac_R_T;
          sigma_Ndot_ions_gas(ai, i_bin) += n2V_Z1 * pow(10., log_sigma_Ndot_val);
          // Photoheating rates [cm^2 erg/s]
          auto& log_sigma_L_ion = spec_ff_Z1.log_sigma_L_ions[ai];
          double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T]*frac_L_T
                                 + log_sigma_L_ion[i_bin][i_R_T]*frac_R_T;
          sigma_L_ions_gas(ai, i_bin) += n2V_Z1 * pow(10., log_sigma_L_val);
        }
        for (int j = 0; j < n_dust_species; ++j) {
          kappas_Ndot_gas[j][i_bin] += EVAL_FF_Z1(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
          albedos_Ndot_gas[j][i_bin] += EVAL_FF_Z1(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
          cosines_Ndot_gas[j][i_bin] += COS_NDOT_FF(Z1); // Dust scattering anisotropy rates [photons/s]
        }
        linear_Ndot_gas[i_bin] += LIN_NDOT_FF(Z1); // Normalization for cosine rates [photons/s]
      }
      // Free-free emission (Z = 2)
      if (read_HeII) {
        const double n2V_Z2 = n2V_HeIII;     // n_e n_ions V (Z = 2)
        for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
          const double local_bin_Ndot_ff = EVAL_FF_Z2(Ndot);
          const double local_bin_Ndot2_ff = local_bin_Ndot_ff * local_bin_Ndot_ff;
          const double local_bin_L_ff = EVAL_FF_Z2(L);
          local_Ndot_ff += local_bin_Ndot_ff; // Photon rates [photons/s]
          bin_Ndot_gas[i_bin] += local_bin_Ndot_ff;
          bin_Ndot2_gas[i_bin] += local_bin_Ndot2_ff;
          bin_L_gas[i_bin] += local_bin_L_ff; // Luminosities [erg/s]
          if (i_grp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_ff;
            if (in_grp_vir) {
              #pragma omp atomic
              bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_ff;
              #pragma omp atomic
              bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_ff;
            }
            if (in_grp_gal) {
              #pragma omp atomic
              bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_ff;
              #pragma omp atomic
              bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_ff;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_ff;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_ff;
            if (in_sub_vir) {
              #pragma omp atomic
              bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_ff;
              #pragma omp atomic
              bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_ff;
            }
            if (in_sub_gal) {
              #pragma omp atomic
              bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_ff;
              #pragma omp atomic
              bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_ff;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_ff;
            #pragma omp atomic
            bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_ff;
          }
          for (int ai = 0; ai < n_active_ions; ++ai) {
            // Cross-section rates [cm^2 photons/s]
            auto& log_sigma_Ndot_ion = spec_ff_Z2.log_sigma_Ndot_ions[ai];
            double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T]*frac_L_T
                                      + log_sigma_Ndot_ion[i_bin][i_R_T]*frac_R_T;
            sigma_Ndot_ions_gas(ai, i_bin) += n2V_Z2 * pow(10., log_sigma_Ndot_val);
            // Photoheating rates [cm^2 erg/s]
            auto& log_sigma_L_ion = spec_ff_Z2.log_sigma_L_ions[ai];
            double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T]*frac_L_T
                                   + log_sigma_L_ion[i_bin][i_R_T]*frac_R_T;
            sigma_L_ions_gas(ai, i_bin) += n2V_Z2 * pow(10., log_sigma_L_val);
          }
          for (int j = 0; j < n_dust_species; ++j) {
            kappas_Ndot_gas[j][i_bin] += EVAL_FF_Z2(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
            albedos_Ndot_gas[j][i_bin] += EVAL_FF_Z2(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
            cosines_Ndot_gas[j][i_bin] += COS_NDOT_FF(Z2); // Dust scattering anisotropy rates [photons/s]
          }
          linear_Ndot_gas[i_bin] += LIN_NDOT_FF(Z2); // Normalization for cosine rates [photons/s]
        }
      }
      local_Ndot += local_Ndot_ff;           // Photon rates [photons/s]
    }

    // Free-bound continuum emission (HII->HI, HeII->HeI, HeIII->HeII)
    if (free_bound) {
      int i_L_T = 0;                         // Lower interpolation index
      double frac_R_T = 0.;                  // Upper interpolation fraction
      if (T_i >= spec_fb_HI.max_T) {         // Temperature maximum
        i_L_T = spec_fb_HI.n_Ts - 2;         // Temperature bins - 2
        frac_R_T = 1.;
      } else if (T_i > spec_fb_HI.min_T) {   // Temperature minimum
        const double logT = log10(T_i);      // Interpolate in log space
        i_L_T = floor(spec_fb_HI.inv_dlogT * (logT - spec_fb_HI.min_logT));
        frac_R_T = spec_fb_HI.inv_dlogT * (logT - spec_fb_HI.min_logT) - double(i_L_T);
      }

      // Linear interpolation (based on left and right fractions)
      const int i_R_T = i_L_T + 1;
      const double frac_L_T = 1. - frac_R_T;
      double local_Ndot_fb = 0.;             // Needed for j_cdf_cells
      for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
        const double local_bin_Ndot_fb = EVAL_FB_HI(Ndot);
        const double local_bin_Ndot2_fb = local_bin_Ndot_fb * local_bin_Ndot_fb;
        const double local_bin_L_fb = EVAL_FB_HI(L);
        local_Ndot_fb += local_bin_Ndot_fb;  // Photon rates [photons/s]
        bin_Ndot_gas[i_bin] += local_bin_Ndot_fb;
        bin_Ndot2_gas[i_bin] += local_bin_Ndot2_fb;
        bin_L_gas[i_bin] += local_bin_L_fb;  // Luminosities [erg/s]
        if (i_grp >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_fb;
          #pragma omp atomic
          bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_fb;
          if (output_grp_vir) {
            #pragma omp atomic
            bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_fb;
          }
          if (output_grp_gal) {
            #pragma omp atomic
            bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_fb;
          }
        } else if (i_ugrp >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_fb;
          #pragma omp atomic
          bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_fb;
        }
        if (i_sub >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_fb;
          #pragma omp atomic
          bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_fb;
          if (output_sub_vir) {
            #pragma omp atomic
            bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_fb;
          }
          if (output_sub_gal) {
            #pragma omp atomic
            bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_fb;
          }
        } else if (i_usub >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_fb;
          #pragma omp atomic
          bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_fb;
        }
        for (int ai = 0; ai < n_active_ions; ++ai) {
          // Cross-section rates [cm^2 photons/s]
          auto& log_sigma_Ndot_ion = spec_fb_HI.log_sigma_Ndot_ions[ai];
          double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T]*frac_L_T
                                    + log_sigma_Ndot_ion[i_bin][i_R_T]*frac_R_T;
          sigma_Ndot_ions_gas(ai, i_bin) += n2V_HII * pow(10., log_sigma_Ndot_val);
          // Photoheating rates [cm^2 erg/s]
          auto& log_sigma_L_ion = spec_fb_HI.log_sigma_L_ions[ai];
          double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T]*frac_L_T
                                 + log_sigma_L_ion[i_bin][i_R_T]*frac_R_T;
          sigma_L_ions_gas(ai, i_bin) += n2V_HII * pow(10., log_sigma_L_val);
        }
        for (int j = 0; j < n_dust_species; ++j) {
          kappas_Ndot_gas[j][i_bin] += EVAL_FB_HI(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
          albedos_Ndot_gas[j][i_bin] += EVAL_FB_HI(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
          cosines_Ndot_gas[j][i_bin] += COS_NDOT_FB(HI); // Dust scattering anisotropy rates [photons/s]
        }
        linear_Ndot_gas[i_bin] += LIN_NDOT_FB(HI); // Normalization for cosine rates [photons/s]
      }
      // Free-bound emission (HeII->HeI)
      if (read_HeI) {
        for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
          const double local_bin_Ndot_fb = EVAL_FB_HeI(Ndot);
          const double local_bin_Ndot2_fb = local_bin_Ndot_fb * local_bin_Ndot_fb;
          const double local_bin_L_fb = EVAL_FB_HeI(L);
          local_Ndot_fb += local_bin_Ndot_fb; // Photon rates [photons/s]
          bin_Ndot_gas[i_bin] += local_bin_Ndot_fb;
          bin_Ndot2_gas[i_bin] += local_bin_Ndot2_fb;
          bin_L_gas[i_bin] += local_bin_L_fb; // Luminosities [erg/s]
          if (i_grp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_fb;
            if (in_grp_vir) {
              #pragma omp atomic
              bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_fb;
            }
            if (in_grp_gal) {
              #pragma omp atomic
              bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_fb;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_fb;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_fb;
            if (in_sub_vir) {
              #pragma omp atomic
              bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_fb;
            }
            if (in_sub_gal) {
              #pragma omp atomic
              bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_fb;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_fb;
          }
          for (int ai = 0; ai < n_active_ions; ++ai) {
            // Cross-section rates [cm^2 photons/s]
            auto& log_sigma_Ndot_ion = spec_fb_HeI.log_sigma_Ndot_ions[ai];
            double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T]*frac_L_T
                                      + log_sigma_Ndot_ion[i_bin][i_R_T]*frac_R_T;
            sigma_Ndot_ions_gas(ai, i_bin) += n2V_HeII * pow(10., log_sigma_Ndot_val);
            // Photoheating rates [cm^2 erg/s]
            auto& log_sigma_L_ion = spec_fb_HeI.log_sigma_L_ions[ai];
            double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T]*frac_L_T
                                   + log_sigma_L_ion[i_bin][i_R_T]*frac_R_T;
            sigma_L_ions_gas(ai, i_bin) += n2V_HeII * pow(10., log_sigma_L_val);
          }
          for (int j = 0; j < n_dust_species; ++j) {
            kappas_Ndot_gas[j][i_bin] += EVAL_FB_HeI(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
            albedos_Ndot_gas[j][i_bin] += EVAL_FB_HeI(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
            cosines_Ndot_gas[j][i_bin] += COS_NDOT_FB(HeI); // Dust scattering anisotropy rates [photons/s]
          }
          linear_Ndot_gas[i_bin] += LIN_NDOT_FB(HeI); // Normalization for cosine rates [photons/s]
        }
      }
      // Free-bound emission (HeIII->HeII)
      if (read_HeII) {
        for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
          const double local_bin_Ndot_fb = EVAL_FB_HeII(Ndot);
          const double local_bin_Ndot2_fb = local_bin_Ndot_fb * local_bin_Ndot_fb;
          const double local_bin_L_fb = EVAL_FB_HeII(L);
          local_Ndot_fb += local_bin_Ndot_fb; // Photon rates [photons/s]
          bin_Ndot_gas[i_bin] += local_bin_Ndot_fb;
          bin_Ndot2_gas[i_bin] += local_bin_Ndot2_fb;
          bin_L_gas[i_bin] += local_bin_L_fb; // Luminosities [erg/s]
          if (i_grp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_fb;
            if (in_grp_vir) {
              #pragma omp atomic
              bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_fb;
            }
            if (in_grp_gal) {
              #pragma omp atomic
              bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_fb;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_fb;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_fb;
            if (in_sub_vir) {
              #pragma omp atomic
              bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_fb;
            }
            if (in_sub_gal) {
              #pragma omp atomic
              bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_fb;
              #pragma omp atomic
              bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_fb;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_fb;
            #pragma omp atomic
            bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_fb;
          }
          for (int ai = 0; ai < n_active_ions; ++ai) {
            // Cross-section rates [cm^2 photons/s]
            auto& log_sigma_Ndot_ion = spec_fb_HeII.log_sigma_Ndot_ions[ai];
            double log_sigma_Ndot_val = log_sigma_Ndot_ion[i_bin][i_L_T]*frac_L_T
                                      + log_sigma_Ndot_ion[i_bin][i_R_T]*frac_R_T;
            sigma_Ndot_ions_gas(ai, i_bin) += n2V_HeIII * pow(10., log_sigma_Ndot_val);
            // Photoheating rates [cm^2 erg/s]
            auto& log_sigma_L_ion = spec_fb_HeII.log_sigma_L_ions[ai];
            double log_sigma_L_val = log_sigma_L_ion[i_bin][i_L_T]*frac_L_T
                                   + log_sigma_L_ion[i_bin][i_R_T]*frac_R_T;
            sigma_L_ions_gas(ai, i_bin) += n2V_HeIII * pow(10., log_sigma_L_val);
          }
          for (int j = 0; j < n_dust_species; ++j) {
            kappas_Ndot_gas[j][i_bin] += EVAL_FB_HeII(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
            albedos_Ndot_gas[j][i_bin] += EVAL_FB_HeII(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
            cosines_Ndot_gas[j][i_bin] += COS_NDOT_FB(HeII); // Dust scattering anisotropy rates [photons/s]
          }
          linear_Ndot_gas[i_bin] += LIN_NDOT_FB(HeII); // Normalization for cosine rates [photons/s]
        }
      }
      local_Ndot += local_Ndot_fb;           // Photon rates [photons/s]
    }

    // Two-photon continuum emission (HII->HI, HeII->HeI, HeIII->HeII)
    if (two_photon) {
      const double HI_2p_n2V = n2V_HII * HI_2p_rate(T_i, ne, nHII);
      double local_Ndot_2p = 0.;             // Needed for j_cdf_cells
      for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
        const double local_bin_Ndot_2p = EVAL_2P_HI(Ndot);
        const double local_bin_Ndot2_2p = local_bin_Ndot_2p * local_bin_Ndot_2p;
        const double local_bin_L_2p = EVAL_2P_HI(L);
        local_Ndot_2p += local_bin_Ndot_2p;  // Photon rates [photons/s]
        bin_Ndot_gas[i_bin] += local_bin_Ndot_2p;
        bin_Ndot2_gas[i_bin] += local_bin_Ndot2_2p;
        bin_L_gas[i_bin] += local_bin_L_2p;  // Luminosities [erg/s]
        if (i_grp >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_2p;
          #pragma omp atomic
          bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_2p;
          if (in_grp_vir) {
            #pragma omp atomic
            bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_2p;
          }
          if (in_grp_gal) {
            #pragma omp atomic
            bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_2p;
          }
        } else if (i_ugrp >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_2p;
          #pragma omp atomic
          bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_2p;
        }
        if (i_sub >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_2p;
          #pragma omp atomic
          bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_2p;
          if (in_sub_vir) {
            #pragma omp atomic
            bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_2p;
          }
          if (in_sub_gal) {
            #pragma omp atomic
            bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_2p;
          }
        } else if (i_usub >= 0) {
          #pragma omp atomic
          bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_2p;
          #pragma omp atomic
          bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_2p;
        }
        for (int ai = 0; ai < n_active_ions; ++ai) {
          // Cross-section rates [cm^2 photons/s]
          auto& sigma_Ndot_ion = spec_2p_HI.sigma_Ndot_ions[ai];
          sigma_Ndot_ions_gas(ai, i_bin) += HI_2p_n2V * sigma_Ndot_ion[i_bin];
          // Photoheating rates [cm^2 erg/s]
          auto& sigma_L_ion = spec_2p_HI.sigma_L_ions[ai];
          sigma_L_ions_gas(ai, i_bin) += HI_2p_n2V * sigma_L_ion[i_bin];
        }
        for (int j = 0; j < n_dust_species; ++j) {
          kappas_Ndot_gas[j][i_bin] += EVAL_2P_HI(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
          albedos_Ndot_gas[j][i_bin] += EVAL_2P_HI(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
          cosines_Ndot_gas[j][i_bin] += EVAL_2P_HI(cosines_Ndot[j]); // Dust scattering anisotropy rates [photons/s]
        }
      }
      // Two-photon emission (HeII->HeI)
      // if (read_HeI) {
      //   const double HeI_2p_n2V = n2V_HeII * HeI_2p_rate(T_i, ne, nHII);
      //   for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
      //     const double local_bin_Ndot_2p = EVAL_2P_HeI(Ndot);
      //     const double local_bin_Ndot2_2p = local_bin_Ndot_2p * local_bin_Ndot_2p;
      //     const double local_bin_L_2p = EVAL_2P_HeI(L);
      //     local_Ndot_2p += local_bin_Ndot_2p; // Photon rates [photons/s]
      //     bin_Ndot_gas[i_bin] += local_bin_Ndot_2p;
      //     bin_Ndot2_gas[i_bin] += local_bin_Ndot2_2p;
      //     bin_L_gas[i_bin] += local_bin_L_2p; // Luminosities [erg/s]
      //     if (i_grp >= 0) {
      //       #pragma omp atomic
      //       bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_2p;
      //       #pragma omp atomic
      //       bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_2p;
      //       if (in_grp_vir) {
      //         #pragma omp atomic
      //         bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_2p;
      //         #pragma omp atomic
      //         bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_2p;
      //       }
      //       if (in_grp_gal) {
      //         #pragma omp atomic
      //         bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_2p;
      //         #pragma omp atomic
      //         bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_2p;
      //       }
      //     } else if (i_ugrp >= 0) {
      //       #pragma omp atomic
      //       bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_2p;
      //       #pragma omp atomic
      //       bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_2p;
      //     }
      //     if (i_sub >= 0) {
      //       #pragma omp atomic
      //       bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_2p;
      //       #pragma omp atomic
      //       bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_2p;
      //       if (in_sub_vir) {
      //         #pragma omp atomic
      //         bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_2p;
      //         #pragma omp atomic
      //         bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_2p;
      //       }
      //       if (in_sub_gal) {
      //         #pragma omp atomic
      //         bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_2p;
      //         #pragma omp atomic
      //         bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_2p;
      //       }
      //     } else if (i_usub >= 0) {
      //       #pragma omp atomic
      //       bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_2p;
      //       #pragma omp atomic
      //       bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_2p;
      //     }
      //     for (int ai = 0; ai < n_active_ions; ++ai) {
      //       // Cross-section rates [cm^2 photons/s]
      //       auto& sigma_Ndot_ion = spec_2p_HeI.sigma_Ndot_ions[ai];
      //       sigma_Ndot_ions_gas(ai, i_bin) += HeI_2p_n2V * sigma_Ndot_ion[i_bin];
      //       // Photoheating rates [cm^2 erg/s]
      //       auto& sigma_L_ion = spec_2p_HeI.sigma_L_ions[ai];
      //       sigma_L_ions_gas(ai, i_bin) += HeI_2p_n2V * sigma_L_ion[i_bin];
      //     }
      //     for (int j = 0; j < n_dust_species; ++j) {
      //       kappas_Ndot_gas[j][i_bin] += EVAL_2P_HeI[j](kappas_Ndot); // Dust opacity rates [photons/s cm^2/g dust]
      //       albedos_Ndot_gas[j][i_bin] += EVAL_2P_HeI[j](albedos_Ndot); // Dust scattering albedo rates [photons/s]
      //       cosines_Ndot_gas[j][i_bin] += EVAL_2P_HeI[j](cosines_Ndot); // Dust scattering anisotropy rates [photons/s]
      //     }
      //   }
      // }
      // Two-photon emission (HeIII->HeII)
      if (read_HeII) {
        const double HeII_2p_n2V = n2V_HeIII * HeII_2p_rate(T_i, ne, nHII);
        for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
          const double local_bin_Ndot_2p = EVAL_2P_HeII(Ndot);
          const double local_bin_Ndot2_2p = local_bin_Ndot_2p * local_bin_Ndot_2p;
          const double local_bin_L_2p = EVAL_2P_HeII(L);
          local_Ndot_2p += local_bin_Ndot_2p; // Photon rates [photons/s]
          bin_Ndot_gas[i_bin] += local_bin_Ndot_2p;
          bin_Ndot2_gas[i_bin] += local_bin_Ndot2_2p;
          bin_L_gas[i_bin] += local_bin_L_2p; // Luminosities [erg/s]
          if (i_grp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_grp(i_grp, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_grp(i_grp, i_bin) += local_bin_Ndot2_2p;
            if (in_grp_vir) {
              #pragma omp atomic
              bin_Ndot_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot_2p;
              #pragma omp atomic
              bin_Ndot2_gas_grp_vir(i_grp, i_bin) += local_bin_Ndot2_2p;
            }
            if (in_grp_gal) {
              #pragma omp atomic
              bin_Ndot_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot_2p;
              #pragma omp atomic
              bin_Ndot2_gas_grp_gal(i_grp, i_bin) += local_bin_Ndot2_2p;
            }
          } else if (i_ugrp >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_ugrp(i_ugrp, i_bin) += local_bin_Ndot2_2p;
          }
          if (i_sub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_sub(i_sub, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_sub(i_sub, i_bin) += local_bin_Ndot2_2p;
            if (in_sub_vir) {
              #pragma omp atomic
              bin_Ndot_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot_2p;
              #pragma omp atomic
              bin_Ndot2_gas_sub_vir(i_sub, i_bin) += local_bin_Ndot2_2p;
            }
            if (in_sub_gal) {
              #pragma omp atomic
              bin_Ndot_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot_2p;
              #pragma omp atomic
              bin_Ndot2_gas_sub_gal(i_sub, i_bin) += local_bin_Ndot2_2p;
            }
          } else if (i_usub >= 0) {
            #pragma omp atomic
            bin_Ndot_gas_usub(i_usub, i_bin) += local_bin_Ndot_2p;
            #pragma omp atomic
            bin_Ndot2_gas_usub(i_usub, i_bin) += local_bin_Ndot2_2p;
          }
          for (int ai = 0; ai < n_active_ions; ++ai) {
            // Cross-section rates [cm^2 photons/s]
            auto& sigma_Ndot_ion = spec_2p_HeII.sigma_Ndot_ions[ai];
            sigma_Ndot_ions_gas(ai, i_bin) += HeII_2p_n2V * sigma_Ndot_ion[i_bin];
            // Photoheating rates [cm^2 erg/s]
            auto& sigma_L_ion = spec_2p_HeII.sigma_L_ions[ai];
            sigma_L_ions_gas(ai, i_bin) += HeII_2p_n2V * sigma_L_ion[i_bin];
          }
          for (int j = 0; j < n_dust_species; ++j) {
            kappas_Ndot_gas[j][i_bin] += EVAL_2P_HeII(kappas_Ndot[j]); // Dust opacity rates [photons/s cm^2/g dust]
            albedos_Ndot_gas[j][i_bin] += EVAL_2P_HeII(albedos_Ndot[j]); // Dust scattering albedo rates [photons/s]
            cosines_Ndot_gas[j][i_bin] += EVAL_2P_HeII(cosines_Ndot[j]); // Dust scattering anisotropy rates [photons/s]
          }
        }
      }
      local_Ndot += local_Ndot_2p;           // Photon rates [photons/s]
    }

    j_cdf_cells[i] = local_Ndot;             // Photon rate pdf [photons/s]
  }

  // Weighted cross-sections [cm^2], photoheating [erg], and mean energies [erg]
  double L_gas = 0., Ndot_gas = 0., Ndot2_gas = 0.;
  auto& ig = ion_global;                     // Global ionization data
  auto& ic = ion_gas;                        // Gas ionization data
  #pragma omp parallel for reduction(+:L_gas, Ndot_gas, Ndot2_gas)
  for (int i_bin = 0; i_bin < n_bins; ++i_bin) {
    const double bin_Ndot = bin_Ndot_gas[i_bin]; // Bin Ndot
    const double inv_Ndot = safe_division(1., bin_Ndot); // 1 / Ndot
    L_gas += bin_L_gas[i_bin];               // Total luminosity [erg/s]
    Ndot_gas += bin_Ndot;                    // Total rate [photons/s]
    Ndot2_gas += bin_Ndot2_gas[i_bin];       // Total rate^2 [photons^2/s^2]
    bin_n_cells_eff[i_bin] = calc_n_eff(bin_Ndot_gas[i_bin], bin_Ndot2_gas[i_bin]);
    for (int ai = 0; ai < n_active_ions; ++ai) {
      ic.sigma_ions[ai][i_bin] = chop(inv_Ndot * sigma_Ndot_ions_gas(ai, i_bin), sigma_chop); // Cross-sections [cm^2]
      ic.epsilon_ions[ai][i_bin] = chop(sigma_L_ions_gas(ai, i_bin) / sigma_Ndot_ions_gas(ai, i_bin) - ions_erg[ai]); // Photoheating [erg]
    }
    ic.mean_energy[i_bin] = inv_Ndot * bin_L_gas[i_bin]; // Mean energy of each bin [erg]
    for (int i = 0; i < n_dust_species; ++i) {
      ic.kappas[i][i_bin] = inv_Ndot * kappas_Ndot_gas[i][i_bin]; // Dust opacity [cm^2/g dust]
      ic.albedos[i][i_bin] = inv_Ndot * albedos_Ndot_gas[i][i_bin]; // Dust albedo for scattering vs. absorption
      ic.cosines[i][i_bin] = safe_division(cosines_Ndot_gas[i][i_bin], linear_Ndot_gas[i_bin]); // Anisotropy parameter: <μ> for dust scattering
    }

    // Add to the total bin values
    bin_L_tot[i_bin] += bin_L_gas[i_bin];    // Total luminosity [erg/s]
    bin_Ndot_tot[i_bin] += bin_Ndot;         // Total rate [photons/s]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      ig.sigma_ions[ai][i_bin] += sigma_Ndot_ions_gas(ai, i_bin); // Cross-sections [cm^2]
      ig.epsilon_ions[ai][i_bin] += sigma_L_ions_gas(ai, i_bin); // Photoheating [erg]
    }
    for (int i = 0; i < n_dust_species; ++i) {
      ig.kappas[i][i_bin] += kappas_Ndot_gas[i][i_bin]; // Dust opacity [cm^2/g dust]
      ig.albedos[i][i_bin] += albedos_Ndot_gas[i][i_bin]; // Dust albedo for scattering vs. absorption
      ig.cosines[i][i_bin] += cosines_Ndot_gas[i][i_bin]; // Anisotropy parameter: <μ> for dust scattering
    }
  }
  ion_gas.L = L_gas;                         // Total luminosity [erg/s]
  ion_gas.Ndot = Ndot_gas;                   // Total rate [photons/s]
  n_cells_eff = calc_n_eff(Ndot_gas, Ndot2_gas); // Effective number of cells: <Ndot>^2/<Ndot^2>

  // Calculate HI luminosity and photon rate
  double L_gas_HI = 0., Ndot_gas_HI = 0., Ndot2_gas_HI = 0.;
  #pragma omp parallel for reduction(+:L_gas_HI, Ndot_gas_HI, Ndot2_gas_HI)
  for (int i_bin = HI_bin; i_bin < n_bins; ++i_bin) {
    L_gas_HI += bin_L_gas[i_bin];            // Total luminosity [erg/s]
    Ndot_gas_HI += bin_Ndot_gas[i_bin];      // Total rate [photons/s]
    Ndot2_gas_HI += bin_Ndot2_gas[i_bin];    // Total rate^2 [photons^2/s^2]
  }
  ion_gas.HI.L = L_gas_HI;                   // Total luminosity [erg/s]
  ion_gas.HI.Ndot = Ndot_gas_HI;             // Total rate [photons/s]
  n_cells_eff_HI = calc_n_eff(Ndot_gas_HI, Ndot2_gas_HI); // Effective number of cells: <Ndot>^2/<Ndot^2>

  // Calculate group and subhalo properties
  if constexpr (output_groups) {
    if (n_groups > 0) {
      add_Ndot_gas_halo(Ndot_gas_grp, Ndot2_gas_grp, n_cells_eff_grp, Ndot_gas_HI_grp, Ndot2_gas_HI_grp,
                        n_cells_eff_HI_grp, bin_Ndot_gas_grp, bin_Ndot2_gas_grp, bin_n_cells_eff_grp, bin_Ndot_grp);
      if (output_grp_vir)
        add_Ndot_gas_halo(Ndot_gas_grp_vir, Ndot2_gas_grp_vir, n_cells_eff_grp_vir, Ndot_gas_HI_grp_vir, Ndot2_gas_HI_grp_vir,
                          n_cells_eff_HI_grp_vir, bin_Ndot_gas_grp_vir, bin_Ndot2_gas_grp_vir, bin_n_cells_eff_grp_vir, bin_Ndot_grp_vir);
      if (output_grp_gal)
        add_Ndot_gas_halo(Ndot_gas_grp_gal, Ndot2_gas_grp_gal, n_cells_eff_grp_gal, Ndot_gas_HI_grp_gal, Ndot2_gas_HI_grp_gal,
                          n_cells_eff_HI_grp_gal, bin_Ndot_gas_grp_gal, bin_Ndot2_gas_grp_gal, bin_n_cells_eff_grp_gal, bin_Ndot_grp_gal);
    }
    if (n_ugroups > 0)
      add_Ndot_gas_halo(Ndot_gas_ugrp, Ndot2_gas_ugrp, n_cells_eff_ugrp, Ndot_gas_HI_ugrp, Ndot2_gas_HI_ugrp,
                        n_cells_eff_HI_ugrp, bin_Ndot_gas_ugrp, bin_Ndot2_gas_ugrp, bin_n_cells_eff_ugrp, bin_Ndot_ugrp);
  }
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {
      add_Ndot_gas_halo(Ndot_gas_sub, Ndot2_gas_sub, n_cells_eff_sub, Ndot_gas_HI_sub, Ndot2_gas_HI_sub,
                        n_cells_eff_HI_sub, bin_Ndot_gas_sub, bin_Ndot2_gas_sub, bin_n_cells_eff_sub, bin_Ndot_sub);
      if (output_sub_vir)
        add_Ndot_gas_halo(Ndot_gas_sub_vir, Ndot2_gas_sub_vir, n_cells_eff_sub_vir, Ndot_gas_HI_sub_vir, Ndot2_gas_HI_sub_vir,
                          n_cells_eff_HI_sub_vir, bin_Ndot_gas_sub_vir, bin_Ndot2_gas_sub_vir, bin_n_cells_eff_sub_vir, bin_Ndot_sub_vir);
      if (output_sub_gal)
        add_Ndot_gas_halo(Ndot_gas_sub_gal, Ndot2_gas_sub_gal, n_cells_eff_sub_gal, Ndot_gas_HI_sub_gal, Ndot2_gas_HI_sub_gal,
                          n_cells_eff_HI_sub_gal, bin_Ndot_gas_sub_gal, bin_Ndot2_gas_sub_gal, bin_n_cells_eff_sub_gal, bin_Ndot_sub_gal);
    }
    if (n_usubhalos > 0)
      add_Ndot_gas_halo(Ndot_gas_usub, Ndot2_gas_usub, n_cells_eff_usub, Ndot_gas_HI_usub, Ndot2_gas_HI_usub,
                        n_cells_eff_HI_usub, bin_Ndot_gas_usub, bin_Ndot2_gas_usub, bin_n_cells_eff_usub, bin_Ndot_usub);
  }

  if (output_cells) {
    Ndot_int_cells.resize(n_cells);          // Intrinsic emission [photons/s]
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i)
      Ndot_int_cells[i] = j_cdf_cells[i];    // Copy before cdf conversion
  }

  if (Ndot_gas <= 0.)
    root_error("Total gas photon rate must be positive");
  setup_bin_cdf(ion_gas);                    // Set up frequency cdf
  const double Ndot_norm = 1. / Ndot_gas;
  const double n_photons_norm = 1. / double(n_photons);
  if (j_exp == 1. && V_exp == 1.) {          // No luminosity boosting
    for (int i = 1; i < n_cells; ++i)
      j_cdf_cells[i] += j_cdf_cells[i - 1];  // Turn the pdf into a cdf
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      j_cdf_cells[i] *= Ndot_norm;           // Normalize cdf
      j_weights_cells[i] = n_photons_norm;   // Equal weights
    }
  } else {                                   // Apply luminosity and volume boosting
    const double V_norm = (!cell_based_emission || V_exp == 1.)
      ? 1. : (CARTESIAN ? 1. / dV : double(n_photons) / omp_sum(V)); // Volume normalization factor
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      j_weights_cells[i] = j_cdf_cells[i] * Ndot_norm; // Copy the normalized pdf
      if (j_weights_cells[i] > 0.)           // Apply power law boost
        j_cdf_cells[i] = pow(j_weights_cells[i], j_exp); // pdf^j_exp (unnormalized)
      else
        j_cdf_cells[i] = 0.;                 // No probability
      if (V_exp != 1.)                       // Apply volume boost
        j_cdf_cells[i] *= pow(V_norm * VOLUME(i), V_exp);
    }
    const double cdf_norm = 1. / omp_sum(j_cdf_cells);
    const double dn_photons = double(n_photons);
    #pragma omp parallel for
    for (int i = 0; i < n_cells; ++i) {
      j_cdf_cells[i] *= cdf_norm;            // pdf^j_exp (normalized)
      if (j_cdf_cells[i] > 0.)
        j_weights_cells[i] /= j_cdf_cells[i] * dn_photons; // weight = pdf / (pdf^j_exp * n_photons)
    }
    for (int i = 1; i < n_cells; ++i)
      j_cdf_cells[i] += j_cdf_cells[i - 1];  // Turn the pdf into a cdf
  }

  // Populate a lookup table to quickly find cdf indices (cells)
  const double half_cdf = 0.5 / double(n_cdf_cells); // Half interval width
  for (int i = 0, cell = 0; i < n_cdf_cells; ++i) {
    const double frac_cdf = double(2*i + 1) * half_cdf; // Probability interval midpoint
    while (j_cdf_cells[cell] <= frac_cdf && cell < n_cells - 1)
      cell++;                                // Move to the right bin ( --> )
    j_map_cells[i] = cell;                   // Lookup table for insertion
  }
}

/* AGN setup that is performed once. */
void Ionization::initial_setup_AGN() {
  // AGN source insertion
  i_AGN = find_cell(r_AGN, 0);               // Start with default cell = 0
  #if spherical_escape
    const Vec3 dr_AGN = r_AGN - escape_center; // Position relative to center
    if (dr_AGN.dot() >= emission_radius2)
      root_error("AGN source location must be within the spherical emission region");
  #endif
  #if box_escape
    if (r_AGN.x < emission_box[0].x || r_AGN.x > emission_box[1].x ||
        r_AGN.y < emission_box[0].y || r_AGN.y > emission_box[1].y ||
        r_AGN.z < emission_box[0].z || r_AGN.z > emission_box[1].z)
      root_error("AGN source location must be within the box emission region");
  #endif
  if (AGN_model == "Stalevski12") {
    // Ionizing luminosity [erg/s]
    bin_L_AGN = {0.12217712635749829 * Lbol_AGN, 0.16387222327106707 * Lbol_AGN, 0.3309187233252852 * Lbol_AGN};
    // Ionizing photon rate [photons/s]
    bin_Ndot_AGN = {4231162046.6547365 * Lbol_AGN, 2870090494.1869707 * Lbol_AGN, 1796512643.2839115 * Lbol_AGN};
    // Cross-section rates [cm^2 photons/s]
    sigma_Ndot_HI_AGN = {1.4342775032273053e-08 * Lbol_AGN, 1.6007331888029465e-09 * Lbol_AGN, 7.176677591603307e-11 * Lbol_AGN};
    sigma_Ndot_HeI_AGN = {0., 1.263039931953503e-08 * Lbol_AGN, 1.1744782244859717e-09 * Lbol_AGN};
    sigma_Ndot_HeII_AGN = {0., 0., 9.864052892950707e-10 * Lbol_AGN};
    // Photoheating rates [cm^2 erg/s]
    sigma_L_HI_AGN = {7.178777602026441e-20 * Lbol_AGN, 4.5089596956906785e-20 * Lbol_AGN, 6.6141926038769874e-21 * Lbol_AGN};
    sigma_L_HeI_AGN = {0., 1.615656947502553e-19 * Lbol_AGN, 9.264112560032408e-20 * Lbol_AGN};
    sigma_L_HeII_AGN = {0., 0., 2.855797387760416e-20 * Lbol_AGN};
    if (dust_models[0] == "MW") {
      // Opacity rates [photons/s cm^2/g dust]
      kappa_Ndot_AGN = {470678507512923.44 * Lbol_AGN, 127140624455435.64 * Lbol_AGN, 40762148028044.375 * Lbol_AGN};
      // Scattering albedo rates [photons/s]
      albedo_Ndot_AGN = {919928222.8908963 * Lbol_AGN, 959762592.8283172 * Lbol_AGN, 827775922.2536366 * Lbol_AGN};
      // Scattering anisotropy rates [photons/s]
      cosine_Ndot_AGN = {2974707272.0488696 * Lbol_AGN, 2540692266.338951 * Lbol_AGN, 1759525728.348858 * Lbol_AGN};
    } else if (dust_models[0] == "SMC") {
      // Opacity rates [photons/s cm^2/g dust]
      kappa_Ndot_AGN = {461181972851562.9 * Lbol_AGN, 159104534152330.16 * Lbol_AGN, 63959335286831.88 * Lbol_AGN};
      // Scattering albedo rates [photons/s]
      albedo_Ndot_AGN = {1141562643.7347565 * Lbol_AGN, 865928012.0804739 * Lbol_AGN, 613679933.6122993 * Lbol_AGN};
      // Scattering anisotropy rates [photons/s]
      cosine_Ndot_AGN = {2713440351.89188 * Lbol_AGN, 2500456879.0623217 * Lbol_AGN, 1745460800.6249092 * Lbol_AGN};
    } else                                   // Catch unknown dust models
      root_error("Unrecognized value for dust_model: " + dust_models[0]);
  } else if (AGN_model == "Lusso15") {
    // Ionizing luminosity [erg/s]
    bin_L_AGN = {0.12047547133818264 * Lbol_AGN, 0.1000258145303362 * Lbol_AGN, 0.13257212221476833 * Lbol_AGN};
    // Ionizing photon rate [photons/s]
    bin_Ndot_AGN = {4257214455.2997875 * Lbol_AGN, 1815834111.2350085 * Lbol_AGN, 635064887.5575639 * Lbol_AGN};
    // Cross-section rates [cm^2 photons/s]
    sigma_Ndot_HI_AGN = {1.5158275377343947e-08 * Lbol_AGN, 1.1014571832831546e-09 * Lbol_AGN, 2.8149288073197206e-11 * Lbol_AGN};
    sigma_Ndot_HeI_AGN = {0., 8.461469024827422e-09 * Lbol_AGN, 4.5243642297117167e-10 * Lbol_AGN};
    sigma_Ndot_HeII_AGN = {0., 0., 3.8349052034234036e-10 * Lbol_AGN};
    // Photoheating rates [cm^2 erg/s]
    sigma_L_HI_AGN = {6.923395141699991e-20 * Lbol_AGN, 2.967877082987328e-20 * Lbol_AGN, 2.4932609223884894e-21 * Lbol_AGN};
    sigma_L_HeI_AGN = {0., 9.549286852521454e-20 * Lbol_AGN, 3.3788464425021564e-20 * Lbol_AGN};
    sigma_L_HeII_AGN = {0., 0., 9.608290930091478e-21 * Lbol_AGN};
    if (dust_models[0] == "MW") {
      // Opacity rates [photons/s cm^2/g dust]
      kappa_Ndot_AGN = {479095288908242.94 * Lbol_AGN, 82688261817416.58 * Lbol_AGN, 14610993355948.123 * Lbol_AGN};
      // Scattering albedo rates [photons/s]
      albedo_Ndot_AGN = {920733826.1756417 * Lbol_AGN, 600767937.4231263 * Lbol_AGN, 290577183.6810445 * Lbol_AGN};
      // Scattering anisotropy rates [photons/s]
      cosine_Ndot_AGN = {2971939235.967087 * Lbol_AGN, 1595175029.6822608 * Lbol_AGN, 620943343.2704192 * Lbol_AGN};
    } else if (dust_models[0] == "SMC") {
      // Opacity rates [photons/s cm^2/g dust]
      kappa_Ndot_AGN = {472526628497419.75 * Lbol_AGN, 102443979375151.81 * Lbol_AGN, 22922932598878.668 * Lbol_AGN};
      // Scattering albedo rates [photons/s]
      albedo_Ndot_AGN = {1140266935.1486466 * Lbol_AGN, 543990457.8429457 * Lbol_AGN, 216452989.69520155 * Lbol_AGN};
      // Scattering anisotropy rates [photons/s]
      cosine_Ndot_AGN = {2703069533.9581156 * Lbol_AGN, 1570529333.9226158 * Lbol_AGN, 615830134.5748188 * Lbol_AGN};
    } else                                   // Catch unknown dust models
      root_error("Unrecognized value for dust_model: " + dust_models[0]);
  }

  // Cross-sections [cm^2], photoheating [erg], and mean energies [erg]
  sigma_HI_AGN = {sigma_Ndot_HI_AGN[0] / bin_Ndot_AGN[0], sigma_Ndot_HI_AGN[1] / bin_Ndot_AGN[1], sigma_Ndot_HI_AGN[2] / bin_Ndot_AGN[2]};
  sigma_HeI_AGN = {0., sigma_Ndot_HeI_AGN[1] / bin_Ndot_AGN[1], sigma_Ndot_HeI_AGN[2] / bin_Ndot_AGN[2]};
  sigma_HeII_AGN = {0., 0., sigma_Ndot_HeII_AGN[2] / bin_Ndot_AGN[2]};
  epsilon_HI_AGN = {sigma_L_HI_AGN[0] / sigma_Ndot_HI_AGN[0], sigma_L_HI_AGN[1] / sigma_Ndot_HI_AGN[1], sigma_L_HI_AGN[2] / sigma_Ndot_HI_AGN[2]};
  epsilon_HeI_AGN = {0., sigma_L_HeI_AGN[1] / sigma_Ndot_HeI_AGN[1], sigma_L_HeI_AGN[2] / sigma_Ndot_HeI_AGN[2]};
  epsilon_HeII_AGN = {0., 0., sigma_L_HeII_AGN[2] / sigma_Ndot_HeII_AGN[2]};
  mean_energy_AGN = {bin_L_AGN[0] / bin_Ndot_AGN[0], bin_L_AGN[1] / bin_Ndot_AGN[1], bin_L_AGN[2] / bin_Ndot_AGN[2]};
  kappa_AGN = {kappa_Ndot_AGN[0] / bin_Ndot_AGN[0], kappa_Ndot_AGN[1] / bin_Ndot_AGN[1], kappa_Ndot_AGN[2] / bin_Ndot_AGN[2]};
  albedo_AGN = {albedo_Ndot_AGN[0] / bin_Ndot_AGN[0], albedo_Ndot_AGN[1] / bin_Ndot_AGN[1], albedo_Ndot_AGN[2] / bin_Ndot_AGN[2]};
  cosine_AGN = {cosine_Ndot_AGN[0] / bin_Ndot_AGN[0], cosine_Ndot_AGN[1] / bin_Ndot_AGN[1], cosine_Ndot_AGN[2] / bin_Ndot_AGN[2]};
  L_AGN = bin_L_AGN[0] + bin_L_AGN[1] + bin_L_AGN[2]; // Total luminosity [erg/s]
  Ndot_AGN = bin_Ndot_AGN[0] + bin_Ndot_AGN[1] + bin_Ndot_AGN[2]; // Total rate [photons/s]
  const double _1_n = 1. / double(n_photons); // 1 / n_photons
  bin_cdf_AGN = {bin_Ndot_AGN[0] / Ndot_AGN, (bin_Ndot_AGN[0] + bin_Ndot_AGN[1]) / Ndot_AGN, 1.};
  bin_weight_AGN = {_1_n, _1_n, _1_n};       // Equal weight photons
  if (nu_exp != 0. || min_HI_bin_cdf > 0.)   // TODO: Frequency boosting
    error("Frequency boosting not yet implemented for AGN sources");
}

/* AGN setup that is performed during each iteration. */
void Ionization::iterative_setup_AGN() {
  auto& ig = ion_global;                     // Global ionization data
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    bin_L_tot[i] += bin_L_AGN[i];            // Also for the bin values
    bin_Ndot_tot[i] += bin_Ndot_AGN[i];
    ig.sigma_ions[HI_ION][i] += sigma_Ndot_HI_AGN[i];
    ig.sigma_ions[HeI_ION][i] += sigma_Ndot_HeI_AGN[i];
    ig.sigma_ions[HeII_ION][i] += sigma_Ndot_HeII_AGN[i];
    ig.epsilon_ions[HI_ION][i] += sigma_L_HI_AGN[i];
    ig.epsilon_ions[HeI_ION][i] += sigma_L_HeI_AGN[i];
    ig.epsilon_ions[HeII_ION][i] += sigma_L_HeII_AGN[i];
    for (int j = 0; j < n_dust_species; ++j) {
      ig.kappas[j][i] += kappa_Ndot_AGN[i];
      ig.albedos[j][i] += albedo_Ndot_AGN[i];
      ig.cosines[j][i] += cosine_Ndot_AGN[i];
    }
  }
}

/* Add to global ionization spectral data. */
static void add_ion_bins(IonBinData& ion) {
  auto& ig = ion_global;                     // Global ionization data
  #pragma omp parallel for
  for (int i = 0; i < n_bins; ++i) {
    ig.bin_L[i] += ion.bin_L[i];             // Add to global bin values
    ig.bin_Ndot[i] += ion.bin_Ndot[i];       // Total rate [photons/s]
    for (int ai = 0; ai < n_active_ions; ++ai) {
      ig.sigma_ions[ai][i] += ion.sigma_Ndot_ions[ai][i]; // Cross-sections [cm^2]
      ig.epsilon_ions[ai][i] += ion.sigma_L_ions[ai][i]; // Photoheating [erg]
    }
    for (int j = 0; j < n_dust_species; ++j) {
      ig.kappas[j][i] += ion.kappas_Ndot[j][i]; // Dust opacity [cm^2/g dust]
      ig.albedos[j][i] += ion.albedos_Ndot[j][i]; // Dust albedo for scattering vs. absorption
      ig.cosines[j][i] += ion.cosines_Ndot[j][i]; // Anisotropy parameter: <μ> for dust scattering
    }
  }
}

/* Point source setup that is performed during each iteration. */
void Ionization::iterative_setup_point() {
  add_ion_bins(ion_point);                   // Add to global spectral data
}

/* Plane source setup that is performed during each iteration. */
void Ionization::iterative_setup_plane() {
  add_ion_bins(ion_plane);                   // Add to global spectral data
}

/* Setup everything for ionization radiative transfer. */
void Ionization::setup() {
  // Setup general escape
  if constexpr (check_escape)
    setup_escape();                          // General escape setup

  // Allocate global luminosities and spectral properties
  auto& ig = ion_global;                     // Global ionization data
  bin_L_tot = vector<double>(n_bins);        // Total bin luminosities [erg/s]
  bin_Ndot_tot = vector<double>(n_bins);     // Total bin rates [photons/s]
  ig.bin_cdf = vector<double>(n_bins);       // Total bin cumulative distribution function
  bin_f_src = vector<double>(n_bins);        // Total bin source fraction: sum(w0)
  bin_f2_src = vector<double>(n_bins);       // Total bin squared f_src: sum(w0^2)
  bin_n_photons_src = vector<double>(n_bins); // Effective number of emitted photons: 1/<w0>
  bin_f_esc = vector<double>(n_bins);        // Total bin escape fraction: sum(w)
  bin_f2_esc = vector<double>(n_bins);       // Total bin squared f_esc: sum(w^2)
  bin_n_photons_esc = vector<double>(n_bins); // Effective number of escaped photons: 1/<w>
  bin_f_abs = vector<double>(n_bins);        // Total bin dust absorption fraction
  bin_f2_abs = vector<double>(n_bins);       // Total bin squared f_abs: sum(w^2)
  bin_n_photons_abs = vector<double>(n_bins); // Effective number of absorbed photons: 1/<w>
  f_ions.resize(n_active_ions);              // Total species absorption fractions
  bin_f_ions.resize(n_active_ions);          // Total bin species absorption fractions
  for (auto& bin_f_ion : bin_f_ions)
    bin_f_ion = vector<double>(n_bins);      // Total bin species absorption fractions
  bin_f2_HI = vector<double>(n_bins);        // Total bin squared absorption fractions
  bin_n_photons_HI = vector<double>(n_bins); // Effective number of photons: 1/<w>
  bin_l_abs = vector<double>(n_bins);        // Global bin absorption distance [cm]
  ig.sigma_ions.resize(n_active_ions);       // Species ionization cross-sections [cm^2]
  for (auto& sigma_ion : ig.sigma_ions)
    sigma_ion = vector<double>(n_bins);
  ig.epsilon_ions.resize(n_active_ions);     // Species ionization heating [erg]
  for (auto& epsilon_ion : ig.epsilon_ions)
    epsilon_ion = vector<double>(n_bins);
  ig.mean_energy = vector<double>(n_bins);   // Mean energy of each frequency bin [erg]
  for (int i = 0; i < n_dust_species; ++i) {
    ig.kappas[i] = vector<double>(n_bins);   // Dust opacity [cm^2/g dust]
    ig.albedos[i] = vector<double>(n_bins);  // Dust albedo for scattering vs. absorption
    ig.cosines[i] = vector<double>(n_bins);  // Anisotropy parameter: <μ> for dust scattering
  }

  // Allocate group properties
  if constexpr (output_groups) {
    if (n_groups > 0) {                      // Have groups
      Ndot_grp = vector<double>(n_groups);   // Group photon rates [photons/s]
      f_src_grp = vector<double>(n_groups);  // Group source fractions: sum(w0)
      f2_src_grp = vector<double>(n_groups); // Group squared source fractions: sum(w0^2)
      n_photons_src_grp = vector<double>(n_groups); // Effective number of emitted photons: 1/<w0>
      // HI properties
      Ndot_HI_grp = vector<double>(n_groups); // Group HI photon rates [photons/s]
      f_src_HI_grp = vector<double>(n_groups); // Group HI source fractions: sum(w0)
      f2_src_HI_grp = vector<double>(n_groups); // Group HI squared source fractions: sum(w0^2)
      n_photons_src_HI_grp = vector<double>(n_groups); // Group HI effective number of emitted photons: 1/<w0>
      // Bin properties
      bin_Ndot_grp = Image(n_groups, n_bins); // Group bin photon rates [photons/s]
      bin_f_src_grp = Image(n_groups, n_bins); // Group bin source fractions: sum(w0)
      bin_f2_src_grp = Image(n_groups, n_bins); // Group bin squared source fractions: sum(w0^2)
      bin_n_photons_src_grp = Image(n_groups, n_bins); // Effective number of emitted photons: 1/<w0>
      if (output_grp_obs) {
        f_esc_grp = vector<double>(n_groups); // Group escape fractions
        f2_esc_grp = vector<double>(n_groups); // Group squared escape fractions
        n_photons_esc_grp = vector<double>(n_groups); // Effective number of escaped photons: 1/<w>
        f_abs_grp = vector<double>(n_groups); // Group dust absorption fractions
        f2_abs_grp = vector<double>(n_groups); // Group squared dust absorption fractions
        n_photons_abs_grp = vector<double>(n_groups); // Effective number of absorbed photons: 1/<w>
        // HI properties
        f_esc_HI_grp = vector<double>(n_groups); // Group HI escape fractions
        f2_esc_HI_grp = vector<double>(n_groups); // Group HI squared escape fractions
        n_photons_esc_HI_grp = vector<double>(n_groups); // Group HI effective number of escaped photons: 1/<w>
        f_abs_HI_grp = vector<double>(n_groups); // Group HI dust absorption fractions
        f2_abs_HI_grp = vector<double>(n_groups); // Group HI squared dust absorption fractions
        n_photons_abs_HI_grp = vector<double>(n_groups); // Group HI effective number of absorbed photons: 1/<w>
        // Bin properties
        bin_f_esc_grp = Image(n_groups, n_bins); // Group bin escape fractions
        bin_f2_esc_grp = Image(n_groups, n_bins); // Group bin squared escape fractions
        bin_n_photons_esc_grp = Image(n_groups, n_bins); // Effective number of escaped photons: 1/<w>
        bin_f_abs_grp = Image(n_groups, n_bins); // Group bin dust absorption fractions
        bin_f2_abs_grp = Image(n_groups, n_bins); // Group bin squared dust absorption fractions
        bin_n_photons_abs_grp = Image(n_groups, n_bins); // Effective number of absorbed photons: 1/<w>
      }
      if (output_grp_vir) {
        Ndot_grp_vir = vector<double>(n_groups); // Group photon rates [photons/s]
        f_src_grp_vir = vector<double>(n_groups); // Group source fractions: sum(w0)
        f2_src_grp_vir = vector<double>(n_groups); // Group squared source fractions: sum(w0^2)
        n_photons_src_grp_vir = vector<double>(n_groups); // Effective number of emitted photons: 1/<w0>
        f_esc_grp_vir = vector<double>(n_groups); // Group escape fractions
        f2_esc_grp_vir = vector<double>(n_groups); // Group squared escape fractions
        n_photons_esc_grp_vir = vector<double>(n_groups); // Effective number of escaped photons: 1/<w>
        f_abs_grp_vir = vector<double>(n_groups); // Group dust absorption fractions
        f2_abs_grp_vir = vector<double>(n_groups); // Group squared dust absorption fractions
        n_photons_abs_grp_vir = vector<double>(n_groups); // Effective number of absorbed photons: 1/<w>
        // HI properties
        Ndot_HI_grp_vir = vector<double>(n_groups); // Group HI photon rates [photons/s]
        f_src_HI_grp_vir = vector<double>(n_groups); // Group HI source fractions: sum(w0)
        f2_src_HI_grp_vir = vector<double>(n_groups); // Group HI squared source fractions: sum(w0^2)
        n_photons_src_HI_grp_vir = vector<double>(n_groups); // Group HI effective number of emitted photons: 1/<w0>
        f_esc_HI_grp_vir = vector<double>(n_groups); // Group HI escape fractions
        f2_esc_HI_grp_vir = vector<double>(n_groups); // Group HI squared escape fractions
        n_photons_esc_HI_grp_vir = vector<double>(n_groups); // Group HI effective number of escaped photons: 1/<w>
        f_abs_HI_grp_vir = vector<double>(n_groups); // Group HI dust absorption fractions
        f2_abs_HI_grp_vir = vector<double>(n_groups); // Group HI squared dust absorption fractions
        n_photons_abs_HI_grp_vir = vector<double>(n_groups); // Group HI effective number of absorbed photons: 1/<w>
        // Bin properties
        bin_Ndot_grp_vir = Image(n_groups, n_bins); // Group bin photon rates [photons/s]
        bin_f_src_grp_vir = Image(n_groups, n_bins); // Group bin source fractions: sum(w0)
        bin_f2_src_grp_vir = Image(n_groups, n_bins); // Group bin squared source fractions: sum(w0^2)
        bin_n_photons_src_grp_vir = Image(n_groups, n_bins); // Effective number of emitted photons: 1/<w0>
        bin_f_esc_grp_vir = Image(n_groups, n_bins); // Group bin escape fractions
        bin_f2_esc_grp_vir = Image(n_groups, n_bins); // Group bin squared escape fractions
        bin_n_photons_esc_grp_vir = Image(n_groups, n_bins); // Effective number of escaped photons: 1/<w>
        bin_f_abs_grp_vir = Image(n_groups, n_bins); // Group bin dust absorption fractions
        bin_f2_abs_grp_vir = Image(n_groups, n_bins); // Group bin squared dust absorption fractions
        bin_n_photons_abs_grp_vir = Image(n_groups, n_bins); // Effective number of absorbed photons: 1/<w>
      }
      if (output_grp_gal) {
        Ndot_grp_gal = vector<double>(n_groups); // Group photon rates [photons/s]
        f_src_grp_gal = vector<double>(n_groups); // Group source fractions: sum(w0)
        f2_src_grp_gal = vector<double>(n_groups); // Group squared source fractions: sum(w0^2)
        n_photons_src_grp_gal = vector<double>(n_groups); // Effective number of emitted photons: 1/<w0>
        f_esc_grp_gal = vector<double>(n_groups); // Group escape fractions
        f2_esc_grp_gal = vector<double>(n_groups); // Group squared escape fractions
        n_photons_esc_grp_gal = vector<double>(n_groups); // Effective number of escaped photons: 1/<w>
        f_abs_grp_gal = vector<double>(n_groups); // Group dust absorption fractions
        f2_abs_grp_gal = vector<double>(n_groups); // Group squared dust absorption fractions
        n_photons_abs_grp_gal = vector<double>(n_groups); // Effective number of absorbed photons: 1/<w>
        // HI properties
        Ndot_HI_grp_gal = vector<double>(n_groups); // Group HI photon rates [photons/s]
        f_src_HI_grp_gal = vector<double>(n_groups); // Group HI source fractions: sum(w0)
        f2_src_HI_grp_gal = vector<double>(n_groups); // Group HI squared source fractions: sum(w0^2)
        n_photons_src_HI_grp_gal = vector<double>(n_groups); // Group HI effective number of emitted photons: 1/<w0>
        f_esc_HI_grp_gal = vector<double>(n_groups); // Group HI escape fractions
        f2_esc_HI_grp_gal = vector<double>(n_groups); // Group HI squared escape fractions
        n_photons_esc_HI_grp_gal = vector<double>(n_groups); // Group HI effective number of escaped photons: 1/<w>
        f_abs_HI_grp_gal = vector<double>(n_groups); // Group HI dust absorption fractions
        f2_abs_HI_grp_gal = vector<double>(n_groups); // Group HI squared dust absorption fractions
        n_photons_abs_HI_grp_gal = vector<double>(n_groups); // Group HI effective number of absorbed photons: 1/<w>
        // Bin properties
        bin_Ndot_grp_gal = Image(n_groups, n_bins); // Group bin photon rates [photons/s]
        bin_f_src_grp_gal = Image(n_groups, n_bins); // Group bin source fractions: sum(w0)
        bin_f2_src_grp_gal = Image(n_groups, n_bins); // Group bin squared source fractions: sum(w0^2)
        bin_n_photons_src_grp_gal = Image(n_groups, n_bins); // Effective number of emitted photons: 1/<w0>
        bin_f_esc_grp_gal = Image(n_groups, n_bins); // Group bin escape fractions
        bin_f2_esc_grp_gal = Image(n_groups, n_bins); // Group bin squared escape fractions
        bin_n_photons_esc_grp_gal = Image(n_groups, n_bins); // Effective number of escaped photons: 1/<w>
        bin_f_abs_grp_gal = Image(n_groups, n_bins); // Group bin dust absorption fractions
        bin_f2_abs_grp_gal = Image(n_groups, n_bins); // Group bin squared dust absorption fractions
        bin_n_photons_abs_grp_gal = Image(n_groups, n_bins); // Effective number of absorbed photons: 1/<w>
      }
    }
    if (n_ugroups > 0) {                     // Have unfiltered groups
      Ndot_ugrp = vector<double>(n_ugroups); // Unfiltered group photon rates [photons/s]
      f_src_ugrp = vector<double>(n_ugroups); // Unfiltered group source fractions: sum(w0)
      f2_src_ugrp = vector<double>(n_ugroups); // Unfiltered group squared source fractions: sum(w0^2)
      n_photons_src_ugrp = vector<double>(n_ugroups); // Effective number of emitted photons: 1/<w0>
      // HI properties
      Ndot_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI photon rates [photons/s]
      f_src_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI source fractions: sum(w0)
      f2_src_HI_ugrp = vector<double>(n_ugroups); // Unfiltered group HI squared source fractions: sum(w0^2)
      n_photons_src_HI_ugrp = vector<double>(n_ugroups); // Effective number of emitted photons: 1/<w0>
      // Bin properties
      bin_Ndot_ugrp = Image(n_ugroups, n_bins); // Unfiltered group photon rates [photons/s]
      bin_f_src_ugrp = Image(n_ugroups, n_bins); // Unfiltered group source fractions: sum(w0)
      bin_f2_src_ugrp = Image(n_ugroups, n_bins); // Unfiltered group squared source fractions: sum(w0^2)
      bin_n_photons_src_ugrp = Image(n_ugroups, n_bins); // Effective number of emitted photons: 1/<w0>
    }
  }

  // Allocate subhalo properties
  if constexpr (output_subhalos) {
    if (n_subhalos > 0) {                    // Have subhalos
      Ndot_sub = vector<double>(n_subhalos); // Subhalo photon rates [photons/s]
      f_src_sub = vector<double>(n_subhalos); // Subhalo source fractions: sum(w0)
      f2_src_sub = vector<double>(n_subhalos); // Subhalo squared source fractions: sum(w0^2)
      n_photons_src_sub = vector<double>(n_subhalos); // Effective number of emitted photons: 1/<w0>
      // HI properties
      Ndot_HI_sub = vector<double>(n_subhalos); // Subhalo HI photon rates [photons/s]
      f_src_HI_sub = vector<double>(n_subhalos); // Subhalo HI source fractions: sum(w0)
      f2_src_HI_sub = vector<double>(n_subhalos); // Subhalo HI squared source fractions: sum(w0^2)
      n_photons_src_HI_sub = vector<double>(n_subhalos); // Subhalo HI effective number of emitted photons: 1/<w0>
      // Bin properties
      bin_Ndot_sub = Image(n_subhalos, n_bins); // Subhalo bin photon rates [photons/s]
      bin_f_src_sub = Image(n_subhalos, n_bins); // Subhalo bin source fractions: sum(w0)
      bin_f2_src_sub = Image(n_subhalos, n_bins); // Subhalo bin squared source fractions: sum(w0^2)
      bin_n_photons_src_sub = Image(n_subhalos, n_bins); // Effective number of emitted photons: 1/<w0>
      if (output_sub_obs) {
        f_esc_sub = vector<double>(n_subhalos); // Subhalo escape fractions
        f2_esc_sub = vector<double>(n_subhalos); // Subhalo squared escape fractions
        n_photons_esc_sub = vector<double>(n_subhalos); // Effective number of escaped photons: 1/<w>
        f_abs_sub = vector<double>(n_subhalos); // Subhalo dust absorption fractions
        f2_abs_sub = vector<double>(n_subhalos); // Subhalo squared dust absorption fractions
        n_photons_abs_sub = vector<double>(n_subhalos); // Effective number of absorbed photons: 1/<w>
        // HI properties
        f_esc_HI_sub = vector<double>(n_subhalos); // Subhalo HI escape fractions
        f2_esc_HI_sub = vector<double>(n_subhalos); // Subhalo HI squared escape fractions
        n_photons_esc_HI_sub = vector<double>(n_subhalos); // Subhalo HI effective number of escaped photons: 1/<w>
        f_abs_HI_sub = vector<double>(n_subhalos); // Subhalo HI dust absorption fractions
        f2_abs_HI_sub = vector<double>(n_subhalos); // Subhalo HI squared dust absorption fractions
        n_photons_abs_HI_sub = vector<double>(n_subhalos); // Subhalo HI effective number of absorbed photons: 1/<w>
        // Bin properties
        bin_f_esc_sub = Image(n_subhalos, n_bins); // Subhalo bin escape fractions
        bin_f2_esc_sub = Image(n_subhalos, n_bins); // Subhalo bin squared escape fractions
        bin_n_photons_esc_sub = Image(n_subhalos, n_bins); // Effective number of escaped photons: 1/<w>
        bin_f_abs_sub = Image(n_subhalos, n_bins); // Subhalo bin dust absorption fractions
        bin_f2_abs_sub = Image(n_subhalos, n_bins); // Subhalo bin squared dust absorption fractions
        bin_n_photons_abs_sub = Image(n_subhalos, n_bins); // Effective number of absorbed photons: 1/<w>
      }
      if (output_sub_vir) {
        Ndot_sub_vir = vector<double>(n_subhalos); // Subhalo photon rates [photons/s]
        f_src_sub_vir = vector<double>(n_subhalos); // Subhalo source fractions: sum(w0)
        f2_src_sub_vir = vector<double>(n_subhalos); // Subhalo squared source fractions: sum(w0^2)
        n_photons_src_sub_vir = vector<double>(n_subhalos); // Effective number of emitted photons: 1/<w0>
        f_esc_sub_vir = vector<double>(n_subhalos); // Subhalo escape fractions
        f2_esc_sub_vir = vector<double>(n_subhalos); // Subhalo squared escape fractions
        n_photons_esc_sub_vir = vector<double>(n_subhalos); // Effective number of escaped photons: 1/<w>
        f_abs_sub_vir = vector<double>(n_subhalos); // Subhalo dust absorption fractions
        f2_abs_sub_vir = vector<double>(n_subhalos); // Subhalo squared dust absorption fractions
        n_photons_abs_sub_vir = vector<double>(n_subhalos); // Effective number of absorbed photons: 1/<w>
        // HI properties
        Ndot_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI photon rates [photons/s]
        f_src_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI source fractions: sum(w0)
        f2_src_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI squared source fractions: sum(w0^2)
        n_photons_src_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI effective number of emitted photons: 1/<w0>
        f_esc_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI escape fractions
        f2_esc_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI squared escape fractions
        n_photons_esc_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI effective number of escaped photons: 1/<w>
        f_abs_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI dust absorption fractions
        f2_abs_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI squared dust absorption fractions
        n_photons_abs_HI_sub_vir = vector<double>(n_subhalos); // Subhalo HI effective number of absorbed photons: 1/<w>
        // Bin properties
        bin_Ndot_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin photon rates [photons/s]
        bin_f_src_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin source fractions: sum(w0)
        bin_f2_src_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin squared source fractions: sum(w0^2)
        bin_n_photons_src_sub_vir = Image(n_subhalos, n_bins); // Effective number of emitted photons: 1/<w0>
        bin_f_esc_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin escape fractions
        bin_f2_esc_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin squared escape fractions
        bin_n_photons_esc_sub_vir = Image(n_subhalos, n_bins); // Effective number of escaped photons: 1/<w>
        bin_f_abs_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin dust absorption fractions
        bin_f2_abs_sub_vir = Image(n_subhalos, n_bins); // Subhalo bin squared dust absorption fractions
        bin_n_photons_abs_sub_vir = Image(n_subhalos, n_bins); // Effective number of absorbed photons: 1/<w>
      }
      if (output_sub_gal) {
        Ndot_sub_gal = vector<double>(n_subhalos); // Subhalo photon rates [photons/s]
        f_src_sub_gal = vector<double>(n_subhalos); // Subhalo source fractions: sum(w0)
        f2_src_sub_gal = vector<double>(n_subhalos); // Subhalo squared source fractions: sum(w0^2)
        n_photons_src_sub_gal = vector<double>(n_subhalos); // Effective number of emitted photons: 1/<w0>
        f_esc_sub_gal = vector<double>(n_subhalos); // Subhalo escape fractions
        f2_esc_sub_gal = vector<double>(n_subhalos); // Subhalo squared escape fractions
        n_photons_esc_sub_gal = vector<double>(n_subhalos); // Effective number of escaped photons: 1/<w>
        f_abs_sub_gal = vector<double>(n_subhalos); // Subhalo dust absorption fractions
        f2_abs_sub_gal = vector<double>(n_subhalos); // Subhalo squared dust absorption fractions
        n_photons_abs_sub_gal = vector<double>(n_subhalos); // Effective number of absorbed photons: 1/<w>
        // HI properties
        Ndot_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI photon rates [photons/s]
        f_src_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI source fractions: sum(w0)
        f2_src_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI squared source fractions: sum(w0^2)
        n_photons_src_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI effective number of emitted photons: 1/<w0>
        f_esc_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI escape fractions
        f2_esc_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI squared escape fractions
        n_photons_esc_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI effective number of escaped photons: 1/<w>
        f_abs_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI dust absorption fractions
        f2_abs_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI squared dust absorption fractions
        n_photons_abs_HI_sub_gal = vector<double>(n_subhalos); // Subhalo HI effective number of absorbed photons: 1/<w>
        // Bin properties
        bin_Ndot_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin photon rates [photons/s]
        bin_f_src_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin source fractions: sum(w0)
        bin_f2_src_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin squared source fractions: sum(w0^2)
        bin_n_photons_src_sub_gal = Image(n_subhalos, n_bins); // Effective number of emitted photons: 1/<w0>
        bin_f_esc_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin escape fractions
        bin_f2_esc_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin squared escape fractions
        bin_n_photons_esc_sub_gal = Image(n_subhalos, n_bins); // Effective number of escaped photons: 1/<w>
        bin_f_abs_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin dust absorption fractions
        bin_f2_abs_sub_gal = Image(n_subhalos, n_bins); // Subhalo bin squared dust absorption fractions
        bin_n_photons_abs_sub_gal = Image(n_subhalos, n_bins); // Effective number of absorbed photons: 1/<w>
      }
    }
    if (n_usubhalos > 0) {                   // Have unfiltered subhalos
      Ndot_usub = vector<double>(n_usubhalos); // Unfiltered subhalo photon rates [photons/s]
      f_src_usub = vector<double>(n_usubhalos); // Unfiltered subhalo source fractions: sum(w0)
      f2_src_usub = vector<double>(n_usubhalos); // Unfiltered subhalo squared source fractions: sum(w0^2)
      n_photons_src_usub = vector<double>(n_usubhalos); // Effective number of emitted photons: 1/<w0>
      // HI properties
      Ndot_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI photon rates [photons/s]
      f_src_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI source fractions: sum(w0)
      f2_src_HI_usub = vector<double>(n_usubhalos); // Unfiltered subhalo HI squared source fractions: sum(w0^2)
      n_photons_src_HI_usub = vector<double>(n_usubhalos); // Effective number of emitted photons: 1/<w0>
      // Bin properties
      bin_Ndot_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo photon rates [photons/s]
      bin_f_src_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo source fractions: sum(w0)
      bin_f2_src_usub = Image(n_usubhalos, n_bins); // Unfiltered subhalo squared source fractions: sum(w0^2)
      bin_n_photons_src_usub = Image(n_usubhalos, n_bins); // Effective number of emitted photons: 1/<w0>
    }
  }

  // Setup cameras
  if (have_cameras || output_radial_avg || output_radial_cube_avg || output_radial_map || output_cube_map ||
      output_radial_map_grp || output_radial_avg_grp || output_radial_map_sub || output_radial_avg_sub)
    setup_cameras();                         // General camera setup (radial maps need centering)
  if (have_cameras) {
    // Reset camera data and initialize with zeros
    if (output_escape_fractions)
      f_escs = vector<double>(n_cameras);    // Escape fractions
    if (output_bin_escape_fractions)
      bin_f_escs = vectors(n_cameras, vector<double>(n_bins)); // Bin escape fractions
    if (output_images)
      images = Images(n_cameras, Image(nx_pixels, ny_pixels));
    if (output_cubes)
      bin_images = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_bins));
    if (output_radial_images)
      radial_images = vectors(n_cameras, vector<double>(n_radial_pixels));
    if (output_radial_cubes)
      bin_radial_images = Images(n_cameras, Image(n_radial_cube_pixels, n_bins));

    // Initialize intrinsic cameras (mcrt)
    if (output_mcrt_emission) {
      if (output_images)
        images_int = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_cubes)
        bin_images_int = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_bins));
      if (output_radial_images)
        radial_images_int = vectors(n_cameras, vector<double>(n_radial_pixels));
      if (output_radial_cubes)
        bin_radial_images_int = Images(n_cameras, Image(n_radial_cube_pixels, n_bins));
    }

    // Initialize attenuation cameras (mcrt)
    if (output_mcrt_attenuation) {
      if (output_escape_fractions)
        f_escs_ext = vector<double>(n_cameras); // Escape fractions
      if (output_images)
        images_ext = Images(n_cameras, Image(nx_pixels, ny_pixels));
      if (output_cubes)
        bin_images_ext = Cubes(n_cameras, Cube(nx_cube_pixels, ny_cube_pixels, n_bins));
      if (output_radial_images)
        radial_images_ext = vectors(n_cameras, vector<double>(n_radial_pixels));
      if (output_radial_cubes)
        bin_radial_images_ext = Images(n_cameras, Image(n_radial_cube_pixels, n_bins));
    }
  }

  // Setup non-camera quantities
  if (output_radial_avg)
    radial_avg = vector<double>(n_radial_pixels); // Angle-averaged radial surface brightness
  if (output_radial_cube_avg)
    radial_cube_avg = Image(n_radial_cube_pixels, n_bins); // Angle-averaged bin radial surface brightness

  // Initialize line-of-sight healpix maps
  if (output_map) {
    const int n_pix_map = 12 * n_side_map * n_side_map; // Number of pixels
    map = vector<double>(n_pix_map);
    if (output_map2)
      map2 = vector<double>(n_pix_map);
  }

  // Initialize line-of-sight healpix group maps
  if (output_map_grp) {
    const int n_pix_grp = 12 * n_side_grp * n_side_grp; // Number of pixels
    if (output_grp_obs)
      map_grp = Image(n_groups, n_pix_grp);
    if (output_grp_vir)
      map_grp_vir = Image(n_groups, n_pix_grp);
    if (output_grp_gal)
      map_grp_gal = Image(n_groups, n_pix_grp);
    if (output_map2_grp) {
      if (output_grp_obs)
        map2_grp = Image(n_groups, n_pix_grp);
      if (output_grp_vir)
        map2_grp_vir = Image(n_groups, n_pix_grp);
      if (output_grp_gal)
        map2_grp_gal = Image(n_groups, n_pix_grp);
    }
  }

  // Initialize line-of-sight healpix subhalo maps
  if (output_map_sub) {
    const int n_pix_sub = 12 * n_side_sub * n_side_sub; // Number of pixels
    if (output_sub_obs)
      map_sub = Image(n_subhalos, n_pix_sub);
    if (output_sub_vir)
      map_sub_vir = Image(n_subhalos, n_pix_sub);
    if (output_sub_gal)
      map_sub_gal = Image(n_subhalos, n_pix_sub);
    if (output_map2_sub) {
      if (output_sub_obs)
        map2_sub = Image(n_subhalos, n_pix_sub);
      if (output_sub_vir)
        map2_sub_vir = Image(n_subhalos, n_pix_sub);
      if (output_sub_gal)
        map2_sub_gal = Image(n_subhalos, n_pix_sub);
    }
  }

  // Initialize line-of-sight healpix radial map
  if (output_radial_map) {
    const int n_pix_radial = 12 * n_side_radial * n_side_radial; // 12 * n_side^2
    radial_map = Image(n_pix_radial, n_map_pixels);
  }

  // Initialize line-of-sight healpix radial cube map
  if (output_cube_map) {
    const int n_pix_cube = 12 * n_side_cube * n_side_cube; // 12 * n_side^2
    cube_map = Cube(n_pix_cube, n_map_pixels, n_bins);
  }

  // Initialize line-of-sight healpix radial group maps
  if (output_radial_map_grp) {
    const int n_pix_radial_grp = 12 * n_side_radial_grp * n_side_radial_grp; // 12 * n_side^2
    radial_map_grp = Cube(n_groups, n_pix_radial_grp, n_map_pixels_grp);
  }

  // Initialize angle-averaged radial group profiles
  if (output_radial_avg_grp)
    radial_avg_grp = Image(n_groups, n_pixels_grp);

  // Initialize line-of-sight healpix radial subhalo maps
  if (output_radial_map_sub) {
    const int n_pix_radial_sub = 12 * n_side_radial_sub * n_side_radial_sub; // 12 * n_side^2
    radial_map_sub = Cube(n_subhalos, n_pix_radial_sub, n_map_pixels_sub);
  }

  // Initialize angle-averaged radial subhalo profiles
  if (output_radial_avg_sub)
    radial_avg_sub = Image(n_subhalos, n_pixels_sub);

  // Reserve star buffers
  if (output_stars) {
    source_weight_stars = vector<double>(n_stars); // Photon weight at emission for each star
    weight_stars = vector<double>(n_stars);  // Photon weight at escape for each star
    if (output_grp_vir)
      weight_stars_grp_vir = vector<double>(n_stars); // Photon weight at escape for each star (group virial)
    if (output_grp_gal)
      weight_stars_grp_gal = vector<double>(n_stars); // Photon weight at escape for each star (group galaxy)
    if (output_sub_vir)
      weight_stars_sub_vir = vector<double>(n_stars); // Photon weight at escape for each star (subhalo virial)
    if (output_sub_gal)
      weight_stars_sub_gal = vector<double>(n_stars); // Photon weight at escape for each star (subhalo galaxy)
  }

  // Reserve cell buffers
  if (output_cells) {
    source_weight_cells = vector<double>(n_cells); // Photon weight at emission for each cell
    weight_cells = vector<double>(n_cells);  // Photon weight at escape for each cell
  }

  // UV background ionizations for each cell [events/s]
  if (output_cells_UVB_HI)
    Ndot_UVB_HI_cells.resize(n_cells);
  // Recombinations for each cell [events/s]
  Ndot_rec_ions_cells.resize(n_rec_ions_cells);
  for (int i = 0; i < n_rec_ions_cells; ++i)
    Ndot_rec_ions_cells[i].resize(n_cells);
  // Collisional ionizations for each cell [events/s]
  Ndot_col_ions_cells.resize(n_col_ions_cells);
  for (int i = 0; i < n_col_ions_cells; ++i)
    Ndot_col_ions_cells[i].resize(n_cells);
  // Photoionizations for each cell [events/s]
  Ndot_phot_ions_cells.resize(n_phot_ions_cells);
  for (int i = 0; i < n_phot_ions_cells; ++i)
    Ndot_phot_ions_cells[i].resize(n_cells);
  // Charge exchange ionizations (H) for each cell [events/s]
  Ndot_cxi_H_ions_cells.resize(n_cxi_H_ions_cells);
  for (int i = 0; i < n_cxi_H_ions_cells; ++i)
    Ndot_cxi_H_ions_cells[i].resize(n_cells);
  // Charge exchange ionizations (He) for each cell [events/s]
  Ndot_cxi_He_ions_cells.resize(n_cxi_He_ions_cells);
  for (int i = 0; i < n_cxi_He_ions_cells; ++i)
    Ndot_cxi_He_ions_cells[i].resize(n_cells);
  // Charge exchange recombinations (H) for each cell [events/s]
  Ndot_cxr_H_ions_cells.resize(n_cxr_H_ions_cells);
  for (int i = 0; i < n_cxr_H_ions_cells; ++i)
    Ndot_cxr_H_ions_cells[i].resize(n_cells);
  // Charge exchange recombinations (He) for each cell [events/s]
  Ndot_cxr_He_ions_cells.resize(n_cxr_He_ions_cells);
  for (int i = 0; i < n_cxr_He_ions_cells; ++i)
    Ndot_cxr_He_ions_cells[i].resize(n_cells);
  // Local ionization coupling: Species photoionization rate integrals [1/s]
  rate_ions.resize(n_active_ions);
  for (auto& rate_ion : rate_ions)
    rate_ion = vector<double>(n_cells);
  if (output_photoheating)
    G_ion = vector<double>(n_cells);         // Photoheating rate [erg/s]
  if (output_photon_density)
    bin_n_gamma = Image(n_cells, n_bins);    // Photon density [photons/cm^3]
  // Number of absorptions [photons/s]
  if constexpr (output_ion_cell_age_freq)
    ion_cell_age_freq.setup();               // Cell, age, and frequency
  if constexpr (output_ion_radial_age_freq)
    ion_radial_age_freq.setup();             // Radial, age, and frequency
  if constexpr (output_ion_distance_age_freq)
    ion_distance_age_freq.setup();           // Distance, age, and frequency
  if constexpr (output_radial_flow)
    radial_flow.setup();                     // Radial flows
  if constexpr (output_group_flows)
    for (auto& group_flow : group_flows)
      group_flow.setup();                    // Group flows
  if constexpr (output_subhalo_flows)
    for (auto& subhalo_flow : subhalo_flows)
      subhalo_flow.setup();                  // Subhalo flows

  // Convergence history
  x_V_ions.resize(n_active_ions_atoms);      // Volume-weighted fractions
  for (auto& x_V_ion : x_V_ions)
    x_V_ion.reserve(max_iter);
  x_m_ions.resize(n_active_ions_atoms);      // Mass-weighted fractions
  for (auto& x_m_ion : x_m_ions)
    x_m_ion.reserve(max_iter);
  f_col_HI.reserve(max_iter);                // Fraction of total ionizations
  f_UVB_HI.reserve(max_iter);
  rec_ions.resize(n_active_ions);            // Recombinations [events/s]
  for (auto& rec_ion : rec_ions)
    rec_ion.reserve(max_iter);
  col_ions.resize(n_active_ions);            // Collisional ionizations [events/s]
  for (auto& col_ion : col_ions)
    col_ion.reserve(max_iter);
  phot_ions.resize(n_active_ions);           // Photoionizations [events/s]
  for (auto& phot_ion : phot_ions)
    phot_ion.reserve(max_iter);
  cxi_H_ions.resize(n_active_ions);          // Charge exchange ionizations (H) [events/s]
  for (auto& cxi_H_ion : cxi_H_ions)
    cxi_H_ion.reserve(max_iter);
  cxi_He_ions.resize(n_active_ions);         // Charge exchange ionizations (He) [events/s]
  for (auto& cxi_He_ion : cxi_He_ions)
    cxi_He_ion.reserve(max_iter);
  cxr_H_ions.resize(n_active_ions);          // Charge exchange recombinations (H) [events/s]
  for (auto& cxr_H_ion : cxr_H_ions)
    cxr_H_ion.reserve(max_iter);
  cxr_He_ions.resize(n_active_ions);         // Charge exchange recombinations (He) [events/s]
  for (auto& cxr_He_ion : cxr_He_ions)
    cxr_He_ion.reserve(max_iter);

  // Assign luminosity and opacity functions
  assign_spectra();

  // UV background field
  if (UVB_model == "FG11")
    setup_UVB_FG11();                        // Initialize UVB parameters
  else if (have_UVB)
    root_error("Requested UV background model is not implemented: " + UVB_model);
  if (self_shielding)
    setup_self_shielding();                  // Initialize SS parameters

  // Setup star-based emission sources
  if (star_based_emission) {
    initial_setup_stars();                   // Setup that is performed once
    iterative_setup_stars();                 // Setup that is performed during each iteration
  }

  // Setup cell-based emission sources
  if (cell_based_emission) {
    initial_setup_gas();                     // Setup that is performed once
    iterative_setup_gas();                   // Setup that is performed during each iteration
  }

  // Setup an AGN emission source
  if (AGN_emission) {
    initial_setup_AGN();                     // Setup that is performed once
    iterative_setup_AGN();                   // Setup that is performed during each iteration
  }

  // Setup a point source
  if (point_source) {
    // Point source insertion
    i_point = find_cell(r_point, 0);         // Start with default cell = 0
    iterative_setup_point();                 // Setup that is performed during each iteration
  }

  // Setup a plane source
  if (plane_source) {
    const double cX = plane_center_x_bbox * bbox[0].x + (1. - plane_center_x_bbox) * bbox[1].x; // Box center [cm]
    const double cY = plane_center_y_bbox * bbox[0].y + (1. - plane_center_y_bbox) * bbox[1].y;
    const double cZ = plane_center_z_bbox * bbox[0].z + (1. - plane_center_z_bbox) * bbox[1].z;
    const double rX = plane_radius_x_bbox * (bbox[1].x - bbox[0].x); // Box radius [cm]
    const double rY = plane_radius_y_bbox * (bbox[1].y - bbox[0].y);
    const double rZ = plane_radius_z_bbox * (bbox[1].z - bbox[0].z);
    switch (plane_type) {                    // Determine the plane properties
      case PosX:                             // +x direction
        plane_position = bbox[0].x + 1e-12*rX;
        plane_center_1 = cY;
        plane_center_2 = cZ;
        plane_radius_1 = rY;
        plane_radius_2 = rZ;
        plane_area = rY * rZ;
        break;
      case NegX:                             // -x direction
        plane_position = bbox[1].x - 1e-12*rX;
        plane_center_1 = cY;
        plane_center_2 = cZ;
        plane_radius_1 = rY;
        plane_radius_2 = rZ;
        plane_area = rY * rZ;
        break;
      case PosY:                             // +y direction
        plane_position = bbox[0].y + 1e-12*rY;
        plane_center_1 = cX;
        plane_center_2 = cZ;
        plane_radius_1 = rX;
        plane_radius_2 = rZ;
        plane_area = rX * rZ;
        break;
      case NegY:                             // -y direction
        plane_position = bbox[1].y - 1e-12*rY;
        plane_center_1 = cX;
        plane_center_2 = cZ;
        plane_radius_1 = rX;
        plane_radius_2 = rZ;
        plane_area = rX * rZ;
        break;
      case PosZ:                             // +z direction
        plane_position = bbox[0].z + 1e-12*rZ;
        plane_center_1 = cX;
        plane_center_2 = cY;
        plane_radius_1 = rX;
        plane_radius_2 = rY;
        plane_area = rX * rY;
        break;
      case NegZ:                             // -z direction
        plane_position = bbox[1].z - 1e-12*rZ;
        plane_center_1 = cX;
        plane_center_2 = cY;
        plane_radius_1 = rX;
        plane_radius_2 = rY;
        plane_area = rX * rY;
        break;
      default:
        error("Unrecognized plane direction type.");
    }
    plane_area *= plane_beam ? M_PI : 4.;    // Turn plane into a ellipsoidal beam
    if (Sbol_plane > 0.) {                   // Read Lbol as a surface density
      Lbol_plane *= plane_area;              // Plane bolometric luminosity [erg/s]
      ion_plane.L *= plane_area;             // Plane source luminosity [erg/s]
      ion_plane.Ndot *= plane_area;          // Plane source rate [photons/s]
      ion_plane.boosted_Ndot *= plane_area;  // Plane source rate [photons/s]
      #pragma omp parallel for
      for (int i = 0; i < n_bins; ++i) {
        ion_plane.bin_L[i] *= plane_area;    // Bin luminosities [erg/s]
        ion_plane.bin_Ndot[i] *= plane_area; // Bin rates [photons/s]
        for (int ai = 0; ai < n_active_ions; ++ai) {
          ion_plane.sigma_Ndot_ions[ai][i] *= plane_area; // Rate cross-sections [cm^2 photons/s]
          ion_plane.sigma_L_ions[ai][i] *= plane_area; // Heating cross-sections [cm^2 erg/s]
          ion_plane.sigma_epsilon_ions[ai][i] *= plane_area; // Excess heating cross-sections [cm^2 erg/s]
        }
        for (int j = 0; j < n_dust_species; ++j) {
          ion_plane.kappas_Ndot[j][i] *= plane_area; // Dust opacity rate [cm^2/g dust photons/s]
          ion_plane.albedos_Ndot[j][i] *= plane_area; // Dust albedo rate for scattering vs. absorption [photons/s]
          ion_plane.cosines_Ndot[j][i] *= plane_area; // Anisotropy parameter rate: <μ> for dust scattering [photons/s]
        }
      }
      ion_plane.HI.L *= plane_area;          // HI source luminosity [erg/s]
      ion_plane.HI.Ndot *= plane_area;       // HI source rate [photons/s]
    } else {
      Sbol_plane = Lbol_plane / plane_area;  // Assign the surface density
    }
    iterative_setup_plane();                 // Setup that is performed during each iteration
  }

  // Setup mixed source properties (names, flags, and probabilities)
  const array<string, MAX_ION_SOURCE_TYPES> source_names = {"stars", "cells", "AGN", "point", "plane"};
  const array<bool, MAX_ION_SOURCE_TYPES> source_flags = {star_based_emission, cell_based_emission, AGN_emission, point_source, plane_source};
  n_ion_source_types = 0;
  for (int i = 0; i < MAX_ION_SOURCE_TYPES; ++i)
    if (source_flags[i]) {
      ion_source_types_mask.push_back(i);
      ion_source_names_mask.push_back(source_names[i]);
      n_ion_source_types++;
    }
  if (n_ion_source_types == 0)
    root_error("No source types have been specified.");
  mixed_ion_pdf_mask = vector<double>(n_ion_source_types); // Mixed source pdf (masked)
  mixed_ion_cdf_mask = vector<double>(n_ion_source_types); // Mixed source cdf (masked)
  mixed_ion_weights_mask = vector<double>(n_ion_source_types); // Mixed source weights (masked)
  iterative_setup_calc();                    // Setup that is performed during each iteration

  // Reserve photon output buffers
  if (output_photons) {
    source_ids.resize(n_photons);            // Emission cell or star index
    if (n_ion_source_types > 1)
      source_types.resize(n_photons);        // Emission type specifier
    source_weights.resize(n_photons);        // Photon weight at emission
    if (output_n_scat)
      n_scats.resize(n_photons);             // Number of scattering events
    freq_bins.resize(n_photons);             // Frequency at escape [bin]
    weights.resize(n_photons);               // Photon weight at escape
    dust_weights.resize(n_photons);          // Weight removed by dust absorption
    // Weight removed by the ionization of each species
    ion_weights.resize(n_active_ion_weights); // Ionization weights
    for (auto& ion_weight : ion_weights)
      ion_weight.resize(n_photons);
    if (output_absorption_distance) dist_abss.resize(n_photons); // Photon absorption distance [cm]
    if (output_source_position) source_positions.resize(n_photons); // Position at emission [cm]
    positions.resize(n_photons);             // Position at escape [cm]
    directions.resize(n_photons);            // Direction at escape
  }
}

/* Setup a spatially uniform UV background based on the Faucher-Giguere model (2011 version). */
void Ionization::setup_UVB_FG11() {
  const double logz = log10(z + 1.);         // Log of current redshift
  const int n_logz = 214;                    // Size of logspace table
  const array<double,n_logz> gHIs = {3.76244e-14, 3.83213e-14, 3.90303e-14, 4.01290e-14, // Rate for HI [1/s]
    4.16007e-14, 4.31222e-14, 4.47007e-14, 4.63453e-14, 4.80462e-14, 4.98113e-14, 5.16490e-14,
    5.35503e-14, 5.55239e-14, 5.75769e-14, 5.97016e-14, 6.19076e-14, 6.42009e-14, 6.65751e-14,
    6.90410e-14, 7.16025e-14, 7.42548e-14, 7.70100e-14, 7.98695e-14, 8.28307e-14, 8.59068e-14,
    8.90972e-14, 9.24012e-14, 9.58331e-14, 9.94039e-14, 1.03087e-13, 1.06911e-13, 1.10885e-13,
    1.14984e-13, 1.19226e-13, 1.23641e-13, 1.28194e-13, 1.32903e-13, 1.37801e-13, 1.42848e-13,
    1.48065e-13, 1.53480e-13, 1.59057e-13, 1.64817e-13, 1.70784e-13, 1.76923e-13, 1.83257e-13,
    1.89805e-13, 1.96533e-13, 2.03467e-13, 2.10616e-13, 2.17951e-13, 2.25498e-13, 2.33258e-13,
    2.41203e-13, 2.49362e-13, 2.57724e-13, 2.66267e-13, 2.75018e-13, 2.83999e-13, 2.93102e-13,
    3.02401e-13, 3.11903e-13, 3.21502e-13, 3.31240e-13, 3.41165e-13, 3.51160e-13, 3.61251e-13,
    3.71488e-13, 3.81738e-13, 3.92027e-13, 4.02401e-13, 4.12718e-13, 4.22998e-13, 4.33286e-13,
    4.43424e-13, 4.53438e-13, 4.63352e-13, 4.73011e-13, 4.82441e-13, 4.91671e-13, 5.00591e-13,
    5.09267e-13, 5.17733e-13, 5.25902e-13, 5.33842e-13, 5.41633e-13, 5.48998e-13, 5.56077e-13,
    5.62935e-13, 5.69306e-13, 5.75296e-13, 5.80996e-13, 5.86202e-13, 5.90973e-13, 5.95411e-13,
    5.99308e-13, 6.02729e-13, 6.05783e-13, 6.08265e-13, 6.10246e-13, 6.11843e-13, 6.12855e-13,
    6.13357e-13, 6.13477e-13, 6.13017e-13, 6.12060e-13, 6.10742e-13, 6.08870e-13, 6.06530e-13,
    6.03867e-13, 6.00695e-13, 5.97115e-13, 5.93252e-13, 5.88951e-13, 5.84315e-13, 5.79553e-13,
    5.74327e-13, 5.68850e-13, 5.63301e-13, 5.57385e-13, 5.51270e-13, 5.45110e-13, 5.38750e-13,
    5.32273e-13, 5.25828e-13, 5.19261e-13, 5.12652e-13, 5.06142e-13, 4.99581e-13, 4.93041e-13,
    4.86655e-13, 4.80274e-13, 4.73961e-13, 4.67838e-13, 4.61761e-13, 4.55782e-13, 4.50010e-13,
    4.44306e-13, 4.38712e-13, 4.33323e-13, 4.28006e-13, 4.22791e-13, 4.17762e-13, 4.12791e-13,
    4.07901e-13, 4.03235e-13, 3.98514e-13, 3.93836e-13, 3.89305e-13, 3.84682e-13, 3.80033e-13,
    3.75380e-13, 3.70629e-13, 3.65741e-13, 3.60711e-13, 3.55487e-13, 3.50126e-13, 3.44716e-13,
    3.39250e-13, 3.33815e-13, 3.28484e-13, 3.23176e-13, 3.17893e-13, 3.12648e-13, 3.07359e-13,
    3.02040e-13, 2.96708e-13, 2.91291e-13, 2.85810e-13, 2.80289e-13, 2.74664e-13, 2.68965e-13,
    2.63257e-13, 2.57409e-13, 2.51476e-13, 2.45535e-13, 2.39468e-13, 2.33338e-13, 2.27174e-13,
    2.20960e-13, 2.14712e-13, 2.08469e-13, 2.02191e-13, 1.95911e-13, 1.89667e-13, 1.83417e-13,
    1.77197e-13, 1.71041e-13, 1.64910e-13, 1.58837e-13, 1.52855e-13, 1.46927e-13, 1.41081e-13,
    1.35349e-13, 1.29695e-13, 1.24146e-13, 1.18727e-13, 1.13406e-13, 1.07857e-13, 1.00976e-13,
    9.28557e-14, 8.37879e-14, 7.40917e-14, 6.40348e-14, 5.39463e-14, 4.41478e-14, 3.48885e-14,
    2.64282e-14, 1.89767e-14, 1.26822e-14, 7.64449e-15, 3.90686e-15, 1.45165e-15, 2.10205e-16};
  const array<double,n_logz> gHeIs = {2.08210e-14, 2.12572e-14, 2.17025e-14, 2.23918e-14, // Rate for HeI [1/s]
    2.33198e-14, 2.42879e-14, 2.52975e-14, 2.63568e-14, 2.74619e-14, 2.86143e-14, 2.98232e-14,
    3.10841e-14, 3.23989e-14, 3.37776e-14, 3.52155e-14, 3.67145e-14, 3.82859e-14, 3.99246e-14,
    4.16331e-14, 4.34246e-14, 4.52916e-14, 4.72369e-14, 4.92760e-14, 5.13987e-14, 5.36075e-14,
    5.59217e-14, 5.83284e-14, 6.08297e-14, 6.34491e-14, 6.61700e-14, 6.89941e-14, 7.19495e-14,
    7.50163e-14, 7.81946e-14, 8.15181e-14, 8.49624e-14, 8.85268e-14, 9.22523e-14, 9.61096e-14,
    1.00098e-13, 1.04261e-13, 1.08567e-13, 1.13014e-13, 1.17645e-13, 1.22429e-13, 1.27358e-13,
    1.32482e-13, 1.37766e-13, 1.43200e-13, 1.48839e-13, 1.54642e-13, 1.60595e-13, 1.66759e-13,
    1.73085e-13, 1.79553e-13, 1.86235e-13, 1.93073e-13, 2.00039e-13, 2.07216e-13, 2.14538e-13,
    2.21968e-13, 2.29599e-13, 2.37362e-13, 2.45206e-13, 2.53235e-13, 2.61371e-13, 2.69554e-13,
    2.77895e-13, 2.86311e-13, 2.94728e-13, 3.03264e-13, 3.11833e-13, 3.20347e-13, 3.28932e-13,
    3.37495e-13, 3.45939e-13, 3.54390e-13, 3.62758e-13, 3.70930e-13, 3.79047e-13, 3.87039e-13,
    3.94800e-13, 4.02487e-13, 4.10041e-13, 4.17334e-13, 4.24530e-13, 4.31557e-13, 4.38264e-13,
    4.44821e-13, 4.51181e-13, 4.57167e-13, 4.62960e-13, 4.68531e-13, 4.73684e-13, 4.78604e-13,
    4.83282e-13, 4.87503e-13, 4.91460e-13, 4.95162e-13, 4.98382e-13, 5.01314e-13, 5.03994e-13,
    5.06176e-13, 5.08061e-13, 5.09709e-13, 5.10859e-13, 5.11711e-13, 5.12361e-13, 5.12524e-13,
    5.12396e-13, 5.12115e-13, 5.11372e-13, 5.10365e-13, 5.09259e-13, 5.07720e-13, 5.05921e-13,
    5.04102e-13, 5.01885e-13, 4.99419e-13, 4.97035e-13, 4.94306e-13, 4.91369e-13, 4.88596e-13,
    4.85527e-13, 4.82286e-13, 4.79277e-13, 4.76017e-13, 4.72606e-13, 4.69496e-13, 4.66170e-13,
    4.62701e-13, 4.59597e-13, 4.56306e-13, 4.52855e-13, 4.49830e-13, 4.46633e-13, 4.43238e-13,
    4.40324e-13, 4.37247e-13, 4.33906e-13, 4.31098e-13, 4.28129e-13, 4.24803e-13, 4.22061e-13,
    4.19156e-13, 4.15595e-13, 4.12787e-13, 4.09782e-13, 4.05942e-13, 4.02955e-13, 3.99770e-13,
    3.95765e-13, 3.92516e-13, 3.89054e-13, 3.84702e-13, 3.80985e-13, 3.77102e-13, 3.72393e-13,
    3.68308e-13, 3.64184e-13, 3.59324e-13, 3.55056e-13, 3.50761e-13, 3.45703e-13, 3.41150e-13,
    3.36556e-13, 3.31186e-13, 3.26269e-13, 3.21311e-13, 3.15582e-13, 3.10280e-13, 3.04954e-13,
    2.98712e-13, 2.92929e-13, 2.87174e-13, 2.80426e-13, 2.74242e-13, 2.68097e-13, 2.61268e-13,
    2.54865e-13, 2.48500e-13, 2.41648e-13, 2.35119e-13, 2.28635e-13, 2.21755e-13, 2.15178e-13,
    2.08681e-13, 2.01855e-13, 1.95334e-13, 1.88923e-13, 1.82243e-13, 1.75871e-13, 1.69638e-13,
    1.63189e-13, 1.57052e-13, 1.51075e-13, 1.44932e-13, 1.39099e-13, 1.33017e-13, 1.24981e-13,
    1.15548e-13, 1.04913e-13, 9.31059e-14, 8.08920e-14, 6.85984e-14, 5.63508e-14, 4.47794e-14,
    3.41487e-14, 2.46605e-14, 1.65843e-14, 1.00704e-14, 5.17956e-15, 1.93818e-15, 2.82933e-16};
  const array<double,n_logz> gHeIIs = {1.12165e-16, 1.14812e-16, 1.17562e-16, 1.21829e-16, // Rate for HeII [1/s]
    1.27598e-16, 1.33706e-16, 1.40176e-16, 1.47032e-16, 1.54289e-16, 1.61972e-16, 1.70114e-16,
    1.78734e-16, 1.87857e-16, 1.97519e-16, 2.07747e-16, 2.18571e-16, 2.30028e-16, 2.42151e-16,
    2.54978e-16, 2.68549e-16, 2.82897e-16, 2.98071e-16, 3.14118e-16, 3.31072e-16, 3.48986e-16,
    3.67918e-16, 3.87904e-16, 4.09000e-16, 4.31274e-16, 4.54764e-16, 4.79529e-16, 5.05647e-16,
    5.33157e-16, 5.62124e-16, 5.92630e-16, 6.24716e-16, 6.58451e-16, 6.93928e-16, 7.31182e-16,
    7.70282e-16, 8.11311e-16, 8.54314e-16, 8.99354e-16, 9.46514e-16, 9.95835e-16, 1.04737e-15,
    1.10120e-15, 1.15735e-15, 1.21586e-15, 1.27681e-15, 1.34020e-15, 1.40606e-15, 1.47444e-15,
    1.54532e-15, 1.61871e-15, 1.69462e-15, 1.77301e-15, 1.85382e-15, 1.93707e-15, 2.02262e-15,
    2.11041e-15, 2.20036e-15, 2.29232e-15, 2.38612e-15, 2.48163e-15, 2.57862e-15, 2.67684e-15,
    2.77606e-15, 2.87595e-15, 2.97615e-15, 3.07631e-15, 3.17597e-15, 3.27461e-15, 3.37175e-15,
    3.46672e-15, 3.55884e-15, 3.64744e-15, 3.73165e-15, 3.81065e-15, 3.88427e-15, 3.95257e-15,
    4.01573e-15, 4.07414e-15, 4.12820e-15, 4.17807e-15, 4.22358e-15, 4.26440e-15, 4.30025e-15,
    4.33092e-15, 4.35615e-15, 4.37567e-15, 4.38935e-15, 4.39696e-15, 4.39830e-15, 4.39330e-15,
    4.38180e-15, 4.36368e-15, 4.33895e-15, 4.30754e-15, 4.26941e-15, 4.22468e-15, 4.17338e-15,
    4.11558e-15, 4.05149e-15, 3.98125e-15, 3.90504e-15, 3.82314e-15, 3.73584e-15, 3.64337e-15,
    3.54613e-15, 3.44448e-15, 3.33879e-15, 3.22950e-15, 3.11706e-15, 3.00188e-15, 2.88443e-15,
    2.76522e-15, 2.64469e-15, 2.52330e-15, 2.40159e-15, 2.27998e-15, 2.15895e-15, 2.03896e-15,
    1.92042e-15, 1.80373e-15, 1.68929e-15, 1.57745e-15, 1.46852e-15, 1.36282e-15, 1.26063e-15,
    1.16216e-15, 1.06765e-15, 9.77272e-16, 8.91151e-16, 8.09431e-16, 7.32188e-16, 6.59421e-16,
    5.91164e-16, 5.27374e-16, 4.67931e-16, 4.12768e-16, 3.61758e-16, 3.14704e-16, 2.71484e-16,
    2.31925e-16, 1.95774e-16, 1.62846e-16, 1.32991e-16, 1.06079e-16, 8.22042e-17, 6.15944e-17,
    4.44715e-17, 3.08870e-17, 2.06961e-17, 1.34960e-17, 8.69481e-18, 5.63078e-18, 3.70294e-18,
    2.48255e-18, 1.70973e-18, 1.21531e-18, 8.84193e-19, 6.53144e-19, 4.87505e-19, 3.66698e-19,
    2.77156e-19, 2.10272e-19, 1.60006e-19, 1.22011e-19, 9.31725e-20, 7.12932e-20, 5.46017e-20,
    4.18682e-20, 3.21503e-20, 2.47075e-20, 1.90054e-20, 1.46331e-20, 1.12719e-20, 8.68398e-21,
    6.69136e-21, 5.15238e-21, 3.96403e-21, 3.04862e-21, 2.33985e-21, 1.79111e-21, 1.36828e-21,
    1.04232e-21, 7.91355e-22, 5.99161e-22, 4.52039e-22, 3.39658e-22, 2.54299e-22, 1.89565e-22,
    1.40628e-22, 1.03871e-22, 7.63165e-23, 5.57465e-23, 4.05127e-23, 2.91731e-23, 2.05667e-23,
    1.41855e-23, 9.55468e-24, 6.26749e-24, 4.00081e-24, 2.47791e-24, 1.48254e-24, 8.53030e-25,
    4.68670e-25, 2.42781e-25, 1.16647e-25, 5.03609e-26, 1.83440e-26, 4.84395e-27, 4.96681e-28};
  const array<double,n_logz> eHIs = {2.47740e-25, 2.52581e-25, 2.57518e-25, 2.65152e-25, // Heating for HI [erg/s]
    2.75388e-25, 2.86025e-25, 2.97072e-25, 3.08600e-25, 3.20577e-25, 3.33015e-25, 3.45991e-25,
    3.59472e-25, 3.73468e-25, 3.88063e-25, 4.03223e-25, 4.18957e-25, 4.35363e-25, 4.52402e-25,
    4.70085e-25, 4.88515e-25, 5.07649e-25, 5.27498e-25, 5.48177e-25, 5.69631e-25, 5.91873e-25,
    6.15044e-25, 6.39069e-25, 6.63960e-25, 6.89882e-25, 7.16738e-25, 7.44539e-25, 7.73474e-25,
    8.03432e-25, 8.34412e-25, 8.66638e-25, 8.99975e-25, 9.34417e-25, 9.70214e-25, 1.00721e-24,
    1.04539e-24, 1.08502e-24, 1.12592e-24, 1.16809e-24, 1.21178e-24, 1.25682e-24, 1.30318e-24,
    1.35113e-24, 1.40050e-24, 1.45124e-24, 1.50361e-24, 1.55741e-24, 1.61262e-24, 1.66946e-24,
    1.72774e-24, 1.78739e-24, 1.84866e-24, 1.91132e-24, 1.97526e-24, 2.04084e-24, 2.10762e-24,
    2.17554e-24, 2.24497e-24, 2.31542e-24, 2.38675e-24, 2.45942e-24, 2.53289e-24, 2.60693e-24,
    2.68202e-24, 2.75755e-24, 2.83325e-24, 2.90959e-24, 2.98591e-24, 3.06186e-24, 3.13794e-24,
    3.21339e-24, 3.28784e-24, 3.36172e-24, 3.43427e-24, 3.50505e-24, 3.57461e-24, 3.64244e-24,
    3.70831e-24, 3.77290e-24, 3.83579e-24, 3.89669e-24, 3.95639e-24, 4.01381e-24, 4.06878e-24,
    4.12207e-24, 4.17272e-24, 4.22035e-24, 4.26588e-24, 4.30856e-24, 4.34784e-24, 4.38467e-24,
    4.41838e-24, 4.44836e-24, 4.47566e-24, 4.49965e-24, 4.51970e-24, 4.53692e-24, 4.55078e-24,
    4.56060e-24, 4.56758e-24, 4.57128e-24, 4.57099e-24, 4.56795e-24, 4.56188e-24, 4.55199e-24,
    4.53954e-24, 4.52444e-24, 4.50585e-24, 4.48500e-24, 4.46201e-24, 4.43592e-24, 4.40814e-24,
    4.37853e-24, 4.34630e-24, 4.31269e-24, 4.27802e-24, 4.24119e-24, 4.20332e-24, 4.16532e-24,
    4.12568e-24, 4.08546e-24, 4.04572e-24, 4.00481e-24, 3.96369e-24, 3.92362e-24, 3.88277e-24,
    3.84196e-24, 3.80270e-24, 3.76296e-24, 3.72335e-24, 3.68571e-24, 3.64775e-24, 3.60985e-24,
    3.57423e-24, 3.53837e-24, 3.50229e-24, 3.46874e-24, 3.43493e-24, 3.40042e-24, 3.36861e-24,
    3.33644e-24, 3.30245e-24, 3.27146e-24, 3.23981e-24, 3.20528e-24, 3.17402e-24, 3.14184e-24,
    3.10623e-24, 3.07347e-24, 3.03929e-24, 3.00081e-24, 2.96431e-24, 2.92659e-24, 2.88519e-24,
    2.84629e-24, 2.80737e-24, 2.76578e-24, 2.72686e-24, 2.68798e-24, 2.64603e-24, 2.60615e-24,
    2.56602e-24, 2.52258e-24, 2.48081e-24, 2.43868e-24, 2.39313e-24, 2.34908e-24, 2.30469e-24,
    2.25635e-24, 2.20953e-24, 2.16256e-24, 2.11129e-24, 2.06204e-24, 2.01276e-24, 1.96042e-24,
    1.90978e-24, 1.85920e-24, 1.80656e-24, 1.75522e-24, 1.70408e-24, 1.65138e-24, 1.60001e-24,
    1.54910e-24, 1.49701e-24, 1.44637e-24, 1.39643e-24, 1.34567e-24, 1.29645e-24, 1.24815e-24,
    1.19934e-24, 1.15218e-24, 1.10610e-24, 1.05977e-24, 1.01517e-24, 9.68663e-25, 9.08836e-25,
    8.38466e-25, 7.59438e-25, 6.73026e-25, 5.83548e-25, 4.93603e-25, 4.04893e-25, 3.21073e-25,
    2.44228e-25, 1.75991e-25, 1.18077e-25, 7.15023e-26, 3.66893e-26, 1.36933e-26, 1.99298e-27};
  const array<double,n_logz> eHeIs = {2.21352e-25, 2.26035e-25, 2.30815e-25, 2.38216e-25, // Heating for HeI [erg/s]
    2.48203e-25, 2.58638e-25, 2.69539e-25, 2.81009e-25, 2.92999e-25, 3.05526e-25, 3.18705e-25,
    3.32485e-25, 3.46884e-25, 3.62030e-25, 3.77865e-25, 3.94413e-25, 4.11817e-25, 4.30010e-25,
    4.49020e-25, 4.69012e-25, 4.89903e-25, 5.11723e-25, 5.34672e-25, 5.58637e-25, 5.83653e-25,
    6.09961e-25, 6.37406e-25, 6.66022e-25, 6.96109e-25, 7.27457e-25, 7.60104e-25, 7.94415e-25,
    8.30139e-25, 8.67296e-25, 9.06333e-25, 9.46935e-25, 9.89109e-25, 1.03339e-24, 1.07938e-24,
    1.12709e-24, 1.17710e-24, 1.22898e-24, 1.28273e-24, 1.33896e-24, 1.39721e-24, 1.45746e-24,
    1.52037e-24, 1.58543e-24, 1.65258e-24, 1.72257e-24, 1.79481e-24, 1.86918e-24, 1.94657e-24,
    2.02624e-24, 2.10802e-24, 2.19296e-24, 2.28012e-24, 2.36924e-24, 2.46155e-24, 2.55596e-24,
    2.65210e-24, 2.75142e-24, 2.85271e-24, 2.95544e-24, 3.06128e-24, 3.16882e-24, 3.27734e-24,
    3.38876e-24, 3.50146e-24, 3.61454e-24, 3.73013e-24, 3.84645e-24, 3.96235e-24, 4.08026e-24,
    4.19816e-24, 4.31476e-24, 4.43261e-24, 4.54961e-24, 4.66422e-24, 4.77926e-24, 4.89280e-24,
    5.00329e-24, 5.11399e-24, 5.22289e-24, 5.32802e-24, 5.43307e-24, 5.53562e-24, 5.63337e-24,
    5.73032e-24, 5.82448e-24, 5.91289e-24, 6.00002e-24, 6.08407e-24, 6.16146e-24, 6.23711e-24,
    6.30940e-24, 6.37419e-24, 6.43689e-24, 6.49611e-24, 6.54716e-24, 6.59584e-24, 6.64119e-24,
    6.67784e-24, 6.71202e-24, 6.74328e-24, 6.76553e-24, 6.78531e-24, 6.80292e-24, 6.81138e-24,
    6.81745e-24, 6.82238e-24, 6.81826e-24, 6.81201e-24, 6.80577e-24, 6.79043e-24, 6.77240e-24,
    6.75586e-24, 6.73036e-24, 6.70185e-24, 6.67718e-24, 6.64424e-24, 6.60877e-24, 6.57919e-24,
    6.54173e-24, 6.50194e-24, 6.46977e-24, 6.43004e-24, 6.38786e-24, 6.35514e-24, 6.31507e-24,
    6.27207e-24, 6.24047e-24, 6.20163e-24, 6.15882e-24, 6.12947e-24, 6.09287e-24, 6.05060e-24,
    6.02402e-24, 5.99020e-24, 5.94822e-24, 5.92445e-24, 5.89353e-24, 5.85107e-24, 5.82969e-24,
    5.80152e-24, 5.75127e-24, 5.72877e-24, 5.69886e-24, 5.64006e-24, 5.61438e-24, 5.58225e-24,
    5.52171e-24, 5.49367e-24, 5.46039e-24, 5.39707e-24, 5.36356e-24, 5.32674e-24, 5.26032e-24,
    5.22131e-24, 5.18159e-24, 5.11338e-24, 5.07029e-24, 5.02741e-24, 4.95580e-24, 4.90710e-24,
    4.85904e-24, 4.78224e-24, 4.72701e-24, 4.67297e-24, 4.59040e-24, 4.52881e-24, 4.46921e-24,
    4.37461e-24, 4.30192e-24, 4.23305e-24, 4.12512e-24, 4.04292e-24, 3.96438e-24, 3.85884e-24,
    3.77282e-24, 3.68957e-24, 3.58664e-24, 3.49803e-24, 3.41158e-24, 3.30828e-24, 3.21758e-24,
    3.12959e-24, 3.02662e-24, 2.93544e-24, 2.84745e-24, 2.74615e-24, 2.65600e-24, 2.56947e-24,
    2.47116e-24, 2.38346e-24, 2.29973e-24, 2.20567e-24, 2.12169e-24, 2.03551e-24, 1.91232e-24,
    1.77249e-24, 1.61556e-24, 1.43274e-24, 1.24677e-24, 1.06112e-24, 8.70519e-25, 6.92448e-25,
    5.29443e-25, 3.82589e-25, 2.57593e-25, 1.56831e-25, 8.07264e-26, 3.02490e-26, 4.42766e-27};
  const array<double,n_logz> eHeIIs = {5.00840e-27, 5.12205e-27, 5.23959e-27, 5.42173e-27, // Heating for HeII [erg/s]
    5.66713e-27, 5.92543e-27, 6.19738e-27, 6.48397e-27, 6.78559e-27, 7.10308e-27, 7.43765e-27,
    7.78983e-27, 8.16047e-27, 8.55090e-27, 8.96189e-27, 9.39441e-27, 9.84984e-27, 1.03291e-26,
    1.08334e-26, 1.13642e-26, 1.19225e-26, 1.25097e-26, 1.31276e-26, 1.37771e-26, 1.44598e-26,
    1.51778e-26, 1.59320e-26, 1.67241e-26, 1.75564e-26, 1.84300e-26, 1.93466e-26, 2.03088e-26,
    2.13177e-26, 2.23750e-26, 2.34836e-26, 2.46445e-26, 2.58595e-26, 2.71319e-26, 2.84623e-26,
    2.98526e-26, 3.13056e-26, 3.28223e-26, 3.44044e-26, 3.60544e-26, 3.77733e-26, 3.95624e-26,
    4.14241e-26, 4.33589e-26, 4.53676e-26, 4.74524e-26, 4.96129e-26, 5.18497e-26, 5.41640e-26,
    5.65548e-26, 5.90216e-26, 6.15648e-26, 6.41825e-26, 6.68724e-26, 6.96343e-26, 7.24643e-26,
    7.53590e-26, 7.83164e-26, 8.13307e-26, 8.43965e-26, 8.75097e-26, 9.06625e-26, 9.38466e-26,
    9.70556e-26, 1.00278e-25, 1.03504e-25, 1.06722e-25, 1.09919e-25, 1.13079e-25, 1.16188e-25,
    1.19227e-25, 1.22175e-25, 1.25015e-25, 1.27721e-25, 1.30270e-25, 1.32656e-25, 1.34881e-25,
    1.36945e-25, 1.38860e-25, 1.40634e-25, 1.42269e-25, 1.43760e-25, 1.45096e-25, 1.46267e-25,
    1.47268e-25, 1.48090e-25, 1.48724e-25, 1.49166e-25, 1.49408e-25, 1.49444e-25, 1.49273e-25,
    1.48888e-25, 1.48285e-25, 1.47466e-25, 1.46428e-25, 1.45169e-25, 1.43694e-25, 1.42004e-25,
    1.40100e-25, 1.37989e-25, 1.35676e-25, 1.33166e-25, 1.30469e-25, 1.27593e-25, 1.24547e-25,
    1.21341e-25, 1.17989e-25, 1.14501e-25, 1.10893e-25, 1.07177e-25, 1.03366e-25, 9.94779e-26,
    9.55269e-26, 9.15271e-26, 8.74950e-26, 8.34469e-26, 7.93969e-26, 7.53611e-26, 7.13549e-26,
    6.73916e-26, 6.34853e-26, 5.96499e-26, 5.58963e-26, 5.22360e-26, 4.86801e-26, 4.52365e-26,
    4.19132e-26, 3.87178e-26, 3.56551e-26, 3.27294e-26, 2.99450e-26, 2.73037e-26, 2.48065e-26,
    2.24545e-26, 2.02466e-26, 1.81801e-26, 1.62531e-26, 1.44613e-26, 1.27991e-26, 1.12618e-26,
    9.84290e-27, 8.53438e-27, 7.32975e-27, 6.22278e-27, 5.20850e-27, 4.28865e-27, 3.46929e-27,
    2.75794e-27, 2.15897e-27, 1.67078e-27, 1.28373e-27, 9.84237e-28, 7.55904e-28, 5.81517e-28,
    4.47613e-28, 3.45843e-28, 2.69491e-28, 2.11318e-28, 1.66280e-28, 1.31098e-28, 1.03491e-28,
    8.17011e-29, 6.44917e-29, 5.08899e-29, 4.01304e-29, 3.16163e-29, 2.48990e-29, 1.95887e-29,
    1.53997e-29, 1.21002e-29, 9.49923e-30, 7.45182e-30, 5.84138e-30, 4.57425e-30, 3.57772e-30,
    2.79488e-30, 2.17935e-30, 1.69630e-30, 1.31831e-30, 1.02167e-30, 7.89231e-31, 6.07990e-31,
    4.66800e-31, 3.57068e-31, 2.72229e-31, 2.06735e-31, 1.56322e-31, 1.17727e-31, 8.82520e-32,
    6.58262e-32, 4.88671e-32, 3.60782e-32, 2.64796e-32, 1.93293e-32, 1.39791e-32, 9.89734e-33,
    6.85374e-33, 4.63391e-33, 3.05136e-33, 1.95486e-33, 1.21498e-33, 7.29463e-34, 4.21100e-34,
    2.32071e-34, 1.20595e-34, 5.81084e-35, 2.51545e-35, 9.18775e-36, 2.43198e-36, 2.49922e-37};

  if (logz <= 1e-10) {                       // Set rates to z = 0 value
    UVB_rate_HI = gHIs[0];
    UVB_rate_HeI = gHeIs[0];
    UVB_rate_HeII = gHeIIs[0];
    UVB_heat_HI = eHIs[0];
    UVB_heat_HeI = eHeIs[0];
    UVB_heat_HeII = eHeIIs[0];
  } else if (logz >= 1.064999999) {          // Set rates to zero for z > 11.6
    UVB_rate_HI = 0.;
    UVB_rate_HeI = 0.;
    UVB_rate_HeII = 0.;
    UVB_heat_HI = 0.;
    UVB_heat_HeI = 0.;
    UVB_heat_HeII = 0.;
  } else {
    const double d_logz = 200. * logz;       // Table coordinate
    const double f_logz = floor(d_logz);     // Floor coordinate
    const double frac_R = d_logz - f_logz;   // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_logz;                  // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    UVB_rate_HI = pow(10., log10(gHIs[i_L])*frac_L + log10(gHIs[i_R])*frac_R); // Rate for HI [1/s]
    UVB_rate_HeI = pow(10., log10(gHeIs[i_L])*frac_L + log10(gHeIs[i_R])*frac_R); // Rate for HeI [1/s]
    UVB_rate_HeII = pow(10., log10(gHeIIs[i_L])*frac_L + log10(gHeIIs[i_R])*frac_R); // Rate for HeII [1/s]
    UVB_heat_HI = pow(10., log10(eHIs[i_L])*frac_L + log10(eHIs[i_R])*frac_R); // Heating for HI [erg/s]
    UVB_heat_HeI = pow(10., log10(eHeIs[i_L])*frac_L + log10(eHeIs[i_R])*frac_R); // Heating for HeI [erg/s]
    UVB_heat_HeII = pow(10., log10(eHeIIs[i_L])*frac_L + log10(eHeIIs[i_R])*frac_R); // Heating for HeII [erg/s]
  }
}

/* Setup self-shielding parameters for a spatially uniform UV background based on the Rahmati model (2012). */
void Ionization::setup_self_shielding() {
  const array<double,7> SS_alpha1s = {-3.98, -2.94, -2.22, -1.99, -2.05, -2.63, -2.63};
  const array<double,7> SS_alpha2s = {-1.09, -0.90, -1.09, -0.88, -0.75, -0.57, -0.57};
  const array<double,7> SS_betas = {1.29, 1.21, 1.75, 1.72, 1.93, 1.77, 1.77};
  const array<double,7> SS_xis = {1., 1., 1., 1., 1., 1., 0.};
  const array<double,7> SS_n0s = {-2.94, -2.29, -2.06, -2.13, -2.23, -2.35, -2.35};
  const array<double,7> SS_fs = {0.01, 0.03, 0.03, 0.04, 0.02, 0.01, 0.01};

  if (z <= 1e-10) {                          // Set to z = 0 values
    SS_alpha1 = SS_alpha1s[0];
    SS_alpha2 = SS_alpha2s[0];
    SS_beta = SS_betas[0];
    SS_xi = SS_xis[0];
    SS_n0 = SS_n0s[0];
    SS_f = SS_fs[0];
  } else if (z >= 5.99999999) {              // Set to z = 6 values
    SS_alpha1 = SS_alpha1s[6];
    SS_alpha2 = SS_alpha2s[6];
    SS_beta = SS_betas[6];
    SS_xi = SS_xis[6];
    SS_n0 = SS_n0s[6];
    SS_f = SS_fs[6];
    self_shielding = false;                  // Turn off self-shielding
  } else {
    const double f_z = floor(z);             // Floor coordinate
    const double frac_R = z - f_z;           // Interpolation fraction (right)
    const double frac_L = 1. - frac_R;       // Interpolation fraction (left)
    const int i_L = f_z;                     // Table age index (left)
    const int i_R = i_L + 1;                 // Table age index (right)
    SS_alpha1 = SS_alpha1s[i_L]*frac_L + SS_alpha1s[i_R]*frac_R;
    SS_alpha2 = SS_alpha2s[i_L]*frac_L + SS_alpha2s[i_R]*frac_R;
    SS_beta = SS_betas[i_L]*frac_L + SS_betas[i_R]*frac_R;
    SS_xi = SS_xis[i_L]*frac_L + SS_xis[i_R]*frac_R;
    SS_n0 = SS_n0s[i_L]*frac_L + SS_n0s[i_R]*frac_R;
    SS_f = SS_fs[i_L]*frac_L + SS_fs[i_R]*frac_R;
  }
}
