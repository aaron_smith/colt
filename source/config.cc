/*!
 * \file    config.cc
 *
 * \brief   General configuration for the simulation.
 *
 * \details This file contains code to set up high level configuration options.
 *          Configuration options for individual modules are delegated to the
 *          overloaded member function ``module_config``.
 */

#include "proto.h"
#include <fstream> // File handling
#include <sys/stat.h> // Make directories
#include "config.h" // Configuration functions
#include "Simulation.h" // Base simulation class

void read_halo_catalog(const bool is_group); // Read the halo catalog data from the hdf5 file

//! Declare a directory structure.
#define declare_dir(name) \
  string name##_dir = "";                    /* Directory (optional) */ \
  string name##_subdir = "";                 /* Subdirectory (optional) */ \
  string name##_base = "";                   /* File base (optional) */ \
  string name##_ext = "";                    /* File extension */

//! Set a directory and subdirectory based on default values.
#define set_dir(name, default) { \
  name##_dir = default##_dir;                /* Default directory */ \
  name##_subdir = default##_subdir;          /* Default subdirectory */ \
}

//! Loads a directory and subdirectory.
#define load_dir(name) { \
  load(#name "_dir", name##_dir)             /* Load directory */ \
  load(#name "_subdir", name##_subdir)       /* Load subdirectory */ \
}

//! Ensure a directory exists, or raise an error.
#define make_dir_template(name) \
  if (root) { \
    int status = mkdir(name.c_str(), S_IRWXU | S_IRGRP | S_IXGRP); \
    if (status != 0 && errno != EEXIST) \
      error("Could not create the directory: " + name); \
  }

//! Ensure a directory and subdirectory exist, or raise an error.
#define make_dir(name) { \
  make_dir_template(name##_dir)              /* Make directory */ \
  if (!name##_subdir.empty()) {              /* Allow subdirectories */ \
    name##_dir += "/" + name##_subdir + snap_str; /* Update directory */ \
    make_dir_template(name##_dir)            /* Make subdirectory */ \
  } \
}

//! Loads a directory and subdirectory from default values, and ensures they exist.
#define setup_dir(name, default) { \
  set_dir(name, default)                     /* Set directory and subdirectory */ \
  load_dir(name)                             /* Load directory and subdirectory */ \
  make_dir(name)                             /* Make directories */ \
}

//! Sets a file name (without defaults).
#define update_file(name) { \
  load(#name "_base", name##_base)           /* Load base */ \
  load(#name "_ext", name##_ext)             /* Load extension */ \
  if (!name##_dir.empty())                   /* Allow directories */ \
    name##_file = name##_dir + "/";          /* Prepend directory info */ \
  name##_file += name##_base + snap_str + init_str; /* Set file */ \
}

//! Sets a file name based on default values.
#define set_file(name, default) { \
  name##_base = default##_base;              /* Default base */ \
  name##_ext = default##_ext;                /* Default extension */ \
  update_file(name)                          /* Update file */ \
  name##_file += "." + name##_ext;           /* Add extension */ \
}

//! Loads a configuration value from a file if it exists, or sets it based on a dependency condition.
void load_if_false_info(YAML::Node& file, const bool cond, const string name, bool& val) {
  if (file[name]) {
    val = file[name].as<bool>();             // Set value from file
    file.remove(name);                       // Remove from the file
  }
  if (cond)
    val = true;                              // Pass on dependency
  if (verbose && root)
    cout << name << ": " << val << endl;     // Verbose configuration
}

//! Raises an error if incompatible options are specified in the config file.
void incompatible_info(YAML::Node& file, const string config, const strings names) {
  int count = 0;
  string msg;
  for (auto& name : names) {
    if (file[name]) {
      count++;
      if (count > 1) msg += " and ";
      msg += name;
    }
  }
  if (count > 1)
    root_error("Cannot specify both " + msg + " in " + config);
}

//! General configuration for the simulation.
void Simulation::setup_config() {
  // Setup file and validation maps
  YAML::Node file = YAML::LoadFile(config_file); // User-specified

  // Setup verbose output
  load("verbose", verbose);                  // Verbose output
  if (verbose && root)
    cout << "\nLoading config options from " << config_file << endl;

  // Simulation parameters
  if (!snap_str.empty()) {                   // Ensure "_000" convention
    int snap_padding = 3;                    // Leading zeros for snap_str
    load("snap_padding", snap_padding);      // Allow overriding default
    snap_str = "_" + string(snap_padding - snap_str.length(), '0') + snap_str;
  }
  if constexpr (output_groups) {
    load("output_groups_obs", output_grp_obs); // Output group observed data
    load("output_groups_vir", output_grp_vir); // Output group virial data
    load("output_groups_gal", output_grp_gal); // Output group galaxy data
  }
  if constexpr (output_subhalos) {
    load("output_subhalos_obs", output_sub_obs); // Output subhalo observed data
    load("output_subhalos_vir", output_sub_vir); // Output subhalo virial data
    load("output_subhalos_gal", output_sub_gal); // Output subhalo galaxy data
  }
  bool use_group_catalog = output_groups;    // Use group catalog file
  bool use_subhalo_catalog = output_subhalos; // Use subhalo catalog file
  load("use_all_groups", use_all_groups);    // Use all groups
  load("use_all_subhalos", use_all_subhalos); // Use all subhalos
  if constexpr (output_group_flows) {
    if (root && !use_all_groups)
      cout << "Warning: Group flows require all groups. Using all groups." << endl;
    use_group_catalog = use_all_groups = true; // Require all groups for flows
  }
  if constexpr (output_subhalo_flows) {
    if (root && !use_all_subhalos)
      cout << "Warning: Subhalo flows require all subhalos. Using all subhalos." << endl;
    use_subhalo_catalog = use_all_subhalos = true; // Require all subhalos for flows
  }
  string init_str = "";                      // Initial conditions string
  if (!halo_str.empty() || use_all_groups || use_all_subhalos) {
    incompatible("select_group", "select_subhalo"); // Can only use one of these
    if (file["select_group"]) {
      load("select_group", select_subhalo);  // Specified a group
      select_subhalo = !select_subhalo;      // Invert the selection
    } else
      load("select_subhalo", select_subhalo); // Specified a subhalo
    if (select_subhalo || output_subhalos)
      load("use_subhalo_catalog", use_subhalo_catalog); // Use subhalo catalog file
    if (!select_subhalo || output_groups)
      load("use_group_catalog", use_group_catalog); // Use group catalog file
    if constexpr (output_groups || output_group_flows) {
      if (root && !use_group_catalog)
        cout << "Warning: Group statistics require the group catalog. Using group catalog." << endl;
      use_group_catalog = true;              // Require group catalog for group flows
    }
    if constexpr (output_subhalos || output_subhalo_flows) {
      if (root && !use_subhalo_catalog)
        cout << "Warning: Subhalo flows require the subhalo catalog. Using subhalo catalog." << endl;
      use_subhalo_catalog = true;            // Require subhalo catalog for subhalo flows
    }
    if (!use_group_catalog)
      use_all_groups = false;                // Disable all groups
    if (!use_subhalo_catalog)
      use_all_subhalos = false;              // Disable all subhalos
    use_all_halos = select_subhalo ? use_all_subhalos : use_all_groups; // Use all halos
    halo_str = (use_all_halos || module == "rays") ? "" : (select_subhalo ? "_s" : "_g") + halo_str;
    init_str = (use_group_catalog || use_subhalo_catalog) ? "" : halo_str; // Halo specific initial conditions
    incompatible("filter_group_catalog", "filter_subhalo_catalog"); // Can only use one of these
    load("filter_group_catalog", filter_group_catalog); // Filter group catalog
    load("filter_subhalo_catalog", filter_subhalo_catalog); // Filter subhalo catalog
    if constexpr (output_groups || output_group_flows)
      load("focus_groups_on_emission", focus_groups_on_emission); // Focus group grid on emission
    if constexpr (output_subhalos || output_subhalo_flows)
      load("focus_subhalos_on_emission", focus_subhalos_on_emission); // Focus subhalo grid on emission
  }
  if constexpr (VORONOI) {
    load("save_connections", save_connections); // Save voronoi connections
    load("save_circulators", save_circulators); // Save voronoi circulators
    load("avoid_edges", avoid_edges);        // Avoid ray-tracing through edge cells
    load("inner_edges", inner_edges);        // Flag to also include inner edge cells
    load("output_inner_edges", output_inner_edges); // Flag to output inner edge cells
  }
  declare_dir(cgal)                          // CGAL connectivity directory
  declare_dir(group)                         // Group properties directory
  declare_dir(subhalo)                       // Subhalo properties directory
  declare_dir(abundances)                    // Abundances initial conditions directory
  declare_dir(abundances_output)             // Abundances output directory
  const bool standard_io = (module != "rays") && (module != "reionization");
  const bool cgal = VORONOI && save_connections && standard_io;
  if (file["init_base"]) {
    if (file["init_dir"]) {
      load_dir(init);                        // Load init directory info
      if (cgal)
        setup_dir(cgal, init);               // Load cgal directory info (default to init)
      if (standard_io) {                     // Standard I/O (group, subhalo, and abundances files)
        if (use_group_catalog)
          setup_dir(group, init);            // Load group directory info (default to init)
        if (use_subhalo_catalog)
          setup_dir(subhalo, init);          // Load subhalo directory info (default to init)
        setup_dir(abundances, init);         // Load abundances directory info (default to init)
        setup_dir(abundances_output, abundances); // Load abundances output directory info (default to abundances)
      }
      if (!init_subdir.empty())
        init_dir += "/" + init_subdir + snap_str; // Update init directory
    }
    update_file(init);                       // Update initial conditions file
    if (cgal)
      set_file(cgal, init);                  // Set cgal file
    if (standard_io) {                       // Standard I/O
      if (use_group_catalog)
        set_file(group, init);               // Set group catalog file
      if (use_subhalo_catalog)
        set_file(subhalo, init);             // Set subhalo catalog file
      set_file(abundances, init);            // Set abundances file
      set_file(abundances_output, abundances); // Set abundances output file
    }
  } else {
    require("init_file", init_file);         // Initial conditions file
    if (cgal) {
      cgal_file = init_file;                 // Set default from init_file
      load("cgal_file", cgal_file);
    }
    // Abundances files
    if (standard_io) {
      if (use_group_catalog) {
        group_file = init_file;              // Set default from init_file
        load("group_file", group_file);
      }
      if (use_subhalo_catalog) {
        subhalo_file = init_file;            // Set default from init_file
        load("subhalo_file", subhalo_file);
      }
      abundances_file = init_file;           // Set default from init_file
      load("abundances_file", abundances_file);
      abundances_output_file = abundances_file; // Set default from abundances_file
      load("abundances_output_file", abundances_output_file);
    }
  }
  load_dir(output);                          // Load output directory info
  make_dir(output);                          // Ensure output directory exists
  load("output_base", output_base);          // Output file base name
  load("output_ext", output_ext);            // Output file extension

  MPI_Barrier(MPI_COMM_WORLD);               // Synchronize MPI processes

  // Cosmological parameters
  load("cosmological", cosmological);        // Flag for cosmological simulations
  if (cosmological) {
    load("Omega0", Omega0, "[0,1]");         // Matter density [rho_crit_0]
    load("OmegaB", OmegaB);                  // Baryon density [rho_crit_0]
    load("h100", h100, ">0");                // Hubble constant [100 km/s/Mpc]
    if (OmegaB < 0. || OmegaB > Omega0)
      root_error("OmegaB must be in the range [0,Omega0]"); // Validate OmegaB range
  }

  // Dual grid parameters
  if constexpr (output_ion_radial_age_freq || output_radial_flow) {
    incompatible("radial_center", "focus_radial_on_emission"); // Can only use
    incompatible("radial_motion", "shift_radial_on_emission"); // one of these
    load("radial_center", radial_center);    // Radial grid center position [cm]
    load("radial_motion", radial_motion);    // Radial grid exit velocity [cm/s]
    load("focus_radial_on_emission", focus_radial_on_emission); // Focus radial grid on emission (position)
    load("shift_radial_on_emission", shift_radial_on_emission); // Shift radial grid on emission (velocity)
  }

  // Specialized parameters
  if constexpr (cell_velocity_gradients && SPHERICAL)
    load("continuous_velocity", continuous_velocity); // Use a continuous velocity field

  // Group catalog parameters
  if (use_group_catalog)
    read_halo_catalog(true);                 // Read the group catalog data

  // Subhalo catalog parameters
  if (use_subhalo_catalog)
    read_halo_catalog(false);                // Read the subhalo catalog data

  // Module parameter configuration
  module_config(file);                       // Module configuration

  // Check for unexpected parameters
  for (const auto& kv : file)
    root_error("Unexpected or repeated field '" + kv.first.as<string>() + "' in " + config_file);
}
