# COLT
Monte Carlo radiative transfer and simulation analysis toolkit.

## Overview

The Cosmic Lyman-alpha Transfer (`colt`) code is a Monte Carlo radiative transfer
(MCRT) solver for post-processing hydrodynamical simulations on arbitrary grids.
These include a plane parallel slabs, spherical geometry, 3D Cartesian grids,
adaptive resolution octrees, unstructured Voronoi tessellations, and secondary
outputs. `colt` also includes several visualization and analysis tools that exploit
the underlying ray-tracing algorithms or otherwise benefit from an efficient
hybrid MPI + OpenMP parallelization strategy within a flexible C++ framework.

![COLT Logo](docs/logo.jpg)

## Support

If you have any issues obtaining, compiling, or running the code, please consult
the online [documentation](https://colt.readthedocs.io/en/latest/),
or let us know by email (Aaron Smith, [asmith@utdallas.edu](mailto:asmith@utdallas.edu)).
We are also open to collaboration, which is particularly encouraged when using
or implementing new features.

## Compilation

Obtain the `colt` source code by cloning the repository:
```
$ git clone git@bitbucket.org:aaron_smith/colt.git
```
This creates a new folder named `colt` containing the source code (`source`), documentation (`docs`), and compilation files (e.g. `Makefile`). After installing the required dependencies and setting up the build environment (see below) the executable may be compiled:
```
$ cd colt
$ make -j
```

Note that there are required dependencies:

- `MPI`: Message Passing Interface library for standard code parallelization
- `HDF5`: Hierarchical Data Format library for reading and writing files (with [C++ wrappers](https://portal.hdfgroup.org/pages/viewpage.action?pageId=50073884))
- `yaml-cpp`: Library for parsing YAML config files ([github repository](https://github.com/jbeder/yaml-cpp))

There is also an additional dependency for Voronoi geometry:

- `CGAL`: [Computational Geometry Algorithms Library](https://www.cgal.org) for constructing Delaunay triangulations

More detailed information is provided in the documentation, which is located online at [colt.readthedocs.io](https://colt.readthedocs.io/en/latest/) or can be generated with `make docs` (Doxygen build) followed by `make html` for the full user guide (sphynx build). The documentation may then be opened locally at ``colt/docs/_build/html/index.html``.

## Contributing

1. Fork it!
2. Create your feature branch: ``git checkout -b my-new-feature``
3. Commit your changes: ``git commit -am 'Add some feature'``
4. Push to the branch: ``git push origin my-new-feature``
5. Submit a pull request

The current list of significant contributors includes Dr. Enrico Garaldi, Dr. Ben Kimock, Will McClymont, Shrimoy Satpathy, and Jessica Yuan-Chen Yeh (alphabetical by last name).

## License

`colt` is licensed under a 3-clause BSD style license - see the ``LICENSE.md`` file.
