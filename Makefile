##############
## Makefile ##
##############
EXE    = colt
TYPE   = cc
H_TYPE = h
STD    = c++17

# Source files
VPATH   = source
SRC    := $(shell find $(VPATH) -name "*.$(TYPE)")
HEADER := $(shell find $(VPATH) -name "*.$(H_TYPE)")
DEFS_H := $(VPATH)/compile_time.h

# Build files
BUILD   = build
OBJ    := $(patsubst $(VPATH)/%,$(BUILD)/%,$(SRC:.$(TYPE)=.o))

MACHINE = homebrew
# MACHINE = gcc
# MACHINE = pleiades
# MACHINE = comet
# MACHINE = odyssey
# MACHINE = stampede
# MACHINE = supermuc

# Override compile time options [optional]
DEFS := $(wildcard defines.yaml)
$(shell >| ./defines.log)
ifneq ($(DEFS),)
  $(shell sed 's/[ 	]//g;s/#.*//;s/:/=/' $(DEFS) >> ./defines.log)
  include defines.log
endif

######################################################
# C++ Library Dependencies: HDF5, yaml-cpp           #
######################################################
ifeq ($(MACHINE),homebrew)        ## MacOS homebrew ##
######################################################
  # brew install openmpi libomp hdf5 yaml-cpp cgal gsl
  BREW  := $(shell brew --prefix)
  LOMP  := $(shell brew --prefix libomp)
  YAML  := $(shell brew --prefix yaml-cpp)
  HDF5  := $(shell brew --prefix hdf5)
  GSL   := $(shell brew --prefix gsl)
  GMP   := $(shell brew --prefix gmp)
  CC     = $(BREW)/bin/mpicxx
  CFLAGS = -Xpreprocessor -fopenmp -std=$(STD)
  #CFLAGS = -Xpreprocessor -fopenmp -std=$(STD) -O3 -flto
  #CFLAGS += -g -Wall -Werror -Wunused-parameter
  #CFLAGS += -Wextra -fsanitize=address,undefined -fno-omit-frame-pointer #-Wshadow
  IFLAGS = -I$(BREW)/include -I$(LOMP)/include -I$(YAML)/include -I$(HDF5)/include -I$(GSL)/include
  LFLAGS = -L$(LOMP)/lib -L$(YAML)/lib -L$(HDF5)/lib -L$(GSL)/lib -lomp -lyaml-cpp -lhdf5_cpp -lhdf5 -lgsl
######################################################
else ifeq ($(MACHINE),gcc)     ## Generic gcc setup ##
######################################################
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(HOME)/include
  LFLAGS = -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp -lgsl -lgslcblas
######################################################
else ifeq ($(MACHINE),pleiades)  ## Pleiades (NASA) ##
######################################################
  # module load comp-intel/2018.3.222 mpi-sgi/mpt.2.15r20
  # module load hdf5/1.8.18_mpt boost/1.62 pkgsrc/2018Q3
  CC     = icpc
  CFLAGS = -qopenmp -std=$(STD) -O3 -Wall -Werror -ipo
  IFLAGS = -I$(HOME)/local/include
  LFLAGS = -Wl,-rpath,$(HOME)/local/lib -L$(HOME)/local/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp -lmpi++ -lmpi
######################################################
else ifeq ($(MACHINE),comet)  ## Comet (XSEDE/SDSC) ##
######################################################
  # module load intel intelmpi hdf5-serial
  CC     = mpiicpc
  CFLAGS = -qopenmp -std=$(STD) -O3 -xHOST -Wall -Werror -ipo
  IFLAGS = -I$(HDF5HOME)/include -I$(HOME)/local/include
  LFLAGS = -Wl,-rpath,$(HDF5HOME)/lib -L$(HDF5HOME)/lib -Wl,-rpath,$(HOME)/local/lib -L$(HOME)/local/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
######################################################
else ifeq ($(MACHINE),comet-gcc)     ## Comet (gcc) ##
######################################################
  # module load gnu openmpi_ib hdf5-serial
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(HDF5HOME)/include -I$(HOME)/local-gcc/include
  LFLAGS = -Wl,-rpath,$(HDF5HOME)/lib -L$(HDF5HOME)/lib -Wl,-rpath,$(HOME)/local-gcc/lib64 -L$(HOME)/local-gcc/lib64 -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    # module load mpfr gmp boost
    IFLAGS += -I$(MPFRHOME)/include -I$(GMPHOME)/include -I$(BOOSTHOME)/include
  endif
######################################################
else ifeq ($(MACHINE),expanse)     ## Expanse (gcc) ##
######################################################
  # module load gcc openmpi hdf5/1.10.7-cxx
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(HDF5HOME)/include -I$(HOME)/include
  LFLAGS = -Wl,-rpath,$(HDF5HOME)/lib -L$(HDF5HOME)/lib -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    # module load mpfr gmp boost
    IFLAGS += -I$(MPFRHOME)/include -I$(GMPHOME)/include -I$(BOOSTHOME)/include
  endif
######################################################
else ifeq ($(MACHINE),cannon)     ## Cannon-Harvard ##
######################################################
  # module load gcc/10.2.0-fasrc01 hdf5/1.10.7-fasrc01 openmpi/4.1.3-fasrc01 gsl/2.8-fasrc01
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(ZLIB_INCLUDE) -I$(SZIP_INCLUDE) -I$(HDF5_INCLUDE) -I$(GSL_INCLUDE) -I$(HOME)/include
  LFLAGS = -L$(ZLIB_LIB) -L$(SZIP_LIB) -Wl,-rpath,$(HDF5_LIB) -L$(HDF5_LIB) -Wl,-rpath,$(GSL_LIB) -L$(GSL_LIB) -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp -lgsl -lgslcblas
  ifneq ($(HAVE_CGAL),)
    IFLAGS += -I$(MPFR_INCLUDE) -I$(GMP_INCLUDE) -I/n/helmod/apps/centos7/Core/boost/1.72.0-fasrc01/include -I/n/helmod/apps/centos7/Core/CGAL/5.0.3-fasrc01/include
  endif
######################################################
else ifeq ($(MACHINE),odyssey)   ## Odyssey-Harvard ##
######################################################
  # module load intel/19.0.5-fasrc01 impi/2019.5.281-fasrc01 hdf5/1.10.6-fasrc02
  CC     = mpiicpc
  CFLAGS = -qopenmp -std=$(STD) -O3 -Wall -Werror -ipo
  IFLAGS = -I$(HDF5_INCLUDE) -I$(HOME)/yaml-cpp/include
  LFLAGS = -Wl,-rpath,$(HDF5_LIB) -L$(HDF5_LIB) -Wl,-rpath,$(HOME)/yaml-cpp -L$(HOME)/yaml-cpp -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
######################################################
else ifeq ($(MACHINE),odyssey-gcc)   ## Odyssey-gcc ##
######################################################
  # module load gcc/9.3.0-fasrc01 openmpi/4.0.4-fasrc01 hdf5/1.10.6-fasrc01 CGAL/5.0.3-fasrc01
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(ZLIB_INCLUDE) -I$(SZIP_INCLUDE) -I$(HOME)/HDF5/include -I$(HOME)/yaml-cpp/include
  LFLAGS = -L$(ZLIB_LIB) -L$(SZIP_LIB) -Wl,-rpath,$(HOME)/HDF5/lib -L$(HOME)/HDF5/lib -Wl,-rpath,$(HOME)/yaml-cpp -L$(HOME)/yaml-cpp -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    IFLAGS += -I$(MPFR_INCLUDE) -I$(GMP_INCLUDE) -I/n/helmod/apps/centos7/Core/boost/1.72.0-fasrc01/include -I$(CGAL_INCLUDE)
  endif
######################################################
else ifeq ($(MACHINE),lonestar) ## Lonestar6 (TACC) ##
######################################################
  # module load hdf5/1.10.4 boost/1.84.0 yamlcpp/0.7.0
  CC     = mpicxx
  CFLAGS = -qopenmp -std=$(STD) -O3 -Wall -Werror -Wno-tautological-constant-compare -Wno-vla-cxx-extension -ipo
  #IFLAGS = -I$(TACC_HDF5_INC) -I$(HOME)/include
  IFLAGS = -I$(TACC_HDF5_INC) -I$(TACC_YAMLCPP_INC) -I$(HOME)/include
  #LFLAGS = -Wl,-rpath,$(TACC_HDF5_LIB) -L$(TACC_HDF5_LIB) -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  LFLAGS = -Wl,-rpath,$(TACC_HDF5_LIB) -L$(TACC_HDF5_LIB) -Wl,-rpath,$(TACC_YAMLCPP_LIB) -L$(TACC_YAMLCPP_LIB) -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    IFLAGS += -I$(HOME)/CGAL-5.0.2/include -I$(TACC_BOOST_INC)
  endif
######################################################
else ifeq ($(MACHINE),lonestar-gcc) ## Lonestar6GCC ##
######################################################
  # module load gcc/11.2.0 hdf5/1.10.4 boost/1.84.0
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  GSLINC = /opt/apps/gcc9_4/gsl/2.7/include
  GSLLIB = /opt/apps/gcc9_4/gsl/2.7/lib
  IFLAGS = -I$(TACC_HDF5_INC) -I$(GSLINC) -I$(HOME)/include
  LFLAGS = -Wl,-rpath,$(TACC_HDF5_LIB) -L$(TACC_HDF5_LIB) -Wl,-rpath,$(GSLLIB) -L$(GSLLIB) -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lgsl -lgslcblas -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    IFLAGS += -I$(HOME)/CGAL-5.0.2/include -I$(TACC_BOOST_INC)
  endif
######################################################
else ifeq ($(MACHINE),juno)           ## Juno (UTD) ##
######################################################
  # module load hdf5/1.14.5 gsl/2.7.1
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(HDF5_INC) -I$(GSL_INC) -I$(HOME)/include
  LFLAGS = -Wl,-rpath,$(HDF5_LIB) -L$(HDF5_LIB) -Wl,-rpath,$(GSL_LIB) -L$(GSL_LIB) -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lgsl -lgslcblas -lyaml-cpp
######################################################
else ifeq ($(MACHINE),stampede)  ## Stampede (TACC) ##
######################################################
  # module load intel/18.0.2 hdf5/1.10.4 boost/1.68
  CC     = mpicxx
  CFLAGS = -qopenmp -std=$(STD) -O3 -Wall -Werror -ipo
  IFLAGS = -I$(TACC_HDF5_INC) -I$(HOME)/include
  LFLAGS = -Wl,-rpath,$(TACC_HDF5_LIB) -L$(TACC_HDF5_LIB) -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    IFLAGS += -I$(HOME)/CGAL-5.0.2/include -I/opt/apps/intel18/boost/1.68/include
  endif
######################################################
else ifeq ($(MACHINE),supermuc)  ## Super-MUC (LHZ) ##
######################################################
  # module load hdf5/1.10.2-intel-cxx-frt-threadsafe yaml-cpp/0.6.2-intel
  CC     = mpicxx
  CFLAGS = -qopenmp -std=$(STD) -O3 -Wall -Werror -ipo
  IFLAGS = $(HDF5_INC) $(YAML_CPP_INC)
  LFLAGS = $(HDF5_CPP_SHLIB) $(HDF5_SHLIB) $(YAML_CPP_SHLIB)
######################################################
else ifeq ($(MACHINE),supermuc-gcc)    ## Super-MUC ##
######################################################
  # module unload spack devEnv/Intel/2019 itac/2019
  # module use -p /dss/dsshome1/lrz/sys/spack/.tmp.test.packages/modules/skylake/linux-sles12-skylake_avx512
  # module load tempdir spack/staging/20.2.0 openmpi/4.0.4-gcc8 hdf5/1.10.6-gcc8 yaml-cpp/0.6.3
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = $(HDF5_INC) $(YAML_CPP_INC)
  LFLAGS = $(HDF5_CPP_SHLIB) $(HDF5_SHLIB) $(YAML_CPP_SHLIB)
  ifneq ($(HAVE_CGAL),)
    # module load mpfr/4.0.2-gcc8 gmp/6.1.2 boost/1.70.0-intel19-impi cgal/5.0.3-gcc8
    IFLAGS += -I$(MPFR_BASE)/include $(GMP_INC) $(BOOST_INC) -I$(CGAL_BASE)/include
  endif
######################################################
else ifeq ($(MACHINE),freya-gcc)   ## Freya (MPCDF) ##
######################################################
  CC     = mpicxx
  CFLAGS = -fopenmp -std=$(STD) -O3 -Wall -Werror -flto
  IFLAGS = -I$(HOME)/include -I$(HOME)/lib/yaml-cpp/include -DH5_USE_16_API=1 -I$(HDF5_HOME)/include
  LFLAGS = -Wl,-rpath,$(HOME)/lib -L$(HOME)/lib -L$(HOME)/lib/yaml-cpp/build -L$(HDF5_HOME)/lib -lhdf5_cpp -lhdf5 -lz -lyaml-cpp
  ifneq ($(HAVE_CGAL),)
    IFLAGS += -I$(HOME)/lib/CGAL-5.2/include -I$(HOME)/lib/boost_1_75_0
    LFLAGS += -L$(HOME)/lib/CGAL-5.2/lib -L$(HOME)/lib/boost_1_75_0
  endif
######################################################
else                ## Do not allow unknown systems ##
  $(error MACHINE: $(MACHINE) is an unknown system) ##
endif               ## end MACHINE specific options ##
######################################################

# General library linking
ifneq ($(HAVE_CGAL),)
  ifdef GMP
    IFLAGS += -I$(GMP)/include
    LFLAGS += -L$(GMP)/lib
  endif
  LFLAGS += -lgmp
  ifneq ($(CGAL_LINKED_WITH_TBB),)
    LFLAGS += -ltbb -ltbbmalloc
  endif
endif

# Build rules
.PHONY: all

all: post_build

pre_build:
	$(shell mkdir -p $(BUILD) $(BUILD)/geometry $(BUILD)/mcrt $(BUILD)/ionization $(BUILD)/rays)
	$(shell mkdir -p $(BUILD)/projections $(BUILD)/escape $(BUILD)/escape_cont $(BUILD)/reionization)
	@python3 defines.py $(DEFS_H)~
	if ! cmp $(DEFS_H)~ $(DEFS_H) >/dev/null 2>&1; \
		then cp $(DEFS_H)~ $(DEFS_H); \
	fi
	@rm -f $(DEFS_H)~

main_build: pre_build
	@$(MAKE) main

post_build: main_build
	@rm -f defines.log

main: $(EXE)
	$(info ) $(info Finished $(MAKE) main target on $(shell date).)
	$(info ) $(info Compiled as "$(EXE)".  [$(MACHINE)])

code_dir := $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
test_headers: $(HEADER)
	for file in $^; do \
		printf "Testing $${file}\n"; \
		h_file=$${file/*\/}; b_file=$(BUILD)/$${file/source\/}; \
		cp $${file} $${b_file}; cd $${b_file/\/$${h_file}/}; \
		c_file=$${h_file/.$(H_TYPE)/.$(TYPE)}; e_file=$${h_file/.$(H_TYPE)/}; \
		printf "#include \"$${h_file}\"\nint main() {return 0;}" > $${c_file}; \
		$(CC) $(CFLAGS) $(IFLAGS) $(LFLAGS) $${c_file} -o $${e_file}; cd $(code_dir); \
	done

clean_defs:
	@rm -f defines.log
	@rm -f $(DEFS_H)

clean_exe:
	@rm -f $(EXE)

clean_build:
	@rm -rf $(BUILD)

docs: pre_build
	cd docs; doxygen Doxyfile

clean_docs:
	@rm -rf $(code_dir)/docs/html $(code_dir)/docs/xml

html:
	cd docs; make html

clean_html:
	@rm -rf $(code_dir)/docs/_build

clean: clean_build clean_exe clean_defs clean_docs clean_html
	$(info Finished $(MAKE) clean target.)

tests: pre_build test_headers clean
	$(info ) $(info Finished $(MAKE) tests target on $(shell date).)

.SILENT: pre_build test_headers

# Build dependencies
$(EXE): $(OBJ)
	$(CC) $(CFLAGS) $(OBJ) $(LFLAGS) -o $(EXE)

$(BUILD)/%.o: %.$(TYPE)
	$(CC) $(CFLAGS) $(IFLAGS) -c $< -o $@

$(OBJ): $(SRC) $(HEADER)

%.$(TYPE): %.$(H_TYPE)
