.. _ionization:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

MCRT Ionization Equilibrium
===========================

This page explains the options for ionization equilibrium Monte Carlo radiative transfer (MCRT) simulations. The Ionization module produces self-consistent ionization states that can be used as initial conditions for emission lines with the MCRT module.

.. _Ionization General Parameters:

General Parameters
------------------

The following parameters are important module parameters:

* ``n_photons`` -- Number of photon packets use in the simulation.
  :dv:`[Default value: 1]`

* ``n_photons_per_star`` -- Number of photon packets per star particle.
  :dv:`[Optional]`

* ``n_photons_max`` -- Maximum number of photon packets allowed when ``n_photons_per_star`` is used.
  :dv:`[Optional]`

* ``max_iter`` -- Maximum number of iterations for the radiative transfer and ionization solver.
  :dv:`[Default value: 10 | Valid range: 1 < max_iter < 100]`

* ``max_error`` -- Relative error tolerance for convergence in the ionization solver. Note: Convergence is based on the global rate of hydrogen recombinations: :math:`\int \alpha_\text{B}(T) n_e n_\text{HII} \text{d}V`.
  :dv:`[Default value: 0.01 | Valid range: 0 < max_error < 1]`

.. note:: The solver will stop if the relative error is less than ``max_error`` or if the maximum number of iterations is reached. To prevent updating the ionization states, set ``max_iter`` to 1. Experience shows that the

* ``charge_exchange`` -- Include charge exchange processes.
  :dv:`[Default value: false]`

* ``dialectric_recombination`` -- Include dielectric recombination processes.
  :dv:`[Default value: false]`

* ``output_abundances`` -- Flag to output ionization abundances for each cell, e.g., :math:`x_\text{HII} = n_\text{HII} / n_\text{H}`).
  :dv:`[Default value: false]`

* ``output_photons`` -- Flag to output escaped and absorbed photon packets.
  :dv:`[Default value: true]`

* ``photon_file`` -- Output a separate photon file with this base name, e.g. "photons".
  :dv:`[Optional]`

* ``output_n_scat`` -- Flag to output the number of scattering events for photon packets.
  :dv:`[Default value: false]`

* ``output_absorption_distance`` -- Flag to output the distance traveled until undergoing absorption.
  :dv:`[Default value: false]`

* ``output_source_position`` -- Flag to output the emission position for photon packets.
  :dv:`[Default value: false]`

* ``output_{atom}_absorption`` -- Flag to output the absorption due to a specific atom over all ionization species for photon packets.
  :dv:`[Default value: false]`

* ``output_{ion}_absorption`` -- Flag to output the absorption due to a specific ion for photon packets.
  :dv:`[Default value: false]`

.. note:: The ``{atom}`` placeholder should be replaced with the atom name, which can be any of the following: ``hydrogen``, ``helium``, ``carbon``, ``nitrogen``, ``oxygen``, ``neon``, ``magnesium``, ``silicon``, ``sulfer``, and ``iron``. The ``{ion}`` placeholder should be replaced with the ion name, which can be any of the following: ``HI``, ``HeI``, ``HeII``, ``CI``, ``CII``, ``CIII``, etc.

.. _Ionization Ion Parameters:

Ionization Parameters
---------------------

These parameters control which ions are included in the simulation:

* ``ion_max_eV`` -- Maximum ionization threshold energy to include.
  :dv:`[Default value: 100 | Units: eV]`

* ``helium_ions`` -- Flag to include all helium ions.
  :dv:`[Default value: true]`

* ``HeI_ion`` and ``HeII_ion`` -- Include HeI and HeII ionization.
  :dv:`[Default value: helium_ions]`

* ``metal_ions`` -- Flag to include metal ions, i.e. ``C``, ``N``, ``O``, ``Ne``, ``Mg``, ``Si``, ``S``, and ``Fe``.
  :dv:`[Default value: false]`

* ``{atom}_ions`` -- Flag to include all ions for a specific atom, e.g., ``silicon_ions`` for all silicon ions up to ``ion_max_eV``.
  :dv:`[Default value: metal_ions]`

* ``{ion}_ion`` -- Flag to include a specific ion, e.g., ``SiIV`` for all ions up to SiIV.
   :dv:`[Default value: {atom}_ions]`

* ``ion_bins`` -- Include frequency bin edges corresponding to ionization thresholds of all ions.
  :dv:`[Default value: false]`

* ``metal_bins`` -- Include frequency bin edges corresponding to ionization thresholds of all metal ions.
  :dv:`[Default value: false | Overrides ion_bins]`

* ``{atom}_bins`` -- Include frequency bin edges corresponding to ionization thresholds of all ions for a specific atom.
  :dv:`[Default value: false | Overrides ion_bins and metal_bins]`

* ``{ion}_bin`` -- Include a frequency bin edge corresponding to the ionization threshold of a specific ion.
  :dv:`[Default value: false | Overrides {atom}_bins]`

* ``min_bin_eV`` -- Lower limit of the lowest energy bin.
  :dv:`[Optional | Units: eV]`

* ``min_bin_angstrom`` -- Lower limit of the lowest energy bin in angstroms.
  :dv:`[Optional | Units: angstrom]`

* ``max_bin_eV`` -- Upper limit of the highest energy bin.
  :dv:`[Default value: Corresponds to 1 angstrom | Units: eV]`

* ``max_bin_angstrom`` -- Upper limit of the highest energy bin in angstroms.
  :dv:`[Optional | Units: angstrom]`

* ``max_bin_dex`` -- Maximum frequency bin width in dex (logarithmic scale).
  :dv:`[Optional | Valid range: > 0]`

.. _Ionization Dust Parameters:

Dust Parameters
---------------

These parameters define the dust properties in the simulation:

* ``dust_model`` -- Dust model for the radiative transfer setting the dust opacity, albedo, and anisotropy parameters.
  :dv:`[Options: SMC, MW, LAURSEN_SMC, filename]`

* ``kappa_HI``, ``kappa_HeI``, ``kappa_HeII`` -- Dust opacity in the HI, HeI, and HeII ionizing frequency bins, scaling the absorption coefficient by the dust mass: :math:`k_\text{dust} = \kappa_\text{dust} \rho_\text{dust}`.
  :dv:`[Default value: Based on dust_model | Units: cm^2/g of dust]`

* ``albedo_HI``, ``albedo_HeI``, ``albedo_HeII`` -- Dust scattering albedo in the HI, HeI, and HeII ionizing frequency bins, defined as :math:`A = k_\text{scat} / (k_\text{abs} + k_\text{scat})`.
  :dv:`[Default value: Based on dust_model and line | Valid range: 0 < albedo < 1]`

* ``cosine_HI``, ``cosine_HeI``, ``cosine_HeII`` -- Anisotropy parameter :math:`\langle \mu \rangle` for dust scattering in the HI, HeI, and HeII ionizing frequency bins.
  :dv:`[Default value: Based on dust_model and line | Valid range: -1 < cosine < 1]`

.. _Ionization Radiation Source Parameters:

Radiation Source Parameters
---------------------------

These parameters define the radiation sources in the simulation:

* ``source_file_Z_age`` -- File containing source spectra tabulated as functions of metallicity and age.
  :dv:``[Optional]``

* ``source_model`` -- Model for the stellar sources, most of which are predefined for HI, HeI, and HeII.
  :dv:`[Default value: "" | Options: GMC, MRT, BC03-IMF-SOLAR, BC03-IMF-HALF-SOLAR, BC03-IMF, BPASS-IMF-135-100, BPASS-CHAB-100, custom]`

* ``f_esc_stars`` -- Escape fraction from stellar birth clouds.
  :dv:`[Default value: 1 | Valid range: 0 < f_esc_stars < 1]`

* ``j_exp`` -- Luminosity boosting exponent for power-law emission biasing, i.e. probabilities are rescaled according to :math:`\propto \mathcal{L}_\text{cell}^{j_\text{exp}}` such that :math:`j_\text{exp} < 1` can better sample lower emissivity regions.
  :dv:`[Default value: 1 | Valid range: 0 < j_exp < 1]`

* ``V_exp`` -- Volume boosting exponent for power-law emission biasing, i.e. probabilities are rescaled according to :math:`\propto V_\text{cell}^{V_\text{exp}}` such that :math:`V_\text{exp} < 1` can better sample smaller volume regions.
  :dv:`[Default value: 1]`

* ``nu_exp`` -- Frequency boosting exponent for power-law emission biasing, i.e. probabilities are rescaled according to :math:`\propto \nu^{nu_\text{exp}}` such that :math:`nu_\text{exp} < 1` can better sample lower frequency bands.
  :dv:`[Default value: 1]`

* ``min_HI_bin_cdf`` -- Minimum cumulative distribution function (CDF) value for bins > 13.6 eV.
  :dv:`[Default value: 0]`

.. _Ionization Gas Emission Parameters:

Gas Emission Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters control gas emission processes:

* ``free_free`` -- Include free-free continuum emission.
  :dv:`[Default value: false]`

* ``free_bound`` -- Include free-bound continuum emission.
  :dv:`[Default value: false]`

* ``two_photon`` -- Include two-photon continuum emission.
  :dv:`[Default value: false]`

.. _Ionization AGN Source Parameters:

AGN Source Parameters
~~~~~~~~~~~~~~~~~~~~~

These parameters define the AGN sources in the simulation:

* ``AGN_model`` -- Model for the AGN source.
  :dv:`[Optional | Options: Stalevski12, Lusso15]`

* ``Lbol_AGN`` -- Bolometric luminosity of the AGN. Can also be specified in solar luminosities.
  :dv:`[Default value: 1e11 Lsun | Units: erg/s]`

.. _Ionization Point Source Parameters:

Point Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters define point sources in the simulation:

* ``point`` -- Point source position in `[x y z]` format.
  :dv:`[Optional | Units: cm]`

* ``x_point``, ``y_point``, ``z_point`` -- Point source :math:`(x, y, z)` coordinates.
  :dv:`[Optional | Units: cm]`

* ``T_point`` -- Temperature of the point source blackbody spectrum.
  :dv:`[Required if point_source is true | Units: K]`

* ``Lbol_point`` -- Bolometric luminosity of the point source.
  :dv:`[Optional | Units: erg/s]`

* ``L_point`` -- Luminosity of the point source in the specified energy range.
  :dv:`[Optional | Units: erg/s]`

* ``Ndot_point`` -- Photon rate of the point source.
  :dv:`[Optional | Units: photons/s]`

* ``kappa_point`` -- Dust opacity for the point source.
  :dv:`[Default value: Based on dust_model | Units: cm^2/g of dust]`

* ``albedo_point`` -- Dust scattering albedo for the point source.
  :dv:`[Default value: Based on dust_model | Valid range: 0 < albedo < 1]`

* ``cosine_point`` -- Dust scattering anisotropy parameter for the point source.
  :dv:`[Default value: Based on dust_model | Valid range: -1 < cosine < 1]`

.. _Ionization Plane Source Parameters:

Plane Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters define plane sources in the simulation:

* ``plane_direction`` -- Direction of the plane source.
  :dv:`[Options: '+x', '-x', '+y', '-y', '+z', '-z']`

* ``T_plane`` -- Temperature of the plane source blackbody spectrum.
  :dv:`[Required if plane_source is true and source_file_Z_age is not specified | Units: K]`

* ``Sbol_plane`` -- Plane bolometric surface flux.
  :dv:`[Optional | Units: erg/s/cm^2]`

* ``Lbol_plane`` -- Plane bolometric luminosity.
  :dv:`[Optional | Units: erg/s]`

* ``L_plane`` -- Luminosity of the plane source in the specified energy range.
  :dv:`[Optional | Units: erg/s]`

* ``Ndot_plane`` -- Photon rate of the plane source.
  :dv:`[Optional | Units: photons/s]`

* ``plane_beam`` -- Use an ellipsoidal beam instead of a rectangular plane.
  :dv:`[Default value: false]`

* ``plane_center_x_bbox``, ``plane_center_y_bbox``, ``plane_center_z_bbox`` -- Center of the plane source in bounding box units.
  :dv:`[Valid range: 0 < value < 1]`

* ``plane_radius_x_bbox``, ``plane_radius_y_bbox``, ``plane_radius_z_bbox`` -- Radii of the plane source in bounding box units.
  :dv:`[Valid range: > 0]`

* ``kappa_plane`` -- Dust opacity for the plane source.
  :dv:`[Default value: Based on dust_model | Units: cm^2/g of dust]`

* ``albedo_plane`` -- Dust scattering albedo for the plane source.
  :dv:`[Default value: Based on dust_model | Valid range: 0 < albedo < 1]`

* ``cosine_plane`` -- Dust scattering anisotropy parameter for the plane source.
  :dv:`[Default value: Based on dust_model | Valid range: -1 < cosine < 1]`

.. _Ionization UV Background Parameters:

UV Background Parameters
~~~~~~~~~~~~~~~~~~~~~~~~

These parameters control the inclusion of a UV background:

* ``UVB_model`` -- UV background model to use.
  :dv:`[Optional | Options: FG11, etc.]`

* ``self_shielding`` -- Include UV background self-shielding in dense regions.
  :dv:`[Default value: false]`

.. _Ionization Output Parameters:

Probe Output Parameters
-----------------------

These parameters control various probe output options:

* ``output_stars`` -- Flag to output the emission and escape data for each star.
  :dv:`[Default value: false]`

* ``output_cells`` -- Flag to output the emission and escape data for each cell.
  :dv:`[Default value: false]`

* ``output_cells_UVB_HI`` -- Output cell UV background ionization for HI.
  :dv:`[Default value: false | Units: photons/s]`

* ``output_cells_rec_{ion}`` -- Output cell recombination rates for a specific ion.
  :dv:`[Default value: false | Units: photons/s]`

* ``output_cells_col_{ion}`` -- Output cell collisional ionization rates for a specific ion.
  :dv:`[Default value: false | Units: photons/s]`

* ``output_cells_phot_{ion}`` -- Output cell photoionization rates for a specific ion.
  :dv:`[Default value: false | Units: photons/s]`

* ``output_cells_cx_{ion}`` -- Output cell charge exchange rates for a specific ion.
  :dv:`[Default value: false | Units: photons/s]`

* ``output_photoionization`` -- Flag to output species photoionization rate integrals for each cell.
  :dv:`[Default value: false | Units: photons/s/carrier]`

* ``output_photoheating`` -- Flag to output the total photoheating rate for each cell.
  :dv:`[Default value: false | Units: erg/s]`

* ``output_photon_density`` -- Flag to output the photon density for each cell and frequency bin.
  :dv:`[Default value: false | Units: photons/cm^3]`

.. _Ionization Observed Output Parameters:

Observed Output Parameters
--------------------------

This section describes the output options for MCRT simulations, including angle-averages, peel-off cameras, and directional binning parameters.

.. _Ionization Escape Parameters:

Angle-averaged Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~

The following parameters are related to angular-averaged escape options:

* ``output_radial_avg`` -- Output angle-averaged radial surface brightness.
  :dv:`[Default value: false]`

* ``output_bin_radial_avg`` -- Output angle-averaged bin radial surface brightness.
  :dv:`[Default value: false]`

.. _Ionization Camera Parameters:

Camera Parameters
~~~~~~~~~~~~~~~~~

The following parameters are related to cameras options:

* ``output_escape_fractions`` -- Output camera escape fractions.
  :dv:`[Default value: true | Dimensions: (n_cameras)]`

* ``output_bin_escape_fractions`` -- Output bin escape fractions.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_bins)]`

* ``output_images`` -- Output surface brightness images.
  :dv:`[Default value: true | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: photons/s/cm^2]`

* ``output_bin_images`` -- Output bin surface brightness images.
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels, n_bins) | Output units: photons/s/cm^2]`

* ``output_radial_images`` -- Output radial surface brightness images.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_radial_pixels) | Output units: photons/s/cm^2]`

* ``output_bin_radial_images`` -- Output bin radial surface brightness images.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_radial_pixels, n_bins) | Output units: photons/s/cm^2]`

* ``output_mcrt_emission`` -- Output intrinsic emission without transport based on MCRT sampling (also works for radial images).
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: photons/s/cm^2]`

* ``output_mcrt_attenuation`` -- Output attenuated emission without scattering based on MCRT sampling (also works for radial images).
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: photons/s/cm^2]`

.. _Ionization Healpix Binning Parameters:

Healpix Binning Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~

The following parameters are related to line-of-sight healpix binning options:

* ``n_exp_map`` -- Healpix exponent for the line-of-sight map, resulting in :math:`12 \times 4^{n_\text{exp,map}}` directions. Note: Similar setup for ``n_exp_flux``, ``n_exp_radial``, and ``n_exp_cube``.
  :dv:`[Default value: 2 | Valid range: n_exp_map >= 0]`

* ``n_side_map`` -- Healpix number of base pixel subdivisions for the line-of-sight map, resulting in :math:`12 \times n_\text{side,map}^2` directions. Note: Similar setup for ``n_side_flux``, ``n_side_radial``, and ``n_side_cube``.
  :dv:`[Overrides n_exp_map | Default value: 4 | Valid range: n_side_map >= 1]`

* ``map_radius`` -- Radius for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``cube_map_radius``.
  :dv:`[Units: cm]`

* ``map_radius_bbox`` -- Radius for the line-of-sight healpix map of radial surface brightness images in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``cube_map_radius_bbox``.
  :dv:`[Units: bounding box]`

* ``map_radius_Rvir`` -- Radius for the line-of-sight healpix map of radial surface brightness images in units of the selected halo virial radius :math:`R_{\rm vir}`. Note: Similar setup for ``cube_map_radius_Rvir``.
  :dv:`[Units: Virial radius]`

* ``n_map_pixels`` -- Number of radial pixels for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``n_cube_map_pixels``.
  :dv:`[Default value: 100]`

* ``map_pixel_width`` -- Intrinsic width of each radial pixel for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``cube_map_pixel_width``.
  :dv:`[Units: cm]`

* ``map_pixel_arcsec`` -- Observed angular width of each radial pixel for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``cube_map_pixel_arcsec``.
  :dv:`[Units: arcseconds]`

* ``output_map`` -- Output a line-of-sight healpix map of escape fractions.
  :dv:`[Default value: false | Dimensions: (12 * n_side_map^2)]`

* ``output_map2`` -- Output a statistical moment line-of-sight healpix map, e.g. to estimate convergence statistics as the relative error is approximately :math:`\sqrt{\text{map2}} / \text{map}`.
  :dv:`[Default value: false | Dimensions: (12 * n_side_map^2)]`

* ``output_radial_map`` -- Output a line-of-sight healpix map of radial surface brightness images.
  :dv:`[Default value: false | Dimensions: (12 * n_side_radial^2, n_map_pixels) | Output units: photons/s/cm^2]`

* ``output_bin_radial_map`` -- Output a line-of-sight healpix map of radial spectral data cubes.
  :dv:`[Default value: false | Dimensions: (12 * n_side_cube^2, n_cube_map_pixels, n_bins) | Output units: photons/s/cm^2]`

.. _Ionization Advanced Parameters:

Advanced Parameters
-------------------

These parameters are for advanced outputs and configurations:

* ``output_ion_stats`` -- Flag to output ionization statistics for all active ions.
  :dv:`[Default value: false]`

* ``output_ion_stats_{atom}`` -- Output ionization statistics for a specific atom.
  :dv:`[Default value: false | Overrides output_ion_stats]`

* ``output_ion_stats_{ion}`` -- Output ionization statistics for a specific ion.
  :dv:`[Default value: false | Overrides output_ion_stats_{atom}]`

* ``output_ion_stats_gas`` -- Output total ionization statistics for all active ions.
  :dv:`[Default value: false]`

* ``output_ion_stats_dust`` -- Output dust absorption statistics.
  :dv:`[Default value: false]`

* ``output_ion_stats_tot`` -- Output total ionization and absorption statistics (gas + dust).
  :dv:`[Default value: false]`

.. _Ionization Spatial Statistics Parameters:

Spatial Statistics Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following parameters are compile-time options and need to be set in the ``defines.yaml`` file:

* ``output_ion_cell_age_freq`` -- Output the rate of absorptions by cell, age, and frequency.
  :dv:`[Compile-time parameter | Default value: false | Units: photons/s]`

* ``output_ion_radial_age_freq`` -- Output the rate of absorptions by radial bin, age, and frequency.
  :dv:`[Compile-time parameter | Default value: false | Units: photons/s]`

* ``output_ion_distance_age_freq`` -- Output the rate of absorptions by distance bin, age, and frequency.
  :dv:`[Compile-time parameter | Default value: false | Units: photons/s]`

.. _Ionization Radial Flow Parameters:

Radial Flow Parameters
~~~~~~~~~~~~~~~~~~~~~~

The radial flow data is a 3D probe of the radiation field. The output data is in the ``radial_flow`` group with attributes ``n_radial_bins`` and ``n_freq_bins`` and fields ``radial_edges`` [cm] (radial bin edges), ``freq_edges`` (frequency bin edge indices), ``freq_names`` (frequency bin edge names), ``freq_edges_eV`` [eV] (frequency bin edges in eV), ``Ndot_src`` (emission as a function of source radii and frequency bin), ``Ndot_esc`` (escape as a function of source radii and frequency bin), ``Ndot_{ion}`` (ionization and absorption statistics as a function of absorption radii and frequency bin), ``Ndot_pass`` (first passage by source radii and shell boundaries and frequency bin), and ``Ndot_flow`` (net flux by source radii and shell boundaries and frequency bin).

The following parameters are compile-time options and need to be set in the ``defines.yaml`` file:

* ``output_radial_flow`` -- Output radial flow data.
  :dv:`[Compile-time parameter | Default value: false]`

* ``output_group_flows`` -- Output group flow data.
  :dv:`[Compile-time parameter | Default value: false]`

* ``output_subhalo_flows`` -- Output subhalo flow data.
  :dv:`[Compile-time parameter | Default value: false]`

The associated runtime parameters are:

* ``radial_linspace_pc`` -- Radial linear-spaced binning in parsecs for radial flow data as `[min max n_edges]`.
  :dv:`[Default value: 1 | Units: pc]`

* ``radial_logspace_pc`` -- Radial log-spaced binning in parsecs for radial flow data as `[log_min log_max n_edges]`.
  :dv:`[Default value: 1 | Units: pc]`

* ``radial_linspace_Rvir`` -- Radial linear-spaced binning in units of the selected halo virial radius :math:`R_{\rm vir}` for radial flow data as `[min max n_edges]`.
  :dv:`[Default value: 1 | Units: Virial radius]`

* ``radial_logspace_Rvir`` -- Radial log-spaced binning in units of the selected halo virial radius :math:`R_{\rm vir}` for radial flow data as `[log_min log_max n_edges]`.
  :dv:`[Default value: 1 | Units: Virial radius]`

* ``radial_edges_pc`` -- Radial edges in parsecs for radial flow data. Note: Similar config for ``radial_edges_Rvir`` and ``radial_edges_cm``.
  :dv:`[Default value: 1 | Units: pc]`

.. _Ionization Refinement Parameters:

Refinement Parameters
~~~~~~~~~~~~~~~~~~~~~

The following parameters are for grid refinement:

* ``output_refinement`` -- Flag to output a refined version of the grid based on requirements to resolve HII regions. Note: This option is only available when using the voronoi geometry (and requires ``HAVE_CGAL`` for mesh connectivity).
  :dv:`[Default value: false]`

* ``refinement_rtol`` -- Relative photon rate tolerance for grid refinement.
  :dv:`[Default value: 0.1 | Valid range: > 0]`

* ``refinement_x_HI`` -- Neutral hydrogen fraction threshold for grid refinement.
  :dv:`[Default value: 0.1 | Valid range: 0 < refinement_x_HI < 1]`

* ``refinement_Rmin`` -- Effective resolution threshold for grid refinement.
  :dv:`[Default value: 1 | Valid range: > 0]`

* ``refinement_mdeg`` -- Merging opening angle in degrees for grid refinement.
  :dv:`[Default value: 5 | Valid range: 0 < refinement_mdeg < 30]`

* ``refinement_pert`` -- Perturbation factor for newly added grid refinement points.
  :dv:`[Default value: 0.01 | Valid range: 0 < refinement_pert < 0.01]`
