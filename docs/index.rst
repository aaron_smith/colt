.. COLT documentation master file

Overview
========

.. Monte Carlo radiative transfer and simulation analysis toolkit.

The Cosmic Lyman-alpha Transfer (COLT) code is a Monte Carlo radiative transfer
(MCRT) solver for post-processing hydrodynamical simulations on arbitrary grids.
These include a plane parallel slabs, spherical geometry, 3D Cartesian grids,
adaptive resolution octrees, unstructured Voronoi tessellations, and secondary
outputs. COLT also includes several visualization and analysis tools that exploit
the underlying ray-tracing algorithms or otherwise benefit from an efficient
hybrid MPI + OpenMP parallelization strategy within a flexible C++ framework.

.. image:: logo.jpg
  :width: 700
  :alt: COLT Logo

Support
=======

If you have any issues obtaining, compiling, or running the code, please consult
the below documentation, or let us know by email (Aaron Smith, asmith@utdallas.edu).
We are also open to collaboration, which is particularly encouraged when using
or implementing new features.

.. toctree::
   :maxdepth: 2
   :caption: General Usage

   getting_started

   simulations

   files

   fields

.. toctree::
   :maxdepth: 2
   :caption: Modules

   projections

   mcrt

   ionization

   escape

   rays

   reionization

.. toctree::
   :maxdepth: 2
   :caption: Resources

   publications

   gallery

   examples

.. toctree::
   :maxdepth: 2
   :caption: API

   api

.. toctree::
   :hidden:

   genindex

.. Indices and tables
.. ==================

.. * :ref:`genindex`
