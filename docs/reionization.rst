.. _reionization:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Reionization Box Analysis
=========================

This page explains the options for reionization box simulation analaysis.

.. _EoR General Parameters:

General Parameters
------------------

The following parameters are important reionization analysis parameters:

Coming soon.

.. * ``n_photons`` -- Actual number of photon packets used.
..   :dv:`[Default value: 1]`
