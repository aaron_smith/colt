.. _simulations:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Simulation Parameters
=====================

This page explains the options for configuring COLT simulations.

.. _Configuration:

Configuration Files
-------------------

COLT uses the **yaml-cpp** library to allow for flexible and convenient configuration of run-time parameters. We introduce the ``config.yaml`` file with the following example:

.. code-block:: yaml

    # COLT config file
    --- !mcrt                     # Monte Carlo radiative transfer module

    init_dir: ics                 # Initial conditions directory (default: "ics")
    init_base: colt               # Initial conditions base name (optional)
    output_dir: output            # Output directory name (default: "output")
    output_base: Lya              # Output file base name (default: "colt")

    cameras:                      # Add camera directions manually
      - [1,0,0]
      - [0,1,0]
      - [0,0,1]

    ... Additional run-time parameters

The format is essentially a human readable python dictionary of ``key: value`` pairs that allows #-style comments and complex specifications such as the above list of camera directions.

The YAML header ``--- !mcrt`` specifies which module to run, a convention that sets the module apart from more typical run-time parameters. The value is interpreted in the main file prior to instantiating the simulation to allow high level class polymorphism. By default the MCRT module is assumed in cases where the header line is not included.

.. note:: COLT configuration is design to be minimally verbose, transparent, and catch common unintended errors. In practice this means that only expected parameters are allowed and an error is raised for unexpected or repeated parameters. If a parameter is not specified then the default value will be assigned. Note that the default may depend on other parameters but this is determined by the code logic, i.e. the order of parameters does not matter. Only a few parameters are actually required, such as the initial conditions file naming scheme.

.. _General Parameters:

General Parameters
------------------

The following parameters are shared by most modules:

* ``verbose`` -- Verbose output for tests and debugging.
  :dv:`[Default value: false]`

* ``init_file`` -- Initial conditions file name.
  :dv:`[Required if` ``init_base`` :dv:`is not specified]`

* ``init_dir`` -- Initial conditions directory.
  :dv:`[Optional]`

* ``init_subdir`` -- Initial conditions subdirectory, e.g. allows ``ics/snap_000`` organization.
  :dv:`[Optional]`

* ``init_base`` -- Initial conditions file base.
  :dv:`[Optional]`

* ``init_ext`` -- Initial conditions file extension.
  :dv:`[Default value: hdf5]`

* ``output_dir`` -- Output directory.
  :dv:`[Default value: output]`

* ``output_subdir`` -- Output subdirectory, e.g. allows ``output/snap_000`` organization.
  :dv:`[Optional]`

* ``output_base`` -- Output file base.
  :dv:`[Default value: colt]`

* ``output_ext`` -- Output file extension.
  :dv:`[Default value: hdf5]`

* ``snap_padding`` -- Leading zeros for snapshot number formatting.
  :dv:`[Default value: 3]`

* ``select_subhalo`` (or ``select_group``) -- Optional toggles for the command line subhalo/group specifier. If true then this is interpreted as a subhalo (group) index, otherwise it is interpreted as a group (subhalo) index.

.. note:: In practice, an initial conditions file may be specified by the full name via ``init_file`` or the file pattern ``<init_dir>/<init_base>_<snapshot>.<init_ext>``, which is then mirrored in the convention for output files, e.g. ``output/colt_000.hdf5``.

.. _Cosmological Parameters:

Cosmological Parameters
-----------------------

The following parameters are related to the cosmology:

* ``cosmological`` -- Flag for cosmological simulations. Note: This is for cosmological vs. intrinsic observers, i.e. it changes the output units but not the physics.
  :dv:`[Default value: false]`

* ``Omega0`` -- Matter density in units of the current critical density :math:`\rho_\text{crit,0}`.
  :dv:`[Default value: 0.3111]`

* ``OmegaB`` -- Baryon density in units of the current critical density :math:`\rho_\text{crit,0}`.
  :dv:`[Default value: 0.04897]`

* ``h100`` -- Hubble constant in units of 100 km/s/Mpc.
  :dv:`[Default value: 0.6766]`

.. note:: Cosmological parameters are needed for calculating observed distances and other module specific quantities. The values are from the Planck mission (`2018 results <https://ui.adsabs.harvard.edu/abs/2020A%26A...641A...6P/abstract>`_). For consistency, these parameters may also be read from the initial conditions file.

.. _Gas Parameters:

Gas Parameters
--------------

The following parameters are related to the gas environment:

* ``constant_temperature`` -- Enforce a constant temperature, :math:`T`. Note: This overrides automatically reading from the input file.
  :dv:`[Optional | Units: K]`

* ``T_floor`` -- Apply a temperature floor to all gas cells, :math:`T_\text{floor}`.
  :dv:`[Optional | Units: K]`

* ``T_rtol`` -- Relative tolerance for temperature changes, e.g. in photoionization equilibrium calculations.
  :dv:`[Default value: 0.1]`

* ``electron_fraction`` -- Enforce a constant electron fraction, :math:`x_e = n_e / n_\text{H}`.
  :dv:`[Optional]`

* ``HI_fraction`` or ``neutral_fraction`` -- Enforce a constant neutral fraction, :math:`x_\text{HI} = n_\text{HI} / n_\text{H}`. Note: In general, specifying an ionization state overrides automatically reading from the input file. If neither is present, we assume neutral gas compositions.
  :dv:`[Optional]`

* ``HII_fraction`` or ``ionized_fraction`` -- Enforce a constant ionized fraction, :math:`x_\text{HII} = n_\text{HII} / n_\text{H}`.
  :dv:`[Optional]`

* ``H2_fraction`` or ``molecular_fraction`` -- Enforce a constant molecular hydrogen fraction, :math:`x_\text{H2} = n_\text{H2} / n_\text{H}`.
  :dv:`[Optional]`

* ``HeI_fraction`` -- Enforce a constant HeI fraction, :math:`x_\text{HeI} = n_\text{HeI} / n_\text{He}`.
  :dv:`[Optional]`

* ``HeII_fraction`` -- Enforce a constant HeII fraction, :math:`x_\text{HeII} = n_\text{HeII} / n_\text{He}`.
  :dv:`[Optional]`

* ``HeIII_fraction`` -- Enforce a constant HeIII fraction, :math:`x_\text{HeIII} = n_\text{HeIII} / n_\text{He}`.
  :dv:`[Optional]`

* ``CI_fraction`` -- Enforce a constant CI fraction, :math:`x_\text{CI} = n_\text{CI} / n_\text{C}`.
  :dv:`[Optional]` Note: This is also available for ``CII``, ``CIII``, ``CIV``, ``CV``, ``CVI``, and ``CVII``.

* ``NI_fraction`` -- Enforce a constant NI fraction, :math:`x_\text{NI} = n_\text{NI} / n_\text{N}`.
  :dv:`[Optional]` Note: This is also available for ``NII``, ``NIII``, ``NIV``, ``NV``, ``NVI``, ``NVII``, and ``NVIII``.

* ``OI_fraction`` -- Enforce a constant OI fraction, :math:`x_\text{OI} = n_\text{OI} / n_\text{O}`.
  :dv:`[Optional]` Note: This is also available for ``OII``, ``OIII``, ``OIV``, ``OV``, ``OVI``, ``OVII``, ``OVIII``, and ``OIX``.

* ``NeI_fraction`` -- Enforce a constant NeI fraction, :math:`x_\text{NeI} = n_\text{NeI} / n_\text{Ne}`.
  :dv:`[Optional]` Note: This is also available for ``NeII``, ``NeIII``, ``NeIV``, ``NeV``, ``NeVI``, ``NeVII``, ``NeVIII``, and ``NeIX``.

* ``MgI_fraction`` -- Enforce a constant MgI fraction, :math:`x_\text{MgI} = n_\text{MgI} / n_\text{Mg}`.
  :dv:`[Optional]` Note: This is also available for ``MgII``, ``MgIII``, ``MgIV``, ``MgV``, ``MgVI``, ``MgVII``, ``MgVIII``, ``MgIX``, ``MgX``, and ``MgXI``.

* ``SiI_fraction`` -- Enforce a constant SiI fraction, :math:`x_\text{SiI} = n_\text{SiI} / n_\text{Si}`.
  :dv:`[Optional]` Note: This is also available for ``SiII``, ``SiIII``, ``SiIV``, ``SiV``, ``SiVI``, ``SiVII``, ``SiVIII``, ``SiIX``, ``SiX``, ``SiXI``, ``SiXII``, and ``SiXIII``.

* ``SI_fraction`` -- Enforce a constant SI fraction, :math:`x_\text{SI} = n_\text{SI} / n_\text{S}`.
  :dv:`[Optional]` Note: This is also available for ``SII``, ``SIII``, ``SIV``, ``SV``, ``SVI``, ``SVII``, ``SVIII``, ``SIX``, ``SX``, ``SXI``, ``SXII``, ``SXIII``, ``SXIV``, and ``SXV``.

* ``FeI_fraction`` -- Enforce a constant FeI fraction, :math:`x_\text{FeI} = n_\text{FeI} / n_\text{Fe}`.
  :dv:`[Optional]` Note: This is also available for ``FeII``, ``FeIII``, ``FeIV``, ``FeV``, ``FeVI``, ``FeVII``, ``FeVIII``, ``FeIX``, ``FeX``, ``FeXI``, ``FeXII``, ``FeXIII``, ``FeXIV``, ``FeXV``, ``FeXVI``, and ``FeXVII``.

* ``hydrogen_fraction`` -- Enforce a constant hydrogen mass fraction. Overrides automatically reading :math:`X` from the input file. If neither is present, we assume the primordial value of :math:`X = 0.76`.
  :dv:`[Optional | Units: Mass fraction]`

* ``helium_fraction`` -- Enforce a constant helium mass fraction. Overrides automatically reading :math:`Y` from the input file. If neither is present, we assume the primordial value of :math:`Y = 1 - X`.
  :dv:`[Optional | Units: Mass fraction]`

* ``metallicity`` -- Enforce a constant metallicity. Overrides automatically reading :math:`Z` from the input file. If neither is present, we assume solar metallicity.
  :dv:`[Optional | Units: Mass fraction]`

* ``carbon_metallicity`` -- Enforce a constant carbon metallicity, :math:`Z_\text{C}`. Note: In general, specifying an element specific metallicity overrides automatically reading from the input file. If neither is present, we assume solar scaled abundances, e.g. :math:`Z_\text{C} = Z_{\odot,\text{C}} (Z / Z_{\odot})`.
  :dv:`[Optional | Units: Mass fraction]`

* ``nitrogen_metallicity`` -- Enforce a constant nitrogen metallicity, :math:`Z_\text{N}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``oxygen_metallicity`` -- Enforce a constant oxygen metallicity, :math:`Z_\text{O}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``neon_metallicity`` -- Enforce a constant neon metallicity, :math:`Z_\text{Ne}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``magnesium_metallicity`` -- Enforce a constant magnesium metallicity, :math:`Z_\text{Mg}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``silicon_metallicity`` -- Enforce a constant silicon metallicity, :math:`Z_\text{Si}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``sulfer_metallicity`` -- Enforce a constant sulfer metallicity, :math:`Z_\text{S}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``iron_metallicity`` -- Enforce a constant iron metallicity, :math:`Z_\text{Fe}`.
  :dv:`[Optional | Units: Mass fraction]`

* ``mass_weighted_metallicities`` -- Adopt mass-weighted average metallicity values.
  :dv:`[Default value: false]`

* ``dust_to_metal`` -- Enforce a constant dust-to-metal ratio, :math:`\text{DTM} \equiv \rho_\text{dust} / \rho_Z`.
  :dv:`[Optional | Units: Mass fraction]`

* ``dust_to_gas`` -- Enforce a constant dust-to-gas ratio, :math:`\mathcal{D} \equiv \rho_\text{dust} / \rho`.
  :dv:`[Optional | Units: Mass fraction]`

* ``dust_boost`` -- Rescale the dust values by this boost factor.
  :dv:`[Optional]`

* ``v_turb`` -- Effective microturbulent velocity.
  :dv:`[Default value: 0 | Units: cm/s]`

* ``v_turb_kms`` -- Effective microturbulent velocity.
  :dv:`[Default value: 0 | Units: km/s]`

* ``T_turb`` -- Effective microturbulent temperature.
  :dv:`[Default value: 0 | Units: K]`

* ``scaled_microturb`` -- Scaled microturbulence model where high density gas is more turbulent. Specifically, :math:`T_\text{turb} = {\rm K\,km}^2 \rho / (\gamma k_\text{B})` with an adiabatic index of :math:`\gamma = 5/3`.
  :dv:`[Default value: false]`

.. note:: The microturbulence parameter follows the standard treatment of adding Gaussian line broadening in quadrature: :math:`b = \sqrt{v_\text{th}^2 + v_\text{turb}^2}`. This acts as an effective temperature floor for line radiative transfer with :math:`v_\text{turb}^2 = 2 k_\text{B} T_\text{turb} / m_\text{carrier}`. You can specify only one of ``T_turb``, ``v_turb``, or ``v_turb_kms``, as they are related by the above equation.

.. _Camera Parameters:

Camera Parameters
-----------------

The following parameters are related to controlling cameras:

* ``camera`` -- Add a single camera direction manually in `[1 0 0]` format.
  :dv:`[Optional]`

* ``cameras`` -- Add a list of camera directions manually in `[1 0 0]` format.
  :dv:`[Optional]`

* ``n_exp`` -- Healpix exponent for camera directions, resulting in :math:`12 \times 4^{n_\text{exp}}` directions in RING ordering.
  :dv:`[Optional | Valid range: n_exp >= 0]`

* ``n_side`` -- Healpix number of base pixel subdivisions for camera directions, resulting in :math:`12 \times n_\text{side}^2` directions in RING ordering.
  :dv:`[Overrides n_exp | Optional | Valid range: n_side >= 1]`

* ``n_rot`` -- Number of rotation camera directions, which are evenly spaced in angle around a common axis, e.g. to make movies.
  :dv:`[Optional | Valid range: n_rot > 0]`

* ``phi_start`` -- Starting azimuthal angle for rotations.
  :dv:`[Requires n_rot | Units: degrees]`

* ``phi_end`` -- Ending azimuthal angle for rotations.
  :dv:`[Requires n_rot | Units: degrees]`

* ``rotate_with_snaps`` -- Flag to offset rotations by the snapshot number.
  :dv:`[Requires n_rot | Default value: false]`

* ``rotation_axis`` -- Axis for rotation camera directions in `[0 0 1]` format.
  :dv:`[Requires n_rot | Default value: (0,0,1)]`

* ``camera_center`` -- Camera target position in `[0 0 0]` format.
  :dv:`[Default value: (0,0,0) | Units: cm]`

* ``camera_motion`` -- Camera target velocity in `[0 0 0]` format.
  :dv:`[Default value: (0,0,0) | Units: cm/s]`

* ``focus_cameras_on_emission`` -- Focus the camera target position on the center of luminosity.
  :dv:`[Default value: false]`

* ``shift_cameras_on_emission`` -- Shift the camera target velocity on the center of luminosity.
  :dv:`[Default value: false]`

* ``align_cameras_on_emission`` -- Rotate the camera target direction to align ``camera_north`` with the angular momentum vector based on the center of luminosity.
  :dv:`[Default value: false]`

* ``camera_north`` -- Camera north orientation in `[0 0 1]` format.
  :dv:`[Default value: (0,0,1)]`

* ``image_width`` -- Image width defining a square aperture. Note: Similar setup for ``slit_width`` and ``cube_width``.
  :dv:`[Units: cm]`

* ``image_widths`` -- Image :math:`(x,y)` widths defining a rectangular aperture. Note: Similar setup for ``cube_widths``.
  :dv:`[Units: cm]`

* ``image_radius`` -- Image radius or half of the image width. Note: Similar setup for ``slit_radius`` and ``cube_radius``.
  :dv:`[Units: cm]`

* ``image_radii`` -- Image :math:`(x,y)` radii or half of the image widths. Note: Similar setup for ``cube_radii``.
  :dv:`[Units: cm]`

* ``image_radius_bbox`` -- Image radius or half of the image width in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``slit_radius_bbox`` and ``cube_radius_bbox``.
  :dv:`[Units: bounding box]`

* ``image_radii_bbox`` -- Image :math:`(x,y)` radii or half of the image widths in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``cube_radii_bbox``.
  :dv:`[Units: bounding box]`

* ``image_radius_Rvir`` -- Image radius or half of the image width in units of the selected halo virial radius :math:`R_{\rm vir}`. Note: Similar setup for ``slit_radius_Rvir`` and ``cube_radius_Rvir``.
  :dv:`[Units: Virial radius]`

* ``image_radii_Rvir`` -- Image :math:`(x,y)` radii or half of the image widths in units of the selected halo virial radius :math:`R_{\rm vir}`. Note: Similar setup for ``cube_radii_Rvir``.
  :dv:`[Units: Virial radius]`

* ``n_pixels`` -- Number of :math:`(x,y)` image pixels. Note: Similar setup for ``n_cube_pixels``.
  :dv:`[Default value: 100]`

* ``nx_pixels`` -- Number of :math:`x` image pixels, overriding ``n_pixels`` if both are specified. Note: Similar setup for ``nx_cube_pixels``.
  :dv:`[Default value: 100]`

* ``ny_pixels`` -- Number of :math:`y` image pixels, overriding ``n_pixels`` if both are specified. Note: Similar setup for ``ny_cube_pixels``.
  :dv:`[Default value: 100]`

* ``n_slit_pixels`` -- Number of slit pixels in the spatially-resolved direction.
  :dv:`[Default value: 100]`

* ``pixel_width`` -- Intrinsic width of each image pixel. Note: Similar setup for ``slit_pixel_width`` and ``cube_pixel_width``.
  :dv:`[Units: cm]`

* ``pixel_widths`` -- Intrinsic :math:`(x,y)` widths of each image pixel. Note: Similar setup for ``cube_pixel_widths``.
  :dv:`[Units: cm]`

* ``pixel_arcsec`` -- Observed angular width of each image pixel. Note: Similar setup for ``slit_pixel_arcsec`` and ``cube_pixel_arcsec``.
  :dv:`[Units: arcseconds]`

* ``pixel_arcsecs`` -- Observed angular :math:`(x,y)` widths of each image pixel. Note: Similar setup for ``cube_pixel_arcsecs``.
  :dv:`[Units: arcseconds]`

* ``pixel_arcsec2`` -- Observed angular area of each image pixel. Note: Similar setup for ``cube_pixel_arcsec2``.
  :dv:`[Units: arcseconds^2]`

* ``slit_aperture`` -- Slit aperture width.
  :dv:`[Units: cm]`

* ``slit_aperture_arcsec`` -- Observed angular width of the slit aperture.
  :dv:`[Units: arcseconds]`

* ``radial_image_radius`` -- Radial image radius. Note: Similar setup for ``radial_cube_radius``.
  :dv:`[Units: cm]`

* ``radial_image_radius_bbox`` -- Radial image radius in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``radial_cube_radius_bbox``.
  :dv:`[Units: bounding box]`

* ``radial_image_radius_Rvir`` -- Radial image radius in units of the selected halo virial radius :math:`R_{\rm vir}`. Note: Similar setup for ``radial_cube_radius_Rvir``.
  :dv:`[Units: Virial radius]`

* ``n_radial_pixels`` -- Number of radial image pixels. Note: Similar setup for ``n_radial_cube_pixels``.
  :dv:`[Default value: 100]`

* ``radial_pixel_width`` -- Intrinsic width of each radial image pixel. Note: Similar setup for ``radial_cube_pixel_width``.
  :dv:`[Units: cm]`

* ``radial_pixel_arcsec`` -- Observed angular width of each radial image pixel. Note: Similar setup for ``radial_cube_pixel_arcsec``.
  :dv:`[Units: arcseconds]`

.. note:: Some combinations of camera parameters are redundant but the configuration detects inconsistencies and gives sensible error messages to address these. If left unspecified, the cameras are set up to capture the entire domain as defined by the simulation bounding box.

.. _Escape Parameters:

Escape Parameters
-----------------

The following parameters are related to controlling the conditions for escape:

* ``spherical_escape`` -- Flag to restrict ray-tracing a spherical region.
  :dv:`[Default value: false | compile-time option]`

* ``escape_center`` -- Center of the escape region in `[0 0 0]` format.
  :dv:`[Default value: (0,0,0) | Units: cm]`

* ``escape_radius`` -- Radius for the spherical escape region.
  :dv:`[Units: cm]`

* ``escape_radius_bbox`` -- Radius for the spherical escape region in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``escape_radius_Rvir`` relative to the selected halo virial radius :math:`R_{\rm vir}`.
  :dv:`[Units: bounding box]`

* ``emission_radius`` -- Radius for the spherical emission region.
  :dv:`[Units: cm]`

* ``emission_radius_bbox`` -- Radius for the spherical emission region in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``emission_radius_Rvir`` relative to the selected halo virial radius :math:`R_{\rm vir}`.
  :dv:`[Units: bounding box]`

* ``box_escape`` -- Flag to restrict ray-tracing to a box region.
  :dv:`[Default value: false | compile-time option]`

* ``streaming_escape`` -- Flag to restrict ray-tracing to a maximum free-streaming distance.
  :dv:`[Default value: false | compile-time option]`

* ``max_streaming`` -- Maximum free-streaming distance.
  :dv:`[Units: cm]`

* ``max_streaming_kpc`` -- Maximum free-streaming distance in units of kpc.
  :dv:`[Units: kpc]`

.. _Additional File Parameters:

Additional File Parameters
--------------------------

The following parameters are related to specific additional I/O files:

* ``save_connections`` -- Save the Voronoi connections to a file.
  :dv:`[Default value: true]`

* ``save_circulators`` -- Save the Voronoi circulators to a file.
  :dv:`[Default value: true]`

* ``avoid_edges`` -- Avoid ray-tracing through edge cells.
  :dv:`[Default value: true]`

* ``inner_edges`` -- Flag to also treat inner neighbors of edge cells as edges.
  :dv:`[Default value: false]`

* ``output_inner_edges`` -- Flag to output inner edge cells (for tracking purposes).
  :dv:`[Default value: true]`

* ``cgal_file`` -- CGAL connectivity file (optional).
  :dv:`[Default value:` ``init_file``:dv:`]`

* ``cgal_dir`` -- CGAL connectivity directory (optional).
  :dv:`[Default value:` ``init_dir``:dv:`]`

* ``cgal_subdir`` -- CGAL connectivity subdirectory, e.g. allows ``ics/snap_000`` organization.
  :dv:`[Default value:` ``cgal_dir``:dv:`]`

* ``cgal_base`` -- CGAL connectivity file base (optional).
  :dv:`[Default value:` ``init_base``:dv:`]`

* ``cgal_ext`` -- CGAL connectivity file extension (optional).
  :dv:`[Default value:` ``init_ext``:dv:`]`

.. note:: The CGAL connectivity is usually saved to the original initial conditions file for future use. Here we allow the option of either not saving the connections or writing them to a different file.

* ``group_file`` and ``subhalo_file`` (or ``halo_file`` generically) -- Group or subhalo catalog file (optional).
  :dv:`[Default value:` ``init_file``:dv:`]`

* ``group_dir`` and ``subhalo_dir`` -- Group or subhalo catalog directory (optional).
  :dv:`[Default value:` ``init_dir``:dv:`]`

* ``group_subdir`` and ``subhalo_subdir`` -- Group or subhalo catalog subdirectory, e.g. allows ``ics/snap_000`` organization.
  :dv:`[Default value:` ``group_dir``:dv:`]`

* ``group_base`` and ``subhalo_base`` -- Group or subhalo catalog file base (optional).
  :dv:`[Default value:` ``init_base``:dv:`]`

* ``group_ext`` and ``subhalo_ext`` -- Group or subhalo catalog file extension (optional).
  :dv:`[Default value:` ``init_ext``:dv:`]`

.. note:: The group and subhalo catalogs provide yet another option for spatial reasoning, e.g. cameras can aim at a particular group or subhalo without changing config files.

* ``abundances_file`` -- Abundances initial conditions file (optional).
  :dv:`[Default value:` ``init_file``:dv:`]`

* ``abundances_dir`` -- Abundances initial conditions directory (optional).
  :dv:`[Default value:` ``init_dir``:dv:`]`

* ``abundances_subdir`` -- Abundances initial conditions subdirectory, e.g. allows ``ics/snap_000`` organization.
  :dv:`[Default value:` ``abundances_dir``:dv:`]`

* ``abundances_base`` -- Abundances initial conditions file base (optional).
  :dv:`[Default value:` ``init_base``:dv:`]`

* ``abundances_ext`` -- Abundances initial conditions file extension (optional).
  :dv:`[Default value:` ``init_ext``:dv:`]`

* ``abundances_output_file`` -- Abundances output file (optional).
  :dv:`[Default value:` ``abundances_file``:dv:`]`

* ``abundances_output_dir`` -- Abundances output directory (optional).
  :dv:`[Default value:` ``abundances_dir``:dv:`]`

* ``abundances_output_subdir`` -- Abundances output subdirectory, e.g. allows ``ics/snap_000`` organization.
  :dv:`[Default value:` ``abundances_output_dir``:dv:`]`

* ``abundances_output_base`` -- Abundances output file base (optional).
  :dv:`[Default value:` ``abundances_base``:dv:`]`

* ``abundances_output_ext`` -- Abundances output file extension (optional).
  :dv:`[Default value:` ``abundances_ext``:dv:`]`

.. note:: The abundances are usually read/saved to the original initial conditions file for future use. Here we allow the option of reading/writing them to a different file.

.. _Advanced Parameters:

Advanced Parameters
-------------------

The following parameters are related to advanced options:

* ``set_density_from_mass`` -- Set the density by reading the mass and dividing by volume (``m`` expecting units of ``g``). Note: This can be useful for applying density-like analyses methods, e.g. ray-based projections, to point-like data types such as SPH data, star particles, or dark matter.
  :dv:`[Default value: false]`

* ``read_density_as_mass`` -- Similar to ``set_density_from_mass`` but reads the density as a mass-like field before dividing by volume (``rho`` expecting units of ``g/cm^3``).
  :dv:`[Default value: false]`

* ``use_internal_energy`` -- Set the temperature by reading the (mass) specific internal energy (``e_int`` expecting units of ``cm^2/s^2``). Note: When activated this overrides reading the temperature directly. When the temperature is not present in the initial conditions file and ``constant_temperature`` is not set, the default for this flag will update to true.
  :dv:`[Default value: false]`
