.. _rays:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Ray Data Extractions
====================

This page explains the options for the extraction of ray-like data from simulations.

.. _Ray General Parameters:

General Ray Parameters
----------------------

The ray directions are set by the standard camera options. In addition, the following parameters affect how ray origins are selected:

* ``origins_bbox`` -- List of ray origins (optional).
  :dv:`[Units: bounding box]`

* ``origin_bbox`` -- Common origin for rays (optional).
  :dv:`[Units: bounding box]`

Otherwise, the origin is taken from the group catalogs using the ``group`` or ``subhalo`` command line argument following the ``snapshot`` argument:

* ``fof_base`` -- Friends of friends base name (optional).
  :dv:`[Default value: fof_subhalo_tab]`

* ``black_hole_origin`` -- Flag to use the most massive black hole of the subhalo (optional).
  :dv:`[Default value: false]`

* ``lyman_alpha_origin`` -- Flag to use the Lyman-alpha subhalo catalog (optional).
  :dv:`[Default value: false]`

The following parameters control the start and length of the rays:

* ``start_fvir`` -- Ray starting radius offset intended for halo origins.
  :dv:`[Units: virial radius]`

* ``start_cMpc`` -- Ray starting radius offset intended for halo origins.
  :dv:`[Units: cMpc]`

* ``length_kms`` -- Ray length proxy of Doppler shift induced by Hubble flow.
  :dv:`[Units: km/s]`

* ``length_bbox`` -- Ray length in special box units.
  :dv:`[Units: bounding box]`

* ``length_Mpc`` -- Ray length in physical units.
  :dv:`[Units: Mpc]`

* ``length_cMpc`` -- Ray length in comoving units.
  :dv:`[Units: cMpc]`

* ``impact_kpc`` -- Cylinder impact radius in physical units (optional optimization).
  :dv:`[Units: kpc]`

The following parameters control the ray output fields (note that density is always output):

* ``output_dust`` -- Flag to output the dust metallicity.
  :dv:`[Default value: true]`

* ``output_metallicity`` -- Flag to output the metallicity.
  :dv:`[Default value: true]`

* ``output_HI`` -- Flag to output the HI fraction.
  :dv:`[Default value: true]`

* ``output_HeI`` -- Flag to output the HeI fraction.
  :dv:`[Default value: false]`

* ``output_HeII`` -- Flag to output the HeII fraction.
  :dv:`[Default value: false]`

* ``output_neutral`` -- Flag to output the neutral hydrogen abundance.
  :dv:`[Default value: false]`

* ``output_electrons`` -- Flag to output the electron abundance.
  :dv:`[Default value: true]`

* ``output_internal_energy`` -- Flag to output the internal energy.
  :dv:`[Default value: true]`

* ``output_SFR`` -- Flag to output the star formation rate.
  :dv:`[Default value: true]`

* ``output_parallel_velocity`` -- Flag to output the parallel velocity.
  :dv:`[Default value: true]`

* ``output_velocities`` -- Flag to output the full :math:`(x,y,z)` velocities.
  :dv:`[Default value: false]`

* ``output_acceleration`` -- Flag to output the full :math:`(x,y,z)` acceleration.
  :dv:`[Default value: false]`

* ``output_radiation`` -- Flag to output the photon density.
  :dv:`[Default value: false]`

* ``output_magnetic_field`` -- Flag to output the magnetic field.
  :dv:`[Default value: false]`

* ``output_metals`` -- Flag to output the metal species fractions.
  :dv:`[Default value: false]`

* ``output_metals_as_densities`` -- Flag to output the metal species as densities rather than fractions.
  :dv:`[Default value: false]`
