.. _publications:

Publications
============

This page maintains a current list of publications using various aspects of COLT.

Original Methods Paper:
-----------------------

 * Smith, A., Safranek-Shrader, C., Bromm, V., Milosavljević, M., 2015, `MNRAS, 449, 4336 <https://doi.org/10.1093/mnras/stv565>`_: 'The Lyman α signature of the first galaxies' (`arXiv:1409.4480 <https://arxiv.org/abs/1409.4480>`_)

Science Papers:
---------------

 * McClymont, W., Tacchella, S., Smith, A., Kannan, R., Maiolino, R., Belfiore, F., Hernquist, L., Li, H., Vogelsberger, M., 2024, MNRAS, submitted: 'The nature of diffuse ionised gas in star-forming galaxies' (`arXiv:2403.03243 <https://arxiv.org/abs/2403.03243>`_)

 * Garaldi, E., Kannan, R., Smith, A., Borrow, J., Vogelsberger, M., Pakmor, R., Springel, V., Hernquist, L., Galárraga-Espinosa, D., Yeh, J. Y.-C., Shen, X., Xu, C., Neyer, M., Spina, B., Almualla, M., Zhao, Y., 2024, `MNRAS, in press <https://doi.org/10.1093/mnras/stae839>`_: 'The THESAN project: public data release of radiation-hydrodynamic simulations matching reionization-era JWST observations' (`arXiv:2309.06475 <https://arxiv.org/abs/2309.06475>`_)

 * Xu, C., Smith, A., Borrow, J., Garaldi, E., Kannan, R., Vogelsberger, M., Pakmor, R., Springel, V., Hernquist, L., 2023, `MNRAS, 521, 4356 <https://doi.org/10.1093/mnras/stad789>`_: 'The THESAN project: Lyman-α emitter luminosity function calibration' (`arXiv:2210.16275 <https://arxiv.org/abs/2210.16275>`_)

 * Yeh, J. Y.-C., Smith, A., Kannan, R., Garaldi, E., Vogelsberger, M., Borrow, J., Pakmor, R., Springel, V., Hernquist, L., 2023, `MNRAS, 520, 2757 <https://doi.org/10.1093/mnras/stad210>`_: 'The THESAN project: ionizing escape fractions of reionization-era galaxies' (`arXiv:2205.02238 <https://arxiv.org/abs/2205.02238>`_)

 * Jahn, E. D., Sales, L. V., Marinacci, F., Vogelsberger, M., Torrey, P., Qi, J., Smith, A., Li, H., Kannan, R., Burger, J. D., Zavala, J., 2023, `MNRAS, 520, 461 <https://doi.org/10.1093/mnras/stad109>`_: 'Real and counterfeit cores: how feedback expands haloes and disrupts tracers of inner gravitational potential in dwarf galaxies' (`arXiv:2110.00142 <https://arxiv.org/abs/2110.00142>`_)

 * Smith, A., Kannan, R., Tacchella, S., Vogelsberger, M., Hernquist, L., Marinacci, F., Sales, L.V., Torrey, P., Li, H., Yeh, J. Y.-C., Qi, J., 2022, `MNRAS, 517, 1 <https://doi.org/10.1093/mnras/stac2641>`_: 'The physics of Lyman-α escape from disc-like galaxies' (`arXiv:2111.13721 <https://arxiv.org/abs/2111.13721>`_)

 * Tacchella, S., Smith, A., Kannan, R., Marinacci, F., Hernquist, L., Vogelsberger, M., Torrey, P., Sales, L., Li, H., 2022, `MNRAS, 513, 2904 <https://doi.org/10.1093/mnras/stac818>`_: 'Hα emission in local galaxies: star formation, time variability, and the diffuse ionized gas' (`arXiv:2112.00027 <https://arxiv.org/abs/2112.00027>`_)

 * Garaldi, E., Kannan, R., Smith, A., Springel, V., Pakmor, R., Vogelsberger, M., Hernquist, L., 2022, `MNRAS, 512, 4909 <https://doi.org/10.1093/mnras/stac257>`_: 'The THESAN project: properties of the intergalactic medium and its connection to reionization-era galaxies' (`arXiv:2110.01628 <https://arxiv.org/abs/2110.01628>`_)

 * Smith, A., Kannan, R., Garaldi, E., Vogelsberger, M., Pakmor, R., Springel, V., Hernquist, L., 2022, `MNRAS, 512, 3243 <https://doi.org/10.1093/mnras/stac713>`_: 'The THESAN project: Lyman-α emission and transmission during the Epoch of Reionization' (`arXiv:2110.02966 <https://arxiv.org/abs/2110.02966>`_)

 * Kimock, B., Narayanan, D., Smith, A., Ma, X., Feldmann, R., Anglés-Alcázar, D., Bromm, V., Davé, R., Geach, J., Hopkins, P., Kereš, D., 2021, `ApJ, 909, 119 <https://doi.org/10.3847/1538-4357/abbe89>`_: 'The Origin and Evolution of Lyman-α Blobs in Cosmological Galaxy Formation Simulations' (`arXiv:2004.08397 <https://arxiv.org/abs/2004.08397>`_)

 * Lao, B.-X., Smith, A., 2020, `MNRAS, 497, 3925 <https://doi.org/10.1093/mnras/staa2198>`_: 'Resonant-line radiative transfer within power-law density profiles' (`arXiv:2005.09692 <https://arxiv.org/abs/2005.09692>`_)

 * Yang, Y.-L., Evans, N. J., Smith, A., Lee, J.-E., Tobin, J. J., Terebey, S., Calcutt, H., Jørgensen, J. K., Green, J. D., Bourke, T. L., 2020, `ApJ, 891, 61 <https://doi.org/10.3847/1538-4357/ab7201>`_: 'Constraining the Infalling Envelope Models of Embedded Protostars: BHR 71 and Its Hot Corino' (`arXiv:2002.01478 <https://arxiv.org/abs/2002.01478>`_)

 * Smith, A., Ma, X., Bromm, V., Finkelstein, S. L., Hopkins, P. F., Faucher-Giguère, C.-A., Kereš, D., 2019, `MNRAS, 484, 39 <https://doi.org/10.1093/mnras/sty3483>`_: 'The physics of Lyman α escape from high-redshift galaxies' (`arXiv:1810.08185 <https://arxiv.org/abs/1810.08185>`_)

 * Smith, A., Tsang, B. T.-H., Bromm, V., Milosavljević, M., 2018, `MNRAS, 479, 2065 <https://doi.org/10.1093/mnras/sty1509>`_: 'Discrete diffusion Lyman α radiative transfer' (`arXiv:1709.10187 <https://arxiv.org/abs/1709.10187>`_)

 * Smith, A., Becerra, F., Bromm, V., Hernquist, L., 2017, `MNRAS, 472, 205 <https://doi.org/10.1093/mnras/stx1993>`_: 'Radiative effects during the assembly of direct collapse black holes' (`arXiv:1706.02751 <https://arxiv.org/abs/1706.02751>`_)

 * Smith, A., Bromm, V., Loeb, A., 2017, `MNRAS, 464, 2963 <https://doi.org/10.1093/mnras/stw2591>`_: 'Lyman α radiation hydrodynamics of galactic winds before cosmic reionization' (`arXiv:1607.07166 <https://arxiv.org/abs/1607.07166>`_)

 * Smith, A., Bromm, V., Loeb, A., 2016, `MNRAS, 460, 3143 <https://doi.org/10.1093/mnras/stw1129>`_: 'Evidence for a direct collapse black hole in the Lyman α source CR7' (`arXiv:1602.07639 <https://arxiv.org/abs/1602.07639>`_)
