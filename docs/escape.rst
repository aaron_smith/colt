.. _escape:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Ray-tracing Radiative Transfer
==============================

This page explains the options for ray-tracing radiative transfer (RT) simulations.

.. _RT General Parameters:

General RT Parameters
---------------------

The following parameters are important MCRT parameters:

Coming soon.

.. * ``n_photons`` -- Actual number of photon packets used.
..   :dv:`[Default value: 1]`
