.. _api:

:mod:`pycolt`
=============

..
 .. currentmodule:: pycolt

..
 .. automodule:: pycolt
..
   :members:


.. Usage
.. *****

.. All functionality may be found in the ``pycolt`` package::

..     import pycolt

.. The information below represents the complete specification of the classes.


.. Cosmology Class
.. ***************

..
 .. autoclass:: pycolt.Cosmology
..
   :members:


.. Mesh Class
.. **********

..
 .. autoclass:: pycolt.Mesh
..
   :members:


.. Simulation Class
.. ****************

..
 .. autoclass:: pycolt.Simulation
..
   :members:

.. doxygenvariable:: km

.. doxygenclass:: Escape
   :members:
   :protected-members:
   :private-members:
   :undoc-members:

