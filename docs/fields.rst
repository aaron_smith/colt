.. _fields:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Field Names and Descriptions
============================

COLT aims to have short but intuitive names for its internal fields. This page provides a list of internal fields along with brief descriptions, units, and notes that might be helpful to remember for users or developers. Many fields are optional and only allocated when necessary and most can be used wih the projections module.

.. _Gas Fields:

Gas Fields
----------

* ``n_cells`` -- Number of cells :math:`n_\text{cells}`. In general, gas fields are arrays with this many elements.

* ``r`` -- Position coordinates :math:`(x,y,z)`. Each geometry may interpret this differently. :dv:`[Units: cm]`

* ``V`` -- Cell volume :math:`V`. :dv:`[Units: cm^3]`

* ``v`` -- Bulk velocity :math:`(v_x,v_y,v_z)`. :dv:`[Units: cm/s]`

* ``dvdr`` -- Bulk velocity gradient :math:`\text{d}v/\text{d}r`. :dv:`[Units: 1/s]`

* ``v0`` -- Velocity extrapolation :math:`v_0 = v - r \nabla v`. :dv:`[Units: cm/s]`

* ``rho`` -- Gas density :math:`\rho`. :dv:`[Units: g/cm^3]`

* ``T`` -- Temperature :math:`T`. :dv:`[Units: K]`

* ``e_int`` -- Internal energy :math:`e_\text{int}`. :dv:`[Units: cm^2/s^2]`

* ``n_H`` -- Hydrogen number density :math:`n_\text{H}`. :dv:`[Units: cm^-3]`

* ``n_He`` -- Helium number density :math:`n_\text{He}`. :dv:`[Units: cm^-3]`

* ``n_C`` -- Carbon number density :math:`n_\text{C}`. :dv:`[Units: cm^-3]`

* ``n_N`` -- Nitrogen number density :math:`n_\text{N}`. :dv:`[Units: cm^-3]`

* ``n_O`` -- Oxygen number density :math:`n_\text{O}`. :dv:`[Units: cm^-3]`

* ``n_Ne`` -- Neon number density :math:`n_\text{Ne}`. :dv:`[Units: cm^-3]`

* ``n_Mg`` -- Magnesium number density :math:`n_\text{Mg}`. :dv:`[Units: cm^-3]`

* ``n_Si`` -- Silicon number density :math:`n_\text{Si}`. :dv:`[Units: cm^-3]`

* ``n_S`` -- Sulfer number density :math:`n_\text{S}`. :dv:`[Units: cm^-3]`

* ``n_Fe`` -- Iron number density :math:`n_\text{Fe}`. :dv:`[Units: cm^-3]`

* ``n_upper`` and ``n_lower`` -- Number density of the upper and lower transitions of a two-level atom :math:`n_\text{upper}` and :math:`n_\text{lower}`. :dv:`[Units: cm^-3]`

* ``x_HI`` and ``x_HII`` -- Hydrogen ionization fractions for neutral and ionized gas (:math:`x_\text{HI} = n_\text{HI} / n_\text{H}`). There are also ionized fractions for metal number densities under the convention that each is normalized to unity for the given species, e.g. for helium (:math:`x_\text{HeI} = n_\text{HeI} / n_\text{He}`) :dv:`[Units: mass fraction]`

* ``x_H2`` -- Molecular hydrogen mass fraction (:math:`x_\text{H_2} = n_\text{H_2} / n_\text{H}`). :dv:`[Units: cm^-3]`

* ``x_e`` -- Electron abundance relative to hydrogen (:math:`x_e = n_e / n_\text{H}`). :dv:`[Units: cm^-3]`

* ``G_ion`` -- Total photoheating rate :math:`\Gamma_\text{ion}`. :dv:`[Units: erg/s]`

* ``SFR`` -- Star formation rate. :dv:`[Units: Msun/yr]`

* ``T_dust`` -- Dust temperature :math:`T_\text{dust}`. :dv:`[Units: K]`

* ``v2`` -- Velocity squared :math:`v^2`. :dv:`[Units: cm^2/^2]`

* ``v2_LOS`` -- Line-of-sght velocity squared :math:`v_\text{LOS}^2`. :dv:`[Units: cm^2/s^2]`

* ``v_LOS`` -- Line-of-sght velocity :math:`v_\text{LOS}`. :dv:`[Units: cm/s]`

* ``v_x`` -- Velocity in the camera x-axis direction :math:`v_x`. :dv:`[Units: cm/s]`

* ``v_y`` -- Velocity in the camera y-axis direction :math:`v_y`. :dv:`[Units: cm/s]`

* ``B`` -- Magnetic field :math:`B`. :dv:`[Units: Gauss]`

* ``B2`` -- Magnetic field squared :math:`B^2`. :dv:`[Units: Gauss^2]`

* ``B_LOS`` -- Line-of-sight magnetic field :math:`B_\text{LOS}`. :dv:`[Units: Gauss]`

* ``B_x`` -- Magnetic field in the camera x-axis direction :math:`B_x`. :dv:`[Units: cm/s]`

* ``B_y`` -- Magnetic field in the camera y-axis direction :math:`B_y`. :dv:`[Units: cm/s]`

* ``X`` -- Mass fraction of hydrogen :math:`X = \rho_\text{H} / \rho`. :dv:`[Units: mass fraction]`

* ``Y`` -- Mass fraction of helium :math:`Y = \rho_\text{He} / \rho`. :dv:`[Units: mass fraction]`

* ``Z`` -- Gas metallicity :math:`Z = \rho_Z / \rho`. :dv:`[Units: mass fraction]`

* ``Z_C`` -- Carbon metallicity :math:`Z_\text{C} = \rho_\text{C} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_N`` -- Nitrogen metallicity :math:`Z_\text{N} = \rho_\text{N} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_O`` -- Oxygen metallicity :math:`Z_\text{O} = \rho_\text{O} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_Ne`` -- Neon metallicity :math:`Z_\text{Ne} = \rho_\text{Ne} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_Mg`` -- Magnesium metallicity :math:`Z_\text{Mg} = \rho_\text{Mg} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_Si`` -- Silicon metallicity :math:`Z_\text{Si} = \rho_\text{Si} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_S`` -- Sulfer metallicity :math:`Z_\text{S} = \rho_\text{S} / \rho`. :dv:`[Units: mass fraction]`

* ``Z_Fe`` -- Iron metallicity :math:`Z_\text{Fe} = \rho_\text{Fe} / \rho`. :dv:`[Units: mass fraction]`

* ``D`` -- Dust-to-gas ratio :math:`\mathcal{D} = \rho_\text{dust} / \rho`. :dv:`[Units: mass fraction]`

* ``D_S`` -- Silicate dust-to-gas ratio :math:`\mathcal{D}_\text{Si} = \rho_\text{dust,Si} / \rho`. :dv:`[Units: mass fraction]`

* ``k_e`` -- Absorption coefficient of electrons :math:`k_e`. :dv:`[Units: 1/cm]`

* ``k_0`` -- Absorption coefficient of primary line :math:`k_0`. :dv:`[Units: 1/cm]`

* ``kp_0`` -- Absorption coefficient of doublet line :math:`k'_0`. :dv:`[Units: 1/cm]`

* ``k_dust`` -- Absorption coefficient of dust :math:`k_\text{dust}`. :dv:`[Units: 1/cm]`

* ``rho_dust`` -- Dust density :math:`\rho_\text{dust}`. :dv:`[Units: g/cm^3]`

* ``rho_dust_G`` -- Graphite dust density :math:`\rho_\text{dust,G}`. :dv:`[Units: g/cm^3]`

* ``rho_dust_S`` -- Silicate dust density :math:`\rho_\text{dust,S}`. :dv:`[Units: g/cm^3]`

* ``a`` -- Damping parameter for the Voigt function :math:`a`. :dv:`[Units: None]`

* ``atau`` -- Nonlocal estimate of :math:`a\tau_0` for each cell. :dv:`[Units: None]`

* ``j_line`` -- Primary line emissivity :math:`j`. :dv:`[Units: erg/s/cm^3]`

* ``jp_line`` -- Doublet line emissivity :math:`j'`. :dv:`[Units: erg/s/cm^3]`

* ``u_rad`` -- Radiation energy density :math:`u_\text{rad}`. :dv:`[Units: erg/cm^3]`

* ``a_rad`` -- Acceleration due to radiation pressure :math:`a_\text{rad}`. :dv:`[Units: cm/s^2]`

* ``a_rad_scat`` -- Scattering-based acceleration due to radiation pressure :math:`a_\text{rad,scat}`. :dv:`[Units: cm/s^2]`

* ``a_rad_r`` -- Acceleration due to radiation pressure in the radial direction :math:`a_\text{rad}`. :dv:`[Units: cm/s^2]`

* ``a_rad_r_scat`` -- Scattering-based acceleration due to radiation pressure in the radial direction :math:`a_\text{rad,scat}`. :dv:`[Units: cm/s^2]`

.. * ``P_rad`` -- Radiation pressure tensor :math:`P_\text{rad}`. :dv:`[Units: erg/cm^3]`

* ``P_rad_r`` -- Radiation pressure in the radial direction :math:`P_\text{rad}`. :dv:`[Units: erg/cm^3]`

* ``trap_r`` -- Cumulative trapping ratio :math:`t_\text{trap} / t_\text{light}`. :dv:`[Units: None]` (Spherical geometry only.)

* ``M_F_r`` -- Cumulative force multiplier :math:`M_F` in the radial direction. :dv:`[Units: None]` (Spherical geometry only.)

* ``M_F_r_scat`` -- Cumulative scattering-based force multiplier :math:`M_F^\text{scat}` in the radial direction. :dv:`[Units: None]` (Spherical geometry only.)

* ``P_u_r`` -- Cumulative pressure-to-energy density ratio :math:`P_\text{rad} / u_\text{rad}` in the radial direction. :dv:`[Units: None]` (Spherical geometry only.)

* ``rho2`` -- Density squared :math:`\rho^2`. :dv:`[Units: g^2/cm^6]`

* ``rho2HI`` -- Density squared times the neutral hydrogen fraction :math:`x_\text{HI} \rho^2`. :dv:`[Units: g^2/cm^6]`

* ``rho_star`` -- Stellar density of each cell :math:`\rho_\star`. :dv:`[Units: g/cm^3]`

* ``ion_front`` -- Ionization front. :dv:`[Units: None]`

.. _Star Fields:

Star Fields
-----------

* ``n_stars`` -- Number of stars :math:`n_\text{stars}`. In general, star fields are arrays with this many elements.

* ``r_star`` -- Position coordinates :math:`(x,y,z)`. :dv:`[Units: cm]`

* ``age_star`` -- Star age. :dv:`[Units: Gyr]`

* ``Z_star`` -- Star metallicity :math:`Z_\star`. :dv:`[Units: mass fraction]`

* ``m_init_star`` -- Initial stellar mass :math:`m_{\star,0}`. :dv:`[Units: Msun]`

* ``m_massive_star`` -- Current stellar mass contained in massive stars. :dv:`[Units: Msun]`

* ``L_cont_star`` -- Star continuum band flux :math:`L_\text{cont}`. :dv:`[Units: erg/s/angstrom]`

* ``L_line_star`` -- Star line luminosity :math:`L_\text{line}`. :dv:`[Units: erg/s]`
