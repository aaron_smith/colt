.. _mcrt:

.. raw:: html

   <style> .dv {color:#839192; font-weight:bold; font-style: italic} </style>

.. role:: dv

Monte Carlo Radiative Transfer
==============================

This page explains the options for Monte Carlo radiative transfer (MCRT) simulations. The MCRT module is designed for spatially and spectrally resolved line transport and is well-suited for modeling complex environments where line photons interact with gas and dust via resonant scattering, absorption, and emission.

.. _MCRT General Parameters:

General MCRT Parameters
-----------------------

The following parameters are important MCRT parameters:

* ``n_photons`` -- Number of photon packets use in the simulation.
  :dv:`[Default value: 1]`

* ``n_photons_per_star`` -- Number of photon packets per star particle.
  :dv:`[Optional]`

* ``n_photons_max`` -- Maximum number of photon packets allowed when ``n_photons_per_star`` is used.
  :dv:`[Optional]`

* ``output_stars`` -- Flag to output the emission and escape data for each star.
  :dv:`[Default value: false]`

* ``output_cells`` -- Flag to output the emission and escape data for each cell.
  :dv:`[Default value: false]`

* ``output_photons`` -- Flag to output escaped and absorbed photon packets.
  :dv:`[Default value: true]`

* ``photon_file`` -- Output a separate photon file with this base name, e.g. "photons".
  :dv:`[Optional]`

* ``output_n_scat`` -- Flag to output the number of scattering events for photon packets.
  :dv:`[Default value: false]`

* ``output_path_length`` -- Flag to output the cumulative path length for photon packets.
  :dv:`[Default value: false | Output units: cm]`

* ``output_collisions`` -- Flag to output collisional excitation data for photons and cameras.
  :dv:`[Default value: false]`

* ``output_source_position`` -- Flag to output the emission position for photon packets.
  :dv:`[Default value: false]`

.. _MCRT Line Parameters:

Line Parameters
---------------

The following parameters are related to line specifications:

* ``line`` -- Name of the line to be modeled.
  :dv:`[Default value: Lyman-alpha | Other options: Balmer-alpha, Balmer-beta, Balmer-gamma, Balmer-delta, Balmer-epsilon, Paschen-alpha, Paschen-beta, Brackett-alpha, CIII-1907-1909, CIV-1548-1550, NII-6550, NII-6585, NIII-1747-1749, NIV-1486, OI-6302, OI-6366, OII-3727-3730, OIII-1661-1666, OIII-4364, OIII-4933, OIII-4960, OIII-5008, NeIII-3969, MgII-2796-2803, SiII-1190-1193, SiII-1260, SiII-1304, SiII-1527, SiIII-1206, SiIV-1394-1403, SII-6718, SII-6733]`

* ``recoil`` -- Include recoil induced Doppler shifting with line scattering.
  :dv:`[Default value: true]`

* ``x_crit`` -- Constant critical frequency for core-skipping.
  :dv:`[Compile-time parameter | Default value: 0 | Valid range: x_crit >= 0]`

* ``dynamical_core_skipping`` -- Non-local core-skipping for resonance lines.
  :dv:`[Compile-time parameter | Default value: false]`

* ``n_exp_atau`` -- Healpix exponent for the non-local :math:`a \tau_0` core-skipping estimates, resulting in :math:`12 \times 4^{n_\text{exp,atau}}` directions.
  :dv:`[Default value: 0 | Valid range: n_exp_atau >= 0]`

* ``n_side_atau`` -- Healpix number of base pixel subdivisions for the non-local :math:`a \tau_0` core-skipping estimates, resulting in :math:`12 \times n_\text{side,atau}^2` directions.
  :dv:`[Overrides n_exp_atau | Default value: 1 | Valid range: n_side_atau >= 1]`

* ``p_dest`` -- Line destruction probability between absorption and re-emission.
  :dv:`[Default value: 0 | Valid range: 0 < p_dest < 1]`

* ``doppler_frequency`` -- Output frequency in units of Doppler widths, otherwise as a velocity offset from line center (in units of km/s). Doppler units should only be used for idealized simulations.
  :dv:`[Default value: false]`

* ``T_exit`` -- Exit temperature for standardized Doppler frequency.
  :dv:`[Default value: 1e4 | Units: K]`

* ``v_exit`` -- Exit velocity for shifted observer frames.
  :dv:`[Default value: (0,0,0) | Units: cm/s]`

* ``exit_wrt_com`` -- Exit with respect to the center of mass velocity frame.
  :dv:`[Default value: false]`

.. note:: In what follows the units of "freq" refer to km/s unless using doppler_frequency.

.. _MCRT Dust Parameters:

Dust Parameters
---------------

The following parameters are related to dust specifications:

* ``dust_model`` -- Dust model for the line radiative transfer setting the dust opacity, albedo, and anisotropy parameters.
  :dv:`[Options: SMC, MW, LAURSEN_SMC, filename]`

* ``kappa_dust`` -- Dust opacity at the wavelength of the line, scaling the absorption coefficient by the dust mass: :math:`k_\text{dust} = \kappa_\text{dust} \rho_\text{dust}`.
  :dv:`[Default value: Based on dust_model and line | Units: cm^2/g of dust]`

* ``sigma_dust`` -- Dust cross section (per metallicity) at the wavelength of the line, scaling the absorption coefficient by the hydrogen number density and metallicity: :math:`k_\text{dust} = \sigma_\text{dust} n_\text{H} Z`. It is generally recommended to use ``kappa_dust`` instead.
  :dv:`[Default value: Based on dust_model and line | Units: cm^2/Z/hydrogen atom]`

* ``albedo`` -- Dust scattering albedo defined as :math:`A = k_\text{scat} / (k_\text{abs} + k_\text{scat})`.
  :dv:`[Default value: Based on dust_model and line | Valid range: 0 < albedo < 1]`

* ``g_dust`` -- Anisotropy parameter :math:`\langle \mu \rangle` for dust scattering.
  :dv:`[Default value: Based on dust_model and line | Valid range: -1 < g_dust < 1]`

* ``f_ion`` -- Survival fraction of dust in :math:`{\rm H\,{\small II}}` regions.
  :dv:`[Default value: 1 | Valid range: 0 < f_ion < 1]`

* ``f_dust`` -- Fraction of metals locked in dust grains.
  :dv:`[Default value: 0 | Valid range: 0 < f_dust < 1]`

* ``f_PAH`` -- Fraction of carbonaceous dust in polycyclic aromatic hydrocarbons (PAHs).
  :dv:`[Default value: 0 | Valid range: 0 < f_PAH < 1]`

* ``T_sputter`` -- Thermal sputtering temperature cutoff, such that no dust exists within gas hotter than this value.
  :dv:`[Default value: 1e6 | Units: K]`

.. _MCRT Radiation Source Parameters:

Radiation Source Parameters
---------------------------

The following parameters are related to line radiative transfer options:

* ``j_exp`` -- Luminosity boosting exponent for power-law emission biasing, i.e. probabilities are rescaled according to :math:`\propto \mathcal{L}_\text{cell}^{j_\text{exp}}` such that :math:`j_\text{exp} < 1` can better sample lower emissivity regions.
  :dv:`[Default value: 1 | Valid range: 0 < j_exp < 1]`

* ``V_exp`` -- Volume boosting exponent for power-law emission biasing, i.e. probabilities are rescaled according to :math:`\propto V_\text{cell}^{V_\text{exp}}` such that :math:`V_\text{exp} < 1` can better sample smaller volume regions.
  :dv:`[Default value: 1]`

.. _MCRT Gas Emission Parameters:

Gas Emission Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters control gas emission processes:

* ``recombinations`` -- Include recombination emission with luminosity :math:`\mathcal{L}_\text{rec} = h \nu_0 \int \alpha_\text{B,eff}(T) n_e n_\text{HII} \text{d}V`.
  :dv:`[Default value: false]`

* ``T_floor_rec`` -- Apply a temperature floor for the recombination rate coefficient.
  :dv:`[Default value: 0 | Units: K]`

* ``collisions`` -- Include collisional excitation emission with luminosity for Lyα of :math:`\mathcal{L}_\text{col} = h \nu_0 \int q_\text{col}(T) n_e n_\text{HI} \text{d}V`. Note: This flag works for all implemented hydrogen lines and many collisionally excited metal lines.
  :dv:`[Default value: false]`

* ``collisions_limiter`` -- Limit collisional excitation emission by the photoheating rate. Note: Requires ``collisions`` to be true and reads the photoheating rate from the initial conditions file.
  :dv:`[Default value: None | Valid range: > 1]`

* ``emission_n_min`` -- Minimum number density of emitting cells.
  :dv:`[Default value: 0 | Units: cm^-3]`

* ``emission_n_max`` -- Maximum number density of emitting cells.
  :dv:`[Default value: infinity | Units: cm^-3]`

* ``emission_ne_min`` -- Minimum electron number density of emitting cells.
  :dv:`[Default value: 0 | Units: cm^-3]`

* ``emission_ne_max`` -- Maximum electron number density of emitting cells.
  :dv:`[Default value: infinity | Units: cm^-3]`

.. _MCRT Stellar Source Parameters:

Stellar Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~

These parameters define the stellar sources in the simulation:

* ``continuum`` -- Include stellar continuum emission around the line.
  :dv:`[Default value: false]`

* ``continuum_range`` -- Continuum velocity offset range for frequency sampling.
  :dv:`[Default value: (-2000,2000) | Units: km/s]`

* ``unresolved_HII`` -- Include unresolved HII regions in the simulation. Note: This only works for the Lyman-alpha, Balmer-alpha, and Balmer-beta lines with the conversion factors being 0.68, 0.45, and 0.12, respectively. For example, the Lyman-alpha luminosity is :math:`\mathcal{L}_{\text{Ly}\alpha} = 0.68 \times h \nu_0 (1 - f_\text{esc}^\text{HII}) \dot{N}_\text{ion}`.
  :dv:`[Default value: false]`

* ``f_esc_HII`` -- Escape fraction from unresolved HII regions.
  :dv:`[Default value: 0 | Valid range: 0 < f_esc_HII < 1]`

* ``source_model`` -- Model for the stellar sources to inform ``continuum`` and ``unresolved_HII`` configurations. Note: Using certain source_model options requires reading initial stellar masses, metallicities, and ages.
  :dv:`[Default value: "" | Options: BC03-IMF, BPASS-IMF-135-100, BPASS-CHAB-100]`

.. _MCRT Point Source Parameters:

Point Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters define point sources in the simulation:

* ``point`` -- Point source position in `[x y z]` format.
  :dv:`[Optional | Units: cm]`

* ``x_point``, ``y_point``, ``z_point`` -- Point source :math:`(x, y, z)` coordinates.
  :dv:`[Optional | Units: cm]`

* ``L_point`` -- Point source line luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

.. _MCRT Plane Source Parameters:

Plane Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters define plane sources in the simulation:

* ``plane_direction`` -- Direction of the plane source.
  :dv:`[Options: '+x', '-x', '+y', '-y', '+z', '-z']`

* ``S_plane_cont`` -- Plane continuum surface flux.
  :dv:`[Optional | Units: erg/s/cm^2/angstrom]`

* ``F_plane_cont`` -- Plane continuum source flux.
  :dv:`[Optional | Units: erg/s/angstrom]`

* ``L_plane_cont`` -- Plane continuum source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

* ``plane_beam`` -- Use an ellipsoidal beam instead of a rectangular plane.
  :dv:`[Default value: false]`

* ``plane_center_x_bbox``, ``plane_center_y_bbox``, ``plane_center_z_bbox`` -- Center of the plane source in bounding box units.
  :dv:`[Valid range: 0 < value < 1]`

* ``plane_radius_x_bbox``, ``plane_radius_y_bbox``, ``plane_radius_z_bbox`` -- Radii of the plane source in bounding box units.
  :dv:`[Valid range: > 0]`

.. _MCRT Sphere Source Parameters:

Sphere Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~~

These parameters define sphere sources in the simulation:

* ``sphere_center_cont`` -- Sphere center position (continuum) in `[x, y, z]` format.
  :dv:`[Optional | Units: cm]`

* ``sphere_radius_cont`` -- Sphere radius (continuum).
  :dv:`[Optional | Units: cm]`

* ``L_sphere_cont`` -- Sphere continuum source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

.. _MCRT Shell Source Parameters:

Shell Source Parameters
~~~~~~~~~~~~~~~~~~~~~~~

These parameters define sphere sources in the simulation:

* ``r_shell_line`` -- Shell source radius (line emission). Note: A shell source is a spherical shell with a uniform emissivity and outward initial flux.
  :dv:`[Optional | Units: cm]`

* ``L_shell_line`` -- Shell line source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

* ``r_shell_cont`` -- Shell radius (continuum emission).
  :dv:`[Optional | Units: cm]`

* ``L_shell_cont`` -- Shell continuum source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

* ``shell_blackbody`` -- Set the shell luminosity based on a blackbody spectrum.
  :dv:`[Default value: false]`

* ``T_shell`` -- Shell blackbody temperature.
  :dv:`[Required if shell_blackbody is true | Units: K]`

* ``r_shock`` -- Shock source radius (continuum emission). Note: A shock source is a spherical shell with a uniform emissivity and isotropic initial flux.
  :dv:`[Optional | Units: cm]`

* ``shock_blackbody`` -- Set the shock luminosity based on a blackbody spectrum.
  :dv:`[Default value: false]`

* ``T_shock`` -- Shock blackbody temperature.
  :dv:`[Required if shock_blackbody is true | Units: K]`

* ``L_shock`` -- Shock source luminosity.
  :dv:`[Default value: 1 | Units: erg/s]`

.. note:: Stellar continuum sources should not be used simultaneously with plane, sphere, shell, or shock continuum sources.

.. _MCRT Observed Output Parameters:

Observed Output Parameters
--------------------------

This section describes the output options for MCRT simulations, including angle-averages, peel-off cameras, and directional binning parameters.

.. _MCRT Escape Parameters:

Angle-averaged Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~

The following parameters are related to angular-averaged escape options:

* ``output_flux_avg`` -- Output the angle-averaged flux.
  :dv:`[Default value: true]`

* ``output_radial_avg`` -- Output the angle-averaged radial surface brightness.
  :dv:`[Default value: false]`

* ``output_radial_cube_avg`` -- Output the angle-averaged radial spectral data cube.
  :dv:`[Default value: false]`

.. _MCRT Camera Parameters:

Camera Parameters
~~~~~~~~~~~~~~~~~

The following parameters are related to cameras options:

* ``n_bins`` -- Number of frequency bins.
  :dv:`[Default value: 100]`

* ``n_slit_bins`` -- Number of frequency bins for slits.
  :dv:`[Default value: n_bins]`

* ``n_cube_bins`` -- Number of frequency bins for data cubes.
  :dv:`[Default value: n_bins]`

* ``n_radial_cube_bins`` -- Number of frequency bins for radial data cubes.
  :dv:`[Default value: n_bins]`

* ``freq_range`` -- Frequency extrema in `[min max]` format. Note: Similar setup for ``slit_freq_range``, ``cube_freq_range``, and ``radial_cube_freq_range``.
  :dv:`[Default value: (-1000,1000) | Units: freq]`

* ``freq_min`` -- Frequency range minimum. Note: Similar setup for ``slit_freq_min``, ``cube_freq_min``, and ``radial_cube_freq_min``.
  :dv:`[Default value: -1000 | Units: freq]`

* ``freq_max`` -- Frequency range maximum. Note: Similar setup for ``slit_freq_max``, ``cube_freq_max``, and ``radial_cube_freq_max``.
  :dv:`[Default value: 1000 | Units: freq]`

* ``output_fluxes`` -- Output spectral fluxes.
  :dv:`[Default value: true | Dimensions: (n_cameras, n_bins) | Output units: erg/s/cm^2/angstrom]`

* ``output_images`` -- Output surface brightness images.
  :dv:`[Default value: true | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_slits`` -- Output spectral slits.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_slit_pixels, n_slit_bins) | Output units: erg/s/cm^2/arcsec^2/angstrom]`

* ``output_cubes`` -- Output spectral data cubes.
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_cube_pixels, ny_cube_pixels, n_cube_bins) | Output units: erg/s/cm^2/arcsec^2/angstrom]`

* ``output_images2`` -- Output statistical moment images, e.g. to estimate convergence statistics as the relative error is approximately :math:`\sqrt{\text{images2}} / \text{images}`.
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: erg^2/s^2/cm^4/arcsec^4]`

* ``output_radial_images`` -- Output radial surface brightness images.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_radial_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_radial_cubes`` -- Output radial spectral data cubes.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_radial_cube_pixels, n_radial_cube_bins) | Output units: erg/s/cm^2/arcsec^2/angstrom]`

* ``output_radial_images2`` -- Output statistical moment radial images, e.g. to estimate convergence statistics as the relative error is approximately :math:`\sqrt{\text{radial\_images2}} / \text{radial\_images}`.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_radial_pixels) | Output units: erg^2/s^2/cm^4/arcsec^4]`

The following parameters produce intrinsic or attenuation only images:

* ``output_mcrt_emission`` -- Output intrinsic emission without transport based on MCRT sampling (also works for radial images).
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_mcrt_attenuation`` -- Output attenuated emission without scattering based on MCRT sampling (also works for radial images).
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_proj_emission`` -- Output intrinsic emission without transport with ray-based imaging.
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_proj_attenuation`` -- Output attenuated emission without scattering with ray-based imaging.
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels) | Output units: erg/s/cm^2/arcsec^2]`

.. note:: The intrinsic and attenuation only images do not require scattering. Therefore, the projection outputs are preferred as they employ a ray-based quadtree imaging method for a noiseless solution with adaptive spatial convergence.

The following parameters produce spatially integrated camera frequency moments :dv:`[Dimensions: n_cameras]`:

* ``output_escape_fractions`` -- Output camera escape fractions.
  :dv:`[Default value: true]`

* ``output_freq_avgs`` -- Output camera frequency averages.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq_stds`` -- Output frequency standard deviations.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq_skews`` -- Output frequency skewnesses.
  :dv:`[Default value: false]`

* ``output_freq_kurts`` -- Output frequency kurtoses.
  :dv:`[Default value: false]`

The following parameters produce camera frequency moments for each image pixel :dv:`[Dimensions: (n_cameras, nx_pixels, ny_pixels)]`:

* ``output_freq_images`` -- Output average frequency images.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq2_images`` -- Output frequency^2 images.
  :dv:`[Default value: false | Output units: freq^2]`

* ``output_freq3_images`` -- Output frequency^3 images.
  :dv:`[Default value: false | Output units: freq^3]`

* ``output_freq4_images`` -- Output frequency^4 images.
  :dv:`[Default value: false | Output units: freq^4]`

The following parameters produce camera frequency moments for each radial image pixel :dv:`[Dimensions: (n_cameras, n_radial_pixels)]`:

* ``output_freq_radial_images`` -- Output average frequency radial images.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq2_radial_images`` -- Output frequency^2 radial images.
  :dv:`[Default value: false | Output units: freq^2]`

* ``output_freq3_radial_images`` -- Output frequency^3 radial images.
  :dv:`[Default value: false | Output units: freq^3]`

* ``output_freq4_radial_images`` -- Output frequency^4 radial images.
  :dv:`[Default value: false | Output units: freq^4]`

The following parameters are useful for on-the-fly false color imaging:

* ``output_rgb_images`` -- Output flux-weighted frequency RGB images. Note: The normalized image log intensity can be used as the alpha opacity of the image to give a nice illustration of the spectral data cube.
  :dv:`[Default value: false | Dimensions: (n_cameras, nx_pixels, ny_pixels, 3) | Output units: RGB color]`

* ``output_rgb_radial_images`` -- Output flux-weighted frequency RGB radial images.
  :dv:`[Default value: false | Dimensions: (n_cameras, n_radial_pixels, 3) | Output units: RGB color]`

* ``output_rgb_map`` -- Output flux-weighted frequency RGB Healpix map.
  :dv:`[Default value: false | Dimensions: (12 * n_side_map^2, 3) | Output units: RGB color]`

* ``rgb_freq_range`` -- Frequency extrema in `[min max]` format.
  :dv:`[Default value: (-1000,1000) | Units: freq]`

* ``rgb_freq_min`` -- Frequency range minimum.
  :dv:`[Default value: -1000 | Units: freq]`

* ``rgb_freq_max`` -- Frequency range maximum.
  :dv:`[Default value: 1000 | Units: freq]`

* ``adjust_camera_frequency`` -- Adjust frequencies for each camera based on offsets from a file. Note: This was implemented to center the RGB and other cameras for Lyα on the Hα line center. Thus, the file must have a dataset called "freq_avgs" of length n_cameras.
  :dv:`[Default value: false]`

* ``freq_offset_file`` -- Frequency offset file (optional).
  :dv:`[Default value:` ``init_file``:dv:`]`

* ``freq_offset_dir`` -- Frequency offset directory (optional).
  :dv:`[Default value:` ``init_dir``:dv:`]`

* ``freq_offset_base`` -- Frequency offset file base (optional).
  :dv:`[Default value:` ``init_base``:dv:`]`

* ``freq_offset_ext`` -- Frequency offset file extension (optional).
  :dv:`[Default value: hdf5]`

.. _MCRT Healpix Binning Parameters:

Healpix Binning Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~

The following parameters are related to line-of-sight healpix binning options:

* ``n_exp_map`` -- Healpix exponent for the line-of-sight map, resulting in :math:`12 \times 4^{n_\text{exp,map}}` directions. Note: Similar setup for ``n_exp_flux``, ``n_exp_radial``, and ``n_exp_cube``.
  :dv:`[Default value: 2 | Valid range: n_exp_map >= 0]`

* ``n_side_map`` -- Healpix number of base pixel subdivisions for the line-of-sight map, resulting in :math:`12 \times n_\text{side,map}^2` directions. Note: Similar setup for ``n_side_flux``, ``n_side_radial``, and ``n_side_cube``.
  :dv:`[Overrides n_exp_map | Default value: 4 | Valid range: n_side_map >= 1]`

* ``n_map_bins`` -- Number of frequency bins for the line-of-sight flux map. Note: Similar setup for ``n_cube_map_bins``.
  :dv:`[Default value: 100]`

* ``map_freq_range`` -- Frequency extrema in `[min max]` format for the line-of-sight flux map. Note: Similar setup for ``cube_map_freq_range``.
  :dv:`[Default value: (-1000,1000) | Units: freq]`

* ``map_freq_min`` -- Frequency range minimum for the line-of-sight flux map. Note: Similar setup for ``cube_map_freq_min``.
  :dv:`[Default value: -1000 | Units: freq]`

* ``map_freq_max`` -- Frequency range maximum for the line-of-sight flux map. Note: Similar setup for ``cube_map_freq_max``.
  :dv:`[Default value: 1000 | Units: freq]`

* ``map_radius`` -- Radius for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``cube_map_radius``.
  :dv:`[Units: cm]`

* ``map_radius_bbox`` -- Radius for the line-of-sight healpix map of radial surface brightness images in units of the bounding box radius :math:`R_{\rm box}`. Note: Similar setup for ``cube_map_radius_bbox``.
  :dv:`[Units: bounding box]`

* ``map_radius_Rvir`` -- Radius for the line-of-sight healpix map of radial surface brightness images in units of the selected halo virial radius :math:`R_{\rm vir}`. Note: Similar setup for ``cube_map_radius_Rvir``.
  :dv:`[Units: Virial radius]`

* ``n_map_pixels`` -- Number of radial pixels for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``n_cube_map_pixels``.
  :dv:`[Default value: 100]`

* ``map_pixel_width`` -- Intrinsic width of each radial pixel for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``cube_map_pixel_width``.
  :dv:`[Units: cm]`

* ``map_pixel_arcsec`` -- Observed angular width of each radial pixel for the line-of-sight healpix map of radial surface brightness images. Note: Similar setup for ``cube_map_pixel_arcsec``.
  :dv:`[Units: arcseconds]`

* ``output_map`` -- Output a line-of-sight healpix map of escape fractions.
  :dv:`[Default value: false | Dimensions: (12 * n_side_map^2)]`

* ``output_map2`` -- Output a statistical moment line-of-sight healpix map, e.g. to estimate convergence statistics as the relative error is approximately :math:`\sqrt{\text{map2}} / \text{map}`.
  :dv:`[Default value: false | Dimensions: (12 * n_side_map^2)]`

* ``output_flux_map`` -- Output a line-of-sight healpix map of spectral fluxes.
  :dv:`[Default value: false | Dimensions: (12 * n_side_flux^2, n_map_bins) | Output units: erg/s/cm^2/angstrom]`

* ``output_radial_map`` -- Output a line-of-sight healpix map of radial surface brightness images.
  :dv:`[Default value: false | Dimensions: (12 * n_side_radial^2, n_map_pixels) | Output units: erg/s/cm^2/arcsec^2]`

* ``output_cube_map`` -- Output a line-of-sight healpix map of radial spectral data cubes.
  :dv:`[Default value: false | Dimensions: (12 * n_side_cube^2, n_cube_map_pixels, n_cube_map_bins) | Output units: erg/s/cm^2/arcsec^2/angstrom]`

The following parameters produce frequency moments for each Healpix map pixel :dv:`[Dimensions: 12 * n_side_map^2]`:

* ``output_freq_map`` -- Output average frequency map.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq2_map`` -- Output frequency^2 map.
  :dv:`[Default value: false | Output units: freq^2]`

* ``output_freq3_map`` -- Output frequency^3 map.
  :dv:`[Default value: false | Output units: freq^3]`

* ``output_freq4_map`` -- Output frequency^4 map.
  :dv:`[Default value: false | Output units: freq^4]`

.. _MCRT Angular Binning Parameters:

Angular Binning Parameters
~~~~~~~~~~~~~~~~~~~~~~~~~~

The following parameters are related to line-of-sight angular cosine (:math:`\mu=\cos\theta`) binning options:

* ``n_mu`` -- Number of directional bins for the line-of-sight :math:`\mu` map. Note: Similar setup for ``n_mu_flux``.
  :dv:`[Default value: 100]`

* ``n_mu_bins`` -- Number of frequency bins for the line-of-sight flux :math:`\mu` map.
  :dv:`[Default value: 100]`

* ``mu_freq_range`` -- Frequency extrema in `[min max]` format for the line-of-sight flux :math:`\mu` map.
  :dv:`[Default value: (-1000,1000) | Units: freq]`

* ``mu_freq_min`` -- Frequency range minimum for the line-of-sight flux :math:`\mu` map.
  :dv:`[Default value: -1000 | Units: freq]`

* ``mu_freq_max`` -- Frequency range maximum for the line-of-sight flux :math:`\mu` map.
  :dv:`[Default value: 1000 | Units: freq]`

* ``output_mu`` -- Output a line-of-sight :math:`\mu` map of escape fractions.
  :dv:`[Default value: false | Dimensions: n_mu)]`

* ``output_mu2`` -- Output statistical moment line-of-sight :math:`\mu` map, e.g. to estimate convergence statistics as the relative error is approximately :math:`\sqrt{\text{map2}} / \text{map}`.
  :dv:`[Default value: false | Dimensions: n_mu)]`

* ``output_flux_mu`` -- Output a line-of-sight :math:`\mu` map of spectral fluxes.
  :dv:`[Default value: false | Dimensions: (n_mu, n_mu_bins) | Output units: erg/s/cm^2/angstrom]`

The following parameters produce frequency moments for each :math:`\mu` map :dv:`[Dimensions: n_mu]`:

* ``output_freq_mu`` -- Output average frequency :math:`\mu` map.
  :dv:`[Default value: false | Output units: freq]`

* ``output_freq2_mu`` -- Output frequency^2 :math:`\mu` map.
  :dv:`[Default value: false | Output units: freq^2]`

* ``output_freq3_mu`` -- Output frequency^3 :math:`\mu` map.
  :dv:`[Default value: false | Output units: freq^3]`

* ``output_freq4_mu`` -- Output frequency^4 :math:`\mu` map.
  :dv:`[Default value: false | Output units: freq^4]`

.. _MCRT Radial Flow Parameters:

Radial Flow Parameters
----------------------

The radial flow data is a 3D probe of the radiation field. The output data is in the ``radial_flow`` group with an attribute ``n_radial_bins`` and fields ``radial_edges`` [cm] (radial bin edges), ``L_src`` (emission as a function of source radii), ``L_esc`` (escape as a function of source radii), ``L_abs`` (absorption as a function of absorption radii), ``L_pass`` (first passage by source radii and shell boundaries), and ``L_flow`` (net flux by source radii and shell boundaries).

The following parameters are compile-time options and need to be set in the ``defines.yaml`` file:

* ``output_radial_flow`` -- Output radial flow data.
  :dv:`[Compile-time parameter | Default value: false]`

* ``output_group_flows`` -- Output group flow data.
  :dv:`[Compile-time parameter | Default value: false]`

* ``output_subhalo_flows`` -- Output subhalo flow data.
  :dv:`[Compile-time parameter | Default value: false]`

The associated runtime parameters are:

* ``radial_linspace_pc`` -- Radial linear-spaced binning in parsecs for radial flow data as `[min max n_edges]`.
  :dv:`[Default value: 1 | Units: pc]`

* ``radial_logspace_pc`` -- Radial log-spaced binning in parsecs for radial flow data as `[log_min log_max n_edges]`.
  :dv:`[Default value: 1 | Units: pc]`

* ``radial_linspace_Rvir`` -- Radial linear-spaced binning in units of the selected halo virial radius :math:`R_{\rm vir}` for radial flow data as `[min max n_edges]`.
  :dv:`[Default value: 1 | Units: Virial radius]`

* ``radial_logspace_Rvir`` -- Radial log-spaced binning in units of the selected halo virial radius :math:`R_{\rm vir}` for radial flow data as `[log_min log_max n_edges]`.
  :dv:`[Default value: 1 | Units: Virial radius]`

* ``radial_edges_pc`` -- Radial edges in parsecs for radial flow data. Note: Similar config for ``radial_edges_Rvir`` and ``radial_edges_cm``.
  :dv:`[Default value: 1 | Units: pc]`

.. _MCRT Advanced Parameters:

Advanced Parameters
-------------------

The following parameters are related to advanced options:

* ``two_level_atom`` -- Line radiative transfer incorporates occupation number densities from both lower and upper transitions. If this is false then we assume that bound electrons are always in the ground state. Note: This currently also switches from a Voigt to a Gaussian line profile, and assumes complete redistribution for scattering.
  :dv:`[Default value: false]`

* ``spontaneous`` -- Include spontaneous emission in ``two_level_atom`` line transfer as :math:`\mathcal{L}_\text{sp} = h c A_{21} \int n_2 \text{d}V`.
  :dv:`[Default value: false]`

* ``resonant_scattering`` -- Turn resonant scattering on/off. Note: It is not recommended to override this option unless you know exactly what you are doing.
  :dv:`[Default value: Based on line]`

* ``anisotropic_scattering`` -- Flag to turn anisotropic scattering on or off (in the core of the line only).
  :dv:`[Default value: true for Lyman-alpha, false for other lines]`

* ``load_balancing`` -- Use MPI load balancing algorithms. Note: Due to the OpenMP load balancing this is not recommended in general as the number of MPI tasks is usually much smaller than the number of photon packets.
  :dv:`[Default value: false]`
